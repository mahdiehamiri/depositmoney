( function( $ ) {
$( document ).ready(function() {
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp();
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown();
			element.siblings('li').children('ul').slideUp();
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
		}
	});

	$('#cssmenu>ul>li.has-sub>a').append('<span class="holder"></span>');

	/*(function getColor() {
		var r, g, b;
		var textColor = $('#cssmenu').css('color');
		textColor = textColor.slice(4);
		r = textColor.slice(0, textColor.indexOf(','));
		textColor = textColor.slice(textColor.indexOf(' ') + 1);
		g = textColor.slice(0, textColor.indexOf(','));
		textColor = textColor.slice(textColor.indexOf(' ') + 1);
		b = textColor.slice(0, textColor.indexOf(')'));
		var l = rgbToHsl(r, g, b);
		if (l > 0.7) {
			$('#cssmenu>ul>li>a').css('text-shadow', '0 1px 1px rgba(0, 0, 0, .35)');
			$('#cssmenu>ul>li>a>span').css('border-color', 'rgba(0, 0, 0, .35)');
		}
		else
		{
			$('#cssmenu>ul>li>a').css('text-shadow', '0 1px 0 rgba(255, 255, 255, .35)');
			$('#cssmenu>ul>li>a>span').css('border-color', 'rgba(255, 255, 255, .35)');
		}
	})();*/

	function rgbToHsl(r, g, b) {
	    r /= 255, g /= 255, b /= 255;
	    var max = Math.max(r, g, b), min = Math.min(r, g, b);
	    var h, s, l = (max + min) / 2;

	    if(max == min){
	        h = s = 0;
	    }
	    else {
	        var d = max - min;
	        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
	        switch(max){
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }
	        h /= 6;
	    }
	    return l;
	}
		 $("#resetfileds").click(function(){
			 $("#menu_id").val("");
		 });
	   /* $(".editMenu").click(function(){
	    	var menuId = $(this).attr('data-id');
	    	var menuCaption = $(this).attr('data-caption');
	    	var menuExternalUrl = $(this).attr('data-external-url');
	    	var menuInternalUrl = $(this).attr('data-internal-url');
	    	var menuParentId = $(this).attr('data-parent-id');
	    	var menuClassNameUl = $(this).attr('data-class-name-ul');
	    	var menuClassNameLi = $(this).attr('data-class-name-li');
	    	var menuSort = $(this).attr('data-order');
	    	$("#menu_id").val(menuId);
	    	$("#caption").val(menuCaption);
	    	$("#external_url").val(menuExternalUrl);
	    	$("#internal_url").val(menuInternalUrl);
	    	$("#parent_id").val(menuParentId);
	    	$("#class_name_ul").val(menuClassNameUl);
	    	$("#class_name_li").val(menuClassNameLi);
	    	$("#order").val(menuSort);
	        });
	    /*$(".deleteMenu").click(function(){
	    	var menuId = $(this).attr('data-id');
	    	var elm = $(this);
			   $.ajax({
			      url: '/<?php echo $this->layout()->lang;?>/admin-menu-ajax/delete-menu/token/<?php echo $this->csrf;?>',
			      type:'POST',
			      data: 
				      {
				      	 id: menuId, 
				      },
			      success:function(result) {
			        if(result == "true") {
			        	$(elm).parent("li").hide();
			        }  else if (result == "child") {
			            alert("منو شامل فرزند می باشد.");
			        } else {
			            alert("خطا در حذف منو");
			        }
			      }
			   });
	    });*/
		
});
} )( jQuery );
