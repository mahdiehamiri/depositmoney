-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2015 at 01:09 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1-log
-- PHP Version: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `category_title`, `category_name`, `user_id`, `status`) VALUES
(1, 'متفرقه', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_author_user_id` int(11) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_content` text NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_status` tinyint(4) NOT NULL DEFAULT '0',
  `post_exerpt` text NOT NULL,
  `post_slug` varchar(255) NOT NULL,
  `post_meta_description` text NOT NULL,
  `post_meta_keywords` text NOT NULL,
  `post_source_url` varchar(255) NOT NULL,
  `post_source_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `post_slug` (`post_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1889 ;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `post_author_user_id`, `post_date`, `post_content`, `post_title`, `post_status`, `post_exerpt`, `post_slug`, `post_meta_description`, `post_meta_keywords`, `post_source_url`, `post_source_title`) VALUES
(1888, 1, '2015-10-07 09:32:54', '', 'این مقاله ی من است', 1, 'بیسبیسسبیبسیبیسبسیبیسبسی\r\nبیس\r\nیبس\r\n\r\nبسی\r\nبسی\r\nبسی', 'بیسبسی', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_categories`
--

CREATE TABLE IF NOT EXISTS `blog_post_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_post_id` int(11) NOT NULL,
  `blog_category_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_post_id` (`blog_post_id`),
  KEY `blog_category_id` (`blog_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=420 ;

--
-- Dumping data for table `blog_post_categories`
--

INSERT INTO `blog_post_categories` (`id`, `blog_post_id`, `blog_category_id`) VALUES
(419, 1888, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_tags`
--

CREATE TABLE IF NOT EXISTS `blog_post_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_post_id` int(11) NOT NULL,
  `blog_tag_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_post_id` (`blog_post_id`),
  KEY `blog_tag_id` (`blog_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=214 ;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `tag_title`) VALUES
(2, 'برنامه نویسی'),
(3, 'طراحی سایت'),
(4, 'طراحی گرافیک'),
(7, 'پی اچ پی'),
(8, 'فیسبوک'),
(9, 'ویندوز فون 8.1'),
(10, 'ساعت هوشمند'),
(11, 'Smartwatch'),
(13, 'اندروید'),
(14, 'آیفون'),
(15, 'Google Doodle'),
(16, 'خلاقیت'),
(17, 'شبکه های اجتماعی'),
(18, 'گوگل'),
(19, 'لیندا'),
(20, 'کارآفرینی'),
(21, 'استیو جابز'),
(22, 'اپل'),
(23, 'رایانش ابری'),
(24, 'Cloud'),
(25, 'اپلیکیشن'),
(26, 'موبایل'),
(27, 'وایبر'),
(28, 'جاوا'),
(29, 'SDK'),
(30, 'آمازون'),
(31, 'فناوری'),
(32, 'لینکدین'),
(33, 'اکشن'),
(34, 'فتوشاپ'),
(35, 'فضای منفی'),
(36, 'طراحی وب'),
(37, 'طراحی مسطح'),
(38, 'سی اس اس'),
(39, 'اچ تی ام ال'),
(40, 'سی اس'),
(41, 'موفقیت'),
(42, 'ثروت'),
(43, 'جام جهانی'),
(44, 'دور کاری'),
(45, 'xss'),
(46, 'Cross-site Scripting'),
(47, 'امنیت در PHP'),
(48, 'SQL Injection'),
(49, 'PHP'),
(50, 'IDE'),
(51, 'محیط یکپارچه برنامه نویسی'),
(52, 'Framework'),
(53, 'کایزن'),
(54, 'Kaizen'),
(55, 'الکسا'),
(56, 'Alexa'),
(57, 'اکلیپس'),
(58, 'اندروید استودیو'),
(59, 'توسعه اندروید'),
(60, 'برنامه نویسی اندروید'),
(62, 'همایش تجارت الکترونیک'),
(63, 'فریم ورک'),
(64, 'آموزش برنامه نویسی'),
(65, 'برنامه نویسی به کودکان'),
(66, 'ویندوز'),
(67, 'لینوکس'),
(68, 'مهاجرت به بازمتن'),
(69, 'خالق لینوکس'),
(70, 'لینوس تروالدز'),
(71, 'گیک'),
(72, 'نرد'),
(73, 'Geek'),
(74, 'همایش تجارت و اقتصاد الکترونیک'),
(75, 'کارگاه چرا کودکان می بایست برنامه نویسی یاد بگیرند'),
(76, 'مهندسی معکوس'),
(77, 'یادگیری برنامه نویسی'),
(78, 'کدنویسی'),
(79, 'Game Maven'),
(81, 'آموزش رایگان برنامه نویسی'),
(82, 'ویرایشگر کد'),
(83, 'Kate'),
(84, 'استارت آپ'),
(85, 'جسیکا آلبا'),
(86, 'هالیوود'),
(87, 'رویکرد جزء به کل در یادگیری برنامه نویسی'),
(88, 'مزایای برنامه نویسی'),
(89, 'راه اندازی وب سایت'),
(90, 'استارت آپ آنلاین'),
(91, 'مرورگر'),
(92, 'ایران موبی کد'),
(93, 'Koding'),
(94, 'نرم افزار آزاد'),
(95, 'مقاله نویسی'),
(96, 'سرچ'),
(97, 'ابزارهای هوشمند'),
(98, 'گیت هاب'),
(99, 'HTML'),
(100, 'وردپرس'),
(101, 'طراحی'),
(102, 'Material Design'),
(103, 'CSS'),
(104, 'جاوا اسکریپت'),
(105, 'JS'),
(106, 'کروم'),
(107, 'Chrome'),
(108, 'شبکه اجتماعی'),
(109, 'HumHub'),
(110, 'چابک'),
(111, 'agile'),
(112, 'گرادیانت'),
(114, 'Discourse'),
(115, 'تالار گفتگو'),
(116, 'اسکرام'),
(117, 'Waterbear'),
(118, 'باراک اوباما'),
(119, 'تجربه کاربری'),
(120, 'UX'),
(121, 'Linux'),
(122, 'Ubuntu'),
(123, 'متن باز'),
(124, 'UI'),
(125, 'رابط کاربری'),
(126, 'کفشدوزک'),
(128, 'تست برنامه نویسی'),
(129, 'Sublime Text'),
(130, 'سابلایم'),
(131, 'جادی'),
(132, 'کیبورد آزاد'),
(133, 'مصاحبه'),
(135, 'سالار کابلی'),
(136, 'وراثت در برنامه نویسی'),
(137, 'Inheritance'),
(138, 'روبی'),
(139, 'Ruby on Rails'),
(140, 'PDO'),
(141, 'تیراسیس'),
(142, 'پریسا تبریز'),
(143, 'CMS'),
(144, 'Ello.co'),
(145, 'Facebook'),
(146, 'Swift'),
(147, 'زبان های برنامه نویسی'),
(148, 'MySQL'),
(149, 'Google'),
(150, 'دیتابیس'),
(151, 'لاراول'),
(152, 'Laravel'),
(153, 'فریم ورک های پی اچ پی'),
(154, 'App'),
(155, 'API'),
(156, 'استارتاپ'),
(157, 'هک'),
(158, 'Buffer'),
(159, 'بهینه سازی تصویر'),
(160, 'آموزش زبان'),
(161, 'متا تگ'),
(173, 'سایت های اعتیادآور'),
(174, 'سئو'),
(175, 'SEO'),
(176, 'بهینه سازی سایت'),
(177, 'آزمون دیسک'),
(178, 'DISC Test'),
(179, 'Angular JS'),
(180, 'Backbone.js'),
(181, 'Ember.js'),
(183, 'انجین ایکس'),
(184, 'Nginx'),
(185, 'HTML5'),
(186, 'Asm.js'),
(187, 'Startup'),
(188, 'React Native'),
(189, 'React.js'),
(190, 'Google Code'),
(191, 'Git'),
(192, 'Data Science'),
(193, 'com.google'),
(194, 'Slate'),
(195, 'Adobe'),
(196, 'ادوبی'),
(197, 'قانون هشتاد بیست'),
(198, 'اندی روبین'),
(199, 'سکان آکادمی'),
(200, 'ARC'),
(201, 'پسورد'),
(202, 'Webydo'),
(203, 'IoT'),
(204, 'اشیاء اینترنتی'),
(205, 'مایکروسافت'),
(206, 'اینتل'),
(207, 'کامپوزر'),
(208, 'هوش مصنوعی'),
(209, 'Stackoverflow'),
(210, 'توسعه دهنده'),
(211, 'دات نت'),
(212, 'یو ایکس'),
(213, 'Lumen');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `entity_type` varchar(255) NOT NULL DEFAULT 'post',
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `entity_id`, `entity_type`, `user_id`, `comment`, `status`, `date`) VALUES
(92, 0, 0, 'birthday', 15, 'تست  1 کاربر عادی', 1, '2015-08-29 07:23:58'),
(98, 92, 0, 'birthday', 1, 'پاسخ ادمین به کامنت کاربر تست', 1, '2015-08-29 07:38:01'),
(99, 0, 0, 'birthday', 1, 'تست ادمین سایت', 1, '2015-08-29 07:45:38'),
(100, 0, 0, 'birthday', 15, 'تست 2 کاربر عادی', 1, '2015-08-29 07:51:01'),
(101, 92, 0, 'birthday', 14, 'پاسخ مترجم به کاربر عادی', 1, '2015-08-29 08:00:38'),
(102, 99, 0, 'birthday', 15, 'پاسخ کاربر عادی به ادمین سایت', 1, '2015-08-29 08:55:03'),
(103, 98, 0, 'birthday', 14, 'پاسخ مترجم به کامنت ادمین که برای کاربر عادی بود', 1, '2015-08-29 09:04:12'),
(104, 0, 0, 'birthday', 1, 'پیام تبریک تولد ادمین سایت', 1, '2015-08-29 10:01:55'),
(105, 0, 1865, 'post', 1, 'کامنت ادمین سایت برای وبلاگ', 1, '2015-08-29 10:02:16'),
(106, 0, 23, 'tutorial', 1, 'پیام ادمین سایت برای آموزش HTML', 1, '2015-08-29 10:02:44'),
(107, 0, 21, 'project', 1, 'project url comment', 1, '2015-09-05 14:00:07'),
(108, 0, 21, 'project', 1, 'jsjsfdsd', 1, '2015-09-05 14:09:50'),
(109, 104, 0, 'birthday', 20, 'این سکان آکادمی یک دو سه است', 1, '2015-09-05 14:57:39'),
(110, 100, 0, 'birthday', 20, 'fs', 1, '2015-09-05 15:16:47'),
(111, 110, 0, 'birthday', 20, 'fsd', 1, '2015-09-05 15:16:57'),
(112, 0, 1888, 'post', 1, 'fsdf', 1, '2015-10-07 10:05:40');

-- --------------------------------------------------------

--
-- Table structure for table `forum_categories`
--

CREATE TABLE IF NOT EXISTS `forum_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `main_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_category_id` (`main_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `name`, `main_category_id`) VALUES
(1, 'زبان جاوا', 2),
(3, 'زبان پی اچ پی', 2);

-- --------------------------------------------------------

--
-- Table structure for table `forum_main_categories`
--

CREATE TABLE IF NOT EXISTS `forum_main_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `forum_main_categories`
--

INSERT INTO `forum_main_categories` (`id`, `name`) VALUES
(2, 'برنامه نویسی'),
(3, 'شبکه'),
(4, 'طراحی سایت');

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts`
--

CREATE TABLE IF NOT EXISTS `forum_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_topic_id` int(11) NOT NULL,
  `post_content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `parent_id` (`parent_id`),
  KEY `forum_topic_id` (`forum_topic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `forum_posts`
--

INSERT INTO `forum_posts` (`id`, `forum_topic_id`, `post_content`, `user_id`, `parent_id`, `create_time`) VALUES
(13, 5, 'پاسخ ادمین به آقای مترجم پست اول - 1', 1, 9, '2015-10-02 11:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts_thanks`
--

CREATE TABLE IF NOT EXISTS `forum_posts_thanks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `forum_post_id` (`forum_post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forum_topics`
--

CREATE TABLE IF NOT EXISTS `forum_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_category_id` int(11) NOT NULL,
  `topic_name` varchar(255) NOT NULL,
  `topic_content` text,
  `user_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `topic_name` (`topic_name`),
  KEY `forum_category_id` (`forum_category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `forum_topics`
--

INSERT INTO `forum_topics` (`id`, `forum_category_id`, `topic_name`, `topic_content`, `user_id`, `create_time`) VALUES
(5, 3, 'پی اچ پی بهتر است یا دات نت', 'پی اچ پی بهتر است یا دات نتپی اچ پی بهتر است یا دات نتپی اچ پی بهتر است یا دات نتپی اچ پی بهتر است یا دات نت', 1, '2015-10-02 10:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `forum_topics_thanks`
--

CREATE TABLE IF NOT EXISTS `forum_topics_thanks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(255) NOT NULL,
  `permission_label` varchar(255) NOT NULL,
  `exception` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permission_name`, `permission_label`, `exception`) VALUES
(1, 'application-admin-signin', 'application-admin-signin', 1),
(2, 'application-admin-signup', 'application-admin-signup', 1),
(3, 'application-admin-forgot-password', 'application-admin-forgot-password', 1),
(4, 'application-admin-reset-password', 'application-admin-reset-password', 1),
(5, 'application-admin-index', 'application-admin-index', 0),
(6, 'application-index-index', 'application-index-index', 0),
(8, 'blog-admin-post-list', 'blog-admin-post-list', 0),
(9, 'blog-admin-add-post', 'blog-admin-add-post', 0),
(10, 'blog-admin-category-list', 'blog-admin-category-list', 0),
(11, 'blog-redirect-to-blog', 'blog-redirect-to-blog', 0),
(18, 'application-admin-comments', 'application-admin-comments', 0),
(19, 'application-admin-edit-account', 'application-admin-edit-account', 0),
(20, 'application-admin-logout', 'application-admin-logout', 0),
(21, 'application-admin-delete-comment', 'application-admin-delete-comment', 0),
(22, 'blog-admin-edit-post', 'blog-admin-edit-post', 0),
(33, 'blog-admin-can-delete-all-posts', 'blog-admin-can-delete-all-posts', 0),
(34, 'blog-admin-delete-post', 'blog-admin-delete-post', 0),
(53, 'application-admin-user-comments', 'application-admin-user-comments', 0),
(54, 'application-admin-comments-replies', 'application-admin-comments-replies', 0),
(55, 'application-admin-can-delete-all-comments', 'application-admin-can-delete-all-comments', 0),
(58, 'blog-admin-upload', 'blog-admin-upload', 0),
(67, 'forum-admin-main-categories', 'forum-admin-main-categories', 0),
(68, 'forum-admin-add-main-category', 'forum-admin-add-main-category', 0),
(69, 'forum-admin-index', 'forum-admin-index', 0),
(70, 'forum-admin-add-category', 'forum-admin-add-category', 0),
(71, 'forum-admin-topics', 'forum-admin-topics', 0),
(72, 'forum-admin-add-topic', 'forum-admin-add-topic', 0),
(73, 'forum-admin-can-manage-all-topics', 'forum-admin-can-manage-all-topics', 0),
(74, 'forum-admin-posts', 'forum-admin-posts', 0),
(75, 'forum-admin-can-manage-all-posts', 'forum-admin-can-manage-all-posts', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) DEFAULT '1',
  `salt` varchar(255) NOT NULL,
  `forgot_password_hash` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_birth` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `user_url` varchar(255) DEFAULT NULL,
  `user_registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `user_group_id` (`user_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `password`, `gender`, `salt`, `forgot_password_hash`, `email`, `date_of_birth`, `status`, `user_url`, `user_registration_date`, `user_group_id`) VALUES
(1, 'ادمین ', 'سایت', 'admin', '88e9b1ff3721d02c8501801cc00d4e96', '1', '3453629', '9987d9fa004ce13888e97347b94a97cf', 'admin@admin.com', NULL, 1, NULL, '2015-05-14 11:18:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `title`) VALUES
(1, 'مدیر'),
(2, 'مترجم'),
(3, 'کاربر عادی');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_permissions`
--

CREATE TABLE IF NOT EXISTS `user_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_group_id` (`user_group_id`,`permission_id`),
  KEY `permission_id` (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=157 ;

--
-- Dumping data for table `user_group_permissions`
--

INSERT INTO `user_group_permissions` (`id`, `user_group_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(13, 1, 5),
(16, 1, 6),
(26, 1, 8),
(28, 1, 9),
(30, 1, 10),
(31, 1, 11),
(47, 1, 18),
(48, 1, 19),
(51, 1, 20),
(54, 1, 21),
(55, 1, 22),
(79, 1, 33),
(80, 1, 34),
(122, 1, 53),
(125, 1, 54),
(128, 1, 55),
(131, 1, 58),
(140, 1, 67),
(141, 1, 68),
(143, 1, 69),
(144, 1, 70),
(145, 1, 71),
(146, 1, 72),
(148, 1, 73),
(149, 1, 74),
(150, 1, 75),
(5, 2, 1),
(6, 2, 2),
(7, 2, 3),
(8, 2, 4),
(14, 2, 5),
(17, 2, 6),
(27, 2, 8),
(29, 2, 9),
(32, 2, 11),
(49, 2, 19),
(52, 2, 20),
(118, 2, 21),
(56, 2, 22),
(81, 2, 34),
(123, 2, 53),
(126, 2, 54),
(151, 2, 71),
(153, 2, 72),
(155, 2, 74),
(9, 3, 1),
(10, 3, 2),
(11, 3, 3),
(12, 3, 4),
(15, 3, 5),
(18, 3, 6),
(50, 3, 19),
(53, 3, 20),
(120, 3, 21),
(82, 3, 34),
(124, 3, 53),
(127, 3, 54),
(152, 3, 71),
(154, 3, 72),
(156, 3, 74);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_post_categories`
--
ALTER TABLE `blog_post_categories`
  ADD CONSTRAINT `blog_post_categories_ibfk_1` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_post_categories_ibfk_2` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blog_post_tags`
--
ALTER TABLE `blog_post_tags`
  ADD CONSTRAINT `blog_post_tags_ibfk_1` FOREIGN KEY (`blog_tag_id`) REFERENCES `blog_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_post_tags_ibfk_2` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD CONSTRAINT `forum_categories_ibfk_1` FOREIGN KEY (`main_category_id`) REFERENCES `forum_main_categories` (`id`);

--
-- Constraints for table `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD CONSTRAINT `forum_posts_ibfk_1` FOREIGN KEY (`forum_topic_id`) REFERENCES `forum_topics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_posts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_posts_thanks`
--
ALTER TABLE `forum_posts_thanks`
  ADD CONSTRAINT `forum_posts_thanks_ibfk_1` FOREIGN KEY (`forum_post_id`) REFERENCES `forum_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_posts_thanks_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_topics`
--
ALTER TABLE `forum_topics`
  ADD CONSTRAINT `forum_topics_ibfk_1` FOREIGN KEY (`forum_category_id`) REFERENCES `forum_categories` (`id`),
  ADD CONSTRAINT `forum_topics_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`);

--
-- Constraints for table `user_group_permissions`
--
ALTER TABLE `user_group_permissions`
  ADD CONSTRAINT `user_group_permissions_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_group_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
