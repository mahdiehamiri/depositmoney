-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2015 at 09:48 AM
-- Server version: 5.5.44-0ubuntu0.14.04.1-log
-- PHP Version: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `base`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_title` (`category_title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `category_title`, `user_id`, `status`) VALUES
(1, 'متفرقه', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_author_user_id` int(11) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_content` longtext NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_status` tinyint(4) NOT NULL DEFAULT '0',
  `post_exerpt` text NOT NULL,
  `post_slug` varchar(255) NOT NULL,
  `post_meta_description` text NOT NULL,
  `post_meta_keywords` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `post_slug` (`post_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `post_author_user_id`, `post_date`, `post_content`, `post_title`, `post_status`, `post_exerpt`, `post_slug`, `post_meta_description`, `post_meta_keywords`) VALUES
(1, 1, '2015-08-02 05:20:26', '', 'اولین مقاله من', 1, '<p>بازیگر ترکیه ای که به ایران دعوت شده،از جمله هنرمندان این کشور است که به موازات فعالیت هنری،در حوزه اجتماعی نیز فعال بوده و در راستای تغذیه سالم برنامه هایی اجرا کرده است. شرکت ایرانی با توجه به تجربیات <strong>موفق ترکیه</strong> در خصوص ارائه غذا و با در نظر گرفتن اشتراکات فرهنگی و مذهبی دو کشور همسایه که از مواد غذایی حلال استفاده می کنند،برنامه ای را در حاشیه افتتاح شعبات خود ترتیب داده که در آن هنرمندان ایرانی و ترکیه ای توامان حضور خواهند یافت.</p>', 'اولین مقاله من', '', ''),
(2, 1, '2015-08-10 06:25:49', '', 'این یک مقاله دیگر است', 1, '<p>بازیگر ترکیه ای که به ایران دعوت شده،از جمله هنرمندان این کشور است که به موازات فعالیت هنری،در حوزه اجتماعی نیز فعال بوده و در راستای تغذیه سالم برنامه هایی اجرا کرده است. شرکت ایرانی با توجه به تجربیات موفق ترکیه در خصوص ارائه غذا و با در نظر گرفتن ...</p>', 'این یک مقاله دیگر است', '', ''),
(3, 1, '2015-08-10 06:26:14', '', 'این مقاله چهارم است', 1, '<p>بازیگر ترکیه ای که به ایران دعوت شده،از جمله هنرمندان این کشور است که به موازات فعالیت هنری،در حوزه اجتماعی نیز فعال بوده و در راستای تغذیه سالم برنامه هایی اجرا کرده است. شرکت ایرانی با توجه به تجربیات موفق ترکیه در خصوص ارائه غذا و با در نظر گرفتن ...</p>', 'این مقاله چهارم است', '', ''),
(4, 1, '2015-08-10 06:26:50', '', 'this is to test', 1, '<p>بازیگر ترکیه ای که به ایران دعوت شده،از جمله هنرمندان این کشور است که به موازات فعالیت هنری،در حوزه اجتماعی نیز فعال بوده و در راستای تغذیه سالم برنامه هایی اجرا کرده است. شرکت ایرانی با توجه به تجربیات موفق ترکیه در خصوص ارائه غذا و با در نظر گرفتن ...</p>', 'این آخرین مقاله من است', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_categories`
--

CREATE TABLE IF NOT EXISTS `blog_post_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_post_id` int(11) NOT NULL,
  `blog_category_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_post_id` (`blog_post_id`),
  KEY `blog_category_id` (`blog_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `blog_post_categories`
--

INSERT INTO `blog_post_categories` (`id`, `blog_post_id`, `blog_category_id`) VALUES
(2, 2, 1),
(3, 3, 1),
(4, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_tags`
--

CREATE TABLE IF NOT EXISTS `blog_post_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_post_id` int(11) NOT NULL,
  `blog_tag_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_post_id` (`blog_post_id`),
  KEY `blog_tag_id` (`blog_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=214 ;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `tag_title`) VALUES
(2, 'برنامه نویسی'),
(3, 'طراحی سایت'),
(4, 'طراحی گرافیک'),
(7, 'پی اچ پی'),
(8, 'فیسبوک'),
(9, 'ویندوز فون 8.1'),
(10, 'ساعت هوشمند'),
(11, 'Smartwatch'),
(13, 'اندروید'),
(14, 'آیفون'),
(15, 'Google Doodle'),
(16, 'خلاقیت'),
(17, 'شبکه های اجتماعی'),
(18, 'گوگل'),
(19, 'لیندا'),
(20, 'کارآفرینی'),
(21, 'استیو جابز'),
(22, 'اپل'),
(23, 'رایانش ابری'),
(24, 'Cloud'),
(25, 'اپلیکیشن'),
(26, 'موبایل'),
(27, 'وایبر'),
(28, 'جاوا'),
(29, 'SDK'),
(30, 'آمازون'),
(31, 'فناوری'),
(32, 'لینکدین'),
(33, 'اکشن'),
(34, 'فتوشاپ'),
(35, 'فضای منفی'),
(36, 'طراحی وب'),
(37, 'طراحی مسطح'),
(38, 'سی اس اس'),
(39, 'اچ تی ام ال'),
(40, 'سی اس'),
(41, 'موفقیت'),
(42, 'ثروت'),
(43, 'جام جهانی'),
(44, 'دور کاری'),
(45, 'xss'),
(46, 'Cross-site Scripting'),
(47, 'امنیت در PHP'),
(48, 'SQL Injection'),
(49, 'PHP'),
(50, 'IDE'),
(51, 'محیط یکپارچه برنامه نویسی'),
(52, 'Framework'),
(53, 'کایزن'),
(54, 'Kaizen'),
(55, 'الکسا'),
(56, 'Alexa'),
(57, 'اکلیپس'),
(58, 'اندروید استودیو'),
(59, 'توسعه اندروید'),
(60, 'برنامه نویسی اندروید'),
(62, 'همایش تجارت الکترونیک'),
(63, 'فریم ورک'),
(64, 'آموزش برنامه نویسی'),
(65, 'برنامه نویسی به کودکان'),
(66, 'ویندوز'),
(67, 'لینوکس'),
(68, 'مهاجرت به بازمتن'),
(69, 'خالق لینوکس'),
(70, 'لینوس تروالدز'),
(71, 'گیک'),
(72, 'نرد'),
(73, 'Geek'),
(74, 'همایش تجارت و اقتصاد الکترونیک'),
(75, 'کارگاه چرا کودکان می بایست برنامه نویسی یاد بگیرند'),
(76, 'مهندسی معکوس'),
(77, 'یادگیری برنامه نویسی'),
(78, 'کدنویسی'),
(79, 'Game Maven'),
(81, 'آموزش رایگان برنامه نویسی'),
(82, 'ویرایشگر کد'),
(83, 'Kate'),
(84, 'استارت آپ'),
(85, 'جسیکا آلبا'),
(86, 'هالیوود'),
(87, 'رویکرد جزء به کل در یادگیری برنامه نویسی'),
(88, 'مزایای برنامه نویسی'),
(89, 'راه اندازی وب سایت'),
(90, 'استارت آپ آنلاین'),
(91, 'مرورگر'),
(92, 'ایران موبی کد'),
(93, 'Koding'),
(94, 'نرم افزار آزاد'),
(95, 'مقاله نویسی'),
(96, 'سرچ'),
(97, 'ابزارهای هوشمند'),
(98, 'گیت هاب'),
(99, 'HTML'),
(100, 'وردپرس'),
(101, 'طراحی'),
(102, 'Material Design'),
(103, 'CSS'),
(104, 'جاوا اسکریپت'),
(105, 'JS'),
(106, 'کروم'),
(107, 'Chrome'),
(108, 'شبکه اجتماعی'),
(109, 'HumHub'),
(110, 'چابک'),
(111, 'agile'),
(112, 'گرادیانت'),
(114, 'Discourse'),
(115, 'تالار گفتگو'),
(116, 'اسکرام'),
(117, 'Waterbear'),
(118, 'باراک اوباما'),
(119, 'تجربه کاربری'),
(120, 'UX'),
(121, 'Linux'),
(122, 'Ubuntu'),
(123, 'متن باز'),
(124, 'UI'),
(125, 'رابط کاربری'),
(126, 'کفشدوزک'),
(128, 'تست برنامه نویسی'),
(129, 'Sublime Text'),
(130, 'سابلایم'),
(131, 'جادی'),
(132, 'کیبورد آزاد'),
(133, 'مصاحبه'),
(135, 'سالار کابلی'),
(136, 'وراثت در برنامه نویسی'),
(137, 'Inheritance'),
(138, 'روبی'),
(139, 'Ruby on Rails'),
(140, 'PDO'),
(141, 'تیراسیس'),
(142, 'پریسا تبریز'),
(143, 'CMS'),
(144, 'Ello.co'),
(145, 'Facebook'),
(146, 'Swift'),
(147, 'زبان های برنامه نویسی'),
(148, 'MySQL'),
(149, 'Google'),
(150, 'دیتابیس'),
(151, 'لاراول'),
(152, 'Laravel'),
(153, 'فریم ورک های پی اچ پی'),
(154, 'App'),
(155, 'API'),
(156, 'استارتاپ'),
(157, 'هک'),
(158, 'Buffer'),
(159, 'بهینه سازی تصویر'),
(160, 'آموزش زبان'),
(161, 'متا تگ'),
(173, 'سایت های اعتیادآور'),
(174, 'سئو'),
(175, 'SEO'),
(176, 'بهینه سازی سایت'),
(177, 'آزمون دیسک'),
(178, 'DISC Test'),
(179, 'Angular JS'),
(180, 'Backbone.js'),
(181, 'Ember.js'),
(183, 'انجین ایکس'),
(184, 'Nginx'),
(185, 'HTML5'),
(186, 'Asm.js'),
(187, 'Startup'),
(188, 'React Native'),
(189, 'React.js'),
(190, 'Google Code'),
(191, 'Git'),
(192, 'Data Science'),
(193, 'com.google'),
(194, 'Slate'),
(195, 'Adobe'),
(196, 'ادوبی'),
(197, 'قانون هشتاد بیست'),
(198, 'اندی روبین'),
(199, 'سکان آکادمی'),
(200, 'ARC'),
(201, 'پسورد'),
(202, 'Webydo'),
(203, 'IoT'),
(204, 'اشیاء اینترنتی'),
(205, 'مایکروسافت'),
(206, 'اینتل'),
(207, 'کامپوزر'),
(208, 'هوش مصنوعی'),
(209, 'Stackoverflow'),
(210, 'توسعه دهنده'),
(211, 'دات نت'),
(212, 'یو ایکس'),
(213, 'Lumen');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_title` varchar(255) NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `province_id` (`province_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=331 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_title`, `province_id`) VALUES
(1, 'آذرشهر', 1),
(3, 'اسکو', 1),
(4, 'اهر', 1),
(5, 'بستان آباد', 1),
(6, 'بناب', 1),
(7, 'تبریز', 1),
(8, 'جلفا', 1),
(9, 'سراب', 1),
(10, 'شبستر', 1),
(11, 'عجب شیر', 1),
(12, 'کلیبر', 1),
(13, 'مراغه', 1),
(14, 'مرند', 1),
(15, 'ملکان', 1),
(16, 'میانه', 1),
(17, 'هریس', 1),
(18, 'هشترود', 1),
(19, 'ورزقان', 1),
(21, 'ارومیه', 2),
(22, 'اشنویه', 2),
(23, 'بوکان', 2),
(24, 'پیرانشهر', 2),
(25, 'تکاب', 2),
(26, 'چالدران', 2),
(27, 'خوی', 2),
(28, 'سردشت', 2),
(29, 'سلماس', 2),
(30, 'شاهین دژ', 2),
(31, 'ماکو', 2),
(32, 'مهاباد', 2),
(33, 'میاندوآب', 2),
(34, 'نقده', 2),
(35, 'اردبیل', 3),
(36, 'بیله سوار', 3),
(37, 'پارس آباد', 3),
(38, 'خلخال', 3),
(39, 'کوثر', 3),
(40, 'گرمی', 3),
(41, 'مشکین شهر', 3),
(42, 'نمین', 3),
(43, 'نیر', 3),
(44, 'آران و بیدگل', 4),
(45, 'اردستان', 4),
(46, 'اصفهان', 4),
(47, 'برخوار و میمه', 4),
(48, 'بوئین و میاندشت', 4),
(49, 'تیران و کرون', 4),
(50, 'چادگان', 4),
(51, 'خمینی شهر', 4),
(52, 'خوانسار', 4),
(53, 'سمیرم', 4),
(54, 'شهررضا', 4),
(55, 'فریدن', 4),
(56, 'فریدون شهر', 4),
(57, 'فلاورجان', 4),
(58, 'کاشان', 4),
(59, 'گلپایگان', 4),
(60, 'لنجان', 4),
(61, 'مبارکه', 4),
(62, 'نائین', 4),
(63, 'نجف آباد', 4),
(64, 'نطنز', 4),
(65, 'ساوجبلاغ', 5),
(66, 'طالقان', 5),
(67, 'کرج', 5),
(68, 'نظرآباد', 5),
(69, 'آبدانان', 6),
(70, 'ایلام', 6),
(71, 'ایوان', 6),
(72, 'دره شهر', 6),
(73, 'دهلران', 6),
(74, 'شیروان و چرداول', 6),
(75, 'مهران', 6),
(76, 'بوشهر', 7),
(77, 'تنگستان', 7),
(78, 'جم', 7),
(79, 'دشتستان', 7),
(80, 'دشتی', 7),
(81, 'دیر', 7),
(82, 'دیلم', 7),
(83, 'کنگان', 7),
(84, 'گناوه', 7),
(85, 'اسلام شهر', 8),
(86, 'پاکدشت', 8),
(87, 'تهران', 8),
(88, 'دماوند', 8),
(89, 'رباط کریم', 8),
(90, 'ری', 8),
(91, 'ساوجبلاغ', 8),
(92, 'شمیرانات', 8),
(93, 'شهریار', 8),
(94, 'فیروزکوه', 8),
(95, 'ورامین', 8),
(96, 'اردل', 9),
(97, 'بروجن', 9),
(98, 'شهرکرد', 9),
(99, 'فارسان', 9),
(100, 'کوهرنگ', 9),
(101, 'لردگان', 9),
(102, 'بیرجند', 10),
(103, 'سربیشه', 10),
(104, 'طبس', 10),
(105, 'قائنات', 10),
(106, 'نهبندان', 10),
(107, 'بجستان', 11),
(108, 'بردسکن', 11),
(109, 'تایباد', 11),
(110, 'تربت جام', 11),
(111, 'تربت حیدریه', 11),
(112, 'چناران', 11),
(113, 'خلیل آباد', 11),
(114, 'خواف', 11),
(115, 'درگز', 11),
(116, 'رشتخوار', 11),
(117, 'سبزوار', 11),
(118, 'سرخس', 11),
(119, 'فردوس', 11),
(120, 'فریمان', 11),
(121, 'قوچان', 11),
(122, 'کاشمر', 11),
(123, 'کلات', 11),
(124, 'گناباد', 11),
(125, 'مشهد', 11),
(126, 'نبشابور', 11),
(127, 'اسفراین', 12),
(128, 'بجنورد', 12),
(129, 'جاجرم', 12),
(130, 'شیروان', 12),
(131, 'فاروج', 12),
(132, 'مانه و سملقان', 12),
(133, 'آبادان', 13),
(134, 'امیدیه', 13),
(135, 'اندیمشک', 13),
(136, 'اهواز', 13),
(137, 'ایذه', 13),
(138, 'باغ ملک', 13),
(139, 'بندر ماهشهر', 13),
(140, 'بهبهان', 13),
(141, 'خرمشهر', 13),
(142, 'دزفول', 13),
(143, 'دشت آزادگان', 13),
(144, 'رامهرمز', 13),
(145, 'شادگان', 13),
(146, 'شوش', 13),
(147, 'شوشتر', 13),
(148, 'لالی', 13),
(149, 'مسجد سلیمان', 13),
(150, 'هندیجان', 13),
(151, 'ابهر', 14),
(152, 'ایجرود', 14),
(153, 'خدابنده', 14),
(154, 'خرمدره', 14),
(155, 'زنجان', 14),
(156, 'طارم', 14),
(157, 'ماه نشان', 14),
(158, 'دامغان', 15),
(159, 'سمنان', 15),
(160, 'شاهرود', 15),
(161, 'گرمسار', 15),
(162, 'ابرانشهر', 16),
(163, 'چابهار', 16),
(164, 'خاش', 16),
(165, 'زابل', 16),
(166, 'زاهدان', 16),
(167, 'سراوان', 16),
(168, 'سرباز', 16),
(169, 'نیک شهر', 16),
(170, 'آباده', 17),
(171, 'ارسنجان', 17),
(172, 'استهبان', 17),
(173, 'اقلید', 17),
(174, 'بوانات', 17),
(175, 'پاسارگاد', 17),
(176, 'جهرم', 17),
(177, 'خرم بید', 17),
(178, 'خنج', 17),
(179, 'داراب', 17),
(180, 'زرین دشت', 17),
(181, 'سپیدان', 17),
(182, 'شیراز', 17),
(183, 'فراشبند', 17),
(184, 'فسا', 17),
(185, 'فیروز آباد', 17),
(186, 'قیر و کارزین', 17),
(187, 'کازرون', 17),
(188, 'لارستان', 17),
(189, 'لامرد', 17),
(190, 'مرودشت', 17),
(191, 'ممسنی', 17),
(192, 'مهر', 17),
(193, 'نی ریز', 17),
(194, 'آبیک', 18),
(195, 'البرز', 18),
(196, 'بوئین زهرا', 18),
(197, 'تاکستان', 18),
(198, 'قزوین', 18),
(199, 'سلفچگان', 19),
(200, 'قم', 19),
(201, 'بانه', 20),
(202, 'بیجار', 20),
(203, 'دیواندره', 20),
(204, 'سرو آباد', 20),
(205, 'سقز', 20),
(206, 'سنندج', 20),
(207, 'قروه', 20),
(208, 'کامیاران', 20),
(209, 'مریوان', 20),
(210, 'بافت', 21),
(211, 'بردسیر', 21),
(212, 'بم', 21),
(213, 'جیرفت', 21),
(214, 'راور', 21),
(215, 'رفسنجان', 21),
(216, 'زرند', 21),
(217, 'شهر بابک', 21),
(218, 'عنبرآباد', 21),
(219, 'کرمان', 21),
(220, 'کهنوج', 21),
(221, 'کوهبنان', 21),
(222, 'منوجان', 21),
(223, 'اسلام آباد غرب', 22),
(224, 'پاوه', 22),
(225, 'ثلاث باباجانی', 22),
(226, 'جوانرود', 22),
(227, 'روانسر', 22),
(228, 'سرپل ذهاب', 22),
(229, 'سنقر', 22),
(230, 'صحنه', 22),
(231, 'قصر شیرین', 22),
(232, 'کرمانشاه', 22),
(233, 'کنگاور', 22),
(234, 'گیلان غرب', 22),
(235, 'هرسین', 22),
(236, 'بهمئی', 23),
(237, 'بویراحمد', 23),
(238, 'دنا', 23),
(239, 'کهکیلویه', 23),
(240, 'گچساران', 23),
(241, 'یاسوج', 23),
(242, 'آزاد شهر', 24),
(243, 'آق قلا', 24),
(244, 'بندر ترکمن', 24),
(245, 'بندر گز', 24),
(246, 'رامیان', 24),
(247, 'علی آباد', 24),
(248, 'کردکوی', 24),
(249, 'کلاله', 24),
(250, 'گرگان', 24),
(251, 'گنبد کاووس', 24),
(252, 'مینودشت', 24),
(253, 'آستارا', 25),
(254, 'آستانه اشرفیه', 25),
(255, 'املش', 25),
(256, 'بندر انزلی', 25),
(257, 'تالش', 25),
(258, 'رشت', 25),
(259, 'رضوان شهر', 25),
(260, 'رودبار', 25),
(261, 'رودسر', 25),
(262, 'سیاهکل', 25),
(263, 'شفت', 25),
(264, 'صومعه سرا', 25),
(265, 'فومن', 25),
(266, 'لاهیجان', 25),
(267, 'لنگرود', 25),
(268, 'ماسال', 25),
(269, 'ازنا', 26),
(270, 'الیگودرز', 26),
(271, 'بروجرد', 26),
(272, 'پلدختر', 26),
(273, 'خرم آباد', 26),
(274, 'دلفان', 26),
(275, 'دورود', 26),
(276, 'سلسله', 26),
(277, 'کوهدشت', 26),
(278, 'آمل', 27),
(279, 'بابل', 27),
(280, 'بابلسر', 27),
(281, 'بهشهر', 27),
(282, 'تنکابن', 27),
(283, 'جویبار', 27),
(284, 'چالوس', 27),
(285, 'رامسر', 27),
(286, 'ساری', 27),
(287, 'سوادکوه', 27),
(288, 'قائم شهر', 27),
(289, 'محمود آباد', 27),
(290, 'نکا', 27),
(291, 'نور', 27),
(292, 'نوشهر', 27),
(293, 'اراک', 28),
(294, 'آشتیان', 28),
(295, 'تفرش', 28),
(296, 'خمین', 28),
(297, 'دلیجان', 28),
(298, 'زرندیه', 28),
(299, 'ساوه', 28),
(300, 'شازند', 28),
(301, 'کمیجان', 28),
(302, 'محلات', 28),
(303, 'ابوموسی', 29),
(304, 'بستک', 29),
(305, 'بندرعباس', 29),
(306, 'بندر لنگه', 29),
(307, 'جاسک', 29),
(308, 'جزیره کیش', 29),
(309, 'حاجی آباد', 29),
(310, 'رودان', 29),
(311, 'قشم', 29),
(312, 'گاوبندی', 29),
(313, 'میناب', 29),
(314, 'اسد آباد', 30),
(315, 'بهار', 30),
(316, 'تویسرکان', 30),
(317, 'رزن', 30),
(318, 'کبودر آهنگ', 30),
(319, 'ملایر', 30),
(320, 'نهاوند', 30),
(321, 'همدان', 30),
(322, 'ابرکوه', 31),
(323, 'اردکان', 31),
(324, 'اسکذر', 31),
(325, 'بافق', 31),
(326, 'تفت', 31),
(327, 'خاتم', 31),
(328, 'مهریز', 31),
(329, 'میبد', 31),
(330, 'یزد', 31);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `entity_type` varchar(255) NOT NULL DEFAULT 'post',
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `entity_id`, `entity_type`, `user_id`, `comment`, `status`, `date`) VALUES
(2, 0, 4, 'post', 1, 'fsdfdsfsfsdfs', 1, '2015-08-12 10:41:14');

-- --------------------------------------------------------

--
-- Table structure for table `forum_categories`
--

CREATE TABLE IF NOT EXISTS `forum_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `main_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_category_id` (`main_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `name`, `main_category_id`) VALUES
(1, 'زبان جاوا', 2),
(3, 'زبان پی اچ پی', 2);

-- --------------------------------------------------------

--
-- Table structure for table `forum_main_categories`
--

CREATE TABLE IF NOT EXISTS `forum_main_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `forum_main_categories`
--

INSERT INTO `forum_main_categories` (`id`, `name`) VALUES
(2, 'برنامه نویسی'),
(3, 'شبکه'),
(4, 'طراحی سایت');

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts`
--

CREATE TABLE IF NOT EXISTS `forum_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_topic_id` int(11) NOT NULL,
  `post_content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `parent_id` (`parent_id`),
  KEY `forum_topic_id` (`forum_topic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `forum_posts`
--

INSERT INTO `forum_posts` (`id`, `forum_topic_id`, `post_content`, `user_id`, `parent_id`, `create_time`) VALUES
(14, 7, 'fds', 1, 0, '2015-10-06 09:54:09'),
(16, 7, 'fdsffds', 1, 0, '2015-10-06 10:04:58'),
(17, 7, 'fdsfs', 1, 0, '2015-10-06 10:05:02'),
(18, 7, 'fdss', 1, 0, '2015-10-06 10:06:31'),
(19, 7, 'fsdfsdf', 1, 0, '2015-10-06 10:06:36'),
(20, 7, 'fdsfsdfsdsdfdfsfsdfsddfs', 1, 0, '2015-10-06 10:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts_thanks`
--

CREATE TABLE IF NOT EXISTS `forum_posts_thanks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `forum_post_id` (`forum_post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forum_topics`
--

CREATE TABLE IF NOT EXISTS `forum_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_category_id` int(11) NOT NULL,
  `topic_name` varchar(255) NOT NULL,
  `topic_content` text,
  `user_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `topic_name` (`topic_name`),
  KEY `forum_category_id` (`forum_category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `forum_topics`
--

INSERT INTO `forum_topics` (`id`, `forum_category_id`, `topic_name`, `topic_content`, `user_id`, `create_time`) VALUES
(7, 1, 'آیا جاوا یک زبان کامپالی است یا تفسیری', 'سلام. لطفا در مورد عنوان این موضوع توضیح دهید!', 1, '2015-10-06 09:15:52');

-- --------------------------------------------------------

--
-- Table structure for table `forum_topics_thanks`
--

CREATE TABLE IF NOT EXISTS `forum_topics_thanks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `level` int(10) unsigned NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `level`) VALUES
(1, 'Admin', 'Admin Group with Full Access', 1),
(2, 'User', 'User with Limited Permissions', 1);

-- --------------------------------------------------------

--
-- Table structure for table `group_permissions`
--

CREATE TABLE IF NOT EXISTS `group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT 'true',
  PRIMARY KEY (`id`),
  UNIQUE KEY `resource_id` (`resource_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `group_permissions`
--

INSERT INTO `group_permissions` (`id`, `group_id`, `resource_id`, `value`) VALUES
(5, 1, 2, 'true'),
(14, 1, 10, 'true'),
(19, 1, 14, 'true'),
(33, 1, 9, 'true'),
(34, 1, 27, 'true'),
(35, 2, 9, 'true'),
(39, 1, 28, 'true'),
(40, 1, 29, 'true'),
(41, 2, 29, 'true'),
(42, 1, 30, 'true'),
(43, 2, 30, 'true'),
(44, 1, 35, 'true'),
(45, 1, 36, 'true'),
(46, 1, 37, 'true'),
(47, 1, 39, 'true'),
(48, 1, 40, 'true'),
(49, 1, 41, 'true'),
(50, 1, 42, 'true'),
(51, 1, 44, 'true'),
(52, 1, 45, 'true'),
(53, 1, 46, 'true'),
(54, 1, 47, 'true'),
(55, 1, 48, 'true'),
(56, 1, 49, 'true'),
(57, 1, 50, 'true'),
(58, 1, 51, 'true'),
(59, 1, 52, 'true'),
(60, 1, 53, 'true'),
(61, 1, 54, 'true'),
(62, 1, 55, 'true'),
(63, 1, 56, 'true'),
(64, 1, 57, 'true'),
(65, 1, 58, 'true'),
(66, 1, 60, 'true'),
(67, 1, 61, 'true'),
(68, 1, 62, 'true'),
(69, 1, 63, 'true'),
(70, 1, 64, 'true'),
(71, 1, 65, 'true'),
(72, 1, 66, 'true'),
(73, 1, 68, 'true'),
(74, 1, 69, 'true'),
(75, 1, 70, 'true'),
(76, 1, 71, 'true'),
(77, 1, 72, 'true'),
(78, 1, 73, 'true'),
(79, 1, 74, 'true'),
(80, 1, 75, 'true'),
(81, 1, 76, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `title`, `url`) VALUES
(15, 'Facebook', 'http://www.sokanacademy.ir');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `province_title`) VALUES
(1, 'آذربایجان شرقی'),
(2, 'آذربایجان غربی'),
(3, 'اردبیل'),
(4, 'اصفهان'),
(5, 'البرز'),
(6, 'ایلام'),
(7, 'بوشهر'),
(8, 'تهران'),
(9, 'چهارمحال و بختیاری'),
(10, 'خراسان جنوبی'),
(11, 'خراسان رضوی'),
(12, 'خراسان شمالی'),
(13, 'خوزستان'),
(14, 'زنجان'),
(15, 'سمنان'),
(16, 'سیستان و بلوچستان'),
(17, 'فارس'),
(18, 'قزوین'),
(19, 'قم'),
(20, 'کردستان'),
(21, 'کرمان'),
(22, 'کرمانشاه'),
(23, 'کهکیلویه و بویراحمد'),
(24, 'گلستان'),
(25, 'گیلان'),
(26, 'لرستان'),
(27, 'مازندران'),
(28, 'مرکزی'),
(29, 'هرمزگان'),
(30, 'همدان'),
(31, 'یزد');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE IF NOT EXISTS `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `extra_group` varchar(50) NOT NULL DEFAULT 'general',
  PRIMARY KEY (`id`),
  UNIQUE KEY `resource` (`resource`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `resource`, `type`, `label`, `level`, `extra_group`) VALUES
(2, 'blog-index-*', 'default', 'blog-index-*', 1, 'general'),
(3, 'pages-index-*', 'default', 'pages-index-*', 1, 'general'),
(4, 'skeleton-index-*', 'default', 'skeleton-index-*', 1, 'general'),
(9, 'skeleton-admin-index', 'route', 'skeleton-admin-index', 1, 'general'),
(10, 'pages-admin-list', 'route', 'pages-admin-list', 1, 'general'),
(14, 'socialnetworks-admin-list', 'route', 'socialnetworks-admin-list', 1, 'general'),
(27, 'slideshow-admin-delete', 'route', 'slideshow-admin-delete', 1, 'general'),
(28, 'links-admin-list', 'route', 'links-admin-*', 1, 'general'),
(29, 'skeleton-admin-logout', 'route', 'skeleton-admin-logout', 1, 'general'),
(30, 'skeleton-admin-edit-account', 'route', 'skeleton-admin-edit-account', 1, 'general'),
(31, 'skeleton-index-signin', 'default', 'skeleton-index-signin', 1, 'general'),
(32, 'skeleton-index-forgot-password', 'default', 'skeleton-index-forgot-password', 1, 'general'),
(33, 'skeleton-index-reset-password', 'default', 'skeleton-index-reset-password', 1, 'general'),
(34, 'skeleton-index-signup', 'default', 'skeleton-index-signup', 1, 'general'),
(35, 'pages-admin-add', 'route', 'pages-admin-add', 1, 'general'),
(36, 'pages-admin-edit', 'route', 'pages-admin-edit', 1, 'general'),
(37, 'pages-admin-delete', 'route', 'pages-admin-delete', 1, 'general'),
(39, 'links-admin-add', 'route', 'links-admin-add', 1, 'general'),
(40, 'links-admin-edit', 'route', 'links-admin-edit', 1, 'general'),
(41, 'links-admin-delete', 'route', 'links-admin-delete', 1, 'general'),
(42, 'socialnetworks-admin-add', 'route', 'socialnetworks-admin-add', 1, 'general'),
(43, 'socialnetworks-admin-edit', 'route', 'socialnetworks-admin-edit', 1, 'general'),
(44, 'socialnetworks-admin-delete', 'route', 'socialnetworks-admin-delete', 1, 'general'),
(45, 'blog-admin-post-list', 'route', 'blog-admin-post-list', 1, 'general'),
(46, 'blog-admin-add-category', 'route', 'blog-admin-add-category', 1, 'general'),
(47, 'blog-admin-category-list', 'route', 'blog-admin-category-list', 1, 'general'),
(48, 'blog-admin-add-post', 'route', 'blog-admin-add-post', 1, 'general'),
(49, 'blog-admin-edit-post', 'route', 'blog-admin-edit-post', 1, 'general'),
(50, 'blog-admin-delete-post', 'route', 'blog-admin-delete-post', 1, 'general'),
(51, 'blog-admin-upload', 'route', 'blog-admin-upload', 1, 'general'),
(52, 'blog-index-index', 'route', 'blog-index-index', 1, 'general'),
(53, 'comment-admin-comments', 'route', 'comment-admin-comments', 1, 'general'),
(54, 'slideshow-admin-slideshow-list', 'route', 'slideshow-admin-slideshow-list', 1, 'general'),
(55, 'slideshow-admin-add', 'route', 'slideshow-admin-add', 1, 'general'),
(56, 'slideshow-admin-edit', 'route', 'slideshow-admin-edit', 1, 'general'),
(57, 'comment-admin-delete-comment', 'route', 'comment-admin-delete-comment', 1, 'general'),
(58, 'blog-admin-delete-category', 'route', 'blog-admin-delete-category', 1, 'general'),
(59, 'blog-admin-edit-category', 'route', 'blog-admin-edit-category', 1, 'general'),
(60, 'slideshow-admin-delete-image', 'route', 'slideshow-admin-delete-image', 1, 'general'),
(61, 'forum-admin-main-categories', 'route', 'forum-admin-main-categories', 1, 'general'),
(62, 'forum-admin-edit-main-category', 'route', 'forum-admin-edit-main-category', 1, 'general'),
(63, 'forum-admin-add-category', 'route', 'forum-admin-add-category', 1, 'general'),
(64, 'forum-admin-edit-category', 'route', 'forum-admin-edit-category', 1, 'general'),
(65, 'forum-admin-delete-category', 'route', 'forum-admin-delete-category', 1, 'general'),
(66, 'forum-admin-add-main-category', 'route', 'forum-admin-add-main-category', 1, 'general'),
(68, 'forum-admin-delete-main-category', 'route', 'forum-admin-delete-main-category', 1, 'general'),
(69, 'forum-admin-topics', 'route', 'forum-admin-topics', 1, 'general'),
(70, 'forum-admin-add-topic', 'route', 'forum-admin-add-topic', 1, 'general'),
(71, 'forum-admin-edit-topic', 'route', 'forum-admin-edit-topic', 1, 'general'),
(72, 'forum-admin-delete-topic', 'route', 'forum-admin-delete-topic', 1, 'general'),
(73, 'forum-admin-posts', 'route', 'forum-admin-posts', 1, 'general'),
(74, 'forum-admin-delete-post', 'route', 'forum-admin-delete-post', 1, 'general'),
(75, 'forum-admin-index', 'route', 'forum-admin-index', 1, 'general'),
(76, 'forum-admin-can-manage-all-topics', 'route', 'forum-admin-can-manage-all-topics', 1, 'general'),
(78, 'forum-index-*', 'default', 'forum-index-*', 1, 'general'),
(79, 'forum-admin-can-manage-all-posts', 'route', 'forum-admin-can-manage-all-posts', 1, 'general');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE IF NOT EXISTS `slideshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id`, `name`) VALUES
(10, 'nbvnbvnfdhfgdhgd');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow_images`
--

CREATE TABLE IF NOT EXISTS `slideshow_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slideshow_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slideshow_id` (`slideshow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `social_networks`
--

CREATE TABLE IF NOT EXISTS `social_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `social_networks`
--

INSERT INTO `social_networks` (`id`, `title`, `url`) VALUES
(5, 'Facebofok', 'http://www.sokanacademy.ir '),
(7, 'Instagram', 'http://www.sokanacademy.ir'),
(8, 'twitter', 'http://www.sokanacademy.ir');

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE IF NOT EXISTS `static_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`id`, `title`, `body`, `meta_description`, `meta_keywords`, `status`) VALUES
(19, 'درباره', '\r\n\r\nبا گسترش غیر قابل انکار و روز افزون استفاده از اینترنت و خدمات آنلاین در عرصه های مختلف زندگی امروزی، همواره این نیاز احساس می گردد که خدمات اینترنتی با کیفیت بهتر و به سهولت در اختیار کاربران قرار گیرد.\r\n\r\nشرکت شبکه ارتباط شبدیز با هدف تحول در خدمت رسانی اینترنتی در سال 1382 تاسیس گردید و با به کارگیری متخصصان، کارشناسان و مهندسان حرفه ای، متخصص و خبره از یک سو وهمچنین به کارگیری تجهیزات مدرن و تکنولوژی های روز دنیا از سوی دیگر افتخار سرویس دهی به بیش از 60000 کاربر را داشته است و در این راه مفتخر به دریافت مجوز ها و گواهی نامه هایی شده است که در ذیل به برخی از آن ها اشاره شده است:\r\n', 'fdssdfsd', 'fsdffsdfsdfsdfs', 1),
(20, 'تاریخچه', '\r\n\r\nصدا روی پروتکل اینترنت (به انگلیسی: VoIP) مخفف کلمات Voice Over Internet Protocol است .VoIP به یک گروه از تکنولوژی می‌گویند که برای انتقال صدا و چندرسانه از شبکه‌های مبتنی بر پروتکل اینترنت، مانند اینترنت استفاده می‌کند. VoIP با عنوان‌های تلفن IP، تلفن اینترنتی، تلفن پهن‌باند، صدای پهن‌باند و صدا روی پهن‌باند نیز شناخته‌می‌شود.\r\n\r\nمعمولاً از شرکت‌های ارایه‌کنندهٔ سرویس‌های VoIP به‌عنوان تهیه‌کنندگان، و از پروتکل‌های استفاده شده برای حمل علایم صدا روی شبکهٔ IP، به‌عنوان صدا روی IP یا پروتکل‌های VoIP یاد می‌شود. آن‌ها را می‌توان در اصل توسعهٔ تجاری تجربهٔ پروتکل صدای شبکه (۱۹۷۳) که توسط تهیه‌کنندگان نسل قدیمی اینترنت (ARPANET) اختراع شده‌بود، در نظرگرفت.\r\n\r\nبرخی از کاهش هزینه‌ها تا حدودی به دلیل استفاده از یک شبکهٔ منفرد برای حمل صدا و اطلاعات است، مخصوصاً در جایی که استفاده‌کنندگان به شبکه‌ای دسترسی دارند که از ظرفیت آن کمتر استفاده شده و می‌توانند از VoIP بدون هیچ هزینهٔ اضافی استفاده‌کنند. تماس‌های VoIP به VoIP برخی اوقات مجانی هستند، در حالی که تماس VoIP به شبکه‌های تلفن عمومی(PSTN) ممکن است هزینه‌ای در بر داشته‌باشد که باید توسط استفاده‌کنندگان VoIP پرداخت‌شود. پروتکل‌های صدا روی IP، سیگنال‌های تلفنی را به‌عنوان دادهٔ دیجیتال (که معمولاً با فنون فشرده‌سازی کاهش حجم یافته‌است) حمل می‌کنند.\r\n\r\nدو نوع شبکهٔ تلفن بر روی سرویس‌های VoIP وجود دارد: شماره‌گیری داخلی مستقیم (DID) و شماره‌های دسترسی. شماره‌گیری داخلی مستقیم(DID)، تماس‌گیرنده را به‌صورت مستقیم متصل می‌کند در صورتی که لازمهٔ شماره‌های دسترسی، گرفتن شمارهٔ داخلی توسط تماس‌گیرنده‌است.\r\n\r\nبا گسترش دنیای IT و تکنولوژی Voice Over IP در ایران ، بسیاری از ارائه کنندگان سرویس های اینترنتی به تامین مکالمات و ارتباطات بین‌المللی در بستر اینترنت پرداختند.\r\n\r\nدر این راستا شبکه ارتباط شبدیز نیز اقدام به عرضه سرویس تلفن اینترنتی یا همان voip نموده است و همچون دیگر خدمات خود در ارایه‌ی این خدمت نیز در بازار پیشرو بوده و توانسته با سرمایه‌‌گذاری قابل توجه و استفاده از جدیدترین تجهیزات فناوری اطلاعات و ارتباطات روز دنیا و بهره مندی از برگزیده‌ترین متخصصان ، بهترین کیفیت را برای ارایه‌ی این خدمت فراهم کند.', 'این تاریخچه ماست', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_resources`
--

CREATE TABLE IF NOT EXISTS `sub_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_resource` varchar(255) NOT NULL,
  `resource_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sub_resources`
--

INSERT INTO `sub_resources` (`id`, `sub_resource`, `resource_id`) VALUES
(3, 'zf2base-sub1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) DEFAULT '1',
  `salt` varchar(255) NOT NULL,
  `forgot_password_hash` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_birth` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `user_registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `password`, `gender`, `salt`, `forgot_password_hash`, `email`, `date_of_birth`, `status`, `user_registration_date`) VALUES
(1, 'ادمین ', 'سایت ', '8efed4b8aeff5792dc45f3d91d898c61', '1', '5714622', '5b77f095733fac2d2896593a1b22923b', 'admin@gmail.com', NULL, 1, '2015-05-14 11:18:39'),
(11, 'fsdf', 'fsdfs', '73dba8b13766c44ba81d3d5d9d56f98b', '1', '7436708', '', 'info@test.com', NULL, 1, '2015-08-19 05:30:22'),
(14, NULL, NULL, '7ebf2d0c8bed0736b87dbb8c651bfd82', '1', '4859827', '', 'sokanacademy456@gmail.com', NULL, 1, '2015-08-19 06:05:42'),
(15, NULL, NULL, 'cb94599558e371defd43b40f1f22a052', '1', '1877951', '', 'sokanacademyzaq@gmail.com', NULL, 1, '2015-08-19 06:17:19'),
(16, NULL, NULL, '57100ad50db19a7c48af97e1a36646af', '1', '4227344', '', 'sokanacademy123@gmail.com', NULL, 1, '2015-09-06 05:39:05'),
(17, NULL, NULL, 'b6f8703946a40f0398fcb0f326d7a32c', '1', '6782572', '', 'sokanacademy1234@gmail.com', NULL, 1, '2015-09-06 05:40:21'),
(18, 'کاربر تست', 'تست', '8750c0cf73b989619c4a6a61cb723de6', '1', '3766482', '', 'sokanacademy12345@gmail.com', NULL, 1, '2015-09-06 05:41:16');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(3, 11, 2),
(6, 14, 2),
(7, 15, 2),
(8, 18, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE IF NOT EXISTS `user_permissions` (
  `resource_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  UNIQUE KEY `resource_id` (`resource_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`resource_id`, `user_id`, `value`) VALUES
(1, 1, 'deny');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_post_categories`
--
ALTER TABLE `blog_post_categories`
  ADD CONSTRAINT `blog_post_categories_ibfk_2` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_post_categories_ibfk_3` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blog_post_tags`
--
ALTER TABLE `blog_post_tags`
  ADD CONSTRAINT `blog_post_tags_ibfk_1` FOREIGN KEY (`blog_tag_id`) REFERENCES `blog_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_post_tags_ibfk_2` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD CONSTRAINT `forum_categories_ibfk_1` FOREIGN KEY (`main_category_id`) REFERENCES `forum_main_categories` (`id`);

--
-- Constraints for table `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD CONSTRAINT `forum_posts_ibfk_1` FOREIGN KEY (`forum_topic_id`) REFERENCES `forum_topics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_posts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_posts_thanks`
--
ALTER TABLE `forum_posts_thanks`
  ADD CONSTRAINT `forum_posts_thanks_ibfk_1` FOREIGN KEY (`forum_post_id`) REFERENCES `forum_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_posts_thanks_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_topics`
--
ALTER TABLE `forum_topics`
  ADD CONSTRAINT `forum_topics_ibfk_1` FOREIGN KEY (`forum_category_id`) REFERENCES `forum_categories` (`id`),
  ADD CONSTRAINT `forum_topics_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `group_permissions`
--
ALTER TABLE `group_permissions`
  ADD CONSTRAINT `group_permissions_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group_permissions_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slideshow_images`
--
ALTER TABLE `slideshow_images`
  ADD CONSTRAINT `slideshow_images_ibfk_1` FOREIGN KEY (`slideshow_id`) REFERENCES `slideshow` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `user_group_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
