-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2016 at 01:34 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) NOT NULL,
  `category_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `category_title`, `category_status`) VALUES
(1, 'متفرقه1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_author_user_id` int(11) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_content` text NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_status` tinyint(4) NOT NULL DEFAULT '0',
  `post_exerpt` text NOT NULL,
  `post_slug` varchar(255) NOT NULL,
  `post_meta_description` text NOT NULL,
  `post_meta_keywords` text NOT NULL,
  `post_source_url` varchar(255) NOT NULL,
  `post_source_title` varchar(255) NOT NULL,
  `post_category_id` int(10) unsigned DEFAULT NULL,
  `post_lang` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `post_slug` (`post_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `post_author_user_id`, `post_date`, `post_content`, `post_title`, `post_status`, `post_exerpt`, `post_slug`, `post_meta_description`, `post_meta_keywords`, `post_source_url`, `post_source_title`, `post_category_id`, `post_lang`) VALUES
(4, 1, '2015-12-22 13:04:59', '<p>fdasfdsa</p>', 'fdsafd', 1, '<p>fdas</p>', 'fdsafd', '', '', '', '', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_categories`
--

CREATE TABLE IF NOT EXISTS `blog_post_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_post_id` int(11) NOT NULL,
  `blog_category_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_post_id` (`blog_post_id`),
  KEY `blog_category_id` (`blog_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=423 ;

--
-- Dumping data for table `blog_post_categories`
--

INSERT INTO `blog_post_categories` (`id`, `blog_post_id`, `blog_category_id`) VALUES
(419, 1888, 1),
(420, 1889, 1),
(421, 1890, 1),
(422, 1891, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_tags`
--

CREATE TABLE IF NOT EXISTS `blog_post_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_post_id` int(11) NOT NULL,
  `blog_tag_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_post_id` (`blog_post_id`),
  KEY `blog_tag_id` (`blog_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=269 ;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `tag_title`) VALUES
(253, 'ali'),
(254, 'iman'),
(255, 'مایکروسافت'),
(256, 'سرفیس بوک'),
(257, 'تویوتا'),
(258, 'بازی های آنلاین'),
(259, 'ایکس باکس'),
(260, 'XBox'),
(261, 'fdsfsdf'),
(262, 'fsdfsdfds'),
(263, 'jsj'),
(264, 'تست'),
(265, 'fdsfsdfsd'),
(266, 'fdsfds'),
(267, 'fdsfsd'),
(268, 'fdsa');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `entity_type` varchar(255) NOT NULL DEFAULT 'post',
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=114 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `entity_id`, `entity_type`, `user_id`, `comment`, `status`, `date`) VALUES
(92, 0, 0, 'birthday', 15, 'تست  1 کاربر عادی', 1, '2015-08-29 07:23:58'),
(98, 92, 0, 'birthday', 1, 'پاسخ ادمین به کامنت کاربر تست', 1, '2015-08-29 07:38:01'),
(99, 0, 0, 'birthday', 1, 'تست ادمین سایت', 1, '2015-08-29 07:45:38'),
(100, 0, 0, 'birthday', 15, 'تست 2 کاربر عادی', 1, '2015-08-29 07:51:01'),
(101, 92, 0, 'birthday', 14, 'پاسخ مترجم به کاربر عادی', 1, '2015-08-29 08:00:38'),
(102, 99, 0, 'birthday', 15, 'پاسخ کاربر عادی به ادمین سایت', 1, '2015-08-29 08:55:03'),
(103, 98, 0, 'birthday', 14, 'پاسخ مترجم به کامنت ادمین که برای کاربر عادی بود', 1, '2015-08-29 09:04:12'),
(104, 0, 0, 'birthday', 1, 'پیام تبریک تولد ادمین سایت', 1, '2015-08-29 10:01:55'),
(105, 0, 1865, 'post', 1, 'کامنت ادمین سایت برای وبلاگ', 1, '2015-08-29 10:02:16'),
(106, 0, 23, 'tutorial', 1, 'پیام ادمین سایت برای آموزش HTML', 1, '2015-08-29 10:02:44'),
(107, 0, 21, 'project', 1, 'project url comment', 1, '2015-09-05 14:00:07'),
(108, 0, 21, 'project', 1, 'jsjsfdsd', 1, '2015-09-05 14:09:50'),
(109, 104, 0, 'birthday', 20, 'این سکان آکادمی یک دو سه است', 1, '2015-09-05 14:57:39'),
(110, 100, 0, 'birthday', 20, 'fs', 1, '2015-09-05 15:16:47'),
(111, 110, 0, 'birthday', 20, 'fsd', 1, '2015-09-05 15:16:57'),
(112, 0, 1888, 'post', 1, 'fsdf', 1, '2015-10-07 10:05:40'),
(113, 0, 1894, 'post', 1, 'fdsfs', 1, '2015-10-31 08:56:51');

-- --------------------------------------------------------

--
-- Table structure for table `forum_categories`
--

CREATE TABLE IF NOT EXISTS `forum_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `main_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_category_id` (`main_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `name`, `main_category_id`) VALUES
(1, 'زبان جاوا', 2),
(3, 'زبان پی اچ پی', 2);

-- --------------------------------------------------------

--
-- Table structure for table `forum_main_categories`
--

CREATE TABLE IF NOT EXISTS `forum_main_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `forum_main_categories`
--

INSERT INTO `forum_main_categories` (`id`, `name`) VALUES
(2, 'برنامه نویسی'),
(3, 'شبکه'),
(4, 'طراحی سایت');

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts`
--

CREATE TABLE IF NOT EXISTS `forum_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_topic_id` int(11) NOT NULL,
  `post_content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `parent_id` (`parent_id`),
  KEY `forum_topic_id` (`forum_topic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `forum_posts`
--

INSERT INTO `forum_posts` (`id`, `forum_topic_id`, `post_content`, `user_id`, `parent_id`, `create_time`) VALUES
(13, 5, 'پاسخ ادمین به آقای مترجم پست اول - 1', 1, 9, '2015-10-02 11:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `forum_posts_thanks`
--

CREATE TABLE IF NOT EXISTS `forum_posts_thanks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `forum_post_id` (`forum_post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forum_topics`
--

CREATE TABLE IF NOT EXISTS `forum_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_category_id` int(11) NOT NULL,
  `topic_name` varchar(255) NOT NULL,
  `topic_content` text,
  `user_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `topic_name` (`topic_name`),
  KEY `forum_category_id` (`forum_category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `forum_topics`
--

INSERT INTO `forum_topics` (`id`, `forum_category_id`, `topic_name`, `topic_content`, `user_id`, `create_time`) VALUES
(5, 3, 'پی اچ پی بهتر است یا دات نت', 'پی اچ پی بهتر است یا دات نتپی اچ پی بهتر است یا دات نتپی اچ پی بهتر است یا دات نتپی اچ پی بهتر است یا دات نت', 1, '2015-10-02 10:59:49'),
(6, 1, 'dsad', 'asdas', 1, '2015-12-20 13:13:59');

-- --------------------------------------------------------

--
-- Table structure for table `forum_topics_thanks`
--

CREATE TABLE IF NOT EXISTS `forum_topics_thanks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persian_name` varchar(255) NOT NULL,
  `latin_name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persian_name` (`persian_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `persian_name`, `latin_name`, `parent_id`) VALUES
(1, 'محصولات', 'product', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `caption`, `image`) VALUES
(13, 'fsdfsdfs', '73cd6d55245adfa1e87fa3b0ff6684a4.png');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_files`
--

CREATE TABLE IF NOT EXISTS `gallery_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_id` bigint(20) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `format` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `is_special` varchar(255) NOT NULL COMMENT 'عنوان عکس',
  `original_file_name` varchar(255) NOT NULL,
  `extention` varchar(10) NOT NULL,
  `mime_type` varchar(50) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_main_files_ibfk_1` (`gallery_id`),
  KEY `product_main_files_ibfk_2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `gallery_files`
--

INSERT INTO `gallery_files` (`id`, `gallery_id`, `user_id`, `format`, `file_name`, `is_special`, `original_file_name`, `extention`, `mime_type`, `upload_time`) VALUES
(1, 1, 1, 'Image', '94e2f1818439d0d86f0460fbb8a8899b.jpg', '', 'presistence.jpg', 'jpg', 'image/jpeg', '2016-01-05 12:28:05'),
(2, 1, 1, 'Image', '817d92ed4d90b92bf108a074d50fde19.jpg', 'عکس تست', 'download.jpg', 'jpg', 'image/jpeg', '2016-01-05 12:29:00');

-- --------------------------------------------------------

--
-- Table structure for table `language_languages`
--

CREATE TABLE IF NOT EXISTS `language_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `code` varchar(50) NOT NULL,
  `direction` varchar(3) DEFAULT 'rtl',
  `enable` tinyint(4) NOT NULL,
  `default` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `language_languages`
--

INSERT INTO `language_languages` (`id`, `title`, `code`, `direction`, `enable`, `default`) VALUES
(3, 'فارسی', 'fa', 'rtl', 1, 1),
(4, 'انگلیسی', 'en', 'ltr', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `link`
--

CREATE TABLE IF NOT EXISTS `link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `link`
--

INSERT INTO `link` (`id`, `title`, `url`, `image`) VALUES
(24, '', 'http://www.sokanacademy.com', '08d32b80afa6990320b19491f378e18d.png'),
(25, '', 'http://www.sokanacademy.com', 'f4c75adab1de00dd8851713f7da52f17.png');

-- --------------------------------------------------------

--
-- Table structure for table `menu_main_menus`
--

CREATE TABLE IF NOT EXISTS `menu_main_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT 'fa',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `menu_main_menus`
--

INSERT INTO `menu_main_menus` (`id`, `name`, `lang`) VALUES
(1, 'منوی صفحه اصلی', 'fa'),
(2, 'منوی صفحه اصلی', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `menu_menus`
--

CREATE TABLE IF NOT EXISTS `menu_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_menu_id` int(11) NOT NULL,
  `parent_id` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `external_url` varchar(255) NOT NULL,
  `internal_url` int(10) unsigned DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT NULL,
  `class_name_ul` varchar(255) NOT NULL,
  `class_name_li` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `main_menu_id` (`main_menu_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `menu_menus`
--

INSERT INTO `menu_menus` (`id`, `main_menu_id`, `parent_id`, `caption`, `external_url`, `internal_url`, `class_name`, `sort`, `class_name_ul`, `class_name_li`) VALUES
(1, 1, '0', 'خانه', '/', NULL, 'menu', 0, '', ''),
(2, 1, '0', 'درباره ما', 'google.com', 0, 'menu', 1, '', ''),
(3, 1, '0', 'آنالیز', 'http://nikminingco.netshahr.com/fa/آنالیز-آزمایشگاهی', 4, 'menu', 2, '', ''),
(4, 1, '0', 'گالری تصاویر', 'http://nikminingco.netshahr.com/fa/گالری-تصاویر', NULL, 'menu', 3, '', ''),
(5, 1, '0', 'تماس با ما', 'http://nikminingco.netshahr.com/fa/تماس-با-ما', NULL, 'menu', 4, '', ''),
(6, 2, '0', 'Home', '/', NULL, 'menu', 0, '', ''),
(7, 2, '0', 'About us', 'http://nikminingco.netshahr.com/en/about-us', NULL, 'menu', 1, '', ''),
(8, 2, '0', 'Laboratory analysis', 'http://nikminingco.netshahr.com/en/laboratory-analysis', NULL, 'menu', 2, '', ''),
(9, 2, '0', 'Gallery', 'http://nikminingco.netshahr.com/en/gallery', NULL, 'menu', 3, '', ''),
(10, 2, '0', 'Contact us', 'http://nikminingco.netshahr.com/en/contact-us', NULL, 'menu', 4, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `page_pages`
--

CREATE TABLE IF NOT EXISTS `page_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_author_user_id` int(11) NOT NULL,
  `page_content` text NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_status` tinyint(4) NOT NULL DEFAULT '0',
  `page_lang` varchar(2) NOT NULL DEFAULT 'fa',
  `page_slug` varchar(255) NOT NULL,
  `page_display_name` varchar(255) DEFAULT NULL,
  `page_meta_description` text NOT NULL,
  `page_meta_keywords` text NOT NULL,
  `page_create_date` timestamp NULL DEFAULT NULL,
  `page_modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `page_slug` (`page_slug`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `page_pages`
--

INSERT INTO `page_pages` (`id`, `page_author_user_id`, `page_content`, `page_title`, `page_status`, `page_lang`, `page_slug`, `page_display_name`, `page_meta_description`, `page_meta_keywords`, `page_create_date`, `page_modified_date`) VALUES
(1, 1, '<p>asdfsdf</p>', 'dfsdf', 1, 'fa', 'dfsdf', 'ادمین  سایت', '', '', '2016-01-04 18:34:51', '2016-01-04 07:04:51');

-- --------------------------------------------------------

--
-- Table structure for table `page_page_tags`
--

CREATE TABLE IF NOT EXISTS `page_page_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_page_id` int(11) NOT NULL,
  `page_tag_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_page_id` (`page_page_id`) USING BTREE,
  KEY `page_tag_id` (`page_tag_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_tags`
--

CREATE TABLE IF NOT EXISTS `page_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(255) NOT NULL,
  `permission_label` varchar(255) NOT NULL,
  `exception` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=114 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permission_name`, `permission_label`, `exception`) VALUES
(1, 'application-index-signin', 'application-admin-signin', 1),
(2, 'application-index-signup', 'application-admin-signup', 1),
(3, 'application-index-forgot-password', 'application-admin-forgot-password', 1),
(4, 'application-index-reset-password', 'application-admin-reset-password', 1),
(5, 'application-admin-index', 'application-admin-index', 0),
(6, 'application-index-index', 'application-index-index', 0),
(8, 'blog-admin-list-posts', 'blog-admin-list-posts', 0),
(9, 'blog-admin-add-post', 'blog-admin-add-post', 0),
(10, 'blog-admin-list-categories', 'blog-admin-list-categories', 0),
(11, 'blog-redirect-to-blog', 'blog-redirect-to-blog', 0),
(18, 'application-admin-comments', 'application-admin-comments', 0),
(19, 'application-admin-edit-account', 'application-admin-edit-account', 0),
(20, 'application-admin-logout', 'application-admin-logout', 0),
(21, 'application-admin-delete-comment', 'application-admin-delete-comment', 0),
(22, 'blog-admin-edit-post', 'blog-admin-edit-post', 0),
(33, 'blog-admin-can-delete-all-posts', 'blog-admin-can-delete-all-posts', 0),
(34, 'blog-admin-delete-post', 'blog-admin-delete-post', 0),
(53, 'application-admin-user-comments', 'application-admin-user-comments', 0),
(54, 'application-admin-comments-replies', 'application-admin-comments-replies', 0),
(55, 'application-admin-can-delete-all-comments', 'application-admin-can-delete-all-comments', 0),
(58, 'blog-admin-upload', 'blog-admin-upload', 0),
(67, 'forum-admin-main-categories', 'forum-admin-main-categories', 0),
(68, 'forum-admin-add-main-category', 'forum-admin-add-main-category', 0),
(69, 'forum-admin-index', 'forum-admin-index', 0),
(70, 'forum-admin-add-category', 'forum-admin-add-category', 0),
(71, 'forum-admin-topics', 'forum-admin-topics', 0),
(72, 'forum-admin-add-topic', 'forum-admin-add-topic', 0),
(73, 'forum-admin-can-manage-all-topics', 'forum-admin-can-manage-all-topics', 0),
(74, 'forum-admin-posts', 'forum-admin-posts', 0),
(75, 'forum-admin-can-manage-all-posts', 'forum-admin-can-manage-all-posts', 0),
(76, 'slideshow-admin-list', 'slideshow-admin-list', 0),
(77, 'slideshow-admin-edit', 'slideshow-admin-edit', 0),
(78, 'slideshow-ajax-delete-image', 'slideshow-ajax-delete-image', 0),
(79, 'link-admin-list-links', 'link-admin-list-links', 0),
(80, 'link-admin-edit', 'link-admin-edit', 0),
(81, 'link-admin-delete', 'link-admin-delete', 0),
(82, 'link-admin-add', 'link-admin-add', 0),
(83, 'gallery-admin-index', 'gallery-admin-index', 0),
(84, 'gallery-admin-edit-image', 'gallery-admin-edit-image', 0),
(85, 'gallery-admin-delete-image', 'gallery-admin-delete-image', 0),
(86, 'gallery-admin-add', 'gallery-admin-add', 0),
(87, 'content-admin-add-category', 'content-admin-add-category', 0),
(88, 'content-admin-index', 'content-admin-index', 0),
(89, 'content-admin-categories', 'content-admin-categories', 0),
(90, 'content-admin-edit-category', 'content-admin-edit-category', 0),
(91, 'content-admin-delete-category', 'content-admin-delete-category', 0),
(92, 'content-admin-pages', 'content-admin-pages', 0),
(93, 'content-admin-add-page', 'content-admin-add-page', 0),
(94, 'content-admin-edit-page', 'content-admin-edit-page', 0),
(95, 'content-admin-delete-page', 'content-admin-delete-page', 0),
(96, 'content-admin-contents', 'content-admin-contents', 0),
(97, 'content-admin-delete-content', 'content-admin-delete-content', 0),
(98, 'content-admin-edit-content', 'content-admin-edit-content', 0),
(99, 'content-admin-add-content', 'content-admin-add-content', 0),
(100, 'content-admin-menu', 'content-admin-menu', 0),
(101, 'content-admin-edit-parent-menu', 'content-admin-edit-parent-menu', 0),
(102, 'content-admin-sub-menu', 'content-admin-sub-menu', 0),
(103, 'content-admin-delete-parent-menu', 'content-admin-delete-parent-menu', 0),
(104, 'content-admin-edit-sub-menu', 'content-admin-edit-sub-menu', 0),
(105, 'content-admin-delete-sub-menu', 'content-admin-delete-sub-menu', 0),
(106, 'content-admin-add-parent-menu', 'content-admin-add-parent-menu', 0),
(107, 'language-admin-list-languages', 'language-admin-list-languages', 0),
(108, 'page-admin-*', 'pages-admin-*', 0),
(109, 'menu-*', 'menus-admin-*', 0),
(110, 'forum-admin-*', 'forum-admin-*', 0),
(111, 'blog-admin-*', 'blog-admin-*', 0),
(112, 'gallery-admin-*', 'gallery-admin-*', 0),
(113, 'permission-*', 'permission-*', 0);

-- --------------------------------------------------------

--
-- Table structure for table `permission_all_permissions`
--

CREATE TABLE IF NOT EXISTS `permission_all_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `auto` tinyint(1) unsigned DEFAULT '0',
  `order` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=374 ;

--
-- Dumping data for table `permission_all_permissions`
--

INSERT INTO `permission_all_permissions` (`id`, `name`, `desc`, `auto`, `order`) VALUES
(75, 'application-index-index', 'application-index-index', NULL, NULL),
(302, 'socialnetworks-admin-list', 'socialnetworks-admin-list', 1, NULL),
(303, 'socialnetworks-admin-index', 'socialnetworks-admin-index', 1, NULL),
(304, 'socialnetworks-admin-edit', 'socialnetworks-admin-edit', 1, NULL),
(305, 'socialnetworks-admin-delete', 'socialnetworks-admin-delete', 1, NULL),
(306, 'socialnetworks-admin-add', 'socialnetworks-admin-add', 1, NULL),
(307, 'slideshow-ajax-index', 'slideshow-ajax-index', 1, NULL),
(308, 'slideshow-ajax-delete-image', 'slideshow-ajax-delete-image', 1, NULL),
(309, 'slideshow-admin-listslideshows', 'slideshow-admin-listslideshows', 1, NULL),
(310, 'slideshow-admin-index', 'slideshow-admin-index', 1, NULL),
(311, 'slideshow-admin-editslideshow', 'slideshow-admin-editslideshow', 1, NULL),
(312, 'slideshow-admin-addslideshow', 'slideshow-admin-addslideshow', 1, NULL),
(313, 'permission-ajax-index', 'permission-ajax-index', 1, NULL),
(314, 'permission-ajax-get-permissions', 'permission-ajax-get-permissions', 1, NULL),
(315, 'permission-admin-update-auto-permissions', 'permission-admin-update-auto-permissions', 1, NULL),
(316, 'permission-admin-list-permissions', 'permission-admin-list-permissions', 1, NULL),
(317, 'permission-admin-index', 'permission-admin-index', 1, NULL),
(318, 'permission-admin-editpermission', 'permission-admin-editpermission', 1, NULL),
(319, 'permission-admin-edit-users-groups-permissions', 'permission-admin-edit-users-groups-permissions', 1, NULL),
(320, 'permission-admin-deletepermission', 'permission-admin-deletepermission', 1, NULL),
(321, 'permission-admin-add-permission', 'permission-admin-add-permission', 1, NULL),
(322, 'page-admin-list-pages', 'page-admin-list-pages', 1, NULL),
(323, 'page-admin-index', 'page-admin-index', 1, NULL),
(324, 'page-admin-edit-page', 'page-admin-edit-page', 1, NULL),
(325, 'page-admin-delete-page', 'page-admin-delete-page', 1, NULL),
(326, 'page-admin-add-page', 'page-admin-add-page', 1, NULL),
(327, 'menu-ajax-index', 'menu-ajax-index', 1, NULL),
(328, 'menu-ajax-delete-menu', 'menu-ajax-delete-menu', 1, NULL),
(329, 'menu-admin-show-menu', 'menu-admin-show-menu', 1, NULL),
(330, 'menu-admin-list-main-menus', 'menu-admin-list-main-menus', 1, NULL),
(331, 'menu-admin-index', 'menu-admin-index', 1, NULL),
(332, 'menu-admin-edit-main-menu', 'menu-admin-edit-main-menu', 1, NULL),
(333, 'menu-admin-delete-main-menu', 'menu-admin-delete-main-menu', 1, NULL),
(334, 'menu-admin-add-main-menu', 'menu-admin-add-main-menu', 1, NULL),
(335, 'link-admin-list-links', 'link-admin-list-links', 1, NULL),
(336, 'link-admin-index', 'link-admin-index', 1, NULL),
(337, 'link-admin-edit-link', 'link-admin-edit-link', 1, NULL),
(338, 'link-admin-delete-link', 'link-admin-delete-link', 1, NULL),
(339, 'link-admin-add-link', 'link-admin-add-link', 1, NULL),
(340, 'language-admin-list-languages', 'language-admin-list-languages', 1, NULL),
(341, 'language-admin-index', 'language-admin-index', 1, NULL),
(342, 'language-admin-edit-language', 'language-admin-edit-language', 1, NULL),
(343, 'language-admin-delete-language', 'language-admin-delete-language', 1, NULL),
(344, 'language-admin-add-language', 'language-admin-add-language', 1, NULL),
(345, 'forum-admin-topics', 'forum-admin-topics', 1, NULL),
(346, 'forum-admin-posts', 'forum-admin-posts', 1, NULL),
(347, 'forum-admin-main-categories', 'forum-admin-main-categories', 1, NULL),
(348, 'forum-admin-index', 'forum-admin-index', 1, NULL),
(349, 'forum-admin-edit-topic', 'forum-admin-edit-topic', 1, NULL),
(350, 'forum-admin-edit-main-category', 'forum-admin-edit-main-category', 1, NULL),
(351, 'forum-admin-edit-category', 'forum-admin-edit-category', 1, NULL),
(352, 'forum-admin-delete-topic', 'forum-admin-delete-topic', 1, NULL),
(353, 'forum-admin-delete-post', 'forum-admin-delete-post', 1, NULL),
(354, 'forum-admin-delete-main-category', 'forum-admin-delete-main-category', 1, NULL),
(355, 'forum-admin-delete-category', 'forum-admin-delete-category', 1, NULL),
(356, 'forum-admin-add-topic', 'forum-admin-add-topic', 1, NULL),
(357, 'forum-admin-add-main-category', 'forum-admin-add-main-category', 1, NULL),
(358, 'forum-admin-add-category', 'forum-admin-add-category', 1, NULL),
(359, 'contact-admin-list-contacts', 'contact-admin-list-contacts', 1, NULL),
(360, 'contact-admin-index', 'contact-admin-index', 1, NULL),
(361, 'contact-admin-delete-contact', 'contact-admin-delete-contact', 1, NULL),
(362, 'blog-admin-list-posts', 'blog-admin-list-posts', 1, NULL),
(363, 'blog-admin-list-categories', 'blog-admin-list-categories', 1, NULL),
(364, 'blog-admin-index', 'blog-admin-index', 1, NULL),
(365, 'blog-admin-edit-post', 'blog-admin-edit-post', 1, NULL),
(366, 'blog-admin-edit-category', 'blog-admin-edit-category', 1, NULL),
(367, 'blog-admin-delete-post', 'blog-admin-delete-post', 1, NULL),
(368, 'blog-admin-delete-category', 'blog-admin-delete-category', 1, NULL),
(369, 'blog-admin-add-post', 'blog-admin-add-post', 1, NULL),
(370, 'blog-admin-add-category', 'blog-admin-add-category', 1, NULL),
(371, 'application-admin-logout', 'application-admin-logout', 1, NULL),
(372, 'application-admin-index', 'application-admin-index', 1, NULL),
(373, 'application-admin-edit-account', 'application-admin-edit-account', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_permissions`
--

CREATE TABLE IF NOT EXISTS `permission_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(10) DEFAULT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `perm_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=154 ;

--
-- Dumping data for table `permission_permissions`
--

INSERT INTO `permission_permissions` (`id`, `type`, `type_id`, `perm_type`) VALUES
(1, 'group', 1, 'application-admin-edit-account'),
(2, 'group', 1, 'application-admin-index'),
(3, 'group', 1, 'application-admin-logout'),
(4, 'group', 1, 'application-index-index'),
(5, 'group', 1, 'blog-admin-add-category'),
(6, 'group', 1, 'blog-admin-add-post'),
(7, 'group', 1, 'blog-admin-delete-category'),
(8, 'group', 1, 'blog-admin-delete-post'),
(9, 'group', 1, 'blog-admin-edit-category'),
(10, 'group', 1, 'blog-admin-edit-post'),
(11, 'group', 1, 'blog-admin-index'),
(12, 'group', 1, 'blog-admin-list-categories'),
(13, 'group', 1, 'blog-admin-list-posts'),
(14, 'group', 1, 'forum-admin-add-category'),
(15, 'group', 1, 'forum-admin-add-main-category'),
(16, 'group', 1, 'forum-admin-add-topic'),
(17, 'group', 1, 'forum-admin-delete-category'),
(18, 'group', 1, 'forum-admin-delete-main-category'),
(19, 'group', 1, 'forum-admin-delete-post'),
(20, 'group', 1, 'forum-admin-delete-topic'),
(21, 'group', 1, 'forum-admin-edit-category'),
(22, 'group', 1, 'forum-admin-edit-main-category'),
(23, 'group', 1, 'forum-admin-edit-topic'),
(24, 'group', 1, 'forum-admin-index'),
(25, 'group', 1, 'forum-admin-main-categories'),
(26, 'group', 1, 'forum-admin-posts'),
(27, 'group', 1, 'forum-admin-topics'),
(28, 'group', 1, 'gallery-admin-add-image'),
(29, 'group', 1, 'gallery-admin-delete-image'),
(30, 'group', 1, 'gallery-admin-edit-image'),
(31, 'group', 1, 'gallery-admin-index'),
(32, 'group', 1, 'gallery-admin-list-images'),
(33, 'group', 1, 'language-admin-add-language'),
(34, 'group', 1, 'language-admin-delete-language'),
(35, 'group', 1, 'language-admin-edit-language'),
(36, 'group', 1, 'language-admin-index'),
(37, 'group', 1, 'language-admin-list-languages'),
(38, 'group', 1, 'link-admin-add-link'),
(39, 'group', 1, 'link-admin-delete-link'),
(40, 'group', 1, 'link-admin-edit-link'),
(41, 'group', 1, 'link-admin-index'),
(42, 'group', 1, 'link-admin-list-links'),
(43, 'group', 1, 'menu-admin-add-main-menu'),
(44, 'group', 1, 'menu-admin-delete-main-menu'),
(45, 'group', 1, 'menu-admin-edit-main-menu'),
(46, 'group', 1, 'menu-admin-index'),
(47, 'group', 1, 'menu-admin-list-main-menus'),
(48, 'group', 1, 'menu-admin-show-menu'),
(49, 'group', 1, 'menu-ajax-delete-menu'),
(50, 'group', 1, 'menu-ajax-index'),
(51, 'group', 1, 'page-admin-add-page'),
(52, 'group', 1, 'page-admin-delete-page'),
(53, 'group', 1, 'page-admin-edit-page'),
(54, 'group', 1, 'page-admin-index'),
(55, 'group', 1, 'page-admin-list-pages'),
(56, 'group', 1, 'permission-admin-add-permission'),
(57, 'group', 1, 'permission-admin-deletepermission'),
(58, 'group', 1, 'permission-admin-edit-users-groups-permissions'),
(59, 'group', 1, 'permission-admin-editpermission'),
(60, 'group', 1, 'permission-admin-index'),
(61, 'group', 1, 'permission-admin-list-permissions'),
(62, 'group', 1, 'permission-admin-update-auto-permissions'),
(63, 'group', 1, 'permission-ajax-get-permissions'),
(64, 'group', 1, 'permission-ajax-index'),
(65, 'group', 1, 'slideshow-admin-addslideshow'),
(66, 'group', 1, 'slideshow-admin-editslideshow'),
(67, 'group', 1, 'slideshow-admin-index'),
(68, 'group', 1, 'slideshow-admin-listslideshows'),
(69, 'group', 1, 'slideshow-ajax-delete-image'),
(70, 'group', 1, 'slideshow-ajax-index'),
(71, 'group', 1, 'socialnetworks-admin-add'),
(72, 'group', 1, 'socialnetworks-admin-delete'),
(73, 'group', 1, 'socialnetworks-admin-edit'),
(74, 'group', 1, 'socialnetworks-admin-index'),
(75, 'group', 1, 'socialnetworks-admin-list'),
(76, 'group', 1, 'application-admin-edit-account'),
(77, 'group', 1, 'application-admin-index'),
(78, 'group', 1, 'application-admin-logout'),
(79, 'group', 1, 'application-index-index'),
(80, 'group', 1, 'blog-admin-add-category'),
(81, 'group', 1, 'blog-admin-add-post'),
(82, 'group', 1, 'blog-admin-delete-category'),
(83, 'group', 1, 'blog-admin-delete-post'),
(84, 'group', 1, 'blog-admin-edit-category'),
(85, 'group', 1, 'blog-admin-edit-post'),
(86, 'group', 1, 'blog-admin-index'),
(87, 'group', 1, 'blog-admin-list-categories'),
(88, 'group', 1, 'blog-admin-list-posts'),
(89, 'group', 1, 'contact-admin-delete-contact'),
(90, 'group', 1, 'contact-admin-index'),
(91, 'group', 1, 'contact-admin-list-contacts'),
(92, 'group', 1, 'forum-admin-add-category'),
(93, 'group', 1, 'forum-admin-add-main-category'),
(94, 'group', 1, 'forum-admin-add-topic'),
(95, 'group', 1, 'forum-admin-delete-category'),
(96, 'group', 1, 'forum-admin-delete-main-category'),
(97, 'group', 1, 'forum-admin-delete-post'),
(98, 'group', 1, 'forum-admin-delete-topic'),
(99, 'group', 1, 'forum-admin-edit-category'),
(100, 'group', 1, 'forum-admin-edit-main-category'),
(101, 'group', 1, 'forum-admin-edit-topic'),
(102, 'group', 1, 'forum-admin-index'),
(103, 'group', 1, 'forum-admin-main-categories'),
(104, 'group', 1, 'forum-admin-posts'),
(105, 'group', 1, 'forum-admin-topics'),
(106, 'group', 1, 'gallery-admin-add-image'),
(107, 'group', 1, 'gallery-admin-delete-image'),
(108, 'group', 1, 'gallery-admin-edit-image'),
(109, 'group', 1, 'gallery-admin-index'),
(110, 'group', 1, 'gallery-admin-list-images'),
(111, 'group', 1, 'language-admin-add-language'),
(112, 'group', 1, 'language-admin-delete-language'),
(113, 'group', 1, 'language-admin-edit-language'),
(114, 'group', 1, 'language-admin-index'),
(115, 'group', 1, 'language-admin-list-languages'),
(116, 'group', 1, 'link-admin-add-link'),
(117, 'group', 1, 'link-admin-delete-link'),
(118, 'group', 1, 'link-admin-edit-link'),
(119, 'group', 1, 'link-admin-index'),
(120, 'group', 1, 'link-admin-list-links'),
(121, 'group', 1, 'menu-admin-add-main-menu'),
(122, 'group', 1, 'menu-admin-delete-main-menu'),
(123, 'group', 1, 'menu-admin-edit-main-menu'),
(124, 'group', 1, 'menu-admin-index'),
(125, 'group', 1, 'menu-admin-list-main-menus'),
(126, 'group', 1, 'menu-admin-show-menu'),
(127, 'group', 1, 'menu-ajax-delete-menu'),
(128, 'group', 1, 'menu-ajax-index'),
(129, 'group', 1, 'page-admin-add-page'),
(130, 'group', 1, 'page-admin-delete-page'),
(131, 'group', 1, 'page-admin-edit-page'),
(132, 'group', 1, 'page-admin-index'),
(133, 'group', 1, 'page-admin-list-pages'),
(134, 'group', 1, 'permission-admin-add-permission'),
(135, 'group', 1, 'permission-admin-deletepermission'),
(136, 'group', 1, 'permission-admin-edit-users-groups-permissions'),
(137, 'group', 1, 'permission-admin-editpermission'),
(138, 'group', 1, 'permission-admin-index'),
(139, 'group', 1, 'permission-admin-list-permissions'),
(140, 'group', 1, 'permission-admin-update-auto-permissions'),
(141, 'group', 1, 'permission-ajax-get-permissions'),
(142, 'group', 1, 'permission-ajax-index'),
(143, 'group', 1, 'slideshow-admin-addslideshow'),
(144, 'group', 1, 'slideshow-admin-editslideshow'),
(145, 'group', 1, 'slideshow-admin-index'),
(146, 'group', 1, 'slideshow-admin-listslideshows'),
(147, 'group', 1, 'slideshow-ajax-delete-image'),
(148, 'group', 1, 'slideshow-ajax-index'),
(149, 'group', 1, 'socialnetworks-admin-add'),
(150, 'group', 1, 'socialnetworks-admin-delete'),
(151, 'group', 1, 'socialnetworks-admin-edit'),
(152, 'group', 1, 'socialnetworks-admin-index'),
(153, 'group', 1, 'socialnetworks-admin-list');

-- --------------------------------------------------------

--
-- Table structure for table `post_visit_count`
--

CREATE TABLE IF NOT EXISTS `post_visit_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(15) NOT NULL,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `post_visit_count`
--

INSERT INTO `post_visit_count` (`id`, `user_id`, `post_id`, `time`, `ip`, `end_time`) VALUES
(28, 0, 1890, '2015-11-11 08:51:56', '127.0.0.1', '0000-00-00 00:00:00'),
(29, 0, 1890, '2015-11-11 08:52:44', '127.0.0.1', '0000-00-00 00:00:00'),
(30, 0, 1890, '2015-11-11 08:55:12', '127.0.0.1', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE IF NOT EXISTS `slideshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id`, `name`) VALUES
(10, 'temp');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow_images`
--

CREATE TABLE IF NOT EXISTS `slideshow_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slideshow_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slideshow_id` (`slideshow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) DEFAULT '1',
  `salt` varchar(255) NOT NULL,
  `forgot_password_hash` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_birth` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `user_url` varchar(255) DEFAULT NULL,
  `user_registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `user_group_id` (`user_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `password`, `gender`, `salt`, `forgot_password_hash`, `email`, `date_of_birth`, `status`, `user_url`, `user_registration_date`, `user_group_id`) VALUES
(1, 'ادمین ', 'سایت', 'admin', '88e9b1ff3721d02c8501801cc00d4e96', '1', '3453629', '9987d9fa004ce13888e97347b94a97cf', 'admin@admin.com', NULL, 1, NULL, '2015-05-14 11:18:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `title`) VALUES
(1, 'مدیر کل سایت'),
(2, 'مدیر سایت'),
(3, 'کاربر عادی');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_permissions`
--

CREATE TABLE IF NOT EXISTS `user_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_group_id` (`user_group_id`,`permission_id`),
  KEY `permission_id` (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=196 ;

--
-- Dumping data for table `user_group_permissions`
--

INSERT INTO `user_group_permissions` (`id`, `user_group_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(13, 1, 5),
(16, 1, 6),
(26, 1, 8),
(28, 1, 9),
(30, 1, 10),
(31, 1, 11),
(47, 1, 18),
(48, 1, 19),
(51, 1, 20),
(54, 1, 21),
(55, 1, 22),
(79, 1, 33),
(80, 1, 34),
(122, 1, 53),
(125, 1, 54),
(128, 1, 55),
(131, 1, 58),
(140, 1, 67),
(141, 1, 68),
(143, 1, 69),
(144, 1, 70),
(145, 1, 71),
(146, 1, 72),
(148, 1, 73),
(149, 1, 74),
(150, 1, 75),
(157, 1, 76),
(158, 1, 77),
(159, 1, 78),
(160, 1, 79),
(163, 1, 79),
(161, 1, 80),
(162, 1, 81),
(164, 1, 82),
(165, 1, 83),
(166, 1, 84),
(167, 1, 85),
(168, 1, 86),
(169, 1, 87),
(170, 1, 88),
(171, 1, 89),
(172, 1, 90),
(173, 1, 91),
(174, 1, 92),
(175, 1, 93),
(176, 1, 94),
(177, 1, 95),
(178, 1, 96),
(179, 1, 97),
(180, 1, 98),
(181, 1, 99),
(182, 1, 100),
(183, 1, 101),
(184, 1, 102),
(185, 1, 103),
(186, 1, 104),
(187, 1, 105),
(188, 1, 106),
(189, 1, 107),
(190, 1, 108),
(191, 1, 109),
(192, 1, 110),
(193, 1, 111),
(194, 1, 112),
(195, 1, 113),
(5, 2, 1),
(6, 2, 2),
(7, 2, 3),
(8, 2, 4),
(14, 2, 5),
(17, 2, 6),
(27, 2, 8),
(29, 2, 9),
(32, 2, 11),
(49, 2, 19),
(52, 2, 20),
(118, 2, 21),
(56, 2, 22),
(81, 2, 34),
(123, 2, 53),
(126, 2, 54),
(151, 2, 71),
(153, 2, 72),
(155, 2, 74),
(9, 3, 1),
(10, 3, 2),
(11, 3, 3),
(12, 3, 4),
(15, 3, 5),
(18, 3, 6),
(50, 3, 19),
(53, 3, 20),
(120, 3, 21),
(82, 3, 34),
(124, 3, 53),
(127, 3, 54),
(152, 3, 71),
(154, 3, 72),
(156, 3, 74);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_post_categories`
--
ALTER TABLE `blog_post_categories`
  ADD CONSTRAINT `blog_post_categories_ibfk_1` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_post_categories_ibfk_2` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blog_post_tags`
--
ALTER TABLE `blog_post_tags`
  ADD CONSTRAINT `blog_post_tags_ibfk_1` FOREIGN KEY (`blog_tag_id`) REFERENCES `blog_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_post_tags_ibfk_2` FOREIGN KEY (`blog_post_id`) REFERENCES `blog_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD CONSTRAINT `forum_categories_ibfk_1` FOREIGN KEY (`main_category_id`) REFERENCES `forum_main_categories` (`id`);

--
-- Constraints for table `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD CONSTRAINT `forum_posts_ibfk_1` FOREIGN KEY (`forum_topic_id`) REFERENCES `forum_topics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_posts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_posts_thanks`
--
ALTER TABLE `forum_posts_thanks`
  ADD CONSTRAINT `forum_posts_thanks_ibfk_1` FOREIGN KEY (`forum_post_id`) REFERENCES `forum_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_posts_thanks_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `forum_topics`
--
ALTER TABLE `forum_topics`
  ADD CONSTRAINT `forum_topics_ibfk_1` FOREIGN KEY (`forum_category_id`) REFERENCES `forum_categories` (`id`),
  ADD CONSTRAINT `forum_topics_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `menu_menus`
--
ALTER TABLE `menu_menus`
  ADD CONSTRAINT `menu_menus_ibfk_1` FOREIGN KEY (`main_menu_id`) REFERENCES `menu_main_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_page_tags`
--
ALTER TABLE `page_page_tags`
  ADD CONSTRAINT `page_page_tags_ibfk_1` FOREIGN KEY (`page_page_id`) REFERENCES `page_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `page_page_tags_ibfk_2` FOREIGN KEY (`page_tag_id`) REFERENCES `page_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slideshow_images`
--
ALTER TABLE `slideshow_images`
  ADD CONSTRAINT `slideshow_images_ibfk_1` FOREIGN KEY (`slideshow_id`) REFERENCES `slideshow` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`);

--
-- Constraints for table `user_group_permissions`
--
ALTER TABLE `user_group_permissions`
  ADD CONSTRAINT `user_group_permissions_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_group_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
