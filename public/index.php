<?php 
// $directory = dirname(__FILE__) . "\\..\\module\\Application\\language";
// $domain = 'messages';
// $locale ="fr_FR"; //like pt_BR.utf8";
// $domain = "fr_FR";
// putenv("LANG=".$locale); //not needed for my tests, but people say it's useful for windows

// $locale = setlocale(LC_ALL, $locale);
// bindtextdomain($domain, $directory);
// textdomain($domain);
// bind_textdomain_codeset($domain, 'UTF-8');

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */

error_reporting(E_ALL);
ini_set('display_errors', true);
chdir(dirname(__DIR__));

date_default_timezone_set('Asia/Tehran');

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
$loader = new Zend\Loader\StandardAutoloader();
$loader->registerNamespace('AtDataGrid', realpath('vendor/atukai'));
$loader->registerNamespace('ZfcDatagrid', realpath('vendor/thadafinser')); 

function __($msgid) {
    if (isset($GLOBALS["translator"])) {
        $translator = $GLOBALS["translator"];
        return $translator->translate($msgid);
    } 
    return $msgid;
   
}

 function _t($msgid) {
     $memcache = new \Memcache();
     $memcache->connect('localhost', 11211);
     $langData = $memcache->get($GLOBALS['lang']);
     if(empty($langData)){
         $langData = array("Hi" =>"سلام");
         $memcache->set($GLOBALS['lang'],$langData,MEMCACHE_COMPRESSED,2592000);
     }
     if (isset($langData[$msgid])) {
         return $langData[$msgid];
     } else {
         return "not found";
     }
     
     
}