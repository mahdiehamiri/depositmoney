<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright  Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * EN-Revision: 30.Jul.2011
 */
return array(
    // Zend\Captcha\ReCaptcha
    "Missing captcha fields" => "کد امنیتی وارد نشده است.",
    "Failed to validate captcha" => "امکان بررسی کد امنیتی وجود ندارد",
    "Captcha value is wrong: %value%" => "کد امنیتی وارد شده اشتباه است.",

    // Zend\Captcha\Word
    "Empty captcha value" => "کد امنیتی وارد نشده است.",
    "Captcha ID field is missing" => "کد امنیتی وارد نشده است.",
    "Captcha value is wrong" => "کد امنیتی وارد شده اشتباه است."
);
