<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'ZendXml\\' => array($vendorDir . '/zendframework/zendxml/library'),
    'TpMinify' => array($vendorDir . '/kkamkou/tp-minify/src/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'PHPExcel' => array($vendorDir . '/phpexcel/phpexcel/Classes'),
);
