<?php
namespace ZfcDatagrid\Renderer\BootstrapTable\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Action\AbstractAction;

/**
 * View Helper
 */
class TableRow extends AbstractHelper implements ServiceLocatorAwareInterface
{

    /**
     *
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator = null;

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator            
     * @return mixed
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        
        return $this;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     *
     * @param string $name            
     * @return string
     */
    private function translate($name)
    {
        if ($this->getServiceLocator()->has('translator') === true) {
            return $this->getServiceLocator()
                ->get('translator')
                ->translate($name);
        }
        return $name;
    }

    private function getTr($row, $open = true)
    {
        if ($open !== true) {
            return '</tr>';
        } else {
            if (isset($row['idConcated'])) {
                return '<tr id="' . $row['idConcated'] . '">';
            } else {
                return '<tr>';
            }
        }
    }

    private function getTd($dataValue, $attributes = [])
    {
        $attr = [];
        foreach ($attributes as $name => $value) {
            if ($value != '') {
                $attr[] = $name . '="' . $value . '"';
            }
        }
        
        $attr = implode(' ', $attr);
        
        return '<td ' . $attr . '>' . $dataValue . '</td>';
    }

    /**
     *
     * @param array $row            
     * @param array $cols            
     * @param AbstractAction $rowClickAction            
     * @param array $rowStyles            
     * @throws \Exception
     * @return string
     */
    public function __invoke($c, $row, array $cols, AbstractAction $rowClickAction = null, array $rowStyles = [], $hasMassActions = false)
    {
        $return = $this->getTr($row);
        $return .= '<td>' . $c . '</td>';
        if (true === $hasMassActions) {
            $return .= '<td><input type="checkbox" name="massActionSelected[]" value="' . $row['idConcated'] . '" /></td>';
        }
        
        foreach ($cols as $col) {
            /* @var $col \ZfcDatagrid\Column\AbstractColumn */
            
            $value = $row[$col->getUniqueId()];
            
            $cssStyles = [];
            $classes = [];
            $attributeColumn = [];
            $ajaxAction = [];
            $fieldType = [];
            $fieldOption = '';
            $tablename = '';
            $fieldname = '';
            $equalfield = '';
            $sms = '';
            
            if ($col->isHidden() === true) {
                $classes[] = 'hidden';
            }
            $attributeColumn = $this->accessProtected($col, 'attributeColumn');
            
            if ($attributeColumn) {
                foreach ($attributeColumn as $key => $attrs) {
                    if ($key == 'class') {
                        $classes[] = $attrs;
                    } else 
                        if ($key == 'route') {
                            $ajaxAction[$key] = $attrs;
                        } else 
                            if ($key == 'type') {
                                $fieldType[$key] = $attrs;
                            } elseif ($key == 'options') {
                                $fieldOption = $attrs;
                            } elseif ($key == 'tablename') {
                                $tablename = $attrs;
                            }  elseif ($key == 'field') {
                                $fieldname = $attrs;
                            }   elseif ($key == 'equalfield') {
                                $equalfield = $attrs;
                            }  elseif ($key == 'sms') {
                                $sms = $attrs;
                            }
                }
            }
            
            switch (get_class($col->getType())) {
                case 'ZfcDatagrid\Column\Type\Number':
                    $cssStyles[] = 'text-align: right';
                    break;
                case 'ZfcDatagrid\Column\Type\PhpArray':
                    $value = '<pre>' . print_r($value, true) . '</pre>';
                    break;
            }
            
            $styles = array_merge($rowStyles, $col->getStyles());
            foreach ($styles as $style) {
                /* @var $style \ZfcDatagrid\Column\Style\AbstractStyle */
                if ($style->isApply($row) === true) {
                    switch (get_class($style)) {
                        
                        case 'ZfcDatagrid\Column\Style\Bold':
                            $cssStyles[] = 'font-weight: bold';
                            break;
                        
                        case 'ZfcDatagrid\Column\Style\Italic':
                            $cssStyles[] = 'font-style: italic';
                            break;
                        
                        case 'ZfcDatagrid\Column\Style\Color':
                            $cssStyles[] = 'color: #' . $style->getRgbHexString();
                            break;
                        
                        case 'ZfcDatagrid\Column\Style\BackgroundColor':
                            $cssStyles[] = 'background-color: #' . $style->getRgbHexString();
                            break;
                        
                        case 'ZfcDatagrid\Column\Style\Align':
                            $cssStyles[] = 'text-align: ' . $style->getAlignment();
                            break;
                        
                        case 'ZfcDatagrid\Column\Style\Strikethrough':
                            $value = '<s>' . $value . '</s>';
                            break;
                        
                        case 'ZfcDatagrid\Column\Style\CSSClass':
                            $classes[] = $style->getClass();
                            break;
                        
                        default:
                            throw new \InvalidArgumentException('Not defined style: "' . get_class($style) . '"');
                            break;
                    }
                }
            }
            
            if ($col instanceof Column\Action) {
                /* @var $col \ZfcDatagrid\Column\Action */
                $actions = [];
                foreach ($col->getActions() as $action) {
                    /* @var $action \ZfcDatagrid\Column\Action\AbstractAction */
                    if ($action->isDisplayed($row) === true) {
                        $action->setTitle($this->translate($action->getTitle()));
                        $actions[] = $action->toHtml($row);
                    }
                }
                
                $value = implode(' ', $actions);
            }
            
            // "rowClick" action
            if ($col instanceof Column\Select && $rowClickAction instanceof AbstractAction && $col->isRowClickEnabled()) {
                $value = '<a href="' . $rowClickAction->getLinkReplaced($row) . '">' . $value . '</a>';
            }
            
            $attributes = [
                'class' => implode(' ', $classes),
                'style' => implode(';', $cssStyles),
                'data-table' => $tablename,
                'data-field' => $fieldname,
                'data-route' => implode(' ', $ajaxAction),
                'data-type' => implode(' ', $fieldType), // i should know if field is select checkbox radio or text
                'data-columnUniqueId' => $col->getUniqueId(),
                'data-options' => $fieldOption,
                'data-equalfield' => $equalfield,
                'data-sms' => $sms
            ];
            
            $return .= $this->getTd($value, $attributes);
        }
        
        $return .= $this->getTr($row, false);
        return $return;
    }

    function accessProtected($obj, $prop)
    {
        $reflection = new \ReflectionClass($obj);
        $property = $reflection->getProperty($prop);
        $property->setAccessible(true);
        return $property->getValue($obj);
    }
}
