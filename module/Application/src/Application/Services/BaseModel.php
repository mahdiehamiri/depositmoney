<?php 

namespace Application\Services;
use Zend\Db\Adapter\Adapter;

class BaseModel 
{
	/**
	 * 
	 * @var \Zend\Db\TableGateway\TableGateway
	 */
   protected $adapter;
   public function getDbAdapter()
    {
        $config = $this->serviceManager->get('config');
        $this->adapter = new Adapter($config['db']);
        return $this->adapter ;
    }
    public function getDbAdapterTransaction()
    {
            return $this->adapter->getDriver()->getConnection() ;
    }
}