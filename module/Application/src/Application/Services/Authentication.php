<?php 

namespace Application\Services;

use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceManager;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Authentication\Storage\Session;
use Zend\Db\Sql\Select;

class Authentication extends AuthenticationService 
{
	protected $authAdapter;
	protected $customStorage;
	protected $storage;
	public function __construct(ServiceManager $serviceManager)
	{
		$adapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
		$this->authAdapter = new AuthAdapter($adapter);
		$this->authAdapter->setTableName('users');
		$this->authAdapter->setIdentityColumn('email');
		$this->authAdapter->setCredentialColumn('password');
		$this->authAdapter->setCredentialTreatment('md5(concat(Sha1(?), salt))');
		$this->setAdapter($this->authAdapter); 
		$this->setStorage($serviceManager->get("Application\Services\MyAuthStorage")); 
	}
	
	public function getSessionStorage()
	{
	    if (! $this->storage) {
	        $this->storage = $this->servicemanager->get('Application\Services\MyAuthStorage');
	    }
	    return $this->storage;
	}
	
	public function auth($username, $password, $rm = 0)
	{
		$this->authAdapter->setIdentity($username);
		$this->authAdapter->setCredential($password); 
		
		$isLogin = $this->authenticate($this->authAdapter);
		if ($isLogin->isValid()) {
		    if ($rm == 1 ) {
		        $this->getSessionStorage()
		        ->setRememberMe(1);
		        //set storage again
		        $this->setStorage($this->getSessionStorage());
		    }
		    
			$storage = $this->getStorage();  
			$storage->write($this->authAdapter->getResultRowObject(array(
					'id',
					'user_group_id',
					'username',
					'email',
			        'status',
			        'avatar',
			        'phone',
			        'mobile',
			        'user_registration_date',
			        'confirm'
			))); 
			$this->getIdentity();
			return true;
		} else {
			return false;
		}
	}
	
	public function verifyIt()
	{
	    $storage = $this->getStorage('authentication');  
	    $userData = $storage->read();
	    $userData->status = 1;
	    $storage->write($userData);
		return $this->getIdentity();
	}
	
	public function isLoggedIn()
	{
		return $this->getIdentity();
	}
	
	public function logOut()
	{
		$this->getStorage()->clear();
		return $this->clearIdentity();
	}
}