<?php

namespace Application\Services;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AutoAbstractFactory implements AbstractFactoryInterface {
	public function canCreateServiceWithName(ServiceLocatorInterface $locator, $name, $requestedName) {
	
		if (class_exists ( $requestedName . 'Controller' )) {
			return true;
		}
		return false;
	}
	public function createServiceWithName(ServiceLocatorInterface $locator, $name, $requestedName) {
		$className = $requestedName . 'Controller'; 
		return new $className ();
	}
}