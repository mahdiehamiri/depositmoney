<?php

/**
 * Perform user access management on requests
 * @author Kourosh Sharifi, Mohammad Rostami
 * @package Application/Helper
 * @copyright shabdiznet.com
 */
namespace Application\Services;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Sql;
use Zend\ServiceManager\ServiceManager;
use Zend\Db\TableGateway\TableGateway;
use Zend\Permissions\Acl\Acl;
use Zend\Session\Container;
use Zend\Config\Reader\Json as JsonParser;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Mvc\MvcEvent;

class PermissionManager
{

    /**
     * every visitor has a unique hash
     * 
     * @var unknown
     */
    public $uniqueVisitorHash;

    /**
     *
     * @var MvcEvent
     */
    public $event;

    /**
     *
     * @var Zend\Permission\Acl
     */
    public $zendAcl;

    /**
     *
     * @var ServiceManager Service Manager object
     */
    public $serviceManager;

    /**
     *
     * @var object loggedin user's data
     */
    public $userData;

    public $resources;

    /**
     *
     * @var array user's group permissions by group id and status
     */
    public $groupPermissionsByStatus;

    /**
     *
     * @var array user's group permissions array
     */
    public $groupPermissions;

    public $deniedGroupPermissions;

    /**
     *
     * @var array user's permissions by status
     */
    public $userPermissionsByStatus;

    /**
     *
     * @var array user's permissions array
     */
    public $userPermissions;

    public $deniedUserPermissions;

    /**
     *
     * @var array extra permissions array
     */
    public $extraPermissions;

    /**
     *
     * @var array application's default premissions (such as auth, public pages,...)
     */
    public $defaultPermissions;

    // public $requestPermissions;
    public $route;

    public $isCustomer = false;

    /**
     *
     * @var array permission configs from config.json
     */
    public $permissionsConfig;

    public $matchedByResource;

    public $matchedBySubResource;

    /**
     * Get service manager instance from module on initilizing
     * 
     * @param ServiceManager $sm            
     */
    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        $config = $serviceManager->get("Config");
        $this->permissionsConfig = $config['permissions_config'];
    }

    /**
     *
     * @return Acl
     */
    public function getZendAcl()
    {
        return $this->zendAcl;
    }

    public function getExtraAccess($request, $extraId, $extraGroup, $getValues = null)
    {
        // $this->requestPermissions[] = $request;
        $permissions = $this->getExtraPermissions($extraId, $extraGroup);
        if ($getValues) {
            return isset($permissions[$request]) ? $permissions[$request] : false;
        } else {
            return isset($permissions[$request]) ? true : false;
        }
    }

    /**
     * Get user id and her request and check if the user
     * have a permision or not on her request, if $getValues
     * is set, then the permission value will be returned, else,
     * just a boolean value will be returned as a method output
     *
     * @param string $request            
     * @param array $values            
     * @return boolean | string
     */
    public function getAccess($request, $getValues = null, $userData = null, $requestByRoute = false)
    {
        $this->getResources();
        $groupPermissions = array();
        $customerPermissions = array();
        $userPermissions = array();
        if ($userData) {
            $this->userData = $userData;
        }
        if ($this->isCustomer) {
            $groupPermissions = $this->getCustomerGroupPermissions($this->userData->id);
        }
        if ($this->userData) {
            $groupPermissions = $this->getGroupPermissions($this->userData->id);
            $userPermissions = $this->getUserPermissions($this->userData->id);
            if ($this->deniedUserPermissions) {
                foreach ($groupPermissions as $groupResourceName => $groupResourceValue) {
                    if (array_key_exists($groupResourceName, $this->deniedUserPermissions)) {
                        unset($groupPermissions[$groupResourceName]);
                    }
                }
            }
        }
        if (! $this->defaultPermissions) {
            $this->defaultPermissions = $this->getPermissionsByType($this->permissionsConfig['default_resource_type']);
        }
        $permissions = array_merge($this->defaultPermissions, $groupPermissions, $userPermissions);

        if ($this->permissionsConfig['enable_zend_acl']) {
            if (! $this->zendAcl) {
                $zendAcl = new Acl();
                $parents = array();
                // Add default permissions
                $zendAcl->addRole(new Role('default'));
                $parents[] = 'default';
                foreach ($this->defaultPermissions as $resourceName => $resourceValue) {
                    $zendAcl->addResource($resourceName);
                    $zendAcl->allow('default', $resourceName);
                } 
                if ($this->userData) {
                    $parents[] = 'group';
                    $roleName = 'group';
                    $zendAcl->addRole(new Role($roleName));
                    if (isset($this->groupPermissionsByStatus)) {
                        foreach ($this->groupPermissionsByStatus as $groupId => $resources) {
                            foreach ($resources as $resourceName => $resourceValue) {
                                if (! $zendAcl->hasResource($resourceName)) {
                                    $zendAcl->addResource($resourceName);
                                    if ($resourceValue == 'allowed') {
                                        $zendAcl->allow($roleName, $resourceName);
                                    } else {
                                        $zendAcl->deny($roleName, $resourceName);
                                    }
                                }
                            }
                        }
                    }
                    $roleName = 'user';
                    $zendAcl->addRole(new Role($roleName), $parents);
                    if (isset($this->userPermissionsByStatus)) {
                        foreach ($this->userPermissionsByStatus as $resourceName => $resourceValue) {
                            if (! $zendAcl->hasResource($resourceName)) {
                                $zendAcl->addResource($resourceName);
                                if ($resourceValue == 'allowed') {
                                    $zendAcl->allow($roleName, $resourceName);
                                } else {
                                    $zendAcl->deny($roleName, $resourceName);
                                }
                            }
                        }
                    }
                }
                $view['zendAcl'] = $zendAcl;
                $this->zendAcl = $zendAcl;
            }
        }
        
        // if request is not null
        if ($requestByRoute || $request) {
            $view['request'] = $request;
            $view['partialRequest'] = $this->partialRequest;
        }
        
        $view['permissions'] = $permissions;
        $this->event->getViewModel()->setVariables($view);
        if ($getValues) {
            return isset($permissions[$request]) ? $permissions[$request] : false;
        } else {
            if (isset($permissions[$request])) {
                return true;
            } else {
               
                foreach ($permissions as $permission => $value) {
                    $permission = trim($permission);
                    if (preg_match("/$permission/i", $request)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public function setRoute($request)
    {
        $this->route = $request;
    }

    /**
     * calls from module.php | check route permissions
     * 
     * @param string $request            
     * @param MvcEvent $event            
     * @param string $unauthorizedMode
     *            | 'hasAccess'
     */
    public function hasRouteAccess($request, $event, $userData = null, $partialRequest)
    {
        $this->route = $request;
        
        $this->partialRequest = $partialRequest;
        $this->event = $event; 
        if (! $this->getAccess($request, null, $userData, true)) { 
            if ($userData->id) {
                $unauthorizedRouteMode = $this->permissionsConfig['unauthorized_route_mode'];
            } else {
                $unauthorizedRouteMode = $this->permissionsConfig['not_loggedin_unauthorized_route_mode'];
            } 
            $unauthorizedMode = $unauthorizedRouteMode;
            if (is_array($unauthorizedRouteMode)) {
                foreach ($unauthorizedRouteMode as $mode => $regex) {
                    if ($regex == '*') {
                        $unauthorizedMode = $mode;
                        break;
                    }
                    if (preg_match($regex, $request)) {
                        $unauthorizedMode = $mode;
                        break;
                    }
                }
            }
            return $unauthorizedMode;
        } else { 
            return 'hasAccess';
        }
    }

    /**
     * Get user id and fetch permissions from database
     * 
     * @param int $userId            
     * @return array user's permissions
     */
    public function getGroupPermissions($userId)
    {
        $dbAdapter = $this->getDbAdapter();
        if (! $this->groupPermissions) {
            $select = new Select();
            $where = new Where();
            $tableGateway = new TableGateway('user_group', $dbAdapter);
            $select->from("user_group");
            $select->columns(array(
                "group_id"
            ));
            $select->join("group_permissions", "user_group.group_id = group_permissions.group_id", array(
                "value",
                "resource_id"
            ));
            $where->equalTo("user_id", $userId);
            $select->where($where);
            // var_dump($select->getSqlString());die;
            $groupPermissions = $tableGateway->selectWith($select)->toArray();
            $allowedPermissions = array();
            foreach ($groupPermissions as $permission) {
                if (isset($this->resources[$permission['resource_id']])) {
                    $resource = $this->resources[$permission['resource_id']]['resource'];
                    $subResources = $this->resources[$permission['resource_id']]['sub_resources'];
                    if ($permission["value"] == $this->permissionsConfig['denied_value']) {
                        $this->groupPermissionsByStatus[$permission['group_id']][$resource] = 'denied';
                        $this->deniedGroupPermissions[$resource] = $permission["value"];
                        continue;
                    } else {
                        $allowedPermissions[$resource] = $permission["value"];
                        $this->groupPermissionsByStatus[$permission['group_id']][$resource] = 'allowed';
                        if ($subResources) {
                            foreach ($subResources as $subResource) {
                                $allowedPermissions[$subResource] = $permission["value"];
                                $this->groupPermissionsByStatus[$permission['group_id']][$subResource] = 'allowed';
                            }
                        }
                    }
                }
            }
            $this->groupPermissions = $allowedPermissions;
        } else {
            $allowedPermissions = $this->groupPermissions;
        }
        return $allowedPermissions;
    }

    public function getCustomerGroupPermissions($customerId)
    {
        $allowedPermissions = array();
        $select = new Select();
        $tableGateway = new TableGateway('customer', $this->getCustomerDbAdapter());
        $select->from("customer");
        $select->where(array(
            'id' => $customerId
        ));
        $customerData = $tableGateway->selectWith($select);
        $customerData = $customerData->current();
        if ($customerData) {
            $userGroupId = $customerData->user_group_id;
            $dbAdapter = $this->getDbAdapter();
            if (! $this->groupPermissions) {
                $select = new Select();
                $where = new Where();
                $tableGateway = new TableGateway('group_permissions', $dbAdapter);
                $select->from("group_permissions");
                $where->equalTo("group_id", $userGroupId);
                $select->where($where);
                $groupPermissions = $tableGateway->selectWith($select)->toArray();
                foreach ($groupPermissions as $permission) {
                    if (isset($this->resources[$permission['resource_id']])) {
                        $resource = $this->resources[$permission['resource_id']]['resource'];
                        $subResources = $this->resources[$permission['resource_id']]['sub_resources'];
                        if ($permission["value"] == $this->permissionsConfig['denied_value']) {
                            $this->groupPermissionsByStatus[$permission['group_id']][$resource] = 'denied';
                            $this->deniedGroupPermissions[$resource] = $permission["value"];
                            continue;
                        } else {
                            $allowedPermissions[$resource] = $permission["value"];
                            $this->groupPermissionsByStatus[$permission['group_id']][$resource] = 'allowed';
                            if ($subResources) {
                                foreach ($subResources as $subResource) {
                                    $allowedPermissions[$subResource] = $permission["value"];
                                    $this->groupPermissionsByStatus[$permission['group_id']][$subResource] = 'allowed';
                                }
                            }
                        }
                    }
                }
                $this->groupPermissions = $allowedPermissions;
            } else {
                $allowedPermissions = $this->groupPermissions;
            }
        }
        return $allowedPermissions;
    }

    /**
     * Get user id and return user groups permissions
     * 
     * @param int $userId            
     * @return array user's group's permissions
     */
    public function getUserPermissions($userId)
    {
        $dbAdapter = $this->getDbAdapter();
        if (! $this->userPermissions) {
            $this->getResources();
            $select = new Select();
            $tableGateway = new TableGateway('user_permissions', $dbAdapter);
            $select->from("user_permissions");
            $where = new Where();
            $where->equalTo("user_id", $userId);
            $select->where($where);
            $userPermissions = $tableGateway->selectWith($select)->toArray();
            $allowedPermissions = array();
            foreach ($userPermissions as $permission) {
                if (isset($this->resources[$permission['resource_id']])) {
                    $resource = $this->resources[$permission['resource_id']]['resource'];
                    $subResources = $this->resources[$permission['resource_id']]['sub_resources'];
                    if ($permission["value"] == $this->permissionsConfig['denied_value']) {
                        $this->userPermissionsByStatus[$resource] = 'denied';
                        $this->deniedUserPermissions[$resource] = $permission["value"];
                        continue;
                    } else {
                        $allowedPermissions[$resource] = $permission["value"];
                        $this->userPermissionsByStatus[$resource] = 'allowed';
                        if ($subResources) {
                            foreach ($subResources as $subResource) {
                                $allowedPermissions[$subResource] = $permission["value"];
                                $this->userPermissionsByStatus[$subResource] = 'allowed';
                            }
                        }
                    }
                }
            }
            $this->userPermissions = $allowedPermissions;
        } else {
            $allowedPermissions = $this->userPermissions;
        }
        return $allowedPermissions;
    }

    /**
     * Get array of extra permissions with extra id and extra group name
     * 
     * @param integer $extraId            
     * @param string $extraGroup            
     * @return array
     */
    public function getExtraPermissions($extraId, $extraGroup)
    {
        $dbAdapter = $this->getDbAdapter();
        if (! $this->extraPermissions[$extraGroup][$extraId]) {
            $this->getResources();
            $select = new Select();
            $tableGateway = new TableGateway('extra_permissions', $dbAdapter);
            $select->from("extra_permissions");
            $where = new Where();
            $where->equalTo("extra_id", $extraId);
            $where->equalTo("extra_permissions.extra_group", $extraGroup);
            $select->where($where);
            $extraPermissions = $tableGateway->selectWith($select)->toArray();
            $allowedPermissions = array();
            foreach ($extraPermissions as $permission) {
                if ($permission["value"] == $this->permissionsConfig['denied_value']) {
                    continue;
                } else {
                    if (isset($this->resources[$permission['resource_id']])) {
                        $resource = $this->resources[$permission['resource_id']]["resource"];
                        $subResources = $this->resources[$permission['resource_id']]["sub_resources"];
                        $allowedPermissions[$resource] = $permission["value"];
                        if ($subResources) {
                            foreach ($subResources as $subResource) {
                                $allowedPermissions[$subResource] = $permission["value"];
                            }
                        }
                    }
                }
            }
            $this->extraPermissions[$extraGroup][$extraId] = $allowedPermissions;
        } else {
            $allowedPermissions = $this->extraPermissions[$extraGroup][$extraId];
        }
        return $allowedPermissions;
    }

    /**
     * Get permissions in specific type and return all this type's permissions
     * 
     * @param mixed $type            
     * @return array specific type permission
     */
    public function getPermissionsByType($type)
    {
        $this->getResources();
        $allowedPermissions = array();
        foreach ($this->resources as $permission) {
            if ($permission['type'] == $type) {
                $allowedPermissions[$permission["resource"]] = true;
                if ($permission['sub_resources']) {
                    foreach ($permission['sub_resources'] as $subResource) {
                        $allowedPermissions[$subResource] = true;
                    }
                }
            }
        }
        return $allowedPermissions;
    }

    public function getResources()
    {
        if (! $this->resources) {
            $dbAdapter = $this->getDbAdapter();
            $sql = new Sql($dbAdapter);
            $select = $sql->select();
            $select->from("resources");
            $select->join("sub_resources", 'resource_id = resources.id', array(
                "sub_resource"
            ), Select::JOIN_LEFT);
            $statement = $sql->prepareStatementForSqlObject($select);
            $resources = $statement->execute();
            $tmpResources = array();
            foreach ($resources as $resource) {
                if (! isset($tmpResources[$resource["id"]]['resource'])) {
                    foreach ($resource as $field => $value) {
                        if ($field == 'sub_resource') {
                            continue;
                        }
                        $tmpResources[$resource["id"]][$field] = $value;
                    }
                }
                if ($resource['sub_resource']) {
                    $tmpResources[$resource["id"]]['sub_resources'][] = $resource["sub_resource"];
                } else {
                    $tmpResources[$resource["id"]]['sub_resources'] = array();
                }
            }
            $this->resources = $tmpResources;
        }
        
        return $this->resources;
    }

    /**
     *
     * @return Database Adapter object
     */
    protected function getDbAdapter()
    {
        return $this->serviceManager->get("DbAdapter");
    }

    /**
     *
     * @return Customer Database Adapter object
     */
    protected function getCustomerDbAdapter()
    {
        return $this->serviceManager->get("CustomerDbAdapter");
    }
}