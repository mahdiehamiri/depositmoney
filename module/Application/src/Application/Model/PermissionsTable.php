<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class PermissionsTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('permissions', $this->getDbAdapter());
    }
    
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}

	public function getDefaultPermission()
	{
		$select = new Select('permissions');
		$select->where(array(
			'exception' => 1
		));
		return $this->tableGateway->selectWith($select)->toArray();
	}
}