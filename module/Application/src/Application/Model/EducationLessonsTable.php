<?php 
namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class EducationLessonsTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'education_lessons';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway($this->table, $this->getDbAdapter());
    }
	
	public function search($lesson = false)
	{
	    if ($lesson) {
	        $select = new Select('education_lessons');
	        $select->where->expression("MATCH(name) AGAINST (?)", $lesson);
	        return $this->tableGateway->selectWith($select)->toArray();
	    }
	
	}

}
