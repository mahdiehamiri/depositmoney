<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class UserGroupPermissionsTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('user_group_permissions', $this->getDbAdapter());
    }
    
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getPermissions($userGroupId)
	{
		$select = new Select('user_group_permissions');
		$select->join('permissions', 'permissions.id = user_group_permissions.permission_id', array(
			'permission_name',
			'exception'	
		), Select::JOIN_INNER);
		$select->where(array(
				'user_group_id' => $userGroupId
		));
		return $this->tableGateway->selectWith($select)->toArray();
	}
}