<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class DepositPaymentTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('deposit_payment', $this->getDbAdapter());
    }
    
	public function getRecords()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAll($userId=null,$status=null)
	{
		$select = new Select('deposit');
		if($userId){
			$select->where->equalTo('user_id', $userId); 
		}
		if($status==null){
			$select->where->equalTo('deposit.status', 'در انتظار پرداخت'); 
		}else{
			$select->where->equalTo('deposit.status', $status); 
		}
		
		$select->join('users','users.id = deposit.user_id', array('username'), Select::JOIN_LEFT);

	    return $select;
	}

	public function getAllDeleted($userId=null)
	{
		$select = new Select('deposit');
		if($userId){
			$select->where->equalTo('user_id', $userId); 
		}
		$select->where->equalTo('deposit.status', 'deleted'); 
		$select->join('users','users.id = deposit.user_id', array('username'), Select::JOIN_LEFT);

	    return $select;
	}

	public function add($data)
	{
		$this->tableGateway->insert(array(
	        'user_id' => $data['user_id'],
	        'deposit_id' => $data['deposit_id'],
			'price' => $data['price'],
			'remain_price' => $data['remain_price'],
			'description' => $data['description']
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function getById($id)
	{
		$select = new Select('deposit_payment');
		$select->where->equalTo('deposit_id', $id);
		// $select->join('deposit_details','deposit_details.deposit_id = deposit.id', array('*'), Select::JOIN_LEFT);
		return $this->tableGateway->selectWith($select)->toArray();

	}

	public function update($data,$id){
		return $this->tableGateway->update(array( 
            'user_id' => $data['user_id'],
	        // 'deposit_id' => $data['deposit_id'],
			'price' => $data['price'],
			'remain_price' => $data['remain_price'],
			'description' => $data['description']
        ), array("id" => $id));
	}

	public function updateStatus($data,$id){
		return $this->tableGateway->update(array( 
            'status' => $data,
        ), array("id" => $id));
	}

}
