<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class DepositAccountTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('deposit_account', $this->getDbAdapter());
    }
    
	public function getRecords()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function add($data)
	{
	
		$this->tableGateway->insert(array(
	        'deposit_id' => $data['deposit_id'],
			'account_type' => $data['account_type'],
			'bank_name' => $data['bank_name'],
			'account_number' => $data['account_number'],
			'account_owner' => $data['account_owner'],
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}

	public function getById($id)
	{
		$select = new Select('deposit_account');
		$select->where->equalTo('deposit_id', $id);
		// $select->join('deposit_details','deposit_details.deposit_id = deposit.id', array('*'), Select::JOIN_LEFT);
		return $this->tableGateway->selectWith($select)->toArray();

	}

	public function update($data,$id){
		return $this->tableGateway->update(array(
			'account_type' => $data['account_type'],
			'bank_name' => $data['bank_name'],
			'account_number' => $data['account_number'],
			'account_owner' => $data['account_owner'],
        ), array("id" => $id));
	}

}
