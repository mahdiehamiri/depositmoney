<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Console\Prompt\Select;

class UsersLoginLogTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('users_login_log', $this->getDbAdapter());
    }
	public function getAllGroups()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function insertUserLastLogin($userId)
	{
	    $data = array(
	        'user_id' => $userId,
	        "login_time" => new Expression('NOW()')
	    );
	    //var_dump($data); die;
	    return $this->tableGateway->insert($data);
	
	}
	
	public function updateUserLastLogin($userId)
	{
	    $select = new \Zend\Db\Sql\Select('users_login_log');
	    $select->where->equalTo('user_id', $userId);
	    $select->order('id DESC');
	    $select->limit(1);
	    $select->columns(array('id'));
	    $id = $this->tableGateway->selectWith($select)->toArray();
	    //var_dump($id); die;
	    return $this->tableGateway->update(array(
	        "logout_time" => new Expression('NOW()')), array('id' => $id[0]));
	
	}
}