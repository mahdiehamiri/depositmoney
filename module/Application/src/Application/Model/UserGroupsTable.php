<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;

class UserGroupsTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('user_groups', $this->getDbAdapter());
    }
	public function getAllGroups()
	{
		return $this->tableGateway->select()->toArray();
	}
}