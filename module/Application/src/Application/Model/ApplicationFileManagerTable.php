<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ApplicationFileManagerTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('application_file_manager', $this->getDbAdapter());
	}
	
	public function fetch($entityId = null, $id = null)
	{
		$select = new Select('application_file_manager');
		if ($entityId) {
			$select->where->equalTo('entity_id', $entityId);
		}
		if ($id) {
			$select->where->equalTo('id', $id);						
		}
		$data = $this->tableGateway->selectWith($select)->toArray();
		$main = array();
		if ($data) {
			foreach ($data as $key => $value) {
				$main[$value['entity_type']][] = $value;
			}
		}
		return $main;
	}
	
	public function fetchFiles($entityId = null, $id = null)
	{
	    $select = new Select('application_file_manager');
	    if ($entityId) {
	        $select->where->equalTo('entity_id', $entityId);
	    }
	    if ($id) {
	        $select->where->equalTo('id', $id);
	    }
	    $data = $this->tableGateway->selectWith($select)->toArray();
		$main = array();
		if ($data) {
			foreach ($data as $key => $value) {
				$main[$value['id']] = $value;
			}
		}
		return $main;
	   
	}
	
	public function fetchForDelete($id = null)
	{
	    $select = new Select('application_file_manager');
	    $select->join('shop_product_attributes', 'application_file_manager.entity_type = shop_product_attributes.developer_name',
	        array('is_forced'), 'LEFT');
	    if ($id) {
	        $select->where->equalTo('application_file_manager.id', $id);
	        $select->where->equalTo('is_forced', 0);
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function checkExist($entityId = null, $entityType = null)
	{
	    $select = new Select('application_file_manager');
	    
        $select->where->equalTo('entity_id', $entityId);
        $select->where->equalTo('entity_type', $entityType);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function add($userId, $entityId, $entityType, $name, $metaInfo, $fieldType, $folder = '')
	{
		$this->tableGateway->insert(array(
				'user_id'     => $userId,
				'entity_id'   => $entityId,
				'entity_type' => $entityType,
				'name'        => $name,
				'folder'	  => $folder,
				'meta_info'   => $metaInfo,
				'field_type' => $fieldType,
		));
		return $this->tableGateway->lastInsertValue;
	}
	
	public function delete($id)
	{
		if ($id > 0 && is_numeric($id)) {
			return $this->tableGateway->delete(array(
					'id' => $id 
			));
		}
	}
}
