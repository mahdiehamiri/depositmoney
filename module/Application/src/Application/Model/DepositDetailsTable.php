<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class DepositDetailsTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('deposit_details', $this->getDbAdapter());
    }
    
	public function getRecords()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function add($data)
	{
	
		$this->tableGateway->insert(array(
	        'deposit_id' => $data['deposit_id'],
			'product' => $data['product'],
			'count' => $data['count'],
			'price' => $data['price'],
			'sum' => $data['sum'],
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}

	public function getById($id)
	{
		$select = new Select('deposit_details');
		$select->where->equalTo('deposit_id', $id);
		// $select->join('deposit_details','deposit_details.deposit_id = deposit.id', array('*'), Select::JOIN_LEFT);
		return $this->tableGateway->selectWith($select)->toArray();

	}

	public function update($data,$id){
		return $this->tableGateway->update(array( 
			'product' => $data['product'],
			'count' => $data['count'],
			'price' => $data['price'],
			'sum' => $data['sum'],
        ), array("id" => $id));
	}
}
