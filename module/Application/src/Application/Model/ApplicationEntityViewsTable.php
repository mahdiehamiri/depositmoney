<?php

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

class ApplicationEntityViewsTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;

	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('application_entity_views', $this->getDbAdapter());
	}

	public function insertHit($entityType = null, $entityId = null, $userId = null)
	{
	    
		$currentDate = time() - (24 * 60 * 60);
		$userIp = getenv('HTTP_CLIENT_IP')?:
		getenv('HTTP_X_FORWARDED_FOR')?:
		getenv('HTTP_X_FORWARDED')?:
		getenv('HTTP_FORWARDED_FOR')?:
		getenv('HTTP_FORWARDED')?:
		getenv('REMOTE_ADDR');
		
	    $where = new Where();
	    $where->equalTo("entity_type", $entityType);
	    $where->equalTo("entity_id", $entityId);
	    $where->equalTo("user_ip", $userIp);
	    $where->nest()->equalTo("user_id", $userId)->or->isNull("user_id")->unest;
	    $hitExists = $this->tableGateway->select($where)->toArray();
		
		if (! $hitExists) {
			return $this->tableGateway->insert(array(
					'entity_type' => $entityType,
					'entity_id'   => $entityId,
					'user_id'     => $userId,
					'user_ip'     => $userIp,
					'time'        => time()
			));
		}
		return false;
	}

	public function fetchMostVisitedBlogPosts($entityType = 'blog_posts', $limit, $interval)
	{
		$select = new Select('application_entity_views');
		$where = new Where();
		$where->equalTo('entity_type', $entityType);
	  
		if ($interval && is_numeric($interval) && $interval > 0) {
			$time = time() - $interval;
			$where->greaterThanOrEqualTo('time', $time);
		}
	  
		$select->where($where);
	  
		$select->join($entityType, 'application_entity_views.entity_id = '.$entityType.'.id', array('*'), 'left');
		$select->join('blog_post_categories', 'blog_post_categories.blog_post_id = blog_posts.id', array(), Select::JOIN_LEFT);
		$select->join('blog_categories', 'blog_categories.id = blog_post_categories.blog_category_id', array('category_title'), Select::JOIN_LEFT);

		$select->columns(array(
				'*',
				'count' => new Expression('COUNT(entity_id)')
		));
		$select->group('application_entity_views.entity_id');
		$select->order('count desc');
		$select->limit($limit);

		$resultSet = $this->tableGateway->selectWith($select)->toArray();
		return $resultSet;
	}
}
