<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ShopProductsTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	public $adapter;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->adapter = $this->getDbAdapter();
		$this->tableGateway = new TableGateway('shop_products', $this->adapter);
	}

	public function fetchAll($productId = null)
	{
		$select = new Select('shop_products');
		$select->join('shop_product_attribute_info', 'shop_product_attribute_info.product_id=shop_products.id', array('*'), 'LEFT');
		if ($productId > 0 && is_numeric($productId)) {
			$select->where->equalTo('id', $productId);
		}
		$select->group('shop_products.id');
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchById($productId)
	{
	    $select = new Select('shop_products');
	    $select->join('shop_product_attribute_info', 'shop_product_attribute_info.product_id=shop_products.id', array('*'), 'LEFT');
	    if ($productId > 0 && is_numeric($productId)) { 
	        $select->where->equalTo('category_id', $productId);
	    }
	    $select->where->equalTo('is_deleted', 0);
	    $select->group('shop_products.id');
	    //return $this->tableGateway->selectWith($select)->toArray();
	    return $select;
	}

	public function getCategoryId($productId)
	{
	    $select = new Select('shop_products');
	    
	    $select->where->equalTo('id', $productId);
	    $select->columns(array('category_id'));
	    return $this->tableGateway->selectWith($select)->toArray();
	    //return $select;
	}
	public function getProductByCategoryId($catId)
	{
	    $select = new Select('shop_products');
	    
	    $select->where->equalTo('category_id', $catId);
	    
	    return $this->tableGateway->selectWith($select)->toArray();
	    //return $select;
	}
	
	public function isExist($validData, $productId = null)
	{
		$select = new Select('shop_products');
		if ($productId) {
			$select->where(array(
					'(name = ? OR seo_name = ?)' => array($validData['name'], $validData['seo_name']),
					'id != ?' => $productId
			));			
		} else {
			$select->where(array(
					'(name = ? OR seo_name = ?)' => array($validData['name'], $validData['seo_name'])
			));
		}
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function hasCategory($categoryId)
	{
		if ($categoryId > 0 && is_numeric($categoryId)) {
			$select = new Select('shop_products');
			$select->where->equalTo('category_id', $categoryId);
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	
	public function addProduct($validData)
	{
		$this->tableGateway->insert(array(
				'name'             => $validData['name'],
				'seo_name'         => $validData['seo_name'],
				'description'      => $validData['description'],
				'meta_description' => $validData['meta_description'],
				'meta_keywords'    => $validData['meta_keywords'],
				'icon'             => $validData['icon'],
				'status'           => $validData['status'],
				'category_id'      => $validData['category_id'],
    		    'instructor_id'    => (isset($validData['instructor_id'])?$validData['instructor_id']:null),
    		    'study_id'         => (isset($validData['study_id'])?$validData['study_id']:null),
    		    'lesson_id'        => (isset($validData['lesson_id'])?$validData['lesson_id']:null)
		));
		return $this->tableGateway->getLastInsertValue();
	}
	
	public function updateProduct($validData, $productId)
	{
		if ($productId > 0 && is_numeric($productId)) {
			$newData = array(
				'name'             => $validData['name'],
				'seo_name'         => $validData['seo_name'],
				'description'      => $validData['description'],
				'meta_description' => $validData['meta_description'],
				'meta_keywords'    => $validData['meta_keywords'],
				'icon'             => $validData['icon'],
				'status'           => $validData['status'],
				'category_id'      => $validData['category_id'],
			    'instructor_id'    => (isset($validData['instructor_id'])?$validData['instructor_id']:null),
			    'study_id'         => (isset($validData['study_id'])?$validData['study_id']:null),
			    'lesson_id'        => (isset($validData['lesson_id'])?$validData['lesson_id']:null)
			);
			return $this->tableGateway->update($newData, array('id' => $productId));
		}
	}
	
	public function deleteProduct($productId)
	{
		if ($productId > 0 && is_numeric($productId)) {
			return $this->tableGateway->update(
			    array(
			        'is_deleted' => 1
			    ),
			    array(
					'id' => $productId
			));
		}
	}
	
	public function search($product = false, $lang = "fa", $getPaginator = true)
	{
	    
	        $select = new Select('shop_products');
	        if ($product) {
	           $select->where->expression("MATCH(name, description, meta_description, meta_keywords) AGAINST (?)", $product);
	           $select->where->equalTo("product_lang", $lang);
	           $select->where->equalTo("is_deleted", "0");
	           $select->where->equalTo("status", "1");
	        }
	        if ($getPaginator) {
	            $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
	            return new Paginator($dbSelector);
	        } else {
	            return $this->tableGateway->selectWith($select)->toArray();
	        }
	}
	
	public function searchAdvanced($toolbar = false, $fieldOfStudy = false, $instructor = false, $lang = "fa", $getPaginator = true)
	{
	     
	    $select = new Select('shop_products');
	    if ($toolbar) {
	        $select->where->expression("MATCH(shop_products.name, shop_products.description, shop_products.meta_description, shop_products.meta_keywords) AGAINST (?)", $toolbar);
	        $select->where->equalTo("product_lang", $lang);
	        $select->where->equalTo("is_deleted", "0");
	        $select->where->equalTo("shop_products.status", "1");
	    }
        if ($instructor) {
            $select->join('users_data', 'users_data.user_id=shop_products.instructor_id', array("firstname", "lastname"), 'LEFT');
            $select->where->expression("MATCH(firstname, lastname) AGAINST (? IN BOOLEAN MODE)", $instructor);
        }
        if ($fieldOfStudy) {
           $select->join('education_fieldofstudy', 'education_fieldofstudy.id=shop_products.study_id', array('study_name' => 'name'), 'LEFT');
           $select->where->expression("MATCH(education_fieldofstudy.name) AGAINST (? IN BOOLEAN MODE)", $fieldOfStudy);
        }
//         if ($lesson) {
//             $select->join('education_lessons', 'education_lessons.id=shop_products.lesson_id', array('lesson_name' => 'name'), 'LEFT');
//             $select->where->expression("MATCH(education_lessons.name) AGAINST (? IN BOOLEAN MODE)", $lesson);
//         }
        if ($getPaginator) {
            $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($dbSelector);
        } else {
            return $this->tableGateway->selectWith($select)->toArray();
        }
	}
}
