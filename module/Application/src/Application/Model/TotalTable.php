<?php
namespace Application\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Profiler\Profiler;
use Zend\Validator\LessThan;

class TotalTable extends BaseModel
{ 
    protected $tableGateway; 
    protected $adapter; 
    protected $serviceManager; 
    protected $tablename;

    public function __construct($sm, $tablename)
    { 
        $this->serviceManager = $sm;
        $this->tablename = $tablename;  
        $this->tableGateway = new TableGateway($tablename, $this->getDbAdapter()); 
    }

    public function deleteRecord($id)
    { 
       $where = new Where();
       $where->equalTo('id', $id); 
       return $this->tableGateway->delete($where);
    }
    public function getDataById($equalById, $tablename)
    { 
        $select = new Select($tablename); 
        $select->where->equalTo('id', $equalById);
        return $this->tableGateway->selectWith($select)->current();
    }
}
