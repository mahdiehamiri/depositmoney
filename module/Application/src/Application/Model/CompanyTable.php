<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class CompanyTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('office', $this->getDbAdapter());
    }
    
	public function getRecords()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function add($postData)
	{
	    $this->tableGateway->insert(array(
	        'company_name' => $postData['company_name'],
	        'company_history' => $postData['company_history'],
	        'manager_name' => $postData['manager_name'],
	        'fax' => $postData['fax'],
	        'user_id' => $postData['user_id'],
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
// 	public function getAllComments()
// 	{
// 		return $this->tableGateway->select()->toArray();
// 	}
	
// 	public function fetchComments($userId = null)
// 	{
// 		$select = new Select('comments');
// 		$select->join('users', 'users.id = comments.user_id', array(
// 				'firstname',
// 				'lastname'
// 		), Select::JOIN_LEFT);
// 		$select->join('blog_posts', new Expression('(comments.entity_id = blog_posts.id AND comments.entity_type = "post")'), array(
// 				'post_title',
// 				'post_slug'
// 		), Select::JOIN_LEFT);
// 		$select->join('blog_post_categories', 'blog_post_categories.blog_post_id = blog_posts.id', array(), Select::JOIN_LEFT);
// 		$select->join('blog_categories', 'blog_categories.id = blog_post_categories.blog_category_id', array(
// 			'category_title'
// 		), Select::JOIN_LEFT);
// 		$select->join('shop_products', new Expression('(comments.entity_id = shop_products.id AND comments.entity_type = "product")'), array(
// 				'product_id' => 'id',
// 				'name'
// 		), Select::JOIN_LEFT);
// 		if ($userId) {
// 			$select->where->equalTo('comments.user_id', $userId);
// 		}
// 		$select->order('comments.id DESC');
// 		return $select;
// // 		$comments = $this->tableGateway->selectWith($select)->toArray();
// // 		return $comments;
// 	}
	
// 	public function fetchCommentsReplies($userId = null)
// 	{
// 		$select = new Select('comments');
// 		$select->join('users', 'users.id = comments.user_id', array(
// 				'firstname',
// 				'lastname'
// 		), Select::JOIN_LEFT);
// 		$select->join('blog_posts', new Expression('(comments.entity_id = blog_posts.id AND comments.entity_type = "post")'), array(
// 				'post_title',
// 				'post_slug'
// 		), Select::JOIN_LEFT);
// 		$select->join('projects', new Expression('(comments.entity_id = projects.id AND comments.entity_type = "project")'), array(
// 				'project_id' => 'id',
// 				'project_title'
// 		), Select::JOIN_LEFT);
// 		$select->join('courses', new Expression('(comments.entity_id = courses.id AND comments.entity_type = "tutorials")'), array(
// 				'course_name',
// 				'course_title'
// 		), Select::JOIN_LEFT);
		
// 		$select->join(array('tutorial_courses' => 'courses'), 'tutorial_courses.id = tutorials.course_id', array(
// 				'tutorial_course_name' => 'course_name'
// 		), Select::JOIN_LEFT);
		
// 		$select->join('blog_post_categories', 'blog_post_categories.blog_post_id = blog_posts.id', array(), Select::JOIN_LEFT);
// 		$select->join('blog_categories', 'blog_categories.id = blog_post_categories.blog_category_id', array(
// 			'category_title'
// 		), Select::JOIN_LEFT);
// 		if ($userId) {
// 			$selectOwnComments = new Select();
// 			$selectOwnComments->columns(array('id'));
// 			$selectOwnComments->where->equalTo('user_id', $userId);
// 			$selectOwnComments->from('comments');
// 			$select->where->in('comments.parent_id', $selectOwnComments);
// 		}
// 		$select->order('comments.id DESC');
// 		$comments = $this->tableGateway->selectWith($select)->toArray();
// 		return $comments;
// 	}
	
// 	public function deleteComment($commentId = null)
// 	{
// 	    return $this->tableGateway->delete(array('id' => $commentId));
// 	}


}
