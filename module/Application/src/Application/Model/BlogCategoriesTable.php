<?php 
namespace Application\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class BlogCategoriesTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('blog_categories', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getCategories($where = null)
	{
		return $this->tableGateway->select($where)->toArray();
	}
	public function getCategoryById($categoryId)
	{
	    $select = new Select('blog_categories');
	    $select->where->equalTo('id', $categoryId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getCategoryByLanguage($langugeCode)
	{
	    $select = new Select('blog_categories');
	    $select->where->equalTo('category_lang', $langugeCode);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function addCategory($categoryData)
	{
	    $this->tableGateway->insert(array(
	        'category_title' 	=> $categoryData['category_title'],
	    	'category_lang' => $categoryData['category_lang'],
	        'category_status'   => $categoryData['category_status']
	    ));
	
	    return $this->tableGateway->getLastInsertValue();
	}
	public function editCategory($newData, $languageId)
	{
	    return $this->tableGateway->update(array(
	        'category_title'    => $newData['category_title'],
	    	'category_lang' => $newData['category_lang'],
	        'category_status'   => (int)$newData['category_status'],
	    ), array('id' => $languageId));
	}
	
	public function deleteCategory($languageId)
	{
	    return $this->tableGateway->delete(array(
	        'id' => $languageId
	    ));
	}
}