<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class DepositTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('deposit', $this->getDbAdapter());
    }
    
	public function getRecords()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAll($userId=null,$status=null)
	{
		$select = new Select('deposit');
		if($userId){
			$select->where->equalTo('user_id', $userId); 
		}
		if($status==null){
			$select->where->equalTo('deposit.status', 'در انتظار پرداخت'); 
		}else{
			$select->where->equalTo('deposit.status', $status); 
		}
		
		$select->join('users','users.id = deposit.user_id', array('username'), Select::JOIN_LEFT);

	    return $select;
	}

	public function getAllDeleted($userId=null)
	{
		$select = new Select('deposit');
		if($userId){
			$select->where->equalTo('user_id', $userId); 
		}
		$select->where->equalTo('deposit.status', 'deleted'); 
		$select->join('users','users.id = deposit.user_id', array('username'), Select::JOIN_LEFT);

	    return $select;
	}

	public function add($data)
	{
	
		$this->tableGateway->insert(array(
	        'user_id' => $data['user_id'],
	        'discount' => $data['discount'],
			'tax' => $data['tax'],
			'to_pay_price' => $data['to_pay_price'],
			'description' => $data['description'],
			// 'request_check' => $data['request_check'],
			// 'created_check' => $data['created_check'],
			// 'pay_to' => $data['pay_to'],
			// 'payment_details' => $data['payment_details']
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}

	public function addCheck($data,$id){
		return $this->tableGateway->update(array(
			'request_check' => $data['request_check'],
			'created_check' => $data['created_check'],
			'pay_to' => $data['pay_to'],
        ), array("id" => $id));
	}
	
	public function getById($id)
	{
		$select = new Select('deposit');
		$select->where->equalTo('deposit.id', $id);
		return $this->tableGateway->selectWith($select)->current();

	}

	public function update($data,$id){
		return $this->tableGateway->update(array( 
            'user_id' => $data['user_id'],
	        'discount' => $data['discount'],
			'tax' => $data['tax'],
			'to_pay_price' => $data['to_pay_price'],
			'description' => $data['description'],
			// 'request_check' => $data['request_check'],
			// 'created_check' => $data['created_check'],
			// 'pay_to' => $data['pay_to'],
			// 'payment_details' => $data['payment_details']
        ), array("id" => $id));
	}

	public function updateStatus($data,$id){
		return $this->tableGateway->update(array( 
            'status' => $data,
        ), array("id" => $id));
	}

}
