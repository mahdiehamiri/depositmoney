<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ApplicationTagsTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('application_tags', $this->getDbAdapter());
	}
	
	public function getTagByName($tagTitle)
	{
		if ($tagTitle) {
			$select = new Select('application_tags');
			$select->where->equalTo('tag_title', $tagTitle);
			$result = $this->tableGateway->selectWith($select)->toArray();
			if ($result && count($result) != 0) {
				return $result[0];
			} else {
				return false;
			}
		}
	}
	
	public function insertTag($tagTitle)
	{
		if ($tagTitle) {
			$this->tableGateway->insert(array(
				'tag_title' => $tagTitle,
			));
			return $this->tableGateway->getLastInsertValue();
		}
	}
}
