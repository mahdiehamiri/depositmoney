<?php 
namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class EducationFieldofstudyTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'education_fieldofstudy';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway($this->table, $this->getDbAdapter());
    }

	public function search($fieldOfStudy = false)
	{
	    if ($fieldOfStudy) {
	        $select = new Select('education_fieldofstudy');
	        $select->where->expression("MATCH(name) AGAINST (?)", $fieldOfStudy);
	        return $this->tableGateway->selectWith($select)->toArray();
	    }
	     
	}
}
