<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class UsersTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('users', $this->getDbAdapter());
    }
    
    public function getRecords($where = null)
    {
        return $this->tableGateway->select($where)->toArray();
    }
    public function verifyBulkUser($hash)
    {
    
        $isRecordExist = $this->tableGateway->select(array(
            'verify_hash' => $hash
        ))->toArray();
        if (!$isRecordExist) {
            return 'no_user_exists';
        }
        if ($isRecordExist) {
            $data = array(
                'status' 	   => "1",
                'verify_hash' => ''
            );
            return $this->tableGateway->update($data, array('verify_hash' => $hash));
        }
    }
    
    public function verifyBulkUserMobile($hash)
    {
    
        $isRecordExist = $this->tableGateway->select(array(
            'mobile_verify_hash' => $hash
        ))->toArray();
        if (!$isRecordExist) {
            return 'no_user_exists';
        }
        if ($isRecordExist) {
            $data = array(
                'status' 	   => "1",
                'mobile_verify_hash' => ''
            );
            return $this->tableGateway->update($data, array('mobile_verify_hash' => $hash));
        }
    }
    
	public function getAllUsers()
	{
		$users = $this->tableGateway->select()->toArray();
		return $users;
	}
	
	public function fetchAllTranslators()
	{
		$select = new Select('users');
		$select->where->equalTo('user_group_id', 2);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getUserById($userId)
	{
		$select = new Select('users');
		$select->where->equalTo('users.id', $userId);
		$user = $this->tableGateway->selectWith($select)->toArray();
		return $user;
	}
	
	public function registerNewUser($newUserData, $defaultUserGroup)
	{
		$isEmailRegistered = $this->tableGateway->select(array(
			'email' => $newUserData['email']	
		))->toArray();
		if (!$isEmailRegistered) {
			$salt = rand(1000000, 9000000);
			$data = array(
					'email'    		=> $newUserData['email'],
					'password' 		=> md5(sha1($newUserData['password']) . $salt),
					'salt'     		=> $salt,
					'user_group_id' => $defaultUserGroup,
					'status'		=> 1,
			        'is_parent'     => $newUserData['group'],
			        'username'          => $newUserData['email'],
			);
			 $this->tableGateway->insert($data);
			 return  $this->tableGateway->lastInsertValue;
		} else {
			return 'user_exists';
		}
	}
	public function updateUserLastLogin($userId)
	{
	    return $this->tableGateway->update(array(
	        "last_user_login" => new Expression('NOW()')), array('id' => $userId));
	 
	}
	
	public function updateUser($updatedUserInfo, $userId)
	{
		$isEmailRegistered = $this->tableGateway->select(array(
				'email' => $updatedUserInfo['email'],
				'id != ?' => $userId
		))->toArray();
		if (!$isEmailRegistered) {
			$newUserInfo = array(
					'firstname' 	=> $updatedUserInfo['firstname'],
					'lastname'  	=> $updatedUserInfo['lastname'],
					'gender'    	=> $updatedUserInfo['gender'],
					'email'			=> $updatedUserInfo['email'],
// 					'date_of_birth' => $updatedUserInfo['date_of_birth'],
// 					'user_url'      => $updatedUserInfo['user_url']
			);
			if ($updatedUserInfo['password'] && $updatedUserInfo['password'] != '') {
				$salt = rand(1000000, 9000000);
				$newUserInfo['password'] = md5(sha1($updatedUserInfo['password']) . $salt);
				$newUserInfo['salt'] = $salt;
			}
			
			$update = $this->tableGateway->update($newUserInfo, array('id' => $userId));
			return $update;
		} else {
			return 'email_exists';
		}
	}
	
	public function forgotPassword($userEmail, $hash)
	{
		$isEmailRegistered = $this->tableGateway->select(array(
				'email' => $userEmail['email']
		))->current();
		if ($isEmailRegistered) {
			$data = array(
					'forgot_password_hash' => $hash
			);
			return $this->tableGateway->update($data, array('email' => $isEmailRegistered['email']));
		} else {
			return 'no_user_exists';
		}
	}
	
	public function forgotPasswordMobile($mobile, $hash)
	{
	    $isEmailRegistered = $this->tableGateway->select(array(
	        'mobile' => $mobile
	    ))->current();
	    if ($isEmailRegistered) {
	        $data = array(
	            'forgot_password_hash' => $hash
	        );
	        return $this->tableGateway->update($data, array('mobile' => $mobile));
	    } else {
	        return 'no_user_exists';
	    }
	}
	
	public function getHash($hash)
	{
		return $this->tableGateway->select(array(
				'forgot_password_hash' => $hash
		))->toArray();	
	}
	
	public function getVerifyHash($hash)
	{
	    return $this->tableGateway->select(array(
	        'verify_hash' => $hash
	    ))->toArray();
	}
	
	public function getVerifyHashMobile($hash)
	{
	    return $this->tableGateway->select(array(
	        'mobile_verify_hash' => $hash
	    ))->toArray();
	}
	
	public function resetPassword($userData, $hash)
	{
	    
		$isRecordExist = $this->tableGateway->select(array(
				'email' => $userData['email'],
				'forgot_password_hash' => $hash
		))->toArray();
		if (!$isRecordExist) {
			return 'no_user_exists';
		}
		if ($isRecordExist) {
			$salt = rand(1000000, 9000000);
			$data = array(
					'salt' 	   => $salt,
					'password' => md5(sha1($userData['password']) . $salt),
					'forgot_password_hash' => ''
			);
			return $this->tableGateway->update($data, array('email' => $isRecordExist[0]['email']));
		}
	}
	
	public function search($user = false, $lang = "fa", $getPaginator = true)
	{
	    if ($user) {
	      $select = new Select('users');
	      $select->join('users_data', 'users.id=users_data.user_id', array("firstname", "lastname", "experience", "research", "aboutme"), 'LEFT');
	      $select->join('studdies', 'users.id=studdies.user_id', array("field_of_study", "university"), 'LEFT');
		  $select->where->expression("MATCH(firstname, lastname, experience, research, aboutme) AGAINST (?)", $user);
		  $select->where->equalTo("user_group_id", "4");
		  $select->where->equalTo("users_data.lang", $lang);
		  $select->where->equalTo("studdies.lang", $lang);
		  $select->order("studdies.id DESC");
		  $select->where->equalTo("status", "1");
		  if ($getPaginator) {
		      $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
		      return new Paginator($dbSelector);
		  } else {
		      return $this->tableGateway->selectWith($select)->toArray();
		  }
	    }
	  
	}
    
	public function addUser($postData)
	{
	    $salt = rand(1000000, 9000000);
	    $postData['salt'] = $salt;
	    if($postData['job']){
	        $postData['job'] = $postData['job'];
	    }else{
	        $postData['job'] = ' ';
	    }
	    if($postData['firstname']){
	        $postData['firstname'] = $postData['firstname'];
	    }else{
	        $postData['firstname'] = ' ';
	    }
		if($postData['lastname']){
	        $postData['lastname'] = $postData['lastname'];
	    }else{
	        $postData['lastname'] = ' ';
	    }
	    $this->tableGateway->insert(array(
	        'username' => $postData['username'],
	        'email' => $postData['email'],
	        'phone' => $postData['phone'],
	        'password' => md5(sha1($postData['password']) . $salt),
	        'mobile' => $postData['mobile'],
	        'address' => $postData['address'],
	        'area' => $postData['area'],
	        'district' => $postData['district'],
	        'salt' => $postData['salt'],
	        'user_group_id' => $postData['user_group_id'],
	        'job' => $postData['job'],
	        'firstname' => $postData['firstname'],
	    ));
	    
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function getUserByEmail($email)
	{
	    $select = new Select('users');
	    $select->where->equalTo('email', $email);
	    return $this->tableGateway->selectWith($select)->current();
	}
    
	public function edit($data,$id){
	    $this->tableGateway->update(array(
	        'username'           => $data['username'],
	        'email'         => $data['email'],
	        'phone'        => $data['phone'],
	        'mobile'        => $data['mobile'],
	        'area'        => $data['area'],
	        'district'        => $data['district'],
	        'address'        => $data['address'],
	        'job'        => $data['job'],
	        'description'        => $data['description'],
	        'firstname'        => $data['firstname'],
	    ), array(
	        'id' => $id
	    ));
	    return true;
	}
}