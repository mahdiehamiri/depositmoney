<?php 

namespace Application\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ApplicationTagsIndexTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('application_tags_index', $this->getDbAdapter());
	}
	
	public function fetchAllTags($entityId, $entityType)
	{
		$select = new Select('application_tags_index');
		$select->join('application_tags', 'application_tags.id = application_tags_index.tag_id', array('tag_title'));
		if ($entityId > 0 && is_numeric($entityId)) {
			$select->where->equalTo('entity_id', $entityId);
			$select->where->equalTo('entity_type', $entityType);
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	
	public function insertTagIndex($tagById, $postId, $entityType)
	{
		if ($tagById && $postId && $entityType) {
			$this->tableGateway->insert(array(
				'tag_id'      => $tagById,
				'entity_id'   => $postId,
				'entity_type' => $entityType	
			));
			
			return $this->tableGateway->getLastInsertValue();
		}
	}
	
	public function deleteTags($entityId, $entityType)
	{
		if ($entityId > 0 && is_numeric($entityId)) {
			return $this->tableGateway->delete(array(
					'entity_id'   => $entityId,
					'entity_type' => $entityType
			));
		}
	}
}
