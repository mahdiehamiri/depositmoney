<?php 

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ResetPasswordForm extends Form
{
	public function __construct()
	{
		parent::__construct('resetPasswordForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$email = new Element\Email('email');
		$email->setAttributes(array(
				'id'   		  => '',
				'class' 	  => 'form-control',
		        'placeholder' =>"example@example.com",
		));
		$email->setLabel(__("Email"));
		
		$password = new Element\Password('password');
		$password->setAttributes(array(
				'id'    => '',
				'class' => 'form-control',
		        'placeholder' => __('New password'),
		));
		$password->setLabel(__("New password"));
		
		$passwordRepeat = new Element\Password('password_repeat');
		$passwordRepeat->setAttributes(array(
				'id'    => 'inputSuccess',
				'class' => 'form-control',
		        'placeholder' => __('New password repeat'),
		));
		$passwordRepeat->setLabel(__("New password repeat"));

		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save"));
		$submit->setAttributes(array(
				'id'    => 'register',
				'class' => 'btn btn-primary'
		));

		$this->add($email)
			 ->add($password)
			 ->add($passwordRepeat)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
	             'validators' => array(
	                   array(
			                'name'    => 'EmailAddress'
	                   )
	             ),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => true,
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'password_repeat',
				'required' => true,
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}