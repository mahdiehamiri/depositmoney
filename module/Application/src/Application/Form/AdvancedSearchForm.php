<?php 

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class AdvancedSearchForm extends Form
{
	public function __construct()
	{
		parent::__construct('advancedSearchForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
				'id'	 => 'advanced-search-form'
		));
		
		$text = New Element\Text('text');
		$text->setAttributes(array(
				'id'          => 'search-input-text',
				'class'       => 'form-control',
		));
		$text->setLabel(__("What are you looking for?"));
		
		$FromDate = new Element\Date('from');
		$FromDate->setAttributes(array(
				'id'          => 'search-from-date',
				'class'       => 'form-control'
		));
		$FromDate->setLabel(__('Date From'));
		
		$ToDate = new Element\Date('to');
		$ToDate->setAttributes(array(
				'id'          => 'search-to-date',
				'class'       => 'form-control'
		));
		$ToDate->setLabel(_('Date To'));
		
		$author = new Element\Text('author');
		$author->setAttributes(array(
				'id'          => 'search-author',
				'class'       => 'form-control'
		));
		$author->setLabel(__('Author`s Name'));
		
		
		
		
		$csrf = new Element\Csrf('csrf');
		
		$button = new Element\Submit('submit');
		$button->setAttributes(array(
				'id'    => 'search-button',
				'class' => 'btn btn-primary'
		));
		$button->setValue(_('Search')); 
		
		$this->add($text)
			 ->add($FromDate)
			 ->add($ToDate)
			 ->add($author)
			 ->add($button)
			 ->add($csrf);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'search',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
