<?php 

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Form\Element\Checkbox;

class LoginSigninForm extends Form
{
	public function __construct($action = "/signin")
	{
		parent::__construct('loginForm');
		$this->setAttributes(array(
				'method' => 'post',
		        'action' => $action,
		        'id'     => 'loginForm',
		        'novalidate'=> true

		));

		$username = new Element\Email('username');
		$username->setAttributes(array(
				'id'    	  => '',
				'class'		  => 'form-control validate[required, custom[email]]',
				'placeholder'		  => __("Username"),
		        'data-errormessage-value-missing'=>"Email is required!",
		        'required' => 'required'
		    
		));
		$username->setLabel(__("Username / E-Mail"));
		
		$password = new Element\Password('password');
		$password->setAttributes(array(
				'id'          => '',
				'class' 	  => 'form-control validate[required]',
				'placeholder'		  => __("Password"),
		        'required' => 'required'
		));
		$password->setLabel(__("Password"));

		$rememberMe = new Checkbox("remember_me");
		$rememberMe->setLabel(__("Remember me"));
		$rememberMe->setAttributes(array(
		    'id'          => '',
		    'class' 	  => 'remember-me',
		));
		$csrf = new Element\Csrf("csrf_security_login");
		//$csrf->setAttributes(array('timeout' => 60000));
		$csrf->setOptions(array(
             'csrf_options' => array(
                     'timeout' => 100 * 60,
             )
        ));
		/* $csrf->setCsrfValidatorOptions(
           ['timeout' => 999999999]
        ); */
   

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Login"));
		$submit->setAttributes(array(
				//'id'    => 'submitLogin',
				'class' => 'btn btn-primary'
		));

		$this->add($username)
			 ->add($password)
			 ->add($rememberMe)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'username',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
	             'validators' => array(
	                   array(
			                'name'    => 'EmailAddress'
	                   )
	             ),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => true,
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}