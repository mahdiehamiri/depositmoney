<?php 

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class UserAccountForm extends Form
{
	public function __construct($userInfo)
	{
		parent::__construct('userAccountForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$firstname = new Element\Text('firstname');
		$firstname->setAttributes(array(
				'id'    => 'firstname',
				'class' => 'form-control',
		        'placeholder' => __('First Name'),
		));
		$firstname->setLabel(__("First Name"));

		$lastname = new Element\Text('lastname');
		$lastname->setAttributes(array(
				'id'    => 'lastname',
				'class' => 'form-control',
		        'placeholder' => __('Last Name'),
		));
		$lastname->setLabel('Last Name');
		
		$gender = new Element\Radio('gender');
		$gender->setValueOptions(array(
				'0' => __("Female/"),
				'1' => __("Male"),
		));
		$gender->setAttributes(array(
			'id' 	=> 'gender',
			'class' => ''				
		));
		$gender->setLabel(__("Gender"));
		
		$email = new Element\Email('email');
		$email->setAttributes(array(
				'id' 	=> 'email',
				'class' => 'form-control',
		        'placeholder' =>"example@example.com",
		));
		$email->setLabel(__("Email"));
		
		$password = new Element\Password('password');
		$password->setAttributes(array(
				'id'    => 'password',
				'class' => 'form-control',
		        'placeholder' => __('Edit password'),
		));
		$password->setLabel(__("Edit password"));

// 		$date_of_birth = new Element\Text('date_of_birth');
// 		$date_of_birth->setLabel('تاریخ تولد');
// 		$date_of_birth->setAttributes(array(
// 				'id'    => 'date-of-birth',
// 				'class' => 'form-control'
// 		));
		
// 		$user_url = new Element\Url('user_url');
// 		$user_url->setLabel(_('Site or Blog Address'));
// 		$user_url->setAttributes(array(
// 				'id'    => 'user-url',
// 				'class' => 'form-control'
// 		));
		
		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Edit profile"));
		$submit->setAttributes(array(
				'id'    => 'update-profile',
				'class' => 'btn btn-primary'
		));

		$this->add($firstname)
			 ->add($lastname)
			 ->add($gender)
			 ->add($email)
			 ->add($password)
// 			 ->add($date_of_birth)
// 			 ->add($user_url)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'firstname',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'lastname',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'gender',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}