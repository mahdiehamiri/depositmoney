<?php 

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Application\Form\Element\Mobile;

class ForgotPasswordForm extends Form
{
	public function __construct()
	{
		parent::__construct('forgotPasswordForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$email = new Element\Email('email');
		$email->setAttributes(array(
				'id'      	  => 'inputSuccess',
		        'placeholder' =>"example@example.com",
				'class' 	  => 'form-control',
		));
		$email->setLabel(__("Email"));
		
		$phone = new Mobile('mobile');
		$phone->setAttributes(array(
		    'id'      	  => 'inputSuccess',
		    'placeholder' => "09121234567",
		    'class' 	  => 'form-control',
		));
		$phone->setLabel(__("Mobile"));
		
		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Send password recovery link"));
		$submit->setAttributes(array(
				'id'    => 'forgot-password',
				'class' => 'btn btn-primary'
		));

		$this->add($email)
			 ->add($phone)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	

	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
				'validators' => array(
						array(
								'name'    => 'EmailAddress',
						)
				),
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}