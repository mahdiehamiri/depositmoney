<?php 

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class GetVerifyCodeForm extends Form
{
	public function __construct()
	{
		parent::__construct('getVerifyCodeForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$code = new Element\Text('code');
		$code->setAttributes(array(
				'id'   		  => '',
				'class' 	  => 'form-control',
		        'placeholder' => __('Code'),
		));
		$code->setLabel(__("Code"));
		
		

		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Verify"));
		$submit->setAttributes(array(
				'id'    => 'verify',
				'class' => 'btn btn-secondary'
		));

		$this->add($code)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();

		$inputFilter->add($factory->createInput(array(
				'name'     => 'code',
				'required' => true,
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}