<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;
    
class RequestProjectForm extends Form
{
	public function __construct()
	{
		parent::__construct('requestProjectForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$firstName = new Element\Text('first_name');
		$firstName->setAttributes(array(
		    'id'    => 'first_name',
		    'class' => 'form-control required validate[required]',
		    'placeholder' => __('First Name'),
		    'Lang' => 'fa-IR',
		));
	    $firstName->setLabel(__("First Name"));
		
	    $lastName = new Element\Text('last_name');
	    $lastName->setAttributes(array(
	        'id'    => 'last_name',
	        'class' => 'form-control required validate[required]',
	        'placeholder' => __('Last Name'),
	        'Lang' => 'fa-IR',
	    ));
	    $lastName->setLabel(__("Last Name"));
	    
	    $companyName = new Element\Text('company_name');
	    $companyName->setAttributes(array(
	        'id'    => 'company_name',
	        'class' => 'form-control required validate[required]',
	        'placeholder' => __('Company name'),
	        'Lang' => 'fa-IR',
	    ));
	    $companyName->setLabel(__("Company name"));
	    
	    $email = new Element\Text('email');
	    $email->setAttributes(array(
	        'id'    => 'email',
	        'class' => 'form-control required validate[required, custom[email]]',
	        'placeholder' => 'example@example.com',
	        'dir' => 'ltr',
	    ));
	    $email->setLabel(__("Email"));
	    
	    $tel = new Element\Text('tel');
	    $tel->setAttributes(array(
	        'id'    => 'tel',
	        'class' => 'form-control required validate[required]',
	        'placeholder' => '02122222222',
	    ));
	    $tel->setLabel(__("Phone"));
	    
	    $mobile = new Element\Text('mobile');
	    $mobile->setAttributes(array(
	        'id'    => 'mobile',
	        'class' => 'form-control required validate[required]',
	        'placeholder' =>"09121234567",
	    ));
	    $mobile->setLabel(__("Mobile"));
	    
	    
	    
	    
	    $subject = new Element\Text('subject');
	    $subject->setAttributes(array(
	        'id'    => 'subject',
	        'class' => 'form-control',
	        'placeholder' => __('Subject'),
	        'Lang' => 'fa-IR',
	    ));
	    $subject->setLabel(__("Subject"));
		
	    
	    $requestFile = new Element\File('request_file');
	    $requestFile->setAttributes(array(
	        'id'    => 'fileUpload1'
	    ));
	    $requestFile->setLabel("Project information (Picture, Image, Map, Standard, 3D Model, 2D Model, Other");
	   
	    $projectType = new Element\Select('project_type');
	    $projectType->setAttributes(array(
	        'id' => 'project_type',
	        'class' => 'form-control',
	    ));
	    $projectTypeArray= array(
	        "" => __("Choose an item"),
	        __("Designing") => __("Designing"),
	        __("Reverse Engineering") => __("Reverse Engineering"),
// 	        "طراحی" => "طراحی",
// 	        "مهندسی معکوس" => "مهندسی معکوس"
	    );
	    $projectType->setValueOptions($projectTypeArray);
	    $projectType->setLabel(__("Choose project type"));
	 
	    //---------------------------------------------------------------------
	    
	    $dimensionWay = new Element\Select('dimension_way');
	    $dimensionWay->setAttributes(array(
	        'id' => 'dimension_way',
	        'class' => 'form-control',
	    ));
	    $dimensionWayArray = array(
	        "" => __("Choose an item"),
	        __("3D Scan (Digitizing)") => __("3D Scan (Digitizing)"),
	        "CMM ( Coordinate Measuring Machine)" =>"CMM ( Coordinate Measuring Machine)",
	        __("Laser") => __("Laser"),
	        __("Manual Dimension Measurment") => __("Manual Dimension Measurment"),
	        
// 	        "اسکن سه بعدی (دیجیتایزینگ)" => "اسکن سه بعدی (دیجیتایزینگ)",
// 	        "CMM ( Coordinate Measuring Machine)" => "CMM ( Coordinate Measuring Machine)",
// 	        "لیزر" => "لیزر",
// 	        "ابعاد برداری دستی ( کروکی برداری)" => "ابعاد برداری دستی ( کروکی برداری)"
	    );
	    $dimensionWay->setValueOptions($dimensionWayArray);
	    $dimensionWay->setLabel(__("Choose Dimension Measurment"));
	     
	   
	    
	    //---------------------------------------------------------------------
	    
	    $create3dModel = new Element\Select('create_3d_model');
	    $create3dModel->setAttributes(array(
	        'id' => 'create_3d_model',
	        'class' => 'form-control',
	    ));
	    $create3dModelArray = array(
	        "" => __("Choose an item"),
	        "CATIA" => "CATIA",
	        "Pro Engineer- creo" => "Pro Engineer- creo",
	        "NX -Unigraphics" => "NX -Unigraphics",
	        "SolidWorks" => "SolidWorks",
	        "Inventor" => "Inventor",
	        "AutoCAD" => "AutoCAD",
	        "Rapidform" => "Rapidform",
	        "Geomagic" => "Geomagic",
	        "ArtCAM" => "ArtCAM",
	        "Rhinoceros" => "Rhinoceros",
	        "ProgeCAD" => "ProgeCAD",
	        "Room Arranger" => "Room Arranger",
	        "Takla Structures -Xsteel" => "Takla Structures -Xsteel",
	        "Autodesk Revit" => "Autodesk Revit",
	        "Autodesk Alias" => "Autodesk Alias",
	        "Other" => "Other",
	    );
	    $create3dModel->setValueOptions($create3dModelArray);
	    $create3dModel->setLabel(__("Choose software to use in 3D designing"));
	    
	 
	    //---------------------------------------------------------------------
	    
	    $create2dModel = new Element\Select('create_2d_model');
	    $create2dModel->setAttributes(array(
	        'id' => 'create_2d_model',
	        'class' => 'form-control',
	    ));
	    $create2dModelArray = array(
	        "" => __("Choose an item"),
	        "CATIA" => "CATIA",
	        "Pro Engineer- creo" => "Pro Engineer- creo",
	        "NX -Unigraphics" => "NX -Unigraphics",
	        "SolidWorks" => "SolidWorks",
	        "Inventor" => "Inventor",
	        "AutoCAD" => "AutoCAD",
	        "Rapidform" => "Rapidform",
	        "Geomagic" => "Geomagic",
	        "ArtCAM" => "ArtCAM",
	        "Rhinoceros" => "Rhinoceros",
	        "ProgeCAD" => "ProgeCAD",
	        "Room Arranger" => "Room Arranger",
	        "Takla Structures -Xsteel" => "Takla Structures -Xsteel",
	        "Autodesk Revit" => "Autodesk Revit",
	        "Autodesk Alias" => "Autodesk Alias",
	        "Other" => "Other",
	    );
	    $create2dModel->setValueOptions($create2dModelArray);
	    $create2dModel->setLabel(__("Choose software to use in 2D designing"));
	     
	
	    //---------------------------------------------------------------------
	    
	    $designCast = new Element\Select('design_cast');
	    $designCast->setAttributes(array(
	        'id' => 'design_cast',
	        'class' => 'form-control',
	    ));
	    $designCastArray = array(
	        "" => __("Choose an item"),
	        __("Plastic injection cast") => __("Plastic injection cast"),
	        __("Die cast") => __("Die cast"),
	        __("Gravity cast") => __("Gravity cast"),
	        __("Sand cast") => __("Sand cast"),
	        __("Casting cast") => __("Casting cast"),
	        __("Metal cast") => __("Metal cast"),
	        __("Mandrel & Metrix cast") => __("Mandrel & Metrix cast"),
	        __("Forge cast") => __("Forge cast"),
	        
// 	        __("Choose an item") => __("Choose an item"),
// 	        "قالب تزریق پلاستیک" => "قالب تزریق پلاستیک",
// 	        "قالب دایکست Die cast" => "قالب دایکست Die cast",
// 	        "قالبهای ریژه (Gravity)" => "قالبهای ریژه (Gravity)",
// 	        "قالبهای ماسه ای (Sand cast)" => "قالبهای ماسه ای (Sand cast)",
// 	        "قالب ریختگری" => "قالب ریختگری",
// 	        "قالب فلزی" => "قالب فلزی",
// 	        "قالبهای سنبه و ماتریس" => "قالبهای سنبه و ماتریس",
// 	        "قالب آهنگری (فرج) Forge" => "قالب آهنگری (فرج) Forge"
	    );
	    $designCast->setValueOptions($designCastArray);
	    $designCast->setLabel(__("Choose software to use in 2D design"));
	    
	    $designCastDesc = new Element\Textarea('design_cast_desc');
	    $designCastDesc->setAttributes(array(
	        'id'    => 'design_cast_desc',
	        'class' => 'form-control',
	        'placeholder' => __('Desciption'),
	        'Lang' => 'fa-IR',
	    ));
	    $designCastDesc->setLabel(__("Design Cast Desciption"));
	     
	    //---------------------------------------------------------------------
	   
	    $createCastDesc = new Element\Textarea('create_cast_desc');
	    $createCastDesc->setAttributes(array(
	        'id'    => 'create_cast_desc',
	        'class' => 'form-control',
	        'placeholder' => __('Desciption'),
	        'Lang' => 'fa-IR',
	    ));
	    $createCastDesc->setLabel(__("Cast Desciption"));
	    
	    //---------------------------------------------------------------------
	    
	    $prototypingDesc = new Element\Textarea('prototyping_desc');
	    $prototypingDesc->setAttributes(array(
	        'id'    => 'prototyping_desc',
	        'class' => 'form-control',
	        'placeholder' => __('Desciption'),
	        'Lang' => 'fa-IR',
	    ));
	    $prototypingDesc->setLabel(__("Prototyping Desciption"));
	     
	    //---------------------------------------------------------------------
	    
	    $dimensionControlDesc = new Element\Textarea('dimension_control_desc');
	    $dimensionControlDesc->setAttributes(array(
	        'id'    => 'dimension_control_desc',
	        'class' => 'form-control',
	        'placeholder' => __('Desciption'),
	        'Lang' => 'fa-IR',
	    ));
	    $dimensionControlDesc->setLabel(__("Dimension Control Desciption"));
	     
	    //---------------------------------------------------------------------
	    
	    $productionDesc = new Element\Textarea('production_desc');
	    $productionDesc->setAttributes(array(
	        'id'    => 'production_desc',
	        'class' => 'form-control',
	        'placeholder' => __('Desciption'),
	        'Lang' => 'fa-IR',
	    ));
	    $productionDesc->setLabel(__("Production Desciptions"));
	    
	    //---------------------------------------------------------------------
	    
	    $othersDesc = new Element\Textarea('others_desc');
	    $othersDesc->setAttributes(array(
	        'id'    => 'others_desc',
	        'class' => 'form-control',
	        'placeholder' => __('Desciption'),
	        'Lang' => 'fa-IR',
	    ));
	    $othersDesc->setLabel(__("Other desciption"));
	     
	    //---------------------------------------------------------------------
	     
		$captchaImage  = new Image(array(
		    'font' =>   $_SERVER['DOCUMENT_ROOT'] . "/fonts/arial.ttf",
		    'wordLen' => 4,
		    'timeout' => 300,
		    'width'   =>100,
		    'height'  => 50,
		    '_fsize' => 14,
		    'lineNoiseLevel' => 1 ,
		    'dotNoiseLevel' => 20));
		$captchaImage->setImgDir( $_SERVER['DOCUMENT_ROOT'] . "/captcha");
		$captchaImage->setImgUrl( "/captcha");
		
		$captcha = new Element\Captcha('request_project_myCaptcha');
		
		$captcha->setLabel(__("I'm not robot"));

		$captcha->setOptions( array(
		    'captcha' => $captchaImage,
		));
		$captcha->setAttributes(array(
		    'id'   => "captcha-idrp",
		    'class' => 'form-control',
		    'placeholder' => __("Enter image words here")
		));
		
		$submit = new Element\Submit('request_project_submit');
		
		$submit->setValue(__("Send"));
		
		$submit->setAttributes(array(
				'id'    => 'request_project_submit',
				'class' => 'btn btn-secondary'
		));
		
		$this->add($firstName)
             ->add($lastName)
             ->add($companyName)
             ->add($email)
             ->add($tel)
             ->add($mobile)
             ->add($subject)
             ->add($requestFile)
             ->add($projectType)
             ->add($dimensionWay)
             ->add($create3dModel)
             ->add($create2dModel)
             ->add($designCast)
             ->add($designCastDesc)
             ->add($createCastDesc)
             ->add($prototypingDesc)
             ->add($dimensionControlDesc)
             ->add($productionDesc)
             ->add($othersDesc)
		     ->add($captcha)
			 ->add($submit);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'first_name',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'last_name',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'company_name',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(array(
		    'name'     => 'email',
		    'required' => true,
		    'filters' => array(
		        array(
		            'name' => 'StripTags'
		        )
		    ),
		    'validators' => array(
		        array(
		            'name'    => 'EmailAddress'
		        )
		    ),
		)));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'tel',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'mobile',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'subject',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		
		
		
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'request_file',
		        'required' => false,
		        'allow_empty' => true
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'project_type',
		        'required' => false,
		        'allow_empty' => true,
		    )
		));
		
		
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'dimension_way',
		        'required' => false,
		        'allow_empty' => true,
		    )
		));
		
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'create_3d_model',
		        'required' => false,
		        'allow_empty' => true,
		    )
		));
		
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'create_2d_model',
		        'required' => false,
		        'allow_empty' => true,
		    )
		));
		
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'design_cast',
		        'required' => false,
		        'allow_empty' => true,
		    )
		));
		
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
