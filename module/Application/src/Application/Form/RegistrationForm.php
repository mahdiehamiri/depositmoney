<?php 

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class RegistrationForm extends Form
{
	public function __construct()
	{
		parent::__construct('registrationForm');
		$this->setAttributes(array(
		        'id '    =>  'registrationForm',
				'action' => '/signup',
				'method' => 'post',
		        'class'  => 'registrationForm',
		));

		$email = new Element\Text('email');
		$email->setAttributes(array(
				'id'    => '',
				'class' => 'form-control required validate[required,custom[email]]',
				'placeholder'		  => __("Email"),
		));
		$email->setLabel(__("Email"));
		
		$password = new Element\Password('password');
		$password->setAttributes(array(
				'id'    => 'password',
				'class' => 'form-control required validate[required] password_signup',
				'placeholder'		  => __("Password"),
		));
		$password->setLabel(__("Password"));
		
		$passwordshow = new Element\Checkbox('show_password');
		$passwordshow->setAttributes(array(
				'id'    => 'show_password',
				'class' => 'form-dontrol show_password',
		));
		$passwordshow->setLabel(__("Show password")); 
		
		$rules= new Element\Checkbox('rules');
		$rules->setAttributes(array(
		    'id'    => 'rules',
		    'class' => 'form-dontrol rules_register',
		));
		$rules->setLabel(__("Agree"));
		
		$group= new Element\Checkbox('group');
		$group->setAttributes(array(
		    'id'    => 'group',
		    'class' => 'form-dontrol group',
		));
		$group->setLabel(__("I want to register a group"));

		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Signup"));
		$submit->setAttributes(array(
			'id'    => 'register',
		    'disabled' => 'disabled',
		    'type'    => 'submit',
			'class' => 'btn btn-primary register_button',
		    
		));

		$this->add($email)
			 ->add($password)
			 ->add($passwordshow)
			 ->add($rules)
			 ->add($group)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
	             'validators' => array(
	                   array(
			                'name'    => 'EmailAddress',
	                   )
	             ),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => true,
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
