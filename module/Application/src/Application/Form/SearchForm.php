<?php 

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class SearchForm extends Form
{
	public function __construct()
	{
		parent::__construct('searchForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
				'id'	 => 'searchForm'
		));
		
		$search = New Element\Text('search');
		$search->setAttributes(array(
				'id'          => 'search-input',
				'class'       => 'form-control',
		        'placeholder' => __('Please Enter Your Input'),
		));
		$search->setLabel(__('Please Enter Your Input'));
		
// 		$button = new Element\Button('bsubmit');
// 		$button->setAttributes(array(
// 				'id'    => 'search-button',
// 				'class' => 'btn btn-primary' 
// 		));
// 		$button->setAttribute('onclick','document.getElementById("searchForm").submit()');
// 		$button->setLabel('<i class="fa fa-search"></i>');
// 		$button->setLabelOption('disable_html_escape', true);
		
		$button = new Element\Submit('submit');
		$button->setAttributes(array(
				'id'    => 'search-button',
				'class' => 'btn btn-primary'
		));
		$button->setValue("Search"); 
// 		$button->setLabelOption('disable_html_escape', true);

		$csrf = new Element\Csrf('csrf_security');
		$this->add($search)
			 ->add($button)
			 ->add($csrf);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'search',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
