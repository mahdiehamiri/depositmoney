<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;
    
class RequestTeachingForm extends Form
{
	public function __construct()
	{
		parent::__construct('requestTeachingForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$firstName = new Element\Text('first_name');
		$firstName->setAttributes(array(
		    'id'    => 'first_name',
		    'class' => 'form-control required validate[required]',
		    'placeholder' => __('First Name'),
		    'Lang' => 'fa-IR',
		));
	    $firstName->setLabel(__("First Name"));
		
	    $lastName = new Element\Text('last_name');
	    $lastName->setAttributes(array(
	        'id'    => 'last_name',
	        'class' => 'form-control required validate[required]',
	        'placeholder' => __('Last Name'),
	        'Lang' => 'fa-IR',
	    ));
	    $lastName->setLabel(__("Last Name"));
	    
	    $email = new Element\Text('email');
	    $email->setAttributes(array(
	        'id'    => 'email',
	        'class' => 'form-control required validate[required, custom[email]]',
	        'placeholder' => 'example@example.com',
	        'dir' => 'ltr',
	    ));
	    $email->setLabel(__("Email"));
	    
	    $tel = new Element\Text('tel');
	    $tel->setAttributes(array(
	        'id'    => 'tel',
	        'class' => 'form-control required validate[required]',
	        'placeholder' => '02122222222',
	    ));
	    $tel->setLabel(__("Phone"));
	    
	    $mobile = new Element\Text('mobile');
	    $mobile->setAttributes(array(
	        'id'    => 'mobile',
	        'class' => 'form-control required validate[required]',
	        'placeholder' =>"09121234567",
	    ));
	    $mobile->setLabel(__("Mobile"));
	    
	    $subject = new Element\Text('subject');
	    $subject->setAttributes(array(
	        'id'    => 'subject',
	        'class' => 'form-control',
	        'placeholder' => __('Subject'),
	        'Lang' => 'fa-IR',
	    ));
	    $subject->setLabel(__("Subject"));
	    
	    $provience = new Element\Text('provience');
	    $provience->setAttributes(array(
	        'id'    => 'provience',
	        'class' => 'form-control',
	        'placeholder' => __('Provience'),
	        'Lang' => 'fa-IR',
	    ));
	    $provience->setLabel(__("Provience"));
	    
	    $city = new Element\Text('city');
	    $city->setAttributes(array(
	        'id'    => 'city',
	        'class' => 'form-control',
	        'placeholder' => __('City'),
	        'Lang' => 'fa-IR',
	    ));
	    $city->setLabel(__("City"));
	    
	    $fieldofStudy = new Element\Text('fieldofStudy');
	    $fieldofStudy->setAttributes(array(
	        'id'    => 'fieldofStudy',
	        'class' => 'form-control',
	        'placeholder' => __('Field of Study'),
	        'Lang' => 'fa-IR',
	    ));
	    $fieldofStudy->setLabel(__("Field of Study"));
	   
	    $university = new Element\Text('university');
	    $university->setAttributes(array(
	        'id'    => 'university',
	        'class' => 'form-control',
	        'placeholder' => __('University'),
	        'Lang' => 'fa-IR',
	    ));
	    $university->setLabel(__("University"));
	    
	    
	    $resumeFile = new Element\File('resume_file');
	    $resumeFile->setAttributes(array(
	        'id'    => 'fileUpload1'
	    ));
	    $resumeFile->setLabel(__("Resume"));
	   
	    $cooperationType = new Element\Select('cooperation_type');
	    $cooperationType->setAttributes(array(
	        'id' => 'cooperation_type',
	        'class' => 'form-control',
	    ));
	    $cooperationTypeArray= array(
	        "" => "انتخاب کنید",
	        __("Per project") =>__("Per project"),
	        __("Part time") => __("Part time"),
	        __("Full time") => __("Full time")
// 	        "پروژه ای" => "پروژه ای",
// 	        "پاره وقت" => "پاره وقت",
// 	        "تمام وقت" => "تمام وقت"
	    );
	    $cooperationType->setValueOptions($cooperationTypeArray);
	    $cooperationType->setLabel(__("Choose cooperation type"));
	    
	   
	    
	    $experience = new Element\Textarea('experience');
	    $experience->setAttributes(array(
	        'class' => 'form-control',
	        'id' => 'experience'
	    ));
	    $experience->setLabel(__("Work Experience"));
	    
	    $research = new Element\Textarea('research');
	    $research->setAttributes(array(
	        'class' => 'form-control',
	        'id' => 'research'
	    ));
	    $research->setLabel(__("Research Activities"));
	    
	    $aboutme = new Element\Textarea('aboutme');
	    $aboutme->setAttributes(array(
	        'class' => 'form-control',
	        'id' => 'aboutme'
	    ));
	    $aboutme->setLabel(__("Biography"));
	    
	  
	    $teachingStatus = new Element\Select('teaching_status');
	    $teachingStatus->setAttributes(array(
	        'id' => 'teaching_status',
	        'class' => 'form-control',
	    ));
	    $teachingStatusArray = array(
	        '' => __('Please choose'),
	        __("I reside in Tehran") => __("I reside in Tehran"),
	        __("I do not live in Tehran, but to record videos I can come to Tehran") => __("I do not live in Tehran, but to record videos I can come to Tehran"),
	        __("I do not live in Tehran, but I can record videos and send them remmotely") => __("I do not live in Tehran, but I can record videos and send them remmotely"),
	        
// 	        "ساکن تهران هستم" => "ساکن تهران هستم",
// 	        "ساکن تهران نیستم ولی برای ضبط میتوانم به تهران بیایم" => "ساکن تهران نیستم ولی برای ضبط میتوانم به تهران بیایم",
// 	        "ساکن تهران نیستم ولی میتوانم ویدئوها را ضبط کرده و از راه دور ارسال کنم" => "ساکن تهران نیستم ولی میتوانم ویدئوها را ضبط کرده و از راه دور ارسال کنم",
	        
	       
	    );
	    $teachingStatus->setValueOptions($teachingStatusArray);
	    $teachingStatus->setLabel(__("Choose teaching status"));
	     
	    $othersDesc = new Element\Textarea('others_desc');
	    $othersDesc->setAttributes(array(
	        'id'    => 'others_desc',
	        'class' => 'form-control',
	        'placeholder' => __('Desciption'),
	        'Lang' => 'fa-IR',
	    ));
	    $othersDesc->setLabel(__("Other desciption"));
	     
	    //---------------------------------------------------------------------
	     
		$captchaImage  = new Image(array(
		    'font' =>   $_SERVER['DOCUMENT_ROOT'] . "/fonts/arial.ttf",
		    'wordLen' => 4,
		    'timeout' => 300,
		    'width'   =>100,
		    'height'  => 50,
		    '_fsize' => 14,
		    'lineNoiseLevel' => 1,
		    'dotNoiseLevel' => 20));
		$captchaImage->setImgDir( $_SERVER['DOCUMENT_ROOT'] . "/captcha");
		$captchaImage->setImgUrl( "/captcha");
		
		$captcha = new Element\Captcha('request_teaching_myCaptcha');
		
		$captcha->setLabel(__("I'm not robot"));

		$captcha->setOptions( array(
		    'captcha' => $captchaImage,
		));
		$captcha->setAttributes(array(
		    'id'   => "captcha-idrt",
		    'class' => 'form-control',
		    'placeholder' => __("Enter image words here")
		));
		
		$submit = new Element\Submit('request_teaching_submit');
		
		$submit->setValue(__("Send"));
		
		$submit->setAttributes(array(
				'id'    => 'request_teaching_submit',
				'class' => 'btn btn-secondary'
		));
		
		$this->add($firstName)
             ->add($lastName)
             ->add($email)
             ->add($tel)
             ->add($mobile)
             ->add($subject)
             ->add($resumeFile)
             ->add($provience)
             ->add($city)
             ->add($fieldofStudy)
             ->add($university)
             ->add($cooperationType)
             ->add($experience)
             ->add($research)
             ->add($aboutme)
             ->add($teachingStatus)
             ->add($othersDesc)
		     ->add($captcha)
			 ->add($submit);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'first_name',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'last_name',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		
		$inputFilter->add($factory->createInput(array(
		    'name'     => 'email',
		    'required' => true,
		    'filters' => array(
		        array(
		            'name' => 'StripTags'
		        )
		    ),
		    'validators' => array(
		        array(
		            'name'    => 'EmailAddress'
		        )
		    ),
		)));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'tel',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'mobile',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'subject',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));		
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'resume_file',
		        'required' => false,
		        'allow_empty' => true
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'teaching_status',
		        'required' => true,
		    )
		));
		
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
