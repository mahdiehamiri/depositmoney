<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;
    
class ExecutiveCooperationForm extends Form
{
	public function __construct()
	{
		parent::__construct('requestTeachingForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$firstName = new Element\Text('first_name');
		$firstName->setAttributes(array(
		    'id'    => 'first_name',
		    'class' => 'form-control required validate[required]',
		    'placeholder' => __('First Name'),
		    'Lang' => 'fa-IR',
		));
	    $firstName->setLabel(__("First Name"));
		
	    $lastName = new Element\Text('last_name');
	    $lastName->setAttributes(array(
	        'id'    => 'last_name',
	        'class' => 'form-control required validate[required]',
	        'placeholder' => __('Last Name'),
	        'Lang' => 'fa-IR',
	    ));
	    $lastName->setLabel(__("Last Name"));
	    
	    $email = new Element\Text('email');
	    $email->setAttributes(array(
	        'id'    => 'email',
	        'class' => 'form-control required validate[required, custom[email]]',
	        'placeholder' => 'example@example.com',
	        'dir' => 'ltr',
	    ));
	    $email->setLabel(__("Email"));
	    
	    $tel = new Element\Text('tel');
	    $tel->setAttributes(array(
	        'id'    => 'tel',
	        'class' => 'form-control required validate[required]',
	        'placeholder' => '02122222222',
	    ));
	    $tel->setLabel(__("Phone"));
	    
	    $mobile = new Element\Text('mobile');
	    $mobile->setAttributes(array(
	        'id'    => 'mobile',
	        'class' => 'form-control required validate[required]',
	        'placeholder' =>"09121234567",
	    ));
	    $mobile->setLabel(__("Mobile"));
	    
	    $subject = new Element\Text('subject');
	    $subject->setAttributes(array(
	        'id'    => 'subject',
	        'class' => 'form-control',
	        'placeholder' => __('Subject'),
	        'Lang' => 'fa-IR',
	    ));
	    $subject->setLabel(__("Subject"));
	    
	    $provience = new Element\Text('provience');
	    $provience->setAttributes(array(
	        'id'    => 'provience',
	        'class' => 'form-control',
	        'placeholder' => __('Provience'),
	        'Lang' => 'fa-IR',
	    ));
	    $provience->setLabel(__("Provience"));
	    
	    $city = new Element\Text('city');
	    $city->setAttributes(array(
	        'id'    => 'city',
	        'class' => 'form-control',
	        'placeholder' => __('City'),
	        'Lang' => 'fa-IR',
	    ));
	    $city->setLabel(__("City"));
	    
	    $fieldofStudy = new Element\Text('fieldofStudy');
	    $fieldofStudy->setAttributes(array(
	        'id'    => 'fieldofStudy',
	        'class' => 'form-control',
	        'placeholder' => __('Field of Study'),
	        'Lang' => 'fa-IR',
	    ));
	    $fieldofStudy->setLabel(__("Field of Study"));
	   
	    $university = new Element\Text('university');
	    $university->setAttributes(array(
	        'id'    => 'university',
	        'class' => 'form-control',
	        'placeholder' => __('University'),
	        'Lang' => 'fa-IR',
	    ));
	    $university->setLabel(__("University"));
	    
	    
	    $resumeFile = new Element\File('resume_file');
	    $resumeFile->setAttributes(array(
	        'id'    => 'fileUpload1'
	    ));
	    $resumeFile->setLabel(__("Resume"));
	   
	    $cooperationType = new Element\Select('cooperation_type');
	    $cooperationType->setAttributes(array(
	        'id' => 'cooperation_type',
	        'class' => 'form-control',
	    ));
	    $cooperationTypeArray= array(
	        "" => __("Choose an item"),
	        __("Part time") => __("Part time"),
	        __("Full time") => __("Full time")
	    );
	    $cooperationType->setValueOptions($cooperationTypeArray);
	    $cooperationType->setLabel(__("Choose cooperation type"));
	    
	   
	    
	    $experience = new Element\Textarea('experience');
	    $experience->setAttributes(array(
	        'class' => 'form-control',
	        'id' => 'experience'
	    ));
	    $experience->setLabel(__("Work Experience"));
	    
	    $research = new Element\Textarea('research');
	    $research->setAttributes(array(
	        'class' => 'form-control',
	        'id' => 'research'
	    ));
	    $research->setLabel(__("Research Activities"));
	    
	    $aboutme = new Element\Textarea('aboutme');
	    $aboutme->setAttributes(array(
	        'class' => 'form-control',
	        'id' => 'aboutme'
	    ));
	    $aboutme->setLabel(__("Biography"));
	    
	  
	    $cooperationStatus = new Element\Select('cooperation_status');
	    $cooperationStatus->setAttributes(array(
	        'id' => 'teaching_status',
	        'class' => 'form-control required validate[required]',
	    ));
	    $cooperationStatusArray = array(
	        "" => __("Choose an item"),
	        __("I can just cooperate remotely") => __("I can just cooperate remotely"),
	        __("I can cooperate in Tehran in office") => __("I can cooperate in Tehran in office"),
	        __("I can cooperate both in office & remotly") => __("I can cooperate both in office & remotly"),
// 	        "فقط دورکاری مناسب و ممکن است." => "فقط دورکاری مناسب و ممکن است.",
// 	        "فقط همکاری حضوری در تهران مناسب است." => "فقط همکاری حضوری در تهران مناسب است.",
// 	        "هم دورکاری و هم همکاری حضوری در تهران برای من مناسب می باشد." => "هم دورکاری و هم همکاری حضوری در تهران برای من مناسب می باشد."
	    );
	    $cooperationStatus->setValueOptions($cooperationStatusArray);
	    $cooperationStatus->setLabel(__("Choose cooperation type"));
	     
	    
	    $executiveFields = new Element\Select('executive_fields');
	    $executiveFields->setAttributes(array(
	        'id' => 'executive_fields',
	        'class' => 'form-control required validate[required]',
	        'multiple' => 'multiple',
	    ));
	    $executiveFieldsArray = array(
	        __("Edit Video") => __("Edit Video"),
	        __("Computer Graphics") => __("Computer Graphics"),
	        __("Programming & Web Development") => __("Programming & Web Development"),
	        __("Mobile Programming") => __("Mobile Programming"),
	        __("Blog Specialized Author") => __("Blog Specialized Author"),
// 	        "ویرایش و تدوین ویدئو" => "ویرایش و تدوین ویدئو",
// 	        "گرافیک کامپیوتری" => "گرافیک کامپیوتری",
// 	        "برنامه نویسی و طراحی وب" => "برنامه نویسی و طراحی وب",
// 	        "برنامه نویسی موبایل" => "برنامه نویسی موبایل",
// 	        "نویسنده مطالب تخصصی وبلاگی" => "نویسنده مطالب تخصصی وبلاگی"
	    );
	    $executiveFields->setValueOptions($executiveFieldsArray);
	    $executiveFields->setLabel(__("Choose executive field"));
	    
	    $technicalFields = new Element\Select('technical_fields');
	    $technicalFields->setAttributes(array(
	        'id' => 'technical_fileds',
	        'class' => 'form-control required validate[required]',
	        'multiple' => 'multiple',
	    ));
	    $technicalFieldsArray = array(
	        __("General Cooperation Position") => __("General Cooperation Position"),
	        __("Edit Video Cooperation - Remotely") => __("Edit Video Cooperation - Remotely"),
	        __("Educational Products Marketing") => __("Educational Products Marketing"),
	        __("Sales Manager & Customer Relationship Officer") => __("Sales Manager & Customer Relationship Officer"),
	        __("Secretary") => __("Secretary"),
	        __("Trainee Position") => __("Trainee Position"),
	        __("Translation") => __("Translation"),
	        __("Social Media Manager") => __("Social Media Manager"),
// 	        "موقعیت همکاری عمومی" => "موقعیت همکاری عمومی",
// 	        "موقعیت همکاری ویرایش ویدئو – دورکاری" => "موقعیت همکاری ویرایش ویدئو – دورکاری",
// 	        "بازاریابی و مارکتینگ محصولات آموزشی" => "بازاریابی و مارکتینگ محصولات آموزشی",
// 	        "مسئول و مدیر فروش و ارتباط با مخاطبین" => "مسئول و مدیر فروش و ارتباط با مخاطبین",
// 	        "منشی و امور دفتری" => "منشی و امور دفتری",
// 	        "موقعیت کارآموزی" => "موقعیت کارآموزی",
// 	        "ترجمه متون" => "ترجمه متون",
 	        "مدیریت شبکه های اجتماعی مانند فیس بوک، اینستاگرام، توییتر و ...." => "مدیریت شبکه های اجتماعی مانند فیس بوک، اینستاگرام، توییتر و ...."
	    );
	    $technicalFields->setValueOptions($technicalFieldsArray);
	    $technicalFields->setLabel(__("Choose technical fields"));
	    
	    $othersDesc = new Element\Textarea('others_desc');
	    $othersDesc->setAttributes(array(
	        'id'    => 'others_desc',
	        'class' => 'form-control',
	        'placeholder' => __('Desciption'),
	        'Lang' => 'fa-IR',
	    ));
	    $othersDesc->setLabel(__("Other desciption"));
	     
	    //---------------------------------------------------------------------
	     
		$captchaImage  = new Image(array(
		    'font' =>   $_SERVER['DOCUMENT_ROOT'] . "/fonts/arial.ttf",
		    'wordLen' => 4,
		    'timeout' => 300,
		    'width'   =>100,
		    'height'  => 50,
		    '_fsize' => 14,
		    'lineNoiseLevel' => 1,
		    'dotNoiseLevel' => 20));
		$captchaImage->setImgDir( $_SERVER['DOCUMENT_ROOT'] . "/captcha");
		$captchaImage->setImgUrl( "/captcha");
		
		$captcha = new Element\Captcha('request_teaching_myCaptcha');
		
		$captcha->setLabel(__("I'm not robot"));

		$captcha->setOptions( array(
		    'captcha' => $captchaImage,
		));
		$captcha->setAttributes(array(
		    'id'   => "captcha-idrt",
		    'class' => 'form-control',
		    'placeholder' => __("Enter image words here")
		));
		
		$submit = new Element\Submit('request_teaching_submit');
		
		$submit->setValue(__("Send"));
		
		$submit->setAttributes(array(
				'id'    => 'request_teaching_submit',
				'class' => 'btn btn-secondary'
		));
		
		$this->add($firstName)
             ->add($lastName)
             ->add($email)
             ->add($tel)
             ->add($mobile)
             ->add($subject)
             ->add($resumeFile)
             ->add($provience)
             ->add($city)
             ->add($fieldofStudy)
             ->add($university)
             ->add($cooperationType)
             ->add($experience)
             ->add($research)
             ->add($aboutme)
             ->add($cooperationStatus)
             ->add($executiveFields)
             ->add($technicalFields)
             ->add($othersDesc)
		     ->add($captcha)
			 ->add($submit);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'first_name',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'last_name',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		
		$inputFilter->add($factory->createInput(array(
		    'name'     => 'email',
		    'required' => true,
		    'filters' => array(
		        array(
		            'name' => 'StripTags'
		        )
		    ),
		    'validators' => array(
		        array(
		            'name'    => 'EmailAddress'
		        )
		    ),
		)));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'tel',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'mobile',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'subject',
		        'required' => true,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));		
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'resume_file',
		        'required' => false,
		        'allow_empty' => true
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'cooperation_status',
		        'required' => true,
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'executive_fields',
		        'required' => true,
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'technical_fields',
		        'required' => true,
		    )
		));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
