<?php

namespace Application\Registry;

use Zend\ServiceManager\ServiceManager;

class Registry {
	protected $serviceManager;
	public $messages;
	public $extraMessages;
	public function __construct(ServiceManager $serviceManager) {
		$this->serviceManager = $serviceManager;
	}
	
	/**
	 * For using in baseModel
	 * When redirectTo() calls with null message, extra messages will be send to controller setMessage
	 *
	 * @param string $type        	
	 * @param string $message        	
	 */
	public function setExtraMessage($type, $message) {
		$this->extraMessages [$type] = $message;
	}
	
	/**
	 * Set message for using in view scripts
	 *
	 * @param string $type        	
	 * @param string $message        	
	 * @param string $caller        	
	 * @param number $priority        	
	 */
	public function setViewMessage($type, $message, $caller = "controller") {
		$this->messages [$caller] = "<div class='alert alert-$type'>$message</div>";
	}
	public function getViewMessage($asArray = false) {
		if ($asArray) {
			return $this->messages;
		} else {
			if ($this->messages) {
				foreach ( $this->messages as $caller => $message ) {
					return $message;
				}
			}
		}
		return;
	}
	public function takingPartA() {
 
		return array (
				1 => __("allowance fees for training classes, according to the curriculum"),
				2 => __("allowance buy construction"),
				3 => __("Gift Cards Bank"),
				4 => __("Book of exquisite music"),
				5 => __("tourist trips Touring"),
				6 => __("now on the run, Fstyval¬Ha and music seminars abroad"),
				7 => __("grant preparation workshop and studio equipment"),
				8 => __("allowance provided for performers"),
				9 => __("Book of exquisite music") 
		);
	}
	public function takingPartD() {
		return array (
				0 => __("(preparatory courses (including courses a semester, per person, semester"),
				1 => __("secondary school (including semester courses, the cost of each semester") 
		);
	}
	public function progressivePeriod() {
		return array (
				1 => __("Playing"),
				2 => __("readers"),
				3 => __("compose"),
				4 => __("Orchestra"),
				5 => __("Planning and Management orchestra"),
				6 => __("Music Videos") 
		);
	}
	public function takingPartE() {
		return array (
				"0" => __("typical instrument suitable for playing up to secondary school"),
				1 => __("professional construction;") 
		);
	}
	public function takingPartI() {
		return array (
				1 => __("solo music albums"),
				2 => __("Philharmonic music albums Karaj"),
				3 => __("concert of music"),
				4 => __("dramatic musical and video projects") 
		);
	}
	public function takingPartL() {
		return array (
				1 => __("Karaj Philharmonic Orchestra"),
				2 => __("Branch Chamber Orchestra"),
				3 => __("Branch Youth Orchestra"),
				4 => __("Choral conductor Karaj"),
				5 => __("Traditional Music Orchestra Karaj"),
				6 => __("Orchestras people") 
		);
	}
	public function registrationKind() {
		return array (
				1 => __("Official"),
				2 => __("Temporary"),
				3 => __("Guest") 
		);
	}
	public function sampleSubject() {
 
		return array(
		    '0' => __('website'),
		    '1' => __('Seo'),
		    '2' => __(' Content'),
		);
	}
	public function otherField() {
		return array (
			"taking_part_b2" => __("Musicians on the basis of hours of participation in payment of wages monthly"),
			"taking_part_c3" => __("Funding for the annual membership program for people interested Orchestra and Orchestra, charities and those with limited financial means"),
			"taking_part_f6" => __("Moral and material support of street musicians"),
			"taking_part_g7" => __("Grants to artists, music play that voluntary nursing homes, charities, run programs addressing."),
			"taking_part_h8" => __("Participation and investment to create technical spaces, including spaces music studio, sound recording equipment"),
			"taking_part_j10" => __("Participation in performances of popular music, to create intimate spaces for music among people through holding meetings orchestral music (negotiable and calculated)"),
			"taking_part_k11" => __("Nurseries Philharmonic Security benefits Karaj, including supplying all kinds of mechanism accessories"),
			"taking_part_m13" => __("Participation in Research Projects Music"),
			"taking_part_n14" => __("Plan to participate in music therapy"),
			"taking_part_o15" => __("Participation in setting up specialized music library"),
			"taking_part_p16" => __("Participation launch Music Museum, Documents, Photos, construction"),
			"taking_part_q17" => __("Participation in commissioning, maintenance and development of the official website Orchestra") 
		);
	}
 
}