<?php
namespace Application\Helper;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Adapter\Adapter;
use Smspanel\Model\LogTable;
use Zend\Http\Response;
use Zend\EventManager\EventInterface;
use Smspanel\Model\ActivityLogsTable;
use Zend\ServiceManager\ServiceLocatorInterface;
use Smspanel\Model\TransferCreditTable;
use Smspanel\Model\AccountingLogsTable;
use Application\Model\ApplicationEntityViewsTable;
use Shop\Model\ShopProductsTable;

class LogListener implements ListenerAggregateInterface
{

    protected $adapter;

    protected $listeners = array();

    public function attach(EventManagerInterface $events)
    {
        $sharedEvents = $events->getSharedManager();
        $this->listeners[] = $sharedEvents->attach('*', 'logMe', array(
            $this,
            'addLog'
        ), 100);
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }

    public function addLog(EventInterface $e)
    {
        $params = $e->getParams();
        $applicationEntityViewsTable = new ApplicationEntityViewsTable($this->services);
        $params["entity_type"] = (isset($params["entity_type"])?$params["entity_type"]:null);
        $params["entity_id"] = (isset($params["entity_id"])?$params["entity_id"]:null);
        $params["user_id"] = (isset($params["user_id"])?$params["user_id"]:null);
        $result = $applicationEntityViewsTable->insertHit($params["entity_type"], $params["entity_id"], $params["user_id"]);
        if ($params["entity_type"] == "product" && $result) {
            $shopProductsTable = new ShopProductsTable($this->services);
            $shopProductsTable->changeVisitCount($params["entity_id"] , 1);
        }
        
    }

    
//     public function getAdapter()
//     {
//         $config = $this->services->get('config');
//         $adapter = new Adapter($config['db']);
//         return $adapter ;   
//     }
    
}
?>