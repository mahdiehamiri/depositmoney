<?php 

namespace Application\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class FormGenerator extends AbstractPlugin 
{
	public $minSize = 1;  //KB
	public $maxSize = 10240; //KB
	
	public $minPublicSize = 1; //KB
	public $maxPublicSize = 4096000; //KB
	
	public $basePath = '/uploads/shop/';
	public $extensions = array('jpg', 'png', 'gif', 'jpeg');
	public $mimeTypes = array(
			'image/gif',
			'image/jpg',
			'image/jpeg',
			'image/png',
	);
	public $publicMimeTypes = array(
    	    'image/gif',
    	    'image/jpg',
    	    'image/jpeg',
    	    'image/png',
			'audio/mpeg',
			'application/pdf',
	);
	public $publicFileExtensions = array('.pdf', '.mp3', '.mp4', '.txt', '.docx');
	
	
	private function clean($value)
	{
		return strip_tags($value);	
	}
	
	private function getCurrentValue($fieldName, $type, $currentInfo, $defaultValue = null)
	{
		// TODO Security Check
		$value = isset($_POST[$fieldName]) ? $this->clean($_POST[$fieldName]) : null;
		if ($value !== null)
			return $value;
		
		if ($currentInfo) {
			foreach ($currentInfo as $item) {
				if (@$item['type'] == $type && @$item['product_attribute_name'] == $fieldName) {
					return $item['value'];
				}
			}
		}
		return $defaultValue;
	}
	
	public function getFieldType($fieldName, $allAttributes)
	{
		if (is_array($allAttributes) == true) {
			foreach ($allAttributes as $item) {
				$item = @$item[0];
				if (@$item['developer_name'] == $fieldName) {
					return  $item['type'];
				}
			}
			return null;
		} else {
			return null;
		}
	}
	
	public function _isForced($fieldName, $allAttributes)
	{
	   if (is_array($allAttributes)) {
			foreach ($allAttributes as $item) {
				if (@$item[0]['developer_name'] == $fieldName && @$item[0]['is_forced'] == 1 ) {
					return  true;
				}
			}
			return false;
		} else {
			return false;
		}
	}
	
	public function _isForced_old($fieldName, $currentData)
	{
		if (is_array($currentData)) {
			foreach ($currentData as $item) {
				if (@$item['product_attribute_name'] == $fieldName && @$item['is_forced'] == 1 ) {
					return  true;
				}
			}
			return false;
		} else {
			return false;
		}
	}
	
	public function _isValid($fieldName, $allAttributes, $validData)
	{
		if ($this->_isForced($fieldName, $allAttributes) && empty($validData[$fieldName])) {
			return false;
		}
		return true;
	}
	
	public function isValid($allAttributes, $currentInfo, $validData, $files = null)
	{
		$errors = array();
		foreach ($validData as $name => $validDatum) {
			$temp = $this->_isValid($name, $allAttributes, $validData);
			if ($temp === false) {
				$errors[] = array($name . ' field is mandatory');
			} 
		}
		if ($files) {
			if (is_object($files)) {
				$files = $files->toArray();
			}
			foreach ($files as $key => $value) {
				$fieldType = $this->getFieldType($key, $allAttributes);
				
				switch ($fieldType) {
					case 'image_file':
						$isForced = $this->_isForced($key, $allAttributes);
						if ($isForced  && empty($files[$key][0]['name'])) {
							$errors[] = array($key . ' field is mandatory');
						} elseif(!$isForced && count($files[$key]) > 0 && !empty($files[$key][0]['name'])) {
							if ($value && is_array($value)) {
								foreach ($value as $singleImageInfo) {
									if (!in_array($singleImageInfo['type'], $this->mimeTypes)) {
										$errors[] = array($key . ' mime type not valid');
									}
									if (($singleImageInfo['size'] / 1024) < $this->minSize || ($singleImageInfo['size'] / 1024) > $this->maxSize) {
										$errors[] = array($key . ' file size is not in the valid range');
									}
									if ($singleImageInfo['error'] != 0) {
										$errors[] = array($key . ' unable to upload file');
						
									}
								}
							}
						}
						break;
					case 'public_file':
						$isForced = $this->_isForced($key, $allAttributes);
						if ($isForced && empty($files[$key][0]['name'])) {
							$errors[] = array($key . ' field is mandatory');
						} elseif(!$isForced && count($files[$key]) > 0 && !empty($files[$key][0]['name'])) {
							if ($value && is_array($value)) {
								foreach ($value as $singleImageInfo) {
									if (!in_array($singleImageInfo['type'], $this->publicMimeTypes)) {
										$errors[] = array($key . ' mime type not valid');
									}
									if (($singleImageInfo['size'] / 1024) < $this->minPublicSize || ($singleImageInfo['size'] / 1024) > $this->maxPublicSize) {
										$errors[] = array($key . ' file size is not in the valid range');
									}
									if ($singleImageInfo['error'] != 0) {
										$errors[] = array($key . ' unable to upload file');
									}
								}
							}
						}
						break;
					default:
						break;
				}				
			}
		}
		return $errors;
	}
	
	public function generateForm($input, $name, $csrf, $currentInfo = false, $action = '', $method = 'POST', $enctype = 'multipart/form-data', $includeFormTag = true, $includeCSRF = true, $includeSubmit = true)
	{
		if ($input && $name) {
		    if ($includeFormTag) {
			     $str = '<form action="' . $action . '" method="' . $method . '" name="' . $name . '" enctype="' . $enctype . '">' ;
		    } else {
		        $str = '';
		    }
		  //  var_dump($input);die;
			foreach ($input as $piece) {
				if($piece[0]['index'] === 'radio-button_options') {
					$str .= $this->createRadioTag($piece, $currentInfo);
				} elseif ($piece[0]['index'] === 'select_options') {
					$str .= $this->createSelectTag($piece, $currentInfo);
				} elseif ($piece[0]['index'] === 'text_options') {
					$str .= $this->createTextTag($piece, $currentInfo);
				} elseif ($piece[0]['index'] === 'textarea_options') {
					$str .= $this->createTextareaTag($piece, $currentInfo);
				} elseif ($piece[0]['index'] === 'image_file_options') {
					$str .= $this->createFileTag($piece, $currentInfo);
				} 
				elseif ($piece[0]['index'] === 'public_file_options') {
					$str .= $this->createFileTag($piece, $currentInfo);
				}
			}
			if ($includeCSRF) {
			    $str .= '<input required="required" type="hidden" name="csrf" value="' . $csrf . '">';
			}
			if ($includeSubmit) {
			    $str .= '<input required="required" type="submit" name="submit" id="submit" class="btn btn-primary" value="'. __('Save').'"></input>';
			}
			if ($includeFormTag) {
			    $str .= '</form>';
			}
			return $str;
		}
	}

	public function createSelectTag($input, $currentInfo)
	{
		$fieldName = $input[0]['field_name'];
		if ($input) {
			$select = '<div class="form-group ">';
			$select .= '<label class="control-label">' . $input[0]['name'] . '</label>';
			$select .= ($input[0]['mandatory'] ? '<span class="mandetory-field"></span>' : '');
			$class = ($input[0]['mandatory'] ? 'validate[required]' : '');
			$required = ($input[0]['mandatory'] ? 'required="required"' : '');
			$select .= '<select ' . $required .' name="'. $fieldName .'" class="form-generator-select-tag form-control "' . $class . ' id="form-generator-select-'. $fieldName .'">';
			if ($currentInfo) {
				$currentValue = $this->getCurrentValue($fieldName, 'select', $currentInfo);
				foreach ($input as $selectOption) {
					if ($currentValue == trim($selectOption['value'])) {
						$select .= '<option selected class="form-generator-select-tag-option" value="'. $selectOption['value'] .'">'. $selectOption['value'] .'</option>';						 
					} else {
						$select .= '<option class="form-generator-select-tag-option" value="'. $selectOption['value'] .'">'. $selectOption['value'] .'</option>';
					}
				}
			} else {
				foreach ($input as $selectOption) {
					$select .= '<option class="form-generator-select-tag-option" value="'. $selectOption['value'] .'">'. $selectOption['value'] .'</option>';
				}
			}
			$select .= '</select>';
			if ($input[0]['mandatory']) {
			  $select .= '<span class="required-mark">*</span>';
			}
			$select .= '</div>';
			return $select;
		}
	}
	
	public function createRadioTag($input, $currentInfo)
	{
 
		$fieldName = $input[0]['field_name'];
		if ($input) {
			$radio = '<div class="form-group">';
			$radio .= '<label class="control-label">' . $input[0]['name'] . '</label>';
			$radio .= ($input[0]['mandatory'] ? '<span class="mandetory-field"></span>' : '');
			$class = ($input[0]['mandatory'] ? 'validate[required]' : ''); 
			$required = ($input[0]['mandatory'] ? 'required="required"' : '');
			$radio .= '<div class="form-generator-radio-tag" id="form-generator-radio-'. $fieldName .'">';
			if ($currentInfo) {
				$currentValue = $this->getCurrentValue($fieldName, 'radio-button', $currentInfo);
				
				foreach ($input as $radioItem) {
					if ($currentValue == trim($radioItem['value'])) {
						$radio .= '<input ' . $required . ' checked="checked" class="' . $class . '" type="radio" name="'.$input[0]['field_name'].'" value="'. $radioItem['value'] .'">'. $radioItem['value'] .'<br>';
					} else {
						$radio .= '<input '. $required . ' class="' . $class . '" type="radio" name="'.$input[0]['field_name'].'" value="'. $radioItem['value'] .'">'. $radioItem['value'] .'<br>';						
					}
				}
			} else {
				foreach ($input as $radioItem) {
					$radio .= '<input '. $required . ' class="' . $class . '" type="radio" name="'.$input[0]['field_name'].'" value="'. $radioItem['value'] .'">'. $radioItem['value'] .'<br>';
				}
			}			
			$radio .= '</div>';
			if ($input[0]['mandatory']) {
				$radio .= '<span class="required-mark">*</span>';
			}
			$radio .= '</div>';
			return $radio;
		}
	}
	
	
	public function createTextTag($input, $currentInfo)
	{
		$fieldName = $input[0]['field_name'];
		if ($input) {
			$text = '<div class="form-group">';
			$text .= '<label class="control-label">' . $input[0]['name'] . '</label>';
			$text .= ($input[0]['mandatory'] ? '<span class="mandetory-field"></span>' : '');
			$class = ($input[0]['mandatory'] ? 'validate[required]' : '');
			$kind = (isset($input[0]['kind'] )&& $input[0]['kind'] == 1 ? 'varchar' : 'int');
			$required = ($input[0]['mandatory'] ? 'required="required"' : '');
			if ($currentInfo) {
				$currentValue = $this->getCurrentValue($fieldName, 'text', $currentInfo);
				$text .= '<input '. $required . ' type="text"  data-kind = "'.$kind.'"  name="' . $input[0]['field_name'] . '" id=form-generator-text-"' .  $input[0]['field_name'] . '" class="checkkind form-control ' . $class . '" value="'. $currentValue .'">';				
			} else {
				$text .= '<input '. $required . ' type="text"  data-kind = "'.$kind.'"  name="' . $input[0]['field_name'] . '" id=form-generator-text-"' .  $input[0]['field_name'] . '" class="checkkind form-control ' . $class . '">';
			}
			if ($input[0]['mandatory']) {
				$text .= '<span class="required-mark">*</span>';
			}
			$text .= '</div>';
			return $text;
		}
	}
	
	public function createTextareaTag($input, $currentInfo)
	{
		$fieldName = $input[0]['field_name'];
		if ($input) {
			$textarea = '<div class="form-group">';
			$textarea .= '<label class="control-label">' . $input[0]['name'] . '</label>';
			$textarea .= ($input[0]['mandatory'] ? '<span class="mandetory-field"></span>' : '');
			$class = ($input[0]['mandatory'] ? 'validate[required]' : '');
			$required = ($input[0]['mandatory'] ? 'required="required"' : '');
			if ($currentInfo) {
				$currentValue = $this->getCurrentValue($fieldName, 'textarea', $currentInfo);
				$textarea .= '<textarea '. $required . ' name="'. $input[0]['field_name'] .'" id="form-generator-text-' . $input[0]['field_name'] . '" class="form-control ' . $class . '">' . $currentValue . '</textarea>';				
			} else {
				$textarea .= '<textarea '. $required . ' name="'. $input[0]['field_name'] .'" id="form-generator-text-' . $input[0]['field_name'] . '" class="form-control ' . $class . '">' . $input[0]['value'] . '</textarea>';
			}
			if ($input[0]['mandatory']) {
				$textarea .= '<span class="required-mark">*</span>';
			}
			$textarea .= '</div>';
			return $textarea;
		}
	}
	
	public function createFileTag($input, $currentInfo)
	{
		$fieldName = $input[0]['field_name'];
		if ($input) {
			$file = '<div class="form-group">';
			$file .= '<label class="control-label">' . $input[0]['name'] . '</label>';
			$file .= ($input[0]['mandatory'] ? '<span class="mandetory-field"></span>' : '');
			$class = ($input[0]['mandatory'] ? 'validate[required]' : '');
			$required = ($input[0]['mandatory'] ? 'required="required"' : '');
			$file .= '<input '. $required . ' type="file" multiple="multiple" name="'. $input[0]['field_name'] .'[]" id="form-generator-text-' . $input[0]['field_name'] . ' class="form-control ' . $class . '">';
			$currentFilesWrapper = '';
			if (isset($currentInfo['file_pack'][$fieldName])) {
				foreach ($currentInfo['file_pack'][$fieldName] as $row => $pack) {	
						if ($pack['field_type'] == 'image_file') {
							$currentFilesWrapper .= '<div id="file-' . $pack['id'] .'">';
							$currentFilesWrapper .= '<img height="100" width="100" src="http://' . $_SERVER['HTTP_HOST'] . $this->basePath . $fieldName . '/' . $pack['name'].'">';
							if (!$input[0]['mandatory']) {	
							     $currentFilesWrapper .= '<span style="cursor:pointer;" class="glyphicon glyphicon-remove remove-btn" file-id="' . $pack['id'] . '"></span>';
							}
							$currentFilesWrapper .= '</div>';
						} elseif($pack['field_type'] == 'public_file') {
							$currentFilesWrapper .= '<div id="file-' . $pack['id'] .'">';
							$currentFilesWrapper .= '<a href="http://' . $_SERVER['HTTP_HOST'] . $this->basePath . $fieldName . '/' . $pack['name'].'">'.$pack['name'].'</a>';
							if (!$input[0]['mandatory']) {
							    $currentFilesWrapper .= '<span style="cursor:pointer;" class="glyphicon glyphicon-remove remove-btn" file-id="' . $pack['id'] . '"></span>';
							}
							$currentFilesWrapper .= '</div>';
						}
				}
			}
			if ($currentFilesWrapper != '')
				$file .= '<div class="current-file-wrapper">'.$currentFilesWrapper.'</div>';
			
			$file .= '</div>';
			return $file;
		}
	}
	
	public function uploadFiles($input, $files)
	{
		if ($files) {
			$uploadsInfo = array();
			foreach ($files as $key => $mainFile) {
				foreach ($mainFile as $file) {
					if (empty($file['name'])) {
						continue;						
					}
					$path_parts = pathinfo($file['name']);
					$name = md5($file['name']) . '.' . $path_parts['extension'];
					$directory = $_SERVER['DOCUMENT_ROOT'] . $this->basePath . $key . '/';
					if (!file_exists($directory)) {
						mkdir($directory, 0777);
					}
					$fileName = $directory.$name;
					$isUploaded = move_uploaded_file($file["tmp_name"], $fileName);
					if (!$isUploaded) {
						return false;
					} else {
						chmod($fileName, 0755);
						$uploadsInfo[] = array($key, $name, $this->basePath);
					}
				}
			}
			return $uploadsInfo;
		}
	}
}
