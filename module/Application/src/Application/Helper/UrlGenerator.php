<?php 
namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;

class UrlGenerator extends AbstractHelper
{
	
	public function postUrl($blogPost)
	{
	    $categorySlug = str_replace(' ', '-', $blogPost['category_title']);
	    if ($blogPost['post_lang'] == $GLOBALS["dlang"]) {
	        return $this->view->url('blog-post', array(
	            'category' => $categorySlug,
	            'slug'     => urldecode($blogPost['post_slug'])
	        ));
	    } else {
	        return $this->view->url('blog-post', array(
	            'lang' => $blogPost['post_lang'],
	            'category' => $categorySlug,
	            'slug'     => urldecode($blogPost['post_slug'])
	        ));
	    }
	}
	
	public function categoryUrl($blogCategory)
	{
		$categorySlug = str_replace(' ', '-', $blogCategory['category_title']);
		if ($blogCategory['post_lang'] == $GLOBALS["dlang"]) {
		   return $this->view->url('blog-category', array(
				'category' => $categorySlug
		  ));
		} else {
		   return $this->view->url('blog-category', array(
		        'lang' => $blogCategory['post_lang'],
				'category' => $categorySlug
		  ));
		}
		
		
		
	}
	
	public function tagUrl($tag, $lang)
	{
	    if ($lang == $GLOBALS["dlang"]) {
	        return $this->view->url('blog-tag', array(
				'tag' => str_replace(' ', '-', strtolower($tag))
		    ));
	    } else {
	        return $this->view->url('blog-tag', array(
		        'lang' => $lang,
				'tag' => str_replace(' ', '-', strtolower($tag))
		    ));
	    }
	}
	
	public function authorUrl($post)
	{
		$name = $post['firstname'] . ' ' . $post['lastname'];
		if ($post["post_lang"] == $GLOBALS["dlang"]) {
		    return $this->view->url('blog-author', array(
				'name' => str_replace(' ', '-', $name)
		    ));
		} else {
		    return $this->view->url('blog-author', array(
		        'lang' => $post["post_lang"],
				'name' => str_replace(' ', '-', $name)
		  ));
		}
		
	}
	
}