<?php 
namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Crypt\PublicKey\Rsa\PublicKey;

class UrlGeneratorPage extends AbstractHelper
{
	public function pageUrl($pagePage)
	{
		return $this->view->url('page-home', array(
		    'lang'    => $pagePage["page_lang"],
			'slug'     => urldecode($pagePage['page_slug'])	
		));
	}	
	public function tagUrl($tag, $lang)
	{
		return $this->view->url('page-page', array(
    		    'action' => "tag",
    		    'lang'    => $lang,
				'param' => str_replace(' ', '-', strtolower($tag))
		));
	}
	
	public function authorUrl($pagePage)
	{
		return $this->view->url('page-page', array(
    		    'action' => "author",
    		    'lang'    => $pagePage["page_lang"],
				'param' => str_replace(' ', '-', $pagePage["page_display_name"])
		));
		    
	}

}
