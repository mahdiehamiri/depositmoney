<?php
namespace Application\Helper;

use Zend\Mail\Message as ZendMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;
use Zend\Mail\Transport\Sendmail;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Mime;

use Zend\Mail\Transport\Smtp;



class MailService extends AbstractPlugin
{
    protected $from = 'noreply@kafeebazar.com';
    
    private function theme($text)
    {
        return $text;
    }
    
    private function getBody($textBody)
    {
        $textPart = new MimePart($textBody);
        $textPart->type = "text/plain";
        $textPart->encoding = '8bit';
        $textPart->charset = 'UTF-8';
        
        $htmlBody = $this->theme($textBody);
        $htmlPart = new MimePart($htmlBody);
        $htmlPart->type = "text/html";
        $htmlPart->encoding = '8bit';
        $htmlPart->charset = 'UTF-8';
        
        $body = new MimeMessage();
        $body->setParts(array($htmlPart));
        
        return $body;
    }
    
    private function setHeaders($message)
    {
        $message->getHeaders()->get('content-type')->setType("multipart/alternative");
        
        //         $message->getHeaders()->removeHeader("Content-Type");
        //         $message->getHeaders()->addHeaderLine('Content-Type', 'text/plain; charset=UTF-8');
        
        //         $message->getHeaders()->removeHeader("Content-Transfer-Encoding");
        //         $message->getHeaders()->addHeaderLine('Content-Transfer-Encoding', '8bit');
        
        return $message;
    }
    
    private function sendNativeMail($body, $subject, $to)
    {
        $fromAddress = $this->from;
        $subject = "=?utf-8?b?".base64_encode($subject)."?=";
        $headers = "MIME-Version: 1.0\r\n";
        $headers.= "From: =?utf-8?b?".base64_encode($fromAddress)."?= <".$fromAddress.">\r\n";
        $headers.= "Content-Type: text/plain;charset=utf-8\r\n";
        $headers.= "X-Mailer: PHP/" . phpversion();
        return  mail($to, $subject, $body, $headers);
    }
    
    private function sendMailWithAttachment($to, $subject, $htmlMsg, $fileNames)
    {
        // Render content from template
        $htmlContent = $htmlMsg;
        // Create HTML part
        $htmlPart = new MimePart($htmlContent);
        $htmlPart->type = Mime::TYPE_HTML;
        $htmlPart->charset = 'UTF-8';
        $htmlPart->type = 'text/html';
        // Create plain text part
        $stripTagsFilter = new \Zend\Filter\StripTags();
        $textContent = str_ireplace(array("<br />", "<br>"), "\r\n", $htmlContent);
        $textContent = $stripTagsFilter->filter($textContent);
        $textPart = new MimePart($textContent);
        $textPart->type = Mime::TYPE_HTML;
        $textPart->charset = 'UTF-8';
        $textPart->type = 'text/plain';
        
        // Create separate alternative parts object
        $alternatives = new MimeMessage();
        $alternatives->setParts(array($textPart, $htmlPart));
        $alternativesPart = new MimePart($alternatives->generateMessage());
        $alternativesPart->type = "multipart/alternative;\n boundary=\"".$alternatives->getMime()->boundary()."\"";
        
        $body = new MimeMessage();
        $body->addPart($alternativesPart);
        if ($fileNames) {
            foreach ($fileNames as $fileName) {
                $attachment = new MimePart( file_get_contents($fileName) );
                $attachment->type = \Zend\Mime\Mime::TYPE_OCTETSTREAM;
                $attachment->filename = basename($fileName);
                $attachment->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
                $attachment->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
                $body->addPart($attachment);
            }
        }
        // Create mail message
        $mailMessage = new ZendMessage();
        $mailMessage->setFrom($this->from);
        $mailMessage->setTo($to);
        $mailMessage->setSubject($subject);
        $mailMessage->setBody($body);
        $mailMessage->setEncoding("UTF-8");
        if ($fileNames) {
            $mailMessage->getHeaders()->get('content-type')->setType('multipart/mixed');
        } else {
            $mailMessage->getHeaders()->get('content-type')->setType('multipart/alternative');
        }
        
        
        
        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => 'localhost',
            'host'              => 'localhost',
            'connection_class'  => 'login',
            'port'              => 25,
            'connection_config' => array(
                'username' => 'noreply@kafeebazar.com',
                'password' => 'so8oE@43',
                'ssl' => 'tls',
            ),
            
        ));
        
        $transport->setOptions($options);
        $transport->send($mailMessage);
    }
    
    private function sendZendMail($body, $subject, $to, $attachementFiles)
    {
        
        $content  = new MimeMessage();
        $htmlPart = new MimePart($body);
        $htmlPart->type = 'text/html';
        $textPart = new MimePart($body);
        $textPart->type = 'text/plain';
        $content->setParts(array($textPart, $htmlPart));
        
        $contentPart = new MimePart($content->generateMessage());
        $contentPart->type = 'multipart/alternative;' . PHP_EOL . ' boundary="' . $content->getMime()->boundary() . '"';
        $attachment = array();
        if ($attachementFiles) {
            $attachment = new MimePart(fopen($attachementFiles, 'r'));
            $attachment->type = \Zend\Mime\Mime::TYPE_OCTETSTREAM;
            $attachment->encoding    = Mime::ENCODING_BASE64;
            $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
        }
        
        
        $body = new MimeMessage();
        $body->setParts(array($contentPart, $attachment));
        
        
        
        
        
        
        
        $message = new Message();
        $message->setEncoding('utf-8')
        ->addTo($to)
        ->addFrom($this->from)
        ->setSubject($subject)
        ->setBody($body);
        
        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => 'localhost',
            'host'              => 'localhost',
            'connection_class'  => 'login',
            'port'              => 25,
            'connection_config' => array(
                'username' => 'noreply@kafeebazar.com',
                'password' => 'so8oE@43',
                'ssl' => 'tls',
            ),
        ));
        
        $transport->setOptions($options);
        $transport->send($message);
        
        return;
        
        
        $message = new Message();
        $message->setBody($body);
        $message->setEncoding('UTF-8');
        
        $message->setFrom($this->from);
        $message->addTo($to);
        $message->setSubject($subject);
        
        $message = $this->setHeaders($message);
        
        
        $transport = new Smtp();
        $options   = new SmtpOptions(array(
            'name'              => 'localhost',
            'host'              => 'localhost',
            'connection_class'  => 'login',
            'port'              => 25,
            'connection_config' => array(
                'username' => 'noreply@kafeebazar.com',
                'password' => 'so8oE@43',
                'ssl' => 'tls',
            ),
        ));
        $transport->setOptions($options);
        
        $transport->send($message);
    }
    
    public function sendMail($body, $subject, $to, $attachementFiles = false)
    {
        // var_dump($body, $subject, $to);die;
        
        ini_set('sendmail_from', $this->from);
        
        try {
            $this->sendZendMail($to, $subject, $body, $attachementFiles);
            return true;
        }  catch (\Exception $e) {
            $this->sendNativeMail($body, $subject, $to);
        }
    }
}
