<?php 

namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;

class MenuGenerator extends AbstractHelper 
{
	public function getMenu()
	{
		return array(
// 					array(
// 						'label' => __("Home page"),
// 						'link'  => '/',
// 						'permission' => 'application-index-index',
// 					    'iclass' => 'fa fa-home'
// 					),
					array(
						'label' => __("Dashboard"),
						'link'  => '/admin',
						'permission' => 'application-admin-index',
					    'iclass' => 'fa fa-dashboard'
					),
        		   array(
        		        'label' => __("Permission management"),
        		        'link'  => '#',
					    'iclass' => 'fa fa-key',
        		        'pages' => array( 
        		            array(
        		                'label' 	 => __("Permissions list"),
        		                'link'  	 => '/admin-permission/list-permissions',
        		                'permission' => 'permission-admin-list-permissions'
        		            ),
        		            array(
        		                'label' 	 => __("Update permission"),
        		                'link'  	 => '/admin-permission/update-auto-permissions',
        		                'permission' => 'permission-admin-update-auto-permissions'
        		            ),
        		            array(
        		                'label' 	 => __("User group permission management"),
        		                'link'  	 => '/admin-permission/edit-users-groups-permissions',
        		                'permission' => 'permission-admin-edit-users-groups-permissions'
        		            ),
        		        )
        		    ),
        		    array(
        		        'label' => __("Site language management"),
        		        'link'  => '/admin-language/list-languages',
        		        'iclass' => 'fa fa-globe',
        		        'pages' => array(
        		            array(
        		                'label' => __("Site languages list"),
        		                'link'  => '/admin-language/list-languages',
        		                'permission' => 'language-admin-list-languages'
        		            ),
        		            array(
        		                'label' => __("Site phrases update"),
        		                'link'  => '/admin-language/update-phrases',
        		                'permission' => 'language-admin-update-phrases'
        		            ),
        		            array(
        		                'label' => __("Languages phrases management"),
        		                'link'  => '/admin-language/manage-phrases',
        		                'permission' => 'language-admin-manage-phrases'
        		            ),
        		        )
        		    ),
        		    array(
        		        'label' => __("Gallery management"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-image',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("Gallery List"),
        		                'link'  => '/admin-gallery/list-galleries',
        		                'permission' => 'gallery-admin-index',
        		            ),
        		        )
        		    ),
        		    array(
        		        'label' => __("Optiomaize Uploaded Image"),
        		        'link'  => '/admin-optimizer/index',
        		        'permission' => 'optimizer-admin-index',
        		        'iclass' => 'fa fa-file-archive-o'
        		    ),
        		    array(
        		        'label' => __("Video gallery management"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-file-video-o',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("Video gallery List"),
        		                'link'  => '/admin-video-gallery/list-video-galleries',
        		                'permission' => 'videogallery-admin-index',
        		            ),
        		        )
        		    ),
        		    array(
        		        'label' => __("Slideshow management"),
        		        'link'	=> '#',
        		        'iclass' => 'fa fa-file-image-o',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("Slideshow list"),
        		                'link'  	 => '/admin-slideshow',
        		                'permission' => 'slideshow-admin-list-slideshows'
        		            ),
        		        )
        		    ),
    		    array(
    		        'label' => __("Pages management"),
    		        'link'  => '#',
    		        'iclass' => 'fa fa-file-o',
    		        'pages' => array(
    		            array(
    		                'label' 	 => __("Pages list"),
    		                'link'  	 => '/admin-page',
    		                'permission' => 'page-admin-list-pages'
    		            ),
    		            array(
    		                'label' 	 => __("Add new page"),
    		                'link'  	 => '/admin-page/add-page',
    		                'permission' => 'page-admin-add-page'
    		            ),
    		        )
    		    ),
    		    array(
    		        'label' => __("Menus management"),
    		        'link'  => '#',
    		        'iclass' => 'fa fa-bars',
    		        'pages' => array(
    		            array(
    		                'label' 	 => __("Menus list"),
    		                'link'  	 => '/admin-menu',
    		                'permission' => 'menu-admin-list-main-menus'
    		            ),
    		            array(
    		                'label' 	 => __("Add new main menu"),
    		                'link'  	 => '/admin-menu/add-main-menu',
    		                'permission' => 'menu-admin-add-main-menu'
    		            ),
    		        )
    		    ),
    		    array(
    		        'label' => __("Block management"),
    		        'link'  => '#',
    		        'iclass' => 'fa fa-square',
    		        'pages' => array(
    		            array(
    		                'label' 	 => __("Block list"),
    		                'link'  	 => '/admin-block/list-blocks',
    		                'permission' => 'block-admin-list-blocks'
    		            ),
    		            array(
    		                'label' 	 => __("Layouts management"),
    		                'link'  	 => '/admin-block/lists-layout',
    		                'permission' => 'block-admin--lists-layout'
    		            ),
    		        )
    		    ),
		 
        		    array(
        		        'label' => __("Links management"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-link',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("Link list"),
        		                'link'  => '/admin-link',
							    'permission' => 'link-admin-list-links-category',
        		            ),
        		        )
        		    ),
        		    array(
        		        'label' => __("Newsletter management"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-newspaper-o',
        		        'pages' => array(
        		            array(
        		                'label' 	 =>  __("Newsletter list"),
        		                'link'  => '/admin-newsletter',
        		                'permission' => 'newsletter-admin-list-newsletters'
        		            ),
        		            array(
        		                'label' 	 =>  __("Queues list"),
        		                'link'  => '/admin-newsletter/list-queues',
        		                'permission' => 'newsletter-admin-list-queues'
        		            ),
        		            array(
        		                'label' =>  __("Add newsletter"),
        		                'link'  => '/admin-newsletter/add-queue',
        		                'permission' => 'newsletter-admin-add-newsletter',
        		                'iclass' => 'fa fa-newspaper-o'
        		            ),
        		             
        		        )
        		    ),
		    
        		    array(
        		        'label' => __("Blog management"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-file-text-o',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("Categories list"),
        		                'link'  	 => '/admin-blog/list-categories',
        		                'permission' => 'blog-admin-list-categories'
        		            ),
        		            array(
        		                'label' 	 => __("Articles list"),
        		                'link'  	 => '/admin-blog',
        		                'permission' => 'blog-admin-list-posts'
        		            ),
        		            array(
        		                'label' 	 => __("Add new article"),
        		                'link'  	 => '/admin-blog/add-post',
        		                'permission' => 'blog-admin-add-post'
        		            ),
        		            array(
        		                'label' 	 => 'View blog',
        		                'link'  	 => '/blog',
        		                'permission' => 'blog-redirect-to-blog'
        		            ),
        		        )
        		    ),
        		   array (
						'label' =>__("Comments management"),
						'link' => '#',
						'iclass' => 'fa fa-comment-o',
						'pages' => array (
								array (
										'label' =>__("Comments list"),
										'link' => '/admin-comment',
										'permission' => 'comment-admin-list-comments' 
								) 
						)
						 
				    ),
        		    array (
        		        'label' =>__("products management"),
        		        'link' => '#',
        		        'iclass' => 'fa fa-shopping-basket',
        		        'pages' => array (
        		            array (
        		                'label' =>__("Category list"),
        		                'link' => '/admin-shop/category-list',
        		                'permission' => 'shop-admin-category-list'
        		            ),
        		            array (
        		                'label' =>__("Digibenis Products List"),
        		                'link' => '/admin-shop/list-products',
        		                'permission' => 'shop-admin-list-products'
        		            ),
        		            array (
        		                'label' =>__("Users Products List"),
        		                'link' => '/admin-shop/user-products-list',
        		                'permission' => 'shop-admin-user-products-list'
        		            ),
        		            array (
        		                'label' =>__("Order list"),
        		                'link' => '/admin-shop/orders',
        		                'permission' => 'shop-admin-orders'
        		            ),
        		        )   
        		        
        		    ),
		    
    		    
		    
//         		    array(
//         		        'label' => __("Shop management"),
//         		        'link'  => '#',
//         		        'iclass' => 'fa fa-shopping-basket',
//         		        'pages' => array(
//         		            array(
//         		                'label' 	 => __("Attributes Group Management"),
//         		                'link'  	 => '/admin-shop/attribute-groups',
//         		                'permission' => 'shop-admin-attribute-groups'
//         		            ),
//         		            array(
//         		                'label' 	 => __("Product Category list"),
//         		                'link'  	 => '/admin-shop',
//         		                'permission' => 'shop-admin-categories'
//         		            ),
//         		           /*  array(
//         		                'label' 	 => __("Add Product"),
//         		                'link'  	 => '/admin-shop/add-product',
//         		                'permission' => 'shop-admin-add-product'
//         		            ), */
//         		    //         		                        array(
//         		        //         		             'label' => __("Payment management"),
//         		        //         		                'link'  => '/admin-shop/list-payments',
//         		        //         		                'permission' => 'shop-admin-list-payments'
//         		        //         		            ),
//         		    //                 		    array(
//         		        //                 		        'label' => __("Order management"),
//         		        //                 		        'link'  => '/admin-shop/list-orders',
//         		        //                 		        'permission' => 'shop-admin-list-orders'
//         		        //                 		    ),
//         		    //                 		    array(
//         		        //                 		        'label' => __("Product comment management"),
//         		        //                 		        'link'  => '/admin-product-comment/list-all-product-comments',
//         		        //                 		        'permission' => 'productcomment-admin-list-all-product-comments',
//         		        //                 		        'iclass' => 'fa fa-list'
//         		        //                 		    ),
//         		    //                 		    array(
//         		        //                 		        'label' => __("Sells report"),
//         		        //                 		        'link'  => '/admin-shop/sells-report',
//         		        //                 		        'permission' => 'shop-admin-sells-report',
//         		        //                 		        'iclass' => 'fa fa-dollar'
//         		        //                 		    ),
//         		        )
//         		    ),
        		    array(
        		        'label' => __("Print Services management"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-cogs',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("list Services Types"),
        		                'link'  => '/admin-printservices/services-types',
        		                'permission' => 'printservices-admin-services-types',
        		            ),
        		            array(
        		                'label' 	 => __("list Print Services"),
        		                'link'  => '/admin-printservices/print-services-list',
        		                'permission' => 'printservices-admin-print-services-list',
        		            ),
        		            array(
        		                'label' 	 => __("List Attributes"),
        		                'link'  => '/admin-printservices/attributes-list',
        		                'permission' => 'printservices-admin-attributes-list',
        		            ),
        		            array(
        		                'label' 	 => __("Price List"),
        		                'link'  => '/admin-printservices/price-list',
        		                'permission' => 'printservices-admin-price-list',
        		            ),
        		            array(
        		                'label' 	 => __("Orders List"),
        		                'link'  => '/admin-printservices/orders/all',
        		                'permission' => 'printservices-admin-orders',
        		            ),
        		        )
        		    ),	
        		    array(
        		        'label' => __("Samples"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-th-list',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("Samples Category list"),
        		                'link'  	 => '/admin-samples',
        		                'permission' => 'samples-admin-list-samples-category'
        		            ),
        		            array(
        		                'label' 	 => __("Samples list"),
        		                'link'  	 => '/admin-samples/list-samples',
        		                'permission' => 'samples-admin-list-samples'
        		            ),
        		        )
        		    ),
        		    array(
        		        'label' => __("Faq management"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-question',
        		        'pages' => array(
        		            array(
        		                'label' 	 =>  __("Faq list"),
        		                'link'  => '/admin-faq',
        		                'permission' => 'faq-admin-list',
        		            ),
        		        )
        		    ),
        		    array(
        		        'label' => __("Coworkers"),
        		        'link'  => '#',
        		        'iclass' => 'fa fa-users',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("Coworkers list"),
        		                'link'  	 => '/admin-coworkers',
        		                'permission' => 'coworkers-admin-list-coworkers'
        		            ),
        		        )
        		    ),
        		    array (
        		        'label' =>__("League management"),
        		        'link' => '/admin-football',
        		        'permission' => 'football-admin-list-parameter',
        		        'iclass' => 'fa fa-futbol-o',
        		        'pages' => array (
        		            array (
        		                'label' =>__("Team League"),
        		                'link' => '/admin-football/parameter/League',
        		                'permission' => 'football-admin-list-parameter'
        		            ),
        		    
        		            array (
        		                'label' =>__("Team List"),
        		                'link' => '/admin-football/parameter/Team',
        		                'permission' => 'football-admin-list-parameter'
        		            ),
        		    
        		            array (
        		                'label' =>__("Game List"),
        		                'link' => '/admin-football/parameter/Game',
        		                'permission' => 'football-admin-list-parameter'
        		            )
        		        )
        		    ),
        		     array(
            		     'label' => __("Users management"),
            		     'link'  => '#',
        		         'iclass' => 'fa fa-user',
            		     'pages' => array(
            		         array(
            		             'label' 	 => __("Register Users list"),
            		             'link'  	 => '/admin-user/users',
            		             'permission' => 'user-admin-users'
            		         ),
                		     array(
                    		     'label' 	 => __("Users list"),
                    		     'link'  	 => '/admin-user',
                    		     'permission' => 'user-admin-list-user'
                		     ),
                		     array(
                    		     'label' 	 => __("Add new user"),
                    		     'link'  	 => '/admin-user/add-user',
                    		     'permission' => 'user-admin-add-user'
                		     ),
            		        
                		     array(
                    		     'label' 	 => __('View user'),
                    		     'link'  	 => '/user',
                    		     'permission' => 'user-admin'
                    		     ),
            		     )
					), 
					array(
						'label' => __("Deposit money"),
						'link'  => '#',
						'iclass' => 'fa fa-money',
						'pages' => array(
							array(
								'label' 	 => __("List of deposit funds"),
								'link'  	 => '/admin/deposit-list',
								'permission' => 'application-admin-deposit-list'
							),
							array(
								'label' 	 => __("Add new deposit money"),
								'link'  	 => '/admin/deposit-add',
								'permission' => 'application-admin-deposit-add'
							),
							array(
								'label' 	 => __("deleted deposit money list"),
								'link'  	 => '/admin/deposit-deleted-list',
								'permission' => 'application-admin-deposit-deleted-list'
							),
						   
						)
				   ), 
        		    array(
        		        'label' => __("Area List"),
        		        'link'  => '/admin-user/area-list',
        		        'permission' => 'user-admin-area-list',
        		        'iclass' => 'fa fa-th-list'
        		    ),
        		    array(
        		        'label' => __("Edit profile"),
        		        'link'	=> '#',
        		        'iclass' => 'fa fa-user',
        		        'pages' => array(
        		            array(
        		                'label' 	 => __("Edit profile"),
        		                'link'  	 => '/admin/edit-account',
        		                'permission' => 'application-admin-edit-account',
        		            ),
        		        )
        		    ),
        		    
        		    array(
        		        'label' => __("Configuration management"),
        		        'link'  => '/admin-configuration',
        		        'permission' => 'configuration-admin-list-configurations',
        		        'iclass' => 'fa fa-cog'
        		    ),
					array(
							'label' 	 => __("Logout"),
							'link' 	     => '/admin/logout',
							'permission' => 'application-admin-logout',
		                    'iclass' => 'fa fa-sign-out'
					),
		);
	}
}
