<?php 
namespace Application\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class UrlGeneratorInController extends AbstractPlugin 
{
	public function postUrl($blogPost)
	{
		$categorySlug = str_replace(' ', '-', $blogPost['category_title']);
		return '/blog/' . $blogPost['post_slug'] . '/' . $categorySlug;
	}
}