<?php
namespace Application\Helper;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Language\Model\LanguageLanguagesTable;
use Zend\Validator\AbstractValidator;
use Configuration\Model\ConfigurationTable;
use Application\Registry\Registry;
class BasePluginController  
{
    public function __construct($serviceManager, $lang = "fa")
    {
        $this->serviceLocator = $serviceManager;
        $this->lang = $lang;
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData = $this->authenticationService->isLoggedIn();
    }
    
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    public function getRegistry($methodName, $params = null)
    {
        //	$regisrey = $this->getServiceLocator()->get('Zend\View\Registry\Registry');
        $regisrey = new Registry($this->getServiceLocator ());
    
        return $regisrey->$methodName($params);
    }
}
