<?php 

namespace Application\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Application\Model\GeneralTable;
use Application\Model\UserGroupGeneralTable;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Language\Model\LanguageLanguagesTable;
use Zend\Session\Container;

class GeneralHelper extends AbstractPlugin implements ServiceLocatorAwareInterface
{

    public $allPerm;
    public $allWords;
    public function getServiceLocator() {
	    return $this->serviceLocator->getServiceLocator();
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
	    $this->serviceLocator = $serviceLocator;
	}
    public function getLanguage($controller)
    {
        $languagelanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $defaultLanguage = $languagelanguagesTable->getAllLanguages(array("default" => "1"));
        $defaultLang = "fa";
        if ($defaultLanguage) {
            $defaultLang = $defaultLanguage[0]["code"];
        }
        $lang = ($controller->params('lang') !== null?$controller->params('lang'):$defaultLang);
        $lang = substr($lang, 0, 2);
     
        return $lang;
    }
    
    public function GetFormErrors($form , $oneByOne = false , $array = false)
    {
        
        if(!$oneByOne)
            return $form->getMessages();
        else
        {
            foreach ($form->getMessages() as $key => $value) {
                if(is_array($value))
                    foreach ($value as $k => $v) {
                        return  ($array?array($key,$v):$v) ;
                    }
                else return htmlspecialchars($value);
            }
        }
        return false;
    }
    
    public function createTree($allData, $view = false, $pagesList = null, $count = 0) {
        $menu = array(
            'items' => array(),
            'parents' => array()
        );
        foreach ($allData as $item) {

            // Creates entry into items array with current menu item id ie. $menu['items'][1]
            $menu['items'][$item['id']] = $item;
            // Creates entry into parents array. Parents array contains a list of all items with children
            $menu['parents'][$item['parent_id']][] = $item['id'];
        }
        if ($view)
            return $this->buildMenuView("0", $menu, $pagesList, $count);
        else 
            return $this->buildMenu("0", $menu, $pagesList);
    }
    
    public function buildMenuGallery($parent, $menu)
    {
        $html = array();
        if (isset($menu['parents'][$parent]))
        {
    
            foreach ($menu['parents'][$parent] as $itemId)
            {
                if(!isset($menu['parents'][$itemId]))
                {
                    $html[$itemId]['persian_name']= $menu['items'][$itemId]['persian_name'];
                    $html[$itemId]["latin_name"]= $menu['items'][$itemId]['latin_name'];
                }
                if(isset($menu['parents'][$itemId]))
                {
                    $html[$itemId]["name"]= $menu['items'][$itemId]['persian_name'];
                    $html[$itemId]["latin_name"]= $menu['items'][$itemId]['latin_name'];
                    $html[$itemId]["persian_name"]= $menu['items'][$itemId]['persian_name'];
                    $html[$itemId]["child"] = $this->buildMenuGallery($itemId, $menu);
                }
            }
        }
        return $html;
    }
    
    public function buildMenuVideoGallery($parent, $menu)
    {
        $html = array();
        if (isset($menu['parents'][$parent]))
        {
    
            foreach ($menu['parents'][$parent] as $itemId)
            {
                if(!isset($menu['parents'][$itemId]))
                {
                    $html[$itemId]['persian_name']= $menu['items'][$itemId]['persian_name'];
                    $html[$itemId]["latin_name"]= $menu['items'][$itemId]['latin_name'];
                }
                if(isset($menu['parents'][$itemId]))
                {
                    $html[$itemId]["persian_name"]= $menu['items'][$itemId]['persian_name'];
                    $html[$itemId]["latin_name"]= $menu['items'][$itemId]['latin_name'];
                    $html[$itemId]["child"] = $this->buildMenuVideoGallery($itemId, $menu);
                }
            }
        }
        return $html;
    }
    
    function buildMenu($parent, $menu, $pageList)
    {
        $html = "";
        if (isset($menu['parents'][$parent]))
        {
            $html .= "
      <ul >\n";
            foreach ($menu['parents'][$parent] as $itemId)
            {
                if ($menu['items'][$itemId]['external_url']) {
                    $url = $menu['items'][$itemId]['external_url'];
                } else {
                    $url = (isset($pageList[$menu['items'][$itemId]['internal_url']])?$pageList[$menu['items'][$itemId]['internal_url']]:"");
                }
                
                if(!isset($menu['parents'][$itemId]))
                {
                    $html .= "<li>\n  ".$menu['items'][$itemId]['caption']."   <a target='_blank' data-toggle='tooltip' data-placement='top' title='نمایش' href='".$url."'><i class='fa fa-search-plus'></i></a> <span data-toggle='tooltip' data-placement='top' title='ویرایش' style='cursor:pointer' class='editMenu' href='#' data-id='" . $menu['items'][$itemId]['id'] ."' data-caption='" . $menu['items'][$itemId]['caption'] ."' data-external-url='" . $menu['items'][$itemId]['external_url'] . "' data-internal-url='" . $menu['items'][$itemId]['internal_url'] . "' data-parent-id='" . $menu['items'][$itemId]['parent_id'] ."'  data-order='" . $menu['items'][$itemId]['order'] ."'  data-width='" . $menu['items'][$itemId]['width'] ."'  data-active='" . $menu['items'][$itemId]['active'] ."'  ><i class='fa fa-edit'></i></span>   <span data-toggle='tooltip' data-placement='top' title='حذف' style='cursor:pointer' class='deleteMenu' id='a' data-id=" . $menu['items'][$itemId]['id'] . "><i class='fa fa-remove'></i></span>\n</li> \n";
                }
                if(isset($menu['parents'][$itemId]))
                {
                    $html .= "
             <li>\n  " . $menu['items'][$itemId]['caption'] ."   <a target='_blank' data-toggle='tooltip' data-placement='top' title='نمایش' href='".$url."'><i class='fa fa-search-plus'></i></a> <span data-toggle='tooltip' data-placement='top' title='ویرایش' style='cursor:pointer' class='editMenu' href='#' data-id='" . $menu['items'][$itemId]['id'] ."' data-caption='" . $menu['items'][$itemId]['caption'] ."' data-external-url='" . $menu['items'][$itemId]['external_url'] . "' data-internal-url='" . $menu['items'][$itemId]['internal_url'] . "' data-parent-id='" . $menu['items'][$itemId]['parent_id'] ."'  data-order='" . $menu['items'][$itemId]['order'] ."'  data-width='" . $menu['items'][$itemId]['width'] ."' data-active='" . $menu['items'][$itemId]['active'] ."' ><i class='fa fa-edit'></i></span>   <span data-toggle='tooltip' data-placement='top' title='حذف' style='cursor:pointer' class='deleteMenu' id='a' data-id=" . $menu['items'][$itemId]['id'] . "><i class='fa fa-remove'></i></span> \n";
                    $html .= $this->buildMenu($itemId, $menu, $pageList);
                    $html .= "</li> \n";
                }
            }
            $html .= "</ul> \n";
        }
        return $html;
    }
    
    function buildMenuView($parent, $menu, $pageList, $count = -1)
    {
        $actualLink = rawurldecode($_SERVER["REQUEST_URI"]);
        $width = 100 /$count . "%";
        $html = "";
        if (isset($menu['parents'][$parent]))
        {
            $html .= "
       <ul class='child-" . $count . " " . (isset($menu['items'][$parent][''])?$menu['items'][$parent]['class_name_ul']:'nav navbar-nav ') ."'>\n";
            foreach ($menu['parents'][$parent] as $itemId)
            {
                $className = "";
                if ($menu['items'][$itemId]['external_url']) {
                    $url = $menu['items'][$itemId]['external_url'];
                    if ($url == "disable") {
                        $url = "#";
                        $className = "disable";
                    }
                } else {
                    $url = (isset($pageList[$menu['items'][$itemId]['internal_url']])?$pageList[$menu['items'][$itemId]['internal_url']]:"");
                }
                
                $className .= (isset($menu['items'][$itemId]['class_name_li'])?$menu['items'][$itemId]['class_name_li']:"");
                
                
                if ($actualLink == $url) {
                    $className .= " active";
                }
                $caret = '';
                if ($menu['items'][$itemId]['class_name_ul'] != '') {
                    $caret = "";
                }
                $order = (int)$menu['items'][$itemId]['order'];
                if(!isset($menu['parents'][$itemId]))
                { 
                    if ($menu['items'][$itemId]['active'] == 1) {
                        if ($menu['items'][$itemId]['width']) {
                            $style = "style='width:" . $menu['items'][$itemId]['width'] . "'";
                        } else {
                            $style = "style='width:" . $width . "'";
                        }
                        if(!empty($menu['items'][$itemId]['parent_id'])){
                            if(!empty($menu['items'][$itemId]['image'])){
                                $html .= "<li $style class='$className order-$order'>\n  <a href='".$url."' title ='".$menu['items'][$itemId]['caption']."'><span><img src='/uploads/menus/".$menu['items'][$itemId]['image']."'>" .$menu['items'][$itemId]['caption']."</span>$caret</a>\n</li> \n";   
                            }else{
                                $html .= "<li $style class='$className order-$order'>\n  <a href='".$url."' title ='".$menu['items'][$itemId]['caption']."'><span>".$menu['items'][$itemId]['icon'] .$menu['items'][$itemId]['caption']."</span>$caret</a>\n</li> \n";      
                            }
                        }else{
                            $html .= "<li $style class='$className order-$order'>\n  <a href='".$url."' title ='".$menu['items'][$itemId]['caption']."'><span>".$menu['items'][$itemId]['icon'] .$menu['items'][$itemId]['caption']."</span>$caret</a>\n</li> \n";  
                        }
                    }
                }
                if(isset($menu['parents'][$itemId]))
                {
                    if ($menu['items'][$itemId]['active'] == 1) {
                        if ($menu['items'][$itemId]['width']) {
                            $style = "style='width:" . $menu['items'][$itemId]['width'] . "'";
                        } else {
                            $style = "style='width:" . $width . "'";
                        }
                        $html .= "
                        <li $style class='$className order-$order'>\n  <a href='".$url."' class='' data-toggle='dropdown' title ='".$menu['items'][$itemId]['caption']."'><span>" .$menu['items'][$itemId]['icon'].$menu['items'][$itemId]['caption']."</span>$caret</a> \n";
                        $html .= $this->buildMenuView($itemId, $menu, $pageList);
                        $html .= "</li> \n";
                    }
                  
                }
            }
            $html .= "</ul> \n";
        }
        return $html;
    }
    public function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);
        
        foreach($files as $key => $value) {
            $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
            if(!is_dir($path)) {
                if (strpos($path, "Controller.php")) {
                    $results[] = $path;
                    $file = basename($path, ".php");
                    $sections = explode(DIRECTORY_SEPARATOR, $path);
                    $sections = array_slice($sections, -3, 3, true);
                    $sections = array_values($sections);
                    $controllerName = str_replace("Controller.php", "", $sections[2]);
                    $className = implode("\\", $sections);
                    $className = str_replace(".php", "", $className) ;
                   
                    if (class_exists ( $className ) && is_subclass_of($className, 'Application\Helper\BaseAdminController')) {
                       
                        $methods = get_class_methods($className);
                        
                        $matches  = preg_grep ('/Action/i', $methods);
                        $matches = array_diff($matches, array("notFoundAction", "getMethodFromAction"));
                        foreach ($matches as $action) {
                            $temp = str_replace("Action","", $action);
                            $actionPerm = preg_replace('/\B([A-Z])/', '-$1', $temp);

                            $this->allPerm [] = strtolower($sections[0] . "-" . $controllerName . "-" .  $actionPerm);
                             
                        }
                    }
                }
            } else if($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                if (strpos($path, "Controller.php")) {
    
                    $results[] = $path;
                    $file = basename($path, ".php");
                    $sections = explode("\\", $path);
                    $sections = array_slice($sections, -3, 3, true);
                    $sections = array_values($sections);
                    $controllerName = str_replace("Controller.php", "", $sections[2]);
                    $className = implode("\\", $sections);
                    $className = str_replace(".php", "", $className) ;
                    
                    if (class_exists ( $className )  && is_subclass_of($className, 'Application\Helper\BaseAdminController')) {
                        $methods = get_class_methods($className);
                        $matches  = preg_grep ('/Action/i', $methods);
                        $matches = array_diff($matches, array("notFoundAction", "getMethodFromAction"));
                        foreach ($matches as $action) {
                            $temp = str_replace("Action","", $action);
                            $actionPerm = preg_replace_callback('/[A-Z]/', function($matches){
                                return $matches[0] = '-' . strtolower($matches[0]);
                            }, $temp);
                            $this->allPerm [] = strtolower($sections[0] . "-" . $controllerName . "-" .  $actionPerm);
    
                        }
                    }
    
                }
            }
        }
       
        return $this->allPerm;
    }
    
    public function getDirContentsPhrases($dir, &$results = array()) {
        $files = scandir($dir);
        foreach($files as $key => $value) {
            $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
            if(!is_dir($path)) {
                if (strpos($path, ".php") || strpos($path, "Controller.php") || strpos($path, "Form.php") || strpos($path, ".phtml" )) {
                   
                    $content = file_get_contents($path);
                    preg_match_all('/__\((.*?)(\'|\")\)/', $content, $match);
                    
                    if ($match[1]) {
                        foreach ($match[1] as $word) {
                            $this->allWords [] = str_replace(array("'",'"'), "", $word) ;
                        }
                    }

                }
            } else if($value != "." && $value != "..") {
                $this->getDirContentsPhrases($path, $results);
                if (strpos($path, ".php") || strpos($path, "Controller.php") || strpos($path, "Form.php") || strpos($path, ".phtml")) {
                    $content = file_get_contents($path);
                    preg_match_all('/__\((.*?)(\'|\")\)/', $content, $match);
                    if ($match[1]) {
                        foreach ($match[1] as $word) {
                                $this->allWords [] = str_replace(array("'",'"'), "", $word) ;
                        }
                    }
    
                }
            }
        }
        return $this->allWords;
    }
    
    public function buildGalleryMenu($parent, $menu)
    {
        $html = "";
        if (isset($menu['parents'][$parent]))
        {
            $html .= "
          <ul class='menu'>\n";
            foreach ($menu['parents'][$parent] as $itemId)
            {
                if(!isset($menu['parents'][$itemId]))
                {
                    $html .= "<li>\n  <a href='gallery/index/showimage/".$menu['items'][$itemId]['id']."'>".$menu['items'][$itemId]['persian_name']."</a>\n</li> \n";
                }
                if(isset($menu['parents'][$itemId]))
                {
                    $html .= "
                 <li>\n  <a href='javascript:void(0)'>".$menu['items'][$itemId]['persian_name']."</a> \n";
                    $html .= $this->buildMenu($itemId, $menu);
                    $html .= "</li> \n";
                }
            }
            $html .= "</ul> \n";
        }
        return $html;
    }
    
    public function buildGalleryinnerMenu($parent, $menu)
    {
        $html = "";
        if (isset($menu['parents'][$parent]))
        {
            $html .= "
          <ul class='menu'>\n";
            foreach ($menu['parents'][$parent] as $itemId)
            {
                if(!isset($menu['parents'][$itemId]))
                {
                    $html .= "<li>\n  <a href='".$menu['items'][$itemId]['id']."'>".$menu['items'][$itemId]['persian_name']."</a>\n</li> \n";
                }
                if(isset($menu['parents'][$itemId]))
                {
                    $html .= "
                 <li>\n  <a href='javascript:void(0)'>".$menu['items'][$itemId]['persian_name']."</a> \n";
                    $html .= $this->buildinnerMenu($itemId, $menu);
                    $html .= "</li> \n";
                }
            }
            $html .= "</ul> \n";
        }
        return $html;
    }
    
    public function getFilesFromDir($dir){
        $layouts = scandir($dir."/");
        $layouts = array_diff( $layouts, Array( ".", ".." ) );
        $AllLayout['default'] = __('default');
        foreach ($layouts as $lay){
            $AllLayout[basename($lay,".phtml")]=basename($lay,".phtml");
        
        }
        return $AllLayout;
    }
    public function breadcrumbs($separator = ' ',$home, $lang) {
    //daneshi correct default lang in this function
        $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
        unset($path[1]);
    
        $base = ('http') . '://' . $_SERVER['HTTP_HOST'] . $lang;
        $breadcrumbs = Array("<li><a href=\"$base\">$home</a></li>");
    
        $last = end(($path));
    
        foreach ($path AS $x => $crumb) {
            $specialWord = rawurldecode(ucwords(str_replace(Array('-'), Array('&nbsp;'), $crumb)));
            $title = __($specialWord);
            if ($x != $last)
                $breadcrumbs[] = "<li><a href=\"$base$crumb\">$title</a></li>";
            else
                $breadcrumbs[] = $title;
        }
    
        return implode($separator, $breadcrumbs);
    }
    
    public function shopbreadcrumbs($separator , $home , $lang, $instructor_id, $productSlug = null) {
       
        //echo $instructor_id; die;
        $base = ('http') . '://' . $_SERVER['HTTP_HOST'] . '/'.$lang;
        $home = __('Home');
        $rshop = __('Real shop');
        $vshop = __('Virtual shop');
        
        $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
        $last = end(($path));
        $breadcrumbs = Array("<li><a href=\"$base\">$home</a></li>");
        
        if(!$instructor_id){
            $breadcrumbs[] = "<li><a href=\"$base/shop/real\">$rshop</a></li>";
        }else{
            $breadcrumbs[] = "<li><a href=\"$base/shop/virtual\">$vshop</a></li>";
        }
        if($productSlug){
            $specialWord = rawurldecode(ucwords(str_replace(Array('-'), Array('&nbsp;'), $last)));
            $title = __($specialWord);
            $breadcrumbs[] = "<li><a href=\"$last\">$title</a></li>";
        }
        
       return implode('', $breadcrumbs);
    }
    
    public function educationbreadcrumbs($separator = ' ',$home, $lang, $slug, $id) {
    
        $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
        unset($path[1]);
        $key = array_search($id, $path);
        unset($path[$key]);
        $key1 = array_search('courses', $path);
        //unset($path[$key1]);
        $base = ('http') . '://' . $_SERVER['HTTP_HOST'] . '/'.$lang;
        $breadcrumbs = Array("<li><a href=\"$base\">$home</a></li>");
    
        $last = end(($path));
        
        foreach ($path AS $x => $crumb) {
            $specialWord = rawurldecode(ucwords(str_replace(Array('-'), Array('&nbsp;'), $crumb)));
            $title = __($specialWord);
            if($crumb == 'courses'){ 
                $breadcrumbs[] = "<li><a href=\"$base/$crumb/$id\">$title</a></li>";
            }else{
            if ($x != $last)
                $breadcrumbs[] = "<li><a href=\"$base/courses/$crumb/$id\">$title</a></li>";
            else
                $breadcrumbs[] = $title;
            }
        }
        if($slug){
            $breadcrumbs[] = "<li><a href=\"$base/courses/course-registration/$id\">$slug</a></li>";
        }
        return implode($separator, $breadcrumbs);
    }
    public static function getUserCommentSession()
    {
        $container = new Container('comment_session');
        if ($container->offsetExists('CommentUserSession')) {
            return $container->offsetGet('CommentUserSession');
        }
        return false;
    }
    public static function setUserCommentSession($sessionUser)
    {
        $container = new Container('comment_session');
        $container->offsetSet('CommentUserSession', $sessionUser);
    }
    public static function replaceStringByDotInPersianText($text, $count) {
    
        if (strlen($text)>$count) {
            return  mb_substr($text, 0, $count, 'utf-8'). '...';
        } else  {
            return $text;
        }
    
    }
    
    public static function getweekname($count)
    {
         
        switch ($count) {
            case '1' :
                return ("هفته اول");
            case '2' :
                return ("هفته دوم");
            case '3' :
                return ("هفته سوم");
            case '4' :
                return ("هفته چهارم ");
            case '5' :
                return ("هفته پنجم");
            case '6' :
                return ("هفته ششم");
            case '7' :
                return ("هفته هفتم");
            case '8' :
                return ("هفته هشتم");
            case '9' :
                return ("هفته نهم");
            case '10' :
                return ("هفته دهم");
            case '11' :
                return ("هفته یازدهم");
            case '12' :
                return ("هفته دوازدهم");
            case '13' :
                return ("هفته سیزدهم");
            case '14' :
                return ("هفته چهاردهم");
            case '15' :
                return ("هفته پانزدهم");
            case '16' :
                return ("هفته شانزدهم");
            case '17' :
                return ("هفته هفدهم");
            case '18' :
                return ("هفته هجدهم");
            case '19' :
                return ("هفته نوزدهم");
            case '20' :
                return ("هفته بیستم");
            case '21' :
                return ("هفته بیست و یکم");
            case '22' :
                return ("هفته  بیست و دوم");
            case '23' :
                return ("هفته  بیست و سوم");
            case '24' :
                return ("هفته  بیست و چهارم ");
            case '25' :
                return ("هفته  بیست و پنجم");
            case '26' :
                return ("هفته  بیست و ششم");
            case '27' :
                return ("هفته  بیست و هفتم");
            case '28' :
                return ("هفته  بیست و هشتم");
            case '29' :
                return ("هفته  بیست و نهم");
            case '30' :
                return ("هفته سی ام");
            case '31' :
                return ("هفته سی و یکم ");
            case '32' :
                return ("هفته سی و دوم");
            case '33' :
                return ("هفته سی و سوم");
            case '34' :
                return ("هفته سی و چهارم");
            case '35' :
                return ("هفته سی و پنجم");
            case '36' :
                return ("هفته سی و ششم");
            case '37' :
                return ("هفته سی و هفتم");
            case '38' :
                return ("هفته سی و هشتم");
            case '39' :
                return ("هفته سی و نهم");
            case '40' :
                return ("هفته چهلم");
        }
         
    }
    
//     public function setCartSession($searchValue){
//         $session = new Container('cartSession');
//         $session->offsetSet('contentCartSession', $searchValue);
        
//     }
    public static function getCartSession(){
        $session = new Container('cart_session');
        if($session->offsetExists('contentCartSession')){
            return $session->offsetGet('contentCartSession');
        }
    }
    
    public static function getCartServicesSession(){
        $session = new Container('cart_services_session');
        if($session->offsetExists('contentCartServicesSession')){
            return $session->offsetGet('contentCartServicesSession');
        }
    }
    
    public static function setCartSession($sessionData)
    {
        $container = new Container('cart_session');
        $container->offsetSet('contentCartSession', $sessionData);
    }
    
    public static function setCartServicesSession($sessionData)
    {
        $container = new Container('cart_services_session');
        $container->offsetSet('contentCartServicesSession', $sessionData);
    }
    
    public static function removeSession()
    {
//         $sessionData = array();
//         $container = new Container('cart_session');
//         $container->offsetSet('contentCartSession', $sessionData); 
        unset($_SESSION['cart_session']); 
        unset($_SESSION['cart_services_session']); 
    }
}