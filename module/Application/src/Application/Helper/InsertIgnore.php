<?php
namespace Application\Helper;

class InsertIgnore extends \Zend\Db\Sql\Insert 
{
    /**
     * @var bool
     */
    public $ignore = false;
    
    public function getSqlString(\Zend\Db\Adapter\Platform\PlatformInterface $adapterPlatform = null) {
        if ($this->ignore !== true) {
            return parent::getSqlString($adapterPlatform);
        }
        return preg_replace('/^INSERT/', 'INSERT IGNORE', parent::getSqlString($adapterPlatform), 1);
    }

}

?>