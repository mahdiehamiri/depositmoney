<?php 

namespace Application\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Application\Model\PermissionsTable;
use Application\Model\UserGroupPermissionsTable;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Permission\Model\PermissionPermissionsTable;

class Permissions extends AbstractPlugin  implements ServiceLocatorAwareInterface
{
	public function getPermissions($permissionName = null)
	{ //return 'a';
// 		$controller = $this->getController();
// 		$permissionsTable = new PermissionsTable($this->getServiceLocator());
// 		$defaultPermissions = $permissionsTable->getDefaultPermission();
// 		if ($permissionName) {
// 			foreach ($defaultPermissions as $permission) {
// 			    if (preg_match("/" . $permission["permission_name"] . "/i", $permissionName)) {
// 			        return true;
// 			    }
// 				if ($permissionName == $permission['permission_name']) {
// 					return true;
// 				}
// 			}
// 		}
// 		$userData = $controller->userData;
// 		if ($userData) {
//             $userGroupPermissionsTable = new UserGroupPermissionsTable($this->getServiceLocator());
// 			$permissions = $userGroupPermissionsTable->getPermissions($userData->user_group_id);
// 			if ($permissions) {
// 				$userPermissions = array();
// 				foreach ($permissions as $index => $permission) {
// 					$userPermissions[$index] = $permission['permission_name'];
// 				    if (preg_match("/" . $permission["permission_name"] . "/i", $permissionName)) {
// 					    return true;
// 					}
// 				}
// 				if ($permissionName) {
// 					return in_array($permissionName, $userPermissions);
// 				}
// 				return $userPermissions;
// 			}
// 		}
// 		return false;


	    $controller = $this->getController();
	    $userData = $controller->userData;

	    if ($userData) {
	        $permissionPermissionsTable = new PermissionPermissionsTable($this->getServiceLocator());
	        $permissions = $permissionPermissionsTable->getPermissions($userData->user_group_id);
	        //var_dump($permissions);die;
	        if ($permissions) {
	            $userPermissions = array();
	            foreach ($permissions as $index => $permission) {
	                $userPermissions[$index] = $permission['perm_type'];
	                if ($permission["perm_type"] == $permissionName) {
	                    return true;
	                }
	            }
	            if ($permissionName) {
	                return in_array($permissionName, $userPermissions);
	            }
	            //var_dump($userPermissions);die;
	            return $userPermissions;
	        }
	    }
	    return false;
	    
	}	
	
	public function getServiceLocator() {
	    return $this->serviceLocator->getServiceLocator();
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
	    $this->serviceLocator = $serviceLocator;
	}
}