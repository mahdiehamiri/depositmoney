<?php

namespace Application\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Application\Services\Authentication;

class SMSService extends AbstractPlugin 
{
	public $serviceManager;
	
	public function send($message, $to, $from = false) 
	{
	    return true;
		$result = array();
		$status = $msg = false;
		
		if ($to == '') {
			$status = false;
			$msg = __("Receiver Cannot be empty.");
		} else {
			if ($from == false) {
				$from = '50002371897';
			}
			
			$configs = array(
					'username'      => '',
					'password'      => '',
					'domain'        => 'cadafzar.ir',
					'sendernumber'  => $from,
			);
			try {
			    $client = new \SoapClient('http://ws.upayamak.com/webservice/soap/wsdl');
			    $client->soap_defencoding = 'UTF-8';
			    $client->decode_utf8 = false;
			    $hasAccess = $client->setCredentials($configs['username'], $configs['password']);
			    
			    if (!$hasAccess) {
			        $status = false;
			        $msg = __("Username or Password is Invalid. Authentication Failed.");
			    } else {
			        $messageBodies      = is_array($message) ? $message : array($message);
			        $recipientNumbers   = is_array($to) ? $to : array($to);
			        $senderNumbers      = array($configs['sendernumber']) ;
			    
			        $result = array();
		            $msg = $client->enqueue($configs['domain'], $messageBodies , $recipientNumbers , $senderNumbers);
		            	
		            if (is_array($msg) && $msg[0] > 109) {
		                $status = true;
		            }
			    }
			    
			} catch (\Exception $e) {
			    $status = false;
			    $msg = $e->getMessage();
			}
			
	
		}
		$result['status'] = $status;
		$result['message']  = $msg;
		return $result;
	}
}
