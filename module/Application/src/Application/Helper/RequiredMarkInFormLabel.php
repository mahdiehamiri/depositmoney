<?php
namespace Application\Helper;

use Zend\Form\View\Helper\FormLabel as OriginalFormLabel;
use Zend\Form\ElementInterface;

/**
 * Add mark (*) for all required elements inside a form.
 */
class RequiredMarkInFormLabel extends OriginalFormLabel
{
     /**
     * Invokable
     *
     * @return str
     */    
    public function __invoke(ElementInterface $element = null, $labelContent = null, $position = null)
    {

        // invoke parent and get form label
        $originalformLabel = parent::__invoke($element,$labelContent,$position);

        // check if element is required
        if ($element->hasAttribute('required')) {
            // add a start to required elements
            return '<span class="required-mark">*</span>' . $originalformLabel;
        }else{
            // not start to optional elements
            return  $originalformLabel;
        }
    }
}