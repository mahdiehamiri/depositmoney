<?php
namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;

class SocialMediaHelper extends AbstractHelper
{
    public function share($socialMedia, $options = array())
    {
        if ($socialMedia && $options) {
            $baseUrl = '';
            switch ($socialMedia) {
                case 'facebook':
                    $baseUrl = 'https://www.facebook.com/sharer/sharer.php';
                    break;
                case 'twitter':
                    $baseUrl = 'https://twitter.com/intent/tweet';
                    break;
                case 'g-plus':
                    $baseUrl = 'https://plus.google.com/share';
                    break;
                    break;
                case 'telegram':
                    $baseUrl = 'https://telegram.me/share/url';
                    break;
            }
            $baseUrl .= '?';
            foreach ($options as $key => $option) {
                @$url .= $key . '=' . $option . '&';
            }
            return $baseUrl . $url;
        }
    }
}