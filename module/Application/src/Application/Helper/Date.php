<?php 
namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;

class Date extends AbstractHelper
{
	public function persianDate($time = null, $format = 'Y/m/d H:i:s')
	{
		if (!$time) {
			$time = time();
		}
		return Jdates::jdate($time, $format);
	}	
}