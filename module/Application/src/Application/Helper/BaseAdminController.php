<?php
namespace Application\Helper;

use Zend\Mvc\MvcEvent;
class BaseAdminController extends BaseController
{
    public  $userData;
    
    public function onDispatch(MvcEvent $e)
    { 
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData = $this->authenticationService->isLoggedIn();
      
        if (!$this->userData) {
            return $this->redirect()->toRoute('home');
        }
        if (in_array($this->userData->user_group_id, array('7','8','9'))) {
            return $this->redirect()->toRoute('application',array("action" =>"dashboard"));
        }
        
        $sm = $e->getApplication()->getServiceManager();
        $router = $sm->get('router');
        $request = $sm->get('request');
        $matchedRoute = $router->match($request);
        $params = $matchedRoute->getParams();
        $module_array = explode('\\', $params["__NAMESPACE__"]);
        $controller = $params['controller'];
        $action = $params['action'];
      
        $permissionLink = strtolower($module_array[0] . "-" . $controller . "-" . $action);
   
        $hasPermission = $this->permissions()->getPermissions($permissionLink);

        if (!$hasPermission) {
           $this->flashMessenger()->addErrorMessage(__("You do not have permission to access this page!"));
           return $this->redirect()->toRoute('application', array("lang" => $this->layout()->lang, "action" => "signin"));
        } 
        parent::onDispatch($e);
        $this->layout("layout/" . $this->direction . "_admin_layout.phtml");
 
    }
    
   
}