<?php
namespace Application\Helper;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Language\Model\LanguageLanguagesTable;
use Zend\Validator\AbstractValidator;
use Configuration\Model\ConfigurationTable;
use Application\Registry\Registry;
use Ourservices\Model\AttributesGroupTable;
use Ourservices\Model\PrintServiceOrdersTable;
use Ourservices\Model\AttributesIndexTable;
use User\Model\OfficeTable;
class BaseController extends AbstractActionController
{
    public  $lang;
    public  $direction;
    protected $eventManager;
    
    public function onDispatch(MvcEvent $e)
    {    
        $configurationTable = new ConfigurationTable($this->getServiceLocator());
        $allConfigs = $configurationTable->getRecords();
        $configs = array();
        foreach ($allConfigs as $config) {
            $configs[$config["name"]] = $config["value"];
        }
        $this->configs = $configs;
        $this->eventManager = $this->getServiceLocator()->get('EventManager');
        $languagelanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $defaultLanguage = $languagelanguagesTable->getAllLanguages(array("default" => "1"));
        $this->defaultLang = "fa";
        if ($defaultLanguage) {
            $this->defaultLang = $defaultLanguage[0]["code"];
            $this->defaultLDirection = $defaultLanguage[0]["direction"];
        }
        
        $this->lang = $this->params('lang', $this->defaultLang);
        $this->lang = substr($this->lang, 0,2);
        $getLanguage = $languagelanguagesTable->getAllLanguages(array("code" => $this->lang));
        if ($getLanguage) {
            $this->direction = $getLanguage[0]["direction"];
        }
        $this->layout("layout/layout.phtml");
        $this->layout()->lang = $this->lang;
        
        $GLOBALS["lang"] = $this->lang;
        $GLOBALS["dlang"] = $this->defaultLang;
        $this->layout()->plang = "";
        $this->plang = "";
        if ($this->lang != $this->defaultLang) {
            $this->layout()->plang = "/" . $this->lang;
            $this->plang = "/" . $this->lang;
        }
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $translator->setLocale($this->lang);
        $translator->addTranslationFile(
            "phpArray",
            "vendor/zendframework/zendframework/resources/languages/$this->lang/Zend_Validate.php"
        );
        $translator->addTranslationFile(
            "phpArray",
            "vendor/zendframework/zendframework/resources/languages/$this->lang/Zend_Captcha.php"
        ); 
        $GLOBALS["translator"] = $this->translator = $translator; 
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData = $this->authenticationService->isLoggedIn();
        $this->layout()->userPermissions = $this->permissions()->getPermissions();
        $this->layout()->direction = $this->direction;
        if($this->userData){
            
            if($this->userData->user_group_id == 7){
                $officeTable =  new OfficeTable($this->getServiceLocator());
                $getOffice = $officeTable->getRecordById($this->userData->id);
                $this->layout()->office = $getOffice['office_name'];
            }
        }
         
 
 
// 		$printServiceOrderTable = new PrintServiceOrdersTable($this->getServiceLocator());
//         $listOrders = $printServiceOrderTable->getAll();
//         foreach ($listOrders as $key => $order){
//             $a = json_decode($order['value']); //var_dump($a);die;
//             $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
//             $get = $attributesIndexTable->getAllByPrintService($order['print_service_id']);
//             $i = 0; $i < count($get); $i++){for(
//                 $listOrders[$key]['details'][$get[$i]['attribute_title']] = $a[$i];
//             }
//         }
//         $this->layout()->listOrders = $listOrders;
		
        parent::onDispatch($e);
         
        
    }
    
    protected function setHeadTitle($title = '') {
        if(!empty($title)) {
            $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\PhpRenderer');
            $renderer->headTitle($title);
        }
    }
    /**
     * Generate new token
     * @return string
     */
    public function generateToken($sessionName = 'token')
    {
        $this->container = new Container('token');
        $token = md5(sha1(time().rand()).rand());
        $this->token = $token;
        $this->container->offsetSet($sessionName, $this->token);
        return $token;
    }
    public function getRegistry($methodName, $params = null)
    {
        //	$regisrey = $this->getServiceLocator()->get('Zend\View\Registry\Registry');
        $regisrey = new Registry($this->getServiceLocator ());
    
        return $regisrey->$methodName($params);
    }
    /**
     * Check token with session (check csrf)
     * @param string $token
     * @return boolean
     */
    public function checkToken($token, $sessionName = 'token')
    {
        if ($this->container->offsetExists($sessionName)) {
            $sessionToken = $this->container->offsetGet($sessionName);
            if ($sessionToken == $token) {
                return true;
            }
        }
        return false;
    }
    
}
