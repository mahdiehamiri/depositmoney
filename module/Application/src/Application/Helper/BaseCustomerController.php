<?php
namespace Application\Helper;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Language\Model\LanguageLanguagesTable;
use Zend\Validator\AbstractValidator;
use Configuration\Model\ConfigurationTable;
class BaseCustomerController extends BaseController
{
    public  $lang;
    public  $direction;
    
    public function onDispatch(MvcEvent $e)
    {   
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData = $this->authenticationService->isLoggedIn();
        $this->layout()->userData = $this->userData;
        parent::onDispatch($e);
    }
    
   
    
}
