<?php 

namespace Application\Helper;

use Zend\File\Transfer\Adapter\Http;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Validator\File\FilesSize;
use Zend\Validator\File\Extension;
use Zend\Filter\File\Rename;
use Smspanel\MasterController\MCConnector;
use Zend\Db\Sql\Select;
use RabbitMQManagement\APIClient;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Message;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;

class ImageUploader extends AbstractPlugin
{
	public function UploadFileEdit($data , $validextensions , $uploaddir , $newfileName , $key = null)
	{
		if($data)
		{
			
			$exts = explode(',', $validextensions);
			$FileAdapter = new Http();
			$size = new FilesSize(array('min' => 1));
			$extension = new Extension(array('extension' => $exts));
			$FileAdapter->setValidators(array($size, $extension), $data['name']);
			$ext = pathinfo($data['name'], PATHINFO_EXTENSION);
			$renameFile = new Rename($newfileName.".".strtolower($ext));
			$FileAdapter->setFilters(array($renameFile));
			$FileAdapter->setDestination($uploaddir);
			if($FileAdapter->isValid($data['name'])) {
				 
				$files = glob ($uploaddir . "/" . $newfileName . ".*" );
				if ($files) {
					foreach ( $files as $filename ) {
						@unlink($filename);
					}
				}
				if($FileAdapter->receive($data['name'])) {
					$filename = $FileAdapter->getFileName();
					return array(true , $filename );
				}
			}else{
				return array(false , $FileAdapter);
			}
		}
	}
	 
	public function UploadFile($data , $validextensions , $uploaddir , $newfileName , $createThumb = false, $ThumbImageMaxWidth = 100, $ThumbImageMaxHeight = 100)
	{
		if($data)
		{
		 
		 
			if (!is_dir($uploaddir)) {
				if (false === @mkdir($uploaddir, 0777, true)) {
					throw new \RuntimeException(sprintf('Unable to create the %s directory', $uploaddir));
				}
			}
		    if ($validextensions) {
		        $exts = explode(',', $validextensions);
		        $extension = new Extension(array('extension' => $exts));
		    } else {
		        $extension =array();
		    }
			
			$FileAdapter = new Http();
			$size = new FilesSize(array('min' => 1));
			
			$FileAdapter->setValidators(array($size, $extension), $data['name']);
			$ext = pathinfo($data['name'], PATHINFO_EXTENSION);
			$renameFile = new Rename($newfileName.".".strtolower($ext));
			$FileAdapter->setFilters(array($renameFile));
			$FileAdapter->setDestination($uploaddir);
			if($FileAdapter->isValid($data['name'])) {
				if($FileAdapter->receive($data['name'])) {
					$filename = $newfileName.".".strtolower($ext);
                    if ($createThumb) {
                        $this->generateImageThumbnail($uploaddir . "\\" .  $filename, $uploaddir . "\\thumb\\" . $filename, $ThumbImageMaxWidth, $ThumbImageMaxHeight);
                    }
					return array(true , $filename );
				}
			}else{
				return array(false , $FileAdapter);
			}
		}
	}
	
    public	function generateImageThumbnail($sourceImagePath, $thumbnailImagePath, $ThumbImageMaxWidth = 100, $ThumbImageMaxHeight = 100)
        {
            list($source_image_width, $source_image_height, $source_image_type) = getimagesize($sourceImagePath);
            switch ($source_image_type) {
                case IMAGETYPE_GIF:
                    $source_gd_image = imagecreatefromgif($sourceImagePath);
                    break;
                case IMAGETYPE_JPEG:
                    $source_gd_image = imagecreatefromjpeg($sourceImagePath);
                    break;
                case IMAGETYPE_PNG:
                    $source_gd_image = imagecreatefrompng($sourceImagePath);
                    break;
            }
            if ($source_gd_image === false) {
                return false;
            }
            $source_aspect_ratio = $source_image_width / $source_image_height;
            $thumbnail_aspect_ratio = $ThumbImageMaxWidth / $ThumbImageMaxHeight;
            if ($source_image_width <= $ThumbImageMaxWidth && $source_image_height <= $ThumbImageMaxHeight) {
                $thumbnail_image_width = $source_image_width;
                $thumbnail_image_height = $source_image_height;
            } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
                $thumbnail_image_width = (int) ($ThumbImageMaxHeight * $source_aspect_ratio);
                $thumbnail_image_height = $ThumbImageMaxHeight;
            } else {
                $thumbnail_image_width = $ThumbImageMaxWidth;
                $thumbnail_image_height = (int) ($ThumbImageMaxWidth / $source_aspect_ratio);
            }
            $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
            imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
            imagejpeg($thumbnail_gd_image, $thumbnailImagePath, 90);
            imagedestroy($source_gd_image);
            imagedestroy($thumbnail_gd_image);
            return true;
    }
}
?>