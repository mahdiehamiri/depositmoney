<?php
namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Application\Form\LoginForm;
use Application\Form\RegistrationForm;
use Application\Form\ForgotPasswordForm;
use Application\Form\ResetPasswordForm;
use Application\Model\UsersTable;
use Application\Helper\BaseController;
use Zend\Mvc\MvcEvent;
use Application\Form\GetRestPasscodeForm;
use Application\Form\GetVerifyCodeForm;
use Application\Model\EducationFieldofstudyTable;
use Application\Model\EducationLessonsTable;
use Application\Model\ShopProductsTable;
use Application\Model\UsersDataTable;
use Application\Form\LoginSigninForm;
use Zend\EventManager\EventManager;
use Application\Helper\Jdates;
use Application\Helper\BaseCustomerController;
use Contact\Form\ContactForm;
use Application\Form\RequestProjectForm;
use Configuration\Model\ConfigurationTable;
use Application\Form\RequestTeachingForm;
use Application\Model\UsersLoginLogTable;
use Language\Model\LanguageLanguagesTable;
use Application\Form\ExecutiveCooperationForm;
use Application\Model\BlogPostsTable;
use Application\Model\BlogCategoriesTable;
use Application\Model\PagePageTable;
use Shop\Model\ShopCategoriesTable;
use Ourservices\Model\ServicesTable;
use Zend\Db\Sql\Where;
use Link\Model\LinkTable;
use Application\Model\ServiceTable;
use Application\Model\ServiceIndexTable;
use Newsletter\Model\NewsletterTable;
use Shop\Model\CategoriesTable;
use Shop\Form\ProductsForm;
use Shop\Model\ProductsTable;
use User\Form\UserForm;
use User\Form\OfficeForm;
use User\Model\OfficeTable;
use User\Form\CompanyForm;
use User\Model\CompanyTable;
use User\Model\AreaTable;
use User\Model\DistrictTable;
use Printservices\Form\PrintServiceForm;
use Printservices\Model\PrintServicesTable;
use Printservices\Form\AttributesForm;
use Printservices\Model\AttributesTable;
use Printservices\Model\AttributesValueTable;
use Printservices\Model\AttributesIndexTable;
use Printservices\Form\AttributesValueForm;
use Printservices\Model\AttributesGroupTable;
use Printservices\Model\PrintServiceOrderTable;
use Shop\Model\ProductsOrderTable;
use Printservices\Model\OrdersFileTable;
use Printservices\Model\OrdersTable;
use Printservices\Model\OrderTable;
use Printservices\Model\OrdersIndexTable;
use Printservices\Model\OrderIndexTable;
use Application\Helper\GeneralHelper;
use Application\Helper\Fpdf;
use Printservices\Model\ServicesTypesTable;
use User\Model\DesignerTable;
use Shop\Form\DesignerSampleForm;
use Shop\Model\DesignerSampleTable;

class IndexController extends BaseCustomerController
{

    protected $latestBlogPostsLimit = 3;

    protected $latestBlogEventsLimit = 1;

    public function indexAction()
    {
//        $cache = $this->getServiceLocator()->get('cache');
//        $cache->setItem('adminPriceCache', '');
//        $a = $cache->clearByNamespace('adminPrice');
       //var_dump($a);die;
        //$cache->clean();
        $this->setHeadTitle('پرتال واریز وجه شبدیز');
        $view = array();
        $lang = $this->lang;
        $view['lang'] = $lang;
        /* blog */
        $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
        /* blog */
        $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
        // news en
        if ($this->lang == "en") {
            $newsCatId = 4;
            $blogCatId = 3;
        } else {
            $newsCatId = 2;
            $blogCatId = 1;
        }
        $blogPosts = $blogPostsTable->getLastestBlogPosts($this->latestBlogPostsLimit, $blogCatId);
        $blogNews = $blogPostsTable->getLastestBlogPosts($this->latestBlogEventsLimit, $newsCatId);
        
        $view['blogPosts'] = $blogPosts;
        $view['blogNews'] = $blogNews;
        
        $eventManager = $this->eventManager;
        $eventManager->trigger( 'myUserEventLog', $this, array (
            'parameter' => null,
            'route' => 'application-index-index'
        ));
        
        // GET SERVICE DATA
        $servicesTable = new ServicesTable($this->getServiceLocator());
        $lang = $this->params('lang');
        
        $where = new Where();
        $where->equalTo('active', 1);
        if ($lang) {
            $where->equalTo('lang', $lang);
        }
        $services = $servicesTable->getServiceHomePage($where, 4);
        // $view['position'] = $position;
        $view['services'] = $services;
        // GET CATEGORY DATA
        
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $productsTable = new ProductsTable($this->getServiceLocator());
        $categories = $categoriesTable->fetchAll();
        foreach ($categories as $key => &$category) {
           
            $productList = $productsTable->getByCategory([$category['id']]);
            
            $category['products'] = $productList;
            
        }
//         var_dump($category);die;
//         var_dump($categories);die;
//         $productsTable = new ProductsTable($this->getServiceLocator());
//         $productList = $productsTable->getByCategory($postData['category']);
        
        $view['categories'] = $categories;
//         var_dump($categories);die;
//         $where = array();
//         $categoryShop = new ShopCategoriesTable($this->getServiceLocator());
//         $productShop = new ShopProductsTable($this->getServiceLocator());
//         $categoryShopData = $categoryShop->fetchAllByParentId($where);
//         foreach ($categoryShopData as $key => &$category) {
//             if ($category['parent_id'] == 0) {
//                 unset($categoryShopData[$key]);
//             } else {
//                 $products = $productShop->getProductByCategoryId($category['id']);
//                 $category['products'] = $products;
//             }
//         }
//         $view['categoryShopData'] = $categoryShopData;
        return new ViewModel($view);
        $this->layout()->indexFooter = true;
        
        $languageTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allLangs = $languageTable->getAllLanguages();
        $view = array(
            "lang" => $this->lang,
            "allLang" => $allLangs
        );
        $this->layout()->setVariable('allLang', $allLangs);
        
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'];
        
        $dir = $uploadDir . "/captcha/";
        foreach (glob($dir . "*") as $filename) {
            // echo time();
            if (filectime($filename) < (time() - 1400)) { // 86400 = 60*60*24
                                                       // var_dump($filename);
                @unlink($filename);
            }
        }
        
        $dataDir = $uploadDir . '/../data/ZfcDatagrid/';
        foreach (glob($dataDir . "*") as $filename) {
            // echo time();
            if (filectime($filename) < (time() - 86400)) { // 86400 = 60*60*24
                                                        // var_dump($filename);
                @unlink($filename);
            }
        }
        return new ViewModel($view);
    }

    public function signinbarAction()
    {
        $this->setHeadTitle(__('Signin form'));
        if (isset($this->userData)) {
            return $this->redirect()->toRoute('application-admin');
        }
        $loginForm = new LoginForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $loginForm->setData($postData);
            if ($loginForm->isValid()) {
                $postData = $loginForm->getData();
                $authStatus = $this->authenticationService->auth($postData['username'], $postData['password'], $postData["remember_me"]);
                if ($authStatus === true) {
                    $usersTable = new UsersTable($this->getServiceLocator());
                    $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                    $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                    
                    $userInfo = $usersTable->getRecords(array(
                        "id" => $this->identity()->id
                    ));
                    if ($userInfo && $userInfo[0]["mobile"]) {
                        $userIp = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?: getenv('HTTP_X_FORWARDED') ?: getenv('HTTP_FORWARDED_FOR') ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');
                        $message = __("You logged in: ") . Jdates::jdate(time()) . ", " . __("Your IP: ") . $userIp;
                        // $this->SMSService()->send($message, $userInfo[0]["mobile"]);
                    }
                    
                    return $this->redirect()->toRoute('application-admin');
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Your username or password is incorrect."));
                }
            }
        }
        
        $this->flashMessenger()->addErrorMessage(__("Form is not valid."));
        return $this->redirect()->toRoute('application', array(
            "action" => "signin"
        ));
    }

    public function signinAction()
    {
        
        $this->setHeadTitle(__('Signin form'));
        if (isset($this->userData)) {
            return $this->redirect()->toRoute('application-admin');
        }
        $loginSigninForm = new LoginSigninForm();
        $request = $this->getRequest();
        if ($request->isPost()) { // var_dump($_SESSION);die;
           // var_dump('hhhh');die;
            $postData = $request->getPost();
            $loginSigninForm->setData($postData);
            if ($loginSigninForm->isValid()) {
                $postData = $loginSigninForm->getData();
                $authStatus = $this->authenticationService->auth($postData['username'], $postData['password'], $postData["remember_me"]);
                //var_dump($authStatus);die;
                if ($authStatus === true) {
                    $usersTable = new UsersTable($this->getServiceLocator());
                    $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                    $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                    $userInfo = $usersTable->getRecords(array(
                        "id" => $this->identity()->id
                    ));
                    if ($userInfo && $userInfo[0]["mobile"]) {
                        $userIp = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?: getenv('HTTP_X_FORWARDED') ?: getenv('HTTP_FORWARDED_FOR') ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');
                        $message = __("You logged in: ") . Jdates::jdate(time()) . ", " . __("Your IP: ") . $userIp;
                        $this->SMSService()->send($message, $userInfo[0]["mobile"]);
                    }
                    //if ($_SESSION['cart_services_session'] || $_SESSION['cart_session']) {
                       // return $this->redirect()->toUrl("/request-cart");
                   // } else {
                        var_dump('hellooooo');//die;
                        return $this->redirect()->toRoute('application-admin');
                    //}
                } else {
                    $this->layout()->errorMessage = __("Your username or password is incorrect.");
                }
            }
        }
        $view['loginSigninForm'] = $loginSigninForm;
        return new ViewModel($view);
    }

    public function signupAction()
    {
        $this->setHeadTitle(__('Signup form'));
        if (isset($this->userData)) {
            return $this->redirect()->toRoute('application-admin');
        }
        $registrationForm = new RegistrationForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $registrationForm->setData($postData);
            if ($registrationForm->isValid()) {
                $config = $this->getServiceLocator()->get('Config');
                $usersTable = new UsersTable($this->getServiceLocator());
                $userId = $usersTable->registerNewUser((array) $registrationForm->getData(), $config['default_user_group']);
                $authStatus = $this->authenticationService->auth($postData['email'], $postData['password']);
                if ($userId == 'user_exists') {
                    $this->layout()->errorMessage = __("This email is taken. Please choose another one.");
                } elseif ($authStatus && $userId !== false) {
                    $usersDataTable = new UsersDataTable($this->getServiceLocator());
                    $userData['firstname'] = "";
                    $userData['lastname'] = "";
                    $userData['experience'] = "";
                    $userData['research'] = "";
                    $userData['aboutme'] = "";
                    $userData['lang'] = $this->lang;
                    $usersDataTable->addUserData($userData, $userId);
                    $this->authenticationService->auth($postData['email'], $postData['password']);
                    
                    $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                    $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                    
                    $message = 'Congratulation! You registerd successfully in Cadafzar. your username and password are: <br />' . $postData['email'] . '<br />' . $postData['password'];
                    $this->SMSService()->send($message, $postData['mobile']);
                    
                    $bodyText = 'Congratulation! You registerd successfully in Cadafzar. your username and password are: <br />' . $postData['email'] . '<br />' . $postData['password'];
                    $this->mailService()->sendMail($bodyText, 'Welcome message', $postData['email']);
                    
                    if ($postData['group']) {
                        return $this->redirect()->toUrl('/' . $this->lang . '/admin-instructor/add-bulk-user');
                    } else {
                        $this->flashMessenger()->addSuccessMessage(__("Your registration is completed, you can sign in throught the following form."));
                        return $this->redirect()->toRoute('application-admin');
                    }
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            }
        }
        
        $view['registrationForm'] = $registrationForm;
        return new ViewModel($view);
    }

    public function forgotPasswordAction()
    {
        $this->setHeadTitle(__('Password recovery form'));
        $forgotPasswordForm = new ForgotPasswordForm();
        $view['forgotPasswordForm'] = $forgotPasswordForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            if (! empty($postData['email'])) {
                $forgotPasswordForm->setData($postData);
                if ($forgotPasswordForm->isValid()) {
                    $hash = md5(sha1(rand(1000000, 9000000)));
                    $postData = $forgotPasswordForm->getData();
                    $usersTable = new UsersTable($this->getServiceLocator());
                    $isRegistered = $usersTable->forgotPassword($postData, $hash);
                    if ($isRegistered == 'no_user_exists') {
                        $this->layout()->errorMessage = __("This Nomber is taken. Please choose another one.");
                    } else {
                        $config = $this->getServiceLocator()->get('Config');
                        $bodyText = $config["site_url"] . '/reset-password/hash/' . $hash;
                         $this->mailService()->sendMail($bodyText, 'Password recovery', $postData['email']);
						//var_dump($a);die;
                        $this->flashMessenger()->addSuccessMessage(__("The password recovery link has been sent."));
                    }
                }
            } elseif (! empty($postData['mobile'])) {
                $forgotPasswordForm->setData($postData);
                if ($forgotPasswordForm->isValid()) {
                    $hash = md5(sha1(rand(1000000, 9000000)));
                    $hash = substr($hash, 0, 2) . substr($hash, - 3);
                    $postData = $forgotPasswordForm->getData();
                    $usersTable = new UsersTable($this->getServiceLocator());
                    $isRegistered = $usersTable->forgotPasswordMobile($postData['mobile'], $hash);
                    
                    if ($isRegistered == 'no_user_exists') {
                        $this->layout()->errorMessage = __("This email is taken. Please choose another one.");
                    } else {
                        $config = $this->getServiceLocator()->get('Config');
                        $message = 'Your password recovery code from CadAfzar is:<br />' . $hash;
                        $result = $this->SMSService()->send($message, $postData['mobile']);
                        
                        if (! $result['status']) {
                            print $result['message'];
                        } else {
                            $this->flashMessenger()->addSuccessMessage(__("Secure code sent to your phone. please enter it"));
                            return $this->redirect()->toUrl('/mobile-reset-password');
                        }
                    }
                }
            }
        }
        return new ViewModel($view);
    }

    public function mobileResetPasswordAction()
    {
        $this->setHeadTitle(__('Reset password form'));
        $getRestPasscodeForm = new GetRestPasscodeForm();
        $view['resetPasswordForm'] = $getRestPasscodeForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $getRestPasscodeForm->setData($postData);
            if ($getRestPasscodeForm->isValid()) {
                $postData = $getRestPasscodeForm->getData();
                return $this->redirect()->toRoute('application', array(
                    'action' => 'reset-password',
                    'params' => $postData['code']
                ));
            }
        }
        return new ViewModel($view);
    }

    public function verifyUserMobileAction()
    {
        $this->setHeadTitle(__('Verify account form'));
        $getVerifyCodeForm = new GetVerifyCodeForm();
        $view['getVerifyCodeForm'] = $getVerifyCodeForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $getVerifyCodeForm->setData($postData);
            if ($getVerifyCodeForm->isValid()) {
                $postData = $getVerifyCodeForm->getData();
                $usersTable = new UsersTable($this->getServiceLocator());
                if (! $usersTable->getVerifyHashMobile($postData['code'])) {
                    $this->layout()->errorMessage = __("Invitation link has been expired.");
                } else {
                    $verifyBulkUser = $usersTable->verifyBulkUserMobile($postData['code']);
                    if ($verifyBulkUser == 'no_user_exists') {
                        $this->flashMessenger()->addErrorMessage(__("Such an email is not registered."));
                    } elseif ($verifyBulkUser == 'no_hash_exists') {
                        $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
                    } else {
                        $this->authenticationService->verifyIt();
                        $this->flashMessenger()->addSuccessMessage(__("Your account is verified successfully."));
                    }
                    return $this->redirect()->toRoute('application-admin');
                }
            }
        }
        return new ViewModel($view);
    }

    public function verifyUserAction()
    {
        $this->setHeadTitle(__('Reset password form'));
        $hash = $this->params()->fromRoute('hash');
        
        $usersTable = new UsersTable($this->getServiceLocator());
        
        if ($hash && ! $usersTable->getVerifyHash($hash)) {
            $this->flashMessenger()->addErrorMessage(__("Invitation link has been expired."));
        } else {
            $verifyBulkUser = $usersTable->verifyBulkUser($hash);
            if ($verifyBulkUser == 'no_user_exists') {
                $this->flashMessenger()->addErrorMessage(__("Such an email is not registered."));
            } elseif ($verifyBulkUser == 'no_hash_exists') {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            } else {
                $this->flashMessenger()->addSuccessMessage(__("Your account is verified successfully, you can sign in throught the following form."));
            }
        }
        return $this->redirect()->toRoute('application', array(
            "lang" => $this->layout()->lang,
            "action" => "signin"
        ));
    }

    public function resetPasswordAction()
    {
        $this->setHeadTitle(__('Reset password form'));
        $param = $this->params()->fromRoute('params');
        
        if (isset($this->params)) {
            $hash = $this->params('params');
        } else {
            $hash = $this->params()->fromRoute('params');
        }
        $usersTable = new UsersTable($this->getServiceLocator());
        
        if (! $usersTable->getHash($hash)) {
            $this->layout()->errorMessage = __("Password recovery link has been expired, try again.");
        } else {
            $resetPasswordForm = new ResetPasswordForm();
            $view['resetPasswordForm'] = $resetPasswordForm;
            $request = $this->getRequest();
            if ($request->isPost()) {
                $postData = $request->getPost();
                $resetPasswordForm->setData($postData);
                if ($resetPasswordForm->isValid()) {
                    $postData = $resetPasswordForm->getData();
                    if ($postData['password'] == $postData['password_repeat']) {
                        $resetPassword = $usersTable->resetPassword($postData, $hash);
                        if ($resetPassword == 'no_user_exists') {
                            $this->flashMessenger()->addErrorMessage(__("Such an email is not registered."));
                        } elseif ($resetPassword == 'no_hash_exists') {
                            $this->layout()->errorMessage = __("Operation failed!");
                        } else {
                            $this->flashMessenger()->addSuccessMessage(__("Your password is recovered successfully, you can sign in throught the following form."));
                            return $this->redirect()->toRoute('application', array(
                                "lang" => $this->layout()->lang,
                                "action" => "signin"
                            ));
                        }
                    } else {
                        $this->flashMessenger()->addErrorMessage(__("Password confirmation must be the same as the password."));
                    }
                }
            }
        }
        return new ViewModel($view);
    }

    public function requestProjectAction()
    {
        die();
        $request = $this->getRequest();
        
        $requestProjectForm = new RequestProjectForm();
        $view['requestProjectForm'] = $requestProjectForm;
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            $data = array_merge($data, $files);
            $requestProjectForm->setData($data);
            if ($requestProjectForm->isValid()) {
                $validData = $requestProjectForm->getData();
                // $contactTable = new ContactTable($this->getServiceLocator());
                // $isInsert = $contactTable->addContact($validData);
                $validData = $requestProjectForm->getData();
                $bodyText = 'ظ†ط§ظ…: ' . $validData['first_name'];
                $bodyText .= "<br />" . ' ظ†ط§ظ… ط®ط§ظ†ظˆط§ط¯ع¯غŒ: ' . $validData['last_name'];
                $bodyText .= "<br />" . ' ظ†ط§ظ… ط´ط±ع©طھ: ' . $validData['company_name'];
                $bodyText .= "<br />" . ' ط§غŒظ…غŒظ„: ' . $validData['email'];
                $bodyText .= "<br />" . ' طھظ„ظپظ†: ' . $validData['tel'];
                $bodyText .= "<br />" . ' ظ…ظˆط¨ط§غŒظ„: ' . $validData['mobile'];
                $bodyText .= "<br />" . ' ظ…ظˆط¶ظˆط¹: ' . $validData['subject'];
                $bodyText .= "<br />" . ' ظ†ظˆط¹ ظ¾ط±ظˆعکظ‡: ' . $validData['project_type'];
                
                $bodyText .= "<br />" . ' ط±ظˆط´ ط§ط¨ط¹ط§ط¯ ط¨ط±ط¯ط§ط±غŒ: ' . $validData['dimension_way'];
                
                $bodyText .= "<br />" . ' ط§غŒط¬ط§ط¯ ظ…ط¯ظ„ ط³ظ‡ ط¨ط¹ط¯غŒ: ' . $validData['create_3d_model'];
                
                $bodyText .= "<br />" . ' ط§غŒط¬ط§ط¯ ظ…ط¯ظ„ ط¯ظˆ ط¨ط¹ط¯غŒ: ' . $validData['create_2d_model'];
                
                $bodyText .= "<br />" . ' ط·ط±ط§ط­غŒ ظ‚ط§ظ„ط¨: ' . $validData['design_cast'];
                $bodyText .= "<br />" . ' طھظˆط¶غŒط­ط§طھ ط·ط±ط§ط­غŒ ظ‚ط§ظ„ط¨: ' . $validData['design_cast_desc'];
                $bodyText .= "<br />" . ' ط³ط§ط®طھ ظ‚ط§ظ„ط¨: ' . $validData['design_cast_desc'];
                $bodyText .= "<br />" . ' ظ†ظ…ظˆظ†ظ‡ ط³ط§ط²غŒ: ' . $validData['prototyping_desc'];
                $bodyText .= "<br />" . ' ع©ظ†طھط±ظ„ ط§ط¨ط¹ط§ط¯غŒ: ' . $validData['dimension_control_desc'];
                $bodyText .= "<br />" . ' طھظˆظ„غŒط¯: ' . $validData['production_desc'];
                $bodyText .= "<br />" . ' ط¯غŒع¯ط± ط®ط¯ظ…ط§طھ: ' . $validData['others_desc'];
                $mails = 'info@cadafzar.com';
                
                $attachmentFiles = false;
                if (isset($files["request_file"])) {
                    $config = $this->serviceLocator->get('Config');
                    $fileNameParts = pathinfo($files["request_file"]['name']);
                    $uploadDir = $config['base_route'] . "/uploads/requestProject";
                    $validationExt = "";
                    $newFileName = $fileNameParts["filename"];
                    $isUploaded = $this->imageUploader()->UploadFile($files["request_file"], $validationExt, $uploadDir, $newFileName);
                    
                    if ($isUploaded[0]) {
                        $attachmentFiles = array(
                            $uploadDir . "/" . $isUploaded[1]
                        );
                    }
                }
                $configurationTable = new ConfigurationTable($this->getServiceLocator());
                $allConfigs = $configurationTable->getRecords();
                $configs = array();
                foreach ($allConfigs as $config) {
                    $configs[$config["name"]] = $config["value"];
                }
                $message = __("You received a new contact");
                $this->SMSService()->send($message, $configs['admin-mobile']);
                $emailSent = $this->mailService()->sendMail($bodyText, 'ط§غŒظ…غŒظ„ ط§ط² ط·ط±ظپ ظپط±ظ… ط¯ط±ط®ظˆط§ط³طھ ظ‡ظ…ع©ط§ط±غŒ', $mails, $attachmentFiles);
                if ($emailSent) {
                    $this->layout()->successMessage = __("Operation done successfully.");
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
                if (isset($attachmentFiles[0])) {
                    @unlink($attachmentFiles[0]);
                }
            }
        }
        $view['breadcrumbs'] = $this->generalHelper()->breadcrumbs($separator = ' ', $home = __('Home'), $this->layout()->plang);
        return new ViewModel($view);
    }

    public function requestTeachingAction()
    {
        $request = $this->getRequest();
        
        $requestTeachingForm = new RequestTeachingForm();
        $view['requestTeachingForm'] = $requestTeachingForm;
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            $data = array_merge($data, $files);
            $requestTeachingForm->setData($data);
            if ($requestTeachingForm->isValid()) {
                $validData = $requestTeachingForm->getData();
                // $contactTable = new ContactTable($this->getServiceLocator());
                // $isInsert = $contactTable->addContact($validData);
                $validData = $requestTeachingForm->getData();
                $bodyText = 'ظ†ط§ظ…: ' . $validData['first_name'];
                $bodyText .= "<br />" . ' ظ†ط§ظ… ط®ط§ظ†ظˆط§ط¯ع¯غŒ: ' . $validData['last_name'];
                $bodyText .= "<br />" . ' ط§غŒظ…غŒظ„: ' . $validData['email'];
                $bodyText .= "<br />" . ' طھظ„ظپظ†: ' . $validData['tel'];
                $bodyText .= "<br />" . ' ظ…ظˆط¨ط§غŒظ„: ' . $validData['mobile'];
                $bodyText .= "<br />" . ' ظ…ظˆط¶ظˆط¹: ' . $validData['subject'];
                $bodyText .= "<br />" . ' ط§ط³طھط§ظ†: ' . $validData['provience'];
                $bodyText .= "<br />" . ' ط´ظ‡ط±: ' . $validData['city'];
                $bodyText .= "<br />" . ' ط±ط´طھظ‡ طھط­طµغŒظ„غŒ: ' . $validData['fieldofStudy'];
                $bodyText .= "<br />" . ' ط¯ط§ظ†ط´ع¯ط§ظ‡: ' . $validData['university'];
                $bodyText .= "<br />" . ' ظ†ظˆط¹ ظ‡ظ…ع©ط§ط±غŒ: ' . $validData['cooperation_type'];
                $bodyText .= "<br />" . ' ط³ط§ط¨ظ‚ظ‡ ع©ط§ط±غŒ: ' . $validData['experience'];
                $bodyText .= "<br />" . ' ط³ظˆط§ط¨ظ‚ ظ¾ط²ظˆظ‡ط´غŒ: ' . $validData['research'];
                $bodyText .= "<br />" . ' ط¨غŒظˆع¯ط±ط§ظپغŒ: ' . $validData['aboutme'];
                $bodyText .= "<br />" . ' ظ†ط­ظˆظ‡ ظ‡ظ…ع©ط§ط±غŒ: ' . $validData['teaching_status'];
                $bodyText .= "<br />" . ' طھظˆط¶غŒط­ط§طھ: ' . $validData['others_desc'];
                $mails = 'info@cadafzar.ir';
                // $mails = 'info@popupblocker.ir';
                $attachmentFiles = false;
                if (isset($files["resume_file"])) {
                    $config = $this->serviceLocator->get('Config');
                    $fileNameParts = pathinfo($files["resume_file"]['name']);
                    $uploadDir = $config['base_route'] . "/uploads/requestTeaching";
                    $validationExt = "";
                    $newFileName = $fileNameParts["filename"];
                    $isUploaded = $this->imageUploader()->UploadFile($files["resume_file"], $validationExt, $uploadDir, $newFileName);
                    
                    if ($isUploaded[0]) {
                        $attachmentFiles = array(
                            $uploadDir . "/" . $isUploaded[1]
                        );
                    }
                }
                
                $configurationTable = new ConfigurationTable($this->getServiceLocator());
                $allConfigs = $configurationTable->getRecords();
                $configs = array();
                foreach ($allConfigs as $config) {
                    $configs[$config["name"]] = $config["value"];
                }
                $message = __("You received a new contact");
                $this->SMSService()->send($message, $configs['admin-mobile']);
                $emailSent = $this->mailService()->sendMail($bodyText, 'ط§غŒظ…غŒظ„ ط§ط² ط·ط±ظپ ظپط±ظ… ط¯ط±ط®ظˆط§ط³طھ ظ¾ط±ظˆعکظ‡', $mails, $attachmentFiles);
                
                if ($emailSent) {
                    $this->layout()->successMessage = __("Operation done successfully.");
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
                if (isset($attachmentFiles[0])) {
                    @unlink($attachmentFiles[0]);
                }
            }
        }
        $view['breadcrumbs'] = $this->generalHelper()->breadcrumbs($separator = ' ', $home = __('Home'), $this->layout()->plang);
        return new ViewModel($view);
    }

    public function executiveCooperationAction()
    {
        $request = $this->getRequest();
        
        $executiveCooperationForm = new ExecutiveCooperationForm();
        $view['requestTeachingForm'] = $executiveCooperationForm;
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            $data = array_merge($data, $files);
            $executiveCooperationForm->setData($data);
            if ($executiveCooperationForm->isValid()) {
                $validData = $executiveCooperationForm->getData();
                $validData = $executiveCooperationForm->getData();
                $bodyText = 'ظ†ط§ظ…: ' . $validData['first_name'];
                $bodyText .= "<br />" . ' ظ†ط§ظ… ط®ط§ظ†ظˆط§ط¯ع¯غŒ: ' . $validData['last_name'];
                $bodyText .= "<br />" . ' ط§غŒظ…غŒظ„: ' . $validData['email'];
                $bodyText .= "<br />" . ' طھظ„ظپظ†: ' . $validData['tel'];
                $bodyText .= "<br />" . ' ظ…ظˆط¨ط§غŒظ„: ' . $validData['mobile'];
                $bodyText .= "<br />" . ' ظ…ظˆط¶ظˆط¹: ' . $validData['subject'];
                $bodyText .= "<br />" . ' ط§ط³طھط§ظ†: ' . $validData['provience'];
                $bodyText .= "<br />" . ' ط´ظ‡ط±: ' . $validData['city'];
                $bodyText .= "<br />" . ' ط±ط´طھظ‡ طھط­طµغŒظ„غŒ: ' . $validData['fieldofStudy'];
                $bodyText .= "<br />" . ' ط¯ط§ظ†ط´ع¯ط§ظ‡: ' . $validData['university'];
                $bodyText .= "<br />" . ' ظ†ظˆط¹ ظ‡ظ…ع©ط§ط±غŒ: ' . $validData['cooperation_type'];
                $bodyText .= "<br />" . ' ط³ط§ط¨ظ‚ظ‡ ع©ط§ط±غŒ: ' . $validData['experience'];
                $bodyText .= "<br />" . ' ط³ظˆط§ط¨ظ‚ ظ¾ط²ظˆظ‡ط´غŒ: ' . $validData['research'];
                $bodyText .= "<br />" . ' ط¨غŒظˆع¯ط±ط§ظپغŒ: ' . $validData['aboutme'];
                $bodyText .= "<br />" . ' ظ†ط­ظˆظ‡ ظ‡ظ…ع©ط§ط±غŒ: ' . $validData['cooperation_status'];
                $bodyText .= "<br />" . ' ظ…ظˆظ‚ط¹غŒطھ ظ‡ط§غŒ ط§ط¬ط±ط§غŒغŒ: ' . implode("طŒ", $validData['executive_fields']);
                $bodyText .= "<br />" . ' ظ…ظˆظ‚ط¹غŒطھ ظ‡ط§غŒ ظپظ†غŒ: ' . implode("طŒ", $validData['technical_fields']);
                $bodyText .= "<br />" . ' طھظˆط¶غŒط­ط§طھ: ' . $validData['others_desc'];
                $mails = 'info@cadafzar.ir';
                // $mails = 'info@popupblocker.ir';
                $attachmentFiles = false;
                if (isset($files["resume_file"])) {
                    $config = $this->serviceLocator->get('Config');
                    $fileNameParts = pathinfo($files["resume_file"]['name']);
                    $uploadDir = $config['base_route'] . "/uploads/executiveCooperation";
                    $validationExt = "";
                    $newFileName = $fileNameParts["filename"];
                    $isUploaded = $this->imageUploader()->UploadFile($files["resume_file"], $validationExt, $uploadDir, $newFileName);
                    
                    if ($isUploaded[0]) {
                        $attachmentFiles = array(
                            $uploadDir . "/" . $isUploaded[1]
                        );
                    }
                }
                
                $configurationTable = new ConfigurationTable($this->getServiceLocator());
                $allConfigs = $configurationTable->getRecords();
                $configs = array();
                foreach ($allConfigs as $config) {
                    $configs[$config["name"]] = $config["value"];
                }
                $message = __("You received a new contact");
                $this->SMSService()->send($message, $configs['admin-mobile']);
                $emailSent = $this->mailService()->sendMail($bodyText, 'ط§غŒظ…غŒظ„ ط§ط² ط·ط±ظپ ظپط±ظ… ط¯ط±ط®ظˆط§ط³طھ ظ‡ظ…ع©ط§ط±غŒ ط§ط¬ط±ط§غŒغŒ', $mails, $attachmentFiles);
                
                if ($emailSent) {
                    $this->layout()->successMessage = __("Operation done successfully.");
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
                if (isset($attachmentFiles[0])) {
                    @unlink($attachmentFiles[0]);
                }
            }
        }
        $view['breadcrumbs'] = $this->generalHelper()->breadcrumbs($separator = ' ', $home = __('Home'), $this->layout()->plang);
        return new ViewModel($view);
    }

    public function searchAction()
    {
        $this->setHeadTitle(__('Search'));
        $request = $this->getRequest();
        $page = $this->params('page');
        $productsList = array();
        $instructorList = array();
        $linkList = new LinkTable($this->getServiceLocator());
        $postData = $request->getPost();
        $data = $postData->toArray();
        if (urldecode($request->getUri()->getQuery())) {
            $data['search'] = urldecode($request->getUri()->getQuery());
            $serchKey = urldecode($request->getUri()->getQuery());
        }
        if (isset($data['search'])) {
            $serchKey = $data['search'];
            $view['query'] = $data['search'];
        } else {
            $serchKey = $data['news'];
            $view['query'] = $data['news'];
        }
        $linkListData = $linkList->searchLink($data['search'], $this->lang);
        if (! $page) {
            $page = 1;
        }
        $linkListData->setCurrentPageNumber($this->params('page', $page));
        $linkListData->setItemCountPerPage(1);
        $pageNo = (int) $this->params('page');
        $linkListData->setCurrentPageNumber($pageNo);
        if (isset($data['search'])) {
            if ($linkListData->getCurrentItemCount()) {
                $view["links"] = $linkListData;
                $view["page"] = $page;
            }
            return new ViewModel($view);
        }
    }

    public function AdvancedSearchAction()
    {
        // $advancedSearchForm = new AdvancedSearchForm();
        // $view['advancedSearchForm'] = $advancedSearchForm;
        // return new ViewModel($view);
    }

    public function sitemapAction()
    {
        $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
        $posts = $blogPostsTable->getRecords(array(
            "post_status" => 1
        ));
        $view['posts'] = $posts;
        
        $blogCategoriesTable = new BlogCategoriesTable($this->getServiceLocator());
        $categories = $blogCategoriesTable->getCategories(array(
            "category_status" => 1
        ));
        $view['categories'] = $categories;
        
        $pagePageTable = new PagePageTable($this->getServiceLocator());
        $pages = $pagePageTable->getRecords(array(
            "page_status" => 1
        ));
        $view['pages'] = $pages;
        return new ViewModel($view);
    }

    public function sampleAction()
    {
        $this->setHeadTitle(__('Template sample'));
        $view = array();
        $sampleId = $this->params('id');
        $view['id'] = $sampleId;
        $viewModel = new ViewModel($view);
        return $viewModel;
    }

    public function internalsampleAction()
    {
        $this->setHeadTitle(__('Internal sample'));
        $view = array();
        $sampleId = $this->params('id');
        
        $view['id'] = $sampleId;
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function aboutAction()
    {
        $this->setHeadTitle(__('about shabdiz'));
        $view = array();
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function designAction()
    {
        $this->setHeadTitle(__('Design'));
        $view = array();
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function developAction()
    {
        $this->setHeadTitle(__('develope'));
        $view = array();
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function mobileAction()
    {
        $this->setHeadTitle(__('Mobile'));
        $view = array();
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function productsDetailesAction()
    {
        $this->setHeadTitle(__('Products detailes'));
        $id = $this->params('id');
        $productsTable = new ProductsTable($this->getServiceLocator());
        $product = $productsTable->fetchById($id);
        $view['product'] = $product;
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function productListAction()
    {
        $this->setHeadTitle(__('shopping factor'));
        $view = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray(); //var_dump(array_sum($data['qty']));die;
            if ($this->userData) {
                if (isset($data['product_order_id'])) {
                    if (! empty($data['address'] && $data['postal_code'])) {
                        $sessionData = GeneralHelper::getCartSession();
                        // var_dump($sessionData);die;
                        $orderData['address'] = $data['address'];
                        $orderData['postal_code'] = $data['postal_code'];
                        $orderData['total_price'] = $data['total'];
                        $orderData['price'] = $data['total'];
                        $orderData['qty'] = $data['sum'];
                        $orderData['delivery_type'] = '';
                        $orderData['delivery_price'] = '';
                        $orderData['discount'] = '';
                        $orderData['user_id'] = $this->userData->id;
                        $orderData['type'] = 'product';
                        $orderTable = new OrderTable($this->getServiceLocator());
                        $orderAdd = $orderTable->add($orderData);
                        foreach ($data['product_order_id'] as $key => $order) {
                            $productOrderTable = new ProductsOrderTable($this->getServiceLocator());
                            $productOrderData['product_id'] = $order;
                            $productOrderData['price'] = $data['price'][$order];
                            $productOrderData['qty'] = $data['qty'][$order];
                            $productOrderData['total_price'] = $data['qty'][$order] * $data['price'][$order];
                            $productOrderData['order_id'] = $orderAdd;
                            $productOrderAdd = $productOrderTable->add($productOrderData);
                            foreach ($sessionData as $k => $v) {
                                if ($v['product_id'] == $order) {
                                    $sessionData[$k]['status'] = '0';
                                }
                            }
                        }
                        
                        GeneralHelper::setCartSession($sessionData);
                        $this->flashMessenger()->addSuccessMessage('سفارش شما ثبت گردید.');
                        return $this->redirect()->toUrl("/product-factor/" . $orderAdd);
                    } else {
                        $this->flashMessenger()->addErrorMessage('لطفا آدرس و کدپستی خود را وارد نمایید.');
                        return $this->redirect()->toUrl("/request-cart");
                    }
                } else {
                    $this->flashMessenger()->addErrorMessage('لطفا سفارشات مورد نظر خود را انتخاب نمایید.');
                    return $this->redirect()->toUrl("/request-cart");
                }
            }else{
               return $this->redirect()->toRoute('application', array(
                    'action' => 'signin'
               )); 
            }
            
        }
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function contactAction()
    {
        $this->setHeadTitle(__('Contact us'));
        $view = array();
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function userRegisterAction()
    {
        $this->setHeadTitle(__('User register'));
        $view = array();
        $serviceTable = new ServiceTable($this->getServiceLocator());
        $services = $serviceTable->getRecords();
        $view['services'] = $services;
        // $officeForm = new OfficeForm($services);
        // $view['officeForm'] = $officeForm;
        $configuration = new ConfigurationTable($this->getServiceLocator());
        $adminEmail = $configuration->getConfigByName('admin-email');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost(); // var_dump($postData);die;
            $usersTable = new UsersTable($this->getServiceLocator());
            $email = $postData['email'];
             $existEmail = $usersTable->getUserByEmail($email);
             if($existEmail){
             $this->layout()->errorMessage = __("User with this email already registered.");
             }
             else {
            $postData['user_group_id'] = '7';
            $isAdded = $usersTable->addUser($postData);
            if ($isAdded) {
                $officeData['online_services'] = 0;
                $officeTable = new OfficeTable($this->getServiceLocator());
                $officeData['user_id'] = $isAdded;
                $officeData['office_name'] = $postData['office_name'];
                $officeData['manager_name'] = $postData['manager_name'];
                $officeData['fax'] = $postData['fax'];
                if ($postData['online_services']) {
                    $officeData['online_services'] = $postData['online_services'];
                }
                $officeData['usual_courier'] = $postData['usual_courier'];
                $officeData['special_courier'] = $postData['special_courier'];
                $officeData['in_place'] = $postData['in_place'];
                $addOffice = $officeTable->add($officeData);
                if ($addOffice) {
                    $serviceTable = new ServiceIndexTable($this->getServiceLocator());
                    foreach ($postData['services'] as $service) {
                        $serviceData['office_id'] = $addOffice;
                        $serviceData['service_id'] = $service;
                        $addService = $serviceTable->add($serviceData);
                        // var_dump($addService);
                    }
                    // die;
                }
                // var_dump($addOffice);die;
                $authStatus = $this->authenticationService->auth($postData['email'], $postData['password']);
                $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                $userBodyText = 'ثبت نام شما با موفقیت انجام شد.' . '<br />' . 'ایمیل: ' . $postData['email'] . '<br />' . 'رمز عبور: ' . $postData['password'];
                $adminBodyText = 'کاربر جدیدی با نام کاربری ' . '(' . $postData['email'] .') در بخش ' .  '(' .  'دفتر فنی' .')';
                // $adminBodyText = 'Congratulation! You registerd successfully in Kafeebazar. you registerd with this email: <br />' . $postData['email'];
                $userSendMail = $this->mailService()->sendMail($userBodyText, 'ثبت نام', $postData['email']);
                $adminSendMail = $this->mailService()->sendMail($adminBodyText, 'کاربر جدید', $adminEmail);
                $this->flashMessenger()->addSuccessMessage("ثبت نام شما  با موفقیت انجام شد.");
                return $this->redirect()->toRoute('application', array(
                    'action' => 'dashboard'
                ));
            } else {
                $this->layout()->errorMessage = __("Registeration failed!");
            }
           }
            
            // var_dump($postData);die;
        }
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function userRegisterSellerAction()
    {
        $this->setHeadTitle(__('User register seller'));
        $view = array();
        $serviceTable = new ServiceTable($this->getServiceLocator());
        $services = $serviceTable->getRecords();
        $view['services'] = $services;
        $configuration = new ConfigurationTable($this->getServiceLocator());
        $adminEmail = $configuration->getConfigByName('admin-email');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost(); // var_dump($postData);die;
            $usersTable = new UsersTable($this->getServiceLocator());
            $email = $postData['email'];
            $existEmail = $usersTable->getUserByEmail($email);
            if ($existEmail) {
                $this->layout()->errorMessage = __("User with this email already registered.");
            } else {
                $postData['user_group_id'] = '8';
                $isAdded = $usersTable->addUser($postData);
                if ($isAdded) {
                    $companyTable = new CompanyTable($this->getServiceLocator());
                    $companyData['user_id'] = $isAdded;
                    $companyData['company_name'] = $postData['company_name'];
                    $companyData['company_history'] = $postData['company_history'];
                    $companyData['manager_name'] = $postData['manager_name'];
                    $companyData['fax'] = $postData['fax'];
                    $addCompany = $companyTable->add($companyData);
                    if ($addCompany) {
                        if ($postData['newsletter']) {
                            $newsLetterTable = new NewsletterTable($this->getServiceLocator());
                            $addNewsletter = $newsLetterTable->addNewsletter($postData['email']);
                        }
                    }
                    $authStatus = $this->authenticationService->auth($postData['email'], $postData['password']);
                    $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                    $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                    $userBodyText = 'ثبت نام شما با موفقیت انجام شد.' . '<br />' . 'ایمیل: ' . $postData['email'] . '<br />' . 'رمز عبور: ' . $postData['password'];
                    $adminBodyText = 'کاربر جدیدی با نام کاربری ' . '(' . $postData['email'] .') در بخش ' .  '(' .  'فروشندگان عمده ' .')';
                    // $adminBodyText = 'Congratulation! You registerd successfully in Kafeebazar. you registerd with this email: <br />' . $postData['email'];
                    $userSendMail = $this->mailService()->sendMail($userBodyText, 'ثبت نام', $postData['email']);
                    $adminSendMail = $this->mailService()->sendMail($adminBodyText, 'کاربر جدید', $adminEmail);
                    $this->flashMessenger()->addSuccessMessage("ثبت نام شما  با موفقیت انجام شد.");
                    return $this->redirect()->toRoute('application', array(
                        'action' => 'dashboard'
                    ));
                } else {
                    $this->layout()->errorMessage = __("Registeration failed!");
                }
            }
        }
        $viewModel = new ViewModel($view);
        return $viewModel;
    }
	
	public function userRegisterDesignerAction()
    {
        $this->setHeadTitle(__('User register designer'));
        $validationCvExt = "pdf,doc,docx,jpeg,jpg";
        $validationSamplesExt = "zip";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/designer/";
        $view = array();
        $request = $this->getRequest();
        $configuration = new ConfigurationTable($this->getServiceLocator());
        $adminEmail = $configuration->getConfigByName('admin-email');
        // var_dump($adminEmail);die;
        // $bodyText = 'Congratulation! You registerd successfully in Kafeebazar. you registerd with this email: <br />' . $postData['email'];
        //                     $mail = $this->mailService()->sendMail($bodyText, 'Welcome message', 'donya.valeh@gmail.com');
        //                     var_dump($mail);die;
        if ($request->isPost()) {
            $postData = $request->getPost(); //var_dump($postData);die;
            $file = $request->getFiles()->toArray();//var_dump($file['cv_file']['tmp_name']);die;
            $usersTable = new UsersTable($this->getServiceLocator());
            if(isset($postData['accept'])){
                if ($postData['password'] == $postData['password_repeat']) {
                    $email = $postData['email'];
                    $existEmail = $usersTable->getUserByEmail($email);
                    if ($existEmail) {
                        $this->layout()->errorMessage = __("User with this email already registered.");
                    } else {
                        $postData['user_group_id'] = '9';
                        $isAdded = $usersTable->addUser($postData);
                        if ($isAdded) {
                            if ($file['cv_file']['tmp_name'] != '') {
                                $newFileName = md5(time() . $file['cv_file']['name']);
                                $isUploaded = $this->imageUploader()->UploadFile($file['cv_file'], $validationCvExt, $uploadDir . 'cv', $newFileName);
                                if ($isUploaded[0] == true) {
                                    $postData['cv_file'] = $isUploaded[1];
                                }
                            }
                            
                            if ($file['work_samples']['tmp_name'] != '') {
                                $newFileName = md5(time() . $file['work_samples']['name']);
                                $isUploaded = $this->imageUploader()->UploadFile($file['work_samples'], $validationSamplesExt, $uploadDir . 'samples', $newFileName);
                                if ($isUploaded[0] == true) {
                                    $postData['work_samples'] = $isUploaded[1];
                                }
                            }
                            
                            $designerTable = new DesignerTable($this->getServiceLocator());
                            $designerData['user_id'] = $isAdded;
                            $designerData['cv_file'] = $postData['cv_file'];
                            $designerData['work_exp'] = $postData['work_exp'];
                            $designerData['work_samples'] = $postData['work_samples'];
                            $addDesignerData = $designerTable->add($designerData);
                            $authStatus = $this->authenticationService->auth($postData['email'], $postData['password']);
                            $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                            $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                            
                            $userBodyText = 'ثبت نام شما با موفقیت انجام شد.' . '<br />' . 'ایمیل: ' . $postData['email'] . '<br />' . 'رمز عبور: ' . $postData['password'];
                            $adminBodyText = 'کاربر جدیدی با نام کاربری ' . '(' . $postData['email'] .') در بخش ' .  '(' .  'طراحان' .')';
                            // $adminBodyText = 'Congratulation! You registerd successfully in Kafeebazar. you registerd with this email: <br />' . $postData['email'];
                            $userSendMail = $this->mailService()->sendMail($userBodyText, 'ثبت نام', $postData['email']);
                            $adminSendMail = $this->mailService()->sendMail($adminBodyText, 'کاربر جدید', $adminEmail);
                            // var_dump($mail);die;
                            $this->flashMessenger()->addSuccessMessage("ثبت نام شما  با موفقیت انجام شد.");
                            return $this->redirect()->toRoute('application', array(
                                'action' => 'dashboard'
                            ));
                        } else {
                            $this->layout()->errorMessage = __("Registeration failed!");
                        }
                    }
                } 
            }else{
                $this->layout()->errorMessage = __("you dont accept rules");
            }
            
        }
        $viewModel = new ViewModel($view);
        return $viewModel;
    }
    public function userRegisterPublicAction()
    {
        $this->setHeadTitle(__('Public user register'));
        $view = array();
        
        $configuration = new ConfigurationTable($this->getServiceLocator());
        $adminEmail = $configuration->getConfigByName('admin-email');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost(); // var_dump($postData);die;
            $usersTable = new UsersTable($this->getServiceLocator());
            $email = $postData['email'];
            $existEmail = $usersTable->getUserByEmail($email);
            if ($existEmail) {
                $this->layout()->errorMessage = __("User with this email already registered.");
            } else {
                $postData['user_group_id'] = '3';
                $isAdded = $usersTable->addUser($postData);
                if ($isAdded) {
                    // $companyTable = new CompanyTable($this->getServiceLocator());
                    // $companyData['user_id'] = $isAdded;
                    // $companyData['company_name'] = $postData['company_name'];
                    // $companyData['company_history'] = $postData['company_history'];
                    // $companyData['manager_name'] = $postData['manager_name'];
                    // $companyData['fax'] = $postData['fax'];
                    // $addCompany = $companyTable->add($companyData);
                    // if($addCompany){
                    if ($postData['newsletter']) {
                        $newsLetterTable = new NewsletterTable($this->getServiceLocator());
                        $addNewsletter = $newsLetterTable->addNewsletter($postData['email']);
                    }
                    // }
                    $authStatus = $this->authenticationService->auth($postData['email'], $postData['password']);
                    $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                    $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                    $userBodyText = 'ثبت نام شما با موفقیت انجام شد.' . '<br />' . 'ایمیل: ' . $postData['email'] . '<br />' . 'رمز عبور: ' . $postData['password'];
                    $adminBodyText = 'کاربر جدیدی با نام کاربری ' . '(' . $postData['email'] .') در بخش ' .  '(' .  'مشتریان عمومی' .')';
                    // $adminBodyText = 'Congratulation! You registerd successfully in Kafeebazar. you registerd with this email: <br />' . $postData['email'];
                    $userSendMail = $this->mailService()->sendMail($userBodyText, 'ثبت نام', $postData['email']);
                    $adminSendMail = $this->mailService()->sendMail($adminBodyText, 'کاربر جدید', $adminEmail);
                    $this->flashMessenger()->addSuccessMessage("ثبت نام شما  با موفقیت انجام شد.");
                    return $this->redirect()->toRoute('application', array(
                        'action' => 'dashboard'
                    ));
                } else {
                    $this->layout()->errorMessage = __("Registeration failed!");
                }
            }
        }
        $viewModel = new ViewModel($view);
        return $viewModel;
    }

    public function technicalOfficeSearchAction()
    {
        $this->setHeadTitle(__('Technical office search'));
        $view = array();
        $serviceTable = new ServiceTable($this->getServiceLocator());
        $services = $serviceTable->getRecords(); // var_dump($services);die;
        $view['services'] = $services;
        $areaTable = new AreaTable($this->getServiceLocator());
        $areaList = $areaTable->getRecords(); // var_dump($areaList);die;
        $view['areaList'] = $areaList;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray(); // var_dump($data);die;
            $officeTable = new OfficeTable($this->getServiceLocator());
            $result = $officeTable->search($data);
            die(json_encode($result));
        }
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function printRequestAction()
    {
        $this->setHeadTitle(__('print Request'));
        $view = array();
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function requestCartAction()
    {
        
        // GeneralHelper::removeSession();
        $this->setHeadTitle(__('Cart'));
        
        $view = array();
        
        $config = new ConfigurationTable($this->getServiceLocator());
        $discount = 0;
        // if($this->userData->user_group_id == '3'){
        // $discount = $config->getConfigByName('users-discount');
        // }elseif($this->userData->user_group_id == '7'){
        // $discount = $config->getConfigByName('office-discount');
        // }
        $view['discount'] = $discount;
        $sessionData = GeneralHelper::getCartSession(); // var_dump($sessionData);die;
        $view['productsOrder'] = $sessionData;
        if ($sessionData) {
            $view['exitListOrders'] = 'exitProductOrder';
        }
        $sessionServicesData = GeneralHelper::getCartServicesSession(); // var_dump($sessionServicesData);die;
        if ($sessionServicesData) {
            foreach ($sessionServicesData as $key => $order) {
                $attrGroupTable = new AttributesGroupTable($this->getServiceLocator());
                $getVal = $attrGroupTable->getById($order['attributes_group_id']);
                $a = json_decode($getVal['value']); // var_dump($a);die;
                $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
                $get = $attributesIndexTable->getAllByPrintService($order['parent']);
                for ($i = 0; $i < count($get); $i ++) {
                    $sessionServicesData[$key]['details'][$get[$i]['attribute_title']] = $a[$i];
                }
                $sessionServicesData[$key]['time'] = $getVal['time'];
                if ($order['office_id'] != 0) {
                    $officeTable = new OfficeTable($this->getServiceLocator());
                    $getOffice = $officeTable->getById($order['office_id']); // var_dump($getOffice);
                    $sessionServicesData[$key]['office_name'] = $getOffice['office_name'];
                } else {
                    $sessionServicesData[$key]['office_name'] = 'دیجی بنیس';
                }
            } // var_dump($sessionServicesData);die;
            $view['listOrders'] = $sessionServicesData;
            $view['exitListOrders'] = 'exitServicesOrder';
        }
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();var_dump($data);die; // var_dump(array_unique(array_values($data['office_id'])));die;
                                                    // var_dump($data['office_id']);die;
            if ($this->userData) {
                if (isset($data['session_id'])) {
                    $office = array_unique(array_values($data['office_id']));
                    if (count($office) === 1) {
                        if (! empty($data['address'] && $data['postal_code'])) {
                            $config = $this->serviceLocator->get('Config');
                            $uploadDir = $config['base_route'] . "/uploads/orders/tmp/";
                            $orderData['address'] = $data['address'];
                            $orderData['postal_code'] = $data['postal_code'];
                            $orderData['total_price'] = $data['total_price'];
                            $orderData['price'] = $data['price'];
                            $orderData['qty'] = '';
                            $orderData['delivery_type'] = $data['delivery_type'];
                            $orderData['delivery_price'] = $data['delivery_price'];
                            $orderData['discount'] = $data['discount'];
                            $orderData['user_id'] = $this->userData->id;
                            $orderData['type'] = 'print services';
                            $orderData['office_id'] = $office[0];
                            $orderTable = new OrderTable($this->getServiceLocator());
                            $orderAdd = $orderTable->add($orderData);
                            foreach ($data['session_id'] as $order) {
                                $printServicesOrderTable = new PrintServiceOrderTable($this->getServiceLocator());
                                $printServiceOrderData['office_id'] = $sessionServicesData[$order]['office_id'];
                                $printServiceOrderData['attributes_group_id'] = $sessionServicesData[$order]['attributes_group_id'];
                                $printServiceOrderData['print_service_id'] = $sessionServicesData[$order]['print_service_id'];
                                $printServiceOrderData['price'] = $sessionServicesData[$order]['price'];
                                $printServiceOrderData['time'] = $sessionServicesData[$order]['time'];
                                $printServiceOrderData['order_id'] = $orderAdd;
                                $printServiceOrderAdd = $printServicesOrderTable->add($printServiceOrderData);
                                foreach ($sessionServicesData[$order]['files'] as $file) {
                                    $upload = rename($config['base_route'] . '/uploads/orders/tmp/' . $file, $config['base_route'] . '/uploads/orders/' . $file);
                                    if ($upload == true) {
                                        $fileData['image'] = $file;
                                        $fileData['order_id'] = $orderAdd;
                                        $orderFilesTable = new OrdersFileTable($this->getServiceLocator());
                                        $filesAdd = $orderFilesTable->add($fileData);
                                    }
                                }
                                $sessionServicesData[$order]['status'] = '0';
                            }
                            GeneralHelper::setCartServicesSession($sessionServicesData);
                            $this->flashMessenger()->addSuccessMessage('سفارش شما ثبت گردید.');
                            return $this->redirect()->toUrl("/orders/" . $orderAdd);
                        } else {
                            $this->layout()->errorMessage = "لطفا آدرس و کدپستی خود را به صورت صحیح وارد نمایید.";
                        }
                    } else {
                        $this->layout()->errorMessage = "لطفا سفارشات با سفارش گیرنده های مشابه را برای ثبت فاکتور انتخاب نمایید.";
                    }
                } else {
                    $this->layout()->errorMessage = "لطفا سفارشات مورد نظر خود را انتخاب نمایید.";
                }
            } else {
                return $this->redirect()->toRoute('application', array(
                    'action' => 'signin'
                ));
            }
        }
        $viewModel = new ViewModel($view);
        return $viewModel;
    }

    // function myfunction($products, $field, $value)
    // {
    // foreach($products as $key => $product)
    // {//var_dump($product[$field]);
    // if ( $product[$field] == $value )
    // return $key;
    // }
    // return false;
    // }
    public function productsAction()
    {
        $this->setHeadTitle(__('Product sell'));
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $categories = $categoriesTable->fetchAll();
        $view['categories'] = $categories;
        $eventManager = $this->eventManager;
        $eventManager->trigger( 'myUserEventLog', $this, array (
            'parameter' => null,
            'route' => 'application-index-products'
        ));
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $sessionData = GeneralHelper::getCartSession();
            if ($sessionData) {
                foreach ($sessionData as $key => $session) {
                    if ($session['product_id'] == $postData['id'] && $session['status'] != 0) {//var_dump('aaa');die;
                        $sessionData[$key]['qty'] = $session['qty'] + 1;
                        $sessionData[$key]['price'] = $session['price'] + $postData['price'];
                        GeneralHelper::setCartSession($sessionData);
                        $postSession['qty'] = $session['qty'] + 1;
                        $postSession['price'] = $sessionData[$key]['price'];
                        die(json_encode($postSession));
                    }
                }
                $sessionData[] = [
                    'product_id' => $postData['id'],
                    'price' => $postData['price'],
                    'name' => $postData['name'],
                    'image' => $postData['image'],
                    'qty' => 1,
                    'status' => 1
                ];
                $creatSession = 'true';
            } else {
                $sessionData[] = [
                    'product_id' => $postData['id'],
                    'price' => $postData['price'],
                    'name' => $postData['name'],
                    'image' => $postData['image'],
                    'qty' => 1,
                    'status' => 1
                ];
                $creatSession = 'true';
            }
            
            if ($creatSession == 'true') {
                GeneralHelper::setCartSession($sessionData);
                die('new');
            }
        }
        return new ViewModel($view);
    }

    public function addProductAction()
    {
        $validationExt = "jpg,jpeg,png,gif,mp4";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/products/";
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $categories = $categoriesTable->fetchAll();
        $productsForm = new ProductsForm($categories);
        $view['productsForm'] = $productsForm;
        if (! $this->userData->id) {
            return $this->redirect()->toRoute('application', array(
                'action' => 'signin'
            ));
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $file = $request->getFiles()->toArray();
            $productsTable = new ProductsTable($this->getServiceLocator());
            $data['image'] = '';
            if ($file['image']['tmp_name'] != '') {
                $newFileName = md5(time() . $file['image']['name']);
                $isUploaded = $this->imageUploader()->UploadFile($file['image'], $validationExt, $uploadDir, $newFileName);
                if ($isUploaded[0] == true) {
                    $data['image'] = $isUploaded[1];
                }
            }
            $data['status'] = 'pending';
            $addProduct = $productsTable->add($this->userData->id, $data);
            
            if ($addProduct) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toUrl("/dashboard");
            }
        }
        return new ViewModel($view);
    }
	
	public function addDesignerSampleAction()
    {
        $validationExt = "jpg,jpeg,png,gif,pdf";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/designer/samples/";
        $designerSampleForm = new DesignerSampleForm();
        $view['designerSampleForm'] = $designerSampleForm;
        if (! $this->userData->id) {
            return $this->redirect()->toRoute('application', array(
                'action' => 'signin'
            ));
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $file = $request->getFiles()->toArray();
//             var_dump($data);var_dump($file);die;
            $designerSampleTable = new DesignerSampleTable($this->getServiceLocator());
            $data['file'] = '';
            if ($file['file']['tmp_name'] != '') {
                $newFileName = md5(time() . $file['file']['name']);
                $isUploaded = $this->imageUploader()->UploadFile($file['file'], $validationExt, $uploadDir, $newFileName);
                if ($isUploaded[0] == true) {
                    $data['file'] = $isUploaded[1];
                }
            }
            $add = $designerSampleTable->add($this->userData->id, $data);
            
            if ($add) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toUrl("/dashboard");
            }
        }
        return new ViewModel($view);
    }
    
    public function editDesignerSampleAction()
    {
        $validationExt = "jpg,jpeg,png,gif,pdf";
        $config = $this->serviceLocator->get('Config');
        $id = $this->params('id');
        $uploadDir = $config['base_route'] . "/uploads/designer/samples/";
        $designerSampleTable = new DesignerSampleTable($this->getServiceLocator());
        $getSample = $designerSampleTable->fetchById($id);//var_dump($getSample);die;
        $designerSampleForm = new DesignerSampleForm();
        $designerSampleForm->populateValues($getSample);
        $view['designerSampleForm'] = $designerSampleForm;
        $view['getSample'] = $getSample;
        if (! $this->userData->id) {
            return $this->redirect()->toRoute('application', array(
                'action' => 'signin'
            ));
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $file = $request->getFiles()->toArray();
            // var_dump($data);die;
            $data['file'] = '';
            if ($file['file']['tmp_name'] != '') {
                $newFileName = md5(time() . $file['file']['name']);
                $isUploaded = $this->imageUploader()->UploadFile($file['file'], $validationExt, $uploadDir, $newFileName);
                // var_dump($isUploaded);die;
                if ($isUploaded[0] == true) {
                    $data['file'] = $isUploaded[1];
                }
            }else{
                $data['file'] = $getSample['file'];
            }
            $edit = $designerSampleTable->updateByUser($id, $data);
            
            if ($edit) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toUrl("/dashboard");
            }
        }
        return new ViewModel($view);
    }

    public function deleteDesignerSampleAction()
    {
        $id = $this->params('id', - 1);
        $designerSampleTable = new DesignerSampleTable($this->getServiceLocator());
        $isDelete = $designerSampleTable->delete($id);
        if ($isDelete) {
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toUrl('/dashboard');
        } else {
            $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
            return $this->redirect()->toUrl('/dashboard');
        }
    }

    public function dashboarddAction()
    {
        $this->setHeadTitle(__('Product sell'));
        if (! $this->userData->id) {
            return $this->redirect()->toRoute('application', array(
                'action' => 'signin'
            ));
        }
        
        if ($this->userData->user_group_id == 1 || $this->userData->user_group_id == 2 || $this->userData->user_group_id == 3) {
            return $this->redirect()->toRoute('application-admin');
        }
        
        $ordersTable = new OrderTable($this->getServiceLocator());
        
        // *** my print service orders *** //
        $printServicesOrders = $ordersTable->getByUser($this->userData->id, 'print services');
        foreach ($printServicesOrders as $key => $printServicesOrder) {
            $printServiceOrderDetailTable = new PrintServiceOrderTable($this->getServiceLocator());
            $getDetail = $printServiceOrderDetailTable->getByOrder($printServicesOrder['id']);
            $printServicesOrders[$key]['detail'] = $getDetail;
        }
        $view['printServicesOrders'] = $printServicesOrders;
        // var_dump($printServicesOrders);die;
        // *** my product orders *** //
        $productsOrders = $ordersTable->getByUser($this->userData->id, 'product');
        foreach ($productsOrders as $key => $productOrder) {
            $productOrderDetailTable = new ProductsOrderTable($this->getServiceLocator());
            $getDetail = $productOrderDetailTable->getByOrder($productOrder['id']);
            $productsOrders[$key]['detail'] = $getDetail;
        }
        $view['productsOrders'] = $productsOrders;
        
        // var_dump($productsOrders);die;
        // var_dump($printServicesOrders);die;
        // $ordersIndexTable = new OrdersIndexTable($this->getServiceLocator());
        
        if ($this->userData->user_group_id == 7) {
            $officeTable = new OfficeTable($this->getServiceLocator());
            $getOffice = $officeTable->getRecordById($this->userData->id);
            $listOfficeOrders = $ordersTable->getByOffice($getOffice['id'], 'print services'); // var_dump($listOfficeOrders);die;
                                                                                              // $listOfficeOrders = $printServiceOrderDetailTable->getByOffice($getOffice['id']);
                                                                                              // foreach ($listOfficeOrders as $key => $officeOrders){
                                                                                              // $listOfficeOrders[$key]['value'] = implode(',',json_decode($officeOrders['value']));
                                                                                              // }
            foreach ($listOfficeOrders as $key => $printServicesOrder) {
                $printServiceOrderDetailTable = new PrintServiceOrderTable($this->getServiceLocator());
                $getDetail = $printServiceOrderDetailTable->getByOrder($printServicesOrder['id']);
                $listOfficeOrders[$key]['detail'] = $getDetail;
            }
            $view['listOfficeOrders'] = $listOfficeOrders;
        }
        // var_dump($listOfficeOrders);die;
        
        // if($this->userData->user_group_id == 7){
        // $officeTable = new OfficeTable($this->getServiceLocator());
        // $getOffice = $officeTable->getRecordById($this->userData->id);
        // $listOfficeOrders = $printServiceOrderDetailTable->getByOffice($getOffice['id']);
        // foreach ($listOfficeOrders as $key => $officeOrders){
        // // var_dump($officeOrders);die;
        // $NOfficeOrders[$officeOrders['order_id']][] = $officeOrders;
        // $NOfficeOrders[$officeOrders['order_id']][$key]['value'] = implode(',',json_decode($officeOrders['value']));
        // }
        // $view['listOfficeOrders'] = $NOfficeOrders;
        
        // }
        // var_dump($NOfficeOrders);die;
        
        // $listOrders = $ordersTable->getByUser($this->userData->id);
        // foreach ($listOrders as $key => $order){
        // $listOrders[$key]['orderDetail'] = $ordersIndexTable->getByOrder($order['id']);
        // }
        
        // $view['listOrders'] = $listOrders;
        
        // $orderTable = new OrderTable($this->getServiceLocator());
        // $orderIndexTable = new OrderIndexTable($this->getServiceLocator());
        // $plistOrder = $orderTable->getByUser($this->userData->id);
        // foreach ($plistOrder as $key => $order){
        // $plistOrder[$key]['orderDetail'] = $orderIndexTable->getByOrder($order['id']);
        // }
        // $view['plistOrders'] = $plistOrder;
        $productsTable = new ProductsTable($this->getServiceLocator());
        $products = $productsTable->fetchByUser($this->userData->id);
        
        $designerSampleTable = new DesignerSampleTable($this->getServiceLocator());
        $samples = $designerSampleTable->fetchByUser($this->userData->id);
        // var_dump($samples);die;
        
        
        $view['products'] = $products;
        $view['samples'] = $samples;
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }

    public function editProductAction()
    {
        $id = $this->params('id');
        $validationExt = "jpg,jpeg,png,gif,mp4";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/products/";
        $productsTable = new ProductsTable($this->getServiceLocator());
        $product = $productsTable->fetchById($id);
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $categories = $categoriesTable->fetchAll();
        $productsForm = new ProductsForm($categories);
        $productsForm->populateValues($product);
        $view['productsForm'] = $productsForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $file = $request->getFiles()->toArray();
            $productsTable = new ProductsTable($this->getServiceLocator());
            if ($file['image']['tmp_name'] != '') {
                $newFileName = md5(time() . $file['image']['name']);
                $isUploaded = $this->imageUploader()->UploadFile($file['image'], $validationExt, $uploadDir, $newFileName);
                if ($isUploaded[0] == true) {
                    $data['image'] = $isUploaded[1];
                }
            }
            $updateProduct = $productsTable->update($id, $data);
            if ($updateProduct) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/list-products');
            }
        }
        
        return new ViewModel($view);
    }

    public function deleteProductAction()
    {
        $productId = $this->params('id', - 1);
        $shopProductsTable = new ProductsTable($this->getServiceLocator());
        $isDelete = $shopProductsTable->delete($productId);
        if ($isDelete) {
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toUrl('/dashboard');
        } else {
            $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
            return $this->redirect()->toUrl('/dashboard');
        }
    }

    public function logoutAction()
    {
        if (! $this->userData) {
            return $this->redirect()->toRoute('home');
        }
        $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
        $ttt = $loginLogTable->updateUserLastLogin($this->identity()->id);
        
        $this->authenticationService->logOut();
        GeneralHelper::removeSession();
        return $this->redirect()->toRoute('home');
    }

    public function profileAction()
    {
        $id = $this->identity()->id;
        $usersTable = new UsersTable($this->getServiceLocator());
        $getUser = $usersTable->getUserById($id);
        $getUser = $getUser[0];
        $userForm = new UserForm();
        $userForm->populateValues($getUser);
        $view['userForm'] = $userForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $usersTable = new UsersTable($this->getServiceLocator());
            $updateUser = $usersTable->edit($postData, $id);
            if ($updateUser) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('application', array(
                    'action' => 'dashboard'
                ));
            }
        }
        return new ViewModel($view);
    }

    public function sellerProfileAction()
    {
        $id = $this->identity()->id;
        $usersTable = new UsersTable($this->getServiceLocator());
        $getUser = $usersTable->getUserById($id);
        $getUser = $getUser[0];
        $userForm = new UserForm();
        $userForm->populateValues($getUser);
        $view['userForm'] = $userForm;
        $companyTable = new CompanyTable($this->getServiceLocator());
        $userCompanyData = $companyTable->getRecordById($id);
        $companyForm = new CompanyForm();
        $companyForm->populateValues($userCompanyData);
        $view['companyForm'] = $companyForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $usersTable = new UsersTable($this->getServiceLocator());
            $updateUser = $usersTable->edit($postData, $id);
            if ($updateUser) {
                $companyTable = new CompanyTable($this->getServiceLocator());
                $companyData['company_name'] = $postData['company_name'];
                $companyData['company_history'] = $postData['company_history'];
                $companyData['manager_name'] = $postData['manager_name'];
                $companyData['fax'] = $postData['fax'];
                $updateCompany = $companyTable->edit($companyData, $id);
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('application', array(
                    'action' => 'dashboard'
                ));
            }
        }
        return new ViewModel($view);
    }

    public function technicalOfficeProfileAction()
    {
        $id = $this->identity()->id;
        $usersTable = new UsersTable($this->getServiceLocator());
        $getUser = $usersTable->getUserById($id);
        $getUser = $getUser[0];
        $userForm = new UserForm();
        $userForm->populateValues($getUser);
        $view['userForm'] = $userForm;
        $officeTable = new OfficeTable($this->getServiceLocator());
        $userOfficeData = $officeTable->getRecordById($id);
        $serviceTable = new ServiceTable($this->getServiceLocator());
        $serviceIndexTable = new ServiceIndexTable($this->getServiceLocator());
        $services = $serviceTable->getRecords();
        $getServices = $serviceIndexTable->getRecordOffice($userOfficeData['id']);
        $officeForm = new OfficeForm($services);
        $userOfficeData['service_id'] = explode(',', $getServices['service_id']);
        $officeForm->populateValues($userOfficeData);
        $view['officeForm'] = $officeForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $usersTable = new UsersTable($this->getServiceLocator());
            $updateUser = $usersTable->edit($postData, $id);
            if ($updateUser) {
                $officeTable = new OfficeTable($this->getServiceLocator());
                $officeData['office_name'] = $postData['office_name'];
                $officeData['manager_name'] = $postData['manager_name'];
                $officeData['fax'] = $postData['fax'];
                $officeData['online_services'] = $postData['online_services'];
                $officeData['usual_courier'] = $postData['usual_courier'];
                $officeData['special_courier'] = $postData['special_courier'];
                $updateOffice = $officeTable->edit($officeData, $id);
                if ($updateOffice) {
                    $serviceTable = new ServiceIndexTable($this->getServiceLocator());
                    $newServices = array_diff($postData['service_id'], $userOfficeData['service_id']);
                    $deleteServices = array_diff($userOfficeData['service_id'], $postData['service_id']);
                    foreach ($deleteServices as $delService) {
                        $delService = $serviceTable->delete($userOfficeData['id'], $delService);
                    }
                    foreach ($newServices as $service) {
                        $serviceData['office_id'] = $userOfficeData['id'];
                        $serviceData['service_id'] = $service;
                        $addService = $serviceTable->add($serviceData);
                    }
                }
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('application', array(
                    'action' => 'dashboard'
                ));
            }
        }
        return new ViewModel($view);
    }

    public function servicesAction()
    {
        // var_dump($_SERVER['HTTP_REFERER']);die;
        // GeneralHelper::removeSession();
        $offset = false;
        $validationExt = "jpg,jpeg,png,psd,tiff,tif,pdf";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/orders/tmp/";
        $printServiceId = $this->params('id');
        $view['id'] = $printServiceId;
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $printService = $printServicesTable->fetchById($printServiceId); // var_dump($printService);die;
        $view['printService'] = $printService;
        $eventManager = $this->eventManager;
        $eventManager->trigger( 'myUserEventLog', $this, array (
            'parameter' => null,
            'route' => 'application-index-services'
        ));
        
        
        $officeTable = new OfficeTable($this->getServiceLocator());
        $offices = $officeTable->getRecords();
        $view['offices'] = $offices;
        $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
        $get = $attributesIndexTable->getAllByPrintService($printServiceId);
        if(!$get){
            $get = $attributesIndexTable->getAllByPrintService($printService['parent_id']);
        }
        $attributesGroupTable = new AttributesGroupTable($this->getServiceLocator());
        if($printService['type_id'] == '14'){
            $offset = true;
            $groups = $attributesGroupTable->getByPrintServiceId($printServiceId);
            if(!$groups){
                $groups = $attributesGroupTable->getByPrintServiceId($printService['parent_id']);
            }
            foreach ($groups as $key=>$group){
                $val = json_decode($group['value']);
                $groups[$key]['value'] = $val;
                $size[] = $val[2] . '*' . $val[3];
            }
        }
        
        foreach ($get as $key => $attr) {
           $valueArr = [];
//            if($offset){
//            if($attr['attribute_title'] == 'طول' || $attr['attribute_title'] == 'عرض'){
//                unset($get[$key]);
//            } 
//            }
           if($attr['attribute_title'] == 'طول' || $attr['attribute_title'] == 'عرض'){
               unset($get[$key]);
           }else{
               $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
               $val = $attributesValueTable->getAllByAttribute($attr['id']);//var_dump($val);
               if(!empty($val)){
                   foreach ($val as $k => $attrVal){
                       $valueArr[$key][] = $attrVal['value'];
                   }
                   $get[$key]['options'] = $valueArr[$key];
               }
           }  
        }
        if($offset){
            $get[count($get)] = [
                'attribute_title' => 'ابعاد',
                'options'  => array_unique($size)
            ];
        }
//         $get[count($get)] = [
//             'attribute_title' => 'سایز',
//             'options'  => array_unique($size)
//         ];
//         var_dump($get);die;
        $view['get'] = $get;
        if (empty($get)) {
            // $this->layout()->errorMessage = "در حال حاضر امکان سفارش این خدمات وجود ندارد.";
            $this->flashMessenger()->addErrorMessage(__("در حال حاضر امکان سفارش این خدمات وجود ندارد."));
            return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
        }else{
            $eventManager = $this->eventManager;//var_dump($eventManager);die;
            $eventManager->trigger('myProductEventLog', $this, array (
                'parameter' => $printServiceId,
                'route' => 'application-index-services'
            ));
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray(); // var_dump($postData);die;
            $files = $request->getFiles()->toArray();
            if (! empty($postData['attributes_group_price'])) {
                $uploadedFile = [];
                foreach ($files['images'] as $key => $file) {
                    if ($file['tmp_name'] != '') {
                        $newFileName = md5(time() . $file['name']);
                        $isUploaded = $this->imageUploader()->UploadFile($file, $validationExt, $uploadDir, $newFileName);
                        if ($isUploaded[0] == true) {
                            $uploadedFile[] = $isUploaded[1];
                        }
                    }
                }
                $sessionData = GeneralHelper::getCartServicesSession();
                $sessionData[] = [
                    'parent' => $printService['parent_id'],
                    'print_service_id' => $printServiceId,
                    'office_id' => $postData['office'],
                    'attributes_group_id' => $postData['attributes_group_id'],
                    'price' => $postData['attributes_group_price'],
                    'name' => $printService['title'],
                    'image' => $printService['image'],
                    'qty' => 1,
                    'files' => $uploadedFile,
                    'file_desc' => $postData['file_desc'],
                    'status' => '1'
                ];
                GeneralHelper::setCartServicesSession($sessionData);
                die('true');
            } else {
                die('empty price');
            }
            
            // $printServiceOrdersTable = new PrintServiceSessionTable($this->getServiceLocator());
            // $ordersData['user_id'] = $this->userData->id;
            // $ordersData['office_id'] = $postData['office_id'];
            // $ordersData['attributes_group_id'] = $postData['attributes_group_id'];
            // $ordersData['print_service_id'] = $printServiceId;
            
            // $orderAdd = $printServiceOrdersTable->add($ordersData);
            // if($orderAdd){
            // foreach ($files['images'] as $file){
            // if ($file['tmp_name'] != '') {
            // $newFileName = md5(time() . $file['name']);
            // $isUploaded = $this->imageUploader()->UploadFile($file,$validationExt, $uploadDir, $newFileName);
            // if($isUploaded[0] == true){
            // $fileData['image'] = $isUploaded[1];
            // $fileData['order_id'] = $orderAdd;
            // $orderFilesTable = new OrdersFileTable($this->getServiceLocator());
            // $filesAdd = $orderFilesTable->add($fileData);
            // }
            // }
            // }
            // die($orderAdd);
            // }
        }
        return new ViewModel($view);
    }

    public function categoryAction()
    {
        $printServiceId = $this->params('id');
        $offset = $this->params('params');
        $eventManager = $this->eventManager;//var_dump($eventManager);die;
        $eventManager->trigger('myProductEventLog', $this, array (
            'parameter' => $printServiceId,
            'route' => 'application-index-category'
        ));
        $eventManager = $this->eventManager;
        $eventManager->trigger( 'myUserEventLog', $this, array (
            'parameter' => null,
            'route' => 'application-index-category'
        ));
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $printService = $printServicesTable->fetchById($printServiceId);
        $view['printService'] = $printService;//var_dump($printService);die;
        $printServiceChild = $printServicesTable->fetchChildByPrintService($printServiceId);
        $view['printServiceChild'] = $printServiceChild;
        $view['offset'] = $offset;
        
        return new ViewModel($view);
    }
    public function mainCategoryAction()
    {
//         $printServiceId = $this->params('id');
//         $offset = $this->params('params');
        $eventManager = $this->eventManager;//var_dump($eventManager);die;
//         $eventManager->trigger('myProductEventLog', $this, array (
//             'parameter' => $printServiceId,
//             'route' => 'application-index-category'
//         ));
        $eventManager = $this->eventManager;
        $eventManager->trigger( 'myUserEventLog', $this, array (
            'parameter' => null,
            'route' => 'application-index-category'
        ));
        $servicesTable = new ServicesTypesTable($this->getServiceLocator());
        $servicesType = $servicesTable->getAll();
        $view['servicesType'] = $servicesType;
//         $printServiceChild = $printServicesTable->fetchChildByPrintService($printServiceId);
//         $view['printServiceChild'] = $printServiceChild;
//         $view['offset'] = $offset;
    
        return new ViewModel($view);
    }
    public function secondCategoryAction()
    {
        $typeId = $this->params('id');
//         $offset = $this->params('params');
        $eventManager = $this->eventManager;//var_dump($eventManager);die;
        $eventManager->trigger('myProductEventLog', $this, array (
            'parameter' => $typeId,
            'route' => 'application-index-category'
        ));
        $eventManager = $this->eventManager;
        $eventManager->trigger( 'myUserEventLog', $this, array (
            'parameter' => null,
            'route' => 'application-index-category'
        ));
        
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $printService = $printServicesTable->fetchByTypeId($typeId);
//         var_dump($printService);die;
        $view['printService'] = $printService;
        
    
        return new ViewModel($view);
    }
    public function formsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray(); //var_dump($postData);die;
            $printServicesTable = new PrintServicesTable($this->getServiceLocator());
            $printService = $printServicesTable->fetchById($postData['print_service_id']);
            //var_dump($printService);die;
            if($printService['type_id'] == '14'){
                $attr = explode('*', $postData['attribute'][2]);
                unset($postData['attribute'][2]);
                $attribute = array_merge($postData['attribute'],$attr);
            }else{
                $attribute = $postData['attribute'];
            }
//             var_dump($attribute);die;
            $table = new AttributesGroupTable($this->getServiceLocator());
            $implode = json_encode($attribute);
            $searchAdmin = $table->get($implode);
            // var_dump($searchAdmin);die;
            if ($postData['office_id'] > 0) {
                $searchOffices = $table->get($implode, $postData['office_id']);
                $price['office'] = $searchOffices;
            }
            
            $price['admin'] = $searchAdmin;
            
            // else{
            
            // }
            
            // var_dump($search);die;
            die(json_encode($price));
        }
    }

    public function getConfigValAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $configurationTable = new ConfigurationTable($this->getServiceLocator());
            $getConfig = $configurationTable->getConfigByName($postData['name']);
            die(json_encode($getConfig));
        }
    }

    public function ordersAction()
    {
        $orderId = $this->params('id');
        $ordersTable = new OrderTable($this->getServiceLocator());
        $order = $ordersTable->getById($orderId); // var_dump($order);die;
        $view['order'] = $order;
        
        $printServicesOrderTable = new PrintServiceOrderTable($this->getServiceLocator());
        $orderDetail = $printServicesOrderTable->getByOrder($orderId);
        $view['orderDetail'] = $orderDetail;
        return new ViewModel($view);
    }

    public function productFactorAction()
    {
        $orderId = $this->params('id');
        // $ordersIndexTable = new OrderIndexTable($this->getServiceLocator());
        // $orderDetail = $ordersIndexTable->getByOrder($orderId);
        $ordersTable = new OrderTable($this->getServiceLocator());
        $order = $ordersTable->getById($orderId); // var_dump($order);die;
        $view['order'] = $order;
        $productOrderTable = new ProductsOrderTable($this->getServiceLocator());
        $orderDetail = $productOrderTable->getByOrder($orderId);
        $view['orderDetail'] = $orderDetail;
        
//         var_dump($orderDetail);die;
        return new ViewModel($view);
    }

    public function getProductsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $productsTable = new ProductsTable($this->getServiceLocator());
            $productList = $productsTable->getByCategory($postData['category']);
            die(json_encode($productList));
        }
    }

    public function searchProductsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $productsTable = new ProductsTable($this->getServiceLocator());
            $productList = $productsTable->searchByName($postData['name']);
            die(json_encode($productList));
        }
    }

    // public function setCartSessionAction(){
    // $request = $this->getRequest();
    // if ($request->isPost()) {
    // $postData = $request->getPost()->toArray();
    // $generalHelper = new GeneralHelper();
    // $generalHelper->setCartSession($postData['itemArr']);
    // die('true');
    // }
    // }
    public function getCartSessionAction()
    {
        $sessionData = GeneralHelper::getCartSession();
        $sessionServicesData = GeneralHelper::getCartServicesSession();
        if ($sessionData && $sessionServicesData) {
            $sessionData = array_merge($sessionData, $sessionServicesData);
        } elseif ($sessionData) {
            $sessionData = $sessionData;
        } elseif ($sessionServicesData) {
            $sessionData = $sessionServicesData;
        }
        $totalPrice = 0;
        $count = 0;
        if ($sessionData) {
            $totalPrice = array_sum(array_map(function ($item) {
                if ($item['status'] == '1') {
                    return $item['price'];
                }
            }, $sessionData));
            $count = array_sum(array_map(function ($item) {
                if ($item['status'] == '1') {
                    return $item['qty'];
                }
            }, $sessionData));
        }
        
        $view['sessionData'] = $sessionData;
        $view['total_price'] = $totalPrice;
        $view['sum_qty'] = $count;
        if ($sessionData) {
            die(json_encode($view));
        } else {
            die('null');
        }
    }

    public function addPriceAction()
    {
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $printServicesList = $printServicesTable->fetchParent('office'); // var_dump($printServicesList);die;
        $attributesTable = new AttributesTable($this->getServiceLocator());
        $attributesList = $attributesTable->getAll();
        $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
        $attributesValList = $attributesValueTable->getAll();
        
        $attributesForm = new AttributesValueForm($printServicesList, $attributesList, $attributesValList);
        $view['attributesForm'] = $attributesForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $postData['value'] = json_encode($postData['value']);
            $officeTable = new OfficeTable($this->getServiceLocator());
            $getOffice = $officeTable->getRecordById($this->userData->id);
            $postData['office_id'] = $getOffice['id'];
            $table = new AttributesGroupTable($this->getServiceLocator());
            $isAdd = $table->addByUser($postData);
            if ($isAdd) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('application', array(
                    'action' => 'dashboard'
                ));
            }
        }
        return new ViewModel($view);
    }

    public function getFormAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
            $get = $attributesIndexTable->getAllByPrintService($postData['id']); // var_dump($get);die;
            foreach ($get as $key => $attr) {
                $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
                $attrVal = $attributesValueTable->getAllByAttribute($attr['id']);
                if (! empty($attrVal['value'])) {
                    $value = explode(',', $attrVal['value']);
                    $id = explode(',', $attrVal['id']);
                    foreach ($value as $k => $val) {
                        $get[$key]['options'][$id[$k]] = $val;
                    }
                }
            }
            die(json_encode($get));
        }
    }

    public function priceListAction()
    {
        $attributesGroupTable = new AttributesGroupTable($this->getServiceLocator());
        $officeTable = new OfficeTable($this->getServiceLocator());
        $getOffice = $officeTable->getRecordById($this->userData->id);
        $groups = $attributesGroupTable->getByOffice($getOffice['id']);
        foreach ($groups as $key => $gp) {
            $groups[$key]['val'] = implode(',', json_decode($gp['value']));
        }
        $view['groups'] = $groups;
        return new ViewModel($view);
    }

    public function deletePriceAction()
    {
        $id = $this->params('id');
        $attributeGroupTable = new AttributesGroupTable($this->getServiceLocator());
        $isDelete = $attributeGroupTable->delete($id);
        if ($isDelete) {
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toUrl('/price-list');
        } else {
            $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
            return $this->redirect()->toUrl('/price-list');
        }
    }

    public function getServiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $serviceIndexTable = new ServiceIndexTable($this->getServiceLocator());
            $getServices['services'] = $serviceIndexTable->getByOffice($postData['id']);
            $attributesGroupTable = new AttributesGroupTable($this->getServiceLocator());
            $groups = $attributesGroupTable->getByOffice($postData['id']);
            foreach ($groups as $key => $gp) {
                $groups[$key]['val'] = implode(',', json_decode($gp['value']));
            }
            $getServices['groups'] = $groups;
            
            die(json_encode($getServices));
        }
    }

    public function getDistrictAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $districtTable = new DistrictTable($this->getServiceLocator());
            $getServices = $districtTable->getRecordsByArea($postData['id']);
            die(json_encode($getServices));
        }
    }

    public function searchProductAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $productTable = new ProductsTable($this->getServiceLocator());
            $getProducts = $productTable->searchByName($postData['name']);
            die(json_encode($getProducts));
        }
    }

    public function searchFilterProductsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray(); // var_dump($postData);die;
            $productTable = new ProductsTable($this->getServiceLocator()); // var_dump($postData['filter']);die;
            $getProducts = $productTable->searchByFilter($postData['filter'], $postData['category']);
            // $newArr = [];
            foreach ($getProducts as $k => $product) {
                if (empty($product['discount_price'])) {
                    $getProducts[$k]['discount_price'] = $product['price'];
                    // $newArr[] = $product;
                    // unset($getProducts[$k]);
                }
            }
            // var_dump($getProducts);
            // var_dump($newArr);
            if ($postData['filter'] == 2) {
                usort($getProducts, function ($a, $b) {
                    // if($a['discount_price']==$b['discount_price']) return 0;
                    // return $a['discount_price'] < $b['discount_price']?1:-1;
                    return $a['discount_price'] - $b['discount_price'];
                });
            } elseif ($postData['filter'] == 3) {
                usort($getProducts, function ($a, $b) {
                    if ($a['discount_price'] == $b['discount_price'])
                        return 0;
                    return $a['discount_price'] < $b['discount_price'] ? 1 : - 1;
                    // return $a['discount_price'] - $b['discount_price'];
                });
            }
            
            // foreach ($getProducts as $key => $row)
            // {
            // if(!empty($row['discount_price'])){
            // $getProducts[$key]['price'] = $row['discount_price'];
            // }
            // // if($row['discount_price'] == 0){
            // // $getProducts[$key]['discount_price'] = 10000000000;
            // // }
            // $vc_array_value[$key] = $row['price'];
            // // $vc_array_name[$key] = $row['name'];
            // }
            // var_dump($getProducts);die;
            // $arr = array_multisort($vc_array_value, SORT_DESC, $getProducts);
            die(json_encode($getProducts));
        }
    }

    public function AllpriceListAction()
    {
        $cache = $this->getServiceLocator()->get('cache');
        
//         $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        
        $adminPrice = [];
        $priceList = [];
        $attributesGroupTable = new AttributesGroupTable($this->getServiceLocator());
        $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
        $adminPriceCache = $cache->getItem('adminPrice', $success);
        if($adminPriceCache){
            $adminPrice = $adminPriceCache;  
        }else{
            $adminPrice = $attributesGroupTable->getAll();
            foreach ($adminPrice as $key => $gp) {
                $adminPrice[$key]['val'] = json_decode($gp['value']);
                $attrTitle = $attributesIndexTable->getAllByPrintService($gp['print_service_id']);
                if(!$attrTitle){
                    $attrTitle = $attributesIndexTable->getAllByPrintService($gp['parent_id']);
                }
                $adminPrice[$key]['attrTitle'] = $attrTitle;
            }
            $cache->setItem('adminPrice', $adminPrice);
        }
        
        
        $view['adminPrice'] = $adminPrice;
//         var_dump($cache);die;
//         var_dump($adminPrice);die;
        $officeTable = new OfficeTable($this->getServiceLocator());
        $offices = $officeTable->getRecords();
        foreach ($offices as $k => $office) {
            
            $groups = $attributesGroupTable->getByOffice($office['id']);
            if ($groups) {
                $priceList[$k]['office_name'] = $office['office_name'];
                foreach ($groups as $key => $gp) {
                    $priceList[$k]['prices'][] = [
                        'print_service' => $gp['print_service'],
                        'val' => implode(', ', json_decode($gp['value'])),
                        'price' => $gp['price']
                    ];
                }
            }
        }
        
        $view['groups'] = $priceList;
        
        return new ViewModel($view);
    }

    public function changeStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray(); // var_dump($postData);die;
            $orderTable = new OrderTable($this->getServiceLocator());
            $update = $orderTable->updateStatus($postData['order_status'], $postData['order_id'], $postData['status-desc']);
            var_dump($update);
            die();
        }
    }
    
    private function get($serviceId=null,$parentId=null)
    {
        $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
        $attrTitle = $attributesIndexTable->getAllByPrintService($serviceId);
        if(!$attrTitle){
            $attrTitle = $attributesIndexTable->getAllByPrintService($parentId);
        }
        return $attrTitle;
    }
    public function createPdfAction()
    {
        // create new PDF document
        $pdf = new Fpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        // set document information
        $pdf->SetTitle('لیست قیمت ها');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'لیست قیمت ها', 'www.digibenis.com');
        
        // set header and footer fonts
        $pdf->setHeaderFont(Array('bahijnazanin', '', '17'));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        
        // set default monospaced font
        //         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        
        // set image scale factor
        //         $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        
        // set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'rtl';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';
        
        // set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);
        
        // ---------------------------------------------------------
        
        // set font
        $pdf->SetFont('bahijnazanin', '', 12);
        // add a page
        $pdf->AddPage();
        //         $attributesGroupTable = new AttributesGroupTable($this->getServiceLocator());
        //         $adminPrice = $attributesGroupTable->getAll();
        //         var_dump($adminPrice);die();
                
        //         foreach ($adminPrice as $key => $gp) {
        //             $adminPrice[$key]['val'] = implode(', ', json_decode($gp['value']));
        //         }
        $header = array('عنوان خدمات چاپ', 'ویژگی', 'قیمت');
        $newArr = [];
        
        //         var_dump($adminPrice);die;
        //         foreach ($adminPrice as $key => $group){
        //             $newArr[$group['print_service_id']][] = $group;
        //         }
        //         foreach ($newArr as $arr){ //var_dump($arr);die;
        //             foreach ($arr as $key=> $array){
        //                 $newArrss[$key]['id'] = $key + 1;
        //                 $newArrss[$key]['title'] = $array['print_service'];
        //                 $newArrss[$key]['attribute'] = $array['value'];
        //                 $newArrss[$key]['price'] = $array['price'];
                        
        //             }
        //             $pdf->Cell(0, 12, ' کف بازار',0,1,'C');
        //             $pdf->ImprovedTable($header,$newArrss);
        //         }
        //         var_dump($newArr);die;
        
        $cache = $this->getServiceLocator()->get('cache');
        $adminPrices = $cache->getItem('adminPrice', $success);
        //         var_dump($newArr);die;
        //         foreach ($adminPrices as $adminPrice){
        //             $newArr[$adminPrice['print_service_id']][] = $adminPrice;
        //         }
                
        //         foreach ($newArr as $arr){
        //             var_dump($arr);die;
        //         }
                //var_dump($newArr);die;
        //         foreach ($newArr as $a){
        //             var_dump($a);
        //         }die;
        //         if(!$adminPriceCache){
        //             foreach ($adminPrice as $key => $price){
        //                 $val = json_decode($price['value']);
        //                 $attrTitle = $this->get($price['print_service_id'],$price['parent_id']);
        //                 //var_dump($attrTitle);
        //                 $a = '';
        //                 foreach ($attrTitle as $k=>$t){
        //                     $a  .= '<p>' . $t['attribute_title'] . ' : ' . $val[$k] . '</p>';
        //                 }
                        
        //                 $newArr[$key]['id'] = $key + 1;
        //                 $newArr[$key]['title'] = $price['print_service'];
        //                 $newArr[$key]['attribute'] = $a;
        //                 $newArr[$key]['price'] = $price['price'];
        //             }
        //             $cache->setItem('adminPriceCache', $newArr);
        //         }else{
        //             $newArr = $adminPriceCache;  
        //         }
        //         var_dump($newArr);die;
        $pdf->Cell(0, 12, ' کف بازار ',0,1,'C');
        $pdf->ImprovedTable($header,$adminPrices);
        //die;
        
        
        
                // print newline
        //         $pdf->Ln();
        //         $officeTable = new OfficeTable($this->getServiceLocator());
        //         $offices = $officeTable->getRecords();
        //         foreach ($offices as $k => $office) {
        //             $groups = $attributesGroupTable->getByOffice($office['id']);
        //             if ($groups) {
        //                 $priceList[$k]['office_name'] = $office['office_name'];
        //                 foreach ($groups as $key => $gp) {
        //                     $priceList[$k]['prices'][] = [
        //                         'print_service' => $gp['print_service'],
        //                         'val' => implode(', ', json_decode($gp['value'])),
        //                         'price' => $gp['price']
        //                     ];
        //                 }
        //             }
        //         }
        //         foreach ($priceList as $k => $office){
        //             $pdf->Cell(0, 12, $office['office_name'],0,1,'C');
        //             $officeArr = [];
        //             foreach ($office['prices'] as $key => $price){
        //                 $officeArr[$key]['id'] = $key + 1;
        //                 $officeArr[$key]['title'] = $price['print_service'];
        //                 $officeArr[$key]['attribute'] = $price['val'];
        //                 $officeArr[$key]['price'] = $price['price'];
        //             }
        //             $pdf->BasicTable($header,$officeArr);
        //         }
        $pdf->Output('price-list.pdf', 'I');
        // ---------------------------------------------------------
    }

    public function designerSamplesAction(){
        $designerTable = new DesignerTable($this->getServiceLocator());
        $getDesigner = $designerTable->fetchAllDesigner();
        $view['getDesigner'] = $getDesigner;
        return new ViewModel($view);
    }
    public function designerSamplesDetailesAction($id){
        $id = $this->params('id');
        // $designerTable = new DesignerTable($this->getServiceLocator());
        $designerSampleTable = new DesignerSampleTable($this->getServiceLocator());
        $samples = $designerSampleTable->fetchByUser($id);
        // var_dump($samples);die;
        $view['samples'] = $samples;
        
        
        
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $productsTable = new ProductsTable($this->getServiceLocator());
        $categories = $categoriesTable->fetchAll();
        foreach ($categories as $key => &$category) {
            $productList = $productsTable->getByCategory([$category['id']]);
            $category['products'] = $productList;
        }
        
        $view['categories'] = $categories;
        return new ViewModel($view);
    }
}
