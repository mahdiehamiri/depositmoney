<?php

namespace Application\Controller;


use Application\Helper\BaseCustomerController;
use ProductComment\Model\ProductCommentTable;
use ProductComment\Form\ProdcutCommentForm;
use Zend\Mvc\Controller\AbstractActionController;
use Contact\Model\ContactTable;
use Configuration\Model\ConfigurationTable;
use Contact\Form\ContactForm;
use Application\Form\RequestProjectForm;
use Application\Form\RequestTeachingForm;
use Zend\Db\Adapter\Adapter;
use Application\Model\TotalTable;

class AjaxController extends AbstractActionController
{	
    public function refreshrpAction()
    {
        $form = new RequestProjectForm();
        $captcha = $form->get('request_project_myCaptcha')->getCaptcha();
        $data = array();
        $data['id']  = $captcha->generate();
        $data['src'] = $captcha->getImgUrl() .
        $captcha->getId() .
        $captcha->getSuffix();
        die(json_encode($data));
    }
	
    public function refreshrtAction()
    {
        $form = new RequestTeachingForm();
        $captcha = $form->get('request_teaching_myCaptcha')->getCaptcha();
        $data = array();
        $data['id']  = $captcha->generate();
        $data['src'] = $captcha->getImgUrl() .
        $captcha->getId() .
        $captcha->getSuffix();
        die(json_encode($data));
    }
    public function deleteAllAction()
    { 
    	$config = $this->getServiceLocator()->get('Config');
    	$dbAdapter = new Adapter($config['db']);
    	$request = $this->getRequest(); 
	   
    	$isdeleted = false;
    	$cantDelete = false;
    	
    	$configTable = new ConfigurationTable($this->getServiceLocator());
     
    	 if ($request->isPost()) {
	        $data = $request->getPost()->toArray();  
	    
    		if ($data['deletedIds']) { 
	    		$model = new TotalTable( $this->getServiceLocator(), $data['tablename']);
	    		foreach ($data['deletedIds'] as $datadeleted) {
	    			$result = $model->deleteRecord($datadeleted); 
	    			if ($result) {
	    				$isdeleted[] = $datadeleted; 
	    			} else {
	    				$cantDelete[] = $datadeleted;
	    			}
	    		}
    		}
    	}
		 $result = array(
		 		'deleted' =>$isdeleted,
		 		'notDelete' =>$cantDelete,
		 );
		 die(json_encode($result)); 
    }
}