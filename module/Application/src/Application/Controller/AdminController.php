<?php
namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Application\Form\UserAccountForm;
use Application\Model\CommentsTable;
use Application\Model\UsersTable;
use Blog\Model\BlogPostsTable;
use Application\Helper\BaseAdminController;
use Application\Model\UsersLoginLogTable;
use Application\Helper\GeneralHelper;
use ActivityLog\Model\ActivityLogsTable;
use ActivityLog\Model\DataLogsTable;
use Zend\Db\Sql\Where;
use Application\Model\DepositTable;
use Application\Model\DepositDetailsTable;
use Application\Model\DepositAccountTable;
use Application\Model\DepositPaymentTable;
use Application\Model\DepositFilesTable;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;

class AdminController extends BaseAdminController
{

    protected $noOfCommentsPerPage = 50;
    
    public function getOnlineUser()
    {
        $activityLogsTable = new ActivityLogsTable($this->getServiceLocator());
        $dataLogsTable = new DataLogsTable($this->getServiceLocator());
        //         $productViewTable = $this->getModel('ProductView', 'ProductViewTable');
        //         $productGroupTable = $this->getModel('Product', 'ProductGroupTable');
        $where = new where();
        $where->equalTo('status', 1);
        $this->activityLogsTotal = $activityLogsTable->getSessionIsExistOnlines($where);
        $this->logsTotal = $dataLogsTable->getSessionIsExistOnlines();
        $time = time();
        $time_check = $time - 600;
        
        $date = date("Y-m-d");
        $yesterday = date("Y-m-d H:i:s", strtotime("-1 day"));
        $lastMonth = date("Y-m-d H:i:s", strtotime("-1 month"));
        $lastMonth = explode(' ', $lastMonth);
        $lastMonth = explode('-', $lastMonth[0]);
        $lastMonthGetDate = $lastMonth[0] . '-' . $lastMonth[1];
        
        $thisDay = explode(' ', $date);
        $thisDay = explode('-', $thisDay[0]);
        $thisDayGetDate = $thisDay[0] . '-' . $thisDay[1] . '-' . $thisDay[2];
        
        $thisMonth = explode(' ', $date);
        $thisMonth = explode('-', $thisMonth[0]);
        $thisMonthGetDate = $thisMonth[0] . '-' . $thisMonth[1];
        
        $thisWeak = date("Y-m-d H:i:s", strtotime("-7 day"));
        $thisWeak = explode(' ', $thisWeak);
        $thisWeak = explode('-', $thisWeak[0]);
        $thisWeekGetDate = $thisWeak[0] . '-' . $thisWeak[1] . '-' . $thisWeak[2];
        
        
        $yesterday = explode(' ', $yesterday);
        $yesterday = explode('-', $yesterday[0]);
        $yesterdayGetDate = $yesterday[0] . '-' . $yesterday[1] . '-' . $yesterday[2];
        $activity_logs_total = array();
        $productViewVisitKeeper = array();
        $productGroupVisitKeeper = array();
        $latestActivityLogsTableData = array();
        $latestActivityLogsTableLastMonth = array();
        $latestActivityLogsTableThistDay = array();
        $latestActivityLogsTableThistMonth = array();
        $activityLogsTableThistWeak = array();
        $latestActivityLogsTableYesterday = array();
        if ($this->activityLogsTotal) {
            foreach ($this->activityLogsTotal as $data) {
                $activity_logs_total[$data['ip_address']] = $data;
                if ($data['status'] == 1 && $data['time'] > $time_check) {
                    $latestActivityLogsTableData[$data['ip_address']] = $data;
                }
                if (strstr($data['date'], $lastMonthGetDate)) {
                    $latestActivityLogsTableLastMonth[$data['ip_address']] = $data;
                }
                if (strstr($data['date'], $thisDayGetDate)) {
                    $latestActivityLogsTableThistDay[$data['ip_address']] = $data;
                }
                if (strstr($data['date'], $thisMonthGetDate)) {
                    $latestActivityLogsTableThistMonth[$data['ip_address']] = $data;
                }
                if (strtotime($data['date']) > strtotime($thisWeekGetDate)) {
                    $activityLogsTableThistWeak[$data['ip_address']] = $data;
                }
                if (strstr($data['date'], $yesterdayGetDate)) {
                    $latestActivityLogsTableYesterday[$data['ip_address']] = $data;
                }
            }
        }
        
        $lastMonth = 0;
        if ($this->logsTotal) {
            foreach ($this->logsTotal as $data) {
                if (strstr($data['date'], $lastMonthGetDate)) {
                    $lastMonth ++;
                }
            }
        }
        
        $results['online_user'] = count($latestActivityLogsTableData);
        $results['latestActivityLogsTableThistDay'] = count($latestActivityLogsTableThistDay);
        $results['online_user_yesterday'] = count($latestActivityLogsTableYesterday);
        $results['online_user_lastMonth'] = count($latestActivityLogsTableLastMonth) + $lastMonth;
        $results['online_user_thisMonth'] = count($latestActivityLogsTableThistMonth);
        $results['online_user_thisWeak'] = count($activityLogsTableThistWeak);
        $results['activity_logs_total'] = count($activity_logs_total) + count($this->logsTotal);
        $results['activity_logs_total'] = count($activity_logs_total) + count($this->logsTotal);
        
        /*
         * $results['groupKeeper'] = $productGroupVisitKeeper;
         * $results['productKeeper'] = $productViewVisitKeeper;
         */
        
        return $results;
    }
    public function indexAction()
    {
        // var_dump('hhhhhhh');die;
        $this->setHeadTitle(__('Dashboard'));
        $view = array();
        $viewModel = new ViewModel($view);
        if ($this->userData->user_group_id == 33) {
            $viewModel->setTemplate("application/admin/user.phtml");
        } else 
            if ($this->userData->user_group_id == 5) {
                $viewModel->setTemplate("application/admin/userspecial.phtml");
            } else 
                if ($this->userData->user_group_id == 4) {
                    $viewModel->setTemplate("application/admin/instructor.phtml");
                } else {
                    $viewModel->setTemplate("application/admin/index.phtml");
                }
//         var_dump($this->getOnlineUser());die;

        $view['onlines'] = $this->getOnlineUser(); 
        return $viewModel->setVariables($view);
    }
    // public function depositListAction()
    // {
    //     //var_dump('hhhhhhh');die;
    //     $this->setHeadTitle(__('Deposit List'));
    //     $view = array();
    //     $viewModel = new ViewModel($view);
    

    //     // $view['onlines'] = $this->getOnlineUser(); 
    //     return $viewModel->setVariables($view);
    // }

    public function depositListAction()
	{
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	    // var_dump($this->params('status'));die;
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $depositTable = new DepositTable($this->getServiceLocator());
        // var_dump($this->userData);die;
        $user = null;
        if($this->userData->user_group_id == 3){
            // var_dump($this->userData);die;
            // var_dump('aa');die;
            $user = $this->userData->id;
        }
        $allData = $depositTable->getAll($user,$this->params('status'));
	    $grid->setTitle(__('Deposit list'));
	    $grid->setDefaultItemsPerPage(10);
	    $grid->setDataSource($allData,$dbAdapter);
	    
	    
	    $col = new Column\Select('id',"deposit");
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->setWidth(5);
	    $col->addStyle(new Style\Bold());
        $grid->addColumn($col);  
        
        $col = new Column\Select('username',"users");
	    $col->setLabel(__('Username'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
	    $col = new Column\Select('to_pay_price');
	    $col->setLabel(__('To Pay Price'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('discount');
	    $col->setLabel(__('Discount'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('tax');
	    $col->setLabel(__('Tax'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
       
        $col = new Column\Select('status','deposit');
	    $col->setLabel(__('Status'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	     
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction = new Column\Action\Icon();
	    $viewAction->setIconClass('glyphicon glyphicon-edit'); 
	    $viewAction->setLink('/admin/deposit-edit/' . $rowId);
	    $viewAction->setTooltipTitle(__('Edit'));
	    
	    $viewAction1 = new Column\Action\Icon();
	    $viewAction1->setIconClass('glyphicon glyphicon-remove');
	    $viewAction1->setLink('/admin/deposit-update-status/'.$rowId.'/حذف شده/token/'.$csrf);
        $viewAction1->setTooltipTitle(__('Delete'));
        
        $viewAction2 = new Column\Action\Icon();
	    $viewAction2->setIconClass('glyphicon glyphicon-print');
	    $viewAction2->setLink('/admin/print/'.$rowId.'/token/'.$csrf);
        $viewAction2->setTooltipTitle(__('Print'));

        $viewAction3 = new Column\Action\Icon();
	    $viewAction3->setIconClass('glyphicon glyphicon-ok');
	    $viewAction3->setLink('/admin/deposit-update-status/'.$rowId.'/پرداخت شده/token/'.$csrf);
	    $viewAction3->setTooltipTitle(__('Paid'));
	
	   
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction);
	    $actions2->addAction($viewAction1);
	    $actions2->addAction($viewAction2);
	    $actions2->addAction($viewAction3);
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	    
	    $link[] = '<a href="/admin/deposit-add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/admin/deposit-list/پرداخت شده" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Paid").'</a>';
	    $link[] = '<a href="/admin/deposit-list/در انتظار پرداخت" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Waiting for pay").'</a>';
	    // $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
	    $grid->setLink($link);
	     
	    $grid->render();
	    
	    return $grid->getResponse();   

    }

    public function depositDeletedListAction()
	{
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	    
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $depositTable = new DepositTable($this->getServiceLocator());
        $user = null;
        if(!$this->userData->user_group_id == 1 || !$this->userData->user_group_id == 2){
            $user = $this->userData->id;
        }
        $allData = $depositTable->getAllDeleted($user);
	    $grid->setTitle(__('Deposit list'));
	    $grid->setDefaultItemsPerPage(10);
	    $grid->setDataSource($allData,$dbAdapter);
	    
	    
	    $col = new Column\Select('id',"deposit");
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->setWidth(5);
	    $col->addStyle(new Style\Bold());
        $grid->addColumn($col);  
        
        $col = new Column\Select('username',"users");
	    $col->setLabel(__('Username'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
	    $col = new Column\Select('to_pay_price');
	    $col->setLabel(__('To Pay Price'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('discount');
	    $col->setLabel(__('Discount'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('tax');
	    $col->setLabel(__('Tax'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select(__('status'),'deposit');
	    $col->setLabel(__('Status'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	   
	     
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction = new Column\Action\Icon();
	    $viewAction->setIconClass('glyphicon glyphicon-edit'); 
	    $viewAction->setLink('/admin/deposit-edit/' . $rowId);
	    $viewAction->setTooltipTitle(__('Edit'));
	    
	    $viewAction1 = new Column\Action\Icon();
	    $viewAction1->setIconClass('glyphicon glyphicon-remove');
	    $viewAction1->setLink('/admin/deposit-delete/'.$rowId.'/token/'.$csrf);
        $viewAction1->setTooltipTitle(__('Delete'));
        
        $viewAction2 = new Column\Action\Icon();
	    $viewAction2->setIconClass('glyphicon glyphicon-print');
	    $viewAction2->setLink('/admin/print/'.$rowId.'/token/'.$csrf);
        $viewAction2->setTooltipTitle(__('Print'));
        
	
	   
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction);
	    // $actions2->addAction($viewAction1);
	    $actions2->addAction($viewAction2);
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	    
	    $link[] = '<a href="/admin/deposit-add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
	    $grid->setLink($link);
	     
	    $grid->render();
	    
	    return $grid->getResponse();   

    }
    
    public function printAction(){

        $this->setHeadTitle(__('Print'));
        $id = $this->params('id');
        $view = array();
        $depositTable = new DepositTable($this->getServiceLocator());
        $depositDetailsTable = new DepositDetailsTable($this->getServiceLocator());
        $depositAccountTable = new DepositAccountTable($this->getServiceLocator());
        $depositPaymentTable = new DepositPaymentTable($this->getServiceLocator());

        $getDepositData = $depositTable->getById($id);
        $getDepositDetailData = $depositDetailsTable->getById($getDepositData['id']);
        $getDepositAccountData = $depositAccountTable->getById($getDepositData['id']);
        $getDepositPaymentData = $depositPaymentTable->getById($getDepositData['id']);

        $view['getDepositData'] = $getDepositData;
        $view['getDepositDetailData'] = $getDepositDetailData;
        $view['getDepositAccountData'] = $getDepositAccountData;
        $view['getDepositPaymentData'] = $getDepositPaymentData;
        $view['remainPrice'] = $getDepositPaymentData[count($getDepositPaymentData) - 1]['remain_price'];
        $viewModel = new ViewModel($view);
        return $viewModel;

    }

    public function depositAddAction()
    {

        $this->setHeadTitle(__('Add deposit'));
        $validationExt = "jpg,jpeg,png,gif,mp4";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/deposit/";
        $view = array();
        $depositTable = new DepositTable($this->getServiceLocator());
        $depositDetailsTable = new DepositDetailsTable($this->getServiceLocator());
        $depositAccountTable = new DepositAccountTable($this->getServiceLocator());
        $depositPaymentTable = new DepositPaymentTable($this->getServiceLocator());
        $depositFilesTable = new DepositFilesTable($this->getServiceLocator());
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $file = $request->getFiles()->toArray();//var_dump($file);die;

            $depositData['user_id'] =  $this->userData->id;
            $depositData['discount'] = $data['discount'];
            $depositData['tax'] = $data['tax'];
            $depositData['to_pay_price'] = $data['to_pay'];
            $depositData['description'] = $data['description'];
            $addDeposit = $depositTable->add($depositData);

            if(!empty($data['request_check'])){
                $depositData['request_check'] = $data['request_check'];
                $depositData['created_check'] = $data['created_check'];
                $depositData['pay_to'] = $data['pay_to'];
                $updateDeposit = $depositTable->addCheck($depositData,$addDeposit);
            }

            foreach($data['product'] as $key=>$detail){
                $depositDetailData['deposit_id'] = $addDeposit;
                $depositDetailData['product'] = $data['product'][$key];
                $depositDetailData['count'] = $data['count'][$key];
                $depositDetailData['price'] = $data['price'][$key];
                $depositDetailData['sum'] = $data['sum'][$key];
                $addDepositDetails = $depositDetailsTable->add($depositDetailData);
            }

            foreach($data['account_type'] as $key=>$detail){
                if(!empty($data['bank_name'][$key])){
                    $depositDetailData['deposit_id'] = $addDeposit;
                    $depositDetailData['account_type'] = $data['account_type'][$key];
                    $depositDetailData['bank_name'] = $data['bank_name'][$key];
                    $depositDetailData['account_number'] = $data['account_number'][$key];
                    $depositDetailData['account_owner'] = $data['account_owner'][$key];
                    $addDepositAccount = $depositAccountTable->add($depositDetailData);
                }
            }
            
            foreach($data['payment_price'] as $key=>$detail){
                if(!empty($data['payment_price'][$key])){
                    $depositPaymentData['deposit_id'] = $addDeposit;
                    $depositPaymentData['user_id'] =  $this->userData->id;
                    $depositPaymentData['price'] = $data['payment_price'][$key];
                    $depositPaymentData['description'] = $data['payment_desc'][$key];
                    $depositPaymentData['remain_price'] = $data['payment_remain_price'];
                    $addDepositPayment = $depositPaymentTable->add($depositPaymentData);
                }
                
            }
            
            foreach($file['file'] as $key=>$detail){//var_dump($detail);
                if ($detail['tmp_name'] != '') {
                    $newFileName = md5(time() . $detail['name']);
                    $isUploaded = $this->imageUploader()->UploadFile($detail, $validationExt, $uploadDir, $newFileName);
                    $depositFileData['file'] = $isUploaded[1];
                    $depositFileData['deposit_id'] = $addDeposit;
                    $depositFileData['user_id'] =  $this->userData->id;
                    $addDepositFile = $depositFilesTable->add($depositFileData);
                }
                
            }

            $this->flashMessenger()->addSuccessMessage(__("Operation is done successfully."));
            return $this->redirect()->toUrl('/admin/deposit-list');
        }
        $viewModel = new ViewModel($view);
    

        // $view['onlines'] = $this->getOnlineUser(); 
        return $viewModel->setVariables($view);
    }

    public function depositEditAction()
    {

        $this->setHeadTitle(__('Edit Deposit'));
        $validationExt = "jpg,jpeg,png,gif,mp4";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/deposit/";
        $id = $this->params('id');
        $view = array();
        $depositTable = new DepositTable($this->getServiceLocator());
        $depositDetailsTable = new DepositDetailsTable($this->getServiceLocator());
        $depositAccountTable = new DepositAccountTable($this->getServiceLocator());
        $depositPaymentTable = new DepositPaymentTable($this->getServiceLocator());
        $depositFilesTable = new DepositFilesTable($this->getServiceLocator());

        $getDepositData = $depositTable->getById($id);
        $getDepositDetailData = $depositDetailsTable->getById($getDepositData['id']);
        $getDepositAccountData = $depositAccountTable->getById($getDepositData['id']);
        $getDepositPaymentData = $depositPaymentTable->getById($getDepositData['id']);
        $getDepositFilesData = $depositFilesTable->getById($getDepositData['id']);
        $accountTypeArr = ['شماره کارت','شماره حساب','شماره شبا'];
        
         $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $file = $request->getFiles()->toArray();
            // var_dump($data);die;
            $depositData['user_id'] =  $this->userData->id;
            $depositData['discount'] = $data['discount'];
            $depositData['tax'] = $data['tax'];
            $depositData['to_pay_price'] = $data['to_pay'];
            $depositData['description'] = $data['description'];
            // $depositData['request_check'] = $data['request_check'];
            // $depositData['created_check'] = $data['created_check'];
            // $depositData['pay_to'] = $data['pay_to'];
            $depositData['payment_details'] = $data['payment_details'];
            $editDeposit = $depositTable->update($depositData,$id);

            if(!empty($data['request_check'])){
                $depositData['request_check'] = $data['request_check'];
                $depositData['created_check'] = $data['created_check'];
                $depositData['pay_to'] = $data['pay_to'];
                $updateDeposit = $depositTable->addCheck($depositData,$addDeposit);
            }

            foreach($data['product'] as $key=>$detail){
                if(isset($data['deposit_detail_id'][$key])){
                    $depositDetailData['product'] = $data['product'][$key];
                    $depositDetailData['count'] = $data['count'][$key];
                    $depositDetailData['price'] = $data['price'][$key];
                    $depositDetailData['sum'] = $data['sum'][$key];
                    $updateDepositDetails = $depositDetailsTable->update($depositDetailData,$data['deposit_detail_id'][$key]);
                }else{
                    $depositDetailData['deposit_id'] = $id;
                    $depositDetailData['product'] = $data['product'][$key];
                    $depositDetailData['count'] = $data['count'][$key];
                    $depositDetailData['price'] = $data['price'][$key];
                    $depositDetailData['sum'] = $data['sum'][$key];
                    $addDepositDetails = $depositDetailsTable->add($depositDetailData);
                }
            }

            foreach($data['account_type'] as $key=>$detail){
                if(isset($data['account_deposit_detail_id'][$key])){
                    $depositDetailData['account_type'] = $data['account_type'][$key];
                    $depositDetailData['bank_name'] = $data['bank_name'][$key];
                    $depositDetailData['account_number'] = $data['account_number'][$key];
                    $depositDetailData['account_owner'] = $data['account_owner'][$key];
                    $addDepositPayment = $depositAccountTable->update($depositDetailData,$data['account_deposit_detail_id'][$key]);

                }else{
                    $depositDetailData['deposit_id'] = $id;
                    $depositDetailData['account_type'] = $data['account_type'][$key];
                    $depositDetailData['bank_name'] = $data['bank_name'][$key];
                    $depositDetailData['account_number'] = $data['account_number'][$key];
                    $depositDetailData['account_owner'] = $data['account_owner'][$key];
                    $addDepositPayment = $depositAccountTable->add($depositDetailData);
                }   
            }

            foreach($data['payment_price'] as $key=>$detail){
                if(isset($data['payment_deposit_id'][$key])){
                    // $depositPaymentData['deposit_id'] = $addDeposit;
                    $depositPaymentData['user_id'] =  $this->userData->id;
                    $depositPaymentData['price'] = $data['payment_price'][$key];
                    $depositPaymentData['description'] = $data['payment_desc'][$key];
                    $depositPaymentData['remain_price'] = $data['payment_remain_price'];
                    $addDepositPayment = $depositPaymentTable->update($depositPaymentData,$data['payment_deposit_id'][$key]);
                }else{
                    if(!empty($data['payment_price'][$key])){
                        $depositPaymentData['deposit_id'] = $id;
                        $depositPaymentData['user_id'] =  $this->userData->id;
                        $depositPaymentData['price'] = $data['payment_price'][$key];
                        $depositPaymentData['description'] = $data['payment_desc'][$key];
                        $depositPaymentData['remain_price'] = $data['payment_remain_price'];
                        $addDepositPayment = $depositPaymentTable->add($depositPaymentData);
                    }
                }
                
                
                
            }
            
            foreach($file['file'] as $key=>$detail){//var_dump($detail);
                if ($detail['tmp_name'] != '') {
                    $newFileName = md5(time() . $detail['name']);
                    $isUploaded = $this->imageUploader()->UploadFile($detail, $validationExt, $uploadDir, $newFileName);
                    $depositFileData['file'] = $isUploaded[1];
                    $depositFileData['deposit_id'] = $id;
                    $depositFileData['user_id'] =  $this->userData->id;
                    $addDepositFile = $depositFilesTable->add($depositFileData);
                }
                
            }

            $this->flashMessenger()->addSuccessMessage(__("Operation is done successfully."));
            return $this->redirect()->toUrl('/admin/deposit-list');
        }   


        $view['accountTypeArr'] = $accountTypeArr;
        $view['getDepositData'] = $getDepositData;
        $view['getDepositDetailData'] = $getDepositDetailData;
        $view['getDepositAccountData'] = $getDepositAccountData;
        $view['getDepositPaymentData'] = $getDepositPaymentData;
        $view['getDepositFilesData'] = $getDepositFilesData;
        $viewModel = new ViewModel($view);
        return $viewModel;


        // $this->setHeadTitle(__('Add deposit'));
        // $view = array();
        // $depositTable = new DepositTable($this->getServiceLocator());
        // $depositDetailsTable = new DepositDetailsTable($this->getServiceLocator());
        // $depositAccountTable = new DepositAccountTable($this->getServiceLocator());
        

        // $request = $this->getRequest();
        // if ($request->isPost()) {
        //     $data = $request->getPost();
        //     // var_dump($data);die;
        //     $depositData['user_id'] =  $this->userData->id;
        //     $depositData['discount'] = $data['discount'];
        //     $depositData['tax'] = $data['tax'];
        //     $depositData['to_pay_price'] = $data['to_pay'];
        //     $depositData['description'] = $data['description'];
        //     $depositData['request_check'] = $data['request_check'];
        //     $depositData['created_check'] = $data['created_check'];
        //     $depositData['pay_to'] = $data['pay_to'];
        //     $depositData['payment_details'] = $data['payment_details'];
        //     $addDeposit = $depositTable->add($depositData);
        //     // var_dump($addDeposit);//die;
        //     foreach($data['product'] as $key=>$detail){
        //         $depositDetailData['deposit_id'] = $addDeposit;
        //         $depositDetailData['product'] = $data['product'][$key];
        //         $depositDetailData['count'] = $data['count'][$key];
        //         $depositDetailData['price'] = $data['price'][$key];
        //         $depositDetailData['sum'] = $data['sum'][$key];
        //         $addDepositDetails = $depositDetailsTable->add($depositDetailData);
        //         // var_dump($addDepositDetails);die;
        //     }

        //     foreach($data['account_type'] as $key=>$detail){
        //         $depositDetailData['deposit_id'] = $addDeposit;
        //         $depositDetailData['account_type'] = $data['account_type'][$key];
        //         $depositDetailData['bank_name'] = $data['bank_name'][$key];
        //         $depositDetailData['account_number'] = $data['account_number'][$key];
        //         $depositDetailData['account_owner'] = $data['account_owner'][$key];
        //         $addDepositPayment = $depositAccountTable->add($depositDetailData);
        //         // var_dump($addDepositDetails);die;
        //     }
        //     //$depositPaymentData['deposit_id'] = $data['payment_details'];
        //     // var_dump($addDepositPayment);die;
        //     $this->flashMessenger()->addSuccessMessage(__("Operation is done successfully."));
        //     return $this->redirect()->toUrl('/admin/deposit-list');
        // }
        // $viewModel = new ViewModel($view);
    
        // return $viewModel->setVariables($view);
    }
    
    // public function depositDeleteAction(){
    //     $this->setHeadTitle(__('Delete Deposit'));
    //     $id = $this->params('id');

    //     $depositTable = new DepositTable($this->getServiceLocator());
    //     if($id){
    //         $delete = $depositTable->delete('deleted',$id);
    //         if($delete){
    //             $this->flashMessenger()->addSuccessMessage(__("Operation is done successfully."));
    //             return $this->redirect()->toUrl('/admin/deposit-list');
    //         }
    //     }

    // }

    public function depositUpdateStatusAction(){//var_dump('aaaa');die;
        $this->setHeadTitle(__('Delete Deposit'));
        $id = $this->params('id');
        $status = $this->params('status');

        $depositTable = new DepositTable($this->getServiceLocator());
        if($id){
            $delete = $depositTable->updateStatus($status,$id);
            if($delete){
                $this->flashMessenger()->addSuccessMessage(__("Operation is done successfully."));
                return $this->redirect()->toUrl('/admin/deposit-list');
            }
        }

    }

    public function userAction()
    {
        die();
        $this->setHeadTitle(__('Dashboard'));
        
        $usersTable = new UsersTable($this->getServiceLocator());
        $allUsers = $usersTable->getAllUsers();
        $view['all_users'] = $allUsers;
        
        $commentsTable = new CommentsTable($this->getServiceLocator());
        $allComments = $commentsTable->getAllComments();
        $view['all_comments'] = $allComments;
        
        $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
        $allBlogPosts = $blogPostsTable->getAllPosts();
        $view['all_blog_posts'] = $allBlogPosts;
        
        $viewModel = new ViewModel($view);
        if ($this->userData->user_group_id == 33) {
            $viewModel->setTemplate("application/admin/user.phtml");
        } else {
            $viewModel->setTemplate("application/admin/index.phtml");
        }
        
        return $viewModel;
    }

    public function logoutAction()
    {
        if (! $this->userData) {
            return $this->redirect()->toRoute('home');
        }
        $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
        $ttt = $loginLogTable->updateUserLastLogin($this->identity()->id);
        
        $this->authenticationService->logOut();
        GeneralHelper::removeSession();
        return $this->redirect()->toRoute('home');
    }

    public function editAccountAction()
    {
        $this->setHeadTitle(__('Edit profile'));
        $userId = $this->userData->id;
        $usersTable = new UsersTable($this->getServiceLocator());
        $userInfo = $usersTable->getUserById($userId);
        if (! $userInfo) {
            return $this->redirect()->toRoute('application-admin');
        } else {
            $userAccountForm = new UserAccountForm($userInfo);
            $userAccountForm->populateValues($userInfo[0]);
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = $request->getPost();
                $userAccountForm->setData($data);
                if ($userAccountForm->isValid()) {
                    $updatedUserInfo = $userAccountForm->getData();
                    $isUpdated = $usersTable->updateUser($updatedUserInfo, $userId);
                    if ($isUpdated == 'email_exists') {
                        $this->layout()->errorMessage = __("This email is taken. Please choose another one.");
                    } elseif ($isUpdated !== false) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation is done successfully."));
						return $this->redirect()->toRoute('application-admin');
					}
				}
			}
		}   	
    	$view['userAccountForm'] = $userAccountForm;
    	return new ViewModel($view);
    }
}   
