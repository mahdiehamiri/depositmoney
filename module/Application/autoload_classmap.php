<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
    'ZfcDatagridExamples\Data\Doctrine2'            => __DIR__ . '/Data/Doctrine2.php',
    'Application\Data\PhpArray'                     => __DIR__ . '/Data/PhpArray.php',
    'ZfcDatagridExamples\Data\ZendSelect'           => __DIR__ . '/Data/ZendSelect.php',
    'Application\Module'                            => __DIR__ . '/Module.php',
    'Application\Controller\AdminController'        => __DIR__ . '/src/Application/Controller/AdminController.php',
    'Application\Controller\AjaxController'         => __DIR__ . '/src/Application/Controller/AjaxController.php',
    'Application\Controller\IndexController'        => __DIR__ . '/src/Application/Controller/IndexController.php',
    'Application\Form\AdvancedSearchForm'           => __DIR__ . '/src/Application/Form/AdvancedSearchForm.php',
    'Application\Form\Element\Mobile'               => __DIR__ . '/src/Application/Form/Element/Mobile.php',
    'Application\Form\Element\Phone'                => __DIR__ . '/src/Application/Form/Element/Phone.php',
    'Application\Form\ForgotPasswordForm'           => __DIR__ . '/src/Application/Form/ForgotPasswordForm.php',
    'Application\Form\GetRestPasscodeForm'          => __DIR__ . '/src/Application/Form/GetRestPasscodeForm.php',
    'Application\Form\GetVerifyCodeForm'            => __DIR__ . '/src/Application/Form/GetVerifyCodeForm.php',
    'Application\Form\LoginForm'                    => __DIR__ . '/src/Application/Form/LoginForm.php',
    'Application\Form\LoginSigninForm'              => __DIR__ . '/src/Application/Form/LoginSigninForm.php',
    'Application\Form\RegistrationForm'             => __DIR__ . '/src/Application/Form/RegistrationForm.php',
    'Application\Form\RequestProjectForm'           => __DIR__ . '/src/Application/Form/RequestProjectForm.php',
    'Application\Form\RequestTeachingForm'          => __DIR__ . '/src/Application/Form/RequestTeachingForm.php',
    'Application\Form\ResetPasswordForm'            => __DIR__ . '/src/Application/Form/ResetPasswordForm.php',
    'Application\Form\SearchForm'                   => __DIR__ . '/src/Application/Form/SearchForm.php',
    'Application\Form\UserAccountForm'              => __DIR__ . '/src/Application/Form/UserAccountForm.php',
    'Application\Helper\BaseAdminController'        => __DIR__ . '/src/Application/Helper/BaseAdminController.php',
    'Application\Helper\BaseController'             => __DIR__ . '/src/Application/Helper/BaseController.php',
    'Application\Helper\BaseCustomerController'     => __DIR__ . '/src/Application/Helper/BaseCustomerController.php',
    'Application\Helper\BasePluginController'       => __DIR__ . '/src/Application/Helper/BasePluginController.php',
    'Application\Helper\Date'                       => __DIR__ . '/src/Application/Helper/Date.php',
    'Application\Helper\FormGenerator'              => __DIR__ . '/src/Application/Helper/FormGenerator.php',
    'Application\Helper\GeneralHelper'              => __DIR__ . '/src/Application/Helper/GeneralHelper.php',
    'Application\Helper\GravatarGenerator'          => __DIR__ . '/src/Application/Helper/GravatarGenerator.php',
    'Application\Helper\ImageUploader'              => __DIR__ . '/src/Application/Helper/ImageUploader.php',
    'Application\Helper\ImageValidator'             => __DIR__ . '/src/Application/Helper/ImageValidator.php',
    'Application\Helper\InsertIgnore'               => __DIR__ . '/src/Application/Helper/InsertIgnore.php',
    'Application\Helper\Jdates'                     => __DIR__ . '/src/Application/Helper/Jdates.php',
    'Application\Helper\LogListener'                => __DIR__ . '/src/Application/Helper/LogListener.php',
    'Application\Helper\MailService'                => __DIR__ . '/src/Application/Helper/MailService.php',
    'Application\Helper\MenuGenerator'              => __DIR__ . '/src/Application/Helper/MenuGenerator.php',
    'Application\Helper\NumberConverter'            => __DIR__ . '/src/Application/Helper/NumberConverter.php',
    'Application\Helper\Permissions'                => __DIR__ . '/src/Application/Helper/Permissions.php',
    'Application\Helper\PhpArray'                   => __DIR__ . '/src/Application/Helper/PhpArray.php',
    'Application\Helper\RequiredMarkInFormLabel'    => __DIR__ . '/src/Application/Helper/RequiredMarkInFormLabel.php',
    'Application\Helper\SMSService'                 => __DIR__ . '/src/Application/Helper/SMSService.php',
    'Application\Helper\UrlGenerator'               => __DIR__ . '/src/Application/Helper/UrlGenerator.php',
    'Application\Helper\UrlGeneratorInController'   => __DIR__ . '/src/Application/Helper/UrlGeneratorInController.php',
    'Application\Helper\UrlGeneratorPage'           => __DIR__ . '/src/Application/Helper/UrlGeneratorPage.php',
    'Application\Helper\ZendSelect'                 => __DIR__ . '/src/Application/Helper/ZendSelect.php',
    'Application\Model\ApplicationEntityViewsTable' => __DIR__ . '/src/Application/Model/ApplicationEntityViewsTable.php',
    'Application\Model\ApplicationFileManagerTable' => __DIR__ . '/src/Application/Model/ApplicationFileManagerTable.php',
    'Application\Model\ApplicationTagsIndexTable'   => __DIR__ . '/src/Application/Model/ApplicationTagsIndexTable.php',
    'Application\Model\ApplicationTagsTable'        => __DIR__ . '/src/Application/Model/ApplicationTagsTable.php',
    'Application\Model\CommentsTable'               => __DIR__ . '/src/Application/Model/CommentsTable.php',
    'Application\Model\EducationFieldofstudyTable'  => __DIR__ . '/src/Application/Model/EducationFieldofstudyTable.php',
    'Application\Model\EducationLessonsTable'       => __DIR__ . '/src/Application/Model/EducationLessonsTable.php',
    'Application\Model\PermissionsTable'            => __DIR__ . '/src/Application/Model/PermissionsTable.php',
    'Application\Model\ShopProductsTable'           => __DIR__ . '/src/Application/Model/ShopProductsTable.php',
    'Application\Model\UserGroupPermissionsTable'   => __DIR__ . '/src/Application/Model/UserGroupPermissionsTable.php',
    'Application\Model\UserGroupsTable'             => __DIR__ . '/src/Application/Model/UserGroupsTable.php',
    'Application\Model\UsersDataTable'              => __DIR__ . '/src/Application/Model/UsersDataTable.php',
    'Application\Model\UsersLoginLogTable'          => __DIR__ . '/src/Application/Model/UsersLoginLogTable.php',
    'Application\Model\UsersTable'                  => __DIR__ . '/src/Application/Model/UsersTable.php',
    'Application\Services\Authentication'           => __DIR__ . '/src/Application/Services/Authentication.php',
    'Application\Services\AutoAbstractFactory'      => __DIR__ . '/src/Application/Services/AutoAbstractFactory.php',
    'Application\Services\BaseModel'                => __DIR__ . '/src/Application/Services/BaseModel.php',
    'Application\Services\MyAuthStorage'            => __DIR__ . '/src/Application/Services/MyAuthStorage.php',
);
