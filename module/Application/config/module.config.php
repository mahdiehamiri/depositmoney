<?php
return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action'     => 'index',
                    ),
                ),
            ),	
            'application' => array (
                'type' => 'segment',
                'options' => array (
                    'route' => '[/:lang][/:action][/:id][/hash/:hash][/page/:page][/:params][?:query]',
                    'constraints' => array (
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'params' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'hash' => '[a-zA-Z0-9_-]*',
                        'page' => '[1-9][0-9]*',
                        'lang' => '[a-zA-Z]{2}[\/]?',
                        'query' => '.*',
            
                    ),
                    'defaults' => array (
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index'
                    )
                )
            ),
            'application-ajax-request-project' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application-ajax-request-project',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Ajax',
                        'action' => 'refreshrp'
            
                    ),
                ),
            ),
            'application-ajax-delete-all' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application-ajax-delete-all',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Ajax',
                        'action' => 'delete-all'
            
                    ),
                ),
            ),
    
            'application-links' => array (
                'type' => 'segment',
                'options' => array (
                    'route' => '[/:lang]/index[/:action]',
                    'constraints' => array (
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'lang' => '[a-zA-Z]{2}',
            
                    ),
                    'defaults' => array (
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index'
                    )
                )
            ),
        		
            'application-admin' => array (
                'type' => 'segment',
                'options' => array (
                    'route' => '[/:lang]/admin[/:action][/:id][/:status][/page/:page][/delete/:delete][/clear/:clear][/token/:token][/count/:count][/hash/:hash]',
                    'constraints' => array (
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'lang' => '[a-zA-Z]{2}',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                        'delete' => '[0-9]+',
                        'clear' => 'true',
                        'count' => '[0-9]+',
                        'token' => '[a-f0-9]+',
                        'hash' => '[a-f0-9]{32}',
            
                    ),
                    'defaults' => array (
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Admin',
                        'action' => 'index'
                    )
                )
            ),
         
        	
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            // Datasources
            'phpArray' => 'Application\Helper\PhpArray',
            //'zfcDatagrid.examples.data.doctrine2' => 'ZfcDatagridExamples\Data\Doctrine2',
            'zendSelect' => 'Application\Helper\ZendSelect'
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'fa',
        'translation_file_patterns' => array(
            array(
                'type'     => 'phpArray',
                'base_dir' =>  './language',
                'pattern'  => '%s.php',
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            include_once(__DIR__ . '/../template_map.php'),
//              'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
//           	 'blogSidebar'			  => __DIR__ . '/../view/layout/blog-sidebar.phtml',
//          	 'emailLayout'		      => __DIR__ . '/../view/layout/email-layout.phtml',
//              'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
//              'error/404'               => __DIR__ . '/../view/error/404.phtml',
//              'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ), 
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    
);
