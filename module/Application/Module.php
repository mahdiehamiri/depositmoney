<?php

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Application\Helper\UrlGenerator;
use Application\Helper\Date;
use Application\Helper\Permissions;
use Application\Helper\UrlGeneratorInController;
use Application\Helper\MenuGenerator;
use Application\Helper\GravatarGenerator;
use Application\Helper\MailService;
use Zend\Validator\AbstractValidator; 
use Zend\I18n\Translator\Translator;
use Application\Helper\NumberConverter;
use Application\Helper\ImageUploader;
use Application\Helper\GeneralHelper;
use Application\Helper\GeneralControllerHelper;
use Application\Helper\UrlGeneratorPage;
use Zend\Session\Container;
use Application\Helper\RequiredMarkInFormLabel;
use Application\Helper\SMSService;
use Zend\Db\TableGateway\TableGateway as ZendTableGateway;
use AtDataGrid\DataSource\ZendDb\TableGateway;
use AtDataGrid\Manager;
use AtDataGrid\Renderer\Html;
use Application\Helper\User;
use Application\Helper\FormGenerator;
use Application\Form\RegistrationForm;
use Application\Form\LoginForm;
use Contact\Form\ContactForm;
use Application\Helper\LogListener;
use Application\Services\MyAuthStorage;
use Application\Helper\Redirect;
use Application\Helper\Url;
use TpMinify\Helper\HeadScript;
use Application\Helper\SocialMediaHelper;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
    
        $eventManager        = $e->getApplication()->getEventManager();
        
        $logListener        = new LogListener();
        $logListener->setServiceLocator($e->getApplication()->getServiceManager());
        $eventManager->attach($logListener);
        
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        $app = $e->getParam('application');
        $app->getEventManager()->attach(MvcEvent::EVENT_RENDER, array($this, 'setFormToView'), 100);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
    	return array(
    			'factories' => array(
    				'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
    				'AuthenticationService'   => function ($sm){
    					return new \Application\Services\Authentication($sm);
    				},	
    				'Application\Services\MyAuthStorage' => function($sm){
    				return new MyAuthStorage();
    				},
    			),
    			'aliases' => array(
    					'Zend\Authentication\AuthenticationService' => 'AuthenticationService',
    			),
    	);
    }
    
    public function getViewHelperConfig()
    {
    	return array(
    		'factories' => array(
    			'menuGenerator' =>  function() {
    					return new MenuGenerator();
    			},
    			'urlGenerator' =>  function() {
    					return new UrlGenerator();
    			},
    			'urlGeneratorPage' =>  function() {
    			return new UrlGeneratorPage();
    			},
    			'date' => function () {
    				return  new Date();
    			},
    			'gravatarGenerator' => function() {
    				return new GravatarGenerator();
    			},
    			'blogGenerator' => function () {
    			return new BlogGenerator ();
    			},
    			'socialMediaHelper' => function () {
    			    return new SocialMediaHelper ();
    			}
    			
    		),
    		    'invokables' => array(
    		    'formlabel' => new RequiredMarkInFormLabel(),
    		    ),
    		
    	);
    }
    public function getControllerConfig()
    {
        return array(
            'abstract_factories' => array(
                'Application\Services\AutoAbstractFactory'
            ),
        );
    }
    public function getControllerPluginConfig()
    {
    	return array(
    		'factories' => array(
        		    'generalHelper' => function() {
        		    return new GeneralHelper();
        		    },
    				'permissions' =>  function () {
    					return new Permissions();
   					},
    				'mailService' =>  function () {
    					return new MailService();
   					},
   					'numberConverter' => function() {
   						return new NumberConverter();
   					},
   					'urlGeneratorInController' => function() {
   						return new UrlGeneratorInController();
   					},
   					'Redirect' => function() {
   					    return new Redirect();
   					},
   					'Url' => function() {
   					return new Url();
   					},
   					'imageUploader' => function () {
   					return new ImageUploader();
   					},
   					'smsService' =>  function () {
   						return new SMSService();
   					},  
   					'formGenerator' =>  function () {
   					    return new FormGenerator();
   					}, 
   							'headscript' => function() {
   								return new HeadScript();
   							}
   					 
    		)
    	);
    }
    
    public function setFormToView($event)
    {
        $registrationForm = new RegistrationForm();
      //  $loginForm = new LoginForm();
        $contactForm = new ContactForm();
        $viewModel = $event->getViewModel();
        $viewModel->setVariables(array(
            'registrationForm' => $registrationForm,
      //      'loginForm'        => $loginForm,
            'contactForm'      => $contactForm
        ));
        
    }
    
}
