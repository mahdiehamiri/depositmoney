<?php
return array(
    'router' => array(
        'routes' => array( 
            'configuration-home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/configuration[/:action][/:id]',
                	'constraints' => array(
                	        'lang' => '[a-zA-Z]{2}',
                			'page' => '[1-9][0-9]*' //star is used to show that the second digit can or cannot be entered.
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Configuration\Controller',
        				'controller' => 'Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'configuration-admin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-configuration[/:action][/:id][/token/:token]', 
                        'constraints' => array(
                                'lang' => '[a-zA-Z]{2}',
                                'id' => '[1-9][0-9]*',
                                'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
                        		'token' => '[a-f0-9]{32}'
                        ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Configuration\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-configurations' 
                    ),
                ) 
            ),   
        ),
    ),  
   
    'view_manager' => array(
        'template_map' => array(
            include_once(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
