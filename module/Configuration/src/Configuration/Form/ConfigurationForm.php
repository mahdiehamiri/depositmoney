<?php

namespace Configuration\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ConfigurationForm extends Form
{
	public function __construct()
	{
		parent::__construct('postForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		$name = new Element\Text('name');
		$name->setAttributes(array(
				'id'    => 'configuration-name',
				'class' => 'form-control',
		        'required' => 'required',
		        'placeholder' => __('Configuration name'),
		));
		$name->setLabel(__("Configuration name"));
		
		$value = new Element\Text('value');
		$value->setAttributes(array(
		    'id'    => 'configuration-value',
		    'class' => 'form-control',
		    'required' => 'required',
		    'placeholder' => __('Configuration value'),
		));
		$value->setLabel(__("Configuration value"));
		
		
		
	
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save and close"));
		$submit->setAttributes(array(
		    'id'    => 'addEducationForm-button',
		    'class' => 'btn btn-primary validate[required]'
		));
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(__("Save and New"));
		$submit2->setAttributes(array(
		    'id'    => 'addEducationForm-button',
		    'class' => 'btn btn-primary validate[required]'
		));
		
		$this->add($name)
		     ->add($value)		    
			 ->add($csrf)
			 ->add($submit)
			 ->add($submit2);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'name',
				'required' => true,
				
			)
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'value',
		        'required' => true,
		
		    )
		));
	
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
	
	/* public function isValid()
	{
		$this->getInputFilter()->remove('tag_title');
		return parent::isValid();
	} */
}
