<?php

namespace Configuration\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Configuration\Form\ConfigurationForm;
use Configuration\Model\ShopCategoriesTable;
use Configuration\Model\ShopProductsTable;
use Configuration\Model\UsersTable;
use Configuration\Model\ConfigurationTable;

class AdminController extends BaseAdminController
{
	protected $noOfPostsPerPage = 100;
	
	public function listConfigurationsAction()
	{
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	    
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $configurationTable = new ConfigurationTable($this->getServiceLocator());
	    $allConfigurations = $configurationTable->getAllConfigurations();
	    $grid->setTitle(__('Configuration list'));
	    $grid->setDefaultItemsPerPage(10);
	    $grid->setDataSource($allConfigurations,$dbAdapter);
	    
	    
	    $col = new Column\Select('id',"configuration");
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->setWidth(5);
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);  
	    
	    $col = new Column\Select('name');
	    $col->setLabel(__('Name'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('value');
	    $col->setLabel(__('Value'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	   
	     
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction = new Column\Action\Icon();
	    $viewAction->setIconClass('glyphicon glyphicon-edit'); 
	    $viewAction->setLink("/". $this->lang .'/admin-configuration/edit-configuration/' . $rowId);
	    $viewAction->setTooltipTitle(__('Edit'));
	    
	    $viewAction1 = new Column\Action\Icon();
	    $viewAction1->setIconClass('glyphicon glyphicon-remove');
	    $viewAction1->setLink("/". $this->lang .'/admin-configuration/delete-configuration/'.$rowId.'/token/'.$csrf);
	    $viewAction1->setTooltipTitle(__('Delete'));
	
	   
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction);
	    $actions2->addAction($viewAction1);
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	    
	    $link[] = '<a href='."/". $this->lang .'/admin-configuration/add-configuration class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
	    $grid->setLink($link);
	     
	    $grid->render();
	    
	    return $grid->getResponse();   

	}
	
	public function addConfigurationAction()
	{
	    $configurationTable = new ConfigurationTable($this->getServiceLocator());
	
	    $configurationForm = new ConfigurationForm();
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $data = $postData->toArray();
	        if ($configurationForm->setData($postData)->isValid()) {
	            $postData = $configurationForm->getData();
	            try {
	               
	                $isAdded = $configurationTable->addConfiguration($postData);
	                if ($isAdded) {
	                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                    return $this->redirect()->toRoute('configuration-admin');
	                } else {
	                    $this->layout()->errorMessage = __("Operation failed!");
	                }
	            } catch (\Exception $e) {
	                $this->layout()->errorMessage = __("The name is taken, please choose another one.");
	            }
	        }
	    }
	    $view['configurationForm'] = $configurationForm;
	    return new ViewModel($view);
	}
	
	public function editConfigurationAction()
	{
	    $id = $this->params('id');
	    $configurationTable = new ConfigurationTable($this->getServiceLocator());
	   
	    $configurationForm = new ConfigurationForm();
	    
	    $configurationData = $configurationTable->getConfigurationById($id);
	    
	    $configurationForm->populateValues($configurationData[0]);
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $data = $postData->toArray();

	        if ($configurationForm->setData($postData)->isValid()) {
	            $postData = $configurationForm->getData(); 
	            try {
	               
	                $isedited = $configurationTable->editConfiguration($postData, $id);
	                if ($isedited !== false) {
	                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                    return $this->redirect()->toRoute('configuration-admin');
	                } else {
	                    $this->flashMessenger()->addInfoMessage(__("Operation failed!"));
	                    return $this->redirect()->toRoute('configuration-admin');
	                }
	            } catch (\Exception $e) {
	                $this->layout()->errorMessage = __("The name is taken, please choose another one.");
	            }
	        }
	    }
	    $view['configurationForm'] = $configurationForm;
	    return new ViewModel($view);
	}

	public function deleteConfigurationAction()
	{
		$container = new Container('token');
		$token = $this->params('token');
		$id = $this->params('id');
	    $configurationTable = new ConfigurationTable($this->getServiceLocator());
	    $taxdata = $configurationTable->getConfigurationById($id);
		if ($taxdata){
		    if ($id && $container->offsetExists('csrf')) {
		        if ($container->offsetGet('csrf') == $token) {

		            $isDeleted = $configurationTable->deleteConfiguration($id);
		            if ($isDeleted) {
		                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
		            } else {
		                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
		            }
		        }
		    }
		}
		return $this->redirect()->toRoute('configuration-admin');
	}
}
