<?php 
namespace Configuration\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class ConfigurationTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'configuration';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('configuration', $this->getDbAdapter());
    }

    public function getRecords()
    {
        return $this->tableGateway->select()->toArray();
    }
    
	public function getConfigByName($name)
	{
	    $select = new Select($this->table);
	    $select->where(array(
	        'name' => $name
	    ));
	    $config = $this->tableGateway->selectWith($select)->toArray();
	    if ($config) {
	        return $config[0]["value"];
	    } else {
	        return false;
	    }
	}
	
	public function getConfigurationById($id)
	{
	    $select = new Select($this->table);
	    $select->where(array(
	        'id' => $id
	    ));
	    return $this->tableGateway->selectWith($select)->toArray();
	}	

	public function getAllConfigurations()
	{
	    $select = new Select($this->table);
	    return $select;
	}
	
	public function addConfiguration($postData)
	{
        $this->tableGateway->insert(array(
             'name' 	      => $postData['name'],
             'value'          => $postData['value'],
        ));
        return $this->tableGateway->getLastInsertValue();
	}

	
	public function editConfiguration($editData, $Id)
	{
        $data = (array(
            'name' 	      => $editData['name'],
            'value'         => $editData['value'],
        ));

		$update = $this->tableGateway->update($data, array('id' => $Id));
		return $update;
	}
	
	public function deleteConfig($name)
	{

		return $this->tableGateway->delete(array(
				'name' => $name
		));

	}

}
