<?php
return array(
    'router' => array(
        'routes' => array( 
            'user-home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/user[/:action][/:id]',
                	'constraints' => array(
                	        'lang' => '[a-zA-Z]{2}',
                			'page' => '[1-9][0-9]*' //star is used to show that the second digit can or cannot be entered.
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
        				'controller' => 'Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'user-admin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-user[/:action][/:id][/page/:page][/token/:token]', 
                        'constraints' => array(
                                'lang' => '[a-zA-Z]{2}',
                                'id' => '[1-9][0-9]*',
                                'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
                        		'token' => '[a-f0-9]{32}'
                        ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-user' 
                    ),
                ) 
            ),  
            'user-admin-upload' => array(
                        'type'    => 'Literal',
                        'options' => array(
                                'route'    => '/admin-user/upload', 
                                'defaults' => array(
                                    '__NAMESPACE__' => 'User\Controller',
                                    'controller' => 'Admin',
                                    'action'     => 'upload',
                                ),
                        ),
            ),   
            'user-admin-ajax' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin-user/ajax',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Ajax',
                        'action'     => 'get-categories',
                    ),
                ),
            ),
        ),
    ),  
   
    'view_manager' => array(
        'template_map' => array(
            include_once(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
