<?php

namespace User\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use User\Model\UsersTable;
use User\Form\AdduserForm;
use User\Model\ProvincesTable;
use User\Model\CountryTable;
use User\Model\UserGroupsTable;
use User\Model\StuddiesTable;
use User\Model\CitiesTable;
use User\Model\UsersDataTable;
use Language\Model\LanguageLanguagesTable;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Filter;
use User\Form\UserForm;
use User\Form\OfficeForm;
use User\Model\OfficeTable;
use User\Form\CompanyForm;
use User\Model\CompanyTable;
use User\Model\AreaTable;
use User\Form\DistrictForm;
use User\Model\DistrictTable;
use Application\Model\ServiceTable;
use User\Form\AreaForm;
use Application\Model\ServiceIndexTable;
use Newsletter\Model\NewsletterTable;
use User\Model\DesignerTable;
use Shop\Model\DesignerSampleTable;
class AdminController extends BaseAdminController
{
	protected $noOfPostsPerPage = 100;
	
	public function listUserAction()
	{
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	    
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	     
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $usersTable = new UsersTable($this->getServiceLocator());
	    $allusers = $usersTable->getAllUsers();
	    $grid->setTitle(__('User List'));
	    $grid->setDefaultItemsPerPage(10);
	    $grid->setDataSource($allusers,$dbAdapter);

	    $col = new Column\Select('id',"users");
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);  
	    
	    $col = new Column\Select('username');
	    $col->setLabel(__('Username'));
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('name');
	    $col->setLabel(__('Provience'));
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	           
	    $col = new Column\Select('email');
	    $col->setLabel(__('Email'));
	    $grid->addColumn($col);

	    $col = new Column\Select('mobile');
	    $col->setLabel(__('Mobile'));
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('status');
	    $col->setLabel(__('Status'));
	    $col->setWidth(10);
	    $options = array('0' => 'inactive', '1' => 'active');
	    $col->setFilterSelectOptions($options);
	    $replaces = array('0' => 'inactive', '1' => 'active');
	    $col->setReplaceValues($replaces, $col);
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('user_group_id');
	    $col->setLabel(__('User Group'));
	    $options = array('2' => 'Manager', '3' => 'User', '4' => 'User',  '5' => 'Spesial user');
	    $col->setFilterSelectOptions($options);
	    $replaces = array( '2' => 'Manager', '3' => 'User', '4' => 'User', '5' => 'Spesial user');
	    $col->setReplaceValues($replaces, $col);
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('user_registration_date');
	    $col->setLabel(__('User registration date'));
	    $col->setSortDefault(2, 'DESC');
	    $grid->addColumn($col);
	    
	     
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction = new Column\Action\Icon();
	    $viewAction->setIconClass('glyphicon glyphicon-edit'); 
	    $viewAction->setLink("/". $this->lang .'/admin-user/edit-user/' . $rowId);
	    $viewAction->setTooltipTitle(__('Edit user'));
	    
	    $viewAction1 = new Column\Action\Icon();
	    $viewAction1->setIconClass('glyphicon glyphicon-remove');
	    $viewAction1->setLink("/". $this->lang .'/admin-user/delete-user/'.$rowId.'/token/'.$csrf);
	    $viewAction1->setTooltipTitle(__('Delete User'));
	    
	    /* $viewAction2 = new Column\Action\Icon();
	    $viewAction2->setIconClass('glyphicon glyphicon-eye-open');
	    $viewAction2->setLink("/". $this->lang .'/user/show-profile/'.$rowId);
	    $viewAction2->setTooltipTitle(__('Show user profile info')); */
	    
	    $viewAction3 = new Column\Action\Icon();
	    $viewAction3->setIconClass('glyphicon glyphicon-list');
	    $viewAction3->setLink("/". $this->lang .'/admin-user/list-portfolios/'.$rowId);
	    $viewAction3->setTooltipTitle(__('Show users uploaded file'));

	    $viewAction4 = new Column\Action\Icon();
	    $viewAction4->setIconClass('glyphicon glyphicon-list-alt');
	    $viewAction4->setLink("/". $this->lang .'/admin-user/list-courses/'.$rowId);
	    $viewAction4->setTooltipTitle(__('Show users course'));
	    
	    
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction);
	    $actions2->addAction($viewAction1);
	    //$actions2->addAction($viewAction2);
	    $actions2->addAction($viewAction3);
	    $actions2->addAction($viewAction4);
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	    
	    $link[] = '<a href='."/". $this->lang .'/admin-user/add-user class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a tablename="users"  href="/application-ajax-delete-all"  class="btn btn-danger delete_all"><i class="fa fa-times" aria-hidden="true"></i>' . __("Delete Group Permission") . '</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hid den="true"></i>'.__("Help").'</a>';
	    $grid->setLink($link);
	     
	    $grid->render();
	    
	    return $grid->getResponse();   

	}
	
	private function uploadImage($files, $index = NULL)
	{
	    $config = $this->serviceLocator->get('Config');
	    $uploadDir = $config['base_route'] . "/uploads/avatar/";
	    $validationExt ="jpg,jpeg,png,gif";
	    $postImage = array();
	    foreach ($files as $k=>$file) {
	        $newFileName = md5(time() . $file['name']);
	        $isUploaded = $this->imageUploader()->UploadFile($file, $validationExt, $uploadDir, $newFileName);
	        if ($isUploaded[0])
	            $postImage[] = @$isUploaded[1];
	    }
	
	    if ($index !== NULL && count($postImage))
	        return $postImage[$index];
	    if (count($postImage))
	        return $postImage;
	    return null;
	}

	public function addUserAction()
	{
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    
	    $usersTable = new UsersTable($this->getServiceLocator());
	    $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
	    $provienceTable = new ProvincesTable($this->getServiceLocator());
	    $countryTable = new CountryTable($this->getServiceLocator());
	    $studiesTable = new StuddiesTable($this->getServiceLocator());
	    
	    $provience = $provienceTable->getRecords();
	    $usergroups = $userGroupsTable->getRecords();
	    $countries = $countryTable->getRecords();
	    
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
	    
	    $Form = new AdduserForm($countries,$provience,$usergroups,$allActiveLanguages);
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
            $studies = array();
    
            for ($i=1; $i<=5; $i++){
                $studies[$i-1]['univercity'] = $postData['university'.$i];
                $studies[$i-1]['grade'] = $postData['grade'.$i];
                $studies[$i-1]['field_of_study'] = $postData['field_of_study'.$i];
                $studies[$i-1]['lang'] = $postData['lang'];
            }
            
	        $data = $postData->toArray();
	        $files =  $request->getFiles()->toArray();
	        $data = array_merge($data, $files);
	        
	        if ($Form->setData($postData)->isValid()) {
	            $postData = $Form->getData();
	            
	            $postData['user_creator'] = $this->userData->id;
	            try {
	                if($postData['gender'] == "m"){
	                    $avatar = "male.png";
	                }else{
	                    $avatar = "female.png";
	                }
	                (empty($files['avatar']['name']))? $postData['avatar'] = $avatar : $postData['avatar'] = $this->uploadImage($files,0);
	                $newUserId = $usersTable->addUser($postData);
	                if ($newUserId) {
	                    $usersDataTable = new UsersDataTable($this->getServiceLocator());
	                    $insertUserData = $usersDataTable->addUserData($postData, $newUserId);
	                    $insertStudies = $studiesTable->insert($studies, $newUserId);
	                    
	                    if (isset($_POST['submit'])) {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('user-admin');
	                    } else {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('user-admin', array(
	                            'action' => 'add-user'
	                        ));
	                    }
	                } else {
	                    $this->layout()->errorMessage = __("The Email was registered. Please choose another one!");
	                }
	            } catch (\Exception $e) {
	                $this->layout()->errorMessage = __("The title is taken, please choose another one.");
	            }
	        }
	    }
	    $view['Form'] = $Form;
	    return new ViewModel($view);
	}

	public function editUserAction()
	{
	    $userId = $this->params('id');
	    
	    if ($userId !== $this->userData->id && !in_array($this->userData->user_group_id, array(1, 2))) {
	        return $this->redirect()->toRoute('home');
	    }
	    if ($userId == 1 && $this->userData->user_group_id != 1) {
	        return $this->redirect()->toRoute('user-admin');
	    } 
	    
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $usersTable = new UsersTable($this->getServiceLocator());
	    $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
	    $provienceTable = new ProvincesTable($this->getServiceLocator());
	    $countryTable = new CountryTable($this->getServiceLocator());
	    $studiesTable = new StuddiesTable($this->getServiceLocator());
	    $userDataTable = new UsersDataTable($this->getServiceLocator());
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
        
	    $provience = $provienceTable->getRecords();
	    $usergroups = $userGroupsTable->getRecords();
	    $countries = $countryTable->getRecords();
	   

	    $Form = new AdduserForm($countries,$provience,$usergroups, $allActiveLanguages);
	    
	    
	    if(isset($_GET['lang'])){
	        $userLang = $_GET['lang'];
	    } else{
	        $userLang = $this->lang;
	    }
	    $userMainData = $usersTable->getRecordById($userId);
	    
	    $userInfo = $userDataTable->getRecordById($userId, $userLang);
	    
	    $userStudies = $studiesTable->getRecordById($userId, $userLang);
	    
	    if(!empty($userInfo[0])) {
	       $userData[0] = array_merge($userMainData[0], $userInfo[0]);
	    } else {
	        $userMainData[0]["experience"] = "";
	        $userMainData[0]["research"] = "";
	        $userMainData[0]["aboutme"] = "";
	        $userData[0] = $userMainData[0];
	        $userData[0]['lang'] = $userLang;
	    }
	    $i = 1;
	    foreach ($userStudies as $study){
	        $userData[0]['university'.$i] = $study['university'];
	        $userData[0]['grade'.$i] = $study['grade'];
	        $userData[0]['field_of_study'.$i] = $study['field_of_study'];
	        $i ++;
	    }
	    $userData[0]["password"] = '';
	    $Form->populateValues($userData[0]);
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $data = $postData->toArray();
	        $files =  $request->getFiles()->toArray();
	        $data = array_merge($data, $files);
	        // For normal users to bypass validation
	        if (empty($postData["user_group_id"]) && empty($postData["status"])) {
	            $postData["user_group_id"] = $this->userData->user_group_id;
	            $postData["status"] = 1;
	        }
	        // end
	        if ($Form->setData($postData)->isValid()) {
	            $editData = $Form->getData(); 
	            $editData['user_creator'] = $this->userData->id;
	            
	            if (isset($files['avatar']) && empty($files['avatar']['name'])) {
	                $editData['avatar'] = $userData[0]['avatar'];
	            }
	            else {
	                $editData['avatar'] = $this->uploadImage($files,0);
	               
	            }
	            $studies = array();
	            for ($i=1; $i<=5; $i++){
	                if ( $editData['university'.$i] != '') {
	                    $studies[$i-1]['univercity'] = $editData['university'.$i];
	                    $studies[$i-1]['grade'] = $editData['grade'.$i];
	                    $studies[$i-1]['field_of_study'] = $editData['field_of_study'.$i];
	                    $studies[$i-1]['lang'] =$postData['lang'];
	                } 
	            }
                $userDataTable = new UsersDataTable($this->getServiceLocator());
                $isUserDataExist = $userDataTable->getRecordById($userId, $userLang);
                
	            try {
	                $isedited = $usersTable->editUser($editData, $userId, $this->userData->user_group_id);
	                if(!empty($isUserDataExist)){
	                   $editData = $userDataTable->editUser($editData, $userId,$editData['lang']);
	                }else{
	                    $editData = $userDataTable->addUserData($editData, $userId);
	                }
	                if ($isedited !== false && $editData !== false) {
	                    $studiesTable->deleteStudy($userId, $userLang);
                        $editStudies = $studiesTable->editStudy($studies, $userId, $userLang);
	                    $this->layout()->successMessage = __("Operation done successfully.");
	                    // I comment it because normal users haven't access to user list,Therefore they see permission denied message.
	                   // return $this->redirect()->toRoute('user-admin');
	                } else {
	                    $this->layout()->errorMessage = __("Operation failed!");
	                }
	            } catch (\Exception $e) {
	                $this->layout()->errorMessage = __("The title is taken, please choose another one.");
	            }
	        }
	    }
	    $view['userDataId'] = $userId;
	    $view['post'] = $userData;
	    $view['Form'] = $Form;
	    $view['user_id'] = $userId;
	    return new ViewModel($view);
	}


	public function deleteUserAction()
	{
		$config = $this->serviceLocator->get('Config');
		$uploadDir = $config['base_route'];
		
		$container = new Container('token');
		$token = $this->params('token');
		$userId = $this->params('id');
		
		if ($userId == 1 && $this->userData->user_group_id != 1) {
		    return $this->redirect()->toRoute('user-admin');
		}
		
	    $usersTable = new UsersTable($this->getServiceLocator());
	    $userById = $usersTable->getRecordById($userId);
		if ($userById){
		    @unlink($uploadDir . "/uploads/avatar/" . $userById[0]['avatar']);
		    if ($userId && $container->offsetExists('csrf')) {
		        if ($container->offsetGet('csrf') == $token) {

		            $isDeleted = $usersTable->deleteUser($userId);
		            if ($isDeleted) {
		                $usersDataTable = new UsersDataTable($this->getServiceLocator());
		                $usersDataTable->deleteUserData($userId);
		            
		                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
		            } else {
		                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
		            }
		        }
		    }
		}
		return $this->redirect()->toRoute('user-admin');
	}
	public function getCitiesAction()
	{
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	
	    $citiesTable = new CitiesTable($this->getServiceLocator());
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost();
	        $citiesByProviceId = $citiesTable->getRecords($data['id']);
	        die(json_encode($citiesByProviceId));
	    }
	}
	public function getProvinceAction()
	{
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	
	    $ProvienceTable = new ProvincesTable($this->getServiceLocator());
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost();
	        $citiesByProviceId = $ProvienceTable->getRecordsByCountry($data['id']);
	        die(json_encode($citiesByProviceId));
	    }
	}
	
	public function usersAction(){
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $usersTable = new UsersTable($this->getServiceLocator());
	        $user = $usersTable->getRecordByGroup($postData['group_id']);
	    }
	}
	
	public function getUsersAction(){
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $usersTable = new UsersTable($this->getServiceLocator());
	        $user = $usersTable->getRecordByGroup($postData['group_id']);
	        die(json_encode($user));
	    }
	}
	
	public function editAction(){
	    $id = $this->params('id');
	    $usersTable = new UsersTable($this->getServiceLocator());
	    $getUser = $usersTable->getRecordById($id);
	    $userForm = new UserForm();
	    $userForm->populateValues($getUser);
	    $view['userForm'] = $userForm;
	    $view['group'] = $getUser['user_group_id'];
	    
	    if($getUser['user_group_id'] == 7){
	        $officeTable = new OfficeTable($this->getServiceLocator());
	        $userOfficeData = $officeTable->getRecordById($id);
	        $serviceTable = new ServiceTable($this->getServiceLocator());
	        $services = $serviceTable->getRecords();
	        $serviceIndexTable = new ServiceIndexTable($this->getServiceLocator());
	        $getServices = $serviceIndexTable->getRecordOffice($userOfficeData['id']);
	        $userOfficeData['service_id'] = explode(',', $getServices['service_id']);
	        $officeForm = new OfficeForm($services);
	        $officeForm->populateValues($userOfficeData);
	        $view['officeForm'] = $officeForm;
	    }elseif ($getUser['user_group_id'] == 8){
	        $companyTable = new CompanyTable($this->getServiceLocator());
	        $userCompanyData = $companyTable->getRecordById($id);
	        $companyForm = new CompanyForm();
	        $companyForm->populateValues($userCompanyData);
	        $view['companyForm'] = $companyForm;
	    }
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $usersTable = new UsersTable($this->getServiceLocator());
	        $updateUser = $usersTable->edit($postData,$id);
	        if($updateUser){ 
	            if($getUser['user_group_id'] == 7){
	                $officeTable = new OfficeTable($this->getServiceLocator());
	                $officeData['office_name'] = $postData['office_name'];
	                $officeData['manager_name'] = $postData['manager_name'];
	                $officeData['fax'] = $postData['fax'];
	                $officeData['online_services'] = $postData['online_services'];
	                $officeData['usual_courier'] = $postData['usual_courier'];
	                $officeData['special_courier'] = $postData['special_courier'];
	                $updateOffice = $officeTable->edit($officeData,$id);
	                if($updateOffice){
	                    $serviceTable = new ServiceIndexTable($this->getServiceLocator());
	                    $newServices = array_diff($postData['service_id'],$userOfficeData['service_id']);
	                    $deleteServices = array_diff($userOfficeData['service_id'],$postData['service_id']);
	                    foreach ($deleteServices as $delService){
	                        $delService = $serviceTable->delete($userOfficeData['id'],$delService);
	                    }
	                    foreach ($newServices as $service){
	                        $serviceData['office_id'] = $userOfficeData['id'];
	                        $serviceData['service_id'] = $service;
	                        $addService = $serviceTable->add($serviceData);
	                    }
	                }
	            }elseif($getUser['user_group_id'] == 8){
	                $companyTable = new CompanyTable($this->getServiceLocator());
	                $companyData['company_name'] = $postData['company_name'];
	                $companyData['company_history'] = $postData['company_history'];
	                $companyData['manager_name'] = $postData['manager_name'];
	                $companyData['fax'] = $postData['fax'];
	                $updateCompany = $companyTable->edit($companyData,$id);
	            }
	            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	            return $this->redirect()->toRoute('user-admin', array(
	                'action' => 'users'
	            ));
	        }
	    }
	    return new ViewModel($view);
	}
// 	public function editPublicUserAction(){
	    
// 	}
// 	public function editSellerUserAction(){
	    
// 	}
	
// 	public function editTechnicalOfficeUserAction(){
	    
// 	}
    
	public function addAction(){
	    $group = $this->params('id');
	    $serviceTable = new ServiceTable($this->getServiceLocator());
	    $services = $serviceTable->getRecords();
	    $view['services'] = $services;
	    $userForm = new UserForm();
	    $view['userForm'] = $userForm;
	    $view['group'] = $group;
// 	    var_dump($services);die;
	    $officeForm = new OfficeForm($services);
// 	    var_dump($officeForm);die;
// 	    $officeForm->populateValues($userOfficeData);
	    $view['officeForm'] = $officeForm;

	    return new ViewModel($view);
	    
	}
	public function deleteAction(){
	    
	}
    
	public function addUserRegisterAction()
    {
        $this->setHeadTitle(__('User register'));
        $view = array();
        $serviceTable = new ServiceTable($this->getServiceLocator());
        $services = $serviceTable->getRecords();
        $view['services'] = $services;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $usersTable = new UsersTable($this->getServiceLocator());
            $email = $postData['email'];
            $existEmail = $usersTable->getUserByEmail($email);
            if ($existEmail) {
                $this->layout()->errorMessage = __("User with this email already registered.");
            } else {
                $postData['user_group_id'] = '7';
                $isAdded = $usersTable->addUser($postData);
                if ($isAdded) {
                    $officeTable = new OfficeTable($this->getServiceLocator());
                    $officeData['user_id'] = $isAdded;
                    $officeData['office_name'] = $postData['office_name'];
                    $officeData['manager_name'] = $postData['manager_name'];
                    $officeData['fax'] = $postData['fax'];
                    $officeData['online_services'] = $postData['online_services'];
                    $officeData['usual_courier'] = $postData['usual_courier'];
                    $officeData['special_courier'] = $postData['special_courier'];
                    $officeData['in_place'] = $postData['in_place'];
                    $addOffice = $officeTable->add($officeData);
                    if ($addOffice) {
                        $serviceTable = new ServiceIndexTable($this->getServiceLocator());
                        foreach ($postData['services'] as $service) {
                            $serviceData['office_id'] = $addOffice;
                            $serviceData['service_id'] = $service;
                            $addService = $serviceTable->add($serviceData);
                        }
                    }

                    $this->flashMessenger()->addSuccessMessage("ثبت نام شما  با موفقیت انجام شد.");
                    return $this->redirect()->toRoute('/admin-user/users');
                } else {
                    $this->layout()->errorMessage = __("Registeration failed!");
                }
            }
            
            // var_dump($postData);die;
        }
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }
	public function addUserRegisterSellerAction()
    {
        $this->setHeadTitle(__('User register seller'));
        $view = array();
        $serviceTable = new ServiceTable($this->getServiceLocator());
        $services = $serviceTable->getRecords();
        $view['services'] = $services;
        // $officeForm = new OfficeForm($services);
        // $view['officeForm'] = $officeForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
//             var_dump($postData);
//             die();
            $usersTable = new UsersTable($this->getServiceLocator());
            $email = $postData['email'];
            $existEmail = $usersTable->getUserByEmail($email);
            if ($existEmail) {
                $this->layout()->errorMessage = __("User with this email already registered.");
            } else {
                $postData['user_group_id'] = '8';
                $isAdded = $usersTable->addUser($postData);
                if ($isAdded) {
                    $companyTable = new CompanyTable($this->getServiceLocator());
                    $companyData['user_id'] = $isAdded;
                    $companyData['company_name'] = $postData['company_name'];
                    $companyData['company_history'] = $postData['company_history'];
                    $companyData['manager_name'] = $postData['manager_name'];
                    $companyData['fax'] = $postData['fax'];
                    $addCompany = $companyTable->add($companyData);
                    if ($addCompany) {
                        if ($postData['newsletter']) {
                            $newsLetterTable = new NewsletterTable($this->getServiceLocator());
                            $addNewsletter = $newsLetterTable->addNewsletter($postData['email']);
                        }
                        // addNewsletter
                        // $serviceTable = new ServiceIndexTable($this->getServiceLocator());
                        // foreach ($postData['services'] as $service){
                        // $serviceData['office_id'] = $addCompany;
                        // $serviceData['service_id'] = $service;
                        // $addService = $serviceTable->add($serviceData);
                        // var_dump($addService);
                        // }
                        // die;
                    }
                    $this->flashMessenger()->addSuccessMessage("ثبت نام شما  با موفقیت انجام شد.");
                    return $this->redirect()->toRoute('/admin-user/users');
                } else {
                    $this->layout()->errorMessage = __("Registeration failed!");
                }
            }
        }
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }
    public function addUserRegisterPublicAction()
    {
        $this->setHeadTitle(__('Public user register'));
        $view = array();
        $serviceTable = new ServiceTable($this->getServiceLocator());
        // $services = $serviceTable->getRecords();
        // $view['services'] = $services;
        // $officeForm = new OfficeForm($services);
        // $view['officeForm'] = $officeForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
//             var_dump($postData);
//             die();
            $usersTable = new UsersTable($this->getServiceLocator());
            $email = $postData['email'];
            $existEmail = $usersTable->getUserByEmail($email);
            if ($existEmail) {
                $this->layout()->errorMessage = __("User with this email already registered.");
            } else {
                $postData['user_group_id'] = '3';
                $isAdded = $usersTable->addNewUser($postData);
                if ($isAdded) {
                    if ($postData['newsletter']) {
                        $newsLetterTable = new NewsletterTable($this->getServiceLocator());
                        $addNewsletter = $newsLetterTable->addNewsletter($postData['email']);
                    }
//                     $authStatus = $this->authenticationService->auth($postData['email'], $postData['password']);
//                     $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
//                     $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                    $this->flashMessenger()->addSuccessMessage("ثبت نام شما  با موفقیت انجام شد.");
                    return $this->redirect()->toRoute('admin-user/users');
                } else {
                    $this->layout()->errorMessage = __("Registeration failed!");
                }
            }
        }
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }
    
	public function addUserRegisterDesignerAction()
    {
        $this->setHeadTitle(__('Public user register'));
        $validationCvExt = "pdf,doc,docx,jpeg,jpg";
        $validationSamplesExt = "zip";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/designer/";
        $view = array();
        $serviceTable = new ServiceTable($this->getServiceLocator());
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $file = $request->getFiles()->toArray();
            $usersTable = new UsersTable($this->getServiceLocator());
            $email = $postData['email'];
            $existEmail = $usersTable->getUserByEmail($email);
            if ($existEmail) {
                $this->layout()->errorMessage = __("User with this email already registered.");
            } else {
                $postData['user_group_id'] = '9';
                $isAdded = $usersTable->addUser($postData);
                if ($isAdded) {
                    if ($file['cv_file']['tmp_name'] != '') {
                        $newFileName = md5(time() . $file['cv_file']['name']);
                        $isUploaded = $this->imageUploader()->UploadFile($file['cv_file'], $validationCvExt, $uploadDir . 'cv', $newFileName);
                        if ($isUploaded[0] == true) {
                            $postData['cv_file'] = $isUploaded[1];
                        }
                    }
                    
                    if ($file['work_samples']['tmp_name'] != '') {
                        $newFileName = md5(time() . $file['work_samples']['name']);
                        $isUploaded = $this->imageUploader()->UploadFile($file['work_samples'], $validationSamplesExt, $uploadDir . 'samples', $newFileName);
                        if ($isUploaded[0] == true) {
                            $postData['work_samples'] = $isUploaded[1];
                        }
                    }
                    $designerTable = new DesignerTable($this->getServiceLocator());
                    $designerData['user_id'] = $isAdded;
                    $designerData['cv_file'] = $postData['cv_file'];
                    $designerData['work_exp'] = $postData['work_exp'];
                    $designerData['work_samples'] = $postData['work_samples'];
                    $designerData['description'] = $postData['description'];
                    $addDesignerData = $designerTable->add($designerData);
                    $this->flashMessenger()->addSuccessMessage("ثبت نام شما  با موفقیت انجام شد.");
                    return $this->redirect()->toRoute('/admin-user/users');
                } else {
                    $this->layout()->errorMessage = __("Registeration failed!");
                }
            }
        }
        $viewModel = new ViewModel($view);
        
        return $viewModel;
    }
	
    public function areaListAction(){
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $areaTable = new AreaTable($this->getServiceLocator());
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $areaList = $areaTable->getRecords();
        $grid->setTitle(__('Area List'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($areaList, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Name'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-user/edit-area/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-user/delete-area/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-plus');
        $viewAction3->setLink("/" . $this->lang . '/admin-user/add-district/' . $rowId);
        $viewAction3->setTooltipTitle(__('Add district'));
        
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->addAction($viewAction3);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-user/add-area class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    public function addAreaAction(){
        $form = new AreaForm();
        $view['form'] = $form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray(); //var_dump($data);die;
            $areaTable = new AreaTable($this->getServiceLocator());
            if(!empty($data['name'])){
                $add = $areaTable->add($data);
                if($add){
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toRoute('user-admin', array(
                        'action' => 'area-list'
                    ));
                }
            }else{
                $this->flashMessenger()->addErrorMessage(__("نام نمی تواند خالی باشد"));
            }
                
        }
        $viewModel = new ViewModel($view);
        return $viewModel;
    }
    public function editAreaAction(){
        $id = $this->params('id');
        $areaTable = new AreaTable($this->getServiceLocator());
        $getArea = $areaTable->getById($id);
        $form = new AreaForm();
        $form->populateValues($getArea);
        $view['form'] = $form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $edit = $areaTable->edit($data,$id);
            if($edit == true){
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('user-admin', array(
                    'action' => 'area-list'
                ));
            }
        }
        $viewModel = new ViewModel($view);
        return $viewModel;
    }
    public function addDistrictAction(){
        $id = $this->params('id');
        $areaTable = new AreaTable($this->getServiceLocator());
        $districtTable = new DistrictTable($this->getServiceLocator());
        $areaList = $areaTable->getRecords();
        $districts = $districtTable->getRecords(); 
        $form = new DistrictForm($areaList,$districts);
        $getDistrict = $districtTable->getByArea($id); 
        $getDistrict['district'] = explode(',', $getDistrict['district']);
        $getDistrict['area_id'] = $id;
//         foreach ($getDistrict as $key => $district){
//             $getDistrict[$key]['district'] = $district['id'];
//         }
//         var_dump($getDistrict);die;
        $form->populateValues($getDistrict);
        $view['form'] = $form;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $deleteList = array_diff($getDistrict['district'], $data['district']);
            $plusList = array_diff($data['district'],$getDistrict['district']);
            foreach ($deleteList as $delete){
                $del = $districtTable->delete($delete,$data['area_id']);    
            }
                
            foreach ($plusList as $plus){
                $addData['area_id'] = $data['area_id'];
                $addData['district'] = $plus;
                $add = $districtTable->add($addData);
            }
            
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toRoute('user-admin', array(
                'action' => 'area-list'
            )); 
        }
        
        $viewModel = new ViewModel($view);
        return $viewModel;
    }
    public function deleteAreaAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $id = $this->params('id');
        if ($id && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $areaTable = new AreaTable($this->getServiceLocator());
                $isDelete = $areaTable->delete($id);
                if ($isDelete) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-user/area-list');
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-user/area-list');
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        return $this->redirect()->toRoute('admin-user', array(
            'action' => 'area-list'
        ));
    }
    
    public function confirmAction(){
        $id = $this->params('id');
        $usersTable = new UsersTable($this->getServiceLocator());
        $user = $usersTable->update($id);
        if($user == true){
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toRoute('user-admin', array(
                'action' => 'users'
            ));
        }
    }
	
	    
    public function viewAction(){
        $userId = $this->params('id');
        $usersTable = new UsersTable($this->getServiceLocator());
        $designerTable = new DesignerTable($this->getServiceLocator());
        $userMainData = $usersTable->getRecordById($userId);
        $designerData = $designerTable->getRecordById($userId);
        $view['userMainData'] = $userMainData;
        $view['designerData'] = $designerData;
        $viewModel = new ViewModel($view);
        return $viewModel;
    }
    
    public function samplesAction(){
//         $userId = $this->params('id');
//         $usersTable = new UsersTable($this->getServiceLocator());
//         $designerTable = new DesignerSampleTable($this->getServiceLocator());
// //         $userMainData = $usersTable->getRecordById($userId);
//         $designerData = $designerTable->fetchByUser($userId);
// //         $view['userMainData'] = $userMainData;
//         $view['designerData'] = $designerData;
//         $viewModel = new ViewModel($view);
//         return $viewModel;
        
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        $id = $this->params('id');
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $designerTable = new DesignerSampleTable($this->getServiceLocator());
        $designerData = $designerTable->fetchByUser($id);
//         var_dump($designerData);die;
        $grid->setTitle(__('Designer Sample'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($designerData, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('price');
        $col->setLabel(__('Price'));
        $grid->addColumn($col);
        
        $col = new Column\Select('admin_percent');
        $col->setLabel(__('Admin Percent'));
        $grid->addColumn($col);
        
        
        $col = new Column\Select('confirm');
        $col->setLabel(__('Confirm'));
        $grid->addColumn($col);
        
        //         $col = new Column\Select('delivery_price');
        //         $col->setLabel(__('Delivery Price'));
        //         $grid->addColumn($col);
        
        //         $col = new Column\Select('created');
        //         $col->setLabel(__('Order Created'));
        //         $grid->addColumn($col);
        
        //         $col = new Column\Select('status');
        //         $col->setLabel(__('Status'));
        //         $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-edit');
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/edit-samples/' . $rowId);
        $viewAction2->setTitle(__('change status'));
        
        //         $viewAction1 = new Column\Action\Icon();
        //         $viewAction1->setIconClass('fa fa-file-o');
        //         $viewAction1->setLink("/" . $this->lang . '/admin-ourservices/view-order/id/' . $rowId);
        //         $viewAction1->setTitle(__('view detail'));
        
                $actions2 = new Column\Action();
                $actions2->setLabel(__('Operations'));
                $actions2->addAction($viewAction2);
//                 $actions2->addAction($viewAction1);
                $actions2->setWidth(15);
                $grid->addColumn($actions2);
        
//                 $link[] = '<a href=' . "/" . $this->lang . '/admin-ourservices/add-attributes class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        //         $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
//                 $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    

}
