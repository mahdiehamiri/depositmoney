<?php

namespace User\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Blog\Model\BlogPostsTable;
use Blog\Model\BlogCategoriesTable;
use Application\Helper\BaseController;
use Application\Model\ApplicationTagsIndexTable;
use Zend\Db\Adapter\Adapter;
use User\Model\UsersTable;
use User\Model\StuddiesTable;
use User\Model\ProvincesTable;
use User\Model\CountryTable;
use User\Model\CitiesTable;
use User\Model\UserGroupsTable;

class IndexController extends BaseController
{
	protected $noOfPostsPerPage = 20;
	protected $latestBlogPostsLimit = 4;
	
	public function onDispatch(MvcEvent $e)
	{

		parent::onDispatch($e);
	}
	
    public function indexAction()
    {
        $this->setHeadTitle(__('User'));
    	$config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $usersTable = new UsersTable($this->getServiceLocator());

    	$paginator = $usersTable->getAllUsersData($this->lang, true);
    	$page = $this->params('page');
    	if ($page == 1) {
    		return $this->redirect()->toRoute('user-home');
    	}
    	$paginator->setCurrentPageNumber($this->params('page', 1));
    	$paginator->setItemCountPerPage($this->noOfPostsPerPage);
    	//$view['postList'] = $paginator; 
        $view['users'] = $paginator;
        return new ViewModel($view);
    }
    
    public function showProfileAction()
    {
 
        $userId = $this->params('id');
         
        $usersTable = new UsersTable($this->getServiceLocator());
        $studiesTable = new StuddiesTable($this->getServiceLocator());
        $lang = $this->lang;
        
        $userData = $usersTable->getRecordByIdForView($userId, $lang);
         
        if (!$userData) {
            return $this->redirect()->toRoute('user-home');
        }
        $provienceTable = new ProvincesTable($this->getServiceLocator());
        $countryTable = new CountryTable($this->getServiceLocator());
        $cityTable = new CitiesTable($this->getServiceLocator());
        $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
         
        $userData[0]['user_group_id'] = $userGroupsTable->getRecordById($userData[0]['user_group_id']);
        $userData[0]['user_group_id'] = $userData[0]['user_group_id'][0]['title'];
         
        $userData[0]['country'] = $countryTable->getRecords($userData[0]['country']);
        $userData[0]['country'] = $userData[0]['country'][0]['country_name'];
         
        $userData[0]['provience'] = $provienceTable->getRecords($userData[0]['provience']);
        $userData[0]['provience'] = $userData[0]['provience'][0]['name'];
         
        $userData[0]['city'] = $cityTable->getRecordsById($userData[0]['city']);
        $userData[0]['city'] = $userData[0]['city'][0]['name'];
         
        $userStudies = $studiesTable->getRecordById($userId,$lang);
        $userData[0]['studies'] = $userStudies;
      
        $view['user_data'] = $userData[0];
//          var_dump($userData[0]);
        return new ViewModel($view);
    }
   
}
