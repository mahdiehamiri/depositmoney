<?php 

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class AreaForm extends Form
{
    public function __construct()
	{
		parent::__construct('areaForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$name = new Element\Text('name');
		$name->setAttributes(array(
		    'id' => 'name',
		    'class' => 'form-control validate[required]',
		    'placeholder' => __('Area name'),
		    'required' => 'required'
		));
		$name->setLabel(__("Area name"));
		
		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("submit"));
		$submit->setAttributes(array(
				'id'    => 'update-profile',
				'class' => 'btn btn-primary'
		));

		$this->add($name)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		
		$inputFilter->add($factory->createInput(array(
				'name'     => 'gender',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}