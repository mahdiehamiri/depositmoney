<?php 

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class CompanyForm extends Form
{
	public function __construct()
	{
		parent::__construct('companyForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$companyName = new Element\Text('company_name');
		$companyName->setAttributes(array(
				'id'    => 'company-name',
				'class' => 'form-control',
		        'placeholder' => __('Company Name'),
		));
		$companyName->setLabel(__("Company Name"));
		
		$companyHistory = new Element\Text('company_history');
		$companyHistory->setAttributes(array(
		    'id'    => 'company-history',
		    'class' => 'form-control',
		    'placeholder' => __('Company History'),
		));
		$companyHistory->setLabel(__("Company History"));
        
		$managerName = new Element\Text('manager_name');
		$managerName->setAttributes(array(
		    'id'    => 'manager-name',
		    'class' => 'form-control',
		    'placeholder' => __('Manager Name'),
		));
		$managerName->setLabel(__("Manager Name"));
		
		$fax = new Element\Text('fax');
		$fax->setAttributes(array(
		    'id'    => 'fax',
		    'class' => 'form-control',
		    'placeholder' => __('Fax'),
		));
		$fax->setLabel(__("Fax"));
		
		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Edit profile"));
		$submit->setAttributes(array(
				'id'    => 'update-profile',
				'class' => 'btn btn-primary'
		));

		$this->add($companyName)
		     ->add($companyHistory)
		     ->add($managerName)
			 ->add($fax)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'firstname',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'lastname',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'gender',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}