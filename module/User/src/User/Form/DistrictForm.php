<?php 

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class DistrictForm extends Form
{
    public function __construct($areaList,$districtList)
	{
		parent::__construct('districtForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

// 		$district = new Element\Select('district');
// 		$district->setAttributes(array(
// 			'id'    => 'district',
// 			'class' => 'form-control',
// 	        'placeholder' => __('District Name'),
// 		));
// 		$district->setLabel(__("District Name"));
		
		$district = new Element\Select('district');
		$district->setAttributes(array(
		    'id' => 'district',
		    'class' => 'form-control district',
		    'multiple' => true
		));
		$districts = array();
		if ($districtList) {
		    foreach ($districtList as $value) {
		        $districts[$value['name']] = $value['name'];
		    }
		}
		$district->setValueOptions($districts);
		$district->setLabel(_("Choose a district"));
		
		
		$areaId = new Element\Select('area_id');
		$areaId->setAttributes(array(
		    'id' => 'area_id',
		    'class' => 'form-control area_id'
		));
		$areas = array('' => 'انتخاب یا اضافه نمایید.');
		if ($areaList) {
		    foreach ($areaList as $area) {
		        $areas[$area['id']] = $area['name'];
		    }
		}
		$areaId->setValueOptions($areas);
		$areaId->setLabel(_("Choose a area"));
		
		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("submit"));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));

		$this->add($district)
		     ->add($areaId)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'firstname',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'lastname',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'gender',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}