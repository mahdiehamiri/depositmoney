<?php 

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class OfficeForm extends Form
{
	public function __construct($serviceList= null)
	{
		parent::__construct('officeForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$officeName = new Element\Text('office_name');
		$officeName->setAttributes(array(
				'id'    => 'office-name',
				'class' => 'form-control',
		        'placeholder' => __('Office Name'),
		));
		$officeName->setLabel(__("Office Name"));
        
		$managerName = new Element\Text('manager_name');
		$managerName->setAttributes(array(
		    'id'    => 'manager-name',
		    'class' => 'form-control',
		    'placeholder' => __('Manager Name'),
		));
		$managerName->setLabel(__("Manager Name"));
		
		$fax = new Element\Text('fax');
		$fax->setAttributes(array(
		    'id'    => 'fax',
		    'class' => 'form-control',
		    'placeholder' => __('Fax'),
		));
		$fax->setLabel(__("Fax"));
		
		$fax = new Element\Text('fax');
		$fax->setAttributes(array(
		    'id'    => 'fax',
		    'class' => 'form-control',
		    'placeholder' => __('Fax'),
		));
		$fax->setLabel(__("Fax"));
		
// 		$service = new Element\Checkbox('service_id');
// 		$service->setAttributes(array(
// 		    'id'    => 'service-id',
// 		    'class' => 'form-control',
// 		    'placeholder' => __('Service'),
// 		));
// 		$service->setLabel(__("Service"));
		
		
// 		$serviceId = new Element\Checkbox('service_id');
// 		$serviceId->setAttributes(array(
// 		    'id' => 'service-id',
// // 		    'class' => 'form-control service-id',
// 		    'name' => 'service_id[]'
		    
// 		));
// 		$services = array(
// 		    "" => _("Please choose an item")
// 		);
// 		if ($serviceList) {
// 		    foreach ($serviceList as $service) {
// 		        $services[$service['id']] = $service['title'];
// 		    }
// 		}
// 		$serviceId->setOptions($services);
// 		$serviceId->setLabel(_("Service"));

		$serviceId = new Element\MultiCheckbox('service_id');
		$serviceId->setAttributes(array(
		    'id' => 'service-id',
		    'name' => 'service_id'
		    
		));
		$services = [];
		if ($serviceList) {
		    foreach ($serviceList as $service) {
		        $services[$service['id']] = $service['title'];
		    }
		}
		$serviceId->setValueOptions($services);
		
		$onlineServices = new Element\Checkbox('online_services');
		$onlineServices->setAttributes(array(
		    'id'    => 'online-service',
// 		    'class' => 'form-control',
// 		    'placeholder' => __('Online Service'),
		));
		$onlineServices->setLabel(__("Online Service"));
		
		$uCourier = new Element\Checkbox('usual_courier');
		$uCourier->setAttributes(array(
		    'id'    => 'usual_courier',
// 		    'class' => 'form-control',
// 		    'placeholder' => __('Usual Courier'),
		));
		$uCourier->setLabel(__("Usual Courier"));
		
		$sCourier = new Element\Checkbox('special_courier');
		$sCourier->setAttributes(array(
		    'id'    => 'usual_courier',
// 		    'class' => 'form-control',
// 		    'placeholder' => __('Special Courier'),
		));
		$sCourier->setLabel(__("Special Courier"));
		
		$csrf = new Element\Csrf('csrf_security');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Edit profile"));
		$submit->setAttributes(array(
				'id'    => 'update-profile',
				'class' => 'btn btn-primary'
		));

		$this->add($officeName)
			 ->add($managerName)
			 ->add($fax)
			 ->add($serviceId)
			 ->add($onlineServices)
			 ->add($uCourier)
			 ->add($sCourier)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'firstname',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'lastname',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'gender',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}