<?php
namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class UserForm extends Form
{

    public function __construct()
    {
        parent::__construct('postForm');
        $this->setAttributes(array(
            'action' => '',
            'id' => 'userForm',
            'method' => 'post',
            'novalidate' => true
        ));
       
        $username = new Element\Text('username');
        $username->setAttributes(array(
            'id' => 'username',
            'class' => 'form-control ',
            'required' => 'required'
        ));
        $username->setLabel(__("Username"));
        
        $name = new Element\Text('firstname');
        $name->setAttributes(array(
            'id' => 'firstname',
            'class' => 'form-control ',
            'required' => 'required'
        ));
        $name->setLabel(__("Firstname"));
        
        $email = new Element\Text('email');
        $email->setAttributes(array(
            'id' => 'email',
            'class' => 'form-control validate[required, custom[email]',
            'required' => 'required',
            'placeholder' => 'example.domain.com'
        
        ));
        $email->setLabel(__("Email"));
       
        $phone = new Element\Text('phone');
        $phone->setAttributes(array(
            'class' => 'form-control ',
            'id' => 'phone',
            'placeholder' => '02188888888 , 88888888'
            
        ));
        $phone->setLabel(__("Phone"));
        
        $mobile = new Element\Text('mobile');
        $mobile->setAttributes(array(
            'id' => 'mobile',
            'class' => 'form-control validate[required, custom[onlyNumberSp],maxSize[11], minSize[11]]',
            'required' => 'required',
            'placeholder' => '09121234567'
        
        ));
        $mobile->setLabel(__("Mobile"));
        
        $area = new Element\Text('area');
        $area->setAttributes(array(
            'class' => 'form-control',
            'id' => 'area'
        ));
        $area->setLabel(__("Area"));
        
        $district = new Element\Text('district');
        $district->setAttributes(array(
            'class' => 'form-control',
            'id' => 'district'
        ));
        $district->setLabel(__("District"));
        
        $address = new Element\Text('address');
        $address->setAttributes(array(
            'id' => 'address',
            'class' => 'form-control validate[custom[onlyLetterNumber]]'
        ));
        $address->setLabel(__("Address"));
        
        $job = new Element\Text('job');
        $job->setAttributes(array(
            'id' => 'job',
            'class' => 'form-control'
        ));
        $job->setLabel(__("Job"));
        
        $newsletter = new Element\Checkbox('newsletter');
        $newsletter->setAttributes(array(
            'id' => 'newsletter checkboxID2',
            'class' => 'form-control'
        ));
        $newsletter->setLabel(__("NewsLetter"));
        
        $description = new Element\Textarea('description');
        $description->setAttributes(array(
            'id' => 'description',
            'class' => 'form-control'
        ));
        $description->setLabel(__("Description"));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__("ثبت اطلاعات"));
        $submit->setAttributes(array(
            'id' => 'submit',
            'class' => 'btn btn-primary'
        ));
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__("Save and New"));
        $submit2->setAttributes(array(
            'class' => 'btn btn-primary',
            'id' => 'submit'
        ));
        
        $this->add($username)
            ->add($email)
            ->add($name)
            ->add($phone)
            ->add($mobile)
            ->add($area)
            ->add($district)
            ->add($address)
            ->add($job)
            ->add($newsletter)
            ->add($description)
            ->add($csrf)
            ->add($submit2)
            ->add($submit);
        $this->inputFilters();
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'avatar',
            'required' => false
        
        )));
        // }
        /*
         * $inputFilter->add($factory->createInput(
         * array(
         * 'name' => 'post_meta_description',
         * 'required' => false,
         * 'filters' => array(
         * array(
         * 'name' => 'StripTags'
         * )
         * ),
         * )
         * ));
         * $inputFilter->add($factory->createInput(
         * array(
         * 'name' => 'post_meta_keywords',
         * 'required' => false,
         * 'filters' => array(
         * array(
         * 'name' => 'StripTags'
         * )
         * ),
         * )
         * ));
         * $inputFilter->add($factory->createInput(
         * array(
         * 'name' => 'post_source_url',
         * 'required' => false,
         * 'filters' => array(
         * array(
         * 'name' => 'StripTags'
         * )
         * ),
         * )
         * ));
         * $inputFilter->add($factory->createInput(
         * array(
         * 'name' => 'post_source_title',
         * 'required' => false,
         * 'filters' => array(
         * array(
         * 'name' => 'StripTags'
         * )
         * ),
         * )
         * ));
         */
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
