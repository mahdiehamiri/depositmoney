<?php
namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class AdduserForm extends Form
{

    public function __construct($countries, $provinces, $usergroups, $allActiveLanguages)
    {
        parent::__construct('postForm');
        $this->setAttributes(array(
            'action' => '',
            'id' => 'userForm',
            'method' => 'post',
            'novalidate' => true
        ));
        
        $pageLang = new Element\Select('lang');
        $pageLang->setAttributes(array(
            'id' => 'lang',
            'class' => 'form-control'
        ));
        $languages = array();
        if ($allActiveLanguages) {
            foreach ($allActiveLanguages as $language) {
                $languages[$language['code']] = $language['title'];
            }
        }
        $pageLang->setValueOptions($languages);
        $pageLang->setLabel(__("Choose language"));
        
        $firstname = new Element\Text('firstname');
        $firstname->setAttributes(array(
            'id' => 'firstname',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $firstname->setLabel(__("Firstname"));
        
        $lastname = new Element\Text('lastname');
        $lastname->setAttributes(array(
            'id' => 'Lastname',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $lastname->setLabel(__("Lastname"));
        
        $username = new Element\Text('username');
        $username->setAttributes(array(
            'id' => 'email',
            'class' => 'form-control validate[required]'
        
        ));
        $username->setLabel(__("Full name"));
        
        $email = new Element\Text('email');
        $email->setAttributes(array(
            'id' => 'email',
            'class' => 'form-control validate[required, custom[email]',
            'required' => 'required',
            'placeholder' => 'example.domain.com'
        
        ));
        $email->setLabel(__("Email"));
        
        $date_of_birth = new Element\Text('date_of_birth');
        $date_of_birth->setAttributes(array(
            'id' => 'date_of_birth',
            'class' => 'form-control validate[custom[date]]'
        ));
        $date_of_birth->setLabel(__("Date of Birth"));
        
        $image = new Element\File('avatar');
        $image->setAttributes(array(
            'id' => 'fileUpload1'
        ));
        $image->setLabel(__("Avatar"));
        
        $password = new Element\Text('password');
        $password->setAttributes(array(
            'id' => 'password',
            'class' => 'form-control'
        
        ));
        $password->setLabel(__("Password"));
        
        $gender = new Element\Select('gender');
        $gender->setLabel(__("Gender"));
        $gender->setAttributes(array(
            'id' => 'gender',
            'class' => 'validate[required]',
            'required' => 'required'
        ));
        $tags = array(
            'm' => __('Male'),
            'f' => __('Female')
        );
        $gender->setValueOptions($tags);
        
        $user_group = new Element\Select('user_group_id');
        $user_group->setLabel(__("User group"));
        $user_group->setAttributes(array(
            'id' => 'user_group_id',
            'class' => 'form-control ',
            'required' => 'required'
        
        ));
        $groupList = array(
            '' => __('Please choose')
        );
        
        if ($usergroups) {
            foreach ($usergroups as $item) {
                $groupList[$item['id']] = $item['title'];
            }
        }
        $user_group->setValueOptions($groupList);
        
        $priority = new Element\Text('priority');
        $priority->setAttributes(array(
            'id' => 'shop-product-priority',
            'class' => 'form-control validate[custom[onlyNumberSp]]'
        ));
        $priority->setLabel(__('Priority'));
        
        $mobile = new Element\Text('mobile');
        $mobile->setAttributes(array(
            'id' => 'mobile',
            'class' => 'form-control validate[required, custom[onlyNumberSp],maxSize[11], minSize[11]]',
            'required' => 'required',
            'placeholder' => '09121234567'
        
        ));
        $mobile->setLabel(__("Mobile"));
        
        $country = new Element\Select('country');
        $country->setAttributes(array(
            'id' => 'country'
        
        ));
        $country->setLabel(__("country"));
        $country->setValue(1);
        
        $countryList = array(
            '0' => __('Please choose one country')
        );
        
        if ($countries) {
            foreach ($countries as $item) {
                $countryList[$item['id']] = $item['country_name'];
            }
        }
        $country->setValueOptions($countryList);
        
        $provience = new Element\Select('provience');
        $provience->setAttributes(array(
            'id' => 'provience'
        
        ));
        $provience->setLabel(__("provience"));
        $provienceList = array(
            '0' => __('Please choose one provience')
        );
        $provience->setValueOptions($provienceList);
        $provience->setDisableInArrayValidator(true);
        
        $city = new Element\Select('city');
        $city->setAttributes(array(
            'id' => 'city-id'
        
        ));
        $city->setLabel(__("city"));
        $cityList = array(
            '0' => __('Please choose one city')
        );
        $city->setValueOptions($cityList);
        $city->setDisableInArrayValidator(true);
        
        $address = new Element\Textarea('address');
        $address->setAttributes(array(
            'id' => 'address',
            'class' => 'form-control validate[custom[onlyLetterNumber]]'
        ));
        $address->setLabel(__("Address"));
        
        $postalcode = new Element\Text('postalcode');
        $postalcode->setAttributes(array(
            'class' => 'form-control validate[custom[onlyNumberSp],maxSize[10], minSize[10]]',
            'id' => 'postalcode'
        ));
        $postalcode->setLabel(__("Postalcode"));
        
        $status = new Element\Select('status');
        $status->setLabel(__("Status"));
        $status->setAttributes(array(
            'id' => 'status',
            'class' => 'form-control'
        ));
        $stags = array(
            '0' => __("inactive"),
            '1' => __("active")
        );
        $status->setValueOptions($stags);
        
        $telegram = new Element\Text('telegram');
        $telegram->setAttributes(array(
            'class' => 'form-control validate[custom[onlyNumberSp],maxSize[11], minSize[11]]',
            'id' => 'telegram',
            'placeholder' => '09121234567'
        ));
        $telegram->setLabel(__("Telegram No"));
        
        $bankCardNo = new Element\Text('bank_card_no');
        $bankCardNo->setAttributes(array(
            'class' => 'form-control',
            'id' => 'telegram',
            'placeholder' => __("Bank card no")
        ));
        $bankCardNo->setLabel(__("Bank card no"));
        
        $nationalCode = new Element\Text('national_code');
        $nationalCode->setAttributes(array(
            'class' => 'form-control validate[custom[onlyNumberSp],maxSize[10], minSize[10]] ',
            'id' => 'telegram'
        ));
        $nationalCode->setLabel(__("National code"));
        
        $phone = new Element\Text('phone');
        $phone->setAttributes(array(
            'class' => 'form-control validate[custom[onlyNumberSp],maxSize[11], minSize[8]]',
            'id' => 'phone',
            'placeholder' => '02188888888 , 88888888'
        
        ));
        $phone->setLabel(__("Phone"));
        
        $company_address = new Element\Textarea('company_address');
        $company_address->setAttributes(array(
            'class' => 'form-control',
            'id' => 'company_address'
        
        ));
        $company_address->setLabel(__("Company Address"));
        
        $university1 = new Element\Text('university1');
        $university1->setAttributes(array(
            'id' => 'university1',
            'class' => 'form-control'
        ));
        $university1->setLabel(__("Univesity"));
        
        $grade1 = new Element\Select('grade1');
        $grade1->setLabel(__("Grade"));
        $grade1->setAttributes(array(
            'id' => 'grade',
            'class' => 'form-control'
        ));
        $grades = array(
            'under diploma' => __("under diploma"),
            'diploma' => __("diploma"),
            'bachelor' => __("Bachelor"),
            'master' => __("Master"),
            'PHD' => __("PHD")
        );
        $grade1->setValue(__("bachelor"));
        $grade1->setValueOptions($grades);
        
        $field_of_study1 = new Element\Text('field_of_study1');
        $field_of_study1->setLabel(__("Field of study"));
        $field_of_study1->setAttributes(array(
            'id' => 'status',
            'class' => 'form-control'
        ));
        
        $university2 = new Element\Text('university2');
        $university2->setAttributes(array(
            'id' => 'university2',
            'class' => 'form-control'
        ));
        $university2->setLabel(__("Univesity"));
        
        $grade2 = new Element\Select('grade2');
        $grade2->setLabel(__("Grade"));
        $grade2->setAttributes(array(
            'id' => 'grade',
            'class' => 'form-control'
        ));
        $grades = array(
            'under diploma' => __("under diploma"),
            'diploma' => __("diploma"),
            'bachelor' => __("Bachelor"),
            'master' => __("Master"),
            'PHD' => __("PHD")
        );
        $grade2->setValue(__("bachelor"));
        $grade2->setValueOptions($grades);
        
        $field_of_study2 = new Element\Text('field_of_study2');
        $field_of_study2->setLabel(__("Field of study"));
        $field_of_study2->setAttributes(array(
            'id' => 'status',
            'class' => 'form-control'
        ));
        
        $university3 = new Element\Text('university3');
        $university3->setAttributes(array(
            'id' => 'university3',
            'class' => 'form-control'
        ));
        $university3->setLabel(__("Univesity"));
        
        $grade3 = new Element\Select('grade3');
        $grade3->setLabel(__("Grade"));
        $grade3->setAttributes(array(
            'id' => 'grade',
            'class' => 'form-control'
        ));
        $grades = array(
            'under diploma' => __("under diploma"),
            'diploma' => __("diploma"),
            'bachelor' => __("Bachelor"),
            'master' => __("Master"),
            'PHD' => __("PHD")
        );
        $grade3->setValue(__("bachelor"));
        $grade3->setValueOptions($grades);
        
        $field_of_study3 = new Element\Text('field_of_study3');
        $field_of_study3->setLabel(__("Field of study3"));
        $field_of_study3->setAttributes(array(
            'id' => 'status',
            'class' => 'form-control'
        ));
        
        $university4 = new Element\Text('university4');
        $university4->setAttributes(array(
            'id' => 'university4',
            'class' => 'form-control'
        ));
        $university4->setLabel(__("Univesity"));
        
        $grade4 = new Element\Select('grade4');
        $grade4->setLabel(__("Grade"));
        $grade4->setAttributes(array(
            'id' => 'grade4',
            'class' => 'form-control'
        ));
        $grades = array(
            'under diploma' => __("under diploma"),
            'diploma' => __("diploma"),
            'bachelor' => __("Bachelor"),
            'master' => __("Master"),
            'PHD' => __("PHD")
        );
        $grade4->setValue(__("bachelor"));
        $grade4->setValueOptions($grades);
        
        $field_of_study4 = new Element\Text('field_of_study4');
        $field_of_study4->setLabel(__("Field of study"));
        $field_of_study4->setAttributes(array(
            'id' => 'field_of_study4',
            'class' => 'form-control'
        ));
        
        $university5 = new Element\Text('university5');
        $university5->setAttributes(array(
            'id' => 'university5',
            'class' => 'form-control'
        ));
        $university5->setLabel(__("Univesity"));
        
        $grade5 = new Element\Select('grade5');
        $grade5->setLabel(__("Grade"));
        $grade5->setAttributes(array(
            'id' => 'grade5',
            'class' => 'form-control '
        ));
        $grades = array(
            'under diploma' => __("under diploma"),
            'diploma' => __("diploma"),
            'bachelor' => __("Bachelor"),
            'master' => __("Master"),
            'PHD' => __("PHD")
        );
        $grade5->setValue(__("bachelor"));
        $grade5->setValueOptions($grades);
        
        $field_of_study5 = new Element\Text('field_of_study5');
        $field_of_study5->setLabel(__("Field of study"));
        $field_of_study5->setAttributes(array(
            'id' => 'status',
            'class' => 'form-control '
        ));
        
        $university6 = new Element\Text('university6');
        $university6->setAttributes(array(
            'id' => 'university1',
            'class' => 'form-control '
        ));
        $university6->setLabel(__("Univesity"));
        
        $grade6 = new Element\Select('grade6');
        $grade6->setLabel(__("Grade"));
        $grade6->setAttributes(array(
            'id' => 'grade',
            'class' => 'form-control'
        ));
        $grades = array(
            'diploma' => __("diploma"),
            'bachelor' => __("bachelor"),
            'master' => __("master"),
            'PHD' => __("PHD")
        );
        $grade6->setValue(__("bachelor"));
        $grade6->setValueOptions($grades);
        
        $field_of_study6 = new Element\Text('field_of_study6');
        $field_of_study6->setLabel(__("Field of study"));
        $field_of_study6->setAttributes(array(
            'id' => 'status',
            'class' => 'form-control validate[custom[onlyLetterSp]]'
        ));
        
        $experience = new Element\Textarea('experience');
        $experience->setAttributes(array(
            'class' => 'form-control',
            'id' => 'experience'
        ));
        $experience->setLabel(__("Work resume"));
        
        $research = new Element\Textarea('research');
        $research->setAttributes(array(
            'class' => 'form-control',
            'id' => 'research'
        ));
        $research->setLabel(__("Background research"));
        
        $aboutme = new Element\Textarea('aboutme');
        $aboutme->setAttributes(array(
            'class' => 'form-control',
            'id' => 'aboutme'
        ));
        $aboutme->setLabel(__("Biography"));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__("Save"));
        $submit->setAttributes(array(
            'id' => 'submit',
            'class' => 'btn btn-primary'
        ));
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__("Save and New"));
        $submit2->setAttributes(array(
            'class' => 'btn btn-primary',
            'id' => 'submit'
        ));
        
        $this->add($image)
            ->add($firstname)
            ->add($lastname)
            ->add($pageLang)
            ->add($image)
            ->add($password)
            ->add($gender)
            ->add($user_group)
            ->add($priority)
            ->add($mobile)
            ->add($country)
            ->add($address)
            ->add($postalcode)
            ->add($username)
            ->add($email)
            ->add($date_of_birth)
            ->add($provience)
            ->add($city)
            ->add($status)
            ->add($telegram)
            ->add($bankCardNo)
            ->add($nationalCode)
            ->add($phone)
            ->add($company_address)
            ->add($university1)
            ->add($grade1)
            ->add($field_of_study1)
            ->add($university2)
            ->add($grade2)
            ->add($field_of_study2)
            ->add($university3)
            ->add($grade3)
            ->add($field_of_study3)
            ->add($university4)
            ->add($grade4)
            ->add($field_of_study4)
            ->add($university5)
            ->add($grade5)
            ->add($field_of_study5)
            ->add($university6)
            ->add($grade6)
            ->add($field_of_study6)
            ->add($experience)
            ->add($research)
            ->add($aboutme)
            ->add($csrf)
            ->add($submit2)
            ->add($submit);
        $this->inputFilters();
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'avatar',
            'required' => false
        
        )));
        // }
        /*
         * $inputFilter->add($factory->createInput(
         * array(
         * 'name' => 'post_meta_description',
         * 'required' => false,
         * 'filters' => array(
         * array(
         * 'name' => 'StripTags'
         * )
         * ),
         * )
         * ));
         * $inputFilter->add($factory->createInput(
         * array(
         * 'name' => 'post_meta_keywords',
         * 'required' => false,
         * 'filters' => array(
         * array(
         * 'name' => 'StripTags'
         * )
         * ),
         * )
         * ));
         * $inputFilter->add($factory->createInput(
         * array(
         * 'name' => 'post_source_url',
         * 'required' => false,
         * 'filters' => array(
         * array(
         * 'name' => 'StripTags'
         * )
         * ),
         * )
         * ));
         * $inputFilter->add($factory->createInput(
         * array(
         * 'name' => 'post_source_title',
         * 'required' => false,
         * 'filters' => array(
         * array(
         * 'name' => 'StripTags'
         * )
         * ),
         * )
         * ));
         */
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
