<?php 
namespace User\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class EducationFieldofstudyTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'education_fieldofstudy';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway($this->table, $this->getDbAdapter());
    }
	public function getRecords($where = false)
	{
	    $select = new Select($this->table);
		return $this->tableGateway->select($where)->toArray();
	}
	
	

}
