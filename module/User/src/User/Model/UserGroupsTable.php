<?php 
namespace User\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class UserGroupsTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'user_groups';
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway($this->table , $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getRecordById($id)
	{
	    $select = new Select($this->table);
	    $select->where(array(
	        'id' => $id
	    ));
	    //return $select;
	    return $this->tableGateway->selectWith($select)->toArray();
	}

}
