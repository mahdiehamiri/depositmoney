<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;

class PortfolioDetailsTable extends BaseModel 
{
public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('portfolio_details', $this->getDbAdapter());
    }
 
   
    public function getRecordById($id, $language)
    {
        $select = new Select('portfolio_details');
        $select->where(array(
            'portfolio_id' => $id,
            'lang'    => $language
        ));
        //return $select;
        return $this->tableGateway->selectWith($select)->toArray();
    }
	public function deletePortfolio($portfolioId)
	{  
	    return $this->tableGateway->delete(array(
	        'portfolio_id' => $portfolioId
	    ));
	}
	public function addPortfolioDetails($portfolioId, $validData, $lang = "fa")
	{
	    $this->tableGateway->insert(array(
	        'portfolio_id'     => $portfolioId,
	        'title'            => $validData['title'],
	        'description'      => $validData['description'],
	        'lang'   => $validData['lang'],
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function updatePortfolio($portfolioId, $validData)
	{
	    if ($portfolioId > 0 && is_numeric($portfolioId)) {
	        $newData = array(
	            'title'            => $validData['title'],
    	        'description'      => $validData['description']
	        );
	        return $this->tableGateway->update($newData, array('portfolio_id' => $portfolioId, 'lang' => $validData["lang"]));
	    }
	}
}