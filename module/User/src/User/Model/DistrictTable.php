<?php 

namespace User\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Having;

class DistrictTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('district', $this->getDbAdapter());
    }
    
	public function getRecords()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getById($id,$area)
	{
	    $select = new Select('district');
	    $select->where->equalTo('area_id', $area);
	    $select->where->equalTo('id', $id);
// 	    $select->columns(array(
// 	        'district' => new Expression("GROUP_CONCAT(DISTINCT id ORDER BY id ASC SEPARATOR ',')"),
// 	        'area_id'
// 	    ));
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function getByArea($id)
	{
	    $select = new Select('district');
	    $select->where->equalTo('area_id', $id);
	    $select->columns(array(
	        'district' => new Expression("GROUP_CONCAT(DISTINCT name ORDER BY name ASC SEPARATOR ',')"),
	        'area_id'
	    ));
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function getRecordsByArea($name)
	{
	    $select = new Select('district');
// 	    $select->where->equalTo('area_id', $id);
// 	    $select->columns(array(
// 	        'district' => new Expression("GROUP_CONCAT(DISTINCT name ORDER BY name ASC SEPARATOR ',')"),
// 	        'area_id'
// 	    ));
	    $select->join('area', 'area.id = district.area_id', array(
// 	        'name',
// 	        'district',
// 	        'address'
	    ), Select::JOIN_LEFT);
	    $select->where->equalTo('area.name', $name);
  	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function search($data)
	{
	    $select = new Select('office');
	    $select->join('users', 'users.id = office.user_id', array(
			'area',
			'district',
			'address'
		), Select::JOIN_LEFT);
	    
	    $having = new Having();
	    
	    if($data['services'] && !empty($data['services'])){
    	    $select->join('service_index', 'service_index.office_id = office.id', array(
    	        'services_id' => new Expression("GROUP_CONCAT(DISTINCT service_id ORDER BY service_id ASC SEPARATOR ',')")
    	    ), Select::JOIN_LEFT);
    	    
    	    sort($data['services']);
    	    $data['services'] = implode(",",$data['services']);
//     	    $having->like("services_id",'%' . $data['services'] . '%'); 
//     	    $having->nest()->like('services_id', $data['services'] . '%')->or->like('services_id', $data['services'] . ',%')->unnest(); 
	    }
	    
	    if($data['name'] && !empty($data['name'])){
	        $select->where->like('office.office_name', '%' . $data['name'] . '%');
	    }
	    
	    if($data['area'] && !empty($data['area'])){
	        $select->where->like('users.area', '%' . $data['area'] . '%');
	    }
	    
	    if($data['district'] && !empty($data['district'])){
	        $select->where->like('users.district', '%' . $data['district'] . '%');
	    }
	    $select->group('office.id');
	    $select->having($having);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function add($postData)
	{
	    
	    $this->tableGateway->insert(array(
	        'name' => $postData['district'],
	        'area_id' => $postData['area_id'],
	        
	    ));
	    
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function edit($data,$id){
	     $this->tableGateway->update(array(
	        'office_name'           => $data['office_name'],
	        'manager_name'         => $data['manager_name'],
	        'fax'        => $data['fax'],
	        'online_services'        => $data['online_services'],
	        'usual_courier'        => $data['usual_courier'],
	        'special_courier'        => $data['special_courier'],
	    ), array(
	        'user_id' => $id
	    ));
	     
	     return true;
	    
	}
    
	public function delete($name,$area)
	{
	    return $this->tableGateway->delete(array(
	        'name' => $name,
	        'area_id' => $area
	    ));
	}
// 	public function getAllComments()
// 	{
// 		return $this->tableGateway->select()->toArray();
// 	}
	
// 	public function fetchComments($userId = null)
// 	{
// 		$select = new Select('comments');
// 		$select->join('users', 'users.id = comments.user_id', array(
// 				'firstname',
// 				'lastname'
// 		), Select::JOIN_LEFT);
// 		$select->join('blog_posts', new Expression('(comments.entity_id = blog_posts.id AND comments.entity_type = "post")'), array(
// 				'post_title',
// 				'post_slug'
// 		), Select::JOIN_LEFT);
// 		$select->join('blog_post_categories', 'blog_post_categories.blog_post_id = blog_posts.id', array(), Select::JOIN_LEFT);
// 		$select->join('blog_categories', 'blog_categories.id = blog_post_categories.blog_category_id', array(
// 			'category_title'
// 		), Select::JOIN_LEFT);
// 		$select->join('shop_products', new Expression('(comments.entity_id = shop_products.id AND comments.entity_type = "product")'), array(
// 				'product_id' => 'id',
// 				'name'
// 		), Select::JOIN_LEFT);
// 		if ($userId) {
// 			$select->where->equalTo('comments.user_id', $userId);
// 		}
// 		$select->order('comments.id DESC');
// 		return $select;
// // 		$comments = $this->tableGateway->selectWith($select)->toArray();
// // 		return $comments;
// 	}
	
// 	public function fetchCommentsReplies($userId = null)
// 	{
// 		$select = new Select('comments');
// 		$select->join('users', 'users.id = comments.user_id', array(
// 				'firstname',
// 				'lastname'
// 		), Select::JOIN_LEFT);
// 		$select->join('blog_posts', new Expression('(comments.entity_id = blog_posts.id AND comments.entity_type = "post")'), array(
// 				'post_title',
// 				'post_slug'
// 		), Select::JOIN_LEFT);
// 		$select->join('projects', new Expression('(comments.entity_id = projects.id AND comments.entity_type = "project")'), array(
// 				'project_id' => 'id',
// 				'project_title'
// 		), Select::JOIN_LEFT);
// 		$select->join('courses', new Expression('(comments.entity_id = courses.id AND comments.entity_type = "tutorials")'), array(
// 				'course_name',
// 				'course_title'
// 		), Select::JOIN_LEFT);
		
// 		$select->join(array('tutorial_courses' => 'courses'), 'tutorial_courses.id = tutorials.course_id', array(
// 				'tutorial_course_name' => 'course_name'
// 		), Select::JOIN_LEFT);
		
// 		$select->join('blog_post_categories', 'blog_post_categories.blog_post_id = blog_posts.id', array(), Select::JOIN_LEFT);
// 		$select->join('blog_categories', 'blog_categories.id = blog_post_categories.blog_category_id', array(
// 			'category_title'
// 		), Select::JOIN_LEFT);
// 		if ($userId) {
// 			$selectOwnComments = new Select();
// 			$selectOwnComments->columns(array('id'));
// 			$selectOwnComments->where->equalTo('user_id', $userId);
// 			$selectOwnComments->from('comments');
// 			$select->where->in('comments.parent_id', $selectOwnComments);
// 		}
// 		$select->order('comments.id DESC');
// 		$comments = $this->tableGateway->selectWith($select)->toArray();
// 		return $comments;
// 	}
	
// 	public function deleteComment($commentId = null)
// 	{
// 	    return $this->tableGateway->delete(array('id' => $commentId));
// 	}
}
