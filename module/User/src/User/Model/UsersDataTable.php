<?php 
namespace User\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Profiler\Profiler;

class UsersDataTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('users_data', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->sele()->toArray();
	}

	public function getRecordById($id, $language)
	{
	    $select = new Select('users_data');
	    $select->where(array(
	        'user_id' => $id,
	        'lang'    => $language
	    ));
	    //return $select;
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getAllUsers($userId = null)
	{
	    $select = new Select('users_data');
	    if ($userId) {
	        $select->where(array(
	            'user_creator' => $userId
	        ));
	    }
		return $select;
	}

	public function addUserData($postData, $userId)
	{

	        $this->tableGateway->insert(array(
	            'firstname' 	    => $postData['firstname'],
	            'lastname' 		    => $postData['lastname'],
	            'experience'        => $postData['experience'],
	            'research'          => $postData['research'],
	            'aboutme' 		    => $postData['aboutme'],
	            'user_id'           => $userId,
	            'lang'              => $postData['lang'],
	        ));
	        return $this->tableGateway->getLastInsertValue();
	}


	
	public function editUser($editData, $userId, $lang)
	{

	        $data = array(
	            'firstname' 	    => $editData['firstname'],
	            'lastname' 		    => $editData['lastname'],
	            'experience'        => $editData['experience'],
	            'research'          => $editData['research'],
	            'aboutme' 		    => $editData['aboutme'],
	        );
	       
			$update = $this->tableGateway->update($data, array('user_id' => $userId, 'lang' => $lang));
			return $update;
	         

	}
	
	public function deleteUserData($userId)
	{

		return $this->tableGateway->delete(array(
				'user_id' => $userId
		));

	}

}
