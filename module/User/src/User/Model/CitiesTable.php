<?php 

namespace User\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class CitiesTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('city', $this->getDbAdapter());
    }
    
	public function getRecords($id = false)
	{
		$select = new Select('city');
		if ($id) {
			$select->where->equalTo('province_id', $id);
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	
	public function getRecordsById($id = false)
	{
	    $select = new Select('city');
	    if ($id) {
	        $select->where->equalTo('id', $id);
	        return $this->tableGateway->selectWith($select)->toArray();
	    }
	}
}
