<?php 
namespace User\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Profiler\Profiler;

class UsersTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('users', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->sele()->toArray();
	}
	
	
	public function bulkPassword($userEmail, $hash)
	{
	    $isEmailRegistered = $this->tableGateway->select(array(
	        'email' => $userEmail['email']
	    ))->current();
	    if ($isEmailRegistered) {
	        $data = array(
	            'forgot_password_hash' => $hash
	        );
	        return $this->tableGateway->update($data, array('email' => $isEmailRegistered['email']));
	    } else {
	        return 'no_user_exists';
	    }
	}
	
	public function getRecordByIdForView($id, $lang = null)
	{
	    $select = new Select('users');
	    $select->join('users_data', 'users_data.user_id = users.id',array("*"), $select::JOIN_LEFT);
	    $select->where->equalTo("users.id", $id);
	    if ($lang) {
	        $select->where->equalTo("lang", $lang);
	    }
	    //var_dump($lang); die;
	    //return $select;
	    return $this->tableGateway->selectWith($select)->toArray();
	} 
	
	public function getRecordById($id, $lang = null)
	{
	    $select = new Select('users');
	    //$select->join('users_data', 'users_data.user_id = users.id',array("*"), $select::JOIN_LEFT);
	    $select->where->equalTo("users.id", $id);
	    /* if ($lang) {
	        $select->where->equalTo("lang", $lang);
	    } */
	    //var_dump($lang); die;
	    //return $select;
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function getAllUsers($userId = null)
	{
	   $select = new Select('users');
	   $select->join('provinces', 'provinces.id = users.provience',array('name'));
	   // $select->join('users_data', 'users_data.user_id = users.id',array('name' => new Expression('CONCAT(users_data.firstname, " ", users_data.lastname)')));
	    $where = new Where();
	    if ($userId) {
	        $select->where->equalTo('user_creator', $userId);
	        /* $select->where(array(
	            'user_creator' => $userId
	        )); */
	    }
	   
	    $select->where->notEqualTo('users.id', 1);
	    $select->order('user_group_id');
	    
	   // return $this->tableGateway->selectWith($select)->toArray();
		return $select;
	}
	
	public function getAllUsersData($lang=false, $getPaginator = null)
	{
	    $select = new Select('users');
	    $select->join('users_data', 'users.id = users_data.user_id',array('aboutme','firstname', 'lastname'));
	    $select->join('studdies', 'users.id=studdies.user_id', array("field_of_study", "university"), 'LEFT');
	    if ($lang) {
	        $select -> where->equalTo("users_data.lang", $lang);
	    }
	    $select->order("studdies.id DESC");
	    $select->where->equalTo("studdies.lang", $lang);
	    $select->where->equalTo("user_group_id", "4");
	    $select->columns(array('id','username', 'avatar')); 
	    if ($getPaginator) {
	        $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
	        return new Paginator($dbSelector);
	    } else {
	        return $this->tableGateway->selectWith($select)->toArray();
	    }
	    //return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addUser($postData)
	{
	    
	    $isEmailRegistered = $this->tableGateway->select(array(
	        'email' => $postData['email']
	    ))->toArray();
	    if (!$isEmailRegistered) {
	        $salt = rand(1000000, 9000000);
	        $this->tableGateway->insert(array(
	            'salt'     		    => $salt,
	            'password' 			=> md5(sha1($postData['password']) . $salt),
	            'mobile' 			=> $postData['mobile'],
	            'status' 		    => (int)$postData['status'],
	            'user_group_id'     => $postData['user_group_id'],
	            'priority'          => $postData['priority'],
	            'avatar'            => $postData['avatar'],
	            'date_of_birth'     => $postData['date_of_birth'],
	            'user_creator'      => $postData['user_creator'],
	            'email'             => $postData['email'],
	            'username'          => ($postData['username'] == ''?$postData['email'] :$postData['username']),
	            'telegram' 			=> $postData['telegram'],
	            'bank_card_no' 		=> (isset($postData['bank_card_no'])?$postData['bank_card_no']:""),
	            'national_code' 	=> $postData['national_code'],
	            'phone'             => $postData['phone'],
	            'company_address'   => $postData['company_address'],
	            'gender'            => $postData['gender'],
	            'country'           => $postData['country'],
	            'provience'         => $postData['provience'],
	            'city' 		        => $postData['city'],
	            'address' 		    => $postData['address'],
	            'postalcode'        => $postData['postalcode'],
	        ));
	        return $this->tableGateway->getLastInsertValue();
	        
	        
	    } else {
	        return false;
	    }
	    
	}
	
	public function addNewUser($postData)
	{
	    $salt = rand(1000000, 9000000);
	    $postData['salt'] = $salt;
	    if($postData['job']){
	        $postData['job'] = $postData['job'];
	    }else{
	        $postData['job'] = ' ';
	    }
	    if($postData['firstname']){
	        $postData['firstname'] = $postData['firstname'];
	    }else{
	        $postData['firstname'] = ' ';
	    }
	    if($postData['lastname']){
	        $postData['lastname'] = $postData['lastname'];
	    }else{
	        $postData['lastname'] = ' ';
	    }
	    $this->tableGateway->insert(array(
	        'username' => $postData['username'],
	        'email' => $postData['email'],
	        'phone' => $postData['phone'],
	        'password' => md5(sha1($postData['password']) . $salt),
	        'mobile' => $postData['mobile'],
	        'address' => $postData['address'],
	        'area' => $postData['area'],
	        'district' => $postData['district'],
	        'salt' => $postData['salt'],
	        'user_group_id' => $postData['user_group_id'],
	        'job' => $postData['job'],
	        'firstname' => $postData['firstname'],
	    ));
	    
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function addBulkUser($postData, $password, $hash, $mobileHash)
	{
	    $isEmailRegistered = $this->tableGateway->select(array(
	        'email' => $postData['email']
	    ))->toArray();
	    if (!$isEmailRegistered) {
	        $salt = rand(1000000, 9000000);
	        $this->tableGateway->insert(array(
	            'salt'     		    => $salt,
	            'password' 			=> md5(sha1($password) . $salt),
	            'mobile' 			=> $postData['mobile'],
	            'status' 		    => 0,
	            'user_group_id'     => 3,
	            'user_creator'      => $postData['user_creator'],
	            'email'             => $postData['email'],
	            'username'          => ($postData['firstname'].$postData['lastname'] == ''?$postData['email'] :$postData['firstname'] . " " . $postData['lastname']),
	            'verify_hash'       => $hash,
	            'mobile_verify_hash' => $mobileHash,
	            'telegram' 			=> $postData['telegram'],
	            'national_code' 	=> $postData['national_code'],
	            'phone'             => $postData['phone'],
	            'company_address'   => $postData['company_address'],
	            'gender'            => $postData['gender'],
	            'country'           => $postData['country'],
	            'provience'         => $postData['provience'],
	            'city' 		        => $postData['city'],
	            'address' 		    => $postData['address'],
	            'postalcode'        => $postData['postalcode'],
	        ));
	        return $this->tableGateway->getLastInsertValue();
	    } else {
	        return 'user_exists';
	    }
	     
	}
	
	public function getHash($hash)
	{
	    return $this->tableGateway->select(array(
	        'verify_hash' => $hash
	    ))->toArray();
	}
	
	public function editUser($editData, $userId, $userGroup)
	{
	    $isEmailRegistered = $this->tableGateway->select(array(
				'email' => $editData['email'],
				'id != ?' => $userId
		))->toArray();
	    if (!$isEmailRegistered) {
	        $data = (array(
	            'mobile' 			=> $editData['mobile'],
	        
	            'avatar'            => $editData['avatar'],
	            'date_of_birth'     => $editData['date_of_birth'],
	            'email'             => $editData['email'],
	            'username'          => ($editData['username'] == ''?$editData['email'] :$editData['username']),  
	            'telegram' 			=> $editData['telegram'],
	            'bank_card_no' 		=> (isset($editData['bank_card_no'])?$editData['bank_card_no']:""),
	            'national_code' 	=> $editData['national_code'],
	            'phone'             => $editData['phone'],
	            'company_address'   => $editData['company_address'],
	            'gender'            => $editData['gender'],
	            'country'           => $editData['country'],
	            'provience'         => $editData['provience'],
	            'city' 		        => $editData['city'],
	            'address' 		    => $editData['address'],
	            'postalcode'        => $editData['postalcode'],
	            
	        ));
	        if ($userGroup == 1 || $userGroup == 2) {
	            $data['status'] = (int)$editData['status'];
	            if ($editData['user_group_id'] == 1 && $userGroup == 2) {
	                $editData['user_group_id'] = 2;
	            }
	            $data['user_group_id'] = $editData['user_group_id'];
	            $data['priority'] = $editData['priority'];
	        }
	        if ($editData['password'] && $editData['password'] != '') {
				$salt = rand(1000000, 9000000);
				$data['password'] = md5(sha1($editData['password']) . $salt);
				$data['salt'] = $salt;
			}
			
			$update = $this->tableGateway->update($data, array('id' => $userId));
			return $update;
	         
	         
	    } else {
	        return 'Email_exists';
	    }
		
		
	}
	
	public function deleteUser($userId)
	{
		return $this->tableGateway->delete(array(
				'id' => $userId
		));

	}

	public function getRecordByGroup($group)
	{  
	    $select = new Select('users');
	    $select->where->equalTo("users.user_group_id", $group);
	    $select->join('user_groups', 'user_groups.id = users.user_group_id',array(
	       'group_name' => 'title'
	    ), $select::JOIN_LEFT);
	    if($group == 7){
	        $select->join('office', 'users.id = office.user_id',array(
	            'office_name',
	            'manager_name'
	        ), $select::JOIN_LEFT);
	    }
	    elseif($group == 8){
	        $select->join('company', 'users.id = company.user_id',array(
	           'company_name',
	           'manager_name'
	        ), $select::JOIN_LEFT);
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
    
	public function edit($data,$id){
	    $this->tableGateway->update(array(
	        'username'           => $data['username'],
	        'email'         => $data['email'],
	        'phone'        => $data['phone'],
	        'mobile'        => $data['mobile'],
	        'area'        => $data['area'],
	        'district'        => $data['district'],
	        'address'        => $data['address'],
	        'job'        => $data['job'],
	        'description'        => $data['description'],
	    ), array(
	        'id' => $id
	    ));
	    return true;
	}
	
	public function update($id){
	    $this->tableGateway->update(array('confirm' => 1,), array('id' => $id));
	    return true;
	}

	public function getUserByEmail($email)
	{
	    $select = new Select('users');
	    $select->where->equalTo('email', $email);
	    return $this->tableGateway->selectWith($select)->current();
	}
}
