<?php 

namespace User\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class CountryTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('country', $this->getDbAdapter());
    }
    
	public function getRecords($id = false)
	{
		$select = new Select('country');

	   return $this->tableGateway->selectWith($select)->toArray();
	}
}
