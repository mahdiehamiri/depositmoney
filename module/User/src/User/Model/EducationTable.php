<?php 
namespace User\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;

class EducationTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'education_course';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway($this->table, $this->getDbAdapter());
    }
	
	public function getAllFieldEducations($userId)
	{
	    $select = new Select($this->table);
	    $select->join('education_course_registration', 'education_course.id = education_course_registration.education_course_id',array());
	    if($userId){
    	    $select->where(array(
    	        'user_id' => $userId
    	    ));
	    }
	    return $select;
	
	}

}
