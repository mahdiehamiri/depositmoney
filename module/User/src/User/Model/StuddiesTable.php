<?php 
namespace User\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use MyNamespace\Zend\Db\Sql\InsertIgnore;

class StuddiesTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'studdies';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway($this->table, $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->sele()->toArray();
	}
	
	public function getRecordById($id, $lang)
	{
	    $select = new Select($this->table);
	    $select->where(array(
	        'user_id' => $id,
	        'lang' => $lang
	    ));
	    //return $select;
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getAllUsers()
	{
	    $select = new Select($this->table);
	    $select->where(array(
	        'user_group_id' => 4
	    ));
	    
		return $select;
	}
	
	public function insert($postData, $newUserId)
	{
	    //var_dump($postData); die;
	    foreach ($postData as $data){
	        if($data['field_of_study']){
	            $insert = $this->tableGateway->insert(array(
	                'user_id' 	    => $newUserId,
	                'university' 	    => $data['univercity'],
	                'grade' 		    => $data['grade'],
	                'field_of_study' 			=> $data['field_of_study']
	                ));
	        }
	        
	    }
	    return true;
	}

	
	public function editStudy($studies, $userId, $userLang)
	{
	    foreach ($studies as $data){
	        if($data['field_of_study']) {
	            $newData = (array(
	                'user_id' 	    => $userId,
	                'university' 	    => $data['univercity'],
	                'grade' 		    => $data['grade'],
	                'field_of_study' 			=> $data['field_of_study'],
	                'lang' 			=> $userLang
	            ));
	        }
	       $isAdded = $this->tableGateway->insert($newData);
	    }
        if (!empty($isAdded)) {
            return true;
        } else {
            return false;
        }
		
		
	}
	
	public function deleteStudy($userId, $lang)
	{
		return $this->tableGateway->delete(array(
				'user_id' => $userId,
		        'lang' => $lang,
		));

	}

}
