<?php 

namespace User\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class DesignerTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('designer', $this->getDbAdapter());
    }
    
	public function getRecords()
	{
		return $this->tableGateway->select()->toArray();
	}

	public function fetchAllDesigner()
	{
		$select = new Select('designer');
	    $select->join("users", "users.id = designer.user_id", array(
			"designer_firstname" => 'firstname' ,
			"designer_lastname" => 'lastname' ,
			"designer_username" => 'username' ,
	    ), $select::JOIN_LEFT);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getRecordById($id)
	{
	    $select = new Select('designer');
	    $select->where->equalTo('designer.user_id', $id);
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function add($postData)
	{
	    $this->tableGateway->insert(array(
	        'cv_file' => $postData['cv_file'],
	        'work_exp' => $postData['work_exp'],
	        'work_samples' => $postData['work_samples'],
	        'user_id' => $postData['user_id'],
	        'description' => $postData['description'],
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function edit($postData,$id){
	    return $this->tableGateway->update(array(
	        'cv_file' => $postData['cv_file'],
	        'work_exp' => $postData['work_exp'],
	        'work_samples' => $postData['work_samples'],
	    ), array(
	        'user_id' => $id
	    ));
	}
}
