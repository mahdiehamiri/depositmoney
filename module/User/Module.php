<?php

namespace User;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Application\Helper\UrlGenerator;
use Application\Helper\Date;
use Application\Helper\Permissions;
use Application\Helper\UrlGeneratorInController;
use Application\Helper\MenuGenerator;
use Application\Helper\GravatarGenerator;
use Application\Helper\MailService;
use Zend\Validator\AbstractValidator;
use Zend\I18n\Translator\Translator;
use Application\Helper\NumberConverter;
use Application\Helper\ImageUploader;
use Zend\I18n\Translator\Translator as I18nTranslator;
use Application\Helper\UrlGeneratorPage;
use Application\Helper\GeneralHelper;
use Language\Model\LanguageLanguagesTable;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
    	$sm  = $e->getApplication()->getServiceManager();
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
 
}