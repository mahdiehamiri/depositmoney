<?php 
namespace PrintServices\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

class AttributesValueTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    { 
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('attributes_value', $this->adapter);
    }
	
    public function add($postData)
	{
	    $this->tableGateway->insert(array(
	        'attributes_index_id' => $postData['attributes_index_id'],
	        'value' => $postData['value'],
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function getAll()
	{
	    $select = new Select('attributes_value');
	    $select->join('attributes_index', 'attributes_index.id = attributes_value.attributes_index_id ', array(
            '*'
// 	        'attribute_title' => 'title',
// 	        'attribute_name' => 'name'
	    ), Select::JOIN_LEFT);
	    
	    
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getAllVal()
	{
	    $select = new Select('attributes_value');
// 	    $select->where->equalTo('attributes_index_id', $attributeId);
// 	    $select->columns(array(
// 	        'value' => new Expression('Group_Concat(DISTINCT value)')
// 	    ));
	    $select->join('attributes_index', 'attributes_index.id = attributes_value.attributes_index_id ', array(
// 	        'attribute_title' => 'title',
// 	        'attribute_name' => 'name'
	    ), Select::JOIN_LEFT);
	    $select->join('attributes', 'attributes_index.id = attributes_value.attributes_index_id ', array(
	    // 	        'attribute_title' => 'title',
	    // 	        'attribute_name' => 'name'
	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->toArray();
// 	    return $this->tableGateway->select()->toArray();
	}
	
	public function getAllByAttribute($attributeId)
	{
	    $select = new Select('attributes_value');
	    $select->where->equalTo('attributes_index_id', $attributeId);
// 	    $select->columns(array(
// 	        'id' => new Expression('Group_Concat(DISTINCT id)'),
// 	        'attributes_index_id',
// 	        'value' => new Expression('Group_Concat(DISTINCT value)')
// 	    ));
// 	    $select->join('attributes', 'attributes.id = attributes_index.attributes_id ', array(
// 	        'attribute_title' => 'title',
// 	        'attribute_name' => 'name'
// 	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function existValue($attributeId,$val)
	{
	    $select = new Select('attributes_value');
	    $select->where->equalTo('attributes_index_id', $attributeId);
	    $select->where->equalTo('value', $val);
// 	    $select->columns(array(
// 	        'value' => new Expression('Group_Concat(DISTINCT value)')
// 	    ));
	    // 	    $select->join('attributes', 'attributes.id = attributes_index.attributes_id ', array(
	    // 	        'attribute_title' => 'title',
	    // 	        'attribute_name' => 'name'
	    // 	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function delete($id)
	{//var_dump($id);die;
	    return $this->tableGateway->delete(array(
	        'id' => $id
	    ));
	}
	
}
