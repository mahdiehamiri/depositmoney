<?php 
namespace PrintServices\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class OrdersIndexTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    { 
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('orders_index', $this->adapter);
    }
	
    public function getByOrder($id)
    {
        $select = new Select('orders_index');
        $select->where->equalTo('orders_id', $id);
        $select->join('print_service_orders', 'print_service_orders.id = orders_index.order_detail_id ', array(
// //             '*',
            'print_service_id'
        ), Select::JOIN_LEFT);
        $select->join('print_services', 'print_services.id = print_service_orders.print_service_id ', array(
            'title',
            'image'
        ), Select::JOIN_LEFT);
//         $select->join('order', 'order.id = orders_index.orders_id ', array(
//             '*',
//             'order_id' => 'id'
//         ), Select::JOIN_LEFT);
        $select->join('attributes_group', 'attributes_group.id = print_service_orders.attributes_group_id ', array(
            'print_service_id',
            'value',
            'time' ,
            'price' ,
        ), Select::JOIN_LEFT);

        return $this->tableGateway->selectWith($select)->toArray();
    }
    
    public function getByOffice($id)
    {
        $select = new Select('orders_index');
        $select->join('print_service_orders', 'print_service_orders.id = orders_index.order_detail_id ', array(
            '*',
            //             'order_id' => 'id'
        ), Select::JOIN_LEFT);
        $select->join('order', 'order.id = orders_index.orders_id ', array(
            '*',
            'order_id' => 'id'
        ), Select::JOIN_LEFT);
        $select->join('attributes_group', 'attributes_group.id = print_service_orders.attributes_group_id ', array(
            'print_service_id',
            'value',
            'time' ,
            'price' ,
        ), Select::JOIN_LEFT);
        $select->join('print_services', 'print_services.id = print_service_orders.print_service_id ', array(
            'title',
            'image'
            // 	        'attribute_name' => 'name'
        ), Select::JOIN_LEFT);
        $select->where->equalTo('print_service_orders.office_id', $id);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
//     public function get($val){
//         $select = new Select('attributes_group');
//         $select->where->equalTo('value',$val);
//         return $this->tableGateway->selectWith($select)->current();
//     }

    public function add($postData)
	{
	    $this->tableGateway->insert(array(
	        'orders_id' => $postData['orders_id'],
	        'order_detail_id' => $postData['order_detail_id'],
// 	        'delivery_price' => $postData['delivery_price'],
// 	        'discount' => $postData['discount'],
// 	        'created' => date("Y-m-d H:i:m")
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	  
	public function getAll()
	{
	    $select = new Select('orders_index');
	    $select->join('orders', 'orders.id = orders_index.orders_id ', array(
	        '*',
	        'order_id' => 'id'
	    ), Select::JOIN_LEFT);
	    $select->join('print_service_orders', 'print_service_orders.id = orders_index.order_detail_id ', array(
	        '*',
	        //             'order_id' => 'id'
	    ), Select::JOIN_LEFT);
	    $select->join('print_services', 'print_services.id = print_service_orders.print_service_id ', array(
	        'title',
	        'image'
	        // 	        'attribute_name' => 'name'
	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
// 	public function fetchParent()
// 	{
// 	    $select = new Select('print_services');
// 	    $select->where->equalTo('parent_id', 0);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function fetchChild()
// 	{
// 	    $select = new Select('print_services');
// 	    $select->where->notEqualTo('parent_id', 0);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function getService($where = false)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) { 
// 	        $select->where($where);
// 	    } 
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServiceHomePage($where = false, $limit)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) {
// 	        $select->where($where);
// 	    }
// 	    $select->order('id DESC');
// 	    $select->limit($limit);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function searchService($services, $lang, $getPaginator = true)
// 	{
// 	    $select = new Select('services');
// 	    $where = new Where();
// 	    $select->where->nest()->like('name', "%$services%")->or->like('description', "%$services%")->unnest();
// 	    $select->where->equalTo('lang', $lang);
// 	    if ($getPaginator) {
// 	        $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
// 	        return new Paginator($dbSelector);
// 	    } else {
// 	        return $this->tableGateway->selectWith($select)->toArray();
// 	    } 
// 	}
// 	public function getServiceById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServicesChileById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('parent_id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getAllServicesForList()
// 	{
// 	    $select = new Select('services'); 
// 	    return  $select; 
// 	}
// 	public function getServicesByName($name)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->like('name', "%$name%");
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function addServices($postData)
// 	{ 
// 		$this->tableGateway->insert($postData);
// 		return $this->tableGateway->getLastInsertValue();
// 	}
	
// 	public function deleteServices($servicesId, $userId = null)
// 	{
// 		if ($servicesId > 0 && is_numeric($servicesId)) {
// 			if ($userId !== null) {
// 				return $this->tableGateway->delete(array(
// 						'id' 	  			  => $servicesId,
// 						'post_author_user_id' => $userId
// 				));
// 			} else {
// 				return $this->tableGateway->delete(array(
// 						'id' => $servicesId
// 				));
// 			}
// 		}
// 	}
	
// 	public function updateServices($menuData, $menuId)
// 	{ 
// 	    $data = array(
// 	        "name" => $menuData["name"],
// 	        "description" => $menuData["description"],
// 	        "is_parent" => $menuData["is_parent"],
// // 	        "parent_id" => $menuData["parent_id"],
// 	        'lang' => $menuData['lang'],
// 	        "gallery_id" => $menuData["gallery_id"],
// 	        "active" => $menuData["active"],
// 	        "image" => $menuData["image"],
// 	    );
	    
// 	    return $this->tableGateway->update($data,  array('id' => $menuId));
// 	}


}
