<?php 
namespace PrintServices\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class AttributesTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    { 
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('attributes', $this->adapter);
    }
	
    public function add($title)
	{
	    $this->tableGateway->insert(array(
	        'title' => $title,
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function getAll()
	{
	    return $this->tableGateway->select()->toArray();
	}
	
	public function existAttr($id)
	{
	    $select = new Select('attributes');
	    $select->where->equalTo('id', $id);
	    return $this->tableGateway->selectWith($select)->current();
	}

	
}
