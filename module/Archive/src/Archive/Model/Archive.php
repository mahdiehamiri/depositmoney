<?php
namespace Archive\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ArchiveTable extends BaseModel
{

    protected $tableGateway;

    protected $adapter;

    protected $serviceManager;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('archive', $this->adapter);
    }
    
    public function updateMenuId($menu, $id)
    {
        return $this->tableGateway->update(array('menu_id' => $menu),  array('id' => $id));
    }
    public function add($postData)
    {
        $this->tableGateway->insert(array(
            'title' => $postData['title'],
            'parent_id' => $postData['parent_id'],
            'type_id' => $postData['type_id'],
            'image' => $postData['image'],
            'description' => $postData['description']
        ));
        return $this->tableGateway->getLastInsertValue();
    }

    public function edit($postData, $id)
    {
//         var_dump($postData);
//         var_dump($id);
        $data = array(
            'title' => $postData['title'],
            'parent_id' => $postData['parent_id'],
            'image' => $postData['image'],
            'type_id' => $postData['type_id'],
            'description' => $postData['description']
        );
        
         $this->tableGateway->update($data, array(
            'id' => $id
        ));
         
         return true;
    }

    public function getAll()
    {
        // $sql = "Select q.id , q.title, p.title as ParentTitle from print_services as q
        // LEFT JOIN print_services as p ON p.id = q.parent_id ";
        // $allSubVideo = $this->tableGateway->getAdapter()->driver->getConnection()->execute($sql);
        // $subVideoGallery = array();
        // if ($allSubVideo) {
        // foreach ( $allSubVideo as $key => $data ) {
        // $subVideoGallery[$key]['id'] = $data['ParentTitle'];
        // }
        // }
        // return $subVideoGallery;
        // die(var_dump($subVideoGallery));
        // return $this->tableGateway->getAdapter()->driver->getConnection()->execute($sql);
        $select = new Select('archive');
        // $select->join('print_services', 'print_services.id = print_services.parent_id ', array(
        // 'parent_name' => 'print_services.title',
        // ), Select::JOIN_LEFT);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchParent($office = null)
    {
        $select = new Select('archive');
        $select->where->equalTo('parent_id', 0);
        if ($office) {
            $select->where->equalTo('services_types.office_status', 1);
            $select->join('services_types', 'services_types.id = print_services.type_id',array('office_status'));
        }
        
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchChild()
    {
        $select = new Select('archive');
        $select->where->notEqualTo('parent_id', 0);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchChildByArchive($parent)
    {
        $select = new Select('archive');
        $select->where->equalTo('parent_id', $parent);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
    public function fetchByTypeId($type)
    {
        $select = new Select('archive');
        $select->join('services_types', 'services_types.id = archive.type_id ',array(
            'services_type_title' => 'title'
        ), Select::JOIN_LEFT);
        $select->where->equalTo('type_id', $type);
        $select->where->equalTo('parent_id', 0);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchById($id)
    {
        $select = new Select('archive');
        $select->where->equalTo('archive.id', $id);
//         $select->where->equalTo('services_types.office_status', 1);
        $select->join('services_types', 'services_types.id = archive.type_id ',array(
            'office_status',
            'services_type_title' => 'title'
        ), Select::JOIN_LEFT);
        
        $select->join(array('ps' => 'archive'), 'ps.id = archive.parent_id ',array(
            'parent_title' => 'title'
        ), Select::JOIN_LEFT);
        
        
        return $this->tableGateway->selectWith($select)->current();
    }

    public function fetch($id)
    {
        $select = new Select('archive');
        $select->where->equalTo('id', $id);
        return $this->tableGateway->selectWith($select)->current();
    }

    public function delete($id)
    {
        return $this->tableGateway->delete(array(
            'id' => $id
        ));
    }
    
    public function fetchStatus($id){
        $select = new Select('archive');
        $select->where->equalTo('id', $id);
        return $this->tableGateway->selectWith($select)->current();
    }
    
    // public function getService($where = false)
    // {
    // $select = new Select('services');
    // if ($where) {
    // $select->where($where);
    // }
    // return $this->tableGateway->selectWith($select)->toArray();
    // }
    // public function getServiceHomePage($where = false, $limit)
    // {
    // $select = new Select('services');
    // if ($where) {
    // $select->where($where);
    // }
    // $select->order('id DESC');
    // $select->limit($limit);
    // return $this->tableGateway->selectWith($select)->toArray();
    // }
    // public function searchService($services, $lang, $getPaginator = true)
    // {
    // $select = new Select('services');
    // $where = new Where();
    // $select->where->nest()->like('name', "%$services%")->or->like('description', "%$services%")->unnest();
    // $select->where->equalTo('lang', $lang);
    // if ($getPaginator) {
    // $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
    // return new Paginator($dbSelector);
    // } else {
    // return $this->tableGateway->selectWith($select)->toArray();
    // }
    // }
    // public function getServiceById($servicesId)
    // {
    // $select = new Select('services');
    // $select->where->equalTo('id', $servicesId);
    // return $this->tableGateway->selectWith($select)->toArray();
    // }
    // public function getServicesChileById($servicesId)
    // {
    // $select = new Select('services');
    // $select->where->equalTo('parent_id', $servicesId);
    // return $this->tableGateway->selectWith($select)->toArray();
    // }
    // public function getAllServicesForList()
    // {
    // $select = new Select('services');
    // return $select;
    // }
    // public function getServicesByName($name)
    // {
    // $select = new Select('services');
    // $select->where->like('name', "%$name%");
    // return $this->tableGateway->selectWith($select)->toArray();
    // }
    
    // public function addServices($postData)
    // {
    // $this->tableGateway->insert($postData);
    // return $this->tableGateway->getLastInsertValue();
    // }
    
    // public function deleteServices($servicesId, $userId = null)
    // {
    // if ($servicesId > 0 && is_numeric($servicesId)) {
    // if ($userId !== null) {
    // return $this->tableGateway->delete(array(
    // 'id' => $servicesId,
    // 'post_author_user_id' => $userId
    // ));
    // } else {
    // return $this->tableGateway->delete(array(
    // 'id' => $servicesId
    // ));
    // }
    // }
    // }
}
