<?php
namespace PrintServices\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;

class ServicesTypesForm extends Form
{

    public function __construct()
    {
        parent::__construct('servicesTypesForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post'
        ));
        
        $name = new Element\Text('title');
        $name->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control validate[required]',
            'placeholder' => __('title'),
            'required' => 'required'
        ));
        $name->setLabel(__("title"));
        
        $status = new Element\Checkbox('status');
        $status->setAttributes(array(
            'id'    => 'status',
        ));
        $status->setLabel(__('active'));
        
        $officeStatus = new Element\Checkbox('office_status');
        $officeStatus->setAttributes(array(
            'id'    => 'office-status',
        ));
        $officeStatus->setLabel(__('active for offices'));
        
        $submit = new Element\Submit("submit");
        $submit->setValue("Save and Close");
        $submit->setAttribute("class", "btn btn-primary");

        $submit2 = new Element\Submit("submit2");
        $submit2->setValue("Save and New");
        $submit2->setAttribute("class", "btn btn-primary");
        
        $this->add($name);
        $this->add($status);
        $this->add($officeStatus);
        $this->add($submit);
        $this->add($submit2);
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        ))); 
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
