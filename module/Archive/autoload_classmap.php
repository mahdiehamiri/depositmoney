<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
    'Printservices\Module'                                      => __DIR__ . '/Module.php',
    'Printservices\Controller\AdminController'                  => __DIR__ . '/src/Printservices/Controller/AdminController.php',
    'Printservices\Controller\AjaxController'                   => __DIR__ . '/src/Printservices/Controller/AjaxController.php',  
    'Printservices\Controller\IndexController'                  => __DIR__ . '/src/Printservices/Controller/IndexController.php', 
    'Printservices\Model\PrintservicesTable'                         => __DIR__ . '/src/Printservices/Model/PrintservicesTable.php',
 );
