<?php

namespace Permission\Controller;

use Application\Helper\BaseAdminController;
use Permission\Model\PermissionAllPermissionsTable;
use Permission\Model\PermissionPermissionsTable;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Permission\Model\UsersTable;
use Permission\Model\UserGroupsTable;

class AjaxController extends BaseAdminController
{	
    
	public function onDispatch(MvcEvent $e)
	{
	    parent::onDispatch($e);
		$this->layout("layout/empty.phtml");
	}

	public function getPermissionsAction()
	{
	    $permissionPermissionsTable    = new PermissionPermissionsTable($this->getServiceLocator());
	    if ($this->userData->user_group_id == 1) {
	        $permissionAllPermissionsTable    = new PermissionAllPermissionsTable($this->getServiceLocator());
	        $allPermissionsData = $permissionAllPermissionsTable->getAllPermissionsAjax();
	        
	    }  else {
	        // all permissions
	        $permissionAllPermissionsTable    = new PermissionAllPermissionsTable($this->getServiceLocator());
	        $allPermissionsData = $permissionAllPermissionsTable->getAllPermissionsAjax();
	        
	        // group permissions
	        $groupPermissionsData = $permissionPermissionsTable->getPermissions($this->userData->user_group_id);
	        foreach ($allPermissionsData as $key => $value) {
	            $checkHasPermission = $permissionPermissionsTable->checkHasPermissions($this->userData->user_group_id, $value["name"]);
	            if (!$checkHasPermission) {
	                unset($allPermissionsData[$key]);
	            }
	        }
	    }
	    
	   // $allPermissionsData = $permissionAllPermissionsTable->getAllPermissionsAjax();
	    
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost()->toArray();
	        
	        $groupData = $this->checkPermission($this->userData->id, $data['group_id']);
	         
	        if (!$groupData) {
	          //  die("Pesaram sheitooni nakon!");
	            die("1");
	        }
	        
	        if ($data['group_id'] != '') {
	            $permissionsData = $permissionPermissionsTable->getPermissions($data['group_id']);
	        }
	        else {
	            die("error");
	        }
	        $permissions = array();
	        $newViewForPermission = array();
	        if($permissionsData)
	        {
	        foreach ($permissionsData as $k => $v)
	            {
	                $permissions[$k] = $v['perm_type'];
	            }
	        } 
	        if ($allPermissionsData)
	        {
	            foreach ($allPermissionsData as &$value) {
	                $name = explode('-', $value['name']);
	                
	                $value['title'] = $name[0];
	                $newViewForPermission[$value['title']][] = $value;
	            }
	           
	            $view["groups"] = $data['group_id'];
	            $view["permissionsData"] = $permissionsData;
	            $view["permissions"] = $permissions;
	            $view["allPermissionsData"] = $newViewForPermission; 
	            return new ViewModel($view);
	        }
	        else {
	           	die('error');
	        }
	    }
	    else
	        die('error');
	     
	}
	//daneshi added to prevent duplidate codes
	function checkPermission($userId, $checkGroupId = null, $includeMe = true, $returnIds = false)
	{
	    $usersTable = new UsersTable($this->getServiceLocator());
	    $ChildList = $usersTable->getUserChilds($userId);
	    if ($includeMe) {
	        $ChildList[] = $userId;
	    }
	    $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
	    $allGroupsData = $userGroupsTable->getAllGroups($ChildList);
	    $allGroups = array();
	    foreach ($allGroupsData as $value) {
	        $allGroups[] = $value["id"];
	    }
	
	    if ($returnIds) {
	        return $allGroups;
	    }
	    return in_array($checkGroupId, $allGroups);
	}
}