<?php
namespace Permission\Controller;

use Zend\View\Model\ViewModel;
use Permission\Form\PermissionForm;
use Zend\Session\Container;
use Permission\Model\PermissionPermissionsTable;
use Application\Helper\BaseAdminController;
use Permission\Model\PermissionDefaultPermissionsTable;
use Permission\Form\UserGroupsForm;
use Permission\Form\UsersGroupsForm;
use Permission\Model\UserGroupsTable;
use Permission\Model\UsersTable;
use Permission\Model\PermissionAllPermissionsTable;
use Zend\Db\Adapter\Adapter; 
use Permission\Model\PermissionAllPermissionsPersianTable;
use Permission\Form\UserGroupForm;   
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Formatter;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Column\Style;
use ZfcDatagrid\Filter;

class AdminController extends BaseAdminController
{ 
    public function updateAutoPermissionsAction()
    {
        $allPermissions = $this->generalhelper()->getDirContents(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "..");
        $permissionAllPermissionsTable = new PermissionAllPermissionsTable($this->getServiceLocator());
        $permissionAllPermissionsPersianTable = new PermissionAllPermissionsPersianTable($this->getServiceLocator());
        $permissionsDescriptions = $permissionAllPermissionsPersianTable->getRecords();
        foreach ($permissionsDescriptions as $value) {
            $permissionsDescriptionsArray[$value["perm_name"]] = $value["desc"];
        }
        $permissionAllPermissionsTable->deletePermission(array(
            "auto" => "1"
        ));
        if ($allPermissions) {
            rsort($allPermissions);
            foreach ($allPermissions as $permission) {
                $permissionData["name"] = $permission;
                if (isset($permissionsDescriptionsArray[$permissionData["name"]])) {
                    $permissionData["desc"] = $permissionsDescriptionsArray[$permissionData["name"]];
                }
                $isAdded = $permissionAllPermissionsTable->addPermission($permissionData);
            }
        }
        if ($isAdded) {
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
        } else {
            $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
        }
        return $this->redirect()->toRoute('permission-admin');
    }

    public function listPermissionsAction()
    {
        $this->setHeadTitle(__('Permissions list'));
        
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $permissionAllPermissionsTable = new PermissionAllPermissionsTable($this->getServiceLocator());
        $permissionAllPermissions = $permissionAllPermissionsTable->getAllPermissions();
        $grid->setTitle(__('Permissions List'));
        $grid->setDefaultItemsPerPage (30 );
        $grid->setDataSource($permissionAllPermissions, $dbAdapter);
        
        $col = new Column\Select('id', "permission_all_permissions");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Title'));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('desc');
        $col->setLabel(__('Description '));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('auto');
        $col->setLabel(__('Auto'));
        $col->setWidth(10);
        $options = array(
            '0' => 'Manual',
            '1' => 'Auto'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            '0' => 'Manual',
            '1' => 'Auto'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink("/" . $this->lang . '/admin-permission/edit-permission/' . $rowId);
        $viewAction->setTooltipTitle(__('Edit permission'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-permission/delete-permission/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete permission'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $link[] = '<a href=' . "/" . $this->lang . '/admin-permission/update-auto-permissions class="btn btn-primary"><i class="fa fa-refresh" aria-hidden="true"></i>' . __("Update Permission") . '</a>';
        $link[] = '<a href=' . "/" . $this->lang . '/admin-permission/add-permission class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add Permission") . '</a>';
        $link[] = '<a href=' . "/" . $this->lang . '/admin-permission/edit-users-groups-permissions class="btn btn-success"><i class="fa fa-user-plus" aria-hidden="true"></i>' . __("Add User Permission") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
     
        $grid->setLink($link); 
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addPermissionAction()
    {
        $this->setHeadTitle(__('Add permission'));
        $permissionForm = new PermissionForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $permissionForm->setData($data);
            if ($permissionForm->isValid()) {
                $validData = $permissionForm->getData();
                $permissionAllPermissionsTable = new PermissionAllPermissionsTable($this->getServiceLocator());
                $isAdded = $permissionAllPermissionsTable->addPermission($validData, 0);
                if ($isAdded) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toRoute('permission-admin');
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            }
        }
        $view['permissionForm'] = $permissionForm;
        return new ViewModel($view);
    }

    public function editPermissionAction()
    {
        $this->setHeadTitle(__('Edit permission'));
        $permissionForm = new PermissionForm();
        $permissionId = $this->params('id', - 1);
        $permissionAllPermissionsTable = new PermissionAllPermissionsTable($this->getServiceLocator());
        $permissionData = $permissionAllPermissionsTable->getPermissionById($permissionId);
        if (! $permissionData) {
            return $this->redirect()->toRoute('permission-admin');
        }
        $permissionForm->populateValues($permissionData[0]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $permissionForm->setData($data);
            if ($permissionForm->isValid()) {
                $validData = $permissionForm->getData();
                $isUpdated = $permissionAllPermissionsTable->editPermission($validData, $permissionId);
                if ($isUpdated !== false) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toRoute('permission-admin');
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            }
        }
        $view['permissionForm'] = $permissionForm;
        return new ViewModel($view);
    }

    public function deletePermissionAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $permissionId = $this->params('id', - 1);
        $permissionAllPermissionsTable = new PermissionAllPermissionsTable($this->getServiceLocator());
        $permissionData = $permissionAllPermissionsTable->getPermissionById($permissionId);
        if (! $permissionData) {
            return $this->redirect()->toRoute('permission-admin');
        }
        if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = $permissionAllPermissionsTable->deletePermission(array(
                'id' => $permissionId
            ));
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            }
        }
        return $this->redirect()->toRoute('permission-admin');
    }

    public function editUsersGroupsPermissionsAction()
    {
        $this->setHeadTitle(__('Manage permissions'));
        $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
        $groupsList = $this->checkPermission($this->userData->id, false, true, true);
        $groupsData = $userGroupsTable->getAllGroupsByIds($groupsList, true);
        
        $usersGroupsFrom = new UsersGroupsForm($groupsData);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $permissionPermissionsTable = new PermissionPermissionsTable($this->getServiceLocator());
            $permissionsData = $request->getPost();
            
            $groupData = $this->checkPermission($this->userData->id, $permissionsData['group_id']);
            
            if (! $groupData) {
                die("Please choose an item.");
               // $this->layout()->errorMessage = __("Please choose an item.");
            }
            
            $isDeleted = $permissionPermissionsTable->deletePermissions($permissionsData['group_id']);
            
            $perms = array();
            
            if ($isDeleted !== false && isset($permissionsData['chk'])) {
                foreach ($permissionsData['chk'] as $k => $v) {
                    $perms[] = "'$k'";
                    if ($this->userData->user_group_id == 1) {
                        $checkHasPermission = true;
                    } else {
                        $checkHasPermission = $permissionPermissionsTable->checkHasPermissions($this->userData->user_group_id, $k);
                    }
                    
                    if ($permissionsData['group_id'] != '' && $checkHasPermission) {
                        $isAdded = $permissionPermissionsTable->addPermission($permissionsData['group_id'], $k);
                    } else {
                        die("Pesaram sheitooni nakon! Barikallah");
                    } 
                }
                
                // tamame bachehash ro bayad negah konim va age kasi access bishtari dasht ro hazf konim
                $usersTable = new UsersTable($this->getServiceLocator());
                $userData = $usersTable->getUserByUserGroup($permissionsData['group_id']);
                $groupsList = $this->checkPermission($this->userData->id, false, false, true);
                if ($groupsList) {
                    $permissionPermissionsTable->deleteChildPermissions($groupsList, $perms);
                }
                
                // ---------------------------------------------------------------------------------------
                
                $this->layout()->successMessage = __("Operation done successfully.");
            }
        }
        $view["usersGroupsForm"] = $usersGroupsFrom;
        return new ViewModel($view);
    }
    
    // CRUD Group
    public function listUserGroupsAction()
    {
        $this->setHeadTitle(__('User Groups list'));
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
        
        $groupsList = $this->checkPermission($this->userData->id, false, true, true);

        $groupsData = $userGroupsTable->getAllGroupsByIds($groupsList);
        
        $grid->setTitle(__('User Groups List'));
        $grid->setDefaultItemsPerPage (30 );
        $grid->setDataSource($groupsData, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(__('Title'));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('parent_id');
        $col->setLabel(__('parent_id '));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink("/" . $this->lang . '/admin-permission/edit-user-group/' . $rowId);
        $viewAction->setTooltipTitle(__('Edit user group'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-permission/delete-user-group/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete user group'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $link[] = '<a href=' . "/" . $this->lang . '/admin-permission/add-user-group class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($link);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addUserGroupAction()
    {
        $this->setHeadTitle(__('Add user group'));
        $userGroupForm = new UserGroupForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $userGroupForm->setData($data);
            if ($userGroupForm->isValid()) {
                $validData = $userGroupForm->getData();
                $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
                
                try {
                    $isAdded = $userGroupsTable->addGroup($validData, $this->userData->id);
                    if ($isAdded) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toRoute('permission-admin', array(
                            "action" => "list-user-groups"
                        ));
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("Duplicate name!");
                }
            }
        }
        $view['userGroupForm'] = $userGroupForm;
        return new ViewModel($view);
    }

    public function editUserGroupAction()
    {
        $this->setHeadTitle(__('Edit permission'));
        $userGroupForm = new UserGroupForm();
        $groupId = $this->params('id', - 1);
        $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
        $groupData = $this->checkPermission($this->userData->id, $groupId);
        if (! $groupData) {
            return $this->redirect()->toRoute('permission-admin', array(
                "action" => "list-user-groups"
            ));
        } else {
            $groupData = $userGroupsTable->getGroupById($groupId);
        }
        $userGroupForm->populateValues($groupData[0]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $userGroupForm->setData($data);
            if ($userGroupForm->isValid()) {
                $validData = $userGroupForm->getData();
                try {
                    $isUpdated = $userGroupsTable->editGroup($validData, $groupId);
                    if ($isUpdated !== false) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toRoute('permission-admin', array(
                            "action" => "list-user-groups"
                        ));
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("Duplicate name!");
                }
            }
        }
        $view['userGroupForm'] = $userGroupForm;
        return new ViewModel($view);
    }

    public function deleteUserGroupAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $groupId = $this->params('id', - 1);
        $checkPermission = $this->CheckPermission($this->userData->id, $groupId);
        $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
        $groupData = $this->checkPermission($this->userData->id, $groupId);
        if (! $groupData) {
            return $this->redirect()->toRoute('permission-admin', array(
                "action" => "list-user-groups"
            ));
        }
        if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = $userGroupsTable->deleteGroup(array(
                'id' => $groupId
            ));
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            }
        }
        return $this->redirect()->toRoute('permission-admin', array(
            "action" => "list-user-groups"
        ));
    }
    
    // daneshi added to prevent duplidate codes
    function checkPermission($userId, $checkGroupId = null, $includeMe = true, $returnIds = false)
    {
        $usersTable = new UsersTable($this->getServiceLocator());
        $ChildList = $usersTable->getUserChilds($userId);
        if ($includeMe) {
            $ChildList[] = $userId;
        }
        $userGroupsTable = new UserGroupsTable($this->getServiceLocator());
        $allGroupsData = $userGroupsTable->getAllGroups($ChildList);
        $allGroups = array();
        foreach ($allGroupsData as $value) {
            $allGroups[] = $value["id"];
        }
        
        if ($returnIds) {
            return $allGroups;
        }
        return in_array($checkGroupId, $allGroups);
    }
}
