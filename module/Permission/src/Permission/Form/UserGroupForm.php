<?php
namespace Permission\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class UserGroupForm extends Form
{

    public function __construct()
    {
        parent::__construct('userGroupsForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'id' => 'user_group'
        ));
        
        $title = new Element\Text('title');
        $title->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control',
            'required' => 'required',
            'placeholder' => __("Group name")
        ));
        $title->setLabel(__("Group name"));
        
        $time = new Element\Text('credit_time');
        $time->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control',
            'placeholder' => __("For example: 12, Empty fields mean unlimited package")
        ));
        $time->setLabel(__("Credit time pre month"));
        
        $is_package = new Element\Checkbox('is_package');
        $is_package->setAttributes(array(
            'id' => 'package'
        ));
        $is_package->setLabel(__("Is this group a package?"));
        
        $price = new Element\Text('price');
        $price->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control',
//             'required' => 'required',
            'placeholder' => __("For example: 550000")
        ));
        $price->setLabel(__("Package price"));
        
        $percentage = new Element\Text('percentage');
        $percentage->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control',
            'placeholder' => __("For example: 1")
        ));
        $percentage->setLabel(__("Percentage"));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__("Save"));
        $submit->setAttributes(array(
            'id' => 'submit',
            'class' => 'btn btn-primary'
        ));
        
        $this->add($title)
            ->add($time)
            ->add($is_package)
            ->add($price)
            ->add($percentage)
            ->add($csrf)
            ->add($submit);
        $this->inputFilters();
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        
        $inputFilter->add($factory->createInput(array(
            'name' => 'price',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        
        $inputFilter->add($factory->createInput(array(
            'name' => 'percentage',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
