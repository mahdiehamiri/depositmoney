<?php

namespace Permission\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class PermissionForm extends Form
{
	public function __construct()
	{
		parent::__construct('permissionForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		
		$permName = new Element\Text('name');
		$permName->setAttributes(array(
				'id'    => 'perm_name',
				'class' => 'form-control',
		        'placeholder'  => __("Permission name")
		));
		$permName->setLabel(__("Permission name"));
		
		$permDesc = new Element\Text('desc');
		$permDesc->setAttributes(array(
		    'id'    => 'perm_desc',
		    'class' => 'form-control',
		    'placeholder'  => __("Permission description")
		));
		$permDesc->setLabel(__("Permission description"));
		
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save"));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		
		$this->add($permName)
			 ->add($permDesc)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
	    
	}

}
