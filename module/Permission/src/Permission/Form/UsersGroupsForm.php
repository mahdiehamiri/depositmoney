<?php

namespace Permission\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class UsersGroupsForm extends Form
{
	public function __construct($groupsList = null, $usersList = null)
	{
		parent::__construct('permissionForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		$groupId = new Element\Select('group_id');
		$groupId->setAttributes(array(
		    'id' => 'groups',
		    'class' => 'form-control',
		));
		$groups = array("" => __("Please choose an item"));
		if ($groupsList) {
			foreach ($groupsList as $userGroup) {
				$groups[$userGroup['id']] = $userGroup['title'];
			}
		}
		$groupId->setValueOptions($groups);
		$groupId->setLabel(__("Choose a group"));
		
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Button('submit');
		$submit->setLabel(__("Continue"));
		
		$submit->setAttributes(array(
				'id'    => 'submitusergroup',
				'class' => 'btn btn-primary fa fa-arrow-left'
		));
		
		$this->add($groupId)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
	    
	}

}
