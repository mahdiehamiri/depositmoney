<?php
namespace Permission\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class UserGroupsTable extends BaseModel
{

    protected $tableGateway;

    protected $adapter;

    protected $serviceManager;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('user_groups', $this->getDbAdapter());
    }

    public function getRecords($level = false, $restaurantId = null)
    {
        $select = new Select('user_groups');
        if ($level) {
            $select = new Select('user_groups');
            if ($restaurantId) {
                $select->where->equalTo("restaurant_id", $restaurantId);
            }
            $select->where->lessThan("level", $level);
            return $this->tableGateway->selectWith($select)->toArray();
        }
        return $this->tableGateway->select()->toArray();
    }

    public function getGroupLevel($groupId = false, $restaurantId = null)
    {
        $select = new Select('user_groups');
        if ($restaurantId) {
            $select->where->equalTo("restaurant_id", $restaurantId);
        }
        $select->where->equalTo("id", $groupId);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
    // ------------------------------------------
    public function getGroupById($groupId)
    {
        $select = new Select('user_groups');
        $select->where->equalTo('id', $groupId);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getAllGroups($allIds = null)
    {
        $select = new Select('user_groups');
        if ($allIds) {
            $select->where->in("parent_id", $allIds);
        } else {
            $select->where->equalTo("id", - 1);
        }
        
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getAllGroupsByIds($allIds = null, $returnArray = false)
    {
        $select = new Select('user_groups');
        if ($allIds) {
            $select->where->in("id", $allIds);
        } else {
            $select->where->equalTo("id", - 1);
        }
        if ($returnArray) {
            return $this->tableGateway->selectWith($select)->toArray();
        }
        return $select;
    }

    public function getGroupChilds($userId)
    {
        $ALLDATA = array();
        $Cond = '';
        $Tree = $this->tableGateway->select(array(
            'parent_id = ?' => $userId
        ));
        $Tree = $this->adapter->query("
    			SELECT id FROM user_groups
    	 	            WHERE parent_id = ?  AND id <> ?   " . $Cond, array(
            $userId,
            $userId
        ));
        
        $myData = array();
        if ($Tree) {
            $IDS = false;
            foreach ($Tree as $key => $value) {
                if (isset($value['id'])) {
                    $myData[] = $value['id'];
                }
            }
            $ALLDATA[] = $IDS = implode(",", $myData);
        }
        $myData = null;
        if ($IDS) {
            do {
                $statement = $this->adapter->query("
             			SELECT id  FROM user_groups
             	 	            WHERE parent_id IN (" . $IDS . ")");
                $results = $statement->execute();
                if ($results) {
                    $IDS = false;
                    $myData = null;
                    foreach ($results as $key => $value) {
                        if (isset($value['id'])) {
                            $myData[] = $value['id'];
                        }
                    }
                    if ($myData) {
                        $ALLDATA[] = $IDS = implode(",", $myData);
                    }
                } else
                    $IDS = false;
            } while ($IDS);
        }
        
        $ChildList = array();
        if ($ALLDATA) {
            foreach ($ALLDATA as $key => $Child) {
                $ChildList = array_merge($ChildList, explode(",", $Child));
            }
        }
        
        return $ALLDATA;
    }

    public function addGroup($data = null, $userId = null)
    {
        $insertData = array(
            "title" => $data["title"],
            "parent_id" => $userId,
            "credit_time" => $data["credit_time"],
            "price" => $data["price"],
            "percentage" => $data["percentage"],
            "is_package" => $data["is_package"]
        );
        return $this->tableGateway->insert($insertData);
    }

    public function editGroup($data, $groupId)
    {
        $editData = array(
            "title" => $data["title"],
            "credit_time" => $data["credit_time"],
            "price" => $data["price"],
            "percentage" => $data["percentage"],
            "is_package" => $data["is_package"]
        );
        return $this->tableGateway->update($editData, array(
            'id' => $groupId
        ));
    }

    public function deleteGroup($where = null)
    {
        return $this->tableGateway->delete($where);
    }
}