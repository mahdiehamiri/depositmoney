<?php
namespace Permission\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Profiler\Profiler;
use Zend\Validator\LessThan;

class UsersTable extends BaseModel
{

    protected $tableGateway;

    protected $adapter;

    protected $serviceManager;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('users', $this->getDbAdapter());
    }

    public function getUserByUserGroup($userId)
    {
        return $this->tableGateway->select(array(
            "user_creator" => $userId
        ));
    }

    public function GetUserChilds($userId)
    {
        $ALLDATA = array();
        $Cond = '';
        $Tree = $this->tableGateway->select(array(
            'user_creator = ?' => $userId
        ));
        $Tree = $this->adapter->query("
    			SELECT id FROM users
    	 	            WHERE user_creator = ?  AND id <> ?   " . $Cond, array(
            $userId,
            $userId
        ));
        
        $myData = array();
        if ($Tree) {
            $IDS = false;
            foreach ($Tree as $key => $value) {
                if (isset($value['id'])) {
                    $myData[] = $value['id'];
                }
            }
            $ALLDATA[] = $IDS = implode(",", $myData);
        }
        $myData = null;
        if ($IDS) {
            do {
                $statement = $this->adapter->query("
             			SELECT id  FROM users
             	 	            WHERE user_creator IN (" . $IDS . ")");
                $results = $statement->execute();
                if ($results) {
                    $IDS = false;
                    $myData = null;
                    foreach ($results as $key => $value) {
                        if (isset($value['id'])) {
                            $myData[] = $value['id'];
                        }
                    }
                    if ($myData) {
                        $ALLDATA[] = $IDS = implode(",", $myData);
                    }
                } else
                    $IDS = false;
            } while ($IDS);
        }
        
        $ChildList = array();
        if ($ALLDATA) {
            foreach ($ALLDATA as $key => $Child) {
                $ChildList = array_merge($ChildList, explode(",", $Child));
            }
        }
        
        return $ALLDATA;
    }
}