<?php 
namespace Permission\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class PermissionAllPermissionsTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('permission_all_permissions', $this->getDbAdapter());
    }
    
    public function getAllPermissions()
    {
        $select = new Select('permission_all_permissions');
	    $select->order(array("order","name"));
	    return $select;
    }   
    public function getAllPermissionsAjax()
    {
        $select = new Select('permission_all_permissions');
        $select->order(array("order","name"));
        return $this->tableGateway->selectWith($select)->toArray();
    } 
    
    public function getPermissionById($permissionId)
    {
        $select = new Select('permission_all_permissions');
        $select->where->equalTo('id', $permissionId );
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
    public function addPermission($data = null, $auto = 1)
    {
        if (!isset($data["desc"])) {
            $data["desc"] = $data["name"];
        } 
        $insertData = array(
            "name" => $data["name"],
            "desc" => $data["desc"],
            "auto" => $auto,
        );
        return $this->tableGateway->insert($insertData);
    }
    public function editPermission($data, $permissionId)
    {
        if (!$data["desc"]) {
            $data["desc"] = $data["name"];
        }
        return $this->tableGateway->update(array(
            "name" => $data["name"],
            "desc" => $data["desc"],
        ), array('id' => $permissionId));
    }
    
    public function deletePermission($where = null)
    {
        return $this->tableGateway->delete($where);
    }
	
	
}
