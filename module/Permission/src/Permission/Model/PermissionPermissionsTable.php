<?php 
namespace Permission\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Predicate\NotIn;

class PermissionPermissionsTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('permission_permissions', $this->getDbAdapter());
    }
	public function GetAccess( $user=null)
	{
        $result=$this->fetchAll(array("type = 'group'","type_id = ?"=>$user->group_id ))->toArray();
        if ($result)
        {
            foreach ($result as $k=>$v)
            {
                $perm_types[0][]=$v['perm_type'];
            }

            return $perm_types;
        }
        else
            return false;
	}
	
	public function getPermissions($groupId = null)
	{
	    $select = new Select('permission_permissions');
	    $where = new Where();
	    if ($groupId) {
	        $where->equalTo("type", "group");
	        $where->equalTo("type_id", $groupId);
	    }
	    $select->order("perm_type DESC");
	    $select->where($where);
	    return $this->tableGateway->selectWith($select)->toArray();
	
	}
	
	public function checkHasPermissions($groupId = null, $permName ='')
	{
	    $select = new Select('permission_permissions');
	    $where = new Where();
	    if ($groupId) {
	        $where->equalTo("type", "group");
	        $where->equalTo("type_id", $groupId);
	    }
	    if ($permName) {
	        $where->equalTo("perm_type", $permName);
	    }
	    $select->where($where);
	    return $this->tableGateway->selectWith($select)->toArray();
	
	}
	
	public function deleteChildPermissions($groupIds = null, $perms = null)
	{
	    $select = new Select('permission_permissions');
	    $where = new Where();
        $where->equalTo("type", "group");
        $where->in("type_id", $groupIds);
        $perms = implode(",", $perms);
        $where->expression("perm_type NOT IN ($perms)", array());

	    return $this->tableGateway->delete($where);
	}
	
	public function deletePermissions($groupId = null)
	{
	    $where = new Where();
	    if ($groupId) {
	        $where->equalTo("type", "group");
	        $where->equalTo("type_id", $groupId);
	    }
	    return $this->tableGateway->delete($where);
	
	}
	public function addPermission($groupId = null, $termType = null)
	{
	    if ($groupId) {
	        $type = "group";
	        $typeId = $groupId;
	    }
	    $insertData = array(
	        "type" => $type,
	        "type_id" => $typeId,
	        "perm_type" => $termType
	    );

	    return $this->tableGateway->insert($insertData);
	}
	
	
}
