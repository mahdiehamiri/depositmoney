<?php
return array(
    'router' => array(
        'routes' => array(
            'permission-admin' => array (
                'type' => 'segment',
                'options' => array (
                    'route' => '[/:lang]/admin-permission[/:action][/:id][/page/:page][/delete/:delete][/clear/:clear][/token/:token][/count/:count]',
                    'constraints' => array (
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'lang' => '[a-zA-Z]{2}',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                        'delete' => '[0-9]+',
                        'clear' => 'true',
                        'count' => '[0-9]+',
                        'token' => '[a-f0-9]+',
            
                    ),
                    'defaults' => array (
                        '__NAMESPACE__' => 'Permission\Controller',
                        'controller' => 'Admin',
                        'action' => 'list-permissions'
                    )
                )
            ),
            'permission-admin-ajax' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-permission-ajax',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Permission\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'get-permissions'
                    ),
                )
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            include_once(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);