<?php 
namespace Language\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class LanguageMainPhrasesTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('language_main_phrases', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllPhrases($where = null)
	{
	    $select = new Select('language_main_phrases');
	    $select->order("phrase");
	    $select->where($where);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getPhraseById($phraseId)
	{
	    $select = new Select('language_main_phrases');
	    $select->where->equalTo('id', $phraseId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addPhrase($phrase)
	{
		$this->tableGateway->insert(array(
				'phrase' 	=> $phrase,
		));
		return $this->tableGateway->getLastInsertValue();
	}
	public function editPhrase($phrase, $phraseId)
	{
	    return $this->tableGateway->update(array(
	        'phrase' 	=> $phrase,
	    ), array('id' => $phraseId));
	}
	
	public function deletePhrases($where = null)
	{
	    return $this->tableGateway->delete($where);
	}
	
}
