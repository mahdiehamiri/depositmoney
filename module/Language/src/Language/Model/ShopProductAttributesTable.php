<?php 

namespace Language\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class ShopProductAttributesTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_product_attributes', $this->getDbAdapter());
	}
	
	
	
	public function fetchAll($attributeId = null)
	{
	    $select = new Select('shop_product_attributes');
	    $select->columns(array("title_en"));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	
}
