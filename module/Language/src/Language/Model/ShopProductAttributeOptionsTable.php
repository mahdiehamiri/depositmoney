<?php 

namespace Language\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ShopProductAttributeOptionsTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_product_attribute_options', $this->getDbAdapter());
	}
	
	public function fetchAll()
	{
	    $select = new Select('shop_product_attribute_options');
	    $select->columns(array("label"));
	    $select->where->notEqualTo("label", "");
	    return $this->tableGateway->selectWith($select)->toArray();
	}

}
