<?php 
namespace Language\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class LanguageLanguagesPhrasesTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('language_languages_phrases', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	 
	public function getPhrases($languageId = null)
	{ 
	    $select = new Select('language_languages_phrases');
	    $where = new Where();
	    if ($languageId) {
	        $where->equalTo("language_id", (int)$languageId);
	        $where->equalTo("status", 1);
	    }
	    $select->where($where);
	    
	    return $this->tableGateway->selectWith($select)->toArray();
	
	}
	public function getPhrasesByPhrase($phrase, $languageId)
	{ 
	    $select = new Select('language_languages_phrases');
	    $where = new Where();
	    if ($languageId && $phrase) {
	        $where->equalTo("language_id", (int)$languageId);
	        $where->equalTo("phrase", $phrase);
	    }
	    $select->where($where);
	    
	    return $this->tableGateway->selectWith($select)->current();
	
	}
	public function deletePhraseNew($phrase, $languageId = null)
	{
     /* $where = new Where();
	    if ($languageId && $phrase) {
	        $where->equalTo("language_id", $languageId);
	        $where->equalTo("phrase", $phrase);
	    } 
	    return $this->tableGateway->delete($where); */
	    return $this->tableGateway->update(array(
	    		'status'    => 0,
	    ), array('language_id' => $languageId,"phrase"=> $phrase));  
	}
	public function deletePhrases($languageId = null)
	{
	    $where = new Where();
	    if ($languageId) {
	        $where->equalTo("language_id", $languageId);
	    }
	    return $this->tableGateway->delete($where);
	}
	public function deletePhrase($phrase, $languageId = null)
	{
	    $where = new Where();
	    if ($languageId) {
	        $where->equalTo("language_id", $languageId);
	    }
	    if ($phrase) {
	        $where->equalTo("phrase", $phrase);
	    }
	   return $this->tableGateway->delete($where); 
	}
 
	public function addPhrase($languageId = null, $phrase = null, $translate = null)
	{
	    $insertData = array(
	        "language_id" => $languageId,
	        "phrase" => $phrase,
	        "translate" => $translate,
	    );
	
	    return $this->tableGateway->insert($insertData);
	}
	
	public function GetAccess( $user=null)
	{
	    $result=$this->fetchAll(array("type = 'user'","type_id = ?"=>$user->id ))->toArray();
	
	    if ($result)
	    {
	        foreach ($result as $k=>$v)
	        {
	            $perm_types[0][]=$v['perm_type'];
	        }
	
	        return $perm_types;
	    }
	    else
	    {
	        $result=$this->fetchAll(array("type = 'group'","type_id = ?"=>$user->group_id ))->toArray();
	        if ($result)
	        {
	            foreach ($result as $k=>$v)
	            {
	                $perm_types[0][]=$v['perm_type'];
	            }
	
	            return $perm_types;
	        }
	        else
	            return false;
	    }
	
	
	}
	
	
	
	
	
	
	
	
	
	

}
