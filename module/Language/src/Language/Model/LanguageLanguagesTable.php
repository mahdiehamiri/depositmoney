<?php 
namespace Language\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class LanguageLanguagesTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('language_languages', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllLanguages($where = null)
	{
	    $select = new Select('language_languages');
	    $select->order("default DESC");
	    $select->where($where);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getAllLanguagesGrid($where = null)
	{
	    $select = new Select('language_languages');
	    $select->order("default DESC");
	    $select->where($where);
	    //return $this->tableGateway->selectWith($select)->toArray();
	    return $select;
	}
	
	public function getLanguageById($languageId)
	{
	    $select = new Select('language_languages');
	    $select->where->equalTo('id', $languageId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addLanguage($languageData)
	{
	    if ((int)$languageData['default'] == 1) {
	        $this->tableGateway->update(array('default' => "0"));
	    }
		$this->tableGateway->insert(array(
				'title' 	=> $languageData['title'],
				'code' 		=> strtolower($languageData['code']),
		        'direction' 		=> strtolower($languageData['direction']),
		        'enable' 	=> (int)$languageData['enable'],
		        'default'   => (int)$languageData['default']
		));
		
		return $this->tableGateway->getLastInsertValue();
	}
	public function editLanguage($newData, $languageId)
	{
	    if ((int)$newData['default'] == 1) {
	        $this->tableGateway->update(array('default' => "0"));
	    }
	    return $this->tableGateway->update(array(
	        'title'    => $newData['title'],
			'code'     => strtolower($newData['code']),
			'enable'   => $newData['enable'],
	        'direction'   => $newData['direction'],
			'default'  => $newData['default'],
	    ), array('id' => $languageId));
	}
	
	public function deleteLanguage($languageId)
	{
	    return $this->tableGateway->delete(array(
	        'id' => $languageId
	    ));
	}
	public function getDefaultLanguage()
	{
		$select = new Select('language_languages'); 
		$select->where->equalTo('default', 1);
		return $this->tableGateway->selectWith($select)->current();
	}
}
