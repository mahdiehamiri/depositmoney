<?php

namespace Language\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class LanguagePhraseForm extends Form
{
	public function __construct($languagesList = null)
	{
		parent::__construct('permissionForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		
		$languageId = new Element\Select('language_id');
		$languageId->setAttributes(array(
		    'id' => 'language_id',
		    'class' => 'form-control',
		));
		$language = array("" => __("Please choose an item"));
		if ($languagesList) {
		    foreach ($languagesList as $language) {
		        $users[$language['id']] = $language['title'];
		    }
		}
		$languageId->setValueOptions($users);
		$languageId->setLabel(__("Choose language"));
		
		
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Button('submit');
		$submit->setLabel(__("Continue"));
		$submit->setAttributes(array(
				'id'    => 'submitlanguagephrase',
				'class' => 'btn btn-primary fa fa-arrow-left'
		));
		
		$this->add($languageId)
			 ->add($csrf)
			 ->add($submit);
		
	}	

}
