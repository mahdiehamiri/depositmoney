<?php

namespace Language\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class LanguageForm extends Form
{
	public function __construct()
	{
		parent::__construct('languageForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
		        'novalidate'=> true,
		        'id'   => 'languageForm'
		));
		
		$title = new Element\Text('title');
		$title->setAttributes(array(
				'id'    => 'title',
				'class' => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder'  => __("Language")
		));
		$title->setLabel(__("Language"));
		
		$code = new Element\Text('code');
		$code->setAttributes(array(
				'id'    => 'code',
				'class' => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder'  => "en"
		));
		$code->setLabel(__("Language code"));
	
		$enable = new Element\Checkbox('enable');
		$enable->setAttributes(array(
				'id'    => 'enable',
				'class' => 'form-dontrol'
		));
		$enable->setLabel(__("Active/Deactive"));
		
		$default = new Element\Checkbox('default');
		$default->setAttributes(array(
		    'id'    => 'default',
		    'class' => 'form-dontrol'
		));
		$default->setLabel(__("Default language"));
		
		
		$direction = new Element\Radio('direction');
		$direction->setAttributes(array(
		    'id'    => 'direction',
		    'class' => 'validate[required]',
		    'required' => 'required'
		));
		$direction->setValueOptions(array(
		    'ltr' => 'LTR',
		    'rtl' => 'RTL',
		));
		$direction->setLabel(__("Direction"));
		
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save and close"));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary fa fa-floppy-o'
		));
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(__("Save and New"));
		$submit2->setAttributes(array(
				'id'    => 'submit2',
				'class' => 'btn btn-primary fa fa-plus'
		));
		
		$this->add($title)
			 ->add($code)
			 ->add($enable)
			 ->add($default)
			 ->add($direction)
			 ->add($csrf)
			 ->add($submit)
			 ->add($submit2);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'title',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'code',
				'required' => false,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}

}
