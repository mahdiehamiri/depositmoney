<?php

namespace Language\Controller;

use Application\Helper\BaseAdminController;
use Permission\Model\PermissionAllPermissionsTable;
use Permission\Model\PermissionPermissionsTable;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Language\Model\LanguageMainPhrasesTable;
use Language\Model\LanguageLanguagesPhrasesTable;
use Language\Model\LanguageLanguagesTable;

class AjaxController extends BaseAdminController
{	
    
	public function onDispatch(MvcEvent $e)
	{
	    parent::onDispatch($e);
		$this->layout("layout/empty.phtml");
	}

	public function getDefaultAction()
	{
 
	    $languageLanguageTable    = new LanguageLanguagesTable($this->getServiceLocator());
	    $languageLanguageDefault = $languageLanguageTable->getDefaultLanguage();
	    if ($languageLanguageDefault)
	    {
	    	echo json_encode($languageLanguageDefault);die;
	    } else {
	    	die (false);
	    }
	}
	public function getPhrasesAction()
	{ 
	    $languageMainPhrasesTable    = new LanguageMainPhrasesTable($this->getServiceLocator());
	    $languageLanguagesPhrasesTable    = new LanguageLanguagesPhrasesTable($this->getServiceLocator());
	
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost()->toArray();
	        if ($data['language_id'] != '') {
	            $phrasesData = $languageLanguagesPhrasesTable->getPhrases($data['language_id']);
	        }  else {
	               die("error");
	        }
	        $phrases = array();
	        if($phrasesData)
	        {
	            foreach ($phrasesData as $k => $v)
	            {
	                $phrases[$v['phrase']] = $v['translate'];
	            }
	        }
	        $allPhrasesData = $languageMainPhrasesTable->getAllPhrases(); 
	        if ($allPhrasesData)
	        {
	            $view["language_id"] = $data['language_id'];
	            $view["phrases"] = $phrases;
	            $view["allPhrasesData"] = $allPhrasesData; 
	            return new ViewModel($view);
	        }
	        else {
	           	die('error');
	        }
	    }
	    else
	        die('error');
	     
	}
}