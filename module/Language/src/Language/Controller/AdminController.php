<?php 
namespace Language\Controller;

use Zend\View\Model\ViewModel;
use Language\Form\LanguageForm;
use Zend\Session\Container;
use Language\Model\LanguageLanguagesTable;
use Application\Helper\BaseAdminController;
use Language\Model\LanguageMainPhrasesTable;
use Language\Model\LanguageLanguagesPhrasesTable;
use Language\Form\LanguagePhraseForm;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Language\Model\ShopProductAttributesTable;
use Language\Model\ShopProductAttributeOptionsTable;

class AdminController extends BaseAdminController
{	
	public function damagePhraseAction() {
		
		$languageLanguageLanguagesTable = new LanguageLanguagesTable ( $this->getServiceLocator () );
		$allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages ( array (
				"enable" => "1"
		));
		$languageLanguagesPhrasesTable = new LanguageLanguagesPhrasesTable ( $this->getServiceLocator () );
		$languageData = $languageLanguageLanguagesTable->getLanguageById ( 3 );
		$languageFileName =  $languageData [0] ["code"] . ".php";
		$fullPath = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "language" . DIRECTORY_SEPARATOR;
		$languageFile = fopen ( $fullPath . $languageFileName, "r" ) or die ( "Unable to open file!" );
	
		$handle =$languageFile;
		if ($handle) {
			while (($buffer = fgets($handle, 4096)) !== false) {
					
				echo $buffer."</br>";
				$mystring = explode('=>',$buffer);
				if (isset($mystring[1])) {
					$phrase = $mystring[0];
					$phrase =  str_replace('"', '', $phrase);
					$translate = explode(',',$mystring[1]);
					$isDeleted = $languageLanguagesPhrasesTable->deletePhrase ( $phrase, 3 );
					$translate=  str_replace('"', '', $translate[0]);
						
					$isAdded = $languageLanguagesPhrasesTable->addPhrase ( 3, $phrase, $translate );
				}
			}
	
			fclose($handle);
		}
		die("FINISHED");
	}
	
	public function managePhrasesAction()
	{
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes
		 ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        $this->setHeadTitle(__('Languages phrases management'));
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
        
        $languagePhraseForm = new LanguagePhraseForm($allActiveLanguages);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $languageLanguagesPhrasesTable    = new LanguageLanguagesPhrasesTable($this->getServiceLocator());
            $phraseData = $request->getPost();
            if(isset($phraseData['phrase']))
            {
                foreach ($phraseData['phrase'] as $phrase => $translate)
                {
                 
                    if ($translate =='') {
                        continue;
                    } 
                    $isDeleted = $languageLanguagesPhrasesTable->getPhrasesByPhrase($phrase, $phraseData['language_id']); 
                  
                   $isDeleted = $languageLanguagesPhrasesTable->deletePhraseNew($phrase, $phraseData['language_id']);  
                   $isAdded = $languageLanguagesPhrasesTable->addPhrase($phraseData['language_id'], $phrase, $translate);
                }
           
            }
    
            $db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $sql = "DELETE FROM language_languages_phrases WHERE phrase NOT IN (SELECT phrase FROM language_main_phrases)";
            $statement = $db->query($sql);
            $res =  $statement->execute();
       
                $languageData = $languageLanguageLanguagesTable->getLanguageById($phraseData['language_id']);
                $languageFileName = "old-" . $languageData[0]["code"] . ".php";
                $fullPath = __DIR__ .  DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." .  DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR  . "language" . DIRECTORY_SEPARATOR;
                $languageFile = fopen( $fullPath .  $languageFileName , "w") or die("Unable to open file!");
                
                $content = "<?php return array (\n";
                $phrasesData = $languageLanguagesPhrasesTable->getPhrases($phraseData['language_id']);
                
                foreach ($phrasesData as $phraseData)
                {
                    $translate = $phraseData["translate"];
                    $phrase = $phraseData["phrase"];
                  
                    $strCharPhrase = '"';
                    if (strpos($phrase, '"') !== false) {
                        $strCharPhrase = "'";
                    }
                    $strCharTranslate = '"';
                    if (strpos($translate, '"') !== false) {
                        $strCharTranslate = "'";
                    }
//                     if ($translate !== "")
                        $content .=    $strCharPhrase . $phrase . $strCharPhrase . "=>" . $strCharTranslate . $translate . $strCharTranslate . ",\n";
                     
                }
                $content .= ") ?>";
                fwrite($languageFile, $content);
                fclose($languageFile);
                @unlink($fullPath .  $languageData[0]["code"] . ".php");
                rename($fullPath . $languageFileName , $fullPath . $languageData[0]["code"] . ".php" );
                $this->layout()->successMessage = __("Operation done successfully.");
                }
                $view ["languagePhraseForm"] = $languagePhraseForm;
                return new ViewModel($view);
	
	} 
    
    public function updatePhrasesAction()
    {
   		/* $myConfig = new \Zend\Config\Config( require 'config/autoload/global.php' );
		if ($myConfig ) {
			foreach ($myConfig as $confs) {
				var_dump($confs);
			}
		} */
        $shopProductAttributesTable    = new ShopProductAttributesTable($this->getServiceLocator());
        $allAttributesList = $shopProductAttributesTable->fetchAll();
        
//         $shopProductAttributeOptionsTable    = new ShopProductAttributeOptionsTable($this->getServiceLocator());
//         $allAttributesOptionsList = $shopProductAttributeOptionsTable->fetchAll();
        
        $dataGridPhrases = array('Showing', 'of', 'Page', 'items');
        $allAttributes = array();
        $allAttributesOptions = array();
//         foreach ($allAttributesList as $attributes) {
//             $allAttributes[] = $attributes["title_en"];
//         }
//         foreach ($allAttributesOptionsList as $attributesOptions) {
//             $allAttributesOptions[] = $attributesOptions["label"];
//         }
        $allPhrases = $this->generalhelper()->getDirContentsPhrases(__DIR__ .  DIRECTORY_SEPARATOR . ".." .  DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." );
    
       // $allToAddPhrases = array_merge($allAttributes, $allAttributesOptions, $dataGridPhrases);

        //$allPhrases = array_unique(array_merge($allToAddPhrases, $allPhrases));
        
        
        //daneshi add this line when shop module is disabled.
        $allPhrases = array_unique(array_merge($allPhrases, $dataGridPhrases));
 
        $languageMainPhrasesTable    = new LanguageMainPhrasesTable($this->getServiceLocator());
        if ($allPhrases) {
            rsort($allPhrases);
            $isAdded = true;
            
            $languageFile = fopen(__DIR__ .  DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." .  DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR  . "language" . DIRECTORY_SEPARATOR . "en.php", "w") or die(__("Unable to open file!"));
            
            $content = "<?php return array (\n";
            
            $isAdded = $languageMainPhrasesTable->deletePhrases();
            
            foreach ($allPhrases as $phrase) {
                if (in_array($phrase, array('$attribute[name]', '$publicFiles[name]', '$imageFiles[name]', '$daysTranslate', '$specialWord'))) 
                    continue;
                try {
                    $isAdded = $languageMainPhrasesTable->addPhrase($phrase); 
                    $strCharPhrase = '"';
                    if (strpos($phrase, '"') !== false) {
                        $strCharPhrase = "'";
                    } 
                    if ($phrase !== "")
                        $content .=    $strCharPhrase . $phrase . $strCharPhrase . "=>" . $strCharPhrase . $phrase . $strCharPhrase . ",\n";
                     
                } catch (\Exception $e) {
                }
            
            }
            $content .= ") ?>";
            fwrite($languageFile, $content);
            fclose($languageFile);
            if ($isAdded) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            }
        } 
        return $this->redirect()->toRoute('language-admin', array("action" => "list-phrases")); 
    }
    
    public function listPhrasesAction()
    {
        $this->setHeadTitle(__('Phrases list'));
        $container = new Container('token');
        $csrf = md5(time() . rand());
        $container->csrf = $csrf;
        $view['csrf'] = $csrf;
        $languageMainPhrasesTable = new LanguageMainPhrasesTable($this->getServiceLocator());
        $phrases = $languageMainPhrasesTable->getAllPhrases();
        $view['phrases'] = $phrases;
        return new ViewModel($view); 
 
    }
    
 
    public function listLanguagesAction()
	{
	    $this->setHeadTitle(__('Languages list'));
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	     
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	    
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $languageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		$languages = $languageLanguagesTable->getAllLanguagesGrid();
	    $grid->setTitle(__('Languages List'));
	    $grid->setDefaultItemsPerPage(5);
	    $grid->setDataSource($languages,$dbAdapter);
	    
	    $col = new Column\Select('id',"language_languages");
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('title');
	    $col->setLabel('Title');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('code');
	    $col->setLabel(__('Language code'));
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('enable');
	    $col->setLabel(__('Status'));
	    $options = array('0' => 'inactive', '1' => 'active');
	    $col->setFilterSelectOptions($options);
	    $replaces = array('0' => 'inactive', '1' => 'active');
	    $col->setReplaceValues($replaces, $col);
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('default');
	    $col->setLabel(__('Default language'));
	    $options1 = array('0' => '__', '1' => 'Default language');
	    $col->setFilterSelectOptions($options1);
	    $replaces = array('0' => '__', '1' => 'Default language');
	    $col->setReplaceValues($replaces, $col);
	    $grid->addColumn($col);

	    $col = new Column\Select('direction');
	    $col->setLabel(__('Language direction'));
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction1 = new Column\Action\Icon();
	    $viewAction1->setIconClass('glyphicon glyphicon-edit');
	    $viewAction1->setLink("/". $this->lang .'/admin-language/edit-language/' . $rowId);
	    $viewAction1->setTooltipTitle(__('Edit'));
	    
	    $viewAction2 = new Column\Action\Icon();
	    $viewAction2->setIconClass('glyphicon glyphicon-remove');
	    $viewAction2->setLink("/". $this->lang .'/admin-language/delete-language/'.$rowId.'/token/'.$csrf);
	    $viewAction2->setTooltipTitle(__('Delete'));
	    
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction1);
	    $actions2->addAction($viewAction2);
	    $grid->addColumn($actions2);
	     
	    $link[] = '<a href='."/". $this->lang .'/admin-language/add-language class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
	    $grid->setLink($link);
	    
	    $grid->render();
	     
	    return $grid->getResponse();
	     
		
	}

	public function addLanguageAction()
	{
	    $this->setHeadTitle(__('Add language'));
		$languageForm = new LanguageForm();
		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			$languageForm->setData($data);
			if ($languageForm->isValid()) {
				$validData = $languageForm->getData();
				$languageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
				try{
    				$isAdded = $languageLanguagesTable->addLanguage($validData);
    				if ($isAdded) {
    					if (isset($_POST['submit'])) {
    					    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
    					    return $this->redirect()->toRoute('language-admin');
    					} else {
    					    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
    					    return $this->redirect()->toRoute('language-admin', array(
    					        'action' => 'add-language'
    					    ));
    					}
    				} else {
    				    $this->layout()->errorMessage = __("Operation failed!");
    				}
				}catch (\Exception $e) {
	                $this->layout()->errorMessage = __("The name is taken, please choose another one.");
	            }
			}
		}
		$view['languageForm'] = $languageForm;
		return new ViewModel($view);
		
	}

    public function editLanguageAction()
	{
	    $this->setHeadTitle(__('Edit language'));
		$languageForm = new LanguageForm();
		$languageId = $this->params('id', -1);
		$languageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		$languageData = $languageLanguagesTable->getLanguageById($languageId);
		if (!$languageData) {
		    return $this->redirect()->toRoute('language-admin');
		}
		$languageForm->populateValues($languageData[0]);
		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost()->toArray();
			$languageForm->setData($data);
			if ($languageForm->isValid()) {
				$validData = $languageForm->getData();
				try{
    				$isEdited = $languageLanguagesTable->editLanguage($validData, $languageId);
    				if ($isEdited !== false) {
    					$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
    					return $this->redirect()->toRoute('language-admin');
    				} else {
    					$this->layout()->errorMessage = __("Operation failed!");
    				}
				}catch (\Exception $e) {
	                $this->layout()->errorMessage = __("The name is taken, please choose another one.");
	            }
			}
		} 
		$view['languageForm'] = $languageForm;
		return new ViewModel($view);
	}
	
	
	public function deleteLanguageAction()
	{
		$container = new Container('token');
		$token = $this->params('token');
		$languageId = $this->params('id', -1);
		$languageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		$languageData = $languageLanguagesTable->getLanguageById($languageId);
		if (!$languageData) {
		    return $this->redirect()->toRoute('language-admin');
		}
		if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
		    $isDeleted = $languageLanguagesTable->deleteLanguage($languageId);
			if ($isDeleted) {
				$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
			} else {
			    $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
			}
		}
		return $this->redirect()->toRoute('language-admin');
	}
}

 