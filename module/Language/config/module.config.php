<?php
return array(
    'router' => array(
        'routes' => array(
            'language-admin' => array (
                'type' => 'segment',
                'options' => array (
                    'route' => '[/:lang]/admin-language[/:action][/:id][/page/:page][/delete/:delete][/clear/:clear][/token/:token][/count/:count]',
                    'constraints' => array (
                        'lang' => '[a-zA-Z]{2}',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                        'delete' => '[0-9]+',
                        'clear' => 'true',
                        'count' => '[0-9]+',
                        'token' => '[a-f0-9]+',
            
                    ),
                    'defaults' => array (
                        '__NAMESPACE__' => 'Language\Controller',
                        'controller' => 'Admin',
                        'action' => 'list-languages'
                    )
                )
            ),
            'language-admin-ajax' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-language-ajax',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Language\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'get-phrases'
                    ),
                )
            ),
            'admin-language-default-ajax' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-language-default-ajax',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Language\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'get-default'
                    ),
                )
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            include_once(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);