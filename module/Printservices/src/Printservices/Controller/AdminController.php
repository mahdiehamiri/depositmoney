<?php
namespace Printservices\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Ourservices\Form\ServicesForm;
use Zend\Session\Container;
use Ourservices\Model\ServicesTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Gallery\Model\GalleriesTable;
use Zend\Db\Sql\Where;
use Language\Model\LanguageLanguagesTable;
use Page\Model\VideoGalleriesTable;
use Ourservices\Form\ServiceForm;
use Printservices\Form\PrintServiceForm;
use Printservices\Model\PrintServicesTable;
use Printservices\Form\AttributesForm;
use Printservices\Model\AttributesTable;
use Printservices\Model\AttributesValueTable;
use Printservices\Model\AttributesIndexTable;
use Printservices\Form\AttributesValueForm;
use Printservices\Model\AttributesGroupTable;
use Menu\Model\MenuMenusTable;
use Printservices\Model\OrderTable;
use Printservices\Model\OrdersIndexTable;
use Printservices\Model\ServicesTypesTable;
use Application\Helper\Jdates;
use Printservices\Form\ServicesTypesForm;
use Printservices\Model\PrintServiceOrderTable;
class AdminController extends BaseAdminController
{
    
    //type of services
    public function servicesTypesAction(){
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $services = $servicesTypesTable->getAll();
        foreach ($services as $key => $service){
            switch ($service['status']){
                case '0': 
                    $services[$key]['status'] = 'غیرفعال';
                    break;
                case '1':
                    $services[$key]['status'] = 'فعال';
                    break;
            }
        }
        $grid->setTitle(__('list Services Types'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($services, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-printservices/edit-services-types/id/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-printservices/delete-services-types/id/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-printservices/add-services-types class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    
    public function addServicesTypesAction(){
        $servicesTypesForm = new ServicesTypesForm();
        $view['form'] = $servicesTypesForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
            $add= $servicesTypesTable->add($postData);
            if ($add) {
                // add menu for services types
                $menusTable = new MenuMenusTable($this->getServiceLocator());
                $menuData["caption"] = $postData['title'] ;
                $menuData["internal_url"] = '';
                $menuData["order"] = '';
                $menuData["width"] = '';
                $menuData["icon"] = '';
                $menuData["image"] ='';
                $menuData["external_url"] = '';
                if($postData["status"] == 1){
                    $menuData["active"] = 1;  
                }else{
                    $menuData["active"] = 0; 
                }
                
                $menuData["parent_id"] = '78';
                $isAdded = $menusTable->addMenu(1, $menuData);
                if ($isAdded) {
                    $menuData["class_name_ul"] = "dropdown-menu";
                    $menuData["class_name_li"] = "dropdown";
                    $menusTable->editUlLi($menuData["parent_id"], $menuData);
                    $updateMenu = $servicesTypesTable->updateMenuId($isAdded,$add); 
                }
               
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('admin-printservices', array(
                    'action' => 'services-types'
                ));
            }
        }
        return new ViewModel($view);
    }
    
    public function editServicesTypesAction(){
        $id = $this->params('id');
        $servicesTypesForm = new ServicesTypesForm();
        $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
        $getTypes = $servicesTypesTable->getById($id);
        $servicesTypesForm->populateValues($getTypes);
        $view['form'] = $servicesTypesForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
            $edit= $servicesTypesTable->edit($postData,$id);
            if ($edit) {
                $menusTable = new MenuMenusTable($this->getServiceLocator());
                $updateMenu = $menusTable->updateMenuActive($getTypes['menu_id'], $postData["status"],$postData['title']);
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('admin-printservices', array(
                    'action' => 'services-types'
                ));
            }
        }
        return new ViewModel($view);
    }
    
    public function deleteServicesTypesAction(){
        $container = new Container('token');
        $token = $this->params('token');
        $id = $this->params('id');
        if ($id && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $servicesTypeTable = new ServicesTypesTable($this->getServiceLocator());
                $getTypes = $servicesTypeTable->getById($id);
                $isDelete = $servicesTypeTable->delete($id);
                if ($isDelete) {
                    $menusTable = new MenuMenusTable($this->getServiceLocator());
                    $updateMenu = $menusTable->updateMenuActive($getTypes['menu_id'],'0');
//                     var_dump($updateMenu);die;
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-printservices/services-types');
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-printservices/services-types');
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        return $this->redirect()->toRoute('admin-printservices', array(
            'action' => 'print-services-list'
        ));
    }
    
    // print services 
    public function addPrintServiceAction()
    {
        $validationExt = "jpg,jpeg,png,gif,mp4";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/services/";
        
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $getAll = $printServicesTable->fetchParent();
        
        $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
        $getType = $servicesTypesTable->getAll();
        
        $printServicesForm = new PrintServiceForm($getAll,$getType);
        $view['printServicesForm'] = $printServicesForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray(); 
            $files = $request->getFiles()->toArray();
            $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
            $getServiceType= $servicesTypesTable->getById($postData['type_id']);
            $postData['image'] = '';
            if ($files['image']['tmp_name'] != '') {
                $newFileName = md5(time() . $files['image']['name']);
                $isUploaded = $this->imageUploader()->UploadFile($files['image'], $validationExt, $uploadDir, $newFileName);
                $postData['image'] = $isUploaded[1];
            }
            $printServicesTable = new PrintServicesTable($this->getServiceLocator());
            $addPrintService = $printServicesTable->add($postData);
            if ($addPrintService) {
                if($postData['parent_id'] == 0){
                    // add menu for category
                    $menusTable = new MenuMenusTable($this->getServiceLocator());
                    $menuData["caption"] = $postData['title'] ;
                    $menuData["internal_url"] = '';
                    $menuData["order"] = '';
                    $menuData["width"] = '';
                    $menuData["icon"] = '';
                    $menuData["image"] ='';
                    $menuData["external_url"] = '/category/' . $addPrintService;
                    $menuData["active"] = 1;
                    $menuData["parent_id"] = $getServiceType['menu_id'];
                    $isAdded = $menusTable->addMenu(1, $menuData);
                    if ($isAdded) {
                        $menuData["class_name_ul"] = "dropdown-menu";
                        $menuData["class_name_li"] = "dropdown";
                        $menusTable->editUlLi($menuData["parent_id"], $menuData);
                        $updateMenu = $printServicesTable->updateMenuId($isAdded,$addPrintService); 
                    }
                } 
                
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('admin-printservices', array(
                    'action' => 'print-services-list'
                ));
            }
        }
        return new ViewModel($view);
    }

    public function editPrintServiceAction()
    {
        $id = $this->params('id');
        $validationExt = "jpg,jpeg,png,gif,mp4";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/services/";
        
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $getPrintService = $printServicesTable->fetchById($id);
        $getAll = $printServicesTable->fetchParent();
        $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
        $getType = $servicesTypesTable->getAll();
        $printServicesForm = new PrintServiceForm($getAll,$getType);
        $printServicesForm->populateValues($getPrintService);
        $view['printServicesForm'] = $printServicesForm;
        $view['printService'] = $getPrintService;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray(); 
            $files = $request->getFiles()->toArray();
            if ($files['image']['tmp_name'] != '') {
                $newFileName = md5(time() . $files['image']['name']);
                $isUploaded = $this->imageUploader()->UploadFile($files['image'], $validationExt, $uploadDir, $newFileName);
                $postData['image'] = $isUploaded[1];
            } else {
                $postData['image'] = $getPrintService['image'];
            }
            $printServicesTable = new PrintServicesTable($this->getServiceLocator());
            $isAdded = $printServicesTable->edit($postData, $id);//var_dump($isAdded);die;
            if ($isAdded) {
                if($getPrintService['parent_id'] == 0){
                    $menusTable = new MenuMenusTable($this->getServiceLocator());
                    $updateMenu = $menusTable->updateMenu($getPrintService['menu_id'],$postData['title']);
                }
                $this->flashMessenger()->addSuccessMessage(__("operation done."));
                return $this->redirect()->toRoute('admin-printservices', array(
                    'action' => 'print-services-list'
                ));
            }
        }
        return new ViewModel($view);
    }

    public function printServicesListAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $services = $printServicesTable->getAll();
        $grid->setTitle(__('list Print Services'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($services, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-printservices/edit-print-service/id/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-printservices/delete-print-services/id/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-printservices/add-print-service class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }

    public function deletePrintServicesAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $id = $this->params('id');
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $getChilds = $printServicesTable->fetchChildByPrintService($id);//var_dump($getChilds);die;
        $getPrintService = $printServicesTable->fetchById($id);//var_dump($getPrintService);die;
        if ($id && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                if(!$getChilds){
                    $isDelete = $printServicesTable->delete($id);
                    if ($isDelete) {
                        if($getPrintService['parent_id'] == 0){
                            $menusTable = new MenuMenusTable($this->getServiceLocator());
                            $updateMenu = $menusTable->updateMenuActive($getPrintService['menu_id'],'0');
                        }
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toUrl("/" . $this->lang . '/admin-printservices/print-services-list');
                    } else {
                        $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
                        return $this->redirect()->toUrl("/" . $this->lang . '/admin-printservices/print-services-list');
                    }
                } else {
                    $this->flashMessenger()->addErrorMessage(__('first delete subcategory'));
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        return $this->redirect()->toRoute('admin-printservices', array(
            'action' => 'print-services-list'
        ));
    }
    
    // attributes
    public function attributesListAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $attrs = $attributesIndexTable->getAll();
        $grid->setTitle(__('Services list'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($attrs, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('service_type_title');
        $col->setLabel(__('Service Type'));
        $grid->addColumn($col);
        
        $col = new Column\Select('print_service_title');
        $col->setLabel(__('Print Service Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('attribute_title');
        $col->setLabel(__('Attribute'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-printservices/edit-attributes/id/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit Attribute'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-plus');
        $viewAction2->setLink("/" . $this->lang . '/admin-printservices/add-attributes-value/id/' . $rowId);
        $viewAction2->setTooltipTitle(__('Add Attribute Value'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-printservices/add-attributes class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    
    public function addAttributesAction()
    {
        $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
        $getType = $servicesTypesTable->getAll();
        
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $parentList = $printServicesTable->fetchParent();
        $printServicesList = $printServicesTable->fetchChild();
        $attributesTable = new AttributesTable($this->getServiceLocator());
        $attributesList = $attributesTable->getAll();
        
        $attributesForm = new AttributesForm($parentList,$printServicesList, $attributesList,$getType);
        $view['attributesForm'] = $attributesForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();//var_dump($postData);die;
//             if(!isset($postData['print_service_id'])){
            $postData['print_service_id'][] = $postData['parent_id'];
//             }else{
                
//             }
//             var_dump($postData['print_service_id']);die;
            foreach ($postData['print_service_id'] as $postData['print_service_id']){
                $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
                $isSetServices = $attributesIndexTable->fetchByPrintService($postData['print_service_id']);
                if(!$isSetServices){
                    foreach ($postData['title'] as $title) {
                        $existAttr = $attributesTable->existAttr($title);//var_dump($existAttr);die;
                        if ($existAttr) {
                            $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
                            $existAttr = $attributesIndexTable->existAttr($postData['print_service_id'], $title);
                            if (! $existAttr) {
                                $addAttrIndex = $attributesIndexTable->add($postData['print_service_id'], $title);
                            }
                        } else {
                            $addAttr = $attributesTable->add($title);
                            if ($addAttr) {
                                $addAttrIndex = $attributesIndexTable->add($postData['print_service_id'], $addAttr);
                            }
                        }
                    }
                    
                }
//                 else {
//                     $this->flashMessenger()->addErrorMessage(__("برای این خدمات چاپ قبلا ویژگی وارد شده است.در صورت تمایل می توانید آن را ویرایش نمایید."));
//                     return $this->redirect()->toRoute('admin-printservices', array(
//                         'action' => 'attributes-list'
//                     ));
//                 }
            }
            
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toRoute('admin-printservices', array(
                'action' => 'attributes-list'
            ));
            
        }
        return new ViewModel($view);
    }
    
    public function editAttributesAction()
    {
        $id = $this->params('id');
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $printServicesList = $printServicesTable->fetchParent(); 
        $attributesTable = new AttributesTable($this->getServiceLocator());
        $attributesList = $attributesTable->getAll();
        
        $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
        $getAttribute = $attributesIndexTable->fetchByPrintService($id);
        
            
        $getAttribute['title'] = explode(',', $getAttribute['attribute_id']);
        if($getAttribute['parent_id'] == 0) {
            $multiple = true;
            $disabled = ['0' => true,'1' => true,'2' => false];
            $getAttribute['parent_id'] =  $getAttribute['print_service_id'];
        }else{
            $multiple = false;
            $disabled = ['0' => true,'1' => true,'2' => true];
        }
        $getChild = $printServicesTable->fetchChildByPrintService($getAttribute['parent_id']);
        //var_dump($getAttribute);die;
        $servicesTypesTable = new ServicesTypesTable($this->getServiceLocator());
        $getType = $servicesTypesTable->getAll();
        
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $parentList = $printServicesTable->fetchParent();
        $printServicesList = $printServicesTable->fetchChild();
        $attributesTable = new AttributesTable($this->getServiceLocator());
        $attributesList = $attributesTable->getAll();
        
        
        $attributesForm = new AttributesForm($parentList,$getChild, $attributesList,$getType,'editMode',$multiple,$disabled);
        $attributesForm->populateValues($getAttribute);
        $view['attributesForm'] = $attributesForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray(); //var_dump($postData);die;
            if(isset($postData['print_service_id'])){
                foreach ($postData['print_service_id'] as $postData['print_service_id']){
                    $isSetServices = $attributesIndexTable->fetchByPrintService($postData['print_service_id']);
                    if(!$isSetServices){
                        foreach ($postData['title'] as $title) {
                            $existAttr = $attributesTable->existAttr($title);//var_dump($existAttr);die;
                            if ($existAttr) {
                                $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
                                $existAttr = $attributesIndexTable->existAttr($postData['print_service_id'], $title);
                                if (! $existAttr) {
                                    $addAttrIndex = $attributesIndexTable->add($postData['print_service_id'], $title);
                                }
                            } else {
                                $addAttr = $attributesTable->add($title);
                                if ($addAttr) {
                                    $addAttrIndex = $attributesIndexTable->add($postData['print_service_id'], $addAttr);
                                }
                            }
                        }
                    }
                    //var_dump($isSetServices);die;
                }//die;
            }
//             continue;
//             if(isset($postData['print_service_id'])){
            $plus = array_diff($postData['title'],$getAttribute['title']);//var_dump($plus);die;
            $deleteList = array_diff($getAttribute['title'], $postData['title']);//var_dump($delete);die;
            foreach ($plus as $add){
                if(is_numeric($add) == false){
                    $addAttr = $attributesTable->add($add);
                    if ($addAttr) {
                        $addAttrIndex = $attributesIndexTable->add($id, $addAttr);
                    }
                }else{
                    $addAttrIndex = $attributesIndexTable->add($id, $add);
                }
            } 
            foreach ($deleteList as $delete){
                $deleteAttrIndex = $attributesIndexTable->delete($id, $delete);
            }
//             }

            
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toRoute('admin-printservices', array(
                'action' => 'attributes-list'
            ));
        }
        return new ViewModel($view);
    }

    public function priceListAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $attributesGroupTable = new AttributesGroupTable($this->getServiceLocator());
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $groups = $attributesGroupTable->getAll();//var_dump($groups);die;
        foreach ($groups as $key => $gp){
            $groups[$key]['val'] = implode(' , ',json_decode($gp['value']));
        }
        $grid->setTitle(__('Price List'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($groups, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('print_service');
        $col->setLabel(__('Print Service Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('val');
        $col->setLabel(__('Value'));
        $grid->addColumn($col);
        
        $col = new Column\Select('price');
        $col->setLabel(__('Price'));
        $grid->addColumn($col);
        
        $col = new Column\Select('time');
        $col->setLabel(__('Time'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();

        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-printservices/delete-price/id/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-printservices/add-price class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    
    
    public function officePriceListAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $attributesGroupTable = new AttributesGroupTable($this->getServiceLocator());
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $groups = $attributesGroupTable->getAllOfficePrice();
        foreach ($groups as $key => $gp){
            $groups[$key]['val'] = implode(',',json_decode($gp['value']));
        }
        $grid->setTitle(__('Price List'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($groups, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('office_name');
        $col->setLabel(__('Office Name'));
        $grid->addColumn($col);
         
        $col = new Column\Select('print_service');
        $col->setLabel(__('Print Service Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('val');
        $col->setLabel(__('Value'));
        $grid->addColumn($col);
        
        $col = new Column\Select('price');
        $col->setLabel(__('Price'));
        $grid->addColumn($col);
        
        $col = new Column\Select('time');
        $col->setLabel(__('Time'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-printservices/delete-price/id/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-printservices/add-price class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    
    public function addAttributesValueAction()
    {
        $id = $this->params('id');
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $printServicesList = $printServicesTable->getAll();
        $attributesTable = new AttributesTable($this->getServiceLocator());
        $attributesList = $attributesTable->getAll();
        $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
        $attributesValList = $attributesValueTable->getAll();//var_dump($attributesValList);die;
        $attributesForm = new AttributesValueForm($printServicesList, $attributesList, $attributesValList,true);
        $fetch = $printServicesTable->fetchById($id);
        $fetch['print_service_id'] = $fetch['id'];
        $attributesForm->populateValues($fetch);
        $view['attributesForm'] = $attributesForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();//var_dump($postData);die;
            foreach ($postData['value'] as $key => $value) { //var_dump($key);die;
                $attrValArr = [];
                $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
                $val = $attributesValueTable->getAllByAttribute($key);//var_dump($val,'********************************');
                if(!$val){
//                     $val = explode(',', '12,13,14,15');//var_dump($val);die;
// //                     $plus = array_diff($value, $val);var_dump($plus);
                    foreach ($value as $val) {
                        $valData['attributes_index_id'] = $key;
                        $valData['value'] = $val;
                        $addVal = $attributesValueTable->add($valData);
                    }
                }else{
                    foreach ($val as $attrVal){
                        $attrValArr[] = $attrVal['id'];
                    }//var_dump($attrValArr,'****************************************************************');
//                     //$val = explode(',', $val['id']);//var_dump($val);die;
//                     $val = explode(',', '12,13,14');var_dump($val);die;
                    $plus = array_diff($value, $attrValArr);//var_dump('*************plus' . $key . '***********',$plus);
                    $delete = array_diff($attrValArr, $value);//var_dump('*************delete' . $key . '***********',$delete);
                    foreach ($plus as $val) {
                        $valData['attributes_index_id'] = $key;
                        $valData['value'] = $val;
                        $addVal = $attributesValueTable->add($valData);
                    }
// //                     var_dump(count($delete));die;
                    foreach ($delete as $del) {
                        $valData['attributes_index_id'] = $key;
                        $valData['value'] = $del;
                        $delVal = $attributesValueTable->delete($del);//var_dump($delVal);die;
                    }
                }

            }//die;
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toRoute('admin-printservices', array(
                'action' => 'attributes-list'
            ));
        }
        return new ViewModel($view);
    }

    public function addPriceAction()
    {
        $printServicesTable = new PrintServicesTable($this->getServiceLocator());
        $printServicesList = $printServicesTable->fetchChild();
        $attributesTable = new AttributesTable($this->getServiceLocator());
        $attributesList = $attributesTable->getAll();
        $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
        $attributesValList = $attributesValueTable->getAll();
        
        $attributesForm = new AttributesValueForm($printServicesList, $attributesList, $attributesValList);
        $view['attributesForm'] = $attributesForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();//var_dump($postData);die;
            $postData['value'] = json_encode($postData['value']);
            $table = new AttributesGroupTable($this->getServiceLocator());
            $isAdd = $table->add($postData);
            if($isAdd){
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('admin-printservices', array(
                    'action' => 'price-list'
                ));
            }
        }
        return new ViewModel($view);
    }
       
    public function deletePriceAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $id = $this->params('id');
        if ($id && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $attributeGroupTable = new AttributesGroupTable($this->getServiceLocator());
                $isDelete = $attributeGroupTable->delete($id);
                if ($isDelete) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-printservices/price-list');
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-printservices/price-list');
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        return $this->redirect()->toRoute('admin-shop', array(
            'action' => 'price-list'
        ));
    }

    private function getValAttr($get){
        foreach ($get as $key => $attr) {
            $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
            $attrVal = $attributesValueTable->getAllByAttribute($attr['id']);
            if (! empty($attrVal)) {
                foreach ($attrVal as $k => $val){
                    $get[$key]['options'][$val['id']] = $val['value'];
                }
                $exist = 'true';
            }else{
                $exist = 'false';
            }
        }
        $result = [
            'val' => $get,
            'exist' => $exist
        ];
        return $result;
    }
    public function getFormAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
            $printServiceTable = new PrintServicesTable($this->getServiceLocator());
            $get = $attributesIndexTable->getAllByPrintService($postData['id']);
            $attrVal = $this->getValAttr($get);
            if($attrVal['exist'] == 'false'){
                $getPrintService = $printServiceTable->fetchById($postData['id']);
                $postData['id'] = $getPrintService['parent_id'];
                $get = $attributesIndexTable->getAllByPrintService($postData['id']);
                $attrVal = $this->getValAttr($get);
            }
            // if(isset($postData['price']) && $postData['price'] == 'price'){
            //    $getPrintService = $printServiceTable->fetchById($postData['id']);
            //    $postData['id'] = $getPrintService['parent_id'];
            // }
            // var_dump($attrVal);die;
//             foreach ($get as $key => $attr) {
//                 $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
//                 $attrVal = $attributesValueTable->getAllByAttribute($attr['id']);
//                 // var_dump($attrVal);die;
//                 if (! empty($attrVal)) {
// //                     $value = explode(',', $attrVal['value']);
// //                     $id = explode(',', $attrVal['id']);
//                     foreach ($attrVal as $k => $val){
//                         $get[$key]['options'][$val['id']] = $val['value'];
//                     }
//                 }
//             }
            die(json_encode($attrVal['val']));
        }
        
    }

    public function getAttrAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $attributesIndexTable = new AttributesIndexTable($this->getServiceLocator());
            $get = $attributesIndexTable->getAllByPrintService($postData['id']);
        }
        die(json_encode($get));
    }
    
    public function getPrintServicesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $printServiceTable = new PrintServicesTable($this->getServiceLocator());
            $getPrintServices = $printServiceTable->fetchByTypeId($postData['id']);
        }
//         var_dump($getPrintServices);die;
        die(json_encode($getPrintServices));
    }
    
    public function getChildAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $printServiceTable = new PrintServicesTable($this->getServiceLocator());
            $getChild = $printServiceTable->fetchChildByPrintService($postData['id']);
        }
//         die(var_dump($getChild));
        die(json_encode($getChild));
    }

    public function getValueAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $attributesValueTable = new AttributesValueTable($this->getServiceLocator());
            $get = $attributesValueTable->getAllByAttribute($postData['id']);
        }
        die(json_encode(explode(',', $get['value'])));
    }
    
    public function ordersAction(){
        $ordersTable = new OrderTable($this->getServiceLocator());
        $orders = $ordersTable->getAll('print services',$this->params('slug'));
        $view['orders'] = $orders;
        return new ViewModel($view);
        
        
//         $contanier = new Container('token');
//         $csrf = md5(time() . rand());
//         $contanier->csrf = $csrf;
        
//         $ordersTable = new OrdersTable($this->getServiceLocator());
//         $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
//         $config = $this->getServiceLocator()->get('Config');
//         $dbAdapter = new Adapter($config['db']);
//         $orders = $ordersTable->getAll();
//         foreach ($orders as $key => $value){
//             if($value['created']) {
//                 $val = explode(' ', $value['created']);
//                 $val =  explode('-', $val[0]);
//                 $g_y = $val[0];
//                 $g_m = $val[1];
//                 $g_d = $val[2];
//                 $val['created'] = Jdates::gregorian_to_jalali($g_y, $g_m, $g_d);
//             }
//             $orders[$key]['created'] = implode('/', $val['created']);
//             if($value['delivery_type'] == 'courier'){
//                 $orders[$key]['delivery_type'] = 'پیک';
//             }elseif($value['delivery_type'] == 'special courier'){
//                 $orders[$key]['delivery_type'] = 'پیک ویژه';
//             }else{
//                 $orders[$key]['delivery_type'] = 'حضوری';
//             }   
//             if($value['status'] == 'sent'){
//                     $orders[$key]['status'] = 'ارسال شده';
//             }elseif($value['status'] == 'checking'){
//                     $orders[$key]['status'] = 'در حال بررسی';
//             }else{
//                 $orders[$key]['status'] = 'ثبت شده';
//             }
//         }
        
//         $grid->setTitle(__('Orders List'));
//         $grid->setDefaultItemsPerPage(15);
//         $grid->setDataSource($orders, $dbAdapter);
        
//         $col = new Column\Select('id');
//         $col->setLabel(__('Order Id'));
//         $col->setIdentity();
//         $col->addStyle(new Style\Bold());
//         $grid->addColumn($col);
        
//         $col = new Column\Select('username');
//         $col->setLabel(__('Username'));
//         $grid->addColumn($col);
        
//         $col = new Column\Select('total_price');
//         $col->setLabel(__('Total Price'));
//         $grid->addColumn($col);
        
//         $col = new Column\Select('delivery_type');
//         $col->setLabel(__('Delivery Type'));
//         $grid->addColumn($col);
        
//         $col = new Column\Select('delivery_price');
//         $col->setLabel(__('Delivery Price'));
//         $grid->addColumn($col);
        
//         $col = new Column\Select('created');
//         $col->setLabel(__('Order Created'));
//         $grid->addColumn($col);
        
//         $col = new Column\Select('status');
//         $col->setLabel(__('Status'));
//         $grid->addColumn($col);
        
//         $btn = new Column\Action\Button();
//         $btn->setLabel(__('Edit'));
//         $rowId = $btn->getRowIdPlaceholder();
        
//         $viewAction2 = new Column\Action\Icon();
//         $viewAction2->setIconClass('glyphicon glyphicon-check');
//         $viewAction2->setLink("/" . $this->lang . '/admin-printservices/change-order-status/id/' . $rowId);
//         $viewAction2->setTitle(__('change status'));
        
//         $viewAction1 = new Column\Action\Icon();
//         $viewAction1->setIconClass('fa fa-file-o');
//         $viewAction1->setLink("/" . $this->lang . '/admin-printservices/view-order/id/' . $rowId);
//         $viewAction1->setTitle(__('view detail'));
        
//         $actions2 = new Column\Action();
//         $actions2->setLabel(__('Operations'));
//         $actions2->addAction($viewAction2);
//         $actions2->addAction($viewAction1);
//         $actions2->setWidth(15);
//         $grid->addColumn($actions2);
        
//         $grid->render();
//         return $grid->getResponse();
    }
    
    public function viewOrderAction(){
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        $id = $this->params('id');
//         $printServiceOrdersTable = new OrdersIndexTable($this->getServiceLocator());
        $printServiceOrderDetailTable = new PrintServiceOrderTable($this->getServiceLocator());
        $getDetail = $printServiceOrderDetailTable->getByOrder($id);
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        foreach ($getDetail as $key => $val){
            $getDetail[$key]['value'] = implode(', ', json_decode($val['value']));
        }
        
        $grid->setTitle(__('Orders Detail List'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($getDetail, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
     /*    $col = new Column\Select('type');
        $col->setLabel(__('Type'));
        $grid->addColumn($col); */
        
        $col = new Column\Select('title');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('time');
        $col->setLabel(__('Time'));
        $grid->addColumn($col);
        
        $col = new Column\Select('price');
        $col->setLabel(__('Price'));
        $grid->addColumn($col);
        
        $col = new Column\Select('value');
        $col->setLabel(__('type of order'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $grid->render();
        return $grid->getResponse();
    }

    public function changeOrderStatusAction(){
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();//var_dump($postData);die;
            $orderTable = new OrderTable($this->getServiceLocator());
            $update = $orderTable->updateStatus($postData['order_status'], $postData['order_id'],$postData['status-desc']);
            var_dump($update);die;
        }
    }
    
    
}
