<?php 
namespace PrintServices\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

class AttributesIndexTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    { 
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('attributes_index', $this->adapter);
    }
	
    public function getAll()
    {
        $select = new Select('attributes_index');
        $select->columns(array('id' => 'print_service_id'));
        $select->join('attributes', 'attributes.id = attributes_index.attributes_id ', array(
	        'attribute_title' => new Expression('Group_Concat(DISTINCT attributes.title)')
        ), Select::JOIN_LEFT);
	    $select->join('print_services', 'print_services.id = attributes_index.print_service_id ', array(
	        'print_service_title' => 'title',
	    ), Select::JOIN_LEFT);
	    $select->join('services_types', 'services_types.id = print_services.type_id ', array(
	        'service_type_title' => 'title',
	    ), Select::JOIN_LEFT);
	    $select->group('attributes_index.print_service_id');
	    $select->order('attributes_index.id DESC');
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
    public function add($printService,$title)
	{
	    $this->tableGateway->insert(array(
	        'print_service_id' => $printService,
	        'attributes_id' => $title,
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function getAllByPrintService($servicesId)
	{
	    $select = new Select('attributes_index');
	    $select->where->equalTo('print_service_id', $servicesId);
	    $select->join('attributes', 'attributes.id = attributes_index.attributes_id ', array(
	        'attribute_title' => 'title',
	        'attribute_name' => 'name'
	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function existAttr($printService,$attrId)
	{
	    $select = new Select('attributes_index');
	    $select->where->equalTo('attributes_id', $attrId);
	    $select->where->equalTo('print_service_id', $printService);
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function fetchByPrintService($id)
	{
	    $select = new Select('attributes_index');
	    $select->where->equalTo('attributes_index.print_service_id', $id);
	    $select->join('attributes', 'attributes.id = attributes_index.attributes_id ', array(
	        'attribute_id' => new Expression('Group_Concat(DISTINCT attributes.id)')
	    ), Select::JOIN_LEFT);
	    $select->join('print_services', 'print_services.id = attributes_index.print_service_id ', array(
	        'print_service_title' => 'title',
	        'parent_id',
	        'type_id'
	    ), Select::JOIN_LEFT);
	    $select->group('attributes_index.print_service_id');
	    return $this->tableGateway->selectWith($select)->current();
	}
// 	public function getAllByPrintService($servicesId)
// 	{
// 	    $select = new Select('attributes_index');
// 	    $select->where->equalTo('print_service_id', $servicesId);
// 	    $select->join('attributes', 'attributes.id = attributes_index.attributes_id ', array(
// 	        'attribute_title' => 'title',
// 	        'attribute_name' => 'name'
// 	    ), Select::JOIN_LEFT);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getAllServices()
// 	{
// 		return $this->tableGateway->select()->toArray();
// 	}
	
// 	public function getService($where = false)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) { 
// 	        $select->where($where);
// 	    } 
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServiceHomePage($where = false, $limit)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) {
// 	        $select->where($where);
// 	    }
// 	    $select->order('id DESC');
// 	    $select->limit($limit);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function searchService($services, $lang, $getPaginator = true)
// 	{
// 	    $select = new Select('services');
// 	    $where = new Where();
// 	    $select->where->nest()->like('name', "%$services%")->or->like('description', "%$services%")->unnest();
// 	    $select->where->equalTo('lang', $lang);
// 	    if ($getPaginator) {
// 	        $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
// 	        return new Paginator($dbSelector);
// 	    } else {
// 	        return $this->tableGateway->selectWith($select)->toArray();
// 	    } 
// 	}
// 	public function getServiceById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServicesChileById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('parent_id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getAllServicesForList()
// 	{
// 	    $select = new Select('services'); 
// 	    return  $select; 
// 	}
// 	public function getServicesByName($name)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->like('name', "%$name%");
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function addServices($postData)
// 	{ 
// 		$this->tableGateway->insert($postData);
// 		return $this->tableGateway->getLastInsertValue();
// 	}
	
	public function delete($printServiceId, $attrId = null)
	{
// 		if ($servicesId > 0 && is_numeric($servicesId)) {
// 			if ($userId !== null) {
			return $this->tableGateway->delete(array(
			    'attributes_id' 	 => $attrId,
			    'print_service_id' => $printServiceId
			));
// 			} else {
// 				return $this->tableGateway->delete(array(
// 						'id' => $servicesId
// 				));
// 			}
// 		}
	}
	
// 	public function updateServices($menuData, $menuId)
// 	{ 
// 	    $data = array(
// 	        "name" => $menuData["name"],
// 	        "description" => $menuData["description"],
// 	        "is_parent" => $menuData["is_parent"],
// // 	        "parent_id" => $menuData["parent_id"],
// 	        'lang' => $menuData['lang'],
// 	        "gallery_id" => $menuData["gallery_id"],
// 	        "active" => $menuData["active"],
// 	        "image" => $menuData["image"],
// 	    );
	    
// 	    return $this->tableGateway->update($data,  array('id' => $menuId));
// 	}


}
