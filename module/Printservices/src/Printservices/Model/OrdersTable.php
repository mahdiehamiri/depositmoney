<?php 
namespace PrintServices\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

class OrdersTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    { 
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('orders', $this->adapter);
    }
	
    
//     public function get($val){
//         $select = new Select('attributes_group');
//         $select->where->equalTo('value',$val);
//         return $this->tableGateway->selectWith($select)->current();
//     }

    public function add($postData)
	{
	    $this->tableGateway->insert(array(
	        'total_price' => $postData['total_price'],
	        'delivery_type' => $postData['delivery_type'],
	        'delivery_price' => $postData['delivery_price'],
	        'discount' => $postData['discount'],
	        'address' => $postData['address'],
	        'user_id' => $postData['user_id'],
	        'postal_code' => $postData['postal_code'],
	        'status' => $postData['status'],
	        'created' => date("Y-m-d H:i:m")
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function getAll()
	{
	    $select = new Select('orders');
	    $select->join('users', 'users.id = orders.user_id ', array(
	        'username'
	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getById($id)
	{
	    $select = new Select('orders');
	    $select->where->equalTo('orders.id', $id);
	    $select->join('orders_index', 'orders_index.orders_id = orders.id ', array(
	        'order_detail_id' => new Expression('Group_Concat(order_detail_id)')
	    ), Select::JOIN_LEFT);
	    $select->join('orders_file', 'orders_file.order_id = orders.id ', array(
	        'file' => new Expression('Group_Concat(DISTINCT file)')
	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function getByUser($user)
	{
	    $select = new Select('orders');
	    $select->where->equalTo('orders.user_id', $user);
// 	    $select->join('orders_index', 'orders_index.orders_id = orders.id ', array(
// 	        'order_detail_id' => new Expression('Group_Concat(order_detail_id)')
// 	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->toArray();
	}

	public function updateStatus($status, $id)
	{
	    return $this->tableGateway->update(array('status' => $status),  array('id' => $id));
	}
	
	
// 	public function fetchChild()
// 	{
// 	    $select = new Select('print_services');
// 	    $select->where->notEqualTo('parent_id', 0);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function getService($where = false)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) { 
// 	        $select->where($where);
// 	    } 
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServiceHomePage($where = false, $limit)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) {
// 	        $select->where($where);
// 	    }
// 	    $select->order('id DESC');
// 	    $select->limit($limit);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function searchService($services, $lang, $getPaginator = true)
// 	{
// 	    $select = new Select('services');
// 	    $where = new Where();
// 	    $select->where->nest()->like('name', "%$services%")->or->like('description', "%$services%")->unnest();
// 	    $select->where->equalTo('lang', $lang);
// 	    if ($getPaginator) {
// 	        $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
// 	        return new Paginator($dbSelector);
// 	    } else {
// 	        return $this->tableGateway->selectWith($select)->toArray();
// 	    } 
// 	}
// 	public function getServiceById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServicesChileById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('parent_id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getAllServicesForList()
// 	{
// 	    $select = new Select('services'); 
// 	    return  $select; 
// 	}
// 	public function getServicesByName($name)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->like('name', "%$name%");
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function addServices($postData)
// 	{ 
// 		$this->tableGateway->insert($postData);
// 		return $this->tableGateway->getLastInsertValue();
// 	}
	
// 	public function deleteServices($servicesId, $userId = null)
// 	{
// 		if ($servicesId > 0 && is_numeric($servicesId)) {
// 			if ($userId !== null) {
// 				return $this->tableGateway->delete(array(
// 						'id' 	  			  => $servicesId,
// 						'post_author_user_id' => $userId
// 				));
// 			} else {
// 				return $this->tableGateway->delete(array(
// 						'id' => $servicesId
// 				));
// 			}
// 		}
// 	}
	
// 	public function updateServices($menuData, $menuId)
// 	{ 
// 	    $data = array(
// 	        "name" => $menuData["name"],
// 	        "description" => $menuData["description"],
// 	        "is_parent" => $menuData["is_parent"],
// // 	        "parent_id" => $menuData["parent_id"],
// 	        'lang' => $menuData['lang'],
// 	        "gallery_id" => $menuData["gallery_id"],
// 	        "active" => $menuData["active"],
// 	        "image" => $menuData["image"],
// 	    );
	    
// 	    return $this->tableGateway->update($data,  array('id' => $menuId));
// 	}


}
