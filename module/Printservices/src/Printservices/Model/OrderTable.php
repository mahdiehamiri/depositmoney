<?php 
namespace PrintServices\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Symfony\Component\Validator\Constraints\EqualTo;

class OrderTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    { 
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('order', $this->adapter);
    }
	
    public function getByUser($user=null,$type=null)
    {
        $select = new Select('order');
        $select->where->equalTo('order.user_id', $user);
        $select->where->equalTo('order.type', $type);
// 	    $select->join('orders_index', 'orders_index.orders_id = orders.id ', array(
// 	        'order_detail_id' => new Expression('Group_Concat(order_detail_id)')
// 	    ), Select::JOIN_LEFT);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
    public function getByOffice($user=null,$type=null)
    {
        $select = new Select('order');
        $select->where->equalTo('order.office_id', $user);
        $select->where->equalTo('order.type', $type);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
//     public function get($val){
//         $select = new Select('attributes_group');
//         $select->where->equalTo('value',$val);
//         return $this->tableGateway->selectWith($select)->current();
//     }

    public function add($postData)
	{
	    $this->tableGateway->insert(array(
	        'address' => $postData['address'],
	        'postal_code' => $postData['postal_code'],
	        'qty' => $postData['qty'],
	        'total_price' => $postData['total_price'],
	        'delivery_type' => $postData['delivery_type'],
	        'delivery_price' => $postData['delivery_price'],
	        'discount' => $postData['discount'],
	        'created' => date("Y-m-d H:i:m"),
	        'user_id' => $postData['user_id'],
	        'status' => 'requested',
	        'type' => $postData['type'],
	        'office_id' => $postData['office_id'],
	        'price' => $postData['price'],
// 	        'address' => $postData['address'],
// 	        'user_id' => $postData['user_id'],
// 	        'postal_code' => $postData['postal_code'],
// 	        'status' => $postData['status'],
// 	        'created' => date("Y-m-d H:i:m")
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
// 	public function getAll()
// 	{
// 		return $this->tableGateway->select()->toArray();
// 	}
	
	public function updateStatus($status, $id,$statusDesc)
	{
	    return $this->tableGateway->update(array(
	        'status' => $status ,
	        'order_description' => $statusDesc
	    ),  array(
	        'id' => $id
	        
	    ));
	}
	
	public function getById($id)
	{
	    $select = new Select('order');
	    $select->where->equalTo('order.id', $id);
	    $select->join('orders_index', 'orders_index.orders_id = order.id ', array(
	        'order_detail_id' => new Expression('Group_Concat(order_detail_id)')
	    ), Select::JOIN_LEFT);
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	
	public function getAll($type = null,$status=null)
	{
	    $select = new Select('order');
	    $select->join('users', 'users.id = order.user_id ', array(
	        'username'
	    ), Select::JOIN_LEFT);
	    $select->where->equalTo('order.type', $type);
	    if($type="print services"){
	        $select->join('office', 'office.id = order.office_id ', array(
	            'office_name'
	        ), Select::JOIN_LEFT);
	    }
	    
	    if($status != 'all'){
	        $select->where->equalTo('order.status', $status);
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
// 	public function fetchChild()
// 	{
// 	    $select = new Select('print_services');
// 	    $select->where->notEqualTo('parent_id', 0);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function getService($where = false)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) { 
// 	        $select->where($where);
// 	    } 
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServiceHomePage($where = false, $limit)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) {
// 	        $select->where($where);
// 	    }
// 	    $select->order('id DESC');
// 	    $select->limit($limit);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function searchService($services, $lang, $getPaginator = true)
// 	{
// 	    $select = new Select('services');
// 	    $where = new Where();
// 	    $select->where->nest()->like('name', "%$services%")->or->like('description', "%$services%")->unnest();
// 	    $select->where->equalTo('lang', $lang);
// 	    if ($getPaginator) {
// 	        $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
// 	        return new Paginator($dbSelector);
// 	    } else {
// 	        return $this->tableGateway->selectWith($select)->toArray();
// 	    } 
// 	}
// 	public function getServiceById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServicesChileById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('parent_id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getAllServicesForList()
// 	{
// 	    $select = new Select('services'); 
// 	    return  $select; 
// 	}
// 	public function getServicesByName($name)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->like('name', "%$name%");
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function addServices($postData)
// 	{ 
// 		$this->tableGateway->insert($postData);
// 		return $this->tableGateway->getLastInsertValue();
// 	}
	
// 	public function deleteServices($servicesId, $userId = null)
// 	{
// 		if ($servicesId > 0 && is_numeric($servicesId)) {
// 			if ($userId !== null) {
// 				return $this->tableGateway->delete(array(
// 						'id' 	  			  => $servicesId,
// 						'post_author_user_id' => $userId
// 				));
// 			} else {
// 				return $this->tableGateway->delete(array(
// 						'id' => $servicesId
// 				));
// 			}
// 		}
// 	}
	
// 	public function updateServices($menuData, $menuId)
// 	{ 
// 	    $data = array(
// 	        "name" => $menuData["name"],
// 	        "description" => $menuData["description"],
// 	        "is_parent" => $menuData["is_parent"],
// // 	        "parent_id" => $menuData["parent_id"],
// 	        'lang' => $menuData['lang'],
// 	        "gallery_id" => $menuData["gallery_id"],
// 	        "active" => $menuData["active"],
// 	        "image" => $menuData["image"],
// 	    );
	    
// 	    return $this->tableGateway->update($data,  array('id' => $menuId));
// 	}


}
