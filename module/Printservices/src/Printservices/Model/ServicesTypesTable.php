<?php 
namespace PrintServices\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ServicesTypesTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    { 
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('services_types', $this->adapter);
    }
	
    public function add($postData)
	{
	    $this->tableGateway->insert(array(
	        'title' => $postData['title'],
	        'status' => $postData['status'],
	        'office_status' => $postData['office_status'],
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	public function edit($postData, $id)
	{
	    $data = array(
	        'title' => $postData['title'],
	        'status' => $postData['status'],
	        'office_status' => $postData['office_status'],
	    );
	    
	    return $this->tableGateway->update($data,  array('id' => $id));
	}
	
	public function updateMenuId($menu, $id)
	{
	    return $this->tableGateway->update(array('menu_id' => $menu),  array('id' => $id));
	}
	
	public function getAll()
	{
	    $select = new Select('services_types');
	    $select->where->notEqualTo('status', 2);
	    return $this->tableGateway->selectWith($select)->toArray(); 
	}
	
	
	public function getById($id)
	{
	    $select = new Select('services_types');
	    $select->where->equalTo('id', $id);
	    return $this->tableGateway->selectWith($select)->current();
	}
	
// 	public function fetchChild()
// 	{
// 	    $select = new Select('print_services');
// 	    $select->where->notEqualTo('parent_id', 0);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function fetchChildByPrintService($parent)
// 	{
// 	    $select = new Select('print_services');
// 	    $select->where->equalTo('parent_id', $parent);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function fetchById($id)
// 	{
// 	    $select = new Select('print_services');
// 	    $select->where->equalTo('id', $id);
// 	    return $this->tableGateway->selectWith($select)->current();
// 	}
	
// 	public function fetch($id)
// 	{
// 	    $select = new Select('print_services');
// 	    $select->where->equalTo('id', $id);
// 	    return $this->tableGateway->selectWith($select)->current();
// 	}
	
	public function delete($id)
	{
	    return $this->tableGateway->update(array('status' => 2),  array('id' => $id));
	}
// 	public function getService($where = false)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) { 
// 	        $select->where($where);
// 	    } 
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServiceHomePage($where = false, $limit)
// 	{
// 	    $select = new Select('services');
// 	    if ($where) {
// 	        $select->where($where);
// 	    }
// 	    $select->order('id DESC');
// 	    $select->limit($limit);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function searchService($services, $lang, $getPaginator = true)
// 	{
// 	    $select = new Select('services');
// 	    $where = new Where();
// 	    $select->where->nest()->like('name', "%$services%")->or->like('description', "%$services%")->unnest();
// 	    $select->where->equalTo('lang', $lang);
// 	    if ($getPaginator) {
// 	        $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
// 	        return new Paginator($dbSelector);
// 	    } else {
// 	        return $this->tableGateway->selectWith($select)->toArray();
// 	    } 
// 	}
// 	public function getServiceById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getServicesChileById($servicesId)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->equalTo('parent_id', $servicesId);
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
// 	public function getAllServicesForList()
// 	{
// 	    $select = new Select('services'); 
// 	    return  $select; 
// 	}
// 	public function getServicesByName($name)
// 	{
// 	    $select = new Select('services');
// 	    $select->where->like('name', "%$name%");
// 	    return $this->tableGateway->selectWith($select)->toArray();
// 	}
	
// 	public function addServices($postData)
// 	{ 
// 		$this->tableGateway->insert($postData);
// 		return $this->tableGateway->getLastInsertValue();
// 	}
	
// 	public function deleteServices($servicesId, $userId = null)
// 	{
// 		if ($servicesId > 0 && is_numeric($servicesId)) {
// 			if ($userId !== null) {
// 				return $this->tableGateway->delete(array(
// 						'id' 	  			  => $servicesId,
// 						'post_author_user_id' => $userId
// 				));
// 			} else {
// 				return $this->tableGateway->delete(array(
// 						'id' => $servicesId
// 				));
// 			}
// 		}
// 	}
	
	


}
