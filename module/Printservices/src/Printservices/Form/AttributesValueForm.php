<?php
namespace PrintServices\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;

class AttributesValueForm extends Form
{

    public function __construct($printServicesList,$attributesList,$valueList,$disabled=false)
    {
        parent::__construct('attributesValueForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post'
        ));
        
        $printServiceId = new Element\Select('print_service_id');
        $printServiceId->setAttributes(array(
            'id' => 'print_service_id',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'disabled' => $disabled
        ));
        $printServices = array('' => 'انتخاب کنید');
        if ($printServicesList) {
            foreach ($printServicesList as $printService) {
                $printServices[$printService['id']] = $printService['title'];
            }
        }
        $printServiceId->setValueOptions($printServices);
        $printServiceId->setLabel(__("Choose print services"));
        
        $title = new Element\Select('title');
        $title->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'name' => 'title',
//             'multiple' => true
        ));
        $attributes = array('' => 'انتخاب  نمایید');
        if ($attributesList) {
            foreach ($attributesList as $attribute) {
                $attributes[$attribute['id']] = $attribute['title'];
            }
        }
        $title->setValueOptions($attributes);
        $title->setLabel(__("Choose attributes"));
        
        $value = new Element\Select('value');
        $value->setAttributes(array(
            'id' => 'value',
            'class' => 'form-control validate[required]',
            'required' => 'required',
//             'name' => 'value[]',
            'multiple' => true
        ));
        $attributeValue = array('' => 'اضافه  نمایید');
        if ($valueList) {
            foreach ($valueList as $attribute) {
                $attributeValue[$attribute['id']] = $attribute['value'];
            }
        }
        $value->setValueOptions($attributeValue);
        $value->setLabel(__("Choose value"));
        
//         $name = new Element\Select('title');
//         $name->setAttributes(array(
//             'id' => 'title',
//             'class' => 'form-control validate[required]',
//             'placeholder' => __('Service title'),
//             'required' => 'required'
//         ));
//         $name->setLabel(__("title"));
        
        $price = new Element\Text('price');
        $price->setAttributes(array(
            'id' => 'price',
            'class' => 'form-control validate[required]',
            'placeholder' => __('price'),
            'required' => 'required'
        ));
        $price->setLabel(__("price"));
        
        $time = new Element\Text('time');
        $time->setAttributes(array(
            'id' => 'time',
            'class' => 'form-control validate[required]',
            'placeholder' => __('time'),
            'required' => 'required'
        ));
        $time->setLabel(__("time"));
        
        $salePercent = new Element\Text('sale_percent');
        $salePercent->setAttributes(array(
            'id' => 'sale_percent',
            'class' => 'form-control validate[required]',
            'placeholder' => __('Sale Percent'),
            'required' => 'required'
        ));
        $salePercent->setLabel(__("Sale Percent"));
        
        $submit = new Element\Submit("submit");
        $submit->setValue("Save and Close");
        $submit->setAttribute("class", "btn btn-primary");

        $submit2 = new Element\Submit("submit2");
        $submit2->setValue("Save and New");
        $submit2->setAttribute("class", "btn btn-primary");
        
        $this->add($title);
        $this->add($printServiceId);
        $this->add($salePercent);
        $this->add($value);
        $this->add($price);
        $this->add($time);
        $this->add($submit);
        $this->add($submit2);
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        ))); 
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
