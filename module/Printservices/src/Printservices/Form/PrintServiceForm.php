<?php
namespace PrintServices\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;

class PrintServiceForm extends Form
{

    public function __construct($printServicesList,$typesList)
    {
        parent::__construct('printServiceForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post'
        ));
        
        $parent = new Element\Select('parent_id');
        $parent->setAttributes(array(
            'id' => 'parent_id',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'name' => 'parent_id',
        ));
        $parents = array('0' => 'اصلی');
        if ($printServicesList) {
            foreach ($printServicesList as $printService) {
                $parents[$printService['id']] = $printService['title'];
            }
        }
        $parent->setValueOptions($parents);
        $parent->setLabel(__("Choose parent"));
        
        $typeId = new Element\Select('type_id');
        $typeId->setAttributes(array(
            'id' => 'type_id',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'name' => 'type_id',
            //             'multiple' => true
        ));
        $types = array('' => 'انتخاب کنید');
        if ($typesList) {
            foreach ($typesList as $type) {
                $types[$type['id']] = $type['title'];
            }
        }
        $typeId->setValueOptions($types);
        $typeId->setLabel(__("Choose services type"));
        
        $name = new Element\Text('title');
        $name->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control validate[required]',
            'placeholder' => __('Service title'),
            'required' => 'required'
        ));
        $name->setLabel(__("Service title"));
        
        $description = new Element\Textarea('description');
        $description->setAttributes(array(
            'id' => 'description',
            'class' => 'form-control',
            'placeholder' => __('print service description'),
//             'required' => 'required'
        ));
        $description->setLabel(__("print service description"));
        
        $file = new Element\File('image');
        $file->setAttributes(array(
            'id' => 'image',
            'class' => 'form-control',
            'placeholder' => __('image'),
//             'required' => 'required'
        ));
        $file->setLabel(__("image"));
        
        $submit = new Element\Submit("submit");
        $submit->setValue("Save and Close");
        $submit->setAttribute("class", "btn btn-primary");

        $submit2 = new Element\Submit("submit2");
        $submit2->setValue("Save and New");
        $submit2->setAttribute("class", "btn btn-primary");
        
        $this->add($name);
        $this->add($parent);
        $this->add($typeId);
        $this->add($description);
        $this->add($file);
        $this->add($submit);
        $this->add($submit2);
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        ))); 
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
