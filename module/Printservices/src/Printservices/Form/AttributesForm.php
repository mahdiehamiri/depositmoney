<?php
namespace PrintServices\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;

class AttributesForm extends Form
{

    public function __construct($parentList,$printServicesList,$attributesList,$typesList,$mode = null,$multiple=true,$disabled=false)
    {
        parent::__construct('attributesForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post'
        ));
        
        $typeId = new Element\Select('type_id');
        $typeId->setAttributes(array(
            'id' => 'type_id',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'name' => 'type_id',
            'disabled' => $disabled[0]
        ));
        $types = array('' => 'انتخاب کنید');
        if ($typesList) {
            foreach ($typesList as $type) {
                $types[$type['id']] = $type['title'];
            }
        }
        $typeId->setValueOptions($types);
        $typeId->setLabel(__("Choose services type"));
        
        $parentId = new Element\Select('parent_id');
        $parentId->setAttributes(array(
            'id' => 'parent_id',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'disabled' => $disabled[1]
        ));
        $parents = array('' => 'انتخاب کنید');
        if ($parentList) {
            foreach ($parentList as $parent) {
                $parents[$parent['id']] = $parent['title'];
            }
        }
        $parentId->setValueOptions($parents);
        $parentId->setLabel(__("Choose parent"));

        $printServiceId = new Element\Select('print_service_id');
        $printServiceId->setAttributes(array(
            'id' => 'print_service_id',
            'class' => 'form-control validate[required]',
            'multiple' => $multiple,
            'disabled' => $disabled[2]
        ));
        $printServices = array('' => 'انتخاب کنید');
        if($mode == 'editMode'){
            if ($printServicesList) {
                foreach ($printServicesList as $printService) {
                    $printServices[$printService['id']] = $printService['title'];
                }
            }
        }
        
        $printServiceId->setValueOptions($printServices);
        $printServiceId->setLabel(__("Choose print services"));
        
        
        $title = new Element\Select('title');
        $title->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'name' => 'title',
            'multiple' => true
        ));
        $attributes = array('' => 'انتخاب یا اضافه نمایید');
        if ($attributesList) {
            foreach ($attributesList as $attribute) {
                $attributes[$attribute['id']] = $attribute['title'];
            }
        }
        $title->setValueOptions($attributes);
        $title->setLabel(__("Choose attributes"));
        
        $submit = new Element\Submit("submit");
        $submit->setValue("Save and Close");
        $submit->setAttribute("class", "btn btn-primary");

        $submit2 = new Element\Submit("submit2");
        $submit2->setValue("Save and New");
        $submit2->setAttribute("class", "btn btn-primary");
        
        $this->add($title);
        $this->add($parentId);
        $this->add($typeId);
        $this->add($printServiceId);
        $this->add($submit);
        $this->add($submit2);
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        ))); 
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
