<?php
return array(
    'router' => array(
        'routes' => array( 
        	'printservices' => array(
        			'type'    => 'Segment',
        			'options' => array(
        					'route'    => '[/:lang]/printservices[/:action][/id/:id]',
        					'constraints' => array(
        							'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
           							'id' 	 => '[1-9][0-9]*',
        					    'lang' => '[a-zA-Z]{2}',
//         							'title' => '[a-zA-Z-][a-zA-Z0-9_-]+',
//         							'slug'  => '.+'
        					),
        					'defaults' => array(
        					    '__NAMESPACE__' => 'Printservices\Controller',
        					    'controller' => 'Index',
        					    'action' => 'index'
        					),
        			),
        	), 
        	'admin-printservices' => array(
        		'type'    => 'Segment', 
       			'options' => array(
       					'route'    => '[/:lang]/admin-printservices[/:action][/id/:id][/:slug][/group/:group][/token/:token][/clear/:clear][/export/:export][/page/:page][/count/:count]',
       					'constraints' => array(
       							'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
       							'id' 	 => '[1-9][0-9]*',
        					    'lang' => '[a-zA-Z]{2}',
       					        'slug' => '[a-zA-Z-][a-zA-Z0-9_-]+',
       					        'group' 	 => '[1-9][0-9]*',
       							'token'  => '[a-f0-9]{32}',
   					            'clear' => 'true',
       					        'export' => 'true',
           					    'count' => '[0-9]+',
           					    'page' => '[0-9]+',
       					),
       					'defaults' => array(
       							 '__NAMESPACE__' => 'Printservices\Controller',
        					    'controller' => 'Admin',
        					    'action' => 'list-services'
       				    	),
       			   ),
        	 ),   
 
            'printservices-home' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/printservices[/:action][/id/:id][/page/:page][/count/:count][/sort/:sort][/type/:type][/:slug][?:query]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' 	 => '[1-9][0-9]*',
                        'page' => '[0-9]+',
                        'count' => '[0-9]+',
                        'order' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'type' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'slug' => '.*',
                        'query' => '.*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Printservices\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index'
                    ),
                )
            ),  
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
    ),

    'view_manager' => array(
        'template_map' => array(
            include(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),	
    ),
);
