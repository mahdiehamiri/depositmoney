<?php
namespace Ourservices\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Ourservices\Form\ServicesForm;
use Zend\Session\Container;
use Ourservices\Model\ServicesTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Gallery\Model\GalleriesTable;
use Zend\Db\Sql\Where;
use Language\Model\LanguageLanguagesTable;
use Page\Model\VideoGalleriesTable;
use Ourservices\Form\ServiceForm;
use Ourservices\Form\PrintServiceForm;
use Ourservices\Model\PrintServicesTable;
use Ourservices\Form\AttributesForm;
use Ourservices\Model\AttributesTable;
use Ourservices\Model\AttributesValueTable;
use Ourservices\Model\AttributesIndexTable;
use Ourservices\Form\AttributesValueForm;
use Ourservices\Model\AttributesGroupTable;
use Menu\Model\MenuMenusTable;
use Ourservices\Model\OrdersTable;
use Ourservices\Model\OrdersIndexTable;
use Application\Helper\Jdates;
use Ourservices\Model\ServiceTable;

class AdminController extends BaseAdminController
{

    public function addServicesAction()
    {
        $lang = $this->lang;
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/services/";
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $servicesTable = new ServicesTable($this->getServiceLocator());
        
        $where = new Where();
        $where->equalTo('lang', $lang);
        $services = $servicesTable->getService($where);
        
        $galleriesTable = new GalleriesTable($this->getServiceLocator());
        $galleries = $galleriesTable->getAllGallery($lang);
        
        $videoGalleriesTable = new VideoGalleriesTable($this->getServiceLocator());
        $videoes = $videoGalleriesTable->getAllVideoGallery();
        
        $validationExt = "jpg,jpeg,png,gif,zip,rar";
        $request = $this->getRequest();
        $servicesForm = new ServicesForm($allActiveLanguages, $services, $galleries, $videoes);
        $view['servicesForm'] = $servicesForm;
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            if (! empty($files['image']['name'])) {
                $postData = array_merge($postData, $files);
            }
            $servicesForm->setData($postData);
            if ($servicesForm->isValid()) {
                $imageUploaded = false;
                $servicesForm = $servicesForm->getData();
                $filedata = '';
                $assetsArray = array();
                if (! empty($files)) {
                    if (! empty($files)) {
                        if (isset($postData['image'])) {
                            $isUploaded = $this->addImage($postData['image'], $uploadDir);
                            if (! (isset($isUploaded[0]) && $isUploaded[0])) {
                                $this->flashMessenger()->addErrorMessage(__("image Upload Problem"));
                            } else {
                                $filedata = $isUploaded[1];
                                $imageUploaded = true;
                            }
                        } else {
                            $imageUploaded = true;
                            $postData['image'] = '';
                        }
                    }
                }
                $postData['image'] = $filedata;
                $servicesData = array(
                    'name' => strip_tags(trim($postData['name'])),
                    'description' => trim($postData['description']),
                    'gallery_id' => ($postData['gallery_id'] > 0 ? $postData['gallery_id'] : 0),
                    'video_id' => ($postData['video_id'] > 0 ? $postData['video_id'] : 0),
                    'is_parent' => 1,
                    // 'parent_id' => $postData['parent_id'],
                    'parent_id' => 0,
                    'lang' => $postData['post_lang'],
                    'active' => 1,
                    'image' => $filedata
                );
                $repetedName = $servicesTable->getServicesByName($postData['name']);
                if (! $repetedName) {
                    if ($imageUploaded) {
                        $servicesId = $servicesTable->addServices($servicesData);
                        if ($servicesId) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        } else {
                            $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
                        }
                    } else {
                        $this->flashMessenger()->addErrorMessage(__("image Upload Problem"));
                    }
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Duplicated name! please choose another one!"));
                }
            } else {
                $s = $servicesForm->getMessages();
                $this->flashMessenger()->addErrorMessage(__("Validation problem check and try again!"));
            }
            return $this->redirect()->toRoute('admin-ourservices');
        }
        
        $viewModel = new ViewModel($view);
        return new ViewModel($view);
    }

    public function editServicesAction()
    {
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $lang = $this->lang;
        $galleriesTable = new GalleriesTable($this->getServiceLocator());
        $galleries = $galleriesTable->getAllGallery($lang);
        
        $videoGalleriesTable = new VideoGalleriesTable($this->getServiceLocator());
        $videoes = $videoGalleriesTable->getAllVideoGallery();
        
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/services/";
        $servicesId = $this->params('id');
        $servicesTable = new ServicesTable($this->getServiceLocator());
        $where = new Where();
        $where->equalTo('lang', $lang);
        $services = $servicesTable->getService($where);
        $myService = $servicesTable->getServiceById($servicesId);
        $isUploaded = array();
        $validationExt = "jpg,jpeg,png,gif,zip,rar";
        $request = $this->getRequest();
        if (! $myService) {
            $this->flashMessenger()->addSuccessMessage(__("There is not such a record in database."));
            return $this->redirect()->toRoute('admin-ourservices');
        }
        $servicesForm = new ServicesForm($allActiveLanguages, $services, $galleries, $videoes, true);
        $servicesForm->populateValues($myService[0]);
        $view['servicesForm'] = $servicesForm;
        if ($request->isPost()) {
            $imageUploaded = false;
            $postData = $request->getPost()->toArray();
            $servicesForm->setData($postData);
            if ($servicesForm->isValid()) {
                $servicesForm = $servicesForm->getData();
                $files = $request->getFiles()->toArray();
                if (! empty($files['image']['name'])) {
                    $postData = array_merge($postData, $files);
                }
                
                $filedata = $myService[0]['image'];
                $assetsArray = array();
                if (! empty($files)) {
                    if (isset($postData['image'])) {
                        $isUploaded = $this->addImage($postData['image'], $uploadDir);
                        if (! (isset($isUploaded[0]) && $isUploaded[0])) {
                            $this->layout()->errorMessage = __("Error in Upload Image!");
                        } else {
                            $imageUploaded = true;
                            @unlink($uploadDir . $myService[0]['image']);
                            $filedata = $isUploaded[1];
                        }
                    } else {
                        $postData['image'] = $myService[0]['image'];
                    }
                }
                $postData['image'] = $filedata;
                $servicesData = array(
                    'name' => strip_tags(trim($postData['name'])),
                    'description' => trim($postData['description']),
                    'gallery_id' => ($postData['gallery_id'] > 0 ? $postData['gallery_id'] : 0),
                    'video_id' => ($postData['video_id'] > 0 ? $postData['video_id'] : 0),
                    'is_parent' => 1,
                    'parent_id' => 0,
                    'active' => $postData['active'],
                    'lang' => $postData['post_lang'],
                    'image' => $postData['image']
                );
                // $repetedName = $servicesTable->getServicesByName($postData['name']);
                // if (! $repetedName) {
                $isUpdate = $servicesTable->updateServices($servicesData, $servicesId);
                if ($isUpdate !== false) {
                    if ($imageUploaded) {
                        @unlink($uploadDir . $myService[0]['image']);
                    }
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            } else {
                $servicesForm->getMessages();
                $this->flashMessenger()->addErrorMessage(__("Validation problem check and try again!"));
            }
            return $this->redirect()->toRoute('admin-ourservices');
        }
        $myService = $servicesTable->getServiceById($servicesId);
        $view['image'] = $myService[0]['image'];
        $viewModel = new ViewModel($view);
        return new ViewModel($view);
    }

    public function listServicesAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $servicesTable = new ServicesTable($this->getServiceLocator());
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $services = $servicesTable->getAllServicesForList();
        $grid->setTitle(__('Services list'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($services, $dbAdapter);
        
        $col = new Column\Select('id', "services");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Id'));
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('parent_id');
        $col->setLabel(__('Parrent'));
        $grid->addColumn($col);
        
        $col = new Column\Select('is_parent');
        $col->setLabel(__('Is Parrent'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-ourservices/edit-services/id/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit link'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-ourservices/delete-services/id/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete link'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-ourservices/add-services class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }

    public function deleteServicesAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $servicesId = $this->params('id', - 1);
        $servicesTable = new ServicesTable($this->getServiceLocator());
        $servicesById = $servicesTable->getServiceById($servicesId);
        $servicesChild = $servicesTable->getServicesChileById($servicesId);
        if ($servicesChild) {
            $this->flashMessenger()->addSuccessMessage(__("This Service has Child you cant delete it."));
        }
        if ($servicesId && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = $servicesTable->deleteServices($servicesId);
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            }
        }
        return $this->redirect()->toRoute('admin-ourservices');
    }

    public function addImage($validData, $uploadDir)
    {
        $newFileName = md5(time() . $validData['name']);
        $validationExt = "jpg,jpeg,png,gif";
        $isUploaded = $this->imageUploader()->UploadFile($validData, $validationExt, $uploadDir, $newFileName);
        return $isUploaded;
    }

    public function addAction()
    {
        $servicesForm = new ServiceForm();
        $view['servicesForm'] = $servicesForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            var_dump($postData);
            die();
        }
        return new ViewModel($view);
    }

    public function editAction()
    {}
    
    }
