<?php

namespace Ourservices\Controller;


use Application\Helper\BaseCustomerController;
use ProductComment\Model\ProductCommentTable;
use ProductComment\Form\ProdcutCommentForm;
use Zend\Mvc\Controller\AbstractActionController;
use Services\Model\ServicesTable;
use Configuration\Model\ConfigurationTable;
use Services\Form\ServicesForm;

class AjaxController extends AbstractActionController
{	
    public function refreshAction()
    {
        $form = new ServicesForm();
        $captcha = $form->get('myCaptcha')->getCaptcha();
        $data = array();
        $data['id']  = $captcha->generate();
        $data['src'] = $captcha->getImgUrl() .
        $captcha->getId() .
        $captcha->getSuffix();
        die(json_encode($data));
    }
    
	public function indexAction()
	{
		$request = $this->getRequest();
		
		$servicesForm = new ServicesForm();
		if ($request->isPost()) {
		    $data = $request->getPost()->toArray();
		    $servicesForm->setData($data);
		    if ($servicesForm->isValid()) {
		        $validData = $servicesForm->getData();
		        $servicesTable = new ServicesTable($this->getServiceLocator());
		        $isInsert = $servicesTable->addServices($validData);
		        $validData = $servicesForm->getData();
		        $bodyText = 'نام: ' . $validData['name'];
		        $bodyText .= "<br />" .' ایمیل: ' . $validData['email'];
		        $bodyText .= "<br />" .' تلفن: ' . $validData['tel'];
		        $bodyText .= "<br />" . ' متن: ' . $validData['text'];
		        $mails = 'info@cadafzar.com';
		
		
		        $configurationTable = new ConfigurationTable($this->getServiceLocator());
		        $allConfigs = $configurationTable->getRecords();
		        $configs = array();
		        foreach ($allConfigs as $config) {
		            $configs[$config["name"]] = $config["value"];
		        }
		        //$message = __("You received a new services");
		        //$this->SMSService()->send($message, $configs['admin-mobile']);
		        $emailSent = $this->mailService()->sendMail($bodyText, 'ایمیل از طرف فرم تماس با ما', $mails) ;
		        if ( $isInsert && $emailSent){
		            die("true");
		        } else {
		            die("false");
		        }
		    } 
		}
		die("false");
	}
	
	
}