<?php 
namespace Ourservices\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
/* use Services\Model\ServicesTable; */
use Ourservices\Form\ServicesForm;
use Ourservices\Model\UsersTable;
use Configuration\Model\ConfigurationTable;
use Application\Helper\BasePluginController;
use Zend\Db\Sql\Where;
use DropZone\Model\GalleryFilesTable;
use Application\Helper\BaseCustomerController;
use Ourservices\Model\ServicesTable;
use DropZone\Model\VideoGalleryFilesTable;

class IndexController extends BaseCustomerController
{   
   
    public function indexAction()
    { 
        $this->setHeadTitle(__('Services'));
        $servicesTable = new ServicesTable($this->getServiceLocator());
        $lang=$this->params('lang');
         
        $where = new Where();
        $where->equalTo('active', 1);
        if ($lang) {
            $where->equalTo('lang', $lang);
        }
        $services = $servicesTable->getService($where); 
    //  $view['position'] = $position;
        $view['services'] = $services;
        $viewModel = new ViewModel($view); 
        return $viewModel; 
    }
     
    public function detailAction()
    { 
        $this->setHeadTitle(__('Services detailes'));
        $serviceId = $this->params('id'); 
        $lang = $this->params('lang');
        $servicesTable = new ServicesTable($this->getServiceLocator());
        $servicesFileTable = new GalleryFilesTable($this->getServiceLocator());
        $servicesVideoFileTable = new VideoGalleryFilesTable($this->getServiceLocator());
        $fileData = array();
        $fileVideoData = array();
        $where = new Where();
        $where->equalTo('id', $serviceId);
        $where->equalTo('lang', $lang);
        $services = $servicesTable->getService($where);
        if ($services) {
            if ($services[0]['gallery_id']) {
                $where = new Where();
                $where->equalTo('gallery_id', $services[0]['gallery_id']);
                $fileData = $servicesFileTable->getData($where);
            }
            if ($services[0]['video_id']) {
                $where = new Where();
                $where->equalTo('video_gallery_id', $services[0]['video_id']);
                $fileVideoData = $servicesVideoFileTable->getData($where);
            }
        }
        $view['serviceVideoFiles'] = $fileVideoData;
        $view['serviceFiles'] = $fileData;
        $view['services'] = $services;
        $viewModel = new ViewModel($view);
      //  $viewModel->setTemplate("services/index/detail.phtml");
        return $viewModel;
    }
}
