<?php 
namespace Ourservices\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ServicesTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    { 
        $this->serviceManager = $sm;
        $this->adapter = $this->getDbAdapter();
        $this->tableGateway = new TableGateway('services', $this->adapter);
    }
	 
	public function getAllServices()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getService($where = false)
	{
	    $select = new Select('services');
	    if ($where) { 
	        $select->where($where);
	    } 
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getServiceHomePage($where = false, $limit)
	{
	    $select = new Select('services');
	    if ($where) {
	        $select->where($where);
	    }
	    $select->order('id DESC');
	    $select->limit($limit);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function searchService($services, $lang, $getPaginator = true)
	{
	    $select = new Select('services');
	    $where = new Where();
	    $select->where->nest()->like('name', "%$services%")->or->like('description', "%$services%")->unnest();
	    $select->where->equalTo('lang', $lang);
	    if ($getPaginator) {
	        $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
	        return new Paginator($dbSelector);
	    } else {
	        return $this->tableGateway->selectWith($select)->toArray();
	    } 
	}
	public function getServiceById($servicesId)
	{
	    $select = new Select('services');
	    $select->where->equalTo('id', $servicesId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getServicesChileById($servicesId)
	{
	    $select = new Select('services');
	    $select->where->equalTo('parent_id', $servicesId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getAllServicesForList()
	{
	    $select = new Select('services'); 
	    return  $select; 
	}
	public function getServicesByName($name)
	{
	    $select = new Select('services');
	    $select->where->like('name', "%$name%");
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addServices($postData)
	{ 
		$this->tableGateway->insert($postData);
		return $this->tableGateway->getLastInsertValue();
	}
	
	public function deleteServices($servicesId, $userId = null)
	{
		if ($servicesId > 0 && is_numeric($servicesId)) {
			if ($userId !== null) {
				return $this->tableGateway->delete(array(
						'id' 	  			  => $servicesId,
						'post_author_user_id' => $userId
				));
			} else {
				return $this->tableGateway->delete(array(
						'id' => $servicesId
				));
			}
		}
	}
	
	public function updateServices($menuData, $menuId)
	{ 
	    $data = array(
	        "name" => $menuData["name"],
	        "description" => $menuData["description"],
	        "is_parent" => $menuData["is_parent"],
// 	        "parent_id" => $menuData["parent_id"],
	        'lang' => $menuData['lang'],
	        "gallery_id" => $menuData["gallery_id"],
	        "active" => $menuData["active"],
	        "image" => $menuData["image"],
	    );
	    
	    return $this->tableGateway->update($data,  array('id' => $menuId));
	}
}
