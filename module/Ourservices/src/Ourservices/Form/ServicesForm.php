<?php
namespace Ourservices\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;

class ServicesForm extends Form
{

    public function __construct($allActiveLanguages,$services, $galleries = array(), $videoes = array(), $ediMode = false)
    {
        parent::__construct('servicesForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post'
        ));
        $pageLang = new Element\Select('post_lang');
        $pageLang->setAttributes(array(
            'id' => 'post_lang',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $languages = array();
        if ($allActiveLanguages) {
            foreach ($allActiveLanguages as $language) {
                $languages[$language['code']] = $language['title'];
            }
        }
        $pageLang->setValueOptions($languages);
        $pageLang->setLabel(__("Choose language"));
        $name = new Element\Text('name');
        $name->setAttributes(array(
            'id' => 'name',
            'class' => 'form-control validate[required]',
            'placeholder' => __('Service name'),
            'Lang' => 'fa-IR',
            'required' => 'required'
        ));
        $name->setLabel(__("Service name"));
        
        $pageGallery = new Element\Select('gallery_id');
        $pageGallery->setAttributes(array(
            'id' => 'page_lang',
            'class' => 'form-control',
        ));
        $gallery = array('0' => '----');
        if ($galleries) {
            foreach ($galleries as $gall) {
                $gallery[$gall['id']] = $gall['persian_name'];
            }
        }
        $pageGallery->setValueOptions($gallery);
        $pageGallery->setLabel(__("Choose gallery"));
        
        $pagevideo = new Element\Select('video_id');
        $pagevideo->setAttributes(array(
        		'id' => 'page_video',
        		'class' => 'form-control',
        ));
        $video = array('0' => '----');
        if ($videoes) {
            foreach ($videoes as $gall) {
                $video[$gall['id']] = $gall['persian_name'];
            }
        }
        $pagevideo->setValueOptions($video);
        $pagevideo->setLabel(__("Choose video")); 
       
        $active = new Element\Checkbox('active');
        $active->setAttributes(array(
            'id' => 'active', 
            'Lang' => 'fa-IR',
        ));
        $active->setLabel(__("Active Service"));
        if ($ediMode) {
            $this->add($active);
        }
        $description = new Element\Textarea("description");
        $description->setLabel("Description: ");
        $description->setAttributes(array(
            "id" => "description",
            "class" => "form-control validate[required]"
        ));
        
        /*
         * $groupParent = new Element\Select("parent_id");
         * $parentGroups = array();
         * $parentGroups["0"] = "---";
         * if ($services) {
         * foreach ($services as $group) {
         * $parentGroups[$group["id"]] = $group["name"];
         * }
         * $this->add($groupParent);
         * }
         * $groupParent->setValueOptions($parentGroups);
         * $groupParent->setLabel("Parent: ");
         * $groupParent->setAttribute('id', 'parentId');
         */
        
        $groupIsParent = new Element\Checkbox("is_parent");
        $groupIsParent->setLabel("Is Parent: ");
        
        $fileData = new Element\File('image');
        $fileData->setAttributes(array(
            'id' => 'fileUpload'
        ));
        $fileData->setLabel(__("Upload Image"));
        
//         $token = new Element\Csrf("token_services");
        
        $submit = new Element\Submit("submit");
        $submit->setValue("Save and Close");
        $submit->setAttribute("class", "btn btn-primary");

        $submit2 = new Element\Submit("submit2");
        $submit2->setValue("Save and New");
        $submit2->setAttribute("class", "btn btn-primary");
        
        $this->add($name);
        $this->add($fileData);
        $this->add($pageGallery);
        $this->add($pagevideo); 
        $this->add($pageLang);
        $this->add($description); 
        $this->add($submit);
        $this->add($submit2);
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        ))); 
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
