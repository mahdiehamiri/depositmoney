<?php
return array(
    'router' => array(
        'routes' => array( 
            'blog-home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/blog[/page/:page]',
                	'constraints' => array(
                	        'lang' => '[a-zA-Z]{2}',
                			'page' => '[1-9][0-9]*' //star is used to show that the second digit can or cannot be entered.
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Blog\Controller',
        				'controller' => 'Index',
                        'action'     => 'index',
                    ),
                ),
            ),
        	'blog-post' => array(
        			'type'    => 'Segment',
        			'options' => array(
        					'route'    => '[/:lang]/blog/:slug/:category',
        					'constraints' => array(
        					        'lang' => '[a-zA-Z]{2}',
        							'category' => '.+',
        							'slug'     => '((?!page).)*',
        					),
        					'defaults' => array(
        					        '__NAMESPACE__' => 'Blog\Controller',
        							'controller' => 'Index',
        							'action'     => 'post',
        					),
        			),
        	),
        	'blog-category' => array(
        			'type'    => 'Segment',
        			'options' => array(
        					'route'    => '[/:lang]/blog/category/:category[/page/:page]',
        					'constraints' => array(
        					        'lang' => '[a-zA-Z]{2}',
        							'category' =>  '((?!\/).)*',
        							'page'     => '[1-9][0-9]*'
        					),
        					'defaults' => array(
        					        '__NAMESPACE__' => 'Blog\Controller',
        							'controller' => 'Index',
        							'action'     => 'category',
        					),
        			),
        	),
        		'blog-link' => array(
        				'type'    => 'Segment',
        				'options' => array(
        						'route'    => '[/:lang]/blog/:id',
        						'constraints' => array(
        								'lang' => '[a-zA-Z]{2}',
        								'id'     => '[1-9][0-9]*',
        						),
        						'defaults' => array(
        								'__NAMESPACE__' => 'Blog\Controller',
        								'controller' => 'Index',
        								'action'     => 'link',
        						),
        				),
        		),
        	'blog-tag' => array(
        			'type'    => 'Segment',
        			'options' => array(
        					'route'    => '[/:lang]/blog/tag/:tag[/page/:page]',
        					'constraints' => array(
        					        'lang' => '[a-zA-Z]{2}',
        							'tag'  =>  '((?!\/).)*',
        							'page' => '[1-9][0-9]*'
        					),
        					'defaults' => array(
        					        '__NAMESPACE__' => 'Blog\Controller',
        							'controller' => 'Index',
        							'action'     => 'tag',
        					),
        			),
        	),
        	'blog-author' => array(
        			'type'    => 'Segment',
        			'options' => array(
        					'route'    => '[/:lang]/blog/author/:name[/page/:page]',
        					'constraints' => array(
        					        'lang' => '[a-zA-Z]{2}',
        							'page' => '[1-9][0-9]*',
        							'name' => '((?!\/).)*'
        					),
        					'defaults' => array(
        					        '__NAMESPACE__' => 'Blog\Controller',
        							'controller' => 'Index',
        							'action'     => 'author',
        					),
        			),
        	),    
        	'blog-feed' => array(
        			'type'    => 'Literal',
        			'options' => array(
        					'route'    => '/blog/feed',
        					'defaults' => array(
        					        '__NAMESPACE__' => 'Blog\Controller',
        							'controller' => 'Index',
        							'action'     => 'feed',
        					),
        			),
        	),	 
            'blog-admin' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-blog[/:action][/:id][/page/:page][/token/:token]', 
                        'constraints' => array(
                                'lang' => '[a-zA-Z]{2}',
                                'id' => '[1-9][0-9]*',
                                'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
                        		'token' => '[a-f0-9]{32}'
                        ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Blog\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-posts' 
                    ),
                ) 
            ),  
            'blog-admin-upload' => array(
                        'type'    => 'Literal',
                        'options' => array(
                                'route'    => '/admin-blog/upload', 
                                'defaults' => array(
                                    '__NAMESPACE__' => 'Blog\Controller',
                                    'controller' => 'Admin',
                                    'action'     => 'upload',
                                ),
                        ),
            ),   
            'blog-admin-ajax' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin-blog/ajax',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Blog\Controller',
                        'controller' => 'Ajax',
                        'action'     => 'get-categories',
                    ),
                ),
            ),
        ),
    ),  
   
    'view_manager' => array(
        'template_map' => array(
            include_once(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
