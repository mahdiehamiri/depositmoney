<?php

namespace Blog\Controller;

use Zend\View\Model\ViewModel;
use Blog\Form\CategoryForm;
use Blog\Form\PostForm;
use Zend\Session\Container;
use Blog\Model\BlogPostsTable;
use Blog\Model\BlogCategoriesTable;
use Application\Helper\BaseAdminController;
use Blog\Model\BlogPostCategoriesTable;
use Language\Model\LanguageLanguagesTable;
use Application\Model\ApplicationTagsTable;
use Application\Model\ApplicationTagsIndexTable;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;

class AdminController extends BaseAdminController
{
	protected $noOfPostsPerPage = 100;
	
	public function listPostsAction()
	{
	    $this->setHeadTitle(__('Articles list'));
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	    
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	     
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
	    if($this->userData->user_group_id == 6) {
	        $allblogs = $blogPostsTable->getPostsAdmin(true, array("post_author_user_id" => $this->userData->id));
	    } else {
	        $allblogs = $blogPostsTable->getPostsAdmin(true);
	    }
	    
	    $grid->setTitle(__('Blog List'));
	    $grid->setDefaultItemsPerPage(10);
	    $grid->setDataSource($allblogs,$dbAdapter);
	    
	    
	    $col = new Column\Select('id',"blog_posts");
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->setWidth(5);
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);  
	    
	    $col = new Column\Select('post_title');
	    $col->setLabel(__('Title'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	     
// 	    $col = new Column\Select('post_date');
// 	    $col->setLabel(__('Create date'));
// 	    $col->setWidth(20);
// 	    $col->setSortDefault(1, 'ASC');
// 	    $col->addStyle(new Style\Bold());
// 	    $grid->addColumn($col);
	    
	     
	    $col = new Column\Select('post_status');
	    $col->setLabel(__('Status'));
	    $col->setWidth(10);
	    $options = array('0' => 'inactive', '1' => 'active');
	    $col->setFilterSelectOptions($options);
	    $replaces = array('0' => 'inactive', '1' => 'active');
	    $col->setReplaceValues($replaces, $col);
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('post_lang');
	    $col->setLabel(__('Language'));
	    $col->setWidth(10);
	    $options = array('fa' => 'fa', 'en' => 'en');
	    $col->setFilterSelectOptions($options);
	    $replaces = array('fa' => 'fa', 'en' => 'en');
	    $col->setReplaceValues($replaces, $col);
	    $grid->addColumn($col);
	     
	    $col = new Column\Select('username');
	    $col->setLabel(__('Username'));
	    $col->setWidth(10);
	    $col->setSortDefault(2, 'DESC');
	    $grid->addColumn($col);
	     
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction = new Column\Action\Icon();
	    $viewAction->setIconClass('glyphicon glyphicon-edit'); 
	    $viewAction->setLink("/". $this->lang .'/admin-blog/edit-post/' . $rowId);
	    $viewAction->setTooltipTitle(__('Edit'));
	    
	    $viewAction2 = new Column\Action\Icon();
	    $viewAction2->setIconClass('glyphicon glyphicon-remove');
	    $viewAction2->setLink("/". $this->lang .'/admin-blog/delete-post/'.$rowId.'/token/'.$csrf);
	    $viewAction2->setTooltipTitle(__('Delete'));
	     
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction);
	    $actions2->addAction($viewAction2);
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	    
	    $link[] = '<a href='."/". $this->lang .'/admin-blog/add-post class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
	    $grid->setLink($link);
	     
	    $grid->render();
	    
	    return $grid->getResponse();   
	}
	
	private function uploadImage($files, $index = NULL)
	{
	    $config = $this->serviceLocator->get('Config');
	    $uploadDir = $config['base_route'] . "/images/blog/";
	    $validationExt ="jpg,jpeg,png,gif";
	    $postImage = array();
	    foreach ($files as $k=>$file) {
	        $newFileName = md5(time() . $file['name']);
	        $isUploaded = $this->imageUploader()->UploadFile($file, $validationExt, $uploadDir, $newFileName);
	        if ($isUploaded[0])
	            $postImage[] = @$isUploaded[1];
	    }
	
	    if ($index !== NULL && count($postImage))
	        return $postImage[$index];
	    if (count($postImage))
	        return $postImage;
	    return null;
	}
	
	public function addPostAction()
	{
	    $this->setHeadTitle(__('Add new article'));
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
	    
	    $blogCategoriesTable = new BlogCategoriesTable($this->getServiceLocator());
	    $allCategories = $blogCategoriesTable->getCategories();
	    $postForm = new PostForm($allActiveLanguages, $allCategories, null, $this->lang);
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $tags = array();
	        $tags = $postData['tag_title'];
	        $data = $postData->toArray();
	        $files =  $request->getFiles()->toArray();
	        $data = array_merge($data, $files);
	        if($this->userData->user_group_id == 6) {
	            $postData['post_status'] = 0;
	        }
	        if ($postForm->setData($postData)->isValid()) {
	            $postData = $postForm->getData();
	            $postData['tag_title'] = $tags;
	            
	            $postData['post_author_user_id'] = $this->userData->id;
	            $postData['post_title'] = str_replace("/","",$postData['post_title']);
	            $slug = mb_substr($postData['post_title'], 0, 40);
	            $postData['post_slug'] = str_replace(' ', '-', trim($slug)); 
	            $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
	            try {
	                $postData['post_image'] = $this->uploadImage($files,0); 
	                $newPostId = $blogPostsTable->addPost($postData);
	                if ($newPostId) {
	                    if (isset($postData['tag_title'])) {
	                    	$applicationTagsTable = new ApplicationTagsTable($this->getServiceLocator());
	                    	$applicationTagsIndexTable = new ApplicationTagsIndexTable($this->getServiceLocator());
	                        
	                        foreach($postData['tag_title'] as $tagId) {
	                        	$tagExists = $applicationTagsTable->getTagByName($tagId);
	                        	if ($tagExists) {
	                        		$tagById = $tagExists['id'];
	                        	} else {
	                        		$tagById = $applicationTagsTable->insertTag($tagId);
	                        	}
	                        	$applicationTagsIndexTable->insertTagIndex($tagById, $newPostId, 'blog_post');
	                        }
	                        
	                    }
	                    
	                    if (isset($_POST['submit'])) {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('blog-admin');
	                    } else {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('blog-admin', array(
	                            'action' => 'add-post'
	                        ));
	                    }
	                    
	                } else {
	                    $this->layout()->errorMessage = __("Operation failed!");
	                }
	            } catch (\Exception $e) {
	                $this->layout()->errorMessage = __("The title is taken, please choose another one.");
	            }
	        }
	        else {
	             $this->layout()->errorMessage = __("Forn Is not Valid.");
	        }
	    }
	    $view['postForm'] = $postForm;
	    return new ViewModel($view);   
	}
	
	public function editPostAction()
	{
	    $this->setHeadTitle(__('Edit post'));
	    $userId = $this->userData->id;
	    $postId = $this->params('id', -1);
	    if (!$postId) {
	        return $this->redirect()->toRoute('blog-admin');
	    }
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
	    
	    //TODO
	    $applicationTagsTable = new ApplicationTagsTable($this->getServiceLocator());
	    $applicationTagsIndexTable = new ApplicationTagsIndexTable($this->getServiceLocator());
	    $tagsOfPost = $applicationTagsIndexTable->fetchAllTags($postId, 'blog_post');
	    
	    $blogCategoriesTable = new BlogCategoriesTable($this->getServiceLocator());
	    $allCategories = $blogCategoriesTable->getCategories();
	    $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
	    $blogPost = $blogPostsTable->getPostById($postId);
	
	    if (!$blogPost || ($this->userData->user_group_id == 6 && $this->userData->id != $blogPost[0]["post_author_user_id"])) {
	        return $this->redirect()->toRoute('blog-admin');
	    } else {
	        $postForm = new PostForm($allActiveLanguages, $allCategories, $tagsOfPost, $this->lang);
	        if (isset($blogPost[0]['tag_title'])) {
	            $blogPost[0]['tag_title'] = explode(',', $blogPost[0]['tag_title']);
	        }
			
	        $postForm->populateValues($blogPost[0]);
	        $request = $this->getRequest();
	        if ($request->isPost()) {
	            $data = $request->getPost();
	            $tags = array();
	            $tags = $data['tag_title'];
	            $_data = $data->toArray();
	            $files =  $request->getFiles()->toArray();
	            $_data = array_merge($_data, $files);
	            if($this->userData->user_group_id == 6) {
	                $_data['post_status'] = 0;
	            }
	            $postForm->setData($_data);
	            if ($postForm->isValid()) {
	                $editData = $postForm->getData(); 
	                $editData['tag_title'] = $tags; 
	                if (empty($files['post_image']['name']) == false) {
	                    $editData['post_image'] = $this->uploadImage($files,0);
	                }
	                else {
	                    $editData['post_image'] = $blogPost[0]['post_image'];
	                }
	                $applicationTagsIndexTable->deleteTags($postId, 'blog_post');
	                if (isset($editData['tag_title'])) {
	                    foreach($editData['tag_title'] as $tagId) {
	                    	$tagExists = $applicationTagsTable->getTagByName($tagId);
	                    	if ($tagExists) {
	                    		$tagById = $tagExists['id'];
	                    	} else {
	                    		$tagById = $applicationTagsTable->insertTag($tagId);
	                    	}
	                    	$applicationTagsIndexTable->insertTagIndex($tagById, $postId, 'blog_post');
	                    }
	                }
	                $slug = mb_substr($editData['post_title'], 0, 40);
	                $editData['post_slug'] = str_replace(' ', '-', trim($slug));
	                try {
	                    
	                    $isEditted  = $blogPostsTable->editPost($editData, $postId);
	                   
	                    if ($isEditted !== false) {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('blog-admin');
	                    } else {
	                           $this->layout()->errorMessage = __("Operation failed!");
	                      }
	                } catch (\Exception $e) {
	                    $this->layout()->errorMessage = __("The title is taken, please choose another one.");
	                }
	            }
	        }
	        $view['post'] = $blogPost;
	        $view['postForm'] = $postForm;
	        return new ViewModel($view);
	    }
	}

	public function deletePostAction()
	{
		$config = $this->serviceLocator->get('Config');
		$uploadDir = $config['base_route'];
		
		$container = new Container('token');
		$postId = $this->params('id', -1);
		$token = $this->params('token');
		$userId = $this->userData->id;
	    $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
		$postById = $blogPostsTable->getPostById($postId);
		if ($postById) {
		    $html = $postById[0]['post_exerpt'];
		    $doc = new \DOMDocument();
		    $doc->loadHTML($html);
		    $xpath = new \DOMXPath($doc);
		    $imageSrc = $xpath->evaluate("//img");
		    for ($i=0; $i<= count($imageSrc);$i++) {
		        if ($imageSrc->item($i)) {
		            $src = $imageSrc->item($i)->attributes->getNamedItem('src')->nodeValue;
		            @unlink($uploadDir . $src);
		        }
		    }
		    $html = $postById[0]['post_content'];
		    $doc = new \DOMDocument();
		    $doc->loadHTML($html);
		    $xpath = new \DOMXPath($doc);
		    $imageSrc = $xpath->evaluate("//img");
		    for ($i=0; $i<= count($imageSrc);$i++) {
		        if ($imageSrc->item($i)) {
		            $src = $imageSrc->item($i)->attributes->getNamedItem('src')->nodeValue;
		            @unlink($uploadDir . $src);
		        }
		    }
		    @unlink($uploadDir . "/images/blog/" . $postById[0]['post_image']);
		    $canDeleteAllPosts = $this->permissions()->getPermissions('blog-admin-can-delete-all-posts');
		    if ($postId && $container->offsetExists('csrf')) {
		        if ($container->offsetGet('csrf') == $token) {
		            if ($canDeleteAllPosts) {
		                $userId = null;
		            }
		            $isDeleted = $blogPostsTable->deletePost($postId, $userId);
		            if ($isDeleted) {
		                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
		            } else {
		                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
		            }
		        }
		    }
		}
		return $this->redirect()->toRoute('blog-admin');
	}
	
	public function listCategoriesAction()
	{
	    $this->setHeadTitle(__('Categories list'));
		$contanier = new Container('token');
		$csrf = md5(time() . rand());
		$contanier->csrf = $csrf;
		 
		$grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
		
		$config = $this->getServiceLocator()->get('Config');
		$dbAdapter = new Adapter($config['db']);
		$blogCategoriesTable = new BlogCategoriesTable($this->getServiceLocator());
		$allcategories = $blogCategoriesTable->getCategories();
		$grid->setTitle(__('Category List'));
		$grid->setDefaultItemsPerPage(10);
		$grid->setDataSource($allcategories,$dbAdapter);
		 
		 
		$col = new Column\Select('id');
		$col->setLabel(__('Row'));
		$col->setIdentity();
		$col->setWidth(5);
		$col->addStyle(new Style\Bold());
		$grid->addColumn($col);
		 
		$col = new Column\Select('category_title');
		$col->setLabel(__('Title'));
		$col->setWidth(20);
		$col->setSortDefault(1, 'ASC');
		$col->addStyle(new Style\Bold());
		$grid->addColumn($col);
		
		$col = new Column\Select('category_status');
		$col->setLabel(__('Status'));
		$col->setWidth(10);
		$options = array('0' => 'inactive', '1' => 'active');
		$col->setFilterSelectOptions($options);
		$replaces = array('0' => 'inactive', '1' => 'active');
		$col->setReplaceValues($replaces, $col);
		$grid->addColumn($col);
		
		$col = new Column\Select('category_lang');
		$col->setLabel(__('Language'));
		$col->setWidth(10);
		$options = array('fa' => 'fa', 'en' => 'en');
		$col->setFilterSelectOptions($options);
		$replaces = array('fa' => 'fa', 'en' => 'en');
		$col->setReplaceValues($replaces, $col);
		$grid->addColumn($col);
		
		$btn = new Column\Action\Button();
		$btn->setLabel(__('Edit'));
		$rowId = $btn->getRowIdPlaceholder();
		 
		$viewAction = new Column\Action\Icon();
		$viewAction->setIconClass('glyphicon glyphicon-edit');
		$viewAction->setLink("/". $this->lang .'/admin-blog/edit-category/' . $rowId);
		$viewAction->setTooltipTitle(__('Edit'));
		 
		$viewAction2 = new Column\Action\Icon();
		$viewAction2->setIconClass('glyphicon glyphicon-remove');
		$viewAction2->setLink("/". $this->lang .'/admin-blog/delete-category/'.$rowId.'/token/'.$csrf);
		$viewAction->setTooltipTitle(__('Delete'));
		
		$actions2 = new Column\Action();
		$actions2->setLabel(__('Operations'));
		$actions2->addAction($viewAction);
		$actions2->addAction($viewAction2);
		$actions2->setWidth(15);
		$grid->addColumn($actions2);
		 
		$link[] = '<a href='."/". $this->lang .'/admin-blog/add-category class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
		$grid->setLink($link);
		
		$grid->render();
		 
		return $grid->getResponse();
	}
	
	public function addCategoryAction()
	{ 
	    $this->setHeadTitle(__('Add new category'));
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
		$categoryForm = new CategoryForm($allActiveLanguages);
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost()->toArray();
	        $categoryForm->setData($data);
	        if ($categoryForm->isValid()) {
	            $categoryData = $categoryForm->getData();
	            $blogCategoriesTable = new BlogCategoriesTable($this->getServiceLocator());
	            $isAdded = $blogCategoriesTable->addCategory($categoryData);
	            if ($isAdded) {
	                
	                if (isset($_POST['submit'])) {
	                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                    return $this->redirect()->toRoute('blog-admin', array(
	                        "action" => "list-categories"
	                    ));
	                } else {
	                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                    return $this->redirect()->toRoute('blog-admin', array(
	                        "action" => "add-category"
	                    ));
	                }
	            } else {
	                $this->layout()->errorMessage = __("Operation failed!");
	            }
	        } else {
	            $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($categoryForm, true);
	    
	        }
	    }
	    $view['categoryForm'] = $categoryForm;
	    return new ViewModel($view);
	}
	
	public function editCategoryAction()
	{
	    $this->setHeadTitle(__('Edit category'));
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
		$categoryForm = new CategoryForm($allActiveLanguages);
	    $categoryId = $this->params('id');
	    $blogCategoriesTable = new BlogCategoriesTable($this->getServiceLocator());
	    $categoryData = $blogCategoriesTable->getCategoryById($categoryId);
	    if (!$categoryData) {
	        return $this->redirect()->toRoute('blog-admin', array(
	                   "action" => "list-categories"
	               ));
	    }
	    $categoryForm->populateValues($categoryData[0]);
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost()->toArray();
	        $categoryForm->setData($data);
	        if ($categoryForm->isValid()) {
	            $validData = $categoryForm->getData();
	            $isUpdated = $blogCategoriesTable->editCategory($validData, $categoryId);
	            if ($isUpdated !== false) {
	                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                return $this->redirect()->toRoute('blog-admin', array(
	                           "action" => "list-categories"
	                       ));
	            } else {
	                $this->layout()->errorMessage = __("Operation failed!");
	            }
	        } else {
	            $this->layout()->errorMessage = $this->generalHelper()->getFormErrors($categoryForm, true);
	        }
	    }
	    $view['categoryForm'] = $categoryForm;
	    return new ViewModel($view);
	}
	
    public function deleteCategoryAction()
	{
	    $container = new Container('token');
	    $token = $this->params('token');
	    $categoryId = $this->params('id');
	    if ($categoryId && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $blogCategoriesTable = new BlogCategoriesTable($this->getServiceLocator());
            $isDeleted = $blogCategoriesTable->deleteCategory($categoryId);
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            }
	    }
	     return $this->redirect()->toRoute('blog-admin', array(
	                   "action" => "list-categories"
	            ));
	}
}
