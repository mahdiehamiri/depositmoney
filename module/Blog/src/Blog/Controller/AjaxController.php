<?php

namespace Blog\Controller;

use Application\Helper\BaseAdminController;
use Permission\Model\PermissionAllPermissionsTable;
use Permission\Model\PermissionPermissionsTable;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Blog\Model\BlogCategoriesTable;
use Zend\View\Model\JsonModel;

class AjaxController extends BaseAdminController
{	
    
	public function onDispatch(MvcEvent $e)
	{
	    parent::onDispatch($e);
		$this->layout("layout/empty.phtml");
	}

	public function getCategoriesAction()
	{
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost()->toArray();
	        $blogCategoriesTable = new BlogCategoriesTable($this->getServiceLocator());
	        $categoryData = $blogCategoriesTable->getCategoryByLanguage($data["language_id"]);
	        die(json_encode($categoryData));
	    }
	    else
	        die('error');
	}
	
}