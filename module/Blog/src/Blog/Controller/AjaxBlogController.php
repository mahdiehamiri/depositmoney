<?php

namespace Blog\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Application\Model\ApplicationFileManagerTable;
use Application\Helper\BaseController;
use Shop\Model\EducationLessonsTable;
use Shop\Model\ShopProductsTable;
use Application\Helper\BaseAdminController;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Shop\Model\ProvincesTable;
use Shop\Model\CitiesTable;
use Blog\Model\PostsVisitCountTable;

class AjaxBlogController extends BaseController
{	
   
    public function addPostVisitCountAction()
    { 
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$id = $request->isPost('id');  
    		$ip = $_SERVER["REMOTE_ADDR"];
    		$data = $request->getPost()->toArray();
    		$blogCategoriesTable = new PostsVisitCountTable($this->getServiceLocator());
    		$resuldata = $blogCategoriesTable->getDataByUserIpPosts($id, $ip);
    	 
    		if (!$resuldata) {
    			$postData = array(  
    					'post_id' => $id, 
    					'ip' => $ip,  
    			);
    			$blogCategoriesTable->addPostVisit($postData);
    		}
    		$allViewPostData = $blogCategoriesTable->getDataByUserIpPosts($id);
    		if ($allViewPostData) {
    			$countPost = count($allViewPostData);
    			echo json_encode($countPost);
	    		die();
    		}
    	}
     
    }

}
