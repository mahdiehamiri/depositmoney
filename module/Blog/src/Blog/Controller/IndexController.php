<?php

namespace Blog\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Blog\Model\BlogPostsTable;
use Blog\Model\BlogCategoriesTable;
use Application\Helper\BaseController;
use Application\Model\ApplicationTagsIndexTable;
use Application\Model\ApplicationEntityViewsTable;

class IndexController extends BaseController
{
	protected $noOfPostsPerPage = 20;
	protected $latestBlogPostsLimit = 4;
	
	public function onDispatch(MvcEvent $e)
	{ 
		$blogCategoryTable = new BlogCategoriesTable($this->getServiceLocator());
		$applicationEntityViewsTable = new ApplicationEntityViewsTable($this->getServiceLocator());
		$this->layout()->mostVisitedBlogPosts = $applicationEntityViewsTable->fetchMostVisitedBlogPosts('blog_posts', 10, 604800);
		
		$this->layout()->blogCategories = $blogCategoryTable->getCategories();
		parent::onDispatch($e);
	}
	
    public function indexAction()
    {  
        if ($this->params('page')) {
        	$view['categorieId'] = $this->params('page');
        }
     
        $this->setHeadTitle(__('News'));
    	$category = $this->params('category', null); 
    	$tag = $this->params('tag', null);
    	
        $blogPostsTable = new BlogPostsTable($this->getServiceLocator());
    	$paginator = $blogPostsTable->getPosts($category, null, null, $tag, true, $this->lang);
    	$page = $this->params('page');
    	if ($page == 1) {
    		return $this->redirect()->toRoute('blog-home');
    	}
    	$paginator->setCurrentPageNumber($this->params('page', 1));
    	$paginator->setItemCountPerPage($this->noOfPostsPerPage);
    	$view['postList'] = $paginator;
        return new ViewModel($view);
    }
    
    public function postAction()
    { 
    	$userIp = getenv('HTTP_CLIENT_IP')?:
    	getenv('HTTP_X_FORWARDED_FOR')?:
    	getenv('HTTP_X_FORWARDED')?:
    	getenv('HTTP_FORWARDED_FOR')?:
    	getenv('HTTP_FORWARDED')?:
    	getenv('REMOTE_ADDR');
    	$isnumber = false;
    	$slug = $this->params('slug'); 
    	$category = $this->params('category'); 
    	$blogPostsTable = new BlogPostsTable($this->getServiceLocator());
    	$blogPost = $blogPostsTable->getPosts($category, $slug, null, null, null, $this->lang, null,1,false); 
    	$blogArchive = $blogPostsTable->getAllPostsForArchive(15);
    	if (!$blogPost) {
    		return $this->redirect()->toRoute('blog-home');
    	} else if ((!isset($blogPost[0]['id']) || !$blogPost[0]['id'])) {
    		return $this->redirect()->toRoute('blog-home');
    	} 
    	$applicationTagsIndexTable = new ApplicationTagsIndexTable($this->getServiceLocator());
    	$postTags = $applicationTagsIndexTable->fetchAllTags($blogPost[0]['id'], 'blog_post');
    	$tags = array();  
    	if ($postTags) {
    		foreach ($postTags as $key => $postTag) {
    			$blogPostTags = $blogPostsTable->getPosts(null, null, null, $postTag['tag_title'], null, $this->lang, $blogPost[0]['id'], 10);
    			if ($blogPostTags) {
    				foreach ($blogPostTags as $blogPostTag) {
    					$tags[$blogPostTag['id']] = $blogPostTag;
    				}
    			}
    		}
    	} 
        $postEspacialTag = $blogPost[0]['post_tags'];
    	$view['posttagsData'] = $postEspacialTag;
    	$view['entityId'] = $blogPost[0]['id'];
    	$view['relatedBlogPosts'] = $tags;
    	$view['blogArchive'] = $blogArchive; 
    	if ($blogPost) {
    		$view['blogPost'] = $blogPost;
    		$this->setHeadTitle($blogPost[0]['post_title']);
    		$applicationEntityViewsTable = new ApplicationEntityViewsTable($this->getServiceLocator());
    		if ($this->identity()) {
    			$applicationEntityViewsTable->insertHit('blog_posts', $blogPost[0]['id'], $this->identity()->id, $userIp);
    		} else {
    			$applicationEntityViewsTable->insertHit('blog_posts', $blogPost[0]['id'], 0, $userIp);
    		}
    		$view['blogPost'] = $view['blogPost'][0];
    		return new ViewModel($view);
    			
    	} else {
    		return $this->redirect()->toRoute('blog-home');
    	}
    	 
    }
    
    public function linkAction()
    {
    	$userIp = getenv('HTTP_CLIENT_IP')?:
    	getenv('HTTP_X_FORWARDED_FOR')?:
    	getenv('HTTP_X_FORWARDED')?:
    	getenv('HTTP_FORWARDED_FOR')?:
    	getenv('HTTP_FORWARDED')?:
    	getenv('REMOTE_ADDR');
    	$isnumber = false;
    	$id = $this->params('id');
 
    	$category = $this->params('category');
    
    	$blogPostsTable = new BlogPostsTable($this->getServiceLocator());
    	$blogPost = $blogPostsTable->getPostsById($category, $id, null, null, null, $this->lang, null,1,false);
    $blogArchive = $blogPostsTable->getAllPostsForArchive(15);
    	if (!$blogPost) {
    		return $this->redirect()->toRoute('blog-home');
    	} else if ((!isset($blogPost[0]['id']) || !$blogPost[0]['id'])) {
    		return $this->redirect()->toRoute('blog-home');
    	}
    
    	$applicationTagsIndexTable = new ApplicationTagsIndexTable($this->getServiceLocator());
    	$postTags = $applicationTagsIndexTable->fetchAllTags($blogPost[0]['id'], 'blog_post');
    	$tags = array(); 
 
 
    	if ($postTags) {
    		foreach ($postTags as $key => $postTag) {
    			$blogPostTags = $blogPostsTable->getPosts(null, null, null, $postTag['tag_title'], null, $this->lang, $blogPost[0]['id'], 10);
    			if ($blogPostTags) {
    				foreach ($blogPostTags as $blogPostTag) {
    					$tags[$blogPostTag['id']] = $blogPostTag;
    				}
    			}
    		}
    	}
   
    	 
        $postEspacialTag = $blogPost[0]['post_tags'];
    	$view['posttagsData'] = $postEspacialTag;
    	$view['entityId'] = $blogPost[0]['id'];
    	$view['relatedBlogPosts'] = $tags;
    	$view['blogArchive'] = $blogArchive;
    
    	if ($blogPost) {
    		$view['blogPost'] = $blogPost;
    		$this->setHeadTitle($blogPost[0]['post_title']);
    		$applicationEntityViewsTable = new ApplicationEntityViewsTable($this->getServiceLocator());
    		if ($this->identity()) {
    			$applicationEntityViewsTable->insertHit('blog_posts', $blogPost[0]['id'], $this->identity()->id, $userIp);
    		} else {
    			$applicationEntityViewsTable->insertHit('blog_posts', $blogPost[0]['id'], 0, $userIp);
    		}
    		$view['blogPost'] = $view['blogPost'][0];
    		return new ViewModel($view);
    			
    	} else {
    		return $this->redirect()->toRoute('blog-home');
    	}
    	 
    	
    }
    
    
    public function categoryAction()
    {
    	$category = $this->params('category');
    	$this->setHeadTitle($category . " " . __('articles'));
    	$blogPostsTable = new BlogPostsTable($this->getServiceLocator());
    	$paginator = $blogPostsTable->getPosts($category, null, null, null, true, $this->lang);
    	$page = $this->params('page');
    	if ($page == 1) {
    		return $this->redirect()->toRoute('blog-category', array(
    				'category'  => $category
    		));
    	}
    	$paginator->setCurrentPageNumber($this->params('page', 1));
    	$paginator->setItemCountPerPage($this->noOfPostsPerPage);
    	$view['postList'] = $paginator;
    	$view['category'] = $category;
    	
        return new ViewModel($view);
    }
    
     
    public function tagAction()
    {
    	$tag = $this->params('tag');
    	$this->setHeadTitle(__('All articles with') . " " . $tag);
    	$blogPostsTable = new BlogPostsTable($this->getServiceLocator());
    	$paginator = $blogPostsTable->getPosts(null, null, null, $tag, true, $this->lang);
    	$page = $this->params('page');
    	if ($page == 1) {
    		return $this->redirect()->toRoute('blog-tag', array(
    				'tag'  => $tag
    		));
    	}
    	$paginator->setCurrentPageNumber($this->params('page', 1));
    	$paginator->setItemCountPerPage($this->noOfPostsPerPage);
    	$view['postList'] = $paginator;
    	$view['tag'] = $tag;
    	
        return new ViewModel($view);
    }
    
    public function authorAction()
    {
    	$autherName = $this->params('name');
    	$this->setHeadTitle($autherName . "`s " . __('articles'));
    	$blogPostsTable = new BlogPostsTable($this->getServiceLocator());
    	$paginator = $blogPostsTable->getPosts(null, null, $autherName, null, true);
    	$page = $this->params('page');
    	if ($page == 1) {
    		return $this->redirect()->toRoute('blog-author', array(
    				'name' => $autherName
    		));
    	}
    	$paginator->setCurrentPageNumber($this->params('page', 1));
    	$paginator->setItemCountPerPage($this->noOfPostsPerPage);
    	$view['postList'] = $paginator;
    	$view['authorName'] = $autherName;
    	
        return new ViewModel($view);
    }
    
    public function feedAction()
    {
    	$feed = new \Zend\Feed\Writer\Feed;
    	$config = $this->getServiceLocator()->get('Config');
    	$feed->setTitle('Blog feed');
    	$feed->setLink($config["site_url"]);
    	$feed->setFeedLink($config["site_url"] . '/blog/feed', 'atom');
    	$feed->setDateModified(time());
    	$feed->addHub('http://pubsubhubbub.appspot.com/');
    	$blogPostsTable = new BlogPostsTable($this->getServiceLocator());
    	$blogPosts = $blogPostsTable->getLastestBlogPosts($this->latestBlogPostsLimit);
    	if ($blogPosts) {
    		foreach ($blogPosts as $post) {
    			$feedUrl = $this->urlGeneratorInController()->postUrl($post);
		    	$entry = $feed->createEntry();
		    	$entry->setTitle($post['post_title']);
		    	$entry->setLink($feedUrl);
		    	$entry->addAuthor(array(
		    			'name'  => $post['firstname'] . ' ' . $post['lastname']
		    	));
		    	$entry->setDateModified(time());
		    	if ($post['post_exerpt']) {
			    	$entry->setDescription($post['post_exerpt']);
		    	}
		    	$post['post_exerpt'] = preg_replace("/&#?[a-z0-9]+;/i","", $post['post_exerpt']);
		    	$entry->setContent($post['post_exerpt']);
		    	$feed->addEntry($entry);
		    	$feed->setEncoding('US-ASCII');
    		}
    	}
    	echo $feed->export('atom');
    	die;
    }
}
