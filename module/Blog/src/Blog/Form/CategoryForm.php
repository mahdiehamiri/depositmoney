<?php

namespace Blog\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class CategoryForm extends Form
{
	public function __construct($allActiveLanguages)
	{
		parent::__construct('categoryForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
    		    'id'      => 'categoryForm',
    		    'novalidate'=> true
		));

		
		$categoryLang = new Element\Select('category_lang');
		$categoryLang->setAttributes(array(
		    'id' => 'category_lang',
		    'class' => 'form-control validate[required]',
		    'required' => 'required'
		));
		$languages = array();
		if ($allActiveLanguages) {
		    foreach ($allActiveLanguages as $language) {
		        $languages[$language['code']] = $language['title'];
		    }
		}
		$categoryLang->setValueOptions($languages);
		$categoryLang->setLabel('Choose Language');

		$categoryTitle = new Element\Text('category_title');
		$categoryTitle->setLabel(__("Category title"));
		$categoryTitle->setAttributes(array(
		    'id'    => 'category_title',
		    'class' => 'form-control validate[required]',
		    'required' => 'required',
		    'placeholder' => __('Category title'),
		));
		
		$categoryStatus = new Element\Checkbox('category_status');
		$categoryStatus->setAttributes(array(
		    'id'    => 'category_category',
		    'class' => ''
		));
		$categoryStatus->setLabel(__("Status"));

		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save and Close"));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(__("Save and New"));
		$submit2->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));

		$this->add($categoryLang)
    		 ->add($categoryTitle)
    		 ->add($categoryStatus)
    		 ->add($csrf)
    		 ->add($submit)
    		 ->add($submit2);
		$this->inputFilters();
	}
	public function inputFilters()
	{
	    $inputFilter = new InputFilter();
	    $factory     = new InputFactory();
	    $inputFilter->add($factory->createInput(
	        array(
	            'name'     => 'category_title',
	            'required' => true,
	            'filters'  => array(
	                array(
	                    'name' => 'StripTags'
	                )
	            ),
	        )
	    ));
	
	    $this->setInputFilter($inputFilter);
	    return $inputFilter;
	}
}