<?php

namespace Blog\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class PostForm extends Form
{
	public function __construct($allActiveLanguages, $allCategories, $tagsOfPost = null,$currentLang = 'fa')
	{
		parent::__construct('postForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
    		    'id'      => 'postForm',
    		    'novalidate'=> true
		));
		
		$pageLang = new Element\Select('post_lang');
		$pageLang->setAttributes(array(
		    'id' => 'post_lang',
		    'class' => 'form-control validate[required]',
		    'required' => 'required'
		));
		$languages = array();
		if ($allActiveLanguages) {
		    foreach ($allActiveLanguages as $language) {
		        $languages[$language['code']] = $language['title'];
		    }
		}
		$pageLang->setValueOptions($languages);
		$pageLang->setLabel(__("Choose language"));
		
		$postTitle = new Element\Text('post_title');
		$postTitle->setAttributes(array(
				'id'    => 'post_title',
				'class' => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder' => __('Article title'),
		));
		$postTitle->setLabel(__("Article title"));
		
		$postSource = new Element\Text('post_source');
		$postSource->setAttributes(array(
				'id'    => 'post_source',
				'class' => 'form-control',
		        'required' => 'required',
		        'placeholder' => __('Article Source'),
		));
		$postSource->setLabel(__("Article Source"));
		
		$postSlug = new Element\Text('post_slug');
		$postSlug->setAttributes(array(
				'id'    => 'post_slug',
				'class' => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder' => __('Article URL'),
		));
		$postSlug->setLabel(__("Article URL"));
		
		$image = new Element\File('post_image');
		$image->setAttributes(array(
				'id'    => 'fileUpload1'
		));
		$image->setLabel(__("Image"));
		
		$postExerpt = new Element\Textarea('post_exerpt');
		$postExerpt->setAttributes(array(
				'id'    => 'post_exerpt',
				'class' => 'form-control',
		        'placeholder' => __('Intro text'),
		));
		$postExerpt->setLabel(__("Intro text"));

		$postContent = new Element\Textarea('post_content');
		$postContent->setAttributes(array(
				'id'    => 'post_content',
				'class' => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder' => __('Main text'),
		));
		$postContent->setLabel(__("Main text"));
		
		$postTags = new Element\Select('tag_title');
		$postTags->setLabel(__("Article tags"));
		$postTags->setAttributes(array(
				'id'          => 'tag-title',
				'class'       => 'form-control',
				'multiple'    => true
		));
		$tags = array();
		if ($tagsOfPost) {
			foreach ($tagsOfPost as $tag) {
				$tags[$tag['tag_title']] = $tag['tag_title'];
			}
		}
	   $postTags->setValueOptions($tags);
		
		$postMetaDescription = new Element\Textarea('post_meta_description');
		$postMetaDescription->setAttributes(array(
				'id'    => 'post_meta_description',
				'class' => 'form-control',
		        'placeholder' => __('Meta description'),
		));
		$postMetaDescription->setLabel(__("Meta description"));
		
		$postMetaKeywords = new Element\Textarea('post_meta_keywords');
		$postMetaKeywords->setAttributes(array(
				'id'    => 'post_meta_keywords',
				'class' => 'form-control',
		        'placeholder' => __('Meta keywords'),
		));
		$postMetaKeywords->setLabel(__("Meta keywords"));
		
		$postCategoryId = new Element\Select('post_category_id');
		$postCategoryId->setAttributes(array(
				'id' => 'post_category',
				'class' => 'form-control',
		));
		$categories = array();
		if ($allCategories) {
			foreach ($allCategories as $category) {
					$categories[$category['id']] = $category['category_title'];				
			}
		}
		
		$postCategoryId->setValueOptions($categories);
		

		$postCategoryId->setLabel(__("Choose article category"));
		
		
		
		
		
		
		$postSourceTitle = new Element\Text('post_source_title');
		$postSourceTitle->setAttributes(array(
				'class' => 'form-control left-to-right-form-element',
				'placeholder' => 'Source Title e.g A New Technology Is Going to ... '
		));
		$postSourceTitle->setLabel(__("Reference title"));
		
		$postSourceUrl = new Element\Text('post_source_url');
		$postSourceUrl->setAttributes(array(
				'class' => 'form-control left-to-right-form-element',
				'placeholder' => 'Source Link e.g http://www.'
		));
		$postSourceUrl->setLabel(__("Reference url"));
		
		$postStatus = new Element\Checkbox('post_status');
		$postStatus->setAttributes(array(
				'id'    => 'post_category',
				'class' => 'form-dontrol'
		));
		$postStatus->setLabel(__("Is published"));
		
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save and Close"));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(__("Save and New"));
		$submit2->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		
		$this->add($postTitle)
			 ->add($postSlug)
			 ->add($postSource)
			 ->add($image)
			 ->add($postExerpt)
			 ->add($postContent)
			 ->add($postTags)
			 ->add($postMetaDescription)
			 ->add($postMetaKeywords)
			 ->add($postCategoryId)
			 ->add($pageLang)
			 ->add($postStatus)
			 ->add($postSourceTitle)
			 ->add($postSourceUrl)
			 ->add($csrf)
			 ->add($submit)
			 ->add($submit2);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'post_title',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'post_meta_description',
				'required' => false,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(  
			array(
				'name'     => 'post_meta_keywords',
				'required' => false,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(  
			array(
				'name'     => 'post_source_url',
				'required' => false,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(  
			array(
				'name'     => 'post_source_title',
				'required' => false,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
	
	public function isValid()
	{
		$this->getInputFilter()->remove('tag_title');
		return parent::isValid();
	}
}
