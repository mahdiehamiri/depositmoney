<?php

namespace Blog\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class PostsVisitCountTable extends BaseModel {
	protected $tableGateway;
	protected $adapter;
	protected $serviceManager;
	public function __construct($sm) {
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway ( 'post_visit_count', $this->getDbAdapter () );
	}
	public function getAllPosts() {
		return $this->tableGateway->select ()->toArray ();
	}
	public function getAllPostsForVisit() {
		$select = new Select ( 'post_visit_count' );
		$select->columns(array(
				'*',
				'visiteCount' => new Expression('Count(post_id)'), 
		));
		$select->group ( 'post_id' );
		$select->join('blog_posts', 'blog_posts.id = post_visit_count.post_id', array('post_image','post_category_id','post_lang','post_date','post_content','post_title'), 'left');
		$select->order('visiteCount DESC');
 
		return $this->tableGateway->selectWith ( $select )->toArray (); 
	}
	public function getDataByUserIpPosts($id, $ip = false) {
		$select = new Select ( 'post_visit_count' );
		if ($ip) {
			$select->where->equalTo ( 'ip', $ip );
		}
		$select->where->equalTo ( 'post_id', $id );
		
		return $this->tableGateway->selectWith ( $select )->toArray ();
	}
	public function addPostVisit($postData) {
		$this->tableGateway->insert ( $postData );
		return $this->tableGateway->getLastInsertValue ();
	}
	public function deletePost($postId, $userId = null) {
		if ($userId !== null) {
			return $this->tableGateway->delete ( array (
					'id' => $postId,
					'post_author_user_id' => $userId 
			) );
		} else {
			return $this->tableGateway->delete ( array (
					'id' => $postId 
			) );
		}
	}
	public function getLikeAndDislikeCount($commentId = false) {
		$select = new Select ( 'comment_like_dislike' );
		$select->columns ( array (
				'likess' => new Expression ( 'SUM(likes)' ),
				'dislikes' => new Expression ( 'SUM(dislike)' ) 
		) );
		
		$select->where->equalTo ( 'comment_id', $commentId );
		
		$select->group ( 'comment_id' );
		return $this->tableGateway->selectWith ( $select )->toArray ();
	}
}
