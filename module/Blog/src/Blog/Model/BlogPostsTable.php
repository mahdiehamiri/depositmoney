<?php 
namespace Blog\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class BlogPostsTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('blog_posts', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllPosts()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getPosts(
			$category = null,
			$slug = null,
			$author = null,
			$tag = null,
			$getPaginator = null,
			$lang = null,
			$excludeId = null,
			$limit = null
	)
	{
		$select = new Select('blog_posts');
		$select->join('blog_categories', 'blog_categories.id = blog_posts.post_category_id', array('category_title'), Select::JOIN_LEFT);
		$select->join('application_tags_index', 'application_tags_index.entity_id = blog_posts.id', array(), Select::JOIN_LEFT);
		$select->join('application_tags', 'application_tags.id = application_tags_index.tag_id', array(
				'post_tags' => new Expression('Group_Concat(tag_title)')
		), Select::JOIN_LEFT);
		$select->join('users_data', 'users_data.user_id = blog_posts.post_author_user_id', array(
				'firstname',
				'lastname'
		), Select::JOIN_LEFT);
		$select->where(array(
				'post_status' => 1,
				
		));
		if ($lang) {
		    $select->where(array(
		        'post_lang'   => $lang
		    
		    ));
		}
		if ($excludeId) {
			$select->where(array(
					'blog_posts.id != ?' => $excludeId
			));
		}
		if ($limit) {
			$select->limit($limit);
		}
		$select->order('blog_posts.id DESC');
		if ($slug) {
			$select->where->nest()
			->equalTo("post_slug", $slug)
			->or
			->equalTo('post_slug', urlencode($slug))
			->unnest();
		}
		if ($category) {
			$select->where->expression("REPLACE(category_title, ' ', '-') = ?", $category);
		}
		if ($tag) {
			$select->where->expression("REPLACE(tag_title, ' ', '-') = ?", $tag);
		}
		if ($author) {
			$select->where->expression("REPLACE(CONCAT(firstname, ' ', lastname), ' ', '-') = ?", $author);
		}
	
		$select->group('blog_posts.id');
		if ($getPaginator) {
			$dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
			return new Paginator($dbSelector);
		} else {
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	 
	public function getPostsById(
			$category = null,
			$id = null,
			$author = null,
			$tag = null,
			$getPaginator = null,
			$lang = null,
			$excludeId = null,
			$limit = null
	)
	{
		$select = new Select('blog_posts');
		$select->join('blog_categories', 'blog_categories.id = blog_posts.post_category_id', array('category_title'), Select::JOIN_LEFT);
		$select->join('application_tags_index', 'application_tags_index.entity_id = blog_posts.id', array(), Select::JOIN_LEFT);
		$select->join('application_tags', 'application_tags.id = application_tags_index.tag_id', array(
				'post_tags' => new Expression('Group_Concat(tag_title)')
		), Select::JOIN_LEFT);
		$select->join('users_data', 'users_data.user_id = blog_posts.post_author_user_id', array(
				'firstname',
				'lastname'
		), Select::JOIN_LEFT);
		$select->where(array(
				'post_status' => 1,
				
		));
		if ($lang) {
		    $select->where(array(
		        'post_lang'   => $lang
		    
		    ));
		}
		if ($excludeId) {
			$select->where(array(
					'blog_posts.id != ?' => $excludeId
			));
		}
		if ($limit) {
			$select->limit((int)$limit);
		}
		$select->order('blog_posts.id DESC');
		if ($id) {
			$select->where->equalTo("blog_posts.id", $id);
			 
		}
		if ($category) {
			$select->where->expression("REPLACE(category_title, ' ', '-') = ?", $category);
		}
		if ($tag) {
			$select->where->expression("REPLACE(tag_title, ' ', '-') = ?", $tag);
		}
		if ($author) {
			$select->where->expression("REPLACE(CONCAT(firstname, ' ', lastname), ' ', '-') = ?", $author);
		}
	
		$select->group('blog_posts.id');
	 
		if ($getPaginator) {
			$dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
			return new Paginator($dbSelector);
		} else {
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	 
	
	public function getRelatedPostsForBlog($category = null, $postId)
	{
		$select = new Select('blog_posts');
		$select->join('blog_categories', 'blog_categories.id = blog_posts.post_category_id', array('category_title'), Select::JOIN_LEFT);
		$select->join('application_tags_index', 'application_tags_index.entity_id = blog_posts.id', array(), Select::JOIN_LEFT);
		$select->join('application_tags', 'application_tags.id = application_tags_index.tag_id', array(
				'post_tags' => new Expression('Group_Concat(tag_title)')
		), Select::JOIN_LEFT);
		$select->join('users', 'users.id = blog_posts.post_author_user_id', array(
				'firstname',
				'lastname'
		), Select::JOIN_LEFT);
		$select->where(array(
				'post_status' => 1,
				'blog_posts.id != ?' => $postId 
		));
		$select->order('blog_posts.id DESC');

		if ($category) {
						$select->where->expression("REPLACE(category_title, ' ', '-') = ?", array($category));
		}
		$select->group('blog_posts.id');
		$select->limit(3);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getLastestBlogPosts($limit)
	{
	    $select = new Select('blog_posts');
	    $select->join('blog_categories', 'blog_categories.id = blog_posts.post_category_id', array('category_title'), Select::JOIN_LEFT);
		$select->join('users', 'users.id = blog_posts.post_author_user_id', array(
				'firstname',
				'lastname'
		), Select::JOIN_LEFT);
		$select->where(array(
				'post_status' => 1
		));
		$select->order('blog_posts.id DESC');
		$select->limit($limit);
		return $this->tableGateway->selectWith($select)->toArray();
	}	
	
	
	public function getPostById($postId)
	{
		return $this->tableGateway->select(function(Select $select) use ($postId) {
			$select->join('blog_categories', 'blog_categories.id = blog_posts.post_category_id', array('category_title'), Select::JOIN_LEFT);
			$select->join('application_tags_index',
					'application_tags_index.entity_id = blog_posts.id', array(
							'tag_id'
	
					), Select::JOIN_LEFT);
			$select->join('application_tags',
					'application_tags.id = application_tags_index.tag_id',
					array(
							'tag_title' => new Expression('Group_Concat(application_tags.tag_title)')
					), Select::JOIN_LEFT);
			$select->group('blog_posts.id');
			$select->where->equalTo('blog_posts.id', $postId);
		})->toArray();
	}
 
	
	public function addPost($postData)
	{
		$this->tableGateway->insert(array(
				'post_author_user_id' 	=> $postData['post_author_user_id'],
		        'post_category_id' 		=> (int)$postData['post_category_id'],
				'post_content' 			=> $postData['post_content'],
				'post_title' 			=> $postData['post_title'],
				'post_slug' 			=> $postData['post_slug'],
				'post_exerpt' 			=> $postData['post_exerpt'],
				'post_meta_description' => $postData['post_meta_description'],
				'post_meta_keywords'    => $postData['post_meta_keywords'],
				'post_title'            => $postData['post_title'],
// 				'post_source_title'     => $postData['post_source_title'],
// 				'post_source_url' 		=> $postData['post_source_url'],
				'post_status' 		    => (int)$postData['post_status'],
		        'post_image'            => $postData['post_image'],
		        'post_lang'             => $postData['post_lang']
		));
		return $this->tableGateway->getLastInsertValue();
	}
	
	public function getPostsAdmin($getPaginator = null, $where = null)
	{
		$select = new Select('blog_posts');
		//$select->join('blog_categories', 'blog_categories.id = blog_posts.post_category_id', array('category_title'), Select::JOIN_LEFT);
		//$select->join('application_tags_index', 'application_tags_index.entity_id = blog_posts.id', array(), Select::JOIN_LEFT);
		/*$select->join('application_tags', 'application_tags.id = application_tags_index.tag_id', array(
				'post_tags' => new Expression('Group_Concat(tag_title)')
		), Select::JOIN_LEFT);*/
		$select->join('users', 'users.id = blog_posts.post_author_user_id', array('username'), Select::JOIN_LEFT);
		$select->order('post_lang');
		if ($where) {
		    $select->where($where);
		}
		
		//$select->group('blog_posts.id');
		/* if ($getPaginator) {
			$dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
			return new Paginator($dbSelector);
		} else {
			return $this->tableGateway->selectWith($select)->toArray();
		} */
		return $select;
	}

	
	public function editPost($editData, $postId)
	{
		$newData = array(
		        'post_category_id' 		=> (int)$editData['post_category_id'],
				'post_content'          => $editData['post_content'],
				'post_lang'             => $editData['post_lang'],
				'post_title'            => $editData['post_title'],
				'post_slug'             => $editData['post_slug'],
				'post_exerpt'           => $editData['post_exerpt'],
				'post_meta_description' => $editData['post_meta_description'],
				'post_meta_keywords'    => $editData['post_meta_keywords'],
				'post_title'            => $editData['post_title'],
// 				'post_source_title'     => $editData['post_source_title'],
// 				'post_source_url' 		=> $editData['post_source_url'],
				'post_status'           => $editData['post_status'],
				'post_date'             => date('Y/m/d H:i:s'),
		        'post_image'            => $editData['post_image']
		);
		return $this->tableGateway->update($newData, array('id' => $postId));
		
	}
	
	public function deletePost($postId, $userId = null)
	{
		if ($userId !== null) {
			return $this->tableGateway->delete(array(
					'id' 	  			  => $postId,
					'post_author_user_id' => $userId
			));
		} else {
			return $this->tableGateway->delete(array(
					'id' => $postId
			));
		}
	}
	public function search($search, $getPaginator = null)
	{
		$select = new Select('blog_posts');
		$select->join('blog_categories', 'blog_categories.id = blog_posts.post_category_id', array('category_title','category_title_en'), Select::JOIN_LEFT);
		$select->join('application_tags_index', 'application_tags_index.entity_id = blog_posts.id', array(), Select::JOIN_LEFT);
		$select->join('application_tags', 'application_tags.id = application_tags_index.tag_id', array(
				'post_tags' => new Expression('Group_Concat(tag_title)')
		), Select::JOIN_LEFT);
		$select->join('users', 'users.id = blog_posts.post_author_user_id', array(
				'firstname',
				'lastname'
		), Select::JOIN_LEFT);
	
		$where = new Where();
		$where->like('blog_posts.post_content', '%'.$search['search'].'%');
		$where->or->like('blog_posts.post_title', '%'.$search['search'].'%');
		$where->or->like('blog_posts.post_exerpt', '%'.$search['search'].'%');
		$select->where($where);
		if ($getPaginator) {
			$dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
			return new Paginator($dbSelector);
		} else {
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	public function getAllPostsForArchive($limit = 10)
	{
		$select = new Select('blog_posts');
		$select->join('blog_categories', 'blog_categories.id = blog_posts.post_category_id', array('category_title'), Select::JOIN_LEFT);
		$select->where->equalTo('post_status', 1);
		$select->order('id DESC');
		if ($limit) {
			$select->limit($limit);
		}
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
}
