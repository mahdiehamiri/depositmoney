<?php 
namespace Blog\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class BlogPostCategoriesTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('blog_post_categories', $this->getDbAdapter());
    }
    
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function addCategory($categoryId, $postId)
	{
		return $this->tableGateway->insert(array(
				'blog_post_id' => $postId,
				'blog_category_id' => $categoryId
		));
	}
	
	public function editCategory($categoryId, $postId)
	{
		$newCategory = array(
				'blog_category_id' => $categoryId
		);	
		return $this->tableGateway->update($newCategory, array('blog_post_id' => $postId));
	}
	
	public function delete($categoryId, $postId)
	{
	    $where = array(
				'blog_post_id' => $postId,
				'blog_category_id' => $categoryId
		);
	    return $this->tableGateway->delete($where);
	}
}