<?php
// Generated by ZF2's ./bin/templatemap_generator.php
return array(

    'blog/admin/add-category'    => __DIR__ . '/view/blog/admin/add-category.phtml',
    'blog/admin/add-post'        => __DIR__ . '/view/blog/admin/add-post.phtml',
    'blog/admin/edit-category'   => __DIR__ . '/view/blog/admin/edit-category.phtml',
    'blog/admin/edit-post'       => __DIR__ . '/view/blog/admin/edit-post.phtml',
    'blog/admin/list-categories' => __DIR__ . '/view/blog/admin/list-categories.phtml',
    'blog/admin/list-posts'      => __DIR__ . '/view/blog/admin/list-posts.phtml',
    'blog/index/author'          => __DIR__ . '/view/blog/index/author.phtml',
    'blog/index/category'        => __DIR__ . '/view/blog/index/category.phtml',
    'blog/index/feed'            => __DIR__ . '/view/blog/index/feed.phtml',
    'blog/index/index'           => __DIR__ . '/view/blog/index/index.phtml',
    'blog/index/post'            => __DIR__ . '/view/blog/index/post.phtml',
    'blog/index/tag'             => __DIR__ . '/view/blog/index/tag.phtml',
);
