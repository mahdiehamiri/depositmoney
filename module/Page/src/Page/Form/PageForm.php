<?php

namespace Page\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class PageForm extends Form
{
	public function __construct($allActiveLanguages, $galleries = null, $videoGalleriesList = null, $slideshows = null)
	{
		parent::__construct('pageForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
    		    'id'      => 'pageForm',
    		    'novalidate'=> true
		));
		
		$pageTitle = new Element\Text('page_title');
		$pageTitle->setAttributes(array(
				'id'    => 'page_title',
				'class' => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder'  => __("Page title")
		));
		$pageTitle->setLabel(__("Page title"));
		
		$pageSlug = new Element\Text('page_slug');
		$pageSlug->setAttributes(array(
				'id'    => 'page_slug',
				'class' => 'form-control',
		        'placeholder'  => __("Page url")
		));
		$pageSlug->setLabel(__("Page url")); 

		$pageContent = new Element\Textarea('page_content');
		$pageContent->setAttributes(array(
				'id'    => 'page_content',
				'class' => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder'  => __("Page body")
		));
		$pageContent->setLabel(__("Page body"));
	    
		$pageMetaDescription = new Element\Textarea('page_meta_description');
		$pageMetaDescription->setAttributes(array(
				'id'    => 'page_meta_description',
				'class' => 'form-control',
		        'placeholder'  => __("Meta description")
		));
		$pageMetaDescription->setLabel(__("Meta description"));
		
		$pageMetaKeywords = new Element\Textarea('page_meta_keywords');
		$pageMetaKeywords->setAttributes(array(
				'id'    => 'page_meta_keywords',
				'class' => 'form-control',
		        'placeholder'  => __("Meta keywords")
		));
		$pageMetaKeywords->setLabel(__("Meta keywords"));
		
		$pageLang = new Element\Select('page_lang');
		$pageLang->setAttributes(array(
				'id' => 'page_lang',
				'class' => 'form-control',
		));
		$languages = array();
		if ($allActiveLanguages) {
			foreach ($allActiveLanguages as $language) {
				$languages[$language['code']] = $language['title'];
			}
		}
		$pageLang->setValueOptions($languages);
		$pageLang->setLabel(__("Choose language"));
		
		$pageGallery = new Element\Select('page_gallery');
		$pageGallery->setAttributes(array(
		    'id' => 'page_lang',
		    'class' => 'form-control',
		));
		$gallery = array('' => '----');
		if ($galleries) {
		    foreach ($galleries as $gall) {
		        $gallery[$gall['id']] = $gall['persian_name'];
		    }
		}
		$pageGallery->setValueOptions($gallery);
		$pageGallery->setLabel(__("Choose gallery"));
	
		$pageVideoGallery = new Element\Select('page_video_gallery');
		$pageVideoGallery->setAttributes(array(
		    'id' => 'video_gallery_id',
		    'class' => 'form-control',
		));
		$videoGalleryArray= array("" => "----");
		if ($videoGalleriesList) {
		    foreach ($videoGalleriesList as $videoGallery) {
		        $videoGalleryArray[$videoGallery['id']] = $videoGallery['latin_name'];
		    }
		}
		$pageVideoGallery->setValueOptions($videoGalleryArray);
		$pageVideoGallery->setLabel(__("Choose a video gallery"));
		
		$pageSlideshow = new Element\Select('page_slideshow');
		$pageSlideshow->setAttributes(array(
		    'id' => 'page_lang',
		    'class' => 'form-control',
		));
		$slideshow = array('' => '----');
		if ($slideshows) {
		    foreach ($slideshows as $slidesh) {
		        $slideshow[$slidesh['id']] = $slidesh['name'];
		    }
		}
		$pageSlideshow->setValueOptions($slideshow);
		$pageSlideshow->setLabel(__("Choose slideshow"));
		
		$pageStatus = new Element\Checkbox('page_status');
		$pageStatus->setAttributes(array(
				'id'    => 'page_category',
				'class' => 'form-dontrol'
		));
		$pageStatus->setLabel(__("Is published"));
		
		
		
		$csrf = new Element\Csrf('csrf');
		
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(__("Save and New"));
		$submit2->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary fa fa-floppy-o'
		));
		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save and Close"));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary fa fa-floppy-o'
		));
		
		$this->add($pageTitle)
			 ->add($pageSlug)
			 ->add($pageContent)
			 ->add($pageMetaDescription)
			 ->add($pageMetaKeywords)
			 ->add($pageStatus)
			 ->add($pageGallery)
			 ->add($pageVideoGallery)
			 ->add($pageSlideshow)
			 ->add($pageLang)
			 ->add($csrf)
			 ->add($submit)
			 ->add($submit2);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'page_title',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(
				array(
						'name'     => 'page_content',
						'required' => true, 
				)
		));
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'page_meta_description',
				'required' => false,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(  
			array(
				'name'     => 'page_meta_keywords',
				'required' => false,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'tag_title',
		        'required' => false,
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'page_gallery',
		        'required' => false,
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'page_video_gallery',
		        'required' => false,
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'page_slideshow',
		        'required' => false,
		    )
		));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
	
	// Thanks Ms.Khoshnavaz for this code
	
// 	public function isValid()
// 	{
// 		$this->getInputFilter()->remove('tag_title');
// 		return parent::isValid();
// 	}
}
