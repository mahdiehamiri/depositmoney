<?php 
namespace Page\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class PagePageTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('page_pages', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	
	public function getPage($slug = null, $lang = "fa")
	{
		$select = new Select('page_pages');
		$select->join('users_data', 'users_data.user_id = page_pages.page_author_user_id', array(
				'firstname',
		        'lastname',
		), Select::JOIN_LEFT);
		$select->where(array(
				'page_status' => 1,
		        'page_lang'   => $lang,
		        //'users_data.lang'   => $lang
		));
		$select->order('page_pages.id DESC');
		if ($slug) {
			$select->where->nest()
						  ->equalTo("page_slug", $slug)
						  ->or
						  ->equalTo('page_slug', urlencode($slug))
						  ->unnest();
		} 
		return $this->tableGateway->selectWith($select)->toArray();
		
	}
	
	
	
	public function getPageById($pageId)
	{
	    $select = new Select('page_pages');
	    $select->where->equalTo('id', $pageId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addPage($pageData)
	{
		$this->tableGateway->insert(array(
				'page_content' 			=> html_entity_decode($pageData['page_content']),
				'page_title' 			=> $pageData['page_title'],
				'page_slug' 			=> $pageData['page_slug'],
				'page_meta_description' => $pageData['page_meta_description'],
				'page_meta_keywords'    => $pageData['page_meta_keywords'],
				'page_status' 		    => (int)$pageData['page_status'],
		        'page_lang' 			=> $pageData['page_lang'],
		        'page_gallery' 			=> $pageData['page_gallery'],
		        'page_video_gallery' 	=> $pageData['page_video_gallery'],
		        'page_slideshow' 		=> $pageData['page_slideshow'],
		        'page_create_date'      => date('Y/m/d H:i:s'),
		));
		return $this->tableGateway->getLastInsertValue();
	}
	
	/* public function getPagesList($getPaginator = null, $where = null)
	{
		$select = new Select('page_pages');
		
		$select->join('page_page_tags', 'page_page_tags.page_page_id = page_pages.id', array(), Select::JOIN_LEFT);
		$select->join('page_tags', 'page_tags.id = page_page_tags.page_tag_id', array(
				'page_tags' => new Expression('Group_Concat(tag_title)')
		), Select::JOIN_LEFT);
		$select->join('users', 'users.id = page_pages.page_author_user_id', array(
				'firstname',
				'lastname'
		), Select::JOIN_LEFT);
		if ($where) {
		    $select->where($where);
		}
		$select->order('page_pages.id DESC');
		$select->group('page_pages.id');
		if ($getPaginator) {
			$dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
			return new Paginator($dbSelector);
		} else { 
			return $this->tableGateway->selectWith($select);
		}
	} */
	
	 public function getPagesList()
	{
	    $select = new Select('page_pages');
	    $select->join('users', 'users.id = page_pages.page_author_user_id', array(
	        'username',
	    ), Select::JOIN_LEFT);
	    $select->order('page_pages.page_lang, page_pages.id DESC');

	    return $select;
	} 
	
	public function getPages($where = null)
	{
	    $select = new Select('page_pages');
	     if ($where) {
	        $select->where($where);
	    }
	    $select->order('page_pages.id DESC');
	    return $this->tableGateway->selectWith($select)->toArray();;
	   
	}

	public function editPage($editData, $pageId)
	{
		$newData = array(
				'page_content'          => html_entity_decode($editData['page_content']),
				'page_title'            => $editData['page_title'],
				'page_meta_description' => $editData['page_meta_description'],
				'page_meta_keywords'    => $editData['page_meta_keywords'],
				'page_status'           => (int)$editData['page_status'],
		        'page_lang'             => $editData['page_lang'],
		        'page_slug' 			=> $editData['page_slug'],
		        'page_gallery' 			=> $editData['page_gallery'],
		        'page_video_gallery' 	=> $editData['page_video_gallery'],
		        'page_slideshow' 		=> $editData['page_slideshow'],
				
		);
		return $this->tableGateway->update($newData, array('id' => $pageId));
	}
	
	public function deletePage($pageId, $userId = null)
	{
		if ($userId !== null) {
			return $this->tableGateway->delete(array(
					'id' 	  			  => $pageId,
					'page_author_user_id' => $userId
			));
		} else {
			return $this->tableGateway->delete(array(
					'id' => $pageId
			));
		}
	}
	public function search($search, $getPaginator = null)
	{
// 		$select = new Select();
// 		$select->from('page_pages');
// 		$select->join('page_page_tags', 'page_page_tags.page_page_id = page_pages.id', array(), Select::JOIN_LEFT);
// 		$select->join('page_tags', 'page_tags.id = page_page_tags.page_tag_id', array(
// 				'page_tags' => new Expression('Group_Concat(tag_title)')
// 		), Select::JOIN_LEFT);
// 		$select->join('users', 'users.id = page_pages.page_author_user_id', array(
// 				'firstname',
// 				'lastname'
// 		), Select::JOIN_LEFT);
	
// 		$where = new Where();
// 		$where->like('page_pages.page_content', '%'.$search['search'].'%');
// 		$where->or->like('page_pages.page_title', '%'.$search['search'].'%');
// 		$select->where($where);
// 		if ($getPaginator) {
// 			$dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
// 			return new Paginator($dbSelector);
// 		} else {
// 			return $this->tableGateway->selectWith($select)->toArray();
// 		}
	}
}
