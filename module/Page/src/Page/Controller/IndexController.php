<?php

namespace Page\Controller;

use Zend\View\Model\ViewModel;
use Page\Model\PagePageTable;
use Application\Helper\BaseController;

class IndexController extends BaseController
{
	protected $noOfPagesPerPage = 100;
	protected $latestPageLimit = 4;

  
    public function pageAction()
    {        
        $slug = $this->params('slug');
     
        $pagePageTable = new PagePageTable($this->getServiceLocator());
        $pagePage = $pagePageTable->getPage($slug, $this->lang);
        $view['breadcrumbs'] = $this->generalHelper()->breadcrumbs($separator = ' ', $home = __('Home'), $this->lang);
        //var_dump($this->breadcrumbs); die;
        if ($pagePage) {
            $plugins = null;
            if($pagePage[0]['page_slideshow']){ //var_dump($pagePage[0]['page_slideshow']);die;
                $plugins[300]["name"] = "[plg]'slideshow/index/page'[/plg]";
                $plugins[300]["code"] = 'echo $this->getPlugin("slideshow/index/page","'.$pagePage[0]['page_slideshow'].'");';
            }
            preg_match_all("/\[plg\](.*?)\[\/plg\]/i", $pagePage[0]["page_content"], $match);
            if (isset($match[0]) &&  $match[0]) {
                foreach ($match[0] as $k => $plugin) {
                    $plugins[$k]["name"] = $match[0][$k];
                    $plugins[$k]["code"] = 'echo $this->getPlugin('.$match[1][$k].');';
                }

            }
            if($pagePage[0]['page_gallery']){
                $plugins[100]["name"] = "[plg]'gallery/index/page'[/plg]";
                $plugins[100]["code"] = 'echo $this->getPlugin("gallery/index/page","'.$pagePage[0]['page_gallery'].'");';
                
            }
            if($pagePage[0]['page_video_gallery']){
                $plugins[200]["name"] = "[plg]'video-gallery/index/page'[/plg]";
                $plugins[200]["code"] = 'echo $this->getPlugin("video-gallery/index/page","'.$pagePage[0]['page_video_gallery'].'");';

            }
            $view['plugins'] = $plugins;
            $view['pagePage'] = $pagePage[0];
            $this->setHeadTitle($pagePage[0]['page_title']);
//             var_dump($view);die;
            return new ViewModel($view);
        }
        $this->getResponse()->setStatusCode(404);
        return;
    }
    
    public function tagAction()
    {
//     	$tag = $this->params('param');
//     	$pagePageTable = new PagePageTable($this->getServiceLocator());
//     	$paginator = $pagePageTable->getPage(null, null, $tag, true, $this->lang);

//     	$page = $this->params('page');
//     	if ($page == 1) {
//     		return $this->redirect()->toRoute('page-page', array(
//     		        'action'  => 'tag',
//     		        'lang'  => $this->lang,
//     				'tag'  => $tag
//     		));
//     	}
//     	$paginator->setCurrentPageNumber($this->params('page', 1));
//     	$paginator->setItemCountPerPage($this->noOfPagePerPage);
//     	$view['pagesList'] = $paginator;
//     	$view['tag'] = $tag;
//         return new ViewModel($view);
    }
    
    public function authorAction()
    {
//     	$autherName = $this->params('param');
//     	$pagePageTable = new PagePageTable($this->getServiceLocator());
//     	$paginator = $pagePageTable->getPage(null,  $autherName, null, true, $this->lang);
    	
//     	$page = $this->params('page');
//     	if ($page == 1) {
//     		return $this->redirect()->toRoute('page-page', array(
//     		        'action'  => 'author',
//     		        'lang'  => $this->lang,
//     				'name' => $autherName
//     		));
//     	}
//     	$paginator->setCurrentPageNumber($this->params('page', 1));
//     	$paginator->setItemCountPerPage($this->noOfPagesPerPage);

//     	$view['pagesList'] = $paginator;
//     	$view['authorName'] = $autherName;
//         return new ViewModel($view);
    }
    

}
