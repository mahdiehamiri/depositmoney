<?php
namespace Page\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Page\Model\PagePageTable;
use Page\Model\PageTagsTable;
use Page\Model\PagePageTagsTable;
use Application\Helper\BaseAdminController;
use Language\Model\LanguageLanguagesTable;
use Page\Form\PageForm;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Page\Model\GalleriesTable;
use Page\Model\SlideshowTable;
use Page\Model\VideoGalleriesTable;

class AdminController extends BaseAdminController
{

    protected $noOfPagesPerPage = 100;

    public function listPagesAction()
    {
        $this->setHeadTitle(__('Pages list'));
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $pagePageTable = new PagePageTable($this->getServiceLocator());
        $allpage = $pagePageTable->getPagesList();
        
        $grid->setTitle(__('Page List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($allpage, $dbAdapter);
        
        $col = new Column\Select('id', "page_pages");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);  
        
        $col = new Column\Select('page_status');
        $col->setLabel(__('Status'));
        $col->setWidth(10);
        $options = array(
            '0' => 'inactive',
            '1' => 'active'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            '0' => 'inactive',
            '1' => 'active'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $colLang = new Column\Select('page_lang');
        $colLang->setLabel(__('Page language'));
        $options = array(
            'fa' => 'fa',
            'en' => 'en'
        );
        $colLang->setFilterSelectOptions($options);
        $replaces = array(
            'fa' => 'fa',
            'en' => 'en'
        );
        $grid->addColumn($colLang);
        
        $col = new Column\Select('page_title');
        $col->setLabel('Title');
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $colSlug = new Column\Select('page_slug');
        $colSlug->setLabel(__('Page url'));
        $grid->addColumn($colSlug);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-eye-open');
        $viewAction->setLink("/" . $viewAction->getColumnValuePlaceholder($colLang) . '/' . $viewAction->getColumnValuePlaceholder($colSlug));
        $viewAction->setTooltipTitle(__('Preview page'));
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-page/edit-page/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-page/delete-page/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-page/add-page class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a tablename="users"  href="/application-ajax-delete-all"  class="btn btn-danger delete_all"><i class="fa fa-times" aria-hidden="true"></i>' . __("Delete Group Permission") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addPageAction()
    
    {
        $this->setHeadTitle(__('Add new page'));
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        
        $galleriesTable = new GalleriesTable($this->getServiceLocator());
        $galleries = $galleriesTable->getAllGallery();
        
        $videoGlleriesTable = new VideoGalleriesTable($this->getServiceLocator());
        $videoGalleries = $videoGlleriesTable->getAllVideoGallery();
        
        $slideshowTable = new SlideshowTable($this->getServiceLocator());
        $slideshows = $slideshowTable->getRecords();
        
        $pageForm = new PageForm($allActiveLanguages, $galleries, $videoGalleries, $slideshows);
        $view['pageForm'] = $pageForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pageData = $request->getPost();
            if ($pageForm->setData($pageData)->isValid()) {
                $pageData = $pageForm->getData();
                $pageData['page_title'] = str_replace("/", "", $pageData['page_title']);
                $pageData['page_slug'] = str_replace(' ', '-', trim($pageData['page_title']));
                $pagePageTable = new PagePageTable($this->getServiceLocator());
                
                try {
                    $newPageId = $pagePageTable->addPage($pageData);
                    if ($newPageId) {
                        if (isset($_POST['submit'])) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('page-admin');
                        } else {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('page-admin', array(
                                'action' => 'add-page'
                            ));
                        }
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("The title is taken, please choose another one.");
                }
            }
        }
        
        return new ViewModel($view);
    }

    public function editPageAction()
    {
        $this->setHeadTitle(__('Edit page'));
        $userId = $this->userData->id;
        $pageId = $this->params('id');
        if (! $pageId) {
            return $this->redirect()->toRoute('page-admin');
        }
        
        $galleriesTable = new GalleriesTable($this->getServiceLocator());
        $galleries = $galleriesTable->getAllGallery();
        
        $videoGlleriesTable = new VideoGalleriesTable($this->getServiceLocator());
        $videoGalleries = $videoGlleriesTable->getAllVideoGallery();
        
        $slideshowTable = new SlideshowTable($this->getServiceLocator());
        $slideshows = $slideshowTable->getRecords();
        
        $pagePageTable = new PagePageTable($this->getServiceLocator());
        $pagesPage = $pagePageTable->getPageById($pageId);
        if (! $pagesPage) {
            $this->flashMessenger()->addErrorMessage("Page not found!");
            return $this->redirect()->toRoute('page-admin');
        } else {
            // $pagePageTagsTable = new PagePageTagsTable($this->getServiceLocator());
            
            $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
            $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
                "enable" => "1"
            ));
            
            $pageForm = new PageForm($allActiveLanguages, $galleries, $videoGalleries, $slideshows);
            $pageForm->populateValues($pagesPage[0]);
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = $request->getPost(); // var_dump($data); die;
                $pageForm->setData($data);
                if ($pageForm->isValid()) {
                    $editData = $pageForm->getData();
                    // var_dump($editData); die;
                    $editData['page_title'] = str_replace("/", "", $editData['page_title']);
                    $editData['page_slug'] = str_replace(' ', '-', trim($editData['page_title']));
                    try {
                        $isEditted = $pagePageTable->editPage($editData, $pageId);
                        if ($isEditted !== false) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('page-admin');
                        } else {
                            $this->layout()->errorMessage = __("Operation failed!");
                        }
                    } catch (\Exception $e) {
                        // var_dump($e); die('$e');
                        $this->layout()->errorMessage = __("The title is taken, please choose another one.");
                    }
                }
                // var_dump($pageForm->getMessages()); die('not valid');
            }
            $view['pageForm'] = $pageForm;
            return new ViewModel($view);
        }
    }

    public function deletePageAction()
    {
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'];
        $container = new Container('token');
        $pageId = $this->params('id');
        $token = $this->params('token');
        $userId = $this->userData->id;
        $pagePageTable = new PagePageTable($this->getServiceLocator());
        $pagesPage = $pagePageTable->getPageById($pageId);
        
        // $canDeleteAllPages = $this->permissions()->getPermissions('page-admin-can-delete-all-pages');
        $canDeleteAllPages = true;
        if ($pageId && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            if ($canDeleteAllPages) {
                $userId = null;
            }
            $html = $pagesPage[0]['page_content'];
            $doc = new \DOMDocument();
            if ($html) {
                $doc->loadHTML($html);
                $xpath = new \DOMXPath($doc);
                $imageSrc = $xpath->evaluate("//img");
                for ($i = 0; $i <= count($imageSrc); $i ++) {
                    if ($imageSrc->item($i)) {
                        $src = $imageSrc->item($i)->attributes->getNamedItem('src')->nodeValue;
                        @unlink($uploadDir . $src);
                    }
                }
            }
            
            $isDeleted = $pagePageTable->deletePage($pageId, $userId);
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            }
        }
        return $this->redirect()->toRoute('page-admin');
    }
}
