<?php
return array(
    'router' => array(
        'routes' => array(
            'page-home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/:slug',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',

                        'slug'      => '(?!executive-cooperation|request-project|request-teaching|signin|signinbar|signup|search|forgot-password|reset-password|admin/logout|verify-user|verify-user-mobile|admin|application-ajax-delete-all|internalsample|sample|design|seo|develop|fa/contact|en/contact|contact|user-register|technical-office-search|user-register-public|user-register-seller|technical-office|print-request|add-product|dashboard|products|edit-product|delete-product|logout|profile|seller-profile|technical-office-profile|services|category|forms|forgot-password|fa/forgot-password|request-cart|product-list|get-config-val|products-detailes|orders|get-products|product-factor|set-cart-session|get-cart-session|product-orders|add-price|get-form|price-list|delete-price|search-products|get-service|get-district|search-product|search-filter-products|all-price-list|count-cart-session|change-status|create-pdf|main-category|second-category|add-designer-sample|edit-designer-sample|designer-samples|designer-samples-detailes|delete-designer-sample).{3,}',

//                         'slug'      => '(?!executive-cooperation|request-project|request-teaching|signin|signinbar|signup|search|forgot-password|reset-password|admin/logout|verify-user|verify-user-mobile|admin|application-ajax-delete-all|internalsample|sample|design|seo|develop|fa/contact|en/contact|contact|user-register|technical-office-search|user-register-public|user-register-seller|technical-office|print-request|add-product|dashboard|products|edit-product|delete-product|logout|profile|seller-profile|technical-office-profile|services|category|forms|forgot-password|request-cart|product-list|products-detailes).{3,}',

                       
                    ),
                    'defaults' => array (
                        '__NAMESPACE__' => 'Page\Controller',
                        'controller' => 'Index',
                        'action' => 'page'
                    )
                ),
            ),
            'page-page' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/:lang/page/:action/[:param][/page/:page]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'lang' => '[a-zA-Z]{2}',
                        'param'      => '((?!\/).)*',
                        'page' => '[0-9]+',
                         
                    ),
                    'defaults' => array (
                        '__NAMESPACE__' => 'Page\Controller',
                        'controller' => 'Index',
                        'action' => 'list'
                    )
                ),
            ),
            'page-admin' => array (
                'type' => 'segment',
                'options' => array (
                    'route' => '[/:lang]/admin-page[/:action][/:id][/page/:page][/delete/:delete][/clear/:clear][/token/:token][/count/:count]',
                    'constraints' => array (
                        'lang' => '[a-zA-Z]{2}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                        'delete' => '[0-9]+',
                        'clear' => 'true',
                        'count' => '[0-9]+',
                        'token' => '[a-f0-9]+',
            
                    ),
                    'defaults' => array (
                        '__NAMESPACE__' => 'Page\Controller',
                        'controller' => 'Admin',
                        'action' => 'list-pages'
                    )
                )
            ),
    
            
            
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            include_once(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
