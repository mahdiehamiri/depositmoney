<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
    'Page\Module'                     => __DIR__ . '/Module.php',
    'Page\Controller\AdminController' => __DIR__ . '/src/Page/Controller/AdminController.php',
    'Page\Controller\IndexController' => __DIR__ . '/src/Page/Controller/IndexController.php',
    'Page\Form\PageForm'              => __DIR__ . '/src/Page/Form/PageForm.php',
    'Page\Model\GalleriesTable'       => __DIR__ . '/src/Page/Model/GalleriesTable.php',
    'Page\Model\PagePageTable'        => __DIR__ . '/src/Page/Model/PagePageTable.php',
);
