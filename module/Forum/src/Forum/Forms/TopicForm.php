<?php

namespace Forum\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

class TopicForm extends Form
{
	public function __construct($categories)
	{
		parent::__construct('topic');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		$forumCategoryId = new Element\Select('forum_category_id');
		$forumCategoryId->setLabel('نام دسته بندی');
		$forumCategoryId->setAttributes(array(
				'id'    => 'forum-category-id',
				'class' => 'form-control'
		));
		$categoryList = array();
		foreach ($categories as $category) {
		    $categoryList[$category['id']] = $category['main_category_name'] . ' -- ' . $category['name'];
		}   
		$forumCategoryId->setValueOptions($categoryList);
		
		$topicName = new Element\Text('topic_name');
		$topicName->setLabel('عنوان');
		$topicName->setAttributes(array(
		    'id'    => 'topic-name',
		    'class' => 'form-control'
		));
		
		$topicContent = new Element\Textarea('topic_content');
		$topicContent->setLabel('توضیحات');
		$topicContent->setAttributes(array(
		    'id'    => 'topic-content',
		    'class' => 'form-control'
		));
		
		
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Submit('submit');
		$submit->setValue('ارسال');
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		
		$this->add($forumCategoryId)  
			 ->add($topicName)
			 ->add($topicContent)
			 ->add($csrf)
			 ->add($submit);
	}	
}
