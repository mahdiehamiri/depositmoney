<?php

namespace Forum\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

class CategoryForm extends Form
{
	public function __construct($mainCategories)
	{
		parent::__construct('category');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		$name = new Element\Text('name');
		$name->setLabel('نام دسته بندی');
		$name->setAttributes(array(
				'id'    => 'name',
				'class' => 'form-control'
		));
		if ($mainCategories) {
		    $mainCategoryId = new Element\Select('main_category_id');
		    $options = array();
		    foreach ($mainCategories as  $mainCategory) {
		        $options[$mainCategory['id']] = $mainCategory['name'];
		    }
		    $mainCategoryId->setLabel('گروه اصلی انجمن');
		    $mainCategoryId->setValueOptions($options);
			$mainCategoryId->setAttributes(array( 
				'class' => 'form-control'
	       	));
		}
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Submit('submit');
		$submit->setValue('ثبت دسته بندی');
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		
		$this->add($name)  
			 ->add($mainCategoryId)
			 ->add($csrf)
			 ->add($submit);
	}	
}