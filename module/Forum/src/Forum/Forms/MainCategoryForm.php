<?php

namespace Forum\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

class MainCategoryForm extends Form
{
	public function __construct()
	{
		parent::__construct('category');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		$name = new Element\Text('name');
		$name->setLabel('نام گروه اصلی انجمن');
		$name->setAttributes(array(
				'id'    => 'name',
				'class' => 'form-control'
		));
		    
		$csrf = new Element\Csrf('csrf');
		
		$submit = new Element\Submit('submit');
		$submit->setValue('ثبت گروه اصلی انجمن');
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		
		$this->add($name)  
			 ->add($csrf)
			 ->add($submit);
	}	
}
