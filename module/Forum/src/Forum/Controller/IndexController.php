<?php
namespace Forum\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Sql\Ddl\Column\Varbinary;
use Zend\Mvc\MvcEvent;
use Forum\Model\ForumCategoriesTable;
use Forum\Model\ForumMainCategoriesTable;
use Forum\Model\ForumTopicsTable;
use Forum\Model\UsersTable;
use Forum\Model\ForumPostsTable;

class IndexController extends AbstractActionController
{
    protected $userData;
    protected $postsPerPage = '20';
    
    public function onDispatch(MvcEvent $e)
    {
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData = $this->authenticationService->isLoggedIn();
        parent::onDispatch($e);
    }
     
    public function indexAction()
    {
	    $forumCategoriesTable = new ForumCategoriesTable($this->getServiceLocator());
	    $forumMainCategoriesTable = new ForumMainCategoriesTable($this->getServiceLocator());
	    $view['categories'] = $forumCategoriesTable->getRecordsAsArray();
	    $view['mainCategories'] = $forumMainCategoriesTable->getRecordsAsArray();
	    
	    $forumTopicsTable = new ForumTopicsTable($this->getServiceLocator());
        $topicsPaginator = $forumTopicsTable->getTopRecords();  
	    $topicsPaginator->setCurrentPageNumber($this->params('page', 1));
	    $topicsPaginator->setItemCountPerPage($this->postsPerPage);
	    $view['topics'] = $topicsPaginator;
        return new ViewModel($view);
    }

    public function categoryAction()
    {
        $countPerPage = '10';
        $slug = $this->params('slug');  
        
        $forumCategoriesTable = new ForumCategoriesTable($this->getServiceLocator());
        $forumMainCategoriesTable = new ForumMainCategoriesTable($this->getServiceLocator());
        $view['categories'] = $forumCategoriesTable->getRecordsAsArray();
        $viewData = array();  
        $hasCategory = false;
        foreach ($view['categories'] as $mainCategoryId => $categories) {
            foreach ($categories as $categoryId => $categoryName) {
                $categorySlug = strtolower(str_replace(" ", "-", $categoryName));
                if ($categorySlug == $slug) {
                    $view['categoryName'] = $categoryName;
                    $view['categoryId'] = $categoryId;
                    $hasCategory = true;
                }
            }
        }  
        if (!$hasCategory) {
            return $this->redirect()->toRoute('forum-home');            
        }
        $forumTopicsTable = new ForumTopicsTable($this->getServiceLocator());
        $view['mainCategories'] = $forumMainCategoriesTable->getRecordsAsArray();
        $topicsPaginator = $forumTopicsTable->getTopRecords($view['categoryId']);
        $topicsPaginator->setCurrentPageNumber($this->params('page', 1));
        $topicsPaginator->setItemCountPerPage($countPerPage);
        $view['topics'] = $topicsPaginator;
        $view['slug'] = $slug;
        return $view;
    }

    public function topicAction()
    { 
        $countPerPage = '10';
        $slug = $this->params('slug');
        $topicTitle = $this->params('topic');
        
        $forumTopicsTable = new ForumTopicsTable($this->getServiceLocator());
        $topic = $forumTopicsTable->getTopicBySlug($topicTitle);
        if (!$topic) { 
            return $this->redirect()->toRoute('forum-home');
        }
        $forumUsersTable = new UsersTable($this->getServiceLocator());
        $view['users'] = $forumUsersTable->getAllUsers();
        $topic = $topic[0];
        $request = $this->getRequest();
        $forumPostsTable = new ForumPostsTable($this->getServiceLocator());
        $view['parent'] = false;
        $view['edit_post_id'] = false;
        $parentId = 0;
        if ($request->isPost()) {
            if ($this->userData) {
                $userId =  $this->userData->id;
                $postData = $request->getPost();
                // TODO word limitaion
                if ($postData['post'] && $postData['post'] != '') {
                    if (isset($postData['edit_post_id'])) {
                        $editPostId = (int)$postData['edit_post_id'];
                        $view['post'] = $postData['post'];
                        if ($forumPostsTable->editPost($postData['post'], $editPostId, $userId)) {
                            $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت ثبت گردید.');
                            return $this->redirect()->refresh();
                        } else {
                            $view['edit_post_id'] = $editPostId;
                            $view['error'] = 'در ثبت اطلاعات خطایی رخ داده است. لطفا مجددا تلاش نمایید.';
                        }
                    } else {
                        $parentId = (int)$postData['parent'];
                        $view['parent'] = $parentId;
                        $view['post'] = $postData['post'];
                        $lastInsertPostId = $forumPostsTable->addPost($postData['post'], $parentId, $userId, $topic['id']);
                        if ($lastInsertPostId) {
                            $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت ثبت گردید.');
                            return $this->redirect()->refresh();
                        } else {
                            $view['error'] = 'در ثبت اطلاعات خطایی رخ داده است. لطفا مجددا تلاش نمایید.'; 
                        }
                    }
                } else {
                    $view['parent'] = $parentId;
                    $view['error'] = 'متن پیام خالی است.'; 
                }
            }
        }   
        $postsPaginator = $forumPostsTable->getTopRecords($topic['id']);
        $postsPaginator->setCurrentPageNumber($this->params('page', 1));
        $postsPaginator->setItemCountPerPage($countPerPage);
        $viewData = array();
        $view['posts']  = $postsPaginator;
        $view['userData']   = $this->userData;
        $view['slug']   = $slug;
        $view['topic']  = $topic;
        return $view;
    } 
}
