<?php

namespace Forum\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Forum\Forms\MainCategoryForm;
use Forum\Forms\CategoryForm;
use Forum\Forms\TopicForm;
use Forum\Model\ForumCategoriesTable;
use Forum\Model\ForumMainCategoriesTable;
use Forum\Model\ForumTopicsTable;
use Forum\Model\ForumPostsTable;
use Application\Helper\BaseAdminController;

class AdminController extends BaseAdminController
{     
	public function indexAction()
	{ 
	    $container = new Container('token');
	    $view['hash'] = md5(rand() . time() . rand());
	    $container->offsetSet('csrf', $view['hash']);
	    $forumCategoriesTable = new ForumCategoriesTable($this->getServiceLocator());
	    $view['categories'] = $forumCategoriesTable->getRecords();
	    return new ViewModel($view);
	}
	

	public function addCategoryAction()
	{
	    $forumMainCategoriesTable = new ForumMainCategoriesTable($this->getServiceLocator());
	    $categoryForm = new CategoryForm($forumMainCategoriesTable->getRecords());
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $categoryForm->setData($postData);
	        if ($categoryForm->isValid()) {
	            $validData = $categoryForm->getData();
	            $forumCategoriesTable = new ForumCategoriesTable($this->getServiceLocator());
	            if ($forumCategoriesTable->addCategory($validData)) {
	                $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت ثبت گردید.');
	                return $this->redirect()->toRoute('forum-admin');
	            }
	            $this->layout()->errorMessage = 'مشکل در ثبت اطلاعات.';
	        } else {
	            $this->layout()->errorMessage = 'ورودی فرم نامعتبر است.';
	        }
	    }
	    $view['categoryForm'] = $categoryForm;
	    return new ViewModel($view);
	}
	 
	
	public function editCategoryAction()
	{
	    $forumMainCategoriesTable = new ForumMainCategoriesTable($this->getServiceLocator());
	    $categoryForm = new CategoryForm($forumMainCategoriesTable->getRecords());
	    $request = $this->getRequest();
	    $categoryId = $this->params('id');
	    if (!$categoryId) {
	        return $this->redirect()->toRoute('forum-admin');
	    }
	 
	    $forumCategoriesTable = new ForumCategoriesTable($this->getServiceLocator());
	    $category = $forumCategoriesTable->getRecords($categoryId);
	    if (!$category) {
	        return $this->redirect()->toRoute('forum-admin');
	    }
	    $categoryForm->populateValues($category[0]);
	    if ($request->isPost()) {
	        $postData = $request->getPost();
	        $categoryForm->setData($postData);
	        if ($categoryForm->isValid()) {
	            $validData = $categoryForm->getData();
	            if ($forumCategoriesTable->editCategory($validData, $categoryId) !== false) {
	                $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت ثبت گردید.');
	                return $this->redirect()->toRoute('forum-admin');
	            }
	            $this->layout()->errorMessage = 'مشکل در ثبت اطلاعات.';
	
	        } else {
	            $this->layout()->errorMessage = 'ورودی فرم نامعتبر است.';
	        }
	    }
	    $view['categoryForm'] = $categoryForm;
	    return new ViewModel($view);
	}
	
	public function deleteCategoryAction()
	{
	    $container = new Container('token');
	    $token = $this->params('token');
	    $userId = $this->userData->id;
	    $categoryId = $this->params('id');
	    if ($categoryId && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $forumCategoriesTable = new ForumCategoriesTable($this->getServiceLocator());
            if ($forumCategoriesTable->deleteCategory($categoryId)) {
                $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت حذف گردید');
            } else {
                $this->flashMessenger()->addErrorMessage('مشکل در حذف دسته بندی');
            }
	    }
	    return $this->redirect()->toRoute('forum-admin');
	}
	 
    public function mainCategoriesAction()
    {
        $container = new Container('token');
        $view['hash'] = md5(rand() . time() . rand());
        $container->offsetSet('csrf', $view['hash']);
        $forumMainCategoriesTable = new ForumMainCategoriesTable($this->getServiceLocator());
    	$view['categories'] = $forumMainCategoriesTable->getRecords();
    	return new ViewModel($view);
    }
    
    public function addMainCategoryAction()
    {
    	$mainCategoryForm = new MainCategoryForm(); 
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    	    $postData = $request->getPost();
    	    $mainCategoryForm->setData($postData);
    	    if ($mainCategoryForm->isValid()) {
    	        $validData = $mainCategoryForm->getData(); 
    	        $forumMainCategoriesTable = new ForumMainCategoriesTable($this->getServiceLocator());
    	        if ($forumMainCategoriesTable->addMainCategory($validData['name'])) {
    	            $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت ثبت گردید.');
    	            return $this->redirect()->toRoute('forum-admin', array(
    	                'action' => 'main-categories'
    	            ));
    	        }
	            $this->layout()->errorMessage = 'مشکل در ثبت اطلاعات.'; 
    	    } else {
    	        $this->layout()->errorMessage = 'ورودی فرم نامعتبر است.';
    	    }
    	}
    	$view['mainCategoryForm'] = $mainCategoryForm; 
    	return new ViewModel($view);
    }

    public function editMainCategoryAction()
    {
        $mainCategoryForm = new MainCategoryForm();
        $request = $this->getRequest();
        $categoryId = $this->params('id');
       
        if (!$categoryId) {
            return $this->redirect()->toRoute('forum-admin', array(
                'action' => 'main-categories'
            )); 
        } 
        $forumMainCategoriesTable = new ForumMainCategoriesTable($this->getServiceLocator());
        $category = $forumMainCategoriesTable->getRecords($categoryId);
        if (!$category) {
            return $this->redirect()->toRoute('forum-admin', array(
                'action' => 'main-categories'
            ));
        }
        $mainCategoryForm->populateValues($category[0]);
        if ($request->isPost()) {
            $postData = $request->getPost();
            $mainCategoryForm->setData($postData);
            if ($mainCategoryForm->isValid()) {
                $validData = $mainCategoryForm->getData(); 
                if ($forumMainCategoriesTable->editMainCategory($validData['name'], $categoryId) !== false) {
                     $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت ثبت گردید.');
                    return $this->redirect()->toRoute('forum-admin', array(
                        'action' => 'main-categories'
                    ));
                } 
                $this->layout()->errorMessage = 'مشکل در ثبت اطلاعات.';
            
            } else {
                $this->layout()->errorMessage = 'ورودی فرم نامعتبر است.';
            }
        }
        $view['mainCategoryForm'] = $mainCategoryForm;
        return new ViewModel($view);
    }
    
    public function deleteMainCategoryAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $userId = $this->userData->id;
        $categoryId = $this->params('id');
        if ($categoryId && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $forumMainCategoriesTable = new ForumMainCategoriesTable($this->getServiceLocator());
                if ($forumMainCategoriesTable->deleteMainCategory($categoryId)) {
                    $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت حذف گردید');
                } else {
                    $this->flashMessenger()->addErrorMessage('مشکل در حذف گروه');            
                } 
            }
        }   
        return $this->redirect()->toRoute('forum-admin', array(
            'action' => 'main-categories'
        ));
    } 

    public function topicsAction()
    {
        $userId = $this->userData->id;
        if ($this->permissions()->getPermissions('forum-admin-can-manage-all-topics')) {
        	$userId = null;
        }
        $container = new Container('token');
        $view['hash'] = md5(rand() . time() . rand());
        $container->offsetSet('csrf', $view['hash']);
        $forumTopicsTable = new ForumTopicsTable($this->getServiceLocator());
        // TODO if admin $forumTopicsTable->getRecords()
        $view['topics'] = $forumTopicsTable->getUserTopics($userId);
        return new ViewModel($view);
    }
    
    public function addTopicAction()
    {
        $forumCategoriesTable = new ForumCategoriesTable($this->getServiceLocator());
        $topicForm = new TopicForm($forumCategoriesTable->getRecords());
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $topicForm->setData($postData);
            if ($topicForm->isValid()) {
                $validData = $topicForm->getData();
                $forumTopicsTable = new ForumTopicsTable($this->getServiceLocator());
                if ($forumTopicsTable->addTopic($validData, $this->userData->id)) {
                    $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت ثبت گردید.');
                    return $this->redirect()->toRoute('forum-admin', array(
                        'action' => 'topics'
                    ));
                }
                $this->layout()->errorMessage = 'مشکل در ثبت اطلاعات.';
            } else {
                $this->layout()->errorMessage = 'ورودی فرم نامعتبر است.';
            }
        }
        $view['topicForm'] = $topicForm;
        return new ViewModel($view);
    }
     
    public function editTopicAction()
    {
        $userId = $this->userData->id;
        // daneshi added for TODO
        if ($this->permissions()->getPermissions('forum-admin-can-manage-all-topics')) {
            $userId = null;
        }
        // TODO If admin then : $userId = null;
        $forumCategoriesTable = new ForumCategoriesTable($this->getServiceLocator());
        $topicForm = new TopicForm($forumCategoriesTable->getRecords());
        $request = $this->getRequest();
        $topicId = $this->params('id');
        if (!$topicId) {
            return $this->redirect()->toRoute('forum-admin', array(
                'action' => 'topics'
            ));
        }
    
        $forumTopicsTable = new ForumTopicsTable($this->getServiceLocator());
        $topic = $forumTopicsTable->getUserTopics($userId, $topicId);
        if (!$topic) {
            return $this->redirect()->toRoute('forum-admin', array(
                'action' => 'topics'
            ));
        }
        $topicForm->populateValues($topic[0]);
        if ($request->isPost()) {
            $postData = $request->getPost();
            $topicForm->setData($postData);
            if ($topicForm->isValid()) {
                $validData = $topicForm->getData();
                if ($forumTopicsTable->editTopic($validData, $topicId, $userId) !== false) {
                    $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت ثبت گردید.');
                    return $this->redirect()->toRoute('forum-admin', array(
                        'action' => 'topics'
                    ));
                }
                $this->layout()->errorMessage = 'مشکل در ثبت اطلاعات.';
    
            } else {
                $this->layout()->errorMessage = 'ورودی فرم نامعتبر است.';
            }
        }
        $view['topicForm'] = $topicForm;
        return new ViewModel($view);
    }
    
    public function deleteTopicAction()
    {
        $userId = $this->userData->id; 
        if ($this->permissions()->getPermissions('forum-admin-can-manage-all-topics')) {
        	$userId = null;
        }
        $container = new Container('token');
        $token = $this->params('token');
        $topicId = $this->params('id');
        if ($topicId && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $forumTopicsTable = new ForumTopicsTable($this->getServiceLocator());
                if ($forumTopicsTable->deleteTopic($topicId, $userId)) {
                    $this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت حذف گردید');
                } else {
                    $this->flashMessenger()->addErrorMessage('مشکل در حذف گروه');
                }
            }
        }
        return $this->redirect()->toRoute('forum-admin', array(
            'action' => 'topics'
        ));
    }
     
    public function postsAction()
    {
    	$userId = $this->userData->id;
    	if ($this->permissions()->getPermissions('forum-admin-can-manage-all-posts')) {
    		$userId = null;
    	}
    	$container = new Container('token');
    	$view['hash'] = md5(rand() . time() . rand());
    	$container->offsetSet('csrf', $view['hash']);
    	$forumPostsTable = new ForumPostsTable($this->getServiceLocator());
    	$view['posts'] = $forumPostsTable->getPostList($userId);
    	return new ViewModel($view);
    }
    
    public function deletePostAction()
    {
    	$userId = $this->userData->id;
    	if ($this->permissions()->getPermissions('forum-admin-can-manage-all-posts')) {
    		$userId = null;
    	}
    	$container = new Container('token');
    	$token = $this->params('token');
    	$postId = $this->params('id');
    	if ($postId && $container->offsetExists('csrf')) {
    		if ($container->offsetGet('csrf') == $token) {
    			$forumPostsTable = new ForumPostsTable($this->getServiceLocator());
    			if ($forumPostsTable->deletePost($postId, $userId)) {
    				$this->flashMessenger()->addSuccessMessage('اطلاعات مورد نظر با موفقیت حذف گردید');
    			} else {
    				$this->flashMessenger()->addErrorMessage('مشکل در حذف پست');
    			}
    		}
    	}
    	return $this->redirect()->toRoute('forum-admin', array(
    			'action' => 'posts'
    	));
    } 
}
