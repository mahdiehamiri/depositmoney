<?php
namespace Forum\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Sql\Ddl\Column\Varbinary;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use Forum\Model\ForumPostsThanksTable;

class AjaxController extends AbstractActionController
{
    protected $userData;
    
    public function onDispatch(MvcEvent $e)
    {
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData  = $this->authenticationService->isLoggedIn();  
        parent::onDispatch($e);
    }
    
    public function addThanksAction()
    {
        $view['status'] = false;
        $view['message'] = 'دسترسی موجود نیست';
        if ($this->userData->id) {
            $request = $this->getRequest();
            if ($request->isPost()) {
                $postData = $request->getPost();
                if (isset($postData['id'])) {
                    $postId = (int)$postData['id'];
                    $forumPostsThanksTable = new ForumPostsThanksTable($this->getServiceLocator());
                    // TODO check if not own post
                    try {
                        $lastId = $forumPostsThanksTable->addPostThanks($postId, $this->userData->id);
                        if ($lastId) {
                            $view['thanksid'] = $lastId;
                            $view['status'] = true;
                            $view['message'] = $this->userData->firstname . ' ' . $this->userData->lastname;
                        }
                    } catch (\Exception $e) {
                        $view['status'] = false;
                        $view['message'] = 'امکان ثبت وجود ندارد';
                    }
                } 
            }  
        }
        return new JsonModel($view);
    }

    public function removeThanksAction()
    {
        $view['status'] = false;
        $view['message'] = 'دسترسی موجود نیست';
        if ($this->userData->id) {
            $request = $this->getRequest();
            if ($request->isPost()) {
                $postData = $request->getPost();
                if (isset($postData['id'])) {
                    $postThanksId = (int)$postData['id'];
                    $factoryModel = $this->serviceLocator->get('ModelService');
                    $forumPostsThanksTable = new ForumPostsThanksTable($this->getServiceLocator());
                    // TODO check if not own post
                    try {
                        if ($forumPostsThanksTable->deletePostThanks($postThanksId, $this->userData->id)) {
                            $view['status'] = true;
                            $view['message'] = 'با موفقیت حذف شد';
                        } else {
                            $view['status'] = false;
                            $view['message'] = 'امکان حذف وجود ندارد';
                        }
                    } catch (\Exception $e) {
                        $view['status'] = false;
                        $view['message'] = 'امکان حذف وجود ندارد';
                    }
                }
            }
        }
        return new JsonModel($view);
    }
}
