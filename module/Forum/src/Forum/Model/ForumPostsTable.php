<?php 

namespace Forum\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\TableGateway\TableGateway;

class ForumPostsTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('forum_posts', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
	    $where = null;
	    if ($id) {
	        $where = array(
	            'forum_posts.id' => $id
	        );  
	    } 
		return $this->tableGateway->select($where)->toArray();
	}
	
	public function getPostList($userId = null)
	{
		$select = new Select('forum_posts');
		$select->join('users', 'forum_posts.user_id = users.id', array(
			'firstname',
			'lastname',
			'email'
		));
		$select->join('forum_topics', 'forum_posts.forum_topic_id = forum_topics.id', array(
			'topic_name' 
		));
		$select->join('forum_categories', 'forum_categories.id = forum_topics.forum_category_id', array(
			'category_name' => 'name'
		));
		$select->join('forum_posts_thanks', 'forum_posts_thanks.forum_post_id = forum_posts.id', array(
			'thanks_ids' => new Expression("GROUP_CONCAT(forum_posts_thanks.id)"),
			'thanks_user_ids' => new Expression("GROUP_CONCAT(forum_posts_thanks.user_id)")
		), 'left');
		if ($userId) {
			$select->where(array(
					'forum_posts.user_id' => $userId
			));
		}
		$select->order('id', 'asc');
		$select->group('forum_posts.id');
		$dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
		return new Paginator($dbSelector);
	}
	
	public function getTopRecords($topicId = null)
	{
	    $select = new Select('forum_posts');
	    $select->join('users', 'forum_posts.user_id = users.id', array(
	        'firstname',
	        'lastname',
	        'email'
	    ));
	    $select->join('forum_posts_thanks', 'forum_posts_thanks.forum_post_id = forum_posts.id', array(
	        'thanks_ids' => new Expression("GROUP_CONCAT(forum_posts_thanks.id)"),
	        'thanks_user_ids' => new Expression("GROUP_CONCAT(forum_posts_thanks.user_id)")
	    ), 'left');
	    if ($topicId) {
	        $select->where(array(
	            'forum_topic_id' => $topicId
	        ));
	    }  
	    $select->order('id', 'asc');
	    $select->group('forum_posts.id');
	    $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
	    return new Paginator($dbSelector);
	}
	 
	
	public function addPost($post, $parentId, $userId, $topicId)
	{
	    $this->tableGateway->insert(array(
	        'parent_id' => $parentId,
	        'post_content' => $post,
	        'user_id' => $userId,
	        'forum_topic_id' => $topicId
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function editPost($post, $editPostId, $userId)
	{
	    return $this->tableGateway->update(array(
	        'post_content' => $post
	    ), array(
	        'user_id' => $userId,
	        'id' => $editPostId
	    ));
	}

	public function deletePost($id, $userId = null)
	{
		if ($userId) {
			return $this->tableGateway->delete(array(
				'id' => $id,
				'user_id' => $userId
			));
		}
		return $this->tableGateway->delete(array('id' => $id));
	} 
}
