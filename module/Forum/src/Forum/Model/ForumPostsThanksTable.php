<?php 
namespace Forum\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\TableGateway\TableGateway;

class ForumPostsThanksTable  extends BaseModel
{  
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('forum_posts_thanks', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
	    $where = null;
	    if ($id) {
	        $where = array(
	            'forum_posts_thanks.id' => $id
	        );  
	    } 
		return $this->tableGateway->select($where)->toArray();
	}
    
	public function addPostThanks($postId, $userId)
	{
	    $this->tableGateway->insert(array(
	        'forum_post_id' => $postId,
	        'user_id' => $userId
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function deletePostThanks($postThanksId, $userId = null)
	{
	    if ($userId) {
    	    return $this->tableGateway->delete(array(
    	        'user_id'       => $userId,
    	        'id' => $postThanksId
    	    ));	        
	    } else {
    	    return $this->tableGateway->delete(array(
    	        'id' => $postThanksId
    	    ));
	    }
	}
	
	 
}