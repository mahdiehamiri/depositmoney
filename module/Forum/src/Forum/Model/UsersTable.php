<?php 
namespace Forum\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class UsersTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('users', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllUsers()
	{
		$select = new Select('users');
		$users = $this->tableGateway->selectWith($select)->toArray();
		$userList = array();
		foreach ($users as  $user) {
		    $userList[$user['id']] = $user['firstname'] . ' ' . $user['lastname'];
		}
		return $userList;
	}
	 
}