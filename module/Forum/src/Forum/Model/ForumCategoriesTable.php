<?php 
namespace Forum\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class ForumCategoriesTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('forum_categories', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
	    if ($id) {
    		return $this->tableGateway->select(function ($select) use ($id) {
    		    $select->where(array(
    		        'forum_categories.id' => $id
    		    ));
    		    $select->join('forum_main_categories', 'forum_categories.main_category_id = forum_main_categories.id', array('main_category_name' => 'name'));
    		})->toArray();
	    } 
		return $this->tableGateway->select(function ($select) { 
		    $select->join('forum_main_categories', 'forum_categories.main_category_id = forum_main_categories.id', array('main_category_name' => 'name'));
		})->toArray();
	}
	
	public function getRecordsAsArray($mainCategoryId = null)
	{
	    if ($mainCategoryId) {
    	    $tempCategories = $this->tableGateway->select(array('main_category_id' => $mainCategoryId))->toArray();
	    } else {
    	    $tempCategories = $this->tableGateway->select()->toArray();	        
	    }
	    $categories = array();
	    if ($tempCategories) {
	        foreach ($tempCategories as $category) {
	            $categories[$category['main_category_id']][$category['id']] = $category['name'];
	        }
	    }
	    return $categories;
	}
	
	
	public function getRecordsByTopics($categorySlug = null)
	{
	    return $this->tableGateway->select(function($select) use ($categorySlug) { 
	        $select->join('forum_topics', 'forum_categories.id = forum_topics.forum_category_id', array(
	            'topic_name',
	            'user_id',
	            'create_time'
	        ));
	        $select->join('forum_posts', 'forum_topics.id = forum_posts.forum_topic_id', array(
	            'post_count' => new Expression('SUM(IF(forum_posts.id IS NULL,0,1))'),
	            'last_post' => new Expression('max(forum_posts.create_time)')
	        ), Select::JOIN_LEFT);
	        if ($categorySlug) {
	            $select->where->expression("REPLACE(name, ' ', '-') = ?", $categorySlug);
	        }
 	        $select->group('forum_topics.id'); 
	    })->toArray();
	}
	
	
	public function addCategory($data)
	{
	    $this->tableGateway->insert(array(
	        'name' => $data['name'],
	        'main_category_id' => $data['main_category_id']
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function editCategory($data, $id)
	{
	   return $this->tableGateway->update(array(
	       'name' => $data['name'],
	       'main_category_id' => $data['main_category_id']
	   ), array(
	       'id' => $id
	   ));
	}
	
	public function deleteCategory($id)
	{
		return $this->tableGateway->delete(array('id' => $id));
	}
}