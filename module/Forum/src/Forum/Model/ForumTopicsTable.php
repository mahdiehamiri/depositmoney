<?php 
namespace Forum\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\TableGateway\TableGateway;

class ForumTopicsTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('forum_topics', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
	    if ($id) {
    		return $this->tableGateway->select(function ($select) use ($id) {
    		    $select->where(array(
    		        'forum_topics.id' => $id
    		    ));
    		    $select->join('forum_categories', 'forum_categories.id = forum_topics.forum_category_id', array('category_name' => 'name'));
    		})->toArray();
	    } 
		return $this->tableGateway->select(function ($select) { 
		    $select->join('forum_categories', 'forum_categories.id = forum_topics.forum_category_id', array('category_name' => 'name'));
		})->toArray();
	}
	
	public function getUserTopics($userId = null, $topicId = null)
	{
        return $this->tableGateway->select(function ($select) use ($userId, $topicId) {
            if ($userId) {
                $select->where(array(
                    'forum_topics.user_id' => $userId
                ));
            }
            if ($topicId) {
                $select->where(array(
                    'forum_topics.id' => $topicId
                ));                
            }
            $select->join('forum_categories', 'forum_categories.id = forum_topics.forum_category_id', array('category_name' => 'name'));
        })->toArray();
	}
	
	public function getTopRecords($categoryId = null, $orderby = 'date')
	{
	    $select = new Select('forum_topics');
	    $select->join('users', 'forum_topics.user_id = users.id', array(
	        'firstname',
	        'lastname',
	        'email'
	    ));
	    if ($categoryId) {
	        $select->where(array(
	            'forum_category_id' => $categoryId
	        ));
	    }
	    $select->join('forum_posts', 'forum_topics.id = forum_posts.forum_topic_id', array(
	        'post_count' => new Expression("SUM(IF(forum_posts.id IS NULL, 0, 1))")
	    ), Select::JOIN_LEFT);
	    $select->group('forum_topics.id');
	    $select->order('forum_posts.id DESC');
	    $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
	    return new Paginator($dbSelector);
	}
	
	public function getTopicBySlug($topicSlug)
	{
	    return $this->tableGateway->select(function ($select) use ($topicSlug) { 
	        $select->where->expression("REPLACE(topic_name, ' ', '-') = ?", $topicSlug);
	        $select->join('users', 'forum_topics.user_id = users.id', array(
	            'firstname',
	            'lastname',
	            'email'
	        ));
	        $select->join('forum_categories', 'forum_categories.id = forum_topics.forum_category_id', array('category_name' => 'name'));
	    })->toArray();
	}
	 
	
	public function addTopic($data, $userId)
	{
	    $this->tableGateway->insert(array(
	        'topic_name'        => $data['topic_name'],
	        'topic_content'     => $data['topic_content'],
	        'forum_category_id' => $data['forum_category_id'],
	        'user_id'           => $userId
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function editTopic($data, $id, $userId = null)
	{
	    if ($userId) {
    	   return $this->tableGateway->update(array(
              'topic_name'        => $data['topic_name'],
              'topic_content'     => $data['topic_content'],
              'forum_category_id' => $data['forum_category_id'],
    	   ), array(
    	       'id' => $id,
    	       'user_id' => $userId
    	   ));
	    } else {
	        return $this->tableGateway->update(array(
	            'topic_name'        => $data['topic_name'],
	            'topic_content'     => $data['topic_content'],
	            'forum_category_id' => $data['forum_category_id'],
	        ), array(
	            'id' => $id,
	        ));
	    }
	}
	
	public function deleteTopic($id, $userId = null)
	{
	    if ($userId) {
    		return $this->tableGateway->delete(array(
    		    'id' => $id,
    		    'user_id' => $userId
    		));
	    }
		return $this->tableGateway->delete(array('id' => $id));
	}
}
