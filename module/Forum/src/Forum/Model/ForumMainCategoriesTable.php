<?php 
namespace Forum\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class ForumMainCategoriesTable  extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('forum_main_categories', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
	    if ($id) {
    		return $this->tableGateway->select(array(
    		    'id' => $id
    		))->toArray();
	    } 
		return $this->tableGateway->select()->toArray();
	}
	
	public function getRecordsAsArray()
	{
	    $tempMainCategories = $this->tableGateway->select()->toArray();
	    $mainCategories = array();
	    if ($tempMainCategories) {
	        foreach ($tempMainCategories as $mainCategory) {
	            $mainCategories[$mainCategory['id']] = $mainCategory['name'];
	        }
	    }
	    return $mainCategories;
	}
	
	public function getRecordsByCategories()
	{
	    return $this->tableGateway->select(function($select) {
	        $select->join('forum_categories', 'forum_categories.main_category_id = forum_main_categories.id', array(
	            'category_name' => 'name', 
	            'category_id' => 'id'
	        ));
	        $select->join('forum_topics', 'forum_categories.id = forum_topics.forum_category_id', array(
	            'topic_count' => new Expression('SUM(IF(forum_topics.id IS NULL,0,1))')
	        ), Select::JOIN_LEFT);
	        $select->join('forum_posts', 'forum_topics.id = forum_posts.forum_topic_id', array(
	            'post_count' => new Expression('SUM(IF(forum_posts.id IS NULL,0,1))'),
	            'last_post' => new Expression('max(forum_posts.create_time)')
	        ), Select::JOIN_LEFT);
	        $select->group('forum_categories.id');
	    })->toArray();
	}
	
	
	public function addMainCategory($name)
	{
	    $this->tableGateway->insert(array(
	        'name' => $name,
	    ));
	    return $this->tableGateway->getLastInsertValue();
	}
	
	public function editMainCategory($name, $id)
	{
	   return $this->tableGateway->update(array(
	       'name' => $name
	   ), array(
	       'id' => $id
	   ));
	}
	
	public function deleteMainCategory($id)
	{
		return $this->tableGateway->delete(array('id' => $id));
	}
}