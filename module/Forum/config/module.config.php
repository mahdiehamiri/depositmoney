<?php
return array(
    'router' => array(
        'routes' => array(
            'forum-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin-forum[/:action[/:id[/token/:token]]]', 
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'token' => '[a-f0-9]{32}',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array( 
                        '__NAMESPACE__' => 'Forum\Controller',
                        'controller' => 'Admin',
                        'action'     => 'index',
                    ),
                ),
            ),
            'forum-home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/forum[/page/:page]',
                    'constraints' => array(
                        'page' => '[0-9]+'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Forum\Controller',
                        'controller' => 'Index',
                        'action'     => 'index',
                    ),
                ),
            ), 
            'forum-category' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/forum/category/:slug[/page/:page]',
                    'constraints' => array(
                        'slug' => '(.*)',
                        'page' => '[0-9]+'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Forum\Controller',
                        'controller' => 'Index',
                        'action'     => 'category',
                    ),
                ),
            ),
            'forum-topic' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/forum/category/:topic/:slug/[page/:page]',
                    'constraints' => array(
                        'slug' => '(.*)',
                        'topic' => '(.*)',
                        'page' => '[0-9]+'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Forum\Controller',
                        'controller' => 'Index',
                        'action'     => 'topic',
                    ),
                ),
            ),
            'forum-ajax' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/forum/ajax/:action',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array( 
                        '__NAMESPACE__' => 'Forum\Controller',
                        'controller' => 'Ajax',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            include_once(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);