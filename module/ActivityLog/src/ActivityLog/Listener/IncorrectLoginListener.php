<?php
namespace ActivityLog\Listener;

use Zend\EventManager\ListenerAggregateInterface; 
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Adapter\Adapter; 
use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ActivityLog\Model\ActivityLogsTable;

class IncorrectLoginListener implements ListenerAggregateInterface
{

    protected $adapter;

    protected $listeners = array();

    public function attach(EventManagerInterface $events)
    { 
        $sharedEvents = $events->getSharedManager();
        $isAllowedEvent = $this->isLogActive();
        // If event is active
        if($isAllowedEvent) {
            $this->listeners[] = $sharedEvents->attach('*', 'myIncorrectLoginEventLog', array(
                $this,
                'printLog'
            ), 100);
        }
    }

    public function detach(EventManagerInterface $events)
    {
        foreach($this->listeners as $index => $listener) {
            if($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }

    public function printLog(EventInterface $e)
    {
        $params = $e->getParams();
        // echo $this->configs['activeLog']['myProductEventLog'];
        $this->cauthentication = $this->getServiceLocator()->get('CustomerAuthentication');
        if($this->cauthentication->userData) {
            $userId = $this->cauthentication->userData->id;
        } else {
            $this->authentication = $this->getServiceLocator()->get('Authentication');
            if($this->authentication->userData) {
                $userId = $this->authentication->userData->id;
            } else {
                $userId = NULL;
            }
        }
        $this->permissionManager = $this->getServiceLocator()->get("PermissionManager");
        $request = $this->permissionManager->route;
        $adapter = $this->getAdapter();
        $activityLogsTable = new ActivityLogsTable($adapter);
        $params["parameter"] =(isset($params["parameter"]) ? $params["parameter"] : null);
        $activityLogsTable->addNewLogs($userId, $request, $params["parameter"], 1);
    }

    public function getAdapter()
    {
        $config = $this->services->get('config');
        $adapter = new Adapter($config['db_config']['db_default']);
        return $adapter;
    }

    public function isLogActive()
    {
        $this->configs = $this->getServiceLocator()->get('config');
        // If event is active
        if($this->configs['activeLog']['myIncorrectLoginEventLog']) {
            return true;
        } else {
            return false;
        }
    }
}
?>