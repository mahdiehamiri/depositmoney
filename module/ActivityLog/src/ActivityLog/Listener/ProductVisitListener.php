<?php
namespace ActivityLog\Listener;

use Zend\EventManager\ListenerAggregateInterface; 
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Adapter\Adapter; 
use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ActivityLog\Model\ActivityLogsTable;
use Zend\Db\Sql\Where;
use Application\Helper\Permissions;

class ProductVisitListener implements ListenerAggregateInterface
{

    protected $adapter;

    protected $userData;

    protected $listeners = array();

    public function attach(EventManagerInterface $events)
    {
        $sharedEvents = $events->getSharedManager();   
        // If event is active
        $isAllowedEvent = $this->isLogActive();
        if ($isAllowedEvent) {
            $this->listeners[] = $sharedEvents->attach('*', 'myProductEventLog', array(
            $this,
            'addLog'
            ), 100);
        }  
        
    }

    public function detach(EventManagerInterface $events)
    {
        foreach($this->listeners as $index => $listener) {
            if($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }

    public function addLog(EventInterface $e)
    {//var_dump('a');die;
        $params = $e->getParams();   
      //echo  $this->configs['activeLog']['myProductEventLog']; 
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData = $this->authenticationService->isLoggedIn();
//         die(var_dump($this->userData));
        if ($this->userData) {
           $userId = $this->userData->id;
        } else {
            $userId = NULL;
        }
//               $this->authentication = $this->getServiceLocator()->get('AuthenticationService');
//               if ($this->authentication->userData) {
//                   $userId = $this->authentication->userData->id;
//               } else {
//                   $userId = NULL;
//               }
//         } 
//         $this->permissionManager = $this->getServiceLocator()->get("PermissionManager"); 
//         $this->permissionManager = new permissions();
//         $permission = $this->permissionManager->getPermissions();
//         var_dump($permission);die;
//         $request = '/servi/ces/' ;   
//         $this->permissionManager = $this->getServiceLocator()->get("PermissionManagerService");
        $request = $params["route"] ; 
        $adapter = $this->getAdapter();
        $activityLogsTable = new ActivityLogsTable($adapter); 
        $session = session_id ();
        $where = new Where();
        $time = time ();
        $time_check = $time - 600; // SET TIME 10 Minute
        $where->equalTo ( 'session', $session );
        $where->equalTo ( 'request', $request );
        $where->equalTo ( 'status', 1 );
        $where->equalTo ( 'parameter', $params["parameter"] );
        $where->greaterThan( 'time', $time_check );
        $activityLogsTableData = $activityLogsTable->getSessionIsExist ( $where );//var_dump($activityLogsTableData);die;
        if (!$activityLogsTableData) {
            $addedId = $activityLogsTable->addNewLogsBySession( $userId, $session,$time, $request, $params["parameter"], 1 );
        } else {
            $activityLogsTableData = $activityLogsTableData[0];
            $data = array(
                'session' => $session,
                'time' => $time,
            );
            $activityLogsTable->updateLog( $data );
        }  
    }

    public function getAdapter()
    {
        $config = $this->services->get('config'); 
        $adapter = new Adapter($config['db']);
        return $adapter;
    }
    
    public function isLogActive()
    {
        $this->configs = $this->getServiceLocator()->get('config'); 
        // If event is active
        if ($this->configs['activeLog']['myProductEventLog']) {
            return true;
        } else {
            return false;
        }
    }
}
?>