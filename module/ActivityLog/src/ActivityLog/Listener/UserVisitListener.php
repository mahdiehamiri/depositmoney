<?php
namespace ActivityLog\Listener;

use Zend\EventManager\ListenerAggregateInterface; 
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Adapter\Adapter; 
use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ActivityLog\Model\ActivityLogsTable;
use Zend\Db\Sql\Where;

class UserVisitListener implements ListenerAggregateInterface
{

    protected $adapter;

    protected $listeners = array();

    public function attach(EventManagerInterface $events)
    {
        $sharedEvents = $events->getSharedManager();
        $isAllowedEvent = $this->isLogActive();
        // If event is active
        if ($isAllowedEvent) {
            $this->listeners[] = $sharedEvents->attach('*', 'myUserEventLog', array(
                $this,
                'printLog'
            ), 100);
        }
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }

    public function printLog(EventInterface $e)
    {
        $params = $e->getParams();
        // echo $this->configs['activeLog']['myProductEventLog'];
//         $this->cauthentication = $this->getServiceLocator()->get('AuthenticationService');
        $this->authenticationService = $this->getServiceLocator()->get('AuthenticationService');
        $this->userData = $this->authenticationService->isLoggedIn();
//         if ($this->cauthentication->userData) {
//             $userId = $this->cauthentication->userData->id;
//         } else {
//             $this->authentication = $this->getServiceLocator()->get('AuthenticationService');
//             if ($this->authentication->userData) {
//                 $userId = $this->authentication->userData->id;
//             } else {
//                 $userId = NULL;
//             }
//         }
        if ($this->userData) {
            $userId = $this->userData->id;
        } else {
            $userId = NULL;
        }
//         $this->permissionManager = $this->getServiceLocator()->get("PermissionManager");
        $request = $params["route"];
        $adapter = $this->getAdapter();
        $activityLogsTable = new ActivityLogsTable($adapter); 
        $session = session_id ();
        $where = new Where();
        $time = time ();
        $time_check = $time - 600; // SET TIME 10 Minute
        $where->equalTo ( 'session', $session );
        $where->equalTo ( 'status', 1 );
        $where->equalTo ( 'request', $request );
        $where->greaterThan( 'time', $time_check );
        $activityLogsTableData = $activityLogsTable->getSessionIsExist ( $where ); 
        if (!$activityLogsTableData) { 
            $addedId = $activityLogsTable->addNewLogsBySession( $userId, $session,$time, $request, null, 1 );
        } else {
            $activityLogsTableData = $activityLogsTableData[0];
            $data = array(
                'session' => $session,
                'time' => $time,
            );
            $activityLogsTable->updateLog( $data );
        }  
    }

    public function getAdapter()
    {
        $config = $this->services->get('config');
        $adapter = new Adapter($config['db']);
        return $adapter;
    }

    public function isLogActive()
    {
        $this->configs = $this->getServiceLocator()->get('config');
        if ($this->configs['activeLog']['myUserEventLog']) {
            return true;
        } else {
            return false;
        }
    }
}
?>