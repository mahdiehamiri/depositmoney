<?php

namespace ActivityLog\Listener;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Adapter\Adapter;
use Smspanel\Model\LogTable;
use Zend\Http\Response;
use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ActivityLog\Model\ActivityLogsTable;
use ActivityLog\Model\UserOnlineTable;
use Zend\Db\Sql\Where;

class OnlineUserListener implements ListenerAggregateInterface {
	protected $adapter;
	protected $listeners = array ();
	public function attach(EventManagerInterface $events) {
		$sharedEvents = $events->getSharedManager ();
		$isAllowedEvent = $this->isLogActive ();
		// If event is active
		if ($isAllowedEvent) {
			$this->listeners [] = $sharedEvents->attach ( '*', 'myOnlineUserLog', array (
					$this,
					'printLog' 
			), 100 );
		}
	}
	public function detach(EventManagerInterface $events) {
		foreach ( $this->listeners as $index => $listener ) {
			if ($events->detach ( $listener )) {
				unset ( $this->listeners [$index] );
			}
		}
	}
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
	 
		$this->services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->services;
	}
	public function printLog(EventInterface $e) {
		$params = $e->getParams (); 
	 
		$this->authenticationService = $this->getServiceLocator ()->get ( 'AuthenticationService' );
		if ($this->authenticationService->isLoggedIn()) {
			$this->userData = $this->authenticationService->isLoggedIn();
			$userId = $this->userData->id;
		} else {  
				$userId = NULL; 
		}
		  
	    $routeMatch = $this->getServiceLocator()->get('Application')->getMvcEvent()->getRouteMatch(); 
		$request = $routeMatch->getMatchedRouteName();;
		$adapter = $this->getAdapter();
		$session = session_id ();
		
		$activityLogsTable = new UserOnlineTable($adapter );
		$where = new Where ();
		$time = time ();
		$time_check = $time - 600; // SET TIME 10 Minute
		$where->equalTo ( 'session', $session );
		$where->equalTo ( 'status', 1 ); 
		$where->greaterThan( 'time', $time_check );
  
		$activityLogsTableData = $activityLogsTable->getSessionIsExist ( $where );
 
		
		
		if (!$activityLogsTableData) {
			  $addedId = $activityLogsTable->addNewLogs( $userId, $session,$time, $request, null, 1 );
	 
		} else {
			$activityLogsTableData = $activityLogsTableData[0];
			$data = array(
				'session' => $session,
				'time' => $time,
			);
			$activityLogsTable->updateLog( $data ); 
		}  
 
		$data = array( 
				'time' => $time_check,
				'status' => 0,
		);
		$activityLogsTable->disableLog( $data );
 
	}
	public function getAdapter() { 
		$config = $this->getServiceLocator()->get('Config') ;
		$adapter = new Adapter ( $config ['db'] );
		return $adapter;
	}
	public function isLogActive() {
		$this->configs = $this->getServiceLocator ()->get ( 'Config' );
		// If event is active
		if ($this->configs ['activeLog'] ['myOnlineUserLog']) {
			return true;
		} else {
			return false;
		}
	}
}
?>