<?php
namespace ActivityLog\Model;

use Zend\Db\TableGateway\TableGateway; 
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
class UserOnlineTable
{
    protected $tableGateway;
    protected $serviceManager;
    protected $adapter;
    public function __construct($sm)
    { 
    	 $this->serviceManager = $sm ;
    	 $this->tableGateway = new TableGateway('user_online', $this->getAdapter());
    } 
    
     
    public function addNewLogs($userId = null,$session=null,$time =null, $request = null,  $parameter = null, $visit)
    { 
       
    		$data = array(
    				'user_id' => $userId, 
    		        'request' => $request,  
    		        'session' => $session,  
    		        'time' => $time,  
    		        'ip_address' => $_SERVER["REMOTE_ADDR"],
    		);
  
    		if ($_SERVER["REMOTE_ADDR"]) {
	    		$this->tableGateway->insert($data);
	    		return $this->tableGateway->getLastInsertValue(); 
    		}
    }
    
    public function getSessionIsExist($where)
    { 
    	 $select = new Select('user_online');
    	 $select->where($where);
    	 return $this->tableGateway->selectWith($select)->toArray(); 
    }
 
    public function updateLog($data)
    { 
        return $this->tableGateway->update(array( 
            'time' => $data['time'], 
        ), array("session" => $data['session']));
    }
    public function disableLog($data)
    { 
        return $this->tableGateway->update(array( 
            'status' => $data['status'], 
        ), array("time < ?" => $data['time']));
    }
    public function deleteSessin($where)
    { 
        return $this->tableGateway->delete($where);
    }
    
    public function getAdapter()
    {
    	if (!$this->adapter) {
    		$sm = $this->serviceManager;
    		if(method_exists($sm , 'get'))
    			$this->adapter = $sm->get('Zend\Db\Adapter\Adapter');
    		else 
    			$this->adapter = $sm ;
    	}
    	return $this->adapter;
    }
    
}