<?php
namespace ActivityLog\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select; 
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Application\Helper\Jdates;

class ActivityLogsTable 
{

    protected $tableGateway;

    protected $serviceManager;

    protected $adapter;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('activity_log', $this->getAdapter());
    }

    public function addNewLogs($userId = null, $request = null, $parameter = null, $visit)
    {
        $mydate = date('Y-m-d');  
        $mydate = explode('-', $mydate);
        $date = Jdates::gregorian_to_jalali($mydate[0],$mydate[1],$mydate[2], '-');
        $date = $date[0].'-'.$date[1]; 
        $data = array(
            'user_id' => $userId,
            'request' => $request,
            'parameter' => $parameter,
            'visit' => 1,
            'persian_year_month' => $date,
            'persianday' => $date[2],
            'ip_address' => $_SERVER["REMOTE_ADDR"]
        );
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function addNewLogsBySession($userId = null, $session = null, $time = null, $request = null, $parameter = null, $visit)
    {
        $mydate = date('Y-m-d');
//         $date = Jdates::gregorian_to_jalali_byDate($mydate, '-');
//         $date = $date[0].'-'.$date[1]; 
        $data = array(
            'user_id' => $userId,
            'request' => $request,
            'session' => $session,
            'parameter' => $parameter,
//             'persian_year_month' => $date,
//             'persianday' => $date[2],
            'visit' => 1,
            'time' => $time,
            'ip_address' => $_SERVER["REMOTE_ADDR"]
        );
        
        if ($_SERVER["REMOTE_ADDR"]) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->getLastInsertValue();
        }
    }

    public function getAdapter()
    {
        if (! $this->adapter) {
            $sm = $this->serviceManager;
            if (method_exists($sm, 'get'))
                $this->adapter = $sm->get('Zend\Db\Adapter\Adapter');
            else
                $this->adapter = $sm;
        }
        return $this->adapter;
    }

    public function getSessionIsExist($where)
    {
        $select = new Select('activity_log');
        $select->where($where);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function updateLog($data)
    {
        return $this->tableGateway->update(array(
            'time' => $data['time']
        ), array(
            "session" => $data['session']
        ));
    }
    public function updatePersianDate($data)
    {
        return $this->tableGateway->update(array(
            'persian_year_month' => $data['persian_year_month'],
            'persianday' => $data['persianday']
        ), array(
            "id" => $data['id']
        ));
    }
    public function updateStatus($where)
    {
        return $this->tableGateway->update(array( 
            'status' => 0
        ),$where);
    }

    public function getDataGroubByMonth()
    {
        $select = new Select('activity_log');
        $select->columns(array(
            'persian_year_month',
            'count' => new Expression('count(*)')
        ));
        $select->where->equalTo('status', 1); 
        $select->group('persian_year_month'); 
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function getSessionIsExistOnlines($where = false, $groupBy = false)
    {
        $select = new Select('activity_log');
        if ($where) {
            $select->where($where);
        }
        if ($groupBy) {
            $select->group('persiandate');
        } 
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function getSessionOnlines($where = false, $groupBy = false)
    {
        $select = new Select('activity_log');
        $select->columns(array(
            '*',
            'count' => new Expression('COUNT(*)')
        ));
        if ($where) {
            $select->where($where);
        }
        $select->group('request');
        $select->group('parameter');
 
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getMostVisitedProduct()
    {
        $select = new Select('activity_log');
        $select->where->like('request', '%maincontent-product%');
        $select->group('parameter');
        $select->columns(array(
            'id',
            'count' => new Expression("COUNT('parameter')")
        ));
        $select->join('product_view', 'product_view.id = activity_log.parameter', array(
            'pvId' => 'id',
            'name'
        ), 'left');
        $select->order('count DESC');
        $select->where->isNotNull('parameter');
        $select->limit(10);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function getMostVisitedGroup()
    {
        $select = new Select('activity_log');
        $select->where->like('request', '%maincontent-index-category%');
        $select->group('parameter');
        $select->columns(array(
            'id',
            'count' => new Expression("COUNT('parameter')")
        ));
        $select->join('product_group', 'product_group.id = activity_log.parameter', array(
            'gId' => 'id',
            'name'
        ), 'left');
        $select->order('count DESC');
        $select->where->isNotNull('parameter');
        $select->limit(10);
        return $this->tableGateway->selectWith($select)->toArray();
    }
}