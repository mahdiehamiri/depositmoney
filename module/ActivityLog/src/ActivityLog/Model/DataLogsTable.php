<?php
namespace ActivityLog\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Application\Helper\Jdates;

class DataLogsTable
{

    protected $tableGateway;

    protected $serviceManager;

    protected $adapter;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('logs', $this->getAdapter());
    }

    public function getAdapter()
    {
        if (! $this->adapter) {
            $sm = $this->serviceManager;
            if (method_exists($sm, 'get'))
                $this->adapter = $sm->get('Zend\Db\Adapter\Adapter');
            else
                $this->adapter = $sm;
        }
        return $this->adapter;
    }

    public function getSessionIsExistOnlines($where = false, $groupBy = false)
    {
        $select = new Select('logs');
        if ($where) {
            $select->where($where);
        }
        if ($groupBy) {
            $select->group('persiandate');
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function getTotalVisitCount($where = false, $groupBy = false)
    {
        $select = new Select('logs');
        $select->columns(array( 
            'count'=> new Expression('SUM(visit)')
        ));
        
        $res = $this->tableGateway->selectWith($select)->current();
        return $res['count'];
    }

    public function addNewLogs($data)
    {
        $data = array(
            'request' => $data['request'],
            'parameter' => $data['parameter'],
            'visit' => $data['count'],
            'persian_year_month' => $data['persian_year_month'],
            'date' => $data['date']
        );
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }
    public function updateLogs($id, $count)
    {
        $data = array( 
            'visit' => $count, 
        );
        return $this->tableGateway->update($data,array('id' => $id)); 
    }
}