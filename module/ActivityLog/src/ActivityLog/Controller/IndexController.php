<?php
namespace ActivityLog\Controller; 

/*  @var $activityLogsTable \ActivityLog\Model\ActivityLogsTable */

use Application\Helper\BaseController;
use ActivityLog\Model\ActivityLogsTable; 
use Zend\Db\Adapter\Adapter;  
use Zend\Db\Sql\Where; 
use ActivityLog\Model\DataLogsTable;
use Application\Helper\Jdates;

class IndexController extends BaseController
{ 
    public function cronAction()
    {  
        $adapter = $this->getAdapter();
        $activityLogsTable = new ActivityLogsTable($adapter); 
        $logsTable = new DataLogsTable($adapter); 
        $where = new Where();
       
        // step 1
         
        $where->equalTo('persian_year_month', '');
        $where->equalTo('status', 1);
        $categoryData = $activityLogsTable->getSessionIsExistOnlines($where);
        foreach ($categoryData as $data) {
            $mydate = explode(' ', $data['date']);
            $date = Jdates::gregorian_to_jalali_byDate($mydate[0], '-'); 
      
            $mdate = $date[0].'-'.$date[1];
            $data['persian_year_month'] = $mdate;
            $data['persianday'] = $date[2];
            $categoryData = $activityLogsTable->updatePersianDate($data);
        }    
        
     
        // step 2
      
       $categoryDateData = $activityLogsTable->getDataGroubByMonth();
    
        if ($categoryDateData) {
            foreach ($categoryDateData as $category) { 
                $moonYear = $category['persian_year_month'];
                $where = new Where();
                $where->like('persian_year_month', "%$moonYear%");
                $categoriesData = $activityLogsTable->getSessionOnlines($where);
                if ($categoriesData) {
                    foreach ($categoriesData as &$dataArray) {
                        $dataArray['persian_year_month'] = $moonYear;
                        $logsTable->addNewLogs($dataArray, $moonYear);
                    } 
                }
         
                $categoriesData = $activityLogsTable->updateStatus($where);
            }
        }
      
        die("true");
        
    }

    public function indexAction()
    {
        
    }
    public function getAdapter()
    { 
        $config = $this->getServiceLocator()->get('config');  
        $adapter = new Adapter($config['db_config']['db_default']); 
        return $adapter;
    }
}
