<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ActivityLog;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;  
use ActivityLog\Listener\ProductVisitListener;
use ActivityLog\Listener\UserVisitListener;
use ActivityLog\Listener\IncorrectLoginListener;

class Module
{	
    public function onBootstrap(MvcEvent $e)
    { 
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        $productListener        = new ProductVisitListener();
        $productListener->setServiceLocator($e->getApplication()->getServiceManager());
        $productListener->attach($eventManager);
        
        $userListener        = new UserVisitListener();
        $userListener->setServiceLocator($e->getApplication()->getServiceManager());
        $userListener->attach($eventManager);
        
        $incorrectLoginListener        = new IncorrectLoginListener();
        $incorrectLoginListener->setServiceLocator($e->getApplication()->getServiceManager());
        $incorrectLoginListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array (
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } 
    
}