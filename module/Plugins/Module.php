<?php 
namespace Plugins;
 
use Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter as Adapter;
use Zend\Config\Reader\Json as JsonParser;
use Zend\ServiceManager\ServiceManager;
use Plugins\Helper\ControllerPluginManager;
use Plugins\Helper\ViewPluginManager;
use Plugins\Listener\PluginListener;

class Module
{ 
    public function onBootstrap(MvcEvent $e)
    {
//         $eventManager   = $e->getApplication()->getEventManager(); 
//         $pluginListener = new PluginListener();
//         $pluginListener->attach($eventManager); 
    }

    public function getConfig()
    {
    	$config = array();
    	
    	foreach (glob(__DIR__ . '/config/plugin.*.config.php') as $file) {
    		$config = array_replace_recursive($config, require $file);
    	} 
        return $config;
    }

   
    
    public function getViewHelperConfig()
    {
    	return array(
    			'factories' => array( 
    					'getPlugin' => function($helperPluginManager) {
    						$pluginManager = new ViewPluginManager($helperPluginManager);
    						return $pluginManager;
    					},
    			)
    	);
    }
    
    public function getAutoloaderConfig()
    { 
    	$folders = scandir( __DIR__ . '/src/');
    	$namespaces[__NAMESPACE__] = __DIR__ . '/main';
    	if ($folders) {
    		foreach ($folders as $folder) {
    			if ($folder == '.' || $folder == '..') {
    				continue;
    			}
    			$namespaces[$folder] = __DIR__ . '/src/' . $folder;
    			if (is_file(__DIR__ . '/src/' . $folder . "/autoload_classmap.php")) {
    			    $namespaces2[$folder] = __DIR__ . '/src/' . $folder . "/autoload_classmap.php";
    			}
    			
    		}
    	} 
        return array(
            'Zend\Loader\ClassMapAutoloader' => $namespaces2,
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => $namespaces,
            ),
        );
    }
  
}
