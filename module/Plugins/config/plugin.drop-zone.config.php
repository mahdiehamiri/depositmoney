<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /plugins/:plugin/:controller/:action
            'drop-zone-home' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/plugins/drop-zone[/:controller][/:action][/configName/:configName][/extraColumnsValue/:extraColumnsValue]',
                    'constraints' => array(
                        'configName' => '[0-9a-f]{32}',
                        'extraColumnsValue' => '.*'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'DropZone\Controller',
                        'controller' => 'Index',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
   
    'drop-zone-configs' => array(
        'config_1' => array(
            "dbTable" => "DropZone\\Model\\GalleryFilesTable",
            "extraColumns" => array(
                "gallery_id",
                "user_id"
              // column name  for file name  =>  file_name
                        ),
            "format" => "Image",
            "uploadPath" => $_SERVER['DOCUMENT_ROOT'] . "/uploads/gallery/",
            "minSize" => 1,
            "maxSize" => 1024,
            "canDeleteUploadedFile" => "true",
            "canSpecialUploadedFile" => "true",
            "extensions" => array(
                'jpg',
                'png',
                'gif',
                'jpeg'
            ),
            "front_acceptedFiles" => ".jpeg,.jpg,.png,.gif",
            "front_maxFilesize" => 200, // mb
            "front_url" => "/plugins/drop-zone/ajax/index",
            "front_deleteUrl" => "/plugins/drop-zone/ajax/delete",
            "front_specialUrl" => "/plugins/drop-zone/ajax/special",
            "uploadPathThumb" => "/uploads/gallery/",
            "front_maxFiles" => 50,
            "front_clickable" => "true"
        ),
        'config_16' => array(
            "dbTable" => "DropZone\\Model\\UsersFileTable",
            "extraColumns" => array(
                "instructor_id",
                "user_id",
            ),
            "format" => "Any",
            "uploadPath" => $_SERVER['DOCUMENT_ROOT'] . "/uploads/usersFile/",
            "minSize" => 1,
            "maxSize" => 1024000,
            "canDeleteUploadedFile" => "true",
            "canSpecialUploadedFile" => "true",
            "extensions" => array(
                '*',
            ),
            "front_acceptedFiles" => "",
            "front_maxFilesize" => 3024000, // mb
            "front_url" => "/plugins/drop-zone/ajax/index",
            "front_deleteUrl" => "/plugins/drop-zone/ajax/delete",
            "front_specialUrl" => "/plugins/drop-zone/ajax/special",
            "uploadPathThumb" => "/uploads/usersFile/",
            "front_maxFiles" => 1000,
            "front_clickable" => "true"
        ),
        'config_2' => array(
            "dbTable" => "DropZone\\Model\\VideoGalleryFilesTable",
            "extraColumns" => array(
                "video_gallery_id",
                "user_id"
                // column name  for file name  =>  file_name
            ),
            "format" => "Video",
            "uploadPath" => $_SERVER['DOCUMENT_ROOT'] . "/uploads/videoGallery/",
            "minSize" => 1,
            "maxSize" => 524288,
            "canDeleteUploadedFile" => "true",
            "canSpecialUploadedFile" => "true",
            "extensions" => array(
                'mpeg',
                'mp4'
            ),
            "front_acceptedFiles" => ".mpeg,.mp4",
            "front_maxFilesize" => 512, // mb
            "front_url" => "/plugins/drop-zone/ajax/index",
            "front_deleteUrl" => "/plugins/drop-zone/ajax/delete",
            "front_specialUrl" => "/plugins/drop-zone/ajax/special",
            "uploadPathThumb" => "/uploads/videoGallery/",
            "front_maxFiles" => 50,
            "front_clickable" => "true"
        ),
        'config_3' => array(
            "dbTable" => "ProductViewFilesTable",
            "extraColumns" => array(
                "gallery_id"
            // column name  for file name  =>  file_name
                        ),
            "format" => "Text",
            "uploadPath" => $_SERVER['DOCUMENT_ROOT'] . "/uploads/uploadlogos/",
            "minSize" => 5,
            "maxSize" => 302400,
            "canDeleteUploadedFile" => "true",
            "canSpecialUploadedFile" => "true",
            "extensions" => array(
                'pdf',
                'txt',
                'doc',
                'docx'
            ),
            "front_acceptedFiles" => ".pdf,.txt,.doc,.docx",
            "front_maxFilesize" => 2, // mb
            "front_url" => "/plugins/drop-zone/ajax/index",
            "front_deleteUrl" => "/plugins/drop-zone/ajax/delete",
            "front_specialUrl" => "/plugins/drop-zone/ajax/special",
            "uploadPathThumb" => "/uploads/uploadlogos/",
            "front_maxFiles" => 50,
            "front_clickable" => "true"
        ),
        
        
        'config_4' => array(
            "dbTable" => "ProductMainFilesTable",
            "extraColumns" => array(
                "product_main_id",
                "user_id"
              // column name  for file name  =>  file_name
                        ),
            "format" => "Image",
            "uploadPath" => $_SERVER['DOCUMENT_ROOT'] . "/uploads/uploadlogos/productMainImage/",
            "minSize" => 5,
            "maxSize" => 1024,
            "canDeleteUploadedFile" => "true",
            "canSpecialUploadedFile" => "true",
            "extensions" => array(
                'jpg',
                'png',
                'gif',
                'jpeg'
            ),
            "front_acceptedFiles" => ".jpeg,.jpg,.png,.gif",
            "front_maxFilesize" => 2, // mb
            "front_url" => "/plugins/drop-zone/ajax/index",
            "front_deleteUrl" => "/plugins/drop-zone/ajax/delete",
            "front_specialUrl" => "/plugins/drop-zone/ajax/special",
            "uploadPathThumb" => "/uploads/uploadlogos/productMainImage/",
            "front_maxFiles" => 50,
            "front_clickable" => "true"
        ),
        
        'config_5' => array(
            "dbTable" => "ProductGroupFilesTable",
            "extraColumns" => array(
                "product_group_id",
                "user_id" 
                ),
            "format" => "Image",
            "uploadPath" => $_SERVER['DOCUMENT_ROOT'] . "/uploads/uploadlogos/productGroupImage/",
            "minSize" => 5,
            "maxSize" => 1024,
            "canDeleteUploadedFile" => "true",
            "canSpecialUploadedFile" => "true",
            "extensions" => array(
                'jpg',
                'png',
                'gif',
                'jpeg'
            ),
            "front_acceptedFiles" => ".jpeg,.jpg,.png,.gif",
            "front_maxFilesize" => 2, // mb
            "front_url" => "/plugins/drop-zone/ajax/index",
            "front_deleteUrl" => "/plugins/drop-zone/ajax/delete",
            "front_specialUrl" => "/plugins/drop-zone/ajax/special",
            "uploadPathThumb" => "/uploads/uploadlogos/productGroupImage/",
            "front_maxFiles" => 1,
            "front_clickable" => "true"
        ),
        'config_6' => array(
            "dbTable" => "DropZone\\Model\\ShopProductVideoFilesTable",
            "extraColumns" => array(
                "product_id",
                "user_id"
                // column name  for file name  =>  file_name
            ),
            "format" => "Video",
            "uploadPath" => $_SERVER['DOCUMENT_ROOT'] . "/uploads/shop/productVideo/",
            "minSize" => 1,
            "maxSize" => 100000,
            "canDeleteUploadedFile" => "true",
            "canSpecialUploadedFile" => "true",
            "extensions" => array(
                'mpeg',
                'mp4'
            ),
            "front_acceptedFiles" => ".mpeg,.mp4",
            "front_maxFilesize" => 100000, // mb
            "front_url" => "/plugins/drop-zone/ajax/index",
            "front_deleteUrl" => "/plugins/drop-zone/ajax/delete",
            "front_specialUrl" => "/plugins/drop-zone/ajax/special",
            "uploadPathThumb" => "/uploads/shop/productVideo/",
            "front_maxFiles" => 50,
            "front_clickable" => "true"
        ),
    ),
    'controllers' => array (
        'invokables' => array (
            'DropZone\Controller\Index' => 'DropZone\Controller\IndexController',
            'DropZone\Controller\Ajax' => 'DropZone\Controller\AjaxController',
        )
    ),
  
);
