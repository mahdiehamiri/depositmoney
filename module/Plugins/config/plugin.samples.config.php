<?php
return array(
    'router' => array(
        'routes' => array( 
            'samples-home' => array(
                'type'    => 'Segment', 
                'options' => array(
                    'route'    => '/plugins/samples[/:controller][/:action][/:id]', 
                	'constraints' => array(
               				'id' => '[1-9][0-9]*'
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Samples\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            'samples-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-samples[/:action][/:id][/token/:token][/name/:name]', 
                		'constraints' => array(
                		    'name' => '[a-z0-9]*',
                		    'lang' => '[a-zA-Z]{2}',
                			'id'  => '[0-9]*',
                			'token' => '[a-f0-9]{32}',
                		    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Samples\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-samples-category' 
                    ),
                ) 
            ),   
        ),
    ),  
 
);
