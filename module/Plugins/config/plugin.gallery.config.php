<?php
return array(
    'router' => array(
        'routes' => array( 
            'gallery-home' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/plugins/gallery[/:controller][/:action][/:id]', 
                	'constraints' => array(
                	    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
               				'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        					'id' => '[0-9]+',
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Gallery\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            'gallery-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-gallery[/:action][/:id][/:token]', 
                		'constraints'  => array(
            		        'lang' => '[a-zA-Z]{2}',
            				'id'    => '[1-9][0-9]*',
            				'token' => '.*'
                		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Gallery\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-galleries' 
                    ),
                ) 
            ),   
            
            'gallery-ajax-selector' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/gallery-ajax-selector',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Gallery\Controller',
                        'controller' => 'Ajax',
                        'action' => 'selector'
                    ),
                ),
            
            ),
 
        ),
    ),  

);
