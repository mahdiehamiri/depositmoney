<?php
return array (
		'router' => array (
				'routes' => array (
						'football' => array (
								'type' => 'Segment',
								'options' => array (
										'route' => '[/:lang]/football[/:action][/id/:id][/token/:token]',
										'constraints' => array (
												'id' => '[1-9][0-9]*',
												'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
												'token' => '[a-f0-9]{32}' 
										),
										'defaults' => array (
												'__NAMESPACE__' => 'Football\Controller',
												'controller' => 'Index',
												'action' => 'index' 
										) 
								) 
						),
						'admin-football' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '[/:lang]/admin-football[/:action][/parameter/:parameter][/:id][/token/:token]',
										'constraints' => array (
												'id' => '[1-9][0-9]*',
												'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
												'parameter' => '[a-zA-Z-][a-zA-Z0-9_-]+',
												'token' => '[a-f0-9]{32}' 
										),
										'defaults' => array (
												'__NAMESPACE__' => 'Football\Controller',
												'controller' => 'Admin',
												'action' => 'list-parameter' 
										) 
								) 
						),
						'football-ajax-getteam' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/football-ajax-getteam',
										'defaults' => array (
												'__NAMESPACE__' => 'Football\Controller',
												'controller' => 'Ajax',
												'action' => 'getteam' 
										) 
								) 
						),
						'football-ajax-getgame' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/football-ajax-getgame',
										'defaults' => array (
												'__NAMESPACE__' => 'Football\Controller',
												'controller' => 'Ajax',
												'action' => 'getgame' 
										) 
								) 
						) ,
						'football-ajax-getprogram' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/football-ajax-getprogram',
										'defaults' => array (
												'__NAMESPACE__' => 'Football\Controller',
												'controller' => 'Ajax',
												'action' => 'getprogram' 
										) 
								) 
						) ,
						'football-ajax-getweek' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/football-ajax-getweek',
										'defaults' => array (
												'__NAMESPACE__' => 'Football\Controller',
												'controller' => 'Ajax',
												'action' => 'getweek' 
										) 
								) 
						) 
				) 
		),  
   /*  'controllers' => array (
    		'invokables' => array (
    				'Football\Controller\Index' => 'Football\Controller\IndexController',
    				'Football\Controller\Admin' => 'Football\Controller\AdminController'
    		)
    ), */
   
);