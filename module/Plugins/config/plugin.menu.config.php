<?php
return array(
    'router' => array(
        'routes' => array( 
            'menu-home' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/plugins/menu[/:controller][/:action][/:id]', 
                	'constraints' => array(
               				'id' => '[1-9][0-9]*'
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Menu\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            'menu-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-menu[/:action][/:id][/token/:token]', 
                		'constraints' => array(
                		    'lang' => '[a-zA-Z]{2}',
            				'id' => '[1-9][0-9]*',
            		        'token' => '[a-f0-9]{32}'
                		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Menu\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-main-menus' 
                    ),
                ) 
            ),       
            'menu-admin-ajax' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-menu-ajax[/:action][/:id][/token/:token]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id' => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Menu\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'delete-menu'
                    ),
                )
            ),
        ),
    ),  
);
