<?php
return array(
    'router' => array(
        'routes' => array( 
            'newsletter-home' => array(
                'type'    => 'Segment', 
                'options' => array(
                    'route'    => '/plugins/newsletter[/:controller][/:action][/:id]', 
                	'constraints' => array(
               				'id' => '[1-9][0-9]*'
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Newsletter\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),    
            'newsletter-ajax' => array (
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/newsletter-ajax/save-newsletter',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Newsletter\Controller',
                        'controller' => 'Ajax',
                        'action'     => 'save-newsletter',
                    ),
                )
            ),
            'newsletter-cron' => array (
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/newsletter-cron/send-mail',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Newsletter\Controller',
                        'controller' => 'Cron',
                        'action'     => 'send-mail',
                    ),
                )
            ),
            'newsletter-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-newsletter[/:action][/:id][/token/:token]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'  => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Newsletter\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-newsletters'
                    ),
                )
            ),
            
        ),
    ),  
);
