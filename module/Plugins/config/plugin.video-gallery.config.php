<?php
return array(
    'router' => array(
        'routes' => array( 
            'video-gallery-home' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/plugins/video-gallery[/:controller][/:action][/:id]', 
                	'constraints' => array(
                	    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
               				'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        					'id' => '[0-9]+',
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'VideoGallery\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            'video-gallery-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-video-gallery[/:action][/:id][/token/:token]', 
                		'constraints'  => array(
            		        'lang' => '[a-zA-Z]{2}',
            				'id'    => '[1-9][0-9]*',
            				'token' => '.*'
                		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'VideoGallery\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-video-galleries' 
                    ),
                ) 
            ),   
 
        ),
    ),  
);
