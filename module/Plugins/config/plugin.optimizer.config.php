<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array( 
        
            'optimizer-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-optimizer[/:action][/:id][/token/:token]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'  => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Optimizer\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'index'
                    ),
                )
            ),
        ),
    ),  
   
);
