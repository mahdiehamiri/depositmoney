<?php
return array(
    'router' => array(
        'routes' => array( 
            'product-comment-home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/plugins/product-comment[/:action][/page/:page]',
                    'constraints' => array(
                            'page' => '[1-9][0-9]*' //star is used to show that the second digit can or cannot be entered.
                    ),
                    'defaults' => array(
                        'controller' => 'ProductComment\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'product-comment-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-product-comment[/:action][/:id][/token/:token]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'  => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'ProductComment\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-all-product-comments'
                    ),
                )
            ),
            'shop-user-ajax-product-comment' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/shop-user-ajax-product-comment/index',
                    'defaults' => array(
                        '__NAMESPACE__' => 'ProductComment\Controller',
                        'controller' => 'Ajax',
                        'action' => 'index'
            
                    ),
                ),
            ),
            'product-comment-ajax-refresh' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/product-comment-ajax-refresh',
                    'defaults' => array(
                        '__NAMESPACE__' => 'ProductComment\Controller',
                        'controller' => 'Ajax',
                        'action' => 'refresh'
            
                    ),
                ),
            ),
        ),
    ), 
);