<?php
return array(
    'router' => array(
        'routes' => array( 
            'contact-home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/plugins/contact[/:action][/page/:page]',
                    'constraints' => array(
                            'page' => '[1-9][0-9]*' //star is used to show that the second digit can or cannot be entered.
                    ),
                    'defaults' => array(
                        'controller' => 'Contact\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'contact-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-contact[/:action][/:id][/token/:token]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'  => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Contact\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-contacts'
                    ),
                )
            ),
            
            'contact-ajax-contact-us' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/contact-ajax-contact-us',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Contact\Controller',
                        'controller' => 'Ajax',
                        'action' => 'index'
            
                    ),
                ),
            ),
            'contact-ajax-contact-us-refresh' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/contact-ajax-contact-us-refresh',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Contact\Controller',
                        'controller' => 'Ajax',
                        'action' => 'refresh'
            
                    ),
                ),
            ),
        ),
    ),
   
);