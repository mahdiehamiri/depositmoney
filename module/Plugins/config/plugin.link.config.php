<?php
return array(
    'router' => array(
        'routes' => array( 
            'link-home' => array(
                'type'    => 'Segment', 
                'options' => array(
                    'route'    => '/plugins/link[/:controller][/:action][/:id]', 
                	'constraints' => array(
               				'id' => '[1-9][0-9]*'
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Link\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            'link-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-link[/:action][/:id][/token/:token]', 
                		'constraints' => array(
                		    'lang' => '[a-zA-Z]{2}',
                			'id'  => '[1-9][0-9]*',
                			'token' => '[a-f0-9]{32}'
                		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Link\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-links-category' 
                    ),
                ) 
            ),  
            'admin-link_ajax_delete_link_image' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin-link/delete-image',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Link\Controller',
                        'controller' => 'Ajax',
                        'action' => 'delete-image'
            
                    ),
                ),
            ),
        ),
    ),  
    'view_manager' => array(
        'template_path_stack' => array(
        		__DIR__ . '/../src/Link/View',
        ),
    ), 
);
