<?php
return array(
    'router' => array(
        'routes' => array( 
            'faq' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/faq[/:action][/id/:id][/token/:token]',
                    'constraints' => array(
                    		'id' => '[1-9][0-9]*',
                            'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
                    		'token' => '[a-f0-9]{32}'
                    ),
                		'defaults' => array(
                				'__NAMESPACE__' => 'Faq\Controller',
                				'controller'    => 'Index',
                				'action'        => 'index',
                		),                		
                ),
            ),
            'admin-faq' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-faq[/:action][/:id][/token/:token]', 
                        'constraints' => array(
                                'id' => '[1-9][0-9]*',
                                'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
                        		'token' => '[a-f0-9]{32}'
                        ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Faq\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list' 
                    ),
                ) 
            ),   
        ),
    ),  
   /*  'controllers' => array (
    		'invokables' => array (
    				'Faq\Controller\Index' => 'Faq\Controller\IndexController',
    				'Faq\Controller\Admin' => 'Faq\Controller\AdminController'
    		)
    ), */
   
);