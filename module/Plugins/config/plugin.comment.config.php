<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array( 
            'comment-home' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '[/:lang]/plugins/comment[/:controller][/:action]', 
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Comment\Controller',
                        'controller'    => 'Index',
                        'action'        => 'render' 
                    ),
                ) 
            ),  
            'comment-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-comment[/:action][/:id][/token/:token]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'  => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Comment\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-comments'
                    ),
                )
            ),
            'add-comment-ajax' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/add-comment-ajax',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'  => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Comment\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'add-comment'
                    ),
                )
            ),
            'ckeck-user-existance-ajax' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/ckeck-user-existance-ajax',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'  => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Comment\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'ckeck-user-existance'
                    ),
                )
            ),
            'add-comment-dislike-like-ajax' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/add-comment-dislike-like-ajax',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'  => '[1-9][0-9]*',
                        'token' => '[a-f0-9]{32}'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Comment\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'add-comment-dislike-like'
                    ),
                )
            ),
        ),
    ),  
   
);
