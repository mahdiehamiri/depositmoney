<?php
$folders = scandir( __DIR__ . '/../src/');
if ($folders) {
    foreach ($folders as $folder) {
        if ($folder == '.' || $folder == '..') {
            continue;
        }
        $templates['view_manager']['template_path_stack'][] =   __DIR__ . '/../src/' . $folder . '/View';
    }
}
return $templates;

