<?php
return array(
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /plugins/:plugin/:controller/:action
            'plugins-like-button' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/like-button[/:controller][/:action][/:id]',
                	'constraints' => array( 
                		'id'        	=> '[0-9]+' 
                	),
                    'defaults' => array(
                        '__NAMESPACE__' => 'LikeButton\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
        ),
    ),    
    
);
