<?php
return array(
    'router' => array(
        'routes' => array( 
            'add-to-favorite-home' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/plugins/add-to-favorite[/:controller][/:action][/:id]', 
                	'constraints' => array(
               				'id' => '[1-9][0-9]*'
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AddToFavorite\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            'add-to-favorite-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-favorite[/:action][/:id][/token/:token]',
                    'constraints'  => array(
                        'lang' => '[a-zA-Z]{2}',
                        'id'    => '[1-9][0-9]*',
                        'token' => '.*'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AddToFavorite\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-favorites'
                    ),
                )
            ),
            
            'shop-user-ajax-update-favorite' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/shop-user-ajax-update-favorite/update-favorite',
                    'defaults' => array(
                        '__NAMESPACE__' => 'AddToFavorite\Controller',
                        'controller' => 'Ajax',
                        'action' => 'update-favorite'
            
                    ),
                ),
            ),
        ),
    ),  
);
