<?php
return array(
    'router' => array(
        'routes' => array( 
            'banner-home' => array(
                'type'    => 'Segment', 
                'options' => array(
                    'route'    => '/plugins/banner[/:controller][/:action][/:id]', 
                	'constraints' => array(
               				'id' => '[1-9][0-9]*'
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Banner\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            'banner-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-banner[/:action][/:id][/token/:token]', 
                		'constraints' => array(
                		    'lang' => '[a-zA-Z]{2}',
                			'id'  => '[1-9][0-9]*',
                			'token' => '[a-f0-9]{32}'
                		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Banner\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'index' 
                    ),
                ) 
            ),   
        ),
    ),  
    'view_manager' => array(
        'template_path_stack' => array(
        		__DIR__ . '/../src/Banner/View',
        ),
    ), 
);
