<?php
return array(
    'router' => array(
        'routes' => array( 
            'slideshow-home' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/plugins/slideshow[/:controller][/:action][/:id]', 
                	'constraints' => array(
               				'id' => '[1-9][0-9]*'
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Slideshow\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            'slideshow-admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/admin-slideshow[/:action][/:id][/token/:token]', 
                		'constraints' => array(
                		    'lang' => '[a-zA-Z]{2}',
            				'id' => '[1-9][0-9]*',
                		    'token' => '[a-f0-9]{32}'
                		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Slideshow\Controller',
                        'controller'    => 'Admin',
                        'action'        => 'list-slideshows' 
                    ),
                ) 
            ),     
            'slideshow-delete-image' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin-slideshow/delete-image', 
                		'constraints' => array(
                				
                		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Slideshow\Controller',
                        'controller'    => 'Ajax',
                        'action'        => 'delete-image' 
                    ),
                ) 
            ),     
        ),
    ),  
);

