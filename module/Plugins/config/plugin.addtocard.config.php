<?php
return array(
    'router' => array(
        'routes' => array( 
            'add-to-card-home' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/plugins/add-to-card[/:controller][/:action][/:id]', 
                	'constraints' => array(
               				'id' => '[1-9][0-9]*'
               		),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AddToCard\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index' 
                    ),
                ) 
            ),  
            
            'shop-user-ajax-update-card' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/shop-user-ajax-update-card',
                    'defaults' => array(
                        '__NAMESPACE__' => 'AddToCard\Controller',
                        'controller' => 'Ajax',
                        'action' => 'update-card'
            
                    ),
                ),
            ),
            'shop-get-basket-details' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/shop-get-basket-details',
                    'defaults' => array(
                        '__NAMESPACE__' => 'AddToCard\Controller',
                        'controller' => 'Ajax',
                        'action' => 'get-basket-details'
            
                    ),
                ),
            ),
            
        ),
    ),  
);
