<?php
namespace Banner\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Banner\Form\GalleryForm;
use Zend\View\Model\Zend\View\Model;
use Banner\Form\BannerForm;
use Banner\Form\AddPositionForm;
use Banner\Model\BannerModel;
use Application\Helper\BaseAdminController;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Block\Form\BlockForm;
use Block\Model\BlockTable;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Language\Model\LanguageLanguagesTable;
use Block\Form\AddLayoutForm;
use Banner\Model\BannerPositionModel;

class AdminController extends BaseAdminController
{

    public function positionsListAction()
    {
        $this->setHeadTitle(__('Positions List'));
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $bannerPositionModel = new BannerPositionModel($this->getServiceLocator());
        $galleryImages = $bannerPositionModel->fetchAllPositions();
        $grid->setTitle(__('Positions List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($galleryImages, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Position Name'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink('/admin-banner/edit-position/' . $rowId);
        $viewAction->setTooltipTitle(__('Edit'));
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-remove');
        $viewAction3->setLink('/admin-banner/delete-position/' . $rowId . '/token/' . $this->generateToken());
        $viewAction3->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction3);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $block[] = '<a href="/admin-banner/add-position" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $block[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($block);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addPositionAction()
    {
        $form = new AddPositionForm();
        $view['form'] = $form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $validData = $form->getData();
                $bannerPositionModel = new BannerPositionModel($this->getServiceLocator());
                $isAdded = $bannerPositionModel->add($validData);
                if ($isAdded) {
                    return $this->redirect()->toRoute('banner-admin', array(
                        'controller' => 'admin',
                        'action' => 'positions-list'
                    ));
                } else {
                    die('Error adding position');
                }
            }
        }
        return new ViewModel($view);
    }

    public function editPositionAction()
    {
        $positionId = $this->params('id', - 1);
        $bannerPositionModel = new BannerPositionModel($this->getServiceLocator());
        $form = new AddPositionForm();
        $positionById = $bannerPositionModel->fetchPositionById($positionId);
        $form->populateValues($positionById['0']);
        $view['form'] = $form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $validData = $form->getData();
                $isUpdated = $bannerPositionModel->update($validData, $positionId);
                if ($isUpdated) {
                    return $this->redirect()->toRoute('banner-admin', array(
                        'controller' => 'admin',
                        'action' => 'positions-list'
                    ));
                } else {
                    die('Error updating position');
                }
            }
        }
        return new ViewModel($view);
    }

    public function deletePositionAction()
    {
        $positionId = $this->params('id', - 1);
        $token = $this->params('token');
        if ($this->checkToken($token)) {
            $bannerPositionModel = new BannerPositionModel($this->getServiceLocator());
            
            $isDeleted = $bannerPositionModel->delete($positionId);
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__('Banner Position Deleted Successfully.'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(__('Deletion Failed'));
        }
        return $this->redirect()->toRoute('banner-admin', array(
            'controller' => 'admin',
            'action' => 'positions-list'
        ));
    }
}
