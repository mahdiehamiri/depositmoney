<?php

namespace Banner\Controller;

use Application\Helper\BaseAdminController;
use Banner\Model\BannerModel;

class AjaxController extends BaseAdminController
{
	
	public function changeStatusAction()
	{
	    $this->layout("layout/empty.phtml");
		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			
			$bannerrId = $data['id'];
			$status = $data['status'];
			$bannerModel = new BannerModel($this->getServiceLocator());
			 if ($bannerModel->updateStaus($bannerrId,!$status)) {
				$this->flashMessenger()->addMessage('وضعیت با موفقیت تغییر یافت.');
				die('true');
			} else {
				die('false');
			} 
		}
	}
}

