<?php
namespace Banner\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Banner\Model\BannerPositionModel;
use Application\Helper\BasePluginController;

class IndexController extends BasePluginController
{

    public function onDispatch(MvcEvent $e)
    {
        parent::onDispatch($e);
    }

    public function indexAction($params)
    {
        $viewModel = new ViewModel();
        $bannerPositionModel = new BannerPositionModel($this->getServiceLocator());
        $banner = $bannerPositionModel->fetchBannerByPositionName($params);
        $viewModel->setVariable('banner', $banner[0]['script']);
        $viewModel->setTemplate('banner/index/index.phtml');
        return $viewModel;
    }
}
