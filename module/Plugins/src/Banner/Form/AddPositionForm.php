<?php
namespace Banner\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class AddPositionForm extends Form
{

    public function __construct()
    {
        parent::__construct('bannerForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'novalidate'=> true
        ));
        
        $name = new Element\Text('name');
        $name->setAttributes(array(
            'id' => 'banner-position-name',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $name->setLabel(__('Position Name'));
        
        $script = new Element\Textarea('script');        
        $script->setAttributes(array(
            'id' => 'banner-position-script',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $script->setLabel(__('Position Script'));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(_('Save and Close'));
        $submit->setAttributes(array(
            'id' => 'add-banner-position-button',
            'class' => 'btn btn-primary'
        ));
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(_('Save and New'));
        $submit2->setAttributes(array(
            'id' => 'add-banner-position-button',
            'class' => 'btn btn-primary'
        ));
        
        $this->add($name)
            ->add($script)
            ->add($csrf)
            ->add($submit2)
            ->add($submit);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true
        )));
        $inputFilter->add($factory->createInput(array(
            'name' => 'script',
            'required' => true
        )));
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
