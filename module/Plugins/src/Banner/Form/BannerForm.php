<?php 

namespace Banner\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Adapter;

class BannerForm extends Form
{
    protected $adapter;
    
	public function __construct(AdapterInterface $dbAdapter, $editMode = null)
	{
	    $this->adapter =$dbAdapter;
		parent::__construct('bannerForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$owner = new Element\Text('owner');
		$owner->setAttributes(array(
				'id'    => 'banner-catpion',
				'class' => 'form-control',
		));
		$owner->setLabel('مالک بنر');
		
		$link = new Element\Text('link');
		$link->setAttributes(array(
		    'id'    => 'banner-link',
		    'class' => 'form-control',
		));
		$link->setLabel('لینک تصویر');
		
		$startDate = new Element\Date('start_time');
		$startDate->setAttributes(array(
		    'id'    => 'banner-exp',
		    'class' => 'form-control',
		));
		$startDate->setLabel('تاریخ شروع نمایش بنر');
		
		$expDate = new Element\Date('exp_time');
		$expDate->setAttributes(array(
		    'id'    => 'banner-exp',
		    'class' => 'form-control',
		    'format' => 'Y-m-d',
		));
		
		$expDate->setLabel('تاریخ انقضای بنر');
		
		$position = new Element\Select('pos_id');
		$position->setValueOptions($this->getOptionsForSelect());
		$position->setAttributes(array(
		    'id'    => 'banner-pos',
		    'class' => 'form-control',
		));
		$position->setLabel('محل قرارگیری بنر');

		$image = new Element\File('image');
		$image->setAttributes(array(
				'id'    => 'fileUpload1'
		));
		$image->setLabel('تصویر');
		
		
		$csrf = new Element\Csrf('bacsrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(_('Save and Close'));
		$submit->setAttributes(array(
				'id'    => 'update-profile',
				'class' => 'btn btn-primary'
		));
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(_('Save and New'));
		$submit2->setAttributes(array(
				'id'    => 'update-profile',
				'class' => 'btn btn-primary'
		));
		
		$this->add($owner)
		      ->add($link)
		      ->add($startDate)
		      ->add($expDate)
		      ->add($position)
			 ->add($image)
			 ->add($csrf)
			 ->add($submit2)
			 ->add($submit);
		$this->inputFilter($editMode);
	}
	
	public function inputFilter($editMode)
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'owner',
				'required' => true,
		)));
		if (! $editMode) {
			$inputFilter->add($factory->createInput(array(
					'name'     => 'image',
					'required' => true,
			)));
			$inputFilter->add($factory->createInput(array(
					'name'     => 'exp_time',
					'required' => false,
			)));
		}
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
	
	public function getOptionsForSelect()
	{
	    $dbAdapter = $this->adapter;
	    $sql       = 'SELECT *  FROM banner_position';
	    $statement = $dbAdapter->query($sql);
	    $result    = $statement->execute();
	
	    $selectData = array();
	
	    foreach ($result as $res) {
	        $selectData[$res['id']] = $res['name'] .'ـــــــــــــــــ '.$res['size'].' ـــــــــــــــــ'.$res['page'];
	    }
	    return $selectData;
	}
}
