<?php
namespace Banner\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;
use Zend\Db\Sql\Where;

class BannerPositionModel extends BaseModel
{

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('banner_position', $this->getDbAdapter());
    }

    public function fetchAllPositions()
    {
        $select = new Select('banner_position');
        return $select;
    }
    
    public function fetchBannerByPositionName($position)
    {
        $select = new Select('banner_position');
        $select->where->equalTo('name', $position);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fetchPositionById($id)
    {
        if ($id > 0 && is_numeric($id)) {
            $select = new Select('banner_position');
            $select->where->equalTo('id', $id);
            return $this->tableGateway->selectWith($select)->toArray();
        }
    }

    public function add($data)
    {
        return $this->tableGateway->insert(array(
            'name' => $data['name'],
            'script' => $data['script']
        ));
    }

    public function update($data, $id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->update(array(
                'name' => $data['name'],
                'script' => $data['script']
            ), array(
                'id' => $id
            ));
        }
    }

    public function delete($id)
    {
        if ($id > 0 && is_numeric($id)) {
            return $this->tableGateway->delete(array(
                'id' => $id
            ));
        }
    }
}
