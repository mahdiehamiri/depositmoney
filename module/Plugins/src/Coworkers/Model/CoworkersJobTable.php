<?php 
namespace Coworkers\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class CoworkersJobTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('coworker_job', $this->getDbAdapter());
    }
 
	
	public function getRecordsCoworkersJob($id = false)
	{
	    $select = new Select('coworker_job');
	    return $select;
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getRecordsCoworkersJobById($id)
	{
	    $select = new Select('coworker_job'); 
	    $select->where->equalTo('id', $id);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getRecordsCoworkersJobData($where)
	{
	    $select = new Select('coworker_job'); 
	    $select->where($where);
	    return $this->tableGateway->selectWith($select)->toArray();
	}

	public function addCoworkersJob($data)
	{
	    return $this->tableGateway->insert(array(
	       'name' =>   $data['name'],
	        'from_date' => $data['from_date'],
	        'to_date' =>  $data['to_date'] ,
	        'lang' =>  $data['lang'] , 
	        'description' =>  $data['description'] ,
	        'id_coworker' =>  $data['id_coworker']
	    ));
	     
	}
	public function deleteCoworkerJob($id)
	{
	    return $this->tableGateway->delete(array(
	        'id' => $id
	    ));
	}
 
}
