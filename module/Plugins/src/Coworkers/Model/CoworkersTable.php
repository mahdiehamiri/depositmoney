<?php
namespace Coworkers\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class CoworkersTable extends BaseModel
{

    protected $tableGateway;

    protected $adapter;

    protected $serviceManager;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('coworkers', $this->getDbAdapter());
    }

    public function getRecordsCoworkerss($id = false)
    {
        return $this->tableGateway->select()->toArray();
    }

    public function getRecordsCoworkers($id = false)
    {
        $select = new Select('coworkers');
        return $select;
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getRecordsCoworkersById($id = false)
    {
        $select = new Select('coworkers');
        $select->where->equalTo('id', $id);
        return $this->tableGateway->selectWith($select)->current();
    }

    public function addCoworkers($data)
    {
        return $this->tableGateway->insert(array(
            'name' => $data['name'],
            'image' => $data['image'],
            'coworker_position' => $data['coworker_position'],
            'status' => ($data['status'] ? $data['status'] : 1),
            'link_social_network' => $data['link_social_network'],
            'description' => $data['description']
        ));
    }
    public function updateCoworkers($data, $id)
    {
       return $this->tableGateway->update(array(
           'name' => $data['name'],
            'image' => $data['image'],
            'coworker_position' => $data['coworker_position'],
            'status' => ($data['status'] ? $data['status'] : 1),
            'link_social_network' => $data['link_social_network'],
            'description' => $data['description']
        ), array(
            'id' => $id
        ));
    }

    public function getAllCoworkers2()
    {
        $select = new Select('coworkers');
        return $select;
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getAllCoworkers($lang = 'fa', $position = false)
    {
        $select = new Select('coworkers');
        
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getCoworkersById($coworkersId, $lang = false, $position = false)
    {
        $select = new Select('coworkers');
        $select->columns(array(
            'coworkerName'=> 'name',
            '*'
        ));
        $select->where->equalTo('coworkers.id', $coworkersId);
        $select->join('coworker_job', 'coworker_job.id_coworker = coworkers.id', array(
            'jobId'=> 'id',
            'job'=> 'name',
            'from_date',
            'to_date',
            'descriptionjob' => 'description' 
        ), 'left');
        
        $select->join('coworker_education', 'coworker_education.id_coworker = coworkers.id', array(
            'educationId'=> 'id',
            'education'=> 'name',
            'from_date',
            'institutions',
            'to_date',
            'descriptionEducation' => 'description'
        ), 'left');
        
        if ($position) {
            $select->where->equalTo('view_home_page', $position);
        }
        if ($lang) {
            $select->where->equalTo('coworker_education.lang', $lang);
            $select->where->equalTo('coworker_job.lang', $lang);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function deleteCoworker($id)
    {
        return $this->tableGateway->delete(array(
            'id' => $id
        ));
    }
}
