<?php 
namespace Coworkers\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class CoworkersEducationTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('coworker_education', $this->getDbAdapter());
    }
	public function getRecordsCoworkersEducation($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
 
	public function getRecordsCoworkersEducationById($where)
	{
	    $select = new Select('coworker_education');
	    $select->where($where); 
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addCoworkersEducation($data)
	{
	    return $this->tableGateway->insert(array( 
	        'name' =>   $data['name'],
	        'from_date' => $data['from_date'],
	        'to_date' =>  $data['to_date'] ,
	        'lang' =>  $data['lang'] ,
	        'description' =>  $data['description'] ,
	        'institutions' =>  $data['institutions'] ,
	        'id_coworker' =>  $data['id_coworker']
	    ));
	}
	public function deleteCoworkerEducation($id)
	{
	    return $this->tableGateway->delete(array(
	        'id' => $id
	    ));
	}
	
	
}
