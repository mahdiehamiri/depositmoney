<?php
namespace Coworkers\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class CoworkersForm extends Form
{

    public function __construct($allActiveLanguages, $samplesCategoryTableData = array())
    {
        parent::__construct('coworkersForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'id' => 'coworkersForm',
            'novalidate' => true
        ));
        
        $name = new Element\Text('name');
        $name->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control validate[required]',
            'placeholder' => __('Title'),
            'required' => 'required'
        ));
        $name->setLabel(__('Title'));
        
        $position = new Element\Text('coworker_position');
        $position->setAttributes(array(
            'id' => 'order',
            'class' => 'form-control ',
            'placeholder' => __('Post')
        ));
        $position->setLabel(__('Post'));
        
        $description = new Element\Textarea('description');
        $description->setAttributes(array(
            'id' => 'order',
            'class' => 'form-control ',
            'placeholder' => __('Description')
        ));
        $description->setLabel(__('Description'));
        
        $status = new Element\Checkbox('status');
        $status->setAttributes(array(
            'id' => 'order',
            'placeholder' => __('Status')
        ));
        $status->setLabel(__('Status'));
        
        $linkSocialNetwork = new Element\Text('link_social_network');
        $linkSocialNetwork->setAttributes(array(
            'class' => 'form-control validate[custom[url]]',
            'value' => __('http://example.com')
        ));
        $linkSocialNetwork->setLabel(__('Social Network Link'));
        
        $lang = new Element\Select('lang');
        $lang->setAttributes(array(
            'id' => 'lang',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $languages = array();
        if ($allActiveLanguages) {
            foreach ($allActiveLanguages as $language) {
                $languages[$language['code']] = $language['title'];
            }
        }
        $lang->setValueOptions($languages);
        $lang->setLabel(__('Language'));
        
        $image = new Element\File('image');
        $image->setAttributes(array(
            'id' => 'fileUpload1'
        ));
        $image->setLabel(__("Choose image"));
        
        $csrf = new Element\Csrf('cpworkerscsrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__('Save and close'));
        $submit->setAttributes(array(
            'id' => 'submitLink',
            'class' => 'btn btn-primary fa fa-upload'
        )); 
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__('Save and new'));
        $submit2->setAttributes(array(
            'id' => 'submitLink',
            'class' => 'btn btn-primary'
        )); 
        $this->add($name)
            ->add($lang)
            ->add($status)
            ->add($position)
            ->add($linkSocialNetwork)
            ->add($image)
            ->add($description)
            ->add($csrf)
            ->add($submit2)
            ->add($submit);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
