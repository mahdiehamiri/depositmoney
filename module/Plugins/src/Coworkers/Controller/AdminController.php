<?php
namespace Coworkers\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Sample\Model\SampleTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Language\Model\LanguageLanguagesTable;
use Coworkers\Model\CoworkersCategoryTable;
use Coworkers\Model\CoworkersTable;
use Coworkers\Form\CoworkersForm;
use Coworkers\Model\CoworkersEducationTable;
use Coworkers\Model\CoworkersJobTable;
use Zend\Db\Sql\Where;

class AdminController extends BaseAdminController
{

    public function listCoworkersAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        $id = $this->params('id');
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $coworkersTable = new CoworkersTable($this->getServiceLocator());
        $coworkers = $coworkersTable->getRecordsCoworkers($id);
        $grid->setTitle(__('Coworkers list'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($coworkers, $dbAdapter);
        
        $col = new Column\Select('id', "coworkers");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Name'));
        $grid->addColumn($col);
        
        $col = new Column\Select('coworker_position');
        $col->setLabel(__('Coworker Post'));
        $grid->addColumn($col);
        
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-coworkers/edit-coworkers/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-coworkers/delete-coworkers/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $sampleAdd[] = '<a href=' . "/" . $this->lang . '/admin-coworkers/add-coworkers class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $sampleAdd[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($sampleAdd);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addCoworkersAction()
    {
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/Coworkers/";
        $validationExt = "jpg,jpeg,png,gif";
        $education = $config['education'];
        
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $coworkersEducationTable = new CoworkersEducationTable($this->getServiceLocator());
        $coworkersJobTable = new CoworkersJobTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        
        $coworkersForm = new CoworkersForm($allActiveLanguages);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            
            $files = $request->getFiles()->toArray();
            if (! empty($files['image']['name'])) {
                $data = array_merge($data, $files);
            }
            $coworkersForm->setData($data);
            if ($coworkersForm->isValid()) {
                $validData = $coworkersForm->getData();
                $toSaveItems = array();
                if (! empty($files)) {
                    foreach ($files as $key => $datan) {
                        if (! $validData[$key]['error']) {
                            $isUploaded[$key] = $this->addImage($validData[$key], $uploadDir);
                            if (! $isUploaded[$key][0]) {
                                $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                                die();
                                return $this->redirect()->toUrl("/admin-Coworkers/add-coworkers");
                            }
                        }
                    }
                    $item = array(
                        'name' => $validData['name'],
                        'image' => $isUploaded[$key][1],
                        'coworker_position' => $validData['coworker_position'],
                        'status' => ($validData['status'] ? $validData['status'] : 1),
                        'link_social_network' => $validData['link_social_network'],
                        'description' => $validData['description']
                    );
                } else {
                    $item = array(
                        'name' => $validData['name'],
                        'image' => '',
                        'coworker_position' => $validData['coworker_position'],
                        'status' => ($validData['status'] ? $validData['status'] : 1),
                        'link_social_network' => $validData['link_social_network'],
                        'description' => $validData['description']
                    );
                }
                $coworkersTable = new CoworkersTable($this->getServiceLocator());
                try {
                    $isAdded = $coworkersTable->addCoworkers($item);
                    if ($isAdded) {
                        if (isset($data['job']) && $data['job']) {
                            foreach ($data['job'] as $jobs) {
                                $jArray = array(
                                    'name' => $jobs['name'],
                                    'from_date' => $jobs['from_date'],
                                    'to_date' => $jobs['to_date'],
                                    'description' => $jobs['description'],
                                    'lang' => $data['lang'],
                                    'id_coworker' => $isAdded
                                );
                                $coworkersJobTable->addCoworkersJob($jArray);
                            }
                        }
                        if (isset($data['education']) && $data['education']) {
                            foreach ($data['education'] as $educationData) {
                                $eArray = array(
                                    'name' => $educationData['name'],
                                    'from_date' => $educationData['from_date'],
                                    'to_date' => $educationData['to_date'],
                                    'institutions' => $educationData['institutions'],
                                    'lang' => $data['lang'],
                                    'description' => $educationData['description'],
                                    'id_coworker' => $isAdded
                                );
                                $coworkersEducationTable->addCoworkersEducation($eArray);
                            }
                        }
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toUrl("/admin-coworkers/list-coworkers");
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                }
            } else {
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($coworkersForm, true);
            }
        }
        $view['coworkersForm'] = $coworkersForm;
        // $view['cat_id'] = $id;
        $view['education'] = $education;
        return new ViewModel($view);
    }

    public function editCoworkersAction()
    {
        $id = $this->params('id');
        $config = $this->serviceLocator->get('Config');
        $education = $config['education'];
        $uploadDir = $config['base_route'] . "/uploads/Coworkers/";
        $validationExt = "jpg,jpeg,png,gif";
        $lang = $this->lang;
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $coworkersEducationTable = new CoworkersEducationTable($this->getServiceLocator());
        $coworkersJobTable = new CoworkersJobTable($this->getServiceLocator());
        $where = new Where();
        $where->equalTo('id_coworker', $id);
        $where->equalTo('lang', $lang);
        $coworkersEducationTableData = $coworkersEducationTable->getRecordsCoworkersEducationById($where);
        
        $coworkersJobTableData = $coworkersJobTable->getRecordsCoworkersJobData($where);
        
        $coworkersTable = new CoworkersTable($this->getServiceLocator());
        $coworkersTableData = $coworkersTable->getRecordsCoworkersById($id);
        
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        
        $coworkersForm = new CoworkersForm($allActiveLanguages);
        $coworkersForm->populateValues($coworkersTableData);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            if (! empty($files['image']['name'])) {
                $data = array_merge($data, $files);
            }
            
            $coworkersForm->setData($data);
            if ($coworkersForm->isValid()) {
                $validData = $coworkersForm->getData();
                $validData['education'] = (isset($data['education']) ? $data['education'] : '');
                $validData['job'] =(isset($data['job']) ? $data['job'] : '');
                
                $toSaveItems = array();
                if (! empty($files)) {
                    foreach ($files as $key => $datan) {
                        if (! $validData[$key]['error']) {
                            $isUploaded[$key] = $this->addImage($validData[$key], $uploadDir);
                            if (! $isUploaded[$key][0]) {
                                $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                                
                                return $this->redirect()->toUrl("/admin-Coworkers/edit-coworkers");
                            }
                        }
                    }
                    $item = array(
                        'name' => $validData['name'],
                        'image' => $isUploaded[$key][1],
                        'coworker_position' => $validData['coworker_position'],
                        'status' => ($validData['status'] ? $validData['status'] : 1),
                        'link_social_network' => $validData['link_social_network'],
                        'description' => $validData['description']
                    );
                } else {
                    $item = array(
                        'name' => $validData['name'],
                        'image' => $coworkersTableData['image'],
                        'coworker_position' => $validData['coworker_position'],
                        'status' => ($validData['status'] ? $validData['status'] : 1),
                        'link_social_network' => $validData['link_social_network'],
                        'description' => $validData['description']
                    );
                }
                $coworkersTable = new CoworkersTable($this->getServiceLocator());
                try {
                    $isUpdated = $coworkersTable->updateCoworkers($item, $id);
                    if ($coworkersEducationTableData) {
                        foreach ($coworkersEducationTableData as $data) {
                            $coworkersJobTable->deleteCoworkerJob($data['id']);
                        }
                    }
                    
                    if ($isUpdated !== false) {
                        @unlink($uploadDir, $coworkersTableData['image']);
                        if (isset($data['job']) && $data['job']) {
                            foreach ($data['job'] as $jobs) {
                                $jArray = array(
                                    'name' => $jobs['name'],
                                    'from_date' => $jobs['from_date'],
                                    'to_date' => $jobs['to_date'],
                                    'description' => $jobs['description'],
                                    'lang' => $data['lang'],
                                    'id_coworker' => $id
                                );
                                $coworkersJobTable->addCoworkersJob($jArray);
                            }
                          
                        }
                        if (isset($data['education']) && $data['education']) {
                            foreach ($data['education'] as $educationData) {
                                $eArray = array(
                                    'name' => $educationData['name'],
                                    'from_date' => $educationData['from_date'],
                                    'to_date' => $educationData['to_date'],
                                    'institutions' => $educationData['institutions'],
                                    'lang' => $data['lang'],
                                    'description' => $educationData['description'],
                                    'id_coworker' => $id
                                );
                                $coworkersEducationTable->addCoworkersEducation($eArray);
                            }
                            if ($coworkersEducationTableData) {
                                foreach ($coworkersEducationTableData as $data) {
                                    $coworkersEducationTable->deleteCoworkerEducation($data['id']);
                                }
                            }
                        }
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toUrl("/admin-coworkers/list-coworkers");
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                }
            } else {
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($coworkersForm, true);
            }
        }
        
        $view['coworkersForm'] = $coworkersForm;
        $view['education'] = $education;
        $view['educationData'] = $coworkersEducationTableData;
        $view['imageData'] = $coworkersTableData['image'];
        $view['jobData'] = $coworkersJobTableData;
        $view['jobDataCount'] = Count($coworkersJobTableData);
        $view['educationDataCount'] = Count($coworkersEducationTableData);
        return new ViewModel($view);
    }

    public function deleteCoworkersAction()
    {
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/Coworkers/";
        $config = $this->serviceLocator->get('Config');
        $container = new Container('token');
        $token = $this->params('token');
        $coworkerId = $this->params('id');
        $coworkersTable = new CoworkersTable($this->getServiceLocator());
        $coworkersById = $coworkersTable->getCoworkersById($coworkerId); 
        if ($coworkersById && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = $coworkersTable->deleteCoworker($coworkerId);
            if ($isDeleted) { 
                @unlink($uploadDir.$coworkersById[0]['image']);
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->layout()->errorMessage = __("Operation failed!");
            }
        }
        // return $this->redirect()->toRoute('link-admin');
        return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
    }

    public function addImage($validData, $uploadDir)
    {
        $newFileName = md5(time() . $validData['name']);
        $validationExt = "jpg,jpeg,png,gif";
        $isUploaded = $this->imageUploader()->UploadFile($validData, $validationExt, $uploadDir, $newFileName);
        return $isUploaded;
    }
}
