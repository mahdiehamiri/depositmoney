<?php
namespace Coworkers\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Link\Form\LinkForm;
use Link\Model\LinkTable;
use Link\Model\LinkCategoryTable;
use Application\Helper\BasePluginController;
use Coworkers\Model\CoworkersTable;

class IndexController extends BasePluginController
{

    protected $linkTable;

    public function onDispatch(MvcEvent $e)
    {
        parent::onDispatch($e);
    }

    public function indexAction($params)
    {
        $lang = $this->lang;
        $position = 0;
        if ($params) {
            if (isset($params['position'])) {
                $position = 1;
            }
        }
        $viewModel = new ViewModel();
        $coworkersTable = new CoworkersTable($this->getServiceLocator());
        $coworkersTableData = $coworkersTable->getAllcoworkers($lang, $position);
       //  $dataArrayCoworkers = array();
//         if ($coworkersTableData) {
//             foreach ($coworkersTableData as $data) {
//                 $dataArrayCoworkers[$data['catId']][] = $data;
//             }
//         }
        $viewModel->setVariable('coworkers',   $coworkersTableData);
        $viewModel->setTemplate('coworkers/index/index.phtml');
        return $viewModel;
    }

    public function coworkerAction($params)
    {
     
        $lang = $this->lang;
        $viewModel = new ViewModel();
       if (isset($params['id']) && $params['id']) {
             $config = $this->serviceLocator->get('Config');
             $education = $config['education'];
             $uploadDir = $config['base_route'] . "/uploads/coworkers/"; 
            
            $coworker = array();
            $coworkersTable = new CoworkersTable($this->getServiceLocator());
            $coworkersTableData = $coworkersTable->getCoworkersById($params['id'], $lang);
          
             if ($coworkersTableData) {
                 foreach($coworkersTableData as $dataCoworker) {
                     $coworker['coworkerName'] = $dataCoworker['coworkerName'];
                     $coworker['image'] = $dataCoworker['image'];
                     $coworker['coworker_position'] = $dataCoworker['coworker_position'];
                     $coworker['link_social_network'] = $dataCoworker['link_social_network'];
                     $coworker['description'] = $dataCoworker['description'];
                     $coworker['education'][$dataCoworker['educationId']]['name'] = $dataCoworker['education'];
                     $coworker['education'][$dataCoworker['educationId']]['from_date'] = $dataCoworker['from_date'];
                     $coworker['education'][$dataCoworker['educationId']]['to_date'] = $dataCoworker['to_date'];
                     $coworker['education'][$dataCoworker['educationId']]['institutions'] = $dataCoworker['institutions'];
                     $coworker['education'][$dataCoworker['educationId']]['description'] = $dataCoworker['descriptionEducation'];
                
                     $coworker['job'][$dataCoworker['jobId']]['name'] = $dataCoworker['job'];
                     $coworker['job'][$dataCoworker['jobId']]['from_date'] = $dataCoworker['from_date'];
                     $coworker['job'][$dataCoworker['jobId']]['to_date'] = $dataCoworker['to_date'];
                     $coworker['job'][$dataCoworker['jobId']]['description'] = $dataCoworker['descriptionjob'];
              
                      
                 }
                 $viewModel->setVariable('education', $education);
                 $viewModel->setVariable('pathUpload', $uploadDir);
                 $viewModel->setVariable('resume', $coworker);
             }
         }
       
       $viewModel->setTemplate('coworkers/index/coworker.phtml');
         return $viewModel;
    }
}
