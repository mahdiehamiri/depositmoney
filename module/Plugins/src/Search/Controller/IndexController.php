<?php

namespace Search\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Comment\Form\CommentForm;
use Comment\Model\CommentTable;
use Application\Helper\BasePluginController;
use Application\Helper\MailService;
use Application\Helper\BaseAdminController;
use Application\Helper\GoogleSearch;
use Application\Helper\Google_Search;
use Application\Helper\JSONStrategy;
 
class IndexController extends BaseAdminController 
{ 
	protected $defaultGuestId = 1457; //There is no email set for this dummy user!
	public function onDispatch(MvcEvent $e)
	{
		parent::onDispatch($e);
	}
	

	public function indexAction()
	{
 
		$view ['populars'] = array();
		$viewModel = new ViewModel ( $view );
		return $viewModel->setTemplate ( 'search/index/index.phtml' );
	}
 
}
