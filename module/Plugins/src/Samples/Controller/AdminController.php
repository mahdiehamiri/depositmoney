<?php
namespace Samples\Controller;

use Zend\View\Model\ViewModel;
use Sample\Form\SampleForm;
use Zend\Session\Container;
use Sample\Model\SampleTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Language\Model\LanguageLanguagesTable;
use Samples\Form\SamplesCategoryForm;
use Samples\Model\SamplesCategoryTable;
use Samples\Model\SamplesTable;
use Samples\Form\SamplesForm;

class AdminController extends BaseAdminController
{

    public function listSamplesCategoryAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $sampleCategoryTable = new SamplesCategoryTable($this->getServiceLocator());
        
        $samples = $sampleCategoryTable->getRecordsCategorySample(); 
        $grid->setTitle(__('Samples category list'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($samples, $dbAdapter);
        
        $col = new Column\Select('id', "samples_category");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('catergory_title');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('catergory_lang');
        $col->setLabel(__('Language'));
        $grid->addColumn($col);
        
        $col = new Column\Select('parent_id');
        $col->setLabel(__('Parrent'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-samples/edit-samples-category/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit samples'));
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-remove');
        $viewAction->setLink("/" . $this->lang . '/admin-samples/delete-samples-category/' . $rowId . '/token/' . $csrf);
        $viewAction->setTooltipTitle(__('Delete samples'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $samplesbtn[] = '<a href=' . "/" . $this->lang . '/admin-samples/add-samples-category class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $samplesbtn[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($samplesbtn);
        
        $grid->render();
        return $grid->getResponse();
    }

    public function addSamplesCategoryAction()
    {
        $config = $this->serviceLocator->get('Config');
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        
        $samplesCategoryTable = new SamplesCategoryTable($this->getServiceLocator());
        $samplesCategoryTableData = $samplesCategoryTable->getAllSamplesCategory(array(
            "enable" => "1"
        ));
        
        $categorySampleForm = new SamplesCategoryForm($allActiveLanguages, $samplesCategoryTableData);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $categorySampleForm->setData($data);
            if ($categorySampleForm->isValid()) {
                $validData = $categorySampleForm->getData();
                $toSaveItems = array();
                $item = array(
                    'parent_id' => ($validData['parent_id'] ? $validData['parent_id'] : 0),
                    'catergory_lang' => $validData['catergory_lang'],
                    'catergory_title' => $validData['catergory_title'],
                    'category_status' => $validData['category_status'],
                    'category_order' => $validData['category_order']
                );
                try {
                    $isAdded = $samplesCategoryTable->addSamplesCategory($item);
                    if ($isAdded) {
                       
                        if (isset($_POST['submit'])) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('samples-admin');
                        } else {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('samples-admin', array(
                                'action' => 'add-samples-category'
                            ));
                        }
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                } catch (\Exception $e) {
                    
                    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                }
            } else {
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($categorySampleForm, true);
            }
        }
        $view['categorySampleForm'] = $categorySampleForm;
        return new ViewModel($view);
    }

    public function editSamplesCategoryAction()
    {
        $config = $this->serviceLocator->get('Config');
        
        $samplesId = $this->params('id', - 1);
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        
        $samplesCategoryTable = new SamplesCategoryTable($this->getServiceLocator());
        $samplesCategoryTableData = $samplesCategoryTable->getAllSamplesCategory(array(
            "enable" => "1"
        ));
        $samplesCategoryTableDataById = $samplesCategoryTable->getSamplesCategoryById($samplesId);
        
        $categorySampleForm = new SamplesCategoryForm($allActiveLanguages, $samplesCategoryTableData);
        
        $categorySampleForm->populateValues($samplesCategoryTableDataById[0]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $categorySampleForm->setData($data);
            if ($categorySampleForm->isValid()) {
                $validData = $categorySampleForm->getData();
                $item = array();
                $item = array(
                    'parent_id' => ($validData['parent_id'] ? $validData['parent_id'] : 0),
                    'catergory_lang' => $validData['catergory_lang'],
                    'catergory_title' => $validData['catergory_title'],
                    'category_status' => $validData['category_status'],
                    'category_order' => $validData['category_order']
                );
                if ($samplesId > 0 && is_numeric($samplesId)) {
                    try {
                        $isUpdated = $samplesCategoryTable->editSamplesCategory($item, $samplesId);
                        if ($isUpdated) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('samples-admin');
                        } else {
                            $this->layout()->errorMessage = __("Operation failed!");
                        }
                    } catch (\Exception $e) {
                        $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                    }
                }
            } else {
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($categorySampleForm, true);
            }
        } else {
            // var_dump($galleryForm->getMessages());
        }
        $view['categorySampleForm'] = $categorySampleForm;
        return new ViewModel($view);
    }

    public function deleteSamplesCategoryAction()
    {
        $config = $this->serviceLocator->get('Config');
        $container = new Container('token');
        $token = $this->params('token');
        $sampleId = $this->params('id');
        $sampleTable = new SamplesTable($this->getServiceLocator());
        $sampleById = $sampleTable->getAllSamplesBuyCategoryId($sampleId);
 
        if (! empty($sampleById)) {
            $this->flashMessenger()->addErrorMessage(__("This Sample has child. delete theme before."));
        } else {
            $sampleCategoryTable = new SamplesCategoryTable($this->getServiceLocator());
            
            if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            
                $isDeleted = $sampleCategoryTable->deleteSamplesCategory($sampleId);
                if ($isDeleted) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
                }
            }
        }
 
       return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
    }

    public function listSamplesAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        $id = $this->params('id');
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $sampleTable = new SamplesTable($this->getServiceLocator());
        $sample = $sampleTable->getRecordsSample($id);
        $grid->setTitle(__('Samples list'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($sample, $dbAdapter);
        
        $col = new Column\Select('id', "samples");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('sample_title');
        $col->setLabel(__('Title'));
        
        $col = new Column\Select('subject');
        $col->setLabel(__('Cooperation Subject'));
        
        $col = new Column\Select('sample_title');
        $col->setLabel(__('Title'));
        
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-samples/edit-sample/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-samples/delete-sample/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $sampleAdd[] = '<a href=' . "/" . $this->lang . '/admin-samples/add-sample class="btn btn-primary">' . __("Add") . '</a>';
        $sampleAdd[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($sampleAdd);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addSampleAction()
    {
        $id = $this->params('id');
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/samples/";
        $validationExt = "jpg,jpeg,png,gif";
        $subjects = $config['sample_subject'] ;
    
        $samplesCategoryTable = new SamplesCategoryTable($this->getServiceLocator());
        $samplesCategoryTableData = $samplesCategoryTable->getAllSamplesCategory();
        
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        
        $sampleForm = new SamplesForm($samplesCategoryTableData, $subjects);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            
            if (! empty($files['technology_img']['name'])) {
                $data = array_merge($data, $files);
            }
            
            if (! empty($files['website_view_img']['name'])) {
                $data = array_merge($data, $files);
            }
            if (! empty($files['tablet_view_img']['name'])) {
                $data = array_merge($data, $files);
            }
            if (! empty($files['mobile_view_img']['name'])) {
                $data = array_merge($data, $files);
            }
            if (! empty($files['intro_img']['name'])) {
                $data = array_merge($data, $files);
            }
            
            $sampleForm->setData($data);
            if ($sampleForm->isValid()) {
                $validData = $sampleForm->getData(); 

                $toSaveItems = array();
                if (! empty($files)) {
                 foreach ($files as $key => $data) {
                            if (!$validData[$key]['error']) {
                                $isUploaded[$key] = $this->addImage($validData[$key], $uploadDir);
                                if (!$isUploaded[$key][0] ) { 
                                    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");die;
                                    return $this->redirect()->toUrl("/admin-samples/add-sample");
                                }
                            } 
                         
                        }
                    $item = array(
                          'sample_link' => $validData['sample_link'],
                            'samples_category_id' => $validData['samples_category_id'],
                            'sample_title' => $validData['sample_title'],
                            'sample_status' => ($validData['sample_status'] ? $validData['sample_status'] : 1),
                            'subject' => $validData['subject'],
                            'description' => $validData['description'], 
                            'intro_img' => ($isUploaded['intro_img'][1] ? $isUploaded['intro_img'][1] : ''),
                            'technology_img' => ($isUploaded['technology_img'][1] ? $isUploaded['technology_img'][1] :''),
                            'website_view_img' => ($isUploaded['website_view_img'][1] ? $isUploaded['website_view_img'][1] :''),
                            'tablet_view_img' => ($isUploaded['tablet_view_img'][1] ? $isUploaded['tablet_view_img'][1] : ''),
                            'mobile_view_img' =>  ($isUploaded['mobile_view_img'][1] ? $isUploaded['mobile_view_img'][1] :''),
                            'sample_order' =>  ($validData['sample_order'] ? $validData['sample_order'] : 1),
                            'view_home_page' =>  $validData['view_home_page'] ,
                        );
               
                } else {
                    $item = array(
                        'sample_link' => $validData['sample_link'],
                        'subject' => $validData['subject'],
                        'samples_category_id' => $validData['samples_category_id'],
                        'sample_title' => $validData['sample_title'],
                        'sample_status' => ($validData['sample_status'] ? $validData['sample_status'] : 1),
                        'description' => $validData['description'],
                        'technology_img' => '',
                        'website_view_img' => '',
                        'intro_img' => '',
                        'tablet_view_img' => '',
                        'mobile_view_img' => '',
                        'sample_order' => ($validData['sample_order'] ? $validData['sample_order'] : 1),
                        'view_home_page' =>  $validData['view_home_page'] ,
                    );
                }
                $sampleTable = new SamplesTable($this->getServiceLocator());
                
                try {
             
                    $isAdded = $sampleTable->addSamples($item);
                    if ($isAdded) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toUrl("/admin-samples/list-samples/" . $id);
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                }
            } else {
                
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($sampleForm, true);
            }
        }
        $view['linkForm'] = $sampleForm;
        $view['cat_id'] = $id;
        return new ViewModel($view);
    }

    public function editSampleAction()
    {
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/samples/";
        $validationExt = "jpg,jpeg,png,gif";
        $subjects = $config['sample_subject'] ;
//         var_dump($subjects);die;
        $samplesCategoryTable = new SamplesCategoryTable($this->getServiceLocator());
        $samplesCategoryTableData = $samplesCategoryTable->getAllSamplesCategory();
        
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        
        $sampleForm = new SamplesForm($samplesCategoryTableData, $subjects);
        
        $sampleId = $this->params('id', - 1);
        $sampleTable = new SamplesTable($this->getServiceLocator());
        $sampleData = $sampleTable->getSamplesById($sampleId);
 
        $sampleForm->populateValues($sampleData[0]);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray(); 
   
            if (! empty($files['technology_img']['name'])) {
                $data = array_merge($data, $files);
            } 
            if (! empty($files['website_view_img']['name'])) {
                $data = array_merge($data, $files);
            }
            if (! empty($files['tablet_view_img']['name'])) {
                $data = array_merge($data, $files);
            }
            if (! empty($files['mobile_view_img']['name'])) {
                $data = array_merge($data, $files);
            }
            if (! empty($files['intro_img']['name'])) {
                $data = array_merge($data, $files);
            }
            $message = array();
            $sampleForm->setData($data);
            if ($sampleForm->isValid()) {
                $validData = $sampleForm->getData(); 
 
                $toSaveItems = array();
                try {
                    if (! empty($files)) {
                        $isUploaded = array();
                        foreach ($files as $key => $data) { 
                            if (!$validData[$key]['error']) {
                                $isUploaded[$key] = $this->addImage($validData[$key], $uploadDir); 
                                if ($isUploaded[$key][0]) {
                                  @unlink($uploadDir . $sampleData);
                                }
                            } else { 
                                $isUploaded[$key][1] = $sampleData[0][$key];
                            }
                        } 
                        $items = array(
                            'sample_link' => $validData['sample_link'],
                            'samples_category_id' => $validData['samples_category_id'],
                            'sample_title' => $validData['sample_title'],
                            'sample_status' => $validData['sample_status'],
                            'subject' => $validData['subject'],
                            'description' => $validData['description'], 
                            'intro_img' => ($isUploaded['intro_img'][1] ? $isUploaded['intro_img'][1] : $sampleData[0]['intro_img']),
                            'technology_img' => ($isUploaded['technology_img'][1] ? $isUploaded['technology_img'][1] : $sampleData[0]['technology_img']),
                            'website_view_img' => ($isUploaded['website_view_img'][1] ? $isUploaded['website_view_img'][1] : $sampleData[0]['website_view_img']),
                            'tablet_view_img' => ($isUploaded['tablet_view_img'][1] ? $isUploaded['tablet_view_img'][1] : $sampleData[0]['tablet_view_img']),
                            'mobile_view_img' =>  ($isUploaded['mobile_view_img'][1] ? $isUploaded['mobile_view_img'][1] : $sampleData[0]['mobile_view_img']),
                            'sample_order' =>  ($validData['sample_order'] ? $validData['sample_order'] : $sampleData[0]['sample_order']),
                        );
                       
                    } else {
                        $items = array(
                            'sample_link' => $validData['sample_link'],
                            'samples_category_id' => $validData['samples_category_id'],
                            'sample_title' => $validData['sample_title'],
                            'sample_status' => $validData['sample_status'],
                            'subject' => $validData['subject'],
                            'description' => $validData['description'],
                            'intro_img' => $sampleData[0]['intro_img'],
                            'technology_img' => $sampleData[0]['technology_img'],
                            'website_view_img' => $sampleData[0]['website_view_img'],
                            'tablet_view_img' => $sampleData[0]['tablet_view_img'],
                            'mobile_view_img' => $sampleData[0]['mobile_view_img'],
                            'sample_order' => $sampleData[0]['sample_order']
                        );
                    }
                   
              
                    $sampleTable = new SamplesTable($this->getServiceLocator()); 
                    $isUpdate = $sampleTable->editSamples($items, $sampleId);
                    if ($isUpdate !== false) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toUrl("/admin-samples/list-samples/" . $sampleId);
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                } catch (\Exception $e) {
                   
                    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                }
            } else {
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($sampleForm, true);
            }
        } else {
            $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($sampleForm, true);
        }
        
        $view['sampleData'] = $sampleData[0];
        $view['linkForm'] = $sampleForm;
        return new ViewModel($view);
    }

    public function deleteSampleAction()
    {
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/samples/";
        $config = $this->serviceLocator->get('Config');
        $container = new Container('token');
        $token = $this->params('token');
        $sampleId = $this->params('id');
        $sampleTable = new SamplesTable($this->getServiceLocator());
        $sampleById = $sampleTable->getSamplesById($sampleId);
        if ($sampleById && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = $sampleTable->deleteSample($sampleId);
            if ($isDeleted) {
                if ($sampleById[0]['website_view_img'] != '' && file_exists($uploadDir . $sampleById[0]['website_view_img'])) {
                     unlink($uploadDir . $sampleById[0]['website_view_img']);
                }
                
                if ($sampleById[0]['tablet_view_img'] != '' &&  file_exists($uploadDir . $sampleById[0]['tablet_view_img'])) {
                     unlink($uploadDir . $sampleById[0]['tablet_view_img']);
                }
                
                if ($sampleById[0]['mobile_view_img'] != '' &&  file_exists($uploadDir . $sampleById[0]['mobile_view_img'])) {
                     unlink($uploadDir . $sampleById[0]['mobile_view_img']);
                }
                
                if ($sampleById[0]['technology_img'] != '' &&  file_exists($uploadDir . $sampleById[0]['technology_img'])) {
                     unlink($uploadDir . $sampleById[0]['technology_img']);
                } 
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->layout()->errorMessage = __("Operation failed!");
            }
        }
        // return $this->redirect()->toRoute('link-admin');
        return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
    }

    public function addImage($validData, $uploadDir)
    {  
        $newFileName = md5(time() . $validData['name']); 
        $validationExt = "jpg,jpeg,png,gif"; 
        $isUploaded = $this->imageUploader()->UploadFile($validData, $validationExt, $uploadDir, $newFileName);  
        return $isUploaded;
    }
}
