<?php
namespace Samples\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Link\Form\LinkForm;
use Link\Model\LinkTable;
use Link\Model\LinkCategoryTable;
use Application\Helper\BasePluginController;
use Samples\Model\SamplesTable;

class IndexController extends BasePluginController
{

    protected $linkTable;

    public function onDispatch(MvcEvent $e)
    {
        parent::onDispatch($e);
    }

    public function indexAction($params)
    {
        $lang = $this->lang;
        $position = 0;
        if ($params) {
            if (isset($params['position'])) {
                $position = 1;
            }
        }
        $viewModel = new ViewModel();
        $samplesTable = new SamplesTable($this->getServiceLocator());
        $samplesTableData = $samplesTable->getAllSamples($lang, $position);
        $dataArraySamples = array();
        if ($samplesTableData) {
            foreach ($samplesTableData as $data) {
                $dataArraySamples[$data['catId']][] = $data;
            }
        }
        $viewModel->setVariable('samples', $dataArraySamples);
        $viewModel->setTemplate('samples/index/index.phtml');
        return $viewModel;
    }

    public function sampleAction($params)
    {
 
        $lang = $this->lang; 
        $viewModel = new ViewModel();
        if (isset($params['id']) && $params['id']) {
            $config = $this->serviceLocator->get('Config');  
            $uploadDir = $config['base_route'] . "/uploads/samples/";
            $subjects =  $this->getRegistry( 'sampleSubject' );
            
            
            $samplesTable = new SamplesTable($this->getServiceLocator());
            $samplesTableData = $samplesTable->getSamplesById($params['id']);
    
            if ($samplesTableData) {
                $viewModel->setVariable('subjects', $subjects);
                $viewModel->setVariable('pathUpload', $uploadDir);
                $viewModel->setVariable('sample', $samplesTableData);
            }
        }
        $viewModel->setTemplate('samples/index/sample.phtml');
        return $viewModel;
    }
 
}
