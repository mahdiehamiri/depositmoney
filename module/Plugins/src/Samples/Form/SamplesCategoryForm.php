<?php
namespace Samples\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class SamplesCategoryForm extends Form
{

    public function __construct($allActiveLanguages, $samplesCategoryTableData = array())
    {
        parent::__construct('linkForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'id' => 'linkForm',
            'novalidate' => true
        ));
        
        $title = new Element\Text('catergory_title');
        $title->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control validate[required]',
            'placeholder' => __('Title'),
            'required' => 'required'
        ));
        $title->setLabel(__('Title'));
        
        $order = new Element\Text('category_order');
        $order->setAttributes(array(
            'id' => 'order',
            'class' => 'form-control ',
            'placeholder' => __('Order')
        ));
        $order->setLabel(__('Order'));
        
        $status = new Element\Checkbox('category_status');
        $status->setAttributes(array(
            'id' => 'order', 
            'placeholder' => __('Status')
        ));
        $status->setLabel(__('Status'));
        
        $lang = new Element\Select('catergory_lang');
        $lang->setAttributes(array(
            'id' => 'lang',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $languages = array();
        if ($allActiveLanguages) {
            foreach ($allActiveLanguages as $language) {
                $languages[$language['code']] = $language['title'];
            }
        }
        $lang->setValueOptions($languages);
        $lang->setLabel(__('Language'));
        
        
        $category = new Element\Select('parent_id');
        $category->setAttributes(array(
            'id' => 'category',
            'class' => 'form-control'
        ));
    
        $categoryList = array();
    
        if ($samplesCategoryTableData) {
                $categoryList[0] = '';
            foreach ($samplesCategoryTableData as $cat) {
                $categoryList[$cat['id']] = $cat['catergory_title'];
            }
        }
        $category->setValueOptions($categoryList);
        $category->setLabel(__('Parent'));
    
        $image = new Element\File('image');
        $image->setAttributes(array(
            'id' => 'fileUpload1'
        ));
        $image->setLabel(__("Choose image"));
        
        $csrf = new Element\Csrf('sampleCategorycsrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__('Save and Close'));
        $submit->setAttributes(array(
            'id' => 'submitLink',
            'class' => 'btn btn-primary'
        ));
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__('Save and New'));
        $submit2->setAttributes(array(
            'id' => 'submitLink',
            'class' => 'btn btn-primary'
        ));
        
        $this->add($title)  
            ->add($lang) 
            ->add($order) 
            ->add($category) 
            ->add($status) 
            ->add($csrf)
            ->add($submit)
            ->add($submit2);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        
        $inputFilter->add($factory->createInput(array(
            'name' => 'catergory_title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        $inputFilter->add($factory->createInput(
            array(
                'name'     => 'parent_id',
                'required' => false,
            )
            ));
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
