<?php
namespace Samples\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class SamplesForm extends Form
{

    public function __construct($samplesCategoryTableData = array(), $subjects = array())
    {
        parent::__construct('linkForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'id' => 'linkForm',
            "enctype" => "multipart/form-data",
            'novalidate' => true
        ));
        
        $title = new Element\Text('sample_title');
        $title->setAttributes(array(
            'id' => 'title',
            'class' => 'form-control validate[required]',
            'placeholder' => __('Title'),
            'required' => 'required'
        ));
        $title->setLabel(__('Title'));
        
        $order = new Element\Text('sample_order');
        $order->setAttributes(array(
            'id' => 'order',
            'class' => 'form-control ',
            'placeholder' => __('Order')
        ));
        $order->setLabel(__('Order'));
        
        $link = new Element\Text('sample_link');
        $link->setAttributes(array(
            'id' => 'order',
            'class' => 'form-control ',
            'placeholder' => __('Link')
        ));
        $link->setLabel(__('Link'));
        
        $status = new Element\Checkbox('sample_status');
        $status->setAttributes(array(
            'id' => 'order',
            'placeholder' => __('Status')
        ));
        $status->setLabel(__('Status'));
        
        $viewHomePage = new Element\Checkbox('view_home_page');
        $viewHomePage->setAttributes(array(
            'id' => 'order',
            'placeholder' => __('View In Home Page')
        ));
        $viewHomePage->setLabel(__('View Home Page Status'));
        
        $description = new Element\Textarea('description');
        $description->setAttributes(array(
            'id' => 'order',
            'class' => 'form-control ',
            'placeholder' => __('Description')
        ));
        $description->setLabel(__('Description'));
        
        $category = new Element\Select('samples_category_id');
        $category->setAttributes(array(
            'id' => 'category',
             'class' => 'form-control '
        ));
        
        $categoryList = array();
        
        if ($samplesCategoryTableData) {
            foreach ($samplesCategoryTableData as $cat) {
                $categoryList[$cat['id']] = $cat['catergory_title'];
            }
        }
        $category->setValueOptions($categoryList);
        $category->setLabel(__('Category'));
        
        $technologyImage = new Element\File('technology_img');
        $technologyImage->setAttributes(array(
            'id' => 'fileUpload1'
        ));
        $technologyImage->setLabel(__("Choose Technology Image"));
        
        $websiteImage = new Element\File('website_view_img');
        $websiteImage->setAttributes(array(
            'id' => 'fileUpload2'
        ));
        $websiteImage->setLabel(__("Choose Website View Image"));
        
        $tabletImage = new Element\File('tablet_view_img');
        $tabletImage->setAttributes(array(
            'id' => 'fileUpload3'
        ));
        $tabletImage->setLabel(__("Choose Tablet View Image"));
        
        $mobileImage = new Element\File('mobile_view_img');
        $mobileImage->setAttributes(array(
            'id' => 'fileUpload4'
        ));
        $mobileImage->setLabel(__("Choose Mobile View Image"));
        
        $introImage = new Element\File('intro_img');
        $introImage->setAttributes(array(
            'id' => 'fileUpload5'
        ));
        $introImage->setLabel(__("Choose Intro  Image"));
        
        $csrf = new Element\Csrf('samplecsrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__('Save and Close'));
        $submit->setAttributes(array(
            'id' => 'submitLink',
            'class' => 'btn btn-primary'
        ));
        
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__('Save and New'));
        $submit2->setAttributes(array(
            'id' => 'submitLink',
            'class' => 'btn btn-primary'
        ));        
        
        $subject = new Element\Select('subject');
        $subject->setLabel(__('Cooperation Subject'));
        $subject->setAttributes(array(
            'class' => 'form-control '
        ));
        $subjctConfig = array();
        // var_dump($subjects);die;
        if ($subjects) {
            foreach ($subjects as $key => $subjectN) {
                $subjctConfig[$key] = $subjectN;
            }
        }
        
        $subject->setValueOptions($subjctConfig);
        
        $this->add($title)
            ->add($websiteImage)
            ->add($technologyImage)
            ->add($introImage)
            ->add($tabletImage)
            ->add($mobileImage)
            ->add($order)
            ->add($subject)
            ->add($category)
            ->add($viewHomePage)
            ->add($description)
            ->add($status)
            ->add($csrf)
            ->add($submit)
            ->add($submit2)
            ->add($link);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        
        $inputFilter->add($factory->createInput(array(
            'name' => 'sample_title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        $inputFilter->add($factory->createInput(array(
            'name' => 'parent_id',
            'required' => false
        )));
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
    /* public function isValid()
    {
        $this->getInputFilter()->remove('intro_img');
        $this->getInputFilter()->remove('mobile_view_img');
        $this->getInputFilter()->remove('tablet_view_img');
        $this->getInputFilter()->remove('website_view_img');
        return parent::isValid();
    } */
}
