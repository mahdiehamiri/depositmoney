<?php
namespace Samples\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class SamplesTable extends BaseModel
{

    protected $tableGateway;

    protected $adapter;

    protected $serviceManager;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('samples', $this->getDbAdapter());
    }

    public function getRecords()
    {
        return $this->tableGateway->select()->toArray();
    }

    public function getRecordsSample($id = false)
    {
        $select = new Select('samples');
        return $select;
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getSamplesByPosition($position)
    {
        $select = new Select('samples');
        $select->where->equalTo('samples_position', $position);
        $select->columns(array(
            'id'
        ));
        
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getAllSamples($lang = 'fa',$position)
    {
        $select = new Select('samples');
        $select->join('samples_category', 'samples_category.id = samples.samples_category_id', array(
            'catergory_title',
            'catId' => 'id'
        ), 'left');
         $select->where->equalTo('catergory_lang', $lang);
         if ($position) {
          $select->where->equalTo('view_home_page', $position);
         }
         return $this->tableGateway->selectWith($select)->toArray();
    }
    public function getAllSamplesBuyCategoryId($catId)
    {
        $select = new Select('samples');
        $select->join('samples_category', 'samples_category.id = samples.samples_category_id', array(
            'catergory_title',
            'catId' => 'id'
        ), 'left');
         $select->where->equalTo('samples_category_id', $catId);
         return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getAllSamplesList()
    {
        $select = new Select('samples');
        return $select;
    }

    public function getSamplesById($samplesId)
    {
        $select = new Select('samples');
        $select->where->equalTo('id', $samplesId);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function addSamples($data)
    {
        return $this->tableGateway->insert(array(
            'sample_link' => $data['sample_link'],
            'sample_title' => $data['sample_title'],
            'samples_category_id' => $data['samples_category_id'],
            'sample_status' => $data['sample_status'],
            'sample_order' => $data['sample_order'],
            'intro_img' => $data['intro_img'],
            'website_view_img' => $data['website_view_img'],
            'tablet_view_img' => $data['tablet_view_img'],
            'technology_img' => $data['technology_img'],
            'mobile_view_img' => $data['mobile_view_img'],
            'subject' => $data['subject'],
            'view_home_page' => $data['view_home_page'],
            'description' => $data['description']
        ));
    }

    public function editSamples($data, $samplesId)
    {
        return $this->tableGateway->update(array(
            'sample_link' => $data['sample_link'],
            'sample_title' => $data['sample_title'],
            'samples_category_id' => $data['samples_category_id'],
            'sample_status' => $data['sample_status'],
            'intro_img' => $data['intro_img'],
            'sample_order' => $data['sample_order'],
            'technology_img' => $data['technology_img'],
            'website_view_img' => $data['website_view_img'],
            'tablet_view_img' => $data['tablet_view_img'],
            'mobile_view_img' => $data['mobile_view_img'],
            'description' => $data['description'],
            'view_home_page' => $data['view_home_page'],
            'subject' => $data['subject']
        ), array(
            'id' => $samplesId
        ));
    }

    public function deleteSample($samplesId)
    {
        return $this->tableGateway->delete(array(
            'id' => $samplesId
        ));
    }
}
