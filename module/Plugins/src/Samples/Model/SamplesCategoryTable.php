<?php 
namespace Samples\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class SamplesCategoryTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('samples_category', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getRecordsCategorySample($id = false)
	{
	    $select = new Select('samples_category');
	    return $select;
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getSamplesCategoryByPosition($position)
	{
	    $select = new Select('samples_category');
	    $select->where->equalTo('samples_position', $position);
	    $select->columns(array('id'));
	    
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getAllSamplesCategory()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllSamplesCategoryList()
	{
	    $select = new Select('samples_category');
	    return $select;
	}
	
	public function getSamplesCategoryById($samplesId)
	{
		$select = new Select('samples_category');
		$select->where->equalTo('id', $samplesId);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addSamplesCategory($data)
	{
		return $this->tableGateway->insert(array( 
		        'parent_id'=> $data['parent_id'],
// 		        'samples_position'=> $data['samples_position'],
		        'catergory_title'=> $data['catergory_title'],
		        'catergory_lang'=> $data['catergory_lang'],
		        'category_status'=> $data['category_status'],
		        'category_order'=> $data['category_order'],
		));
	}
	
	public function editSamplesCategory($data, $samplesId)
	{
		return $this->tableGateway->update(array(
				'parent_id'=> $data['parent_id'],
// 		        'samples_position'=> $data['samples_position'],
		        'catergory_title'=> $data['catergory_title'],
		        'catergory_lang'=> $data['catergory_lang'],
		        'category_status'=> $data['category_status'],
		        'category_order'=> $data['category_order'],
		), array('id' => $samplesId));		
	}
	
	public function deleteSamplesCategory($samplesId)
	{
		return $this->tableGateway->delete(array(
				'id' => $samplesId
		));
	}
}
