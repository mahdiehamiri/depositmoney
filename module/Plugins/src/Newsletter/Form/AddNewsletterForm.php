<?php

namespace Newsletter\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class AddNewsletterForm extends Form
{
	public function __construct()
	{
		parent::__construct('newsletter_form');
		$this->setAttributes(array(
				'method' => 'post',
		        'id', 'newsletter_form',
    		    'id'      => 'newsletter_form',
    		    'novalidate'=> true
		));
		$newsletterSubject = new Element\Text('subject');
		
		$newsletterSubject->setAttributes(array(
				'class' => 'form-control input-sm validate[required]',
		        'required' => 'required',
		        'placeholder' => __('Enter your subject'),
		));
		$newsletterSubject->setLabel(__("Enter your subject"));
		
		$newsletterBody = new Element\Textarea('body');
		$newsletterBody->setAttributes(array(
		    'class' => 'form-control input-sm newsletter-input validate[required,custom[email]]',
		    'required' => 'required',
		    'placeholder' => __('Enter mail body'),
		));
		$newsletterBody->setLabel(__("Enter mail body"));
		
		
		$submit = new Element\Submit('newsletter_submit');
		$submit->setValue(__("Send"));
		$submit->setAttributes(array(
		        'id'  =>  'newsletter-button',
				'class' => 'fancy-button button-line button-white large zoom subform'
		));
		
		$this->add($newsletterSubject)
			 ->add($newsletterBody)
			 ->add($submit);
		
	}	
	
	
	
	// Thanks Ms.Khoshnavaz for this code
	
// 	public function isValid()
// 	{
// 		$this->getInputFilter()->remove('tag_title');
// 		return parent::isValid();
// 	}
}
