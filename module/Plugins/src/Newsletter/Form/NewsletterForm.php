<?php

namespace Newsletter\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class NewsletterForm extends Form
{
	public function __construct()
	{
		parent::__construct('newsletter_form');
		$this->setAttributes(array(
				'method' => 'post',
		        'id', 'newsletter_form'
		));
		
		$newsletterEmail = new Element\Text('newsletter_email');
		$newsletterEmail->setAttributes(array(
		    'class' => 'form-control input-sm newsletter-input validate[required,custom[email]]',
		    'placeholder' => _('Enter your email')
		));
		
		$submit = new Element\Submit('newsletter_submit');
		$submit->setValue(__("Submit"));
		$submit->setAttributes(array(
		        'id'  =>  'newsletter-button',
				'class' => 'fancy-button button-line button-white large zoom subform'
		));
		
		$this->add($newsletterEmail)
			 ->add($submit);
		
	}	
	
	
	
	// Thanks Ms.Khoshnavaz for this code
	
// 	public function isValid()
// 	{
// 		$this->getInputFilter()->remove('tag_title');
// 		return parent::isValid();
// 	}
}
