<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Newsletter\Controller;

use Application\Helper\BaseController;

use Newsletter\Model\NewsletterTable;


class AjaxController extends BaseController
{
   
    public function saveNewsletterAction()
    {
    
        $this->layout("/layout/empty.phtml");
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $newsletterTable = new NewsletterTable($this->getServiceLocator());
            try{
                $isAdded = $newsletterTable->addNewsletter($data);
                if ($isAdded) {
                    die("save");
                }
            }catch(\Exception $e){
                die('duplicate');
            }
             
        } 
        die("failed");
    }
     
   
    
   
}
