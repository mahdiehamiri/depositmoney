<?php
namespace Newsletter\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Link\Form\LinkForm;
use Link\Model\LinkTable;
use Tour\Model\TourTable;
use Tour\Model\TourTypeTable;
use Search\Form\SearchForm;
use Application\Helper\BaseController;
use Newsletter\Model\NewsletterTable;
use Newsletter\Form\NewsletterForm;
use Application\Helper\BasePluginController;
 
class IndexController extends BasePluginController 
{ 	
	public function indexAction()
	{
        $newsletterForm = new NewsletterForm();
        $view['newsletterForm'] = $newsletterForm;
        $viewModel = new ViewModel($view);
        $viewModel->setTemplate("newsletter/index/index.phtml");
        return $viewModel;
	 
	}
}
