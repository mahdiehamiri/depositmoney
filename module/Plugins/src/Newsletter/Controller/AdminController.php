<?php

namespace Newsletter\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Newsletter\Form\NewsletterForm;
use Zend\Session\Container;
use Newsletter\Model\NewsletterTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Newsletter\Form\AddNewsletterForm;
use Newsletter\Model\AddNewsletterTable;

class AdminController extends BaseAdminController 
{ 	
    
    public function listQueuesAction()
    {
        $this->setHeadTitle(__('List queues'));
         
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
    
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
         
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $addNewsletterTable = new AddNewsletterTable($this->getServiceLocator());
        $queues = $addNewsletterTable->getAllQueues();
        $grid->setTitle(__('Queue list'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($queues,$dbAdapter);
    
    
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $grid->addColumn($col);
    
        $col = new Column\Select('to');
        $col->setLabel(__('To'));
        $grid->addColumn($col);
         
        $col = new Column\Select('subject');
        $col->setLabel(__('Subject'));
        $grid->addColumn($col);
         
        $col = new Column\Select('body');
        $col->setLabel(__('Body'));
        $grid->addColumn($col);
    
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Delete'));
        $rowId = $btn->getRowIdPlaceholder();
        	
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-remove');
        $viewAction->setLink('/' . $this->lang . '/admin-newsletter/delete-queue/'.$rowId.'/token/'.$csrf);
        $viewAction->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        	
        $link[] = '<a href="/' . $this->lang . '/admin-newsletter/add-queue" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($link);
        $grid->render();
    
        return $grid->getResponse();
    }
    
	public function listNewslettersAction()
	{
	    $this->setHeadTitle(__('List newsletters'));
		/* $container = new Container('token');
		$csrf = md5(time() . rand());
		$container->csrf = $csrf;
		$view['csrf'] = $csrf;
	    $newsletterTable = new NewsletterTable($this->getServiceLocator());
		$newsletter = $newsletterTable->getAllNewsletter();
		$view['newsletters'] = $newsletter;
		return new ViewModel($view); */
	    
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	     
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	    
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $newsletterTable = new NewsletterTable($this->getServiceLocator());
		$newsletters = $newsletterTable->getAllNewsletter();
	    $grid->setTitle(__('Newsletter list'));
	    $grid->setDefaultItemsPerPage(10);
	    $grid->setDataSource($newsletters,$dbAdapter);
	     
	     
	    $col = new Column\Select('id');
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->setWidth(5);
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	     
	    $col = new Column\Select('email');
	    $col->setLabel(__('Email'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Delete'));
	    $rowId = $btn->getRowIdPlaceholder();
	     
	    $viewAction = new Column\Action\Icon();
	    $viewAction->setIconClass('glyphicon glyphicon-remove');
	    $viewAction->setLink('/' . $this->lang . '/admin-newsletter/delete-newsletter/'.$rowId.'/token/'.$csrf);
	    $viewAction->setTooltipTitle(__('Delete'));
	    
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction);
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	     
	 
	    $grid->render();
	     
	    return $grid->getResponse();
	}

	
	public function addQueueAction()
	{
	    $this->setHeadTitle(__('Add new newsletter'));
		
		$addNewsletterForm = new AddNewsletterForm();
		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost()->toArray();
			$addNewsletterForm->setData($data);
			if ($addNewsletterForm->isValid()) {
				$validData = $addNewsletterForm->getData();
				
				$newsletterTable = new NewsletterTable($this->getServiceLocator());
				$addNewsletterTable = new AddNewsletterTable($this->getServiceLocator());
				$members = $newsletterTable->getRecords();
				if ($members) {
				    foreach ($members as $member) {
				        $data["to"] = $member["email"];
				        $addNewsletterTable->addQueue($data);
				    }
					$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
					return $this->redirect()->toRoute('newsletter-admin', array("action" =>"list-queues"));
				} else {
				    $this->layout()->errorMessage = __("Operation failed!");
				}
			} else {
				$this->layout()->errorMessage = $this->generalhelper()->getFormErrors($addNewsletterForm, true);
			}
		}
		$view['addNewsletterForm'] = $addNewsletterForm;
		return new ViewModel($view);
	}

	public function editQueueAction()
	{
	    $this->setHeadTitle(__('Edit newsletter'));
	
	    $addNewsletterForm = new AddNewsletterForm();
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost()->toArray();
	        $addNewsletterForm->setData($data);
	        if ($addNewsletterForm->isValid()) {
	            $validData = $addNewsletterForm->getData();
	
	            $newsletterTable = new NewsletterTable($this->getServiceLocator());
	            $addNewsletterTable = new AddNewsletterTable($this->getServiceLocator());
	            $members = $newsletterTable->getRecords();
	            if ($members) {
	                $data["from"] = "info@cadafzar.com";
	                foreach ($members as $member) {
	                    $data["to"] = $member["email"];
	                    $addNewsletterTable->addQueue($data);
	                }
	                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                return $this->redirect()->toRoute('newsletter-admin', array("action" =>"list-queues"));
	            } else {
	                $this->layout()->errorMessage = __("Operation failed!");
	            }
	        } else {
	            $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($addNewsletterForm, true);
	        }
	    }
	    $view['addNewsletterForm'] = $addNewsletterForm;
	    return new ViewModel($view);
	}
	public function deleteNewsletterAction()
	{
	    $config = $this->serviceLocator->get('Config');
	
	    $container = new Container('token');
	    $token = $this->params('token');
	    $newsletterId = $this->params('id');
	    $newsletterTable = new NewsletterTable($this->getServiceLocator());
	    $newsletterById = $newsletterTable->getNewsletterById($newsletterId);
	    if ($newsletterById && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
	        $isDeleted = $newsletterTable->deleteNewsletter($newsletterId);
	        if ($isDeleted) {
	            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	        } else {
	            $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
	        }
	    }
	    return $this->redirect()->toRoute('newsletter-admin', array("action" =>"list-newsletters"));
	}
	
	public function deleteQueueAction()
	{
		$config = $this->serviceLocator->get('Config');
		$container = new Container('token');
		$token = $this->params('token');
		$queueId = $this->params('id');
		$addNewsletterTable = new AddNewsletterTable($this->getServiceLocator());
		$queueById = $addNewsletterTable->getQueueById($queueId);
		if ($queueById && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
				$isDeleted = $addNewsletterTable->deleteQueue($queueId);
			    if ($isDeleted) {
					$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
				} else {
					$this->flashMessenger()->addErrorMessage(__("Operation failed!"));
				}
		}
		return $this->redirect()->toRoute('newsletter-admin', array("action" =>"list-queues"));
	}
	
}
