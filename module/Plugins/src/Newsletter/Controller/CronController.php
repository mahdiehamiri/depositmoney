<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Newsletter\Controller;

use Zend\View\Model\ViewModel;
use Permission\Model\PermissionAllPermissionsTable;
use Permission\Model\PermissionGroupsPermissionsTable;
use Zend\Mvc\Controller\AbstractActionController;
use Application\Helper\BaseController;
use Tariff\Model\TariffTariffTable;
use Tour\Model\TourEnquiryTable;
use Tour\Model\TourTable;
use Newsletter\Model\NewsletterTable;
use Newsletter\Model\AddNewsletterTable;


class CronController extends BaseController
{
    public function sendMailAction()
    {
        $addNewsletterTable = new AddNewsletterTable($this->getServiceLocator());
        $queues = $addNewsletterTable->getSomeQueues();
        if ($queues) {
            $queueIds = array();
            foreach ($queues as $queue) {
                $queueIds[] = $queue["id"];
                $to = $queue["to"];
                $subject = $queue["subject"];
                $body = $queue["body"];
                $this->mailService()->sendMail($body, $subject, $to);
            }
            $addNewsletterTable->deleteQueues($queueIds);
        }
        die;
    }
    
   
}
