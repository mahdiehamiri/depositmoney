<?php 
namespace Newsletter\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;
use Zend\Db\Sql\Where;

class AddNewsletterTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('newsletter_queue', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	public function getSomeQueues($id = false)
	{
	    $select = new Select('newsletter_queue');
		$select->limit(5);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getAllQueues()
	{
	    $select = new Select('newsletter_queue');
	    return $select;
		//return $this->tableGateway->select()->toArray();
	}
	
	public function getQueueById($queueId)
	{
		$select = new Select('newsletter_queue');
		$select->where->equalTo('id', $queueId);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addQueue($data)
	{
		return $this->tableGateway->insert(array(
		        'to'   => $data['to'],
		        'subject'   => $data['subject'],
		        'body'   => $data['body'],
		));
	}
	
	
	public function deleteQueue($queueId)
	{
		return $this->tableGateway->delete(array(
				'id' => $queueId
		));
	}
	
	public function deleteQueues($queueIds)
	{
	    $where = new Where();
	    $where->in("id", $queueIds);
	    return $this->tableGateway->delete($where);
	}
	
}
