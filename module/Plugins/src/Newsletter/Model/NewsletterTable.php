<?php 
namespace Newsletter\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class NewsletterTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('newsletter', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllNewsletter()
	{
	    $select = new Select('newsletter');
	    return $select;
		//return $this->tableGateway->select()->toArray();
	}
	
	public function getNewsletterById($newsletterId)
	{
		$select = new Select('newsletter');
		$select->where->equalTo('id', $newsletterId);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addNewsletter($data)
	{
		return $this->tableGateway->insert(array(
		        'email'   => $data,
		));
	}
	
	
	public function deleteNewsletter($newsletterId)
	{
		return $this->tableGateway->delete(array(
				'id' => $newsletterId
		));
	}
}
