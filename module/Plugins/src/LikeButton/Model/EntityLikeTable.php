<?php

namespace LikeButton\Model;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;
use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;

class EntityLikeTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	public $adapter;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->adapter = $this->getDbAdapter();
		$this->tableGateway = new TableGateway("entity_like", $this->adapter);
	}
	public function addData($data)
	{ 
		$this->tableGateway->insert(array(
							'user_id'	  => 	$data['user_id'],
							'entity_id' =>	$data['entity_id'],
		                    'entity_name' =>	$data['entity_name'],
							'like'		   	  =>	$data['like'],
					  		)
			       	  );
		return $this->tableGateway->lastInsertValue;
		
	}
	
	public function removeData($data)
	{
	    return $this->tableGateway->delete(array(
	        'user_id'	  => 	$data['user_id'],
	        'entity_id'  =>	$data['entity_id'],
	        'entity_name'  =>	$data['entity_name'],
             )
	    );
	
	}
	
	public function getData($entityId = null, $entityName = null, $enableDislikeLikeButton = false)
	{ 
	    $result[1] = null;
		if ($enableDislikeLikeButton) {
			$where = new Where();
			$select = new Select("entity_like");
			$select->columns(array("*"));
		    $select->join("users", "users.id = entity_like.user_id",array("username"),$select::JOIN_LEFT);
			$where->equalTo("entity_id", $entityId);
			$where->equalTo("entity_name", $entityName);
			$where->equalTo("like", "-1");
			$select->where($where);
			$result[1] = $this->tableGateway->selectWith($select)->toArray();
		}
		$where = new Where();
		$select = new Select("entity_like");
		$select->join("users", "users.id = entity_like.user_id",array("username"),$select::JOIN_LEFT);
		$where->equalTo("entity_id", $entityId);
		$where->equalTo("entity_name", $entityName);
		$where->equalTo("like", "1");
		$select->where($where);
		$result[0] = $this->tableGateway->selectWith($select)->toArray();
		$result = array($result[0], $result[1]);
		return $result;
	}
	
	
	
  
}