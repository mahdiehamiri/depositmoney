<?php

namespace LikeButton\Controller;

use Application\Helper\BaseAjaxController;
use Application\Helper\BaseCustomerController;
use LikeButton\Model\ShopLikeTable;
/* @var $customerProductScoresTable \Management\Model\CustomerProductScoresTable */
class AjaxController extends BaseCustomerController 
{

	public function indexAction() 
	{
	    $userId = $this->userData->id;
		$request = $this->getRequest ();
		if ($request->isPost()) {
			$postData = $request->getPost();
			$entityTable = $postData["entityTable"];
			$entityName = $postData["entityName"];
			$entityId = $postData["entityId"];
			$likeValue = $postData["value"];
			$entityTable = str_replace("-", "\\", $entityTable);
			$entityTable = new $entityTable($this->getServiceLocator());
			$insertData = array(
					'user_id'		=> $userId,
					'entity_id'	=> $entityId,
					'like' 			=> $likeValue,
			        'entity_name' 	=> $entityName
			);
			$isAdded = false;
			try {
			    //$isRemoved = $entityTable->removeData($insertData);
				$isAdded = $entityTable->addData($insertData);
			} catch (\Exception $e) {
				$isAdded = false;
			}
			if ($isAdded) {
			    die("true");
			} else {
				die("false");
			}
		} else {
			die("false");
		}
		
	}
	 
}