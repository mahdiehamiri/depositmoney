<?php
namespace LikeButton\Controller;
 
use Application\Helper\BaseController;  
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use LikeButton\Model\ShopLikeTable;
use Application\Helper\BasePluginController;
 
/* @var $attributesScoresTable \Management\Model\AttributesScoresTable */
/* @var $customerProductScoresTable \Management\Model\CustomerProductScoresTable */
class IndexController extends BasePluginController 
{ 
 
	public function indexAction($params)
	{     
	    if ($this->userData) {
    		$view['entityId'] = $params["entityId"];
    		$view['entityName'] = $params["entityName"];
    		$view["entityTable"] = str_replace("\\", "-", $params["entityTable"]);
    		$enableDislikeLikeButton = (isset($params["enableDislikeLikeButton"])?$params["enableDislikeLikeButton"]:false);
    		$view['enableDislikeLikeButton'] = $enableDislikeLikeButton;
    		
    		$entityTable = new $params["entityTable"]($this->getServiceLocator());
    		$entityLikes = $entityTable->getData($params["entityId"], $params["entityName"], $enableDislikeLikeButton);
    		
    		$view['entityLikes'] = $entityLikes;
    		
    		$viewModel = new ViewModel($view);
    		$viewModel->setTemplate("like-button/index/index.phtml");
    		return $viewModel; 
	    }
	}
	
	
	
	 
}