<?php
namespace Gallery\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class GalleryFilesTable extends BaseModel
{

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('gallery_files', $this->getDbAdapter());
    }

    public function getAllImagesById($id)
    {
        $select = new Select('gallery_files');
        $where = new Where();
        $select->where->equalTo('gallery_id', $id);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getAllImages($name, $lang, $limit = null)
    {
        $select = new Select();
        $where = new Where();
        $select->from('gallery_files');
        $select->join('galleries', 'galleries.id = gallery_files.gallery_id');
        $select->where->equalTo('latin_name', $name);
        $select->where->equalTo('lang', $lang);
        if ($limit) {
            $select->limit($limit);
        }
        $select->order("order DESC");
        return $this->tableGateway->selectWith($select)->toArray();
    }
    public function getAllImagesByIdForPage($id = false)
    {
        $select = new Select();
        $where = new Where();
        $select->from('gallery_files');
        $select->join('galleries', 'galleries.id = gallery_files.gallery_id');
        if ($id) {
            $select->where->equalTo('galleries.id', $id);
        }
        $select->order("order DESC");
        return $this->tableGateway->selectWith($select)->toArray();
    }
    // This function is used for showing latest images from galleryfiles in the HomePage of the site!
    public function fetchLatestImages($limit) 
    {
        $select = new Select();
        $select->from('gallery_files');
        $select->limit($limit);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getImageById($imageId)
    {
        $select = new Select();
        $select->from('gallery_files');
        if ($imageId > 0 && is_numeric($imageId)) {
            $select->where->equalTo('id', $imageId);
            return $this->tableGateway->selectWith($select)->toArray();
        }
    }

    public function add($data)
    {
        return $this->tableGateway->insert(array(
            'caption' => $data['caption'],
            'image' => $data['image'],
            'thumbnail' => $data['thumbnail']
        ));
    }

    public function updateImage($newData, $imageId)
    {
        var_dump($newData);
        return $this->tableGateway->update($newData, array(
            'id' => $imageId
        ));
    }

    public function delete($imageId)
    {
        if ($imageId > 0 && is_numeric($imageId)) {
            return $this->tableGateway->delete(array(
                'id' => $imageId
            ));
        }
    }

    public function deleteByGalleryId($Id)
    {
        if ($Id > 0 && is_numeric($Id)) {
            return $this->tableGateway->delete(array(
                'gallery_id' => $Id
            ));
        }
    }

    public function getImageBygalleryId($galleryId)
    {
        $select = new Select();
        $select->from('gallery_files');
        $select->where->equalTo('gallery_id', $galleryId);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
  /*   public function getAllImagesForTour($lang = 'en', $limit = 1000)
    {
        $select = new Select();
        $where = new Where();
        $select->from('gallery_files');
        $select->join('galleries', 'galleries.id = gallery_files.gallery_id'); 
        $select->where->equalTo('lang', $lang);
        if ($limit) {
            $select->limit($limit);
        }
        $select->order("order DESC");
        return $this->tableGateway->selectWith($select)->toArray();
    } */
}
