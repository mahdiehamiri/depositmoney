<?php 

namespace Gallery\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;
use Zend\Db\Sql\Where;

class GalleriesTable extends BaseModel 
{
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('galleries', $this->getDbAdapter());
    }  
	public function getAllParentGallery($galleryId = false)
	{
	    $select = new Select();
	    $select->from('galleries');
	    $select->columns(array(
	        'id',
	        'name' => 'persian_name'
	        
	    ));
	    $select->where->equalTo('parent_id', 0);
	    if ($galleryId) {
		    $select->where->notEqualTo('id', $galleryId);
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fatchAllChild($id)
	{
	    $select = new Select();
	    $select->from('galleries');
	  
	    $select->where->equalTo('parent_id', $id);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	
	
	public function getAllGallery( $lang = false )
	{
	    $sql = "SELECT `id`,`lang`, `persian_name`, `latin_name`, b.`parent_id`, a.Count FROM `galleries` b LEFT OUTER JOIN (SELECT  `parent_id`, COUNT(*) AS Count FROM `galleries` group by `parent_id`)a on b.`id` = a.`parent_id`  ";
	    if ($lang) {
	   	   $sql = "SELECT `id`,`lang`, `persian_name`, `latin_name`, b.`parent_id`, a.Count FROM `galleries` b LEFT OUTER JOIN (SELECT  `parent_id`, COUNT(*) AS Count FROM `galleries` group by `parent_id`)a on b.`id` = a.`parent_id` where `lang` LIKE '%$lang%'";
	    	
	    }
	    return $this->tableGateway->getAdapter()->driver->getConnection()->execute($sql); 
	     
	}
	public function getAllGalleryMain()
	{
		$select = new Select();
		$select->from('galleries');
		$select->where->equalTo('parent_id', 0);
	    return  $select;
	
	}
	public function getAllGallerySub()
	{
		$select = new Select();
		$select->from('galleries');
		$select->where->notEqualTo('parent_id', 0);
	    return $this->tableGateway->selectWith($select)->toArray();
	
	}
	
	public function fetchLatestGallery($limit) // This function is used for showing latest images from gallery in the HomePage of the site!
	{
		$select = new Select();
		$select->from('galleries');
		$select->limit($limit);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getGalleryById($Id)
	{
		$select = new Select();
		$select->from('galleries');
		if ($Id > 0 && is_numeric($Id)) {
			$select->where->equalTo('id', $Id);
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	public function getGalleryByParrentId($Id)
	{
		$select = new Select();
		$select->from('galleries');
		if ($Id > 0 && is_numeric($Id)) {
			$select->where->equalTo('parent_id', $Id);
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	
	public function addGallery($data)
	{
		return $this->tableGateway->insert(array(
		        'lang'   => $data['lang'],
				'latin_name'   => $data['latin_name'],
				'persian_name' => $data['persian_name'],
		        'parent_id'    => $data['parent_id']
		));
	}
	
	public function editGallery($newData, $id)
	{ 
 
		return $this->tableGateway->update(array(
				'lang' => $newData['gallery_lang'],
				'latin_name' => $newData['latin_name'],
				'persian_name' => $newData['persian_name'],
				'parent_id' => $newData['parent_id'],
		), array(
				'id' => $id
		));
	}
	
	public function deleteGallery($Id)
	{
	    $where = new Where();
	    $where->equalTo('id' , $Id)->or->equalTo('parent_id', $Id);
		return $this->tableGateway->delete($where);
	}
}
