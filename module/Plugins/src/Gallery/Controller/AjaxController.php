<?php
namespace Gallery\Controller;
 
use Zend\Mvc\Controller\AbstractActionController;
use Reserving\Model\ReservingReservingsTable;
use Gallery\Model\GalleryFilesTable;

class AjaxController extends AbstractActionController
{

    public function selectorAction()
    {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $galleryId = $request->getPost('galleryId');   
            $galleryFilesTable = new GalleryFilesTable($this->getServiceLocator());
            $allimage = $galleryFilesTable->getImageBygalleryId($galleryId ); 
           // die(json_encode($allimage));
            try {
                if ($allimage) {
                   // echo json_encode($allimage);
                    die(json_encode($allimage));
                }
            } catch (\Exception $e) {
                die(false);
            }
        }
    }
}