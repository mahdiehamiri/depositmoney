<?php
namespace Gallery\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Gallery\Model\GalleriesTable;
use Gallery\Model\GalleryFilesTable;
use Application\Helper\BasePluginController;
 
class IndexController extends BasePluginController 
{ 
	protected $galleryTable;
	
	
    public function indexAction($params)
	{
	   // die('aaaaa');
        $lang = $this->lang;
      
	   if (is_array($params)) {
            $latin_name = $params['latin_name'];
            $limit = $params['limit'];
             $layout = $params['layout'];
        } else {
            $latin_name = $params;
            $limit = 1000;
            $layout = "gallery";
        }
       
        $galleriesTable = new GalleriesTable($this->getServiceLocator());
        $galleryFilesTable = new GalleryFilesTable($this->getServiceLocator());
        $allimage = $galleryFilesTable->getAllImages($latin_name, $lang);
        $galleryImages = $galleriesTable->getAllGallery( $lang );

        
        $menu = array(
            'items' => array(),
            'parents' => array()
        );
        foreach ($galleryImages as  $items){
        
            $menu['items'][$items['id']] = $items;
            $menu['parents'][$items['parent_id']][] = $items['id'];
        
        }
        $galleryImages = $this->buildMenuGallery(0, $menu);
        //var_dump($galleryImages); die;

        $ViewModel = new ViewModel();

        $ViewModel->setTemplate('gallery/index/'.$layout.'.phtml');
        //var_dump($layout); die;
        $ViewModel->setVariable('galleries', $galleryImages);
        
        if(count($allimage) >0 ){
            $ViewModel->setVariable('images', $allimage);
            $ViewModel->setVariable('language', $lang);
            $view['images'] = $allimage;
            $view['language'] = $lang;
          
            return $ViewModel;
        }else{
            $ViewModel->setVariable('language', $lang);
            $view['language'] = $lang;
           
            return $ViewModel;
            //$this->layout()->errorMessage = __('There is no image in this gallery!');
        }
	   
	}
	
	public function maingalleryAction($params)
	{  
 
	        $lang = $this->lang;
  
	        $latin_name = $params['latin_name'];
	        $galleriesTable = new GalleriesTable($this->getServiceLocator());
	        $galleryFilesTable = new GalleryFilesTable($this->getServiceLocator());
	
	        $allimage = $galleryFilesTable->getAllImages($latin_name, $lang);
	        $galleryImages = $galleriesTable->getAllGallery();
 
	        $ViewModel = new ViewModel();
	        $ViewModel->setTemplate('gallery/index/maingallery.phtml');
	        if(count($allimage) >0 ){
	            $ViewModel->setVariable('images', $allimage);
	            $view['images'] = $allimage;
	            return $ViewModel;
	        }else{
	            $this->layout()->errorMessage = __('There is no image in this gallery!');
	        }
 
	
	}
	public function pageAction($id)
	{
	
	    $lang = $this->lang;
	
	
	    $galleriesTable = new GalleriesTable($this->getServiceLocator());
	    $galleryFilesTable = new GalleryFilesTable($this->getServiceLocator());
	    $allimage = $galleryFilesTable->getAllImagesByIdForPage($id);
	    $ViewModel = new ViewModel();
	    $ViewModel->setTemplate('gallery/index/page.phtml');
	
	    if(count($allimage) >0 ){
	        $ViewModel->setVariable('images', $allimage);
	        return $ViewModel;
	    }else{
	        //$this->layout()->errorMessage = __('There is no image in this gallery!');
	    }
	
	}
	public function showImageAction($id)
	{
	    $galleriesTable = new GalleriesTable($this->getServiceLocator());
	    $galleryFilesTable = new GalleryFilesTable($this->getServiceLocator());
	    $allimage = $galleryFilesTable->getAllImages($id);
	    $galleryImages = $galleriesTable->getAllGallery();
	     $menu = array(
	        'items' => array(),
	        'parents' => array()
	    );
	    foreach ($galleryImages as  $items){
	         
	        $menu['items'][$items['id']] = $items;
	        $menu['parents'][$items['parent_id']][] = $items['id'];
	         
	    }
	    $galleryImages = $this->generalhelper()->buildGalleryMenu(0, $menu);
	    
	    if(count($allimage) >0 ){
    	    $view['image'] = $allimage;
    	    return new ViewModel($view);	    
	    }else{
	        $this->layout()->errorMessage = __('There is no image in this gallery!');
	    }
	    
	}
	public function buildMenuGallery($parent, $menu)
	{
	    $html = array();
	    if (isset($menu['parents'][$parent]))
	    {
	
	        foreach ($menu['parents'][$parent] as $itemId)
	        {
	            if(!isset($menu['parents'][$itemId]))
	            {
	                $html[$itemId]['persian_name']= $menu['items'][$itemId]['persian_name'];
	                $html[$itemId]["latin_name"]= $menu['items'][$itemId]['latin_name'];
	            }
	            if(isset($menu['parents'][$itemId]))
	            {
	                $html[$itemId]["name"]= $menu['items'][$itemId]['persian_name'];
	                $html[$itemId]["latin_name"]= $menu['items'][$itemId]['latin_name'];
	                $html[$itemId]["persian_name"]= $menu['items'][$itemId]['persian_name'];
	                $html[$itemId]["child"] = $this->buildMenuGallery($itemId, $menu);
	            }
	        }
	    }
	    return $html;
	}
	
}
