<?php

namespace Gallery\Controller;
   
use Zend\View\Model\ViewModel;
use Gallery\Form\GalleryForm;
use Zend\Session\Container;
use Gallery\Model\GalleriesTable;
use Application\Helper\BaseAdminController;
use DropZone\Model\GalleryFilesTable;
use Language\Model\LanguageLanguagesTable;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Zend\Db\Adapter\Adapter;

class AdminController extends BaseAdminController 
{ 
	public function listGalleriesAction()
	{
	   
	    $this->setHeadTitle(__('List galleries'));
 
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
  
		$view['csrf'] = $csrf;
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $galleriesTable = new GalleriesTable($this->getServiceLocator());
        $imageFile = $galleriesTable->getAllGalleryMain();
        $coworkers = array();
        foreach ($imageFile as $dataFile) {
        	$parentData = $galleriesTable->getGalleryById($dataFile['parent_id']);
        	$dataFile['parrent_name'] = $parentData[0]['persian_name'];
            $coworkers[] = $dataFile;
        }
 
        $grid->setTitle(__('List galleries'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($imageFile, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('persian_name');
        $col->setLabel(__('Persian Name'));
        $grid->addColumn($col);
        
        $col = new Column\Select('latin_name');
        $col->setLabel(__('Latin Name'));
        $grid->addColumn($col);
 
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-plus');
        $viewAction3->setLink("/" . $this->lang . '/admin-gallery/add-image/' . $rowId);
    
        $viewAction3->setTooltipTitle(__('Add picture to gallery'));
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-gallery/edit-gallery/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-gallery/delete-gallery/' . $rowId . '/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $rowIds = $btn->getRowIdPlaceholder ();
        $viewAction4 = new Column\Action\Icon ();
        $viewAction4->setIconClass ( 'glyphicon glyphicon-chevron-up hideData' );
        $viewAction4->setLink ( "#dd" );
        $viewAction4->setTooltipTitle ( __( 'Show sub gallery' ) );
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction4);
        $actions2->addAction($viewAction3);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $sampleAdd[] = '<a href=' . "/" . $this->lang . '/admin-gallery/add-gallery class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
        $sampleAdd[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($sampleAdd);
        
        $allSubGallery = array ();
        $imageFileNew = $galleriesTable->getAllGallerySub();
      
        if ($imageFileNew) {
       
        	foreach ( $imageFileNew as $key => $data ) {
        		/* $head = '<thead class="subrows "><tr style="background-color:#eeefff;"  class="hidden myHidden' . $data['parent_id'] . '" id="hidden' . $data['parent_id'] . '">
	                		 <th colspan="2">' . __("Persian Name ") . '</th> 
	                		<th colspan="2">' . __("Latin Name ") . '</th>   
	                		<th colspan="3">' . __("Action ") . '</th>   
                		 </tr></thead>';
        		$data['head'] = $head; */
        
        		$dataarray = array (
        				//'head' => $data['head'],
        				'parent_id' => $data['parent_id'],
        				//'gallery_id' => $data['id'], 
        				'persian_name' => $data['persian_name'],
        				'latin_name' => $data['latin_name'] ,
        		); 	
        		$allSubGallery[$key] = $dataarray;  
        		$allSubGallery[$key]['addgallerylink'] = '<td colspan="3" style="padding-right: 42px;"><a   href=' . "/" . $this->lang . '/admin-gallery/add-image/' . $data['id'] . ' class=""><i class="glyphicon glyphicon-plus"></i> </a>';
        		$allSubGallery[$key]['editlink'] = '<a   href=' . "/" . $this->lang . '/admin-gallery/edit-gallery/' . $data['id'] . ' class=""> <i class="glyphicon glyphicon-edit"></i></a> ';
        		$allSubGallery[$key]['deletelink'] = '<a   href=' . "/" . $this->lang . '/admin-gallery/delete-gallery/' . $data['id']  . '/' . $csrf.' class=""> <i class="glyphicon glyphicon-remove"></i></a></td> ';
        	}
        }
  
        $grid->setExtraData ( $allSubGallery );
        $grid->render();
        return $grid->getResponse();
	}
	
	public function addGalleryAction()
	{	  
	    $this->setHeadTitle(__('Add new gallery'));
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
	    $galleriesTable = new GalleriesTable($this->getServiceLocator());
	    $parents = $galleriesTable->getAllParentGallery();

	    $galleryForm = new GalleryForm($allActiveLanguages, false, $parents);
	    $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $galleryForm->setData($data);
            if ($galleryForm->isValid()) {
                $validData = $galleryForm->getData();
                $item = array(
                    'lang'           => $validData['gallery_lang'],
                    'persian_name'   => $validData['persian_name'],
                    'latin_name'     => $validData['latin_name'],
                    'parent_id'      => $validData['parent_id']
                );
                try {
                    $isAdded = $galleriesTable->addGallery($item);
	                if ($isAdded) {
	                    if (isset($_POST['submit'])) {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('gallery-admin');
	                    } else {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('gallery-admin', array(
	                            'action' => 'add-gallery'
	                        ));
	                    }
	                }
                	
                } catch (\Exception $e) {
	                $this->layout()->errorMessage = __('Duplicated name! please choose another one!');
                }
            } 
        }
        $view['galleryForm'] = $galleryForm;
        return new ViewModel($view);
	
	}

	public function editGalleryAction()
	{
	    $this->setHeadTitle(__('Edit gallery'));
	    $config = $this->serviceLocator->get('Config');
	    $uploadDir = $config['base_route'] . "/uploads/gallery/";
	    $validationExt ="jpg,jpeg,png,gif";
	    
	    $id = $this->params('id', -1);
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
	    $galleriesTable = new GalleriesTable($this->getServiceLocator());
	    $parents = $galleriesTable->getAllParentGallery($id);
	    
	    $galleryById = $galleriesTable->getGalleryById($id);
	    $galleryForm = new GalleryForm($allActiveLanguages, true, $parents,$galleryById[0]['lang']);
 
	    $view['galleryById'] = $galleryById;
	    $galleryForm->populateValues($galleryById[0]);
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost()->toArray();
	        $files =  $request->getFiles()->toArray();
	        $data = array_merge($data, $files);
	        $galleryForm->setData($data);
	        if ($galleryForm->isValid()) {
	            $validData = $galleryForm->getData();
	            $item = array();
	            $item['gallery_lang'] = $validData['gallery_lang'];
	            $item['latin_name'] = $validData['latin_name'];
	            $item['persian_name'] = $validData['persian_name'];
	            $item['parent_id'] = $validData['parent_id'];
	            if ($id) {
	            	try {
	            	    $isEdited = $galleriesTable->editGallery($item, $id);
	            	 
		                if ($isEdited !== false) {
		                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
		                } else {
		                	$this->flashMessenger()->addErrorMessage(__('Operation failed!'));
		                }
		                return $this->redirect()->toRoute('gallery-admin');
	            	} catch (\Exception $e) { 
	            		$this->layout()->errorMessage = __('Duplicated name! please choose another one!');
	            	}
	            }
	        } 
	    } 
	    $view['galleryForm'] = $galleryForm;
	    return new ViewModel($view);
	}
	
	public function addImageAction()
	{
	    $this->setHeadTitle(__('Add image'));
        $view['userId'] = $this->userData->id;
        $view['galleryId'] = $this->params('id');
        return new ViewModel($view);
	}
	
	public function deleteGalleryAction()
	{
		$container = new Container('token');
		$token = $this->params('token');
		$galleriesTable = new GalleriesTable($this->getServiceLocator());
		$galleryFilesTable = new \Gallery\Model\GalleryFilesTable($this->getServiceLocator());

		$id = $this->params('id', -1);
	 
		if ($id && $container->offsetExists('csrf') && $container->offsetGet('csrf') === $token) {
			$hasChild = $galleriesTable->getGalleryByParrentId($id);
			if (!$hasChild) {
				$Isdelete = $galleriesTable->deleteGallery($id);
				if($Isdelete){
				    $allImageForDelete = $galleryFilesTable->getAllImagesById($id);
				    $deleteImages = $galleryFilesTable->deleteByGalleryId($id);
				    if($deleteImages){
				        foreach ($allImageForDelete as $image){
				            if(file_exists($_SERVER['DOCUMENT_ROOT']."/uploads/gallery/".$image['file_name']) )
				            @unlink($_SERVER['DOCUMENT_ROOT']."/uploads/gallery/".$image['file_name']);
				        }
				    }
				}
				$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
				return $this->redirect()->toRoute('gallery-admin');
		  } else { 
				$this->flashMessenger()->addErrorMessage(__("Operation faild This Gallery Has Child."));
				return $this->redirect()->toRoute('gallery-admin');
		  }

	} else {
		$this->flashMessenger()->addErrorMessage(__("Operation faild There is some problem."));
		return $this->redirect()->toRoute('gallery-admin');
	}
		
	}
}
