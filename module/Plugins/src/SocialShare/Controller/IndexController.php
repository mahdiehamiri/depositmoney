<?php

namespace SocialShare\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use SocialShare\Form\SocialShareForm;
use SocialShare\Model\SocialShareTable;
use Application\Helper\BasePluginController;
use Application\Helper\MailService;
 
class IndexController extends BasePluginController 
{ 
	 
	public function onDispatch(MvcEvent $e)
	{
		parent::onDispatch($e);
	}
	
	
	public function indexAction($params)
	{ 
  
		$view ['parameter'] = $params;
		$viewModel = new ViewModel ( $view );
		return $viewModel->setTemplate ('social-share/index/index.phtml');
	}
 
}
