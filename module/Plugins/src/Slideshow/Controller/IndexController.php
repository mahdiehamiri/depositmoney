<?php 
namespace Slideshow\Controller;
   
 
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Slideshow\Form\SlideshowForm;
use Slideshow\Model\SlideshowTable;
use Application\Helper\BasePluginController;
 
class IndexController extends BasePluginController 
{ 	
	public function onDispatch(MvcEvent $e)
	{
		parent::onDispatch($e);
	}
	public function indexAction($params)
	{
        $lang = $this->lang;
        if (is_array($params)) {
            $position = $params['latin_name'];
            $layout = $params['layout'];
        } else {
            $position = $params;
            $layout = "index";
        }
        $viewModel = new ViewModel();
        $slideshowTable = new SlideshowTable($this->getServiceLocator());
        $slideshow = $slideshowTable->getSlideshowByPosition($lang, $position);
        $viewModel->setVariable('slideshows', $slideshow);
        $viewModel->setTemplate('slideshow/index/' . $layout . '.phtml');
        return $viewModel;
	    
	}
	
	public function pageAction($id)
	{
	    $lang = $this->lang;
	    $slideshowTable = new SlideshowTable($this->getServiceLocator());
	    $slideshow = $slideshowTable->getSlideshowById($id);
	    $ViewModel = new ViewModel();
	    $ViewModel->setTemplate('slideshow/index/page.phtml');
	    if(count($slideshow) >0 ){
	        $ViewModel->setVariable('slideshows', $slideshow);
	        return $ViewModel;
	    }else{
	        //$this->layout()->errorMessage = __('There is no video in this video gallery!');
	    }
	
	}
}
