<?php 
namespace Slideshow\Controller;
   
use Zend\View\Model\ViewModel; 
use Slideshow\Model\SlideshowTable;
use Slideshow\Model\SlideshowImagesTable;
use Application\Helper\BaseAdminController;
use Zend\Session\Container;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Zend\Db\Adapter\Adapter;
use Language\Model\LanguageLanguagesTable;
use Slideshow\Form\SlideshowForm;
 
class AdminController extends BaseAdminController 
{ 		
	public function listSlideshowsAction()
	{
	    
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	    
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	     
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $slideshowTable = new SlideshowTable($this->getServiceLocator());
		$slideshows = $slideshowTable->fetchAllSlideshows();
	    $grid->setTitle(__('Slideshow List'));
	    $grid->setDefaultItemsPerPage(10);
	    $grid->setDataSource($slideshows,$dbAdapter);
	    
	  
	    $col = new Column\Select('id');
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->setWidth(5);
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('name');
	    $col->setLabel(__('Name'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	     
	    $col = new Column\Select('position');
	    $col->setLabel(__('Position'));
	    $col->setWidth(20);
	    $col->setSortDefault(1, 'ASC');
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('lang');
	    $col->setLabel(__('Lang'));
	    $col->setWidth(10);
	    $col->setSortDefault(1, 'DESC');
	    $grid->addColumn($col);
	     
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction = new Column\Action\Icon();
	    $viewAction->setIconClass('glyphicon glyphicon-edit');
	    $viewAction->setLink('/' . $this->lang . '/admin-slideshow/edit-slideshow/' . $rowId);
	    $viewAction->setTooltipTitle(__('Edit'));
	    
	    $viewAction2 = new Column\Action\Icon();
	    $viewAction2->setIconClass('glyphicon glyphicon-remove');
	    $viewAction2->setLink('/' . $this->lang . '/admin-slideshow/delete-slideshow/'.$rowId.'/token/'.$csrf);
	    $viewAction2->setTooltipTitle(__('Delete'));
	     
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction);
	    $actions2->addAction($viewAction2);
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	    $link[] = '<a href="/' . $this->lang . '/admin-slideshow/add-slideshow" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
	    $grid->setLink($link);
	     
	    $grid->render();
	    
	    return $grid->getResponse();
	
	}
	
	public function addSlideshowAction()
	{
	    $this->setHeadTitle(__('Add new slideshow'));
		$view = array();
		$validationExt ="jpg,jpeg,png,gif,mp4";
		$config = $this->serviceLocator->get('Config');
		$uploadDir = $config['base_route'] . "/uploads/slideshow/";
		
		$request = $this->getRequest();
		
		$languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		$allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
		$slideshowForm = new SlideshowForm($allActiveLanguages);
		
		if ($request->isPost()) {
			$validData = $request->getPost();
			try {
			    $slideshowTable = new SlideshowTable($this->getServiceLocator());
				$slideshowId = $slideshowTable->addSlideshow($validData);
				if ($slideshowId) {
					$view['uploadDir'] = $uploadDir . $slideshowId ;
					if (!is_dir($uploadDir)) {
						mkdir($uploadDir);
					}
					$files =  $request->getFiles()->toArray();
					$hasError = false;
					$notUploadedFiles = "Uploaded files: " ;
					foreach ($files["file"] as $k => $file) {
						if ($file['tmp_name'] != '') {
							$newFileName = md5(time() . $file['name']);
							$isUploaded = $this->imageUploader()->UploadFile($file,$validationExt, $uploadDir, $newFileName);
							 
							if ($isUploaded[0] == true) {
								$imageData = array();
								$imageData["caption"] = $validData["caption"][$k];
								$imageData["url"] = $validData["url"][$k];
								$imageData["image"] = $isUploaded[1];
								$imageData["background_color"] = $validData["background_color"][$k];
								$imageData["width"] = $validData["width"][$k];
								$imageData["height"] = $validData["height"][$k];
								$imageData["data_in"] = $validData["data_in"][$k];
								$imageData["description"] = $validData["description"][$k];
								$slideshowImageTable = new SlideshowImagesTable($this->getServiceLocator());
								$slideshowImageTable->addImage($slideshowId, $imageData);
							} else {
								$hasError = true;
								$notUploadedFiles  .= "<br>" . $file["name"];
							}
						}
					}
					if ($hasError) {
						$view['error'] = $notUploadedFiles;
					} else {
						
						
						if (isset($_POST['submit'])) {
						    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
						    return $this->redirect()->toRoute('slideshow-admin');
						} else {
						    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
						    return $this->redirect()->toRoute('slideshow-admin', array(
						        'action' => 'add-slideshow'
						    ));
						}
					}
				} else {
					$this->layout()->errorMessage = __("Operation failed!");
				}
			} catch (\Exception $e) {
				$this->layout()->errorMessage = __("The name is taken, please choose another one.");
			}
		}
		$view["slideshowForm"] = $slideshowForm;
		return new ViewModel($view);
	}
	
	public function editSlideshowAction()
	{
	    $this->setHeadTitle(__('Edit slideshow'));
		$validationExt ="jpg,jpeg,png,gif,mp4";
		$slideshowId = $this->params('id', -1);
		$view['slideshowId'] = $slideshowId;
		$slideshowTable = new SlideshowTable($this->getServiceLocator());
		$slideshowById = $slideshowTable->getSlideshowById($slideshowId);
		$view['slideshowById'] = $slideshowById;

		
		$request = $this->getRequest();
		$config = $this->serviceLocator->get('Config');
		$uploadDir = $config['base_route'] . "/uploads/slideshow/";
		$view['uploadDir'] = $uploadDir . $slideshowId ;
		
		$languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		$allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
		$slideshowForm = new SlideshowForm($allActiveLanguages);
		$slideshowForm->populateValues($slideshowById[0]);
		
		if ($request->isPost()) {
			 $validData = $request->getPost();
			 try {
			     $isEdited = $slideshowTable->editSlideshow($validData, $slideshowId);
			     if ($isEdited !== false) {
			         $slideshowImageTable = new SlideshowImagesTable($this->getServiceLocator());
			         if (isset($validData["captionExist"])) {
			             foreach ($validData["captionExist"] as $k => $caption) {
			                 $editData["urlExist"] = $validData["urlExist"][$k];
			                 $editData["background_color"] = $validData["backgroundExist"][$k];
			                 $editData["width"] = $validData["widthExist"][$k];
			                 $editData["height"] = $validData["heightExist"][$k];
			                 $editData["data_in"] = $validData["dataInExist"][$k];
			                 $editData["description"] = $validData["descExist"][$k];
			                 $slideshowImageTable->editSlideshowImage($k, $caption, $editData);
			             }
			         }
			     
			         if (!is_dir($uploadDir)) {
			             mkdir($uploadDir);
			         }
			         $files =  $request->getFiles()->toArray();
			         $hasError = false;
			         $notUploadedFiles = "Unuploaded files" ;
			         foreach ($files["file"] as $k => $file) {
			             if ($file['tmp_name'] != '') {
			                 $newFileName = md5(time() . $file['name']);
			                 $isUploaded = $this->imageUploader()->UploadFile($file,$validationExt, $uploadDir, $newFileName);
			                 	
			                 if ($isUploaded[0] == true) {
			                     $imageData = array();
			                     $imageData["caption"] = $validData["caption"][$k];
			                     $imageData["url"] = $validData["url"][$k];
			                     $imageData["image"] = $isUploaded[1];
			                     $imageData["background_color"] = $validData["background_color"][$k];
			                     $imageData["width"] = $validData["width"][$k];
			                     $imageData["height"] = $validData["height"][$k];
			                     $imageData["data_in"] = $validData["data_in"][$k];
			                     $imageData["description"] = $validData["description"][$k];
			                     $slideshowImageTable->addImage($slideshowId, $imageData);
			                 } else {
			                     $hasError = true;
			                     $notUploadedFiles  .= "<br>" . $file["name"];
			                 }
			             }
			         }
			         if ($hasError) {
			             $view['error'] = $notUploadedFiles;
			         } else {
			             $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
			             return $this->redirect()->toRoute('slideshow-admin');
			         }
			     }
			 } catch (\Exception $e) {
			     $this->layout()->errorMessage = __("The name is taken, please choose another one.");
			 }
		}
		$view["slideshowForm"] = $slideshowForm;
		return new ViewModel($view);
	}
	
	public function deleteSlideshowAction()
	{
	    $container = new Container('token');
	    $token = $this->params('token');
	    $slideshowId = $this->params('id');
	    $slideshowTable = new SlideshowTable($this->getServiceLocator());
	    $slideshowData = $slideshowTable->getSlideshowById($slideshowId);
	    if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
	        $isDeleted = $slideshowTable->deleteSlideshow($slideshowId);
	        if ($isDeleted) {
	            $config = $this->serviceLocator->get('Config');
	            $uploadDir = $config['base_route'] . "/uploads/slideshow/";
	            foreach ($slideshowData as $slideshowImage) {
    	            $imageRoute = $uploadDir . $slideshowImage['image'];
    	            if ($isDeleted) {
    	                @unlink($imageRoute);
    	            }
	            }
	            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	        } else {
	            $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
	        }
	    }
	    return $this->redirect()->toRoute('slideshow-admin');
	}

}

