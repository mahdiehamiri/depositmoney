<?php

namespace Slideshow\Controller;

use Slideshow\Model\SlideshowImagesTable;
use Application\Helper\BaseAdminController;

class AjaxController extends BaseAdminController
{
// 	public $viewModel;	
// 	public function onDispatch(MvcEvent $e)
// 	{
// 	    parent::onDispatch($e);
// 		$this->layout("layout/empty.phtml");
// 		$authenticationService = $this->getServiceLocator()->get('AuthenticationService');
// 		$this->userData = $authenticationService->isLoggedIn();
// 		if (!$this->userData) {
// 			return $this->redirect()->toRoute('home');
// 		}
// 		$hasPermission = $this->permissions()->getPermissions('slideshow-ajax-' . $this->params('action'));
// 		if (!$hasPermission) {
// 			return $this->redirect()->toRoute('signin');
// 		}
// 		$this->layout()->userPermissions = $this->permissions()->getPermissions();
// 		$this->viewModel = new ViewModel();
// 		$this->viewModel->setTerminal(true);
		
// 	}
	
	
	public function deleteImageAction()
	{
		$request = $this->getRequest();
		$config = $this->serviceLocator->get('Config');
		$uploadDir = $config['base_route'] . "/uploads/slideshow/";
		if ($request->isPost()) {
			$data = $request->getPost();
			$imageId = $data['id'];
			$slideshowImageTable = new SlideshowImagesTable($this->getServiceLocator());
			$imageInfo = $slideshowImageTable->getImageInfo($imageId);
// 			$imageRoute = './public/images/slideshow/' . $imageInfo[0]['slideshow_id'] . '/' . $imageInfo[0]['image'];
			$imageRoute = $uploadDir . $imageInfo[0]['image'];
			$isDeleted = $slideshowImageTable->deleteImage($imageId);
			if ($isDeleted) {
				@unlink($imageRoute);
					die("true");
			} else {
			    die("false");
			}
		}
	}
}