<?php 
namespace Slideshow\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModelInterface;
use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class SlideshowTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('slideshow', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function fetchAllSlideshows()
	{
		$select = new Select('slideshow');
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getSlideshowById($id = null)
	{
	    $select = new Select('slideshow');
	    $select->where->equalTo('slideshow.id', $id);
	    $select->join('slideshow_images', 'slideshow_images.slideshow_id = slideshow.id',array(
	        '*',
	        'image',
	        'url',
	        'caption',
	        'slideshow_id',
	        'background_color',
	        'width',
	        'height',
	        'data_in',
	        'description',
	    ), $select::JOIN_LEFT);
	    
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getSlideshowByPosition($lang, $position)
	{
		$select = new Select('slideshow');
		$select->where->equalTo('slideshow.lang', $lang);
		$select->where->equalTo('slideshow.position', $position);
		
		$select->join('slideshow_images', 'slideshow_images.slideshow_id = slideshow.id',array(
				'id',
				'image', 
				'url',
				'caption',
				'background_color',
				'width',
				'height',
				'data_in',
				'description',
		),$select::JOIN_LEFT);
		return $this->tableGateway->selectWith($select)->toArray();
	}

	public function addSlideshow($validData)
	{
		$result = $this->tableGateway->insert(array(
				'name' => $validData['name'],
		        'position' => $validData['position'],
				'lang' => $validData['lang']
		));
		if ($result) {
			return $this->tableGateway->lastInsertValue;
		} else {
			return false;
		}
	}
	
	public function editSlideshow($validData, $slideshowId)
	{
		if ($slideshowId > 0 && is_numeric($slideshowId)) {
			return $this->tableGateway->update(array(
			        'name' => $validData['name'],
			        'position' => $validData['position'],
					'lang' => $validData['lang']
			), array('id' => $slideshowId));
		}
	}
	
	public function deleteSlideshow($slideshowId)
	{
		return $this->tableGateway->delete(array(
				'id' => $slideshowId
		));
	}
}
