<?php 
namespace Slideshow\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModelInterface;
use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class SlideshowImagesTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('slideshow_images', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getImageInfo($imageId)
	{
		$select = new Select('slideshow_images');
		$select->where(array(
				'id' => $imageId
		));
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function deleteImage($imageId)
	{
		if ($imageId) {
			return $this->tableGateway->delete(array(
					'id' => $imageId
			));
		}
	}
	
	public function editSlideshowImage($slideShowImageId, $caption, $validData)
	{
		$data = array(
			"caption" => $caption,
		    "url"     => $validData['urlExist'],
		    "background_color"     => $validData['background_color'],
		    "width"     => $validData['width'],
		    "height"     => $validData['height'],
		    "data_in"     => $validData['data_in'],
		    "description"     => $validData['description'],
		);
		return $this->tableGateway->update($data, array("id" => $slideShowImageId));
	}
	
	public function addImage($slideShowId, $imageData)
	{
		$data = array(
			"slideshow_id" => $slideShowId,
			"caption" => $imageData["caption"],
			"url" => $imageData["url"],
			"image" => $imageData["image"],
			"background_color" => $imageData["background_color"],
			"width" => $imageData["width"],
			"height" => $imageData["height"],
			"data_in" => $imageData["data_in"],
			"description" => $imageData["description"],
		);
		return $this->tableGateway->insert($data);
	}
}
