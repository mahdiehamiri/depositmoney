<?php 

namespace Slideshow\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class SlideshowForm extends Form
{
	public function __construct($allActiveLanguages)
	{
		parent::__construct('slideshowForm');
		$this->setAttributes(array(
				'action'    => '',
				'method'    => 'post',
		        'enctype'   =>  'multipart/form-data',
    		    'id'      => 'slideshowForm',
    		    'novalidate'=> true
		));

		$name = new Element\Text('name');
		$name->setAttributes(array(
				'id'    	  => 'name',
				'class' 	  => 'form-control  validate[required]',
		        'required' => 'required',
		        'placeholder' => __('Slideshow Name'),
		));
		$name->setLabel(__("Slideshow Name"));

		$slideshowPosition = new Element\Text('position');
		$slideshowPosition->setAttributes(array(
		    'id'    	  => 'position',
		    'class' 	  => 'form-control  validate[required]',
		    'required' => 'required',
		    'placeholder' => __('Position'),
		));
		$slideshowPosition->setLabel(__("Position"));
			
		$slideshowLang = new Element\Select('lang');
		$slideshowLang->setAttributes(array(
		    'id' => 'slideshow_lang',
		    'class' => 'form-control',
		));
		$languages = array();
		if ($allActiveLanguages) {
		    foreach ($allActiveLanguages as $language) {
		        $languages[$language['code']] = $language['title'];
		    }
		}
		$slideshowLang->setValueOptions($languages);
		$slideshowLang->setLabel(__("Choose language"));
		
		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save and Close"));
		$submit->setAttributes(array(
				'id'    => 'update-profile',
				'class' => 'btn btn-primary'
		));
		
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(__("Save and New"));
		$submit2->setAttributes(array(
		    'class' => 'btn btn-primary',
		    'id'    =>  'submit'
		));

		$this->add($name)
		     ->add($slideshowPosition)
		     ->add($slideshowLang)
			 ->add($csrf)
			 ->add($submit2)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
		    'name'     => 'position',
		    'required' => true,
		    'filters' => array(
		        array(
		            'name' => 'StripTags'
		        )
		    )
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}