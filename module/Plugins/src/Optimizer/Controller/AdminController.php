<?php

namespace Optimizer\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Application\Form\LoginForm;
use Application\Form\RegistrationForm;
use Application\Form\ForgotPasswordForm;
use Application\Form\ResetPasswordForm;
use Application\Form\UserAccountForm;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Session\Container;
use Application\Form\QuotationForm;
use Application\Model\UsersTable;
use Application\Model\OptimizersTable;
use Tutorial\Model\ProjectsTable;
use Blog\Model\BlogPostTable;
use Jobs\Model\JobsCompaniesTable;
use Jobs\Model\JobsJobVacanciesTable;
use Application\Model\QuotationsTable;
use Application\Model\TeamMembersTable;
use Application\Form\TeamMembersForm;
use Application\Model\OrderTable;
use Application\Helper\BaseAdminController;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Filter;

class AdminController extends BaseAdminController {
	protected $noOfOptimizersPerPage = 40;
	protected $pathes = array ();
	protected $directories = array ();
	protected $ImageFolder = 'uploads/'; // Source Image Directory End with Slash
	protected $MoveImgeToHere = 'uploads/'; // Destination Image Directory End with Slash
	protected $PicWidth = 100; // New Width of Image
	protected $PicHeight = 100; // New Height of Image
	protected $ImageQuality = 70; // Image Quality
	public function indexAction() {
		$request = $this->getRequest ();
		if ($request->isPost ()) {
			$directory = $_SERVER ['DOCUMENT_ROOT'] . "/uploads";
			$arr = $this->getAllSubdirectories ( $directory );
			if ($this->pathes) {
				foreach ( $this->pathes as $key => $ImageFolder ) {
					if ($dir = opendir ( $ImageFolder )) {
						while ( ($file = readdir ( $dir )) !== false ) {
							$imagePath = $ImageFolder . '/' . $file;
							$destPath = $ImageFolder . $file;
							$checkValidImage = @getimagesize ( $imagePath );
							// var_dump( $imagePath ,$checkValidImage);
							if (file_exists ( $imagePath ) && $checkValidImage) { // Continue only if 2 given parameters are true
								$this->resizeImage ( $imagePath, $imagePath, 500, 500, 80 );
							}
						}
					}
				}
			}
		} 
}
function resizeImage($SrcImage, $DestImage, $MaxWidth, $MaxHeight, $ImageQuality) {
	list ( $iWidth, $iHeight, $type ) = getimagesize ( $SrcImage );
	
	$ImageScale = min ( $iWidth / $iWidth, $iHeight / $iHeight );
	$NewWidth = ceil ( $ImageScale * $iWidth );
	$NewHeight = ceil ( $ImageScale * $iHeight );
	$NewCanves = imagecreatetruecolor ( $NewWidth, $NewHeight );
	
	switch (strtolower ( image_type_to_mime_type ( $type ) )) {
		case 'image/jpeg' :
			$NewImage = imagecreatefromjpeg ( $SrcImage );
			break;
		case 'image/JPEG' :
			$NewImage = imagecreatefromjpeg ( $SrcImage );
			break;
		case 'image/png' :
			$NewImage = imagecreatefrompng ( $SrcImage );
			break;
		case 'image/PNG' :
			$NewImage = imagecreatefrompng ( $SrcImage );
			break;
		case 'image/gif' :
			$NewImage = imagecreatefromgif ( $SrcImage );
			break;
		default :
			return false;
	}
	
	// Resize Image
	if (imagecopyresampled ( $NewCanves, $NewImage, 0, 0, 0, 0, $NewWidth, $NewHeight, $iWidth, $iHeight )) {
		// copy file
		if (imagejpeg ( $NewCanves, $DestImage, $ImageQuality )) {
			imagedestroy ( $NewCanves );
			return true;
		}
	}
}
function getAllSubdirectories($base) {
	if (! is_dir ( $base )) {
		
		return $this->directories;
	}
	if ($dh = opendir ( $base )) {
		while ( ($file = readdir ( $dh )) !== false ) {
			$this->pathes [$base] = $base;
			if ($file == '.' || $file == '..')
				continue;
			if (is_dir ( $base . '/' . $file )) {
				$this->directories [] = $file;
				self::getAllSubdirectories ( $base . '/' . $file );
			} else {
				
				self::getAllSubdirectories ( $base . '/' . $file );
			}
		}
		closedir ( $dh );
	}
}
}   
