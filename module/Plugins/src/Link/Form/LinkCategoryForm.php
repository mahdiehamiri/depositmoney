<?php
namespace Link\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class LinkCategoryForm extends Form
{

    public function __construct()
    {
        parent::__construct('linkForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'novalidate' => true,
            'id' => 'linkCatForm'
        ));
        
        $title = new Element\Text('name');
        $title->setAttributes(array(
            'id' => 'cat-name',
            'class' => 'form-control validate[required]',
            'placeholder' => __('Name'),
            'required' => 'required'
        ));
        $title->setLabel(__('Name'));
        
        $linkPosition = new Element\Text('link_position');
        $linkPosition->setAttributes(array(
            'id' => 'link-position',
            'class' => 'form-control validate[required]',
            'placeholder' => __('Position'),
            'required' => 'required'
        ));
        $linkPosition->setLabel(__('Position'));
        
        $csrf = new Element\Csrf('linkcatcsrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__('Save'));
        $submit->setAttributes(array(
            'id' => 'submitCatLink',
            'class' => 'btn btn-primary submitCatLink'
        ));
        
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__("Save and New"));
        $submit2->setAttributes(array(
            'class' => 'btn btn-primary',
            'id' => 'submit'
        ));
        
        $this->add($title)
            ->add($linkPosition)
            ->add($csrf)
            ->add($submit2)
            ->add($submit);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        $inputFilter->add($factory->createInput(array(
            'name' => 'link_position',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
