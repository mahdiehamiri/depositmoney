<?php 

namespace Link\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class LinkForm extends Form
{
	public function __construct($allActiveLanguages)
	{
		parent::__construct('linkForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
		        'id'      => 'linkForm',
		        'novalidate'=> true
		));

		$title = new Element\Text('title');
		$title->setAttributes(array(
		    'id'    	  => 'title',
		    'class' 	  => 'form-control validate[required]',
		    'placeholder' => __('Title'),
		    'required' => 'required'
		));
		$title->setLabel(__('Title'));
		
		$description = new Element\Textarea('description');
		$description->setAttributes(array(
		    'id'    	  => 'description',
		    'class' 	  => 'form-control ',
		    'placeholder' => __('Description'),
		    //'required' => 'required'
		));
		$description->setLabel(__('Description'));
		
		$order = new Element\Text('order');
		$order->setAttributes(array(
		    'id'    	  => 'order',
		    'class' 	  => 'form-control ',
		    'placeholder' => __('Order'),
		    //'required' => 'required'
		));
		$order->setLabel(__('Order'));
		
		$icon = new Element\Text('icon');
		$icon->setAttributes(array(
		    'id'    	  => 'icon',
		    'class' 	  => 'form-control',
		    'placeholder' => 'fa fa-twitter',
		    'placeholder'  => __("Icon class")
		));
		$icon->setLabel(__('Icon class'));
		
		$lang = new Element\Select('lang');
		$lang->setAttributes(array(
		    'id'    	  => 'lang',
		    'class' 	  => 'form-control validate[required]',
		    'required' => 'required'
		));
		$languages = array();
		if ($allActiveLanguages) {
		    foreach ($allActiveLanguages as $language) {
		        $languages[$language['code']] = $language['title'];
		    }
		}
		$lang->setValueOptions($languages);
		$lang->setLabel(__('Language'));

		$url = new Element\Text('url');
		$url->setAttributes(array(
				'id'    	  => 'url',
				'class' 	  => 'form-control validate[required]',
				'placeholder' => 'http://www.facebook.com/cadafzar',
		        'required' => 'required'
		));
		$url->setLabel(__('link'));
		
		$image = new Element\File('image');
		$image->setAttributes(array(
		    'id'    => 'fileUpload1'
		));
		$image->setLabel(__("Choose image"));
		
		
		$csrf = new Element\Csrf('linkcsrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__('Save'));
		$submit->setAttributes(array(
				'id'    => 'submitLink',
				'class' => 'btn btn-primary'
		));

		$this->add($title)
		     ->add($icon)
		     ->add($image)
		     ->add($lang)
		     ->add($description)
		     ->add($order)
			 ->add($url)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'image',
				'required' => false,
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'url',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
