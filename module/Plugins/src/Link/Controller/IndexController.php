<?php
namespace Link\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Link\Form\LinkForm;
use Link\Model\LinkTable;
use Link\Model\LinkCategoryTable;
use Application\Helper\BasePluginController;

class IndexController extends BasePluginController
{

    protected $linkTable;

    public function onDispatch(MvcEvent $e)
    {
        parent::onDispatch($e);
    }

    public function indexAction($params)
    {
        $lang = $this->lang;
        $link = '';
        if (isset($params)) {
            $layout = is_array($params) ? $params['layout'] : 'index';
        } else {
            $layout = 'index';
        }
        // $layout = 'index';
        $viewModel = new ViewModel();
        $linkCategoryTable = new LinkCategoryTable($this->getServiceLocator());
        $id = $linkCategoryTable->getLinkByPosition($params['position']);
        if (isset($id[0]['id'])) {
            $linkTable = new LinkTable($this->getServiceLocator());
            $link = $linkTable->getLinkChildById($id[0]['id'], $lang);
            } 
            $viewModel->setVariable('links', $link);
            $viewModel->setVariable("params", $params);
            $viewModel->setTemplate('link/index/' . $layout . '.phtml');
            return $viewModel;
       
    }
}
