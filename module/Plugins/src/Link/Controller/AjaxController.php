<?php

namespace Link\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Application\Model\ApplicationFileManagerTable;
use Application\Helper\BaseController;
use Shop\Model\EducationLessonsTable;
use Shop\Model\ShopProductsTable;
use Application\Helper\BaseAdminController;
use Link\Model\LinkTable;

class AjaxController extends BaseAdminController
{	
  
   
	public function deleteImageAction()
	{
		$request = $this->getRequest();
		if ($request->isPost()) {
			$linkTable = new LinkTable($this->getServiceLocator());
			$data = $request->getPost()->toArray();
			$imageName = $linkTable->getLinkById($data['linkId']);
			if ($imageName)
			{
				foreach ($imageName as $image) {
					$isDeleted = $linkTable->deleteImage($data['linkId']);
					if ($isDeleted) {
						$fileToDelete = $_SERVER['DOCUMENT_ROOT'] . "/uploads/links/" . $imageName[0]['image'];
						@unlink($fileToDelete);
						
						die("1");
					} else {
						die("0");
					}
				}
			} else {
				die("0");
			}
			die("0");
		}
	}

}
