<?php

namespace Link\Controller;
   
use Zend\View\Model\ViewModel; 
use Link\Form\LinkForm;
use Zend\Session\Container;
use Link\Model\LinkTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Link\Form\LinkCategoryForm;
use Link\Model\LinkCategoryTable;
use Language\Model\LanguageLanguagesTable;

class AdminController extends BaseAdminController 
{ 	
    public function listLinksCategoryAction()
    {
    
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        	
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
    
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $linkCategoryTable = new LinkCategoryTable($this->getServiceLocator());
        $links = $linkCategoryTable->getAllLinkList();
        $grid->setTitle(__('Links category list'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($links,$dbAdapter);
    
        $col = new Column\Select('id',"link_category");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Id'));
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
    
        $col = new Column\Select('name');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('link_position');
        $col->setLabel(__('Position'));
        $grid->addColumn($col);
    
        	
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-list');
        $viewAction->setLink("/". $this->lang . '/admin-link/list-links/' . $rowId);
        $viewAction->setTooltipTitle(__('Show and add sub links'));
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/". $this->lang . '/admin-link/edit-link-category/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit link'));
    
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/". $this->lang . '/admin-link/delete-link-category/'.$rowId.'/token/'.$csrf);
        $viewAction2->setTooltipTitle(__('Delete link'));
        	
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
       
        $link[] = '<a href='."/". $this->lang . '/admin-link/add-link-category class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($link);
        	
        $grid->render();
        return $grid->getResponse();
    }
    
    public function addLinkCategoryAction()
    {
        $config = $this->serviceLocator->get('Config');
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
        $linkForm = new LinkCategoryForm($allActiveLanguages);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $linkForm->setData($data);
            if ($linkForm->isValid()) {
                $validData = $linkForm->getData(); 
                $toSaveItems = array();

                    $item = array(
                        'name'   => $validData['name'],
                        'link_position'   => $validData['link_position'],
                    );
                $linkTable = new LinkCategoryTable($this->getServiceLocator());
               
                try{
                    $isAdded = $linkTable->addLink($item);
                    if ($isAdded) {
                        
                        
                        if (isset($_POST['submit'])) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('link-admin');
                        } else {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('link-admin', array(
                                'action' => 'add-link-category'
                            ));
                        }
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                }catch (\Exception $e){
                    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                }
            } else {
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($linkForm, true);
            }
        }
        $view['linkForm'] = $linkForm;
        return new ViewModel($view);
    }
    
    public function editLinkCategoryAction()
    {
        $config = $this->serviceLocator->get('Config');
        $linkForm = new LinkCategoryForm();
        
        $linkId = $this->params('id', -1);
        $linkctegoryTable = new LinkCategoryTable($this->getServiceLocator());
        $linkData = $linkctegoryTable->getLinkById($linkId);
        //$view['imageInfo'] = $linkData;
        $linkForm->populateValues($linkData[0]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
    
            $linkForm->setData($data);
            if ($linkForm->isValid()) {
                $validData = $linkForm->getData();
                //var_dump($validData); die;
                $item = array();

                    $item = array(
                        'name'   => $validData['name'],
                        'link_position'   => $validData['link_position'],
                    );
                if ($linkId > 0 && is_numeric($linkId)) {
                    try{
                        $isEditted = $linkctegoryTable->editLink($item, $linkId);
                        if ($isEditted) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('link-admin');
                        } else {
                            $this->layout()->errorMessage = __("Operation failed!");
                        }
                    }catch(\Exception $e){
                        $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
                    }
                }
            } else {
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($linkForm, true);
            }
        } else {
            // 			var_dump($galleryForm->getMessages());
        }
        $view['linkForm'] = $linkForm;
        return new ViewModel($view);
    }
    
    public function deleteLinkCategoryAction()
    {
        $config = $this->serviceLocator->get('Config');
        $container = new Container('token');
        $token = $this->params('token');
        $linkId = $this->params('id');
        $linkTable = new LinkTable($this->getServiceLocator());
        $linkById = $linkTable->getLinkChildById($linkId, $this->lang);
       // var_dump($linkById); die;
        if(!empty($linkById)){
            $this->flashMessenger()->addErrorMessage(__("This Link has child. delete theme before."));
            
        }else{
            $linkCategoryTable = new LinkCategoryTable($this->getServiceLocator());
 
            if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
                $isDeleted = $linkCategoryTable->deleteLink($linkId);
                 if ($isDeleted) {
                 $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                } else { 
                 $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
                }
            }
        }
        //return $this->redirect()->toRoute('link-admin');
        return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
    }
    
	public function listLinksAction()
	{

		$contanier = new Container('token');
		$csrf = md5(time() . rand());
		$contanier->csrf = $csrf;
		$id = $this->params('id');
		//var_dump($id); die;
		$grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
		
		$config = $this->getServiceLocator()->get('Config');
		$dbAdapter = new Adapter($config['db']);
		$linkTable = new LinkTable($this->getServiceLocator());
		$link = $linkTable->getAllLinkList($id);
		$grid->setTitle(__('Links list'));
		$grid->setDefaultItemsPerPage(15);
		$grid->setDataSource($link,$dbAdapter);
		
		$col = new Column\Select('id',"link");
		$col->setLabel(__('Row'));
		$col->setIdentity();
		$col->addStyle(new Style\Bold());
		$grid->addColumn($col);
		
		$col = new Column\Select('title');
		$col->setLabel(__('Title'));

		$col->addStyle(new Style\Bold());
		$grid->addColumn($col);
		
		$col = new Column\Select('lang');
		$col->setLabel(__('Language'));
		$col->addStyle(new Style\Bold());
		$options = array('fa' => 'fa', 'en' => 'en');
		$col->setFilterSelectOptions($options);
		$grid->addColumn($col);
		
		 
		$btn = new Column\Action\Button();
		$btn->setLabel(__('Edit'));
		$rowId = $btn->getRowIdPlaceholder();
		 
		$viewAction1 = new Column\Action\Icon();
		$viewAction1->setIconClass('glyphicon glyphicon-edit');
		$viewAction1->setLink("/". $this->lang . '/admin-link/edit-link/' . $rowId);
		$viewAction1->setTooltipTitle(__('Edit'));
		
		$viewAction2 = new Column\Action\Icon();
		$viewAction2->setIconClass('glyphicon glyphicon-remove');
		$viewAction2->setLink("/". $this->lang . '/admin-link/delete-link/'.$rowId.'/token/'.$csrf);
		$viewAction2->setTooltipTitle(__('Delete'));
		
		$actions2 = new Column\Action();
		$actions2->setLabel(__('Operations'));
		$actions2->addAction($viewAction1);
		$actions2->addAction($viewAction2);
		$actions2->setWidth(15);
		$grid->addColumn($actions2);
		
		$linkAdd[] = '<a href='."/". $this->lang . '/admin-link/add-link/'.$id.' class="btn btn-primary">'.__("Add").'</a>';
		$grid->setLink($linkAdd);
		
        $grid->render();
		 
		return $grid->getResponse(); 
	}

	public function addLinkAction()
	{
	    $id = $this->params('id');
		$config = $this->serviceLocator->get('Config');
		$uploadDir = $config['base_route'] . "/uploads/links/";
		$validationExt ="jpg,jpeg,png,gif";
		
		$languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		$allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
		
		$linkForm = new LinkForm($allActiveLanguages);
		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost()->toArray();
			
			$files =  $request->getFiles()->toArray();
			//var_dump($files['image']['name']);
			if(!empty($files['image']['name'])){ 
			     $data = array_merge($data, $files);
			}
			$linkForm->setData($data);
			if ($linkForm->isValid()) {
				$validData = $linkForm->getData(); 
				$toSaveItems = array();
				if(!empty($files)){
    				//foreach ($files as $file) {
    					$newFileName = md5(time() . $validData['image']['name']);
    					$isUploaded = $this->imageUploader()->UploadFile( $validData['image'], $validationExt, $uploadDir, $newFileName);
    					$item = array(
    							'image'   => $isUploaded[1],
        					    'url'   => $validData['url'],
        					    'icon' => $validData['icon'],
        					    'title'     => $validData['title'],
    					        'description'     => $validData['description'],
    					        'lang'     => $validData['lang'],
    					        'order'     => $validData['order']
    					);
    					//$toSaveItems[] = $toSaveItem;
    				//} 
				}else{
    				 $item = array(
    						'url'   => $validData['url'],
    				        'icon' => $validData['icon'],
    						'title'     => $validData['title'],
    				        'description'     => $validData['description'],
    				        'lang'     => $validData['lang'],
    				        'order'     => $validData['order']
    				); 
				}
				$linkTable = new LinkTable($this->getServiceLocator());
				try{
    				$isAdded = $linkTable->addLink($item,$id);
    				if ($isAdded) {
    					$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
    					//return $this->redirect()->toRoute("link-admin");
    					return $this->redirect()->toUrl("/admin-link/list-links/".$id);
    						
    				} else {
    				    $this->layout()->errorMessage = __("Operation failed!");
    					}
				}catch(\Exception $e){
				    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
				}
			} else {
			    //var_dump($linkForm->getMessages()); die;
				$this->layout()->errorMessage = $this->generalhelper()->getFormErrors($linkForm, true);
			}
		}
		$view['linkForm'] = $linkForm;
		$view['cat_id'] = $id;
		return new ViewModel($view);
	}
	
	public function editLinkAction()
	{
		$config = $this->serviceLocator->get('Config');
		$uploadDir = $config['base_route'] . "/uploads/links/";
		$validationExt ="jpg,jpeg,png,gif";
	
		$languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		$allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1")); 
		
		$linkForm = new LinkForm($allActiveLanguages);
		$linkId = $this->params('id', -1);
		$linkTable = new LinkTable($this->getServiceLocator());
		$linkData = $linkTable->getLinkById($linkId);
		//var_dump($linkData); die;
		$view['imageInfo'] = $linkData;
		$linkForm->populateValues($linkData[0]);
		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost()->toArray();
			$files =  $request->getFiles()->toArray();
			$data = array_merge($data, $files);
				
			$linkForm->setData($data);
			if ($linkForm->isValid()) {
				$validData = $linkForm->getData();
				//var_dump($files); die;
				$item = array();
				if(!empty($validData['image']['name'])){
				 ///foreach ($files as $k => $file) {
					$newFileName = md5(time() . $validData['image']['name']);
					$isUploaded = $this->imageUploader()->UploadFile($validData['image'], $validationExt, $uploadDir, $newFileName);
					@unlink($uploadDir.$linkData[0]['image']);
					if ($isUploaded[0] == true) {
						
						//$item[$k] = $isUploaded[1];
					    $item = array(
					        'image'   => $isUploaded[1],
					        'url'   => $validData['url'],
					        'icon' => $validData['icon'],
					        'title'     => $validData['title'],
					        'lang'     => $validData['lang'],
					        'description'     => $validData['description'],
					        'order'     => $validData['order']
					    );
					}
				}else{ 
				    $item = array(
    						'url'   => $validData['url'],
    				        'icon' => $validData['icon'],
    						'title'     => $validData['title'],
				            'image'  => $linkData[0]['image'],
				            'lang'     => $validData['lang'],
				            'description'     => $validData['description'],
				            'order'     => $validData['order']
    				); 
				}
				if ($linkId > 0 && is_numeric($linkId)) {
				    try{
    				    $isEditted = $linkTable->editLink($item, $linkId);
    					if ($isEditted !== false) {
    						$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
    						return $this->redirect()->toUrl("/admin-link/list-links/".$linkData[0]['parent_id']);
    					} else {
    						$this->layout()->errorMessage = __("Operation failed!");
    					}
					}catch(\Exception $e){
					    $this->layout()->errorMessage = __("Duplicated name! please choose another one!");
					}
				}
			} else {
				$this->layout()->errorMessage = $this->generalhelper()->getFormErrors($linkForm, true);
			}
		} else {
			// 			var_dump($galleryForm->getMessages());
		}
		$view['id'] = $linkData[0]['parent_id'];
		$view['linkForm'] = $linkForm;
		return new ViewModel($view);
	}
	
	
	public function deleteLinkAction()
	{
		$config = $this->serviceLocator->get('Config');
		//$uploadDir = $config['base_route'] . "/images/link/";
		$container = new Container('token');
		$token = $this->params('token');
		$linkId = $this->params('id');
		$linkTable = new LinkTable($this->getServiceLocator());
		$linkById = $linkTable->getLinkById($linkId);
		if ($linkById && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
				$isDeleted = $linkTable->deleteLink($linkId);
			     if ($isDeleted) {
					//unlink($uploadDir . $linkById[0]['image']);
					$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
				} else { 
					$this->layout()->errorMessage = __("Operation failed!");
				}
		}
		//return $this->redirect()->toRoute('link-admin');
		return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
	}
}
