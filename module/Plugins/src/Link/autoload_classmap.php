<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
    'Link\Controller\AdminController' => __DIR__ . '/Controller/AdminController.php',
    'Link\Controller\IndexController' => __DIR__ . '/Controller/IndexController.php',
    'Link\Controller\AjaxController' => __DIR__ . '/Controller/AjaxController.php',
    'Link\Form\LinkCategoryForm'      => __DIR__ . '/Form/LinkCategoryForm.php',
    'Link\Form\LinkForm'              => __DIR__ . '/Form/LinkForm.php',
    'Link\Model\LinkCategoryTable'    => __DIR__ . '/Model/LinkCategoryTable.php',
    'Link\Model\LinkTable'            => __DIR__ . '/Model/LinkTable.php',
);
