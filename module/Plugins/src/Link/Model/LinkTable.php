<?php 
namespace Link\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class LinkTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('link', $this->getDbAdapter());
    }
    public function searchLink($product = false, $lang = "fa", $getPaginator = true)
    {
        $select = new Select('link');
        if ($product) {
            $select->where->nest()->like("title", "%$product%")
            ->OR->like('description', "%$product%") ->unnest();
            $select->where->equalTo("lang", $lang);
        }
    
        if ($getPaginator) {
            $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($dbSelector);
        } else {
            return $this->tableGateway->selectWith($select)->toArray();
        }
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllLink()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllLinkList($id)
	{
	    $select = new Select('link');
	    $select->where->equalTo('parent_id', $id);
	    $select->order('lang','id');
	    return $select;
	}
	
	public function getLinkById($linkId)
	{
		$select = new Select('link');
		$select->where->equalTo('id', $linkId);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	
	
	public function getLinkChildById($linkId, $lang)
	{
	 
	    $select = new Select('link');
	    $select->where->equalTo('parent_id', $linkId);
	    $select->where->equalTo('lang', $lang);
	    $select->order('order');
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addLink($data,$id)
	{
		return $this->tableGateway->insert(array(
				'url'   => $data['url'],
				'title' => $data['title'],
		        'image' => $data['image'],
		        'icon'  => $data['icon'],
		        'parent_id'  => $id,
		        'lang'       => $data['lang'],
		        'description' => $data['description'],
		        'order' => $data['order'],
		));
	}
	
	public function editLink($newData, $linkId)
	{
		return $this->tableGateway->update(array(
				'url'   => $newData['url'],
				'title' => $newData['title'],
		        'image' => $newData['image'],
		        'icon'  => $newData['icon'],
		        'lang'  => $newData['lang'],
		        'description' => $newData['description'],
		        'order'       => $newData['order'],
		), array('id' => $linkId));		
	}
	
	public function deleteImage($linkId)
	{
	    return $this->tableGateway->update(array(
	        'image'   => null
	    ), array('id' => $linkId));
	}
	
	public function deleteLink($linkId)
	{
		return $this->tableGateway->delete(array(
				'id' => $linkId
		));
	}
}
