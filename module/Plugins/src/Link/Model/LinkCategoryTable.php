<?php 
namespace Link\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class LinkCategoryTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('link_category', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getLinkByPosition($position)
	{
	    $select = new Select('link_category');
	    $select->where->equalTo('link_position', $position);
	    $select->columns(array('id'));
	    
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getAllLink()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllLinkList()
	{
	    $select = new Select('link_category');
	    return $select;
	}
	
	public function getLinkById($linkId)
	{
		$select = new Select('link_category');
		$select->where->equalTo('id', $linkId);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addLink($data)
	{
		return $this->tableGateway->insert(array(
				'name'         => $data['name'],
		        'link_position'=> $data['link_position'],
		));
	}
	
	public function editLink($newData, $linkId)
	{
		return $this->tableGateway->update(array(
				'name'         => $newData['name'],
		        'link_position'=> $newData['link_position'],

		), array('id' => $linkId));		
	}
	
	public function deleteLink($linkId)
	{
		return $this->tableGateway->delete(array(
				'id' => $linkId
		));
	}
}
