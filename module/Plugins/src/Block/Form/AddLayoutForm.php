<?php 

namespace Block\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class AddLayoutForm extends Form
{
	public function __construct()
	{
		parent::__construct('block_form');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		       
		));
		
		
		$name = new Element\Text('name');
		$name->setAttributes(array(
		    'class'    => 'form-control validate[required]',
	        'required' => 'required',
		    'id'    => 'layout-name',
		    'placeholder'  => __("Layout name")
		));
		$name->setLabel(__("Layout name"));
		
		$layout = new Element\Textarea('layout_style');
		$layout->setAttributes(array(
		    'class' => 'form-control validate[required]',
		    'required' => 'required',
		    'id'    => 'layout-style',
		    'placeholder'  => __("Layout style")
		));
		$layout->setLabel(__("Layout style"));
		
		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save"));
		$submit->setAttributes(array(
				'class' => 'btn btn-primary',
		        'id'    => 'save'
		));

		$this->add($name)
			 ->add($layout)
			 ->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'layout_style',
				'required' => true,
				
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
