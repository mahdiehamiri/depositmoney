<?php
namespace Block\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class BlockForm extends Form
{

    public function __construct($allActiveLanguages = null, $layouts = array(), $groupId = false)
    {
        parent::__construct('block_form');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'id' => 'blockForm',
            'novalidate' => true
        ));
        
        $blockType = new Element\Select('block_type');
        $blockType->setAttributes(array(
            'id' => 'block-type',
            'class' => 'form-control'
        ));
        if ($groupId && ($groupId > 1)) {
            $blockType->setAttributes(array(
                'class' => 'hidden '
            ));
            $blockType->setLabelAttributes(array(
                'class' => 'hidden'
            ));
        } else {
            $blockType->setAttributes(array(
                'class' => 'form-control validate[required] show',
                'required' => 'required'
            ));
        }
        $languages = array(
            'Simple' => 'Simple',
            'Entity' => 'Create block from entity'
        );
        $blockType->setValueOptions($languages);
        $blockType->setLabel(__("Choose Type"));
        
        $blockLang = new Element\Select('block_lang');
        $blockLang->setAttributes(array(
            'id' => 'page_lang',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $languages = array();
        if ($allActiveLanguages) {
            foreach ($allActiveLanguages as $language) {
                $languages[$language['code']] = $language['title'];
            }
        }
        $blockLang->setValueOptions($languages);
        $blockLang->setLabel(__("Choose language"));
        
        $blockPosition = new Element\Text('block_position');
        $blockPosition->setAttributes(array(
            'class' => 'form-control  validate[required]',
            'required' => 'required',
            'placeholder' => __("Position")
        ));
        $blockPosition->setLabel(__("Position"));
        
        $blockContent = new Element\Textarea('block_content');
        $blockContent->setAttributes(array(
            'class' => 'form-control  ',
            'id' => 'block-content',
            'placeholder' => __("Content")
            // 'required' => 'required'
        ));
        $blockContent->setLabel(__("Content"));
        
        $layout = new Element\Select('layout');
        $layout->setAttributes(array(
            'id' => 'page-layout',
            'class' => 'form-control'
        
        ));
        $layout->setValueOptions($layouts);
        $layout->setValue('default');
        $layout->setLabel(__("Choose Layout"));
        
        $blockKind = new Element\Select('block_kind');
        $blockKind->setAttributes(array(
            'id' => 'block-kind',
            'class' => 'form-control '
            // 'required' => 'required'
        ));
        $blockKind->setValueOptions(array(
            '' => "",
            'educationfs' => __('Cadafzar'),
            'blogp' => __('News'),
            'user' => __('Users'),
            'newestrealproducts' => __('Newest real products'),
            'newestvirtualproducts' => __('Newest virtual products'),
            'bestsellrealproducts' => __('Best sell real products'),
            'bestsellvirtualproducts' => __('Best sell virtual products'),
            'similarproducts' => __('Similar products'),
            'priorityproducts' => __('Priority products'),
            'priorityinstructor' => __('Priority instructor'),
            'portf' => __('Students Files'),
            'similaredu' => __('Similar educations'),
            'dashboardnewestvirtualproducts' => __('Dashboard newest virtual products'),
            'dashboarduservideosorders' => __('Dashboard user videos orders'),
            'language' => __('Site languages link'),
            'link' => __('links')
        ));
        $blockKind->setLabel(__("Choose Kind"));
        
        $order = new Element\Select('order_by');
        $order->setAttributes(array(
            'id' => 'order',
            'class' => 'form-control'
        ));
        $order->setValueOptions(array(
            'ASC' => __('Oldest'),
            'DESC' => __('Newest')
            // 'Most_visited' => __('Most visited'),
            // 'users_file' => __('Students Files'),
        ));
        $order->setLabel(__("Order By"));
        
        $block_limit = new Element\Text('block_limit');
        $block_limit->setAttributes(array(
            'class' => 'form-control',
            'placeholder' => __("Quantity of items")
        ));
        $block_limit->setLabel(__("Quantity of items"));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__("Save and Close"));
        $submit->setAttributes(array(
            'class' => 'btn btn-primary',
            'id' => 'submit'
        ));
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__("Save and New"));
        $submit2->setAttributes(array(
            'class' => 'btn btn-primary',
            'id' => 'submit'
        ));
        
        $this->add($blockLang)
            ->add($blockType)
            ->add($blockPosition)
            ->add($blockContent)
            ->add($layout)
            ->add($blockKind)
            ->add($order)
            ->add($block_limit)
            ->add($csrf)
            ->add($submit2)
            ->add($submit);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'block_lang',
            'required' => true
        )));
        $inputFilter->add($factory->createInput(array(
            'name' => 'block_kind',
            'required' => false
        )));
        $inputFilter->add($factory->createInput(array(
            'name' => 'block_position',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
