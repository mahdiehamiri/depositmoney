<?php
namespace Block\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class BlockTable extends BaseModel
{

    protected $tableGateway;

    protected $adapter;

    protected $serviceManager;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('block', $this->getDbAdapter());
    }

    public function getRecords($id = false)
    {
        return $this->tableGateway->select()->toArray();
    }

    public function getAllBlock($groupId)
    {
        $select = new Select('block');
        $select->order('block_lang', 'id');
        if ($groupId != 1) {
            $select->where(array(
                'block.block_type' => 'Simple'
            ));
        }
        return $select;
    }

    public function getBlockByPostion($lang, $blockPosition)
    {
        $select = new Select('block');
        $select->where->equalTo('block_lang', $lang);
        $select->where->equalTo('block_position', $blockPosition);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getBlockByPostionEntity($blockPosition)
    {
        $select = new Select('block');
        $select->where->equalTo('block_position', $blockPosition);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getBlockType($blockPosition)
    {
        $select = new Select('block');
        $select->where->equalTo('block_position', $blockPosition);
        $select->columns(array(
            'block_type'
        ));
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getBlockById($blockId)
    {
        $select = new Select('block');
        $select->where->equalTo('id', $blockId);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function addBlock($data)
    {
        return $this->tableGateway->insert(array(
            'block_lang' => $data['block_lang'],
            'block_type' => $data['block_type'],
            'block_content' => $data['block_content'],
            'block_position' => $data['block_position'],
            'layout' => $data['layout'],
            'block_kind' => $data['block_kind'],
            'order_by' => $data['order_by'],
            'block_limit' => $data['block_limit']
        ));
    }

    public function editBlock($newData, $blockId)
    {
        return $this->tableGateway->update(array(
            'block_lang' => $newData['block_lang'],
            'block_type' => $newData['block_type'],
            'block_content' => $newData['block_content'],
            'block_position' => $newData['block_position'],
            'layout' => $newData['layout'],
            'block_kind' => $newData['block_kind'],
            'order_by' => $newData['order_by'],
            'block_limit' => $newData['block_limit']
        ), array(
            'id' => $blockId
        ));
    }

    public function deleteBlock($blockId)
    {
        return $this->tableGateway->delete(array(
            'id' => $blockId
        ));
    }
}
