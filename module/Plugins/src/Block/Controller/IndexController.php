<?php
namespace Block\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Block\Model\BlockTable;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Application\Helper\BasePluginController;

class IndexController extends BasePluginController 
{ 
	protected $linkTable;
	
	public function onDispatch(MvcEvent $e)
	{
		parent::onDispatch($e);
	}
	
	public function indexAction($params)
	{   
        $blockTable = new BlockTable($this->getServiceLocator());
        $lang = $this->lang;
        $blockType = $blockTable->getBlockType($params['position']);
        $view = array();
        $blockData = false;
        if ($blockType && $blockType[0]['block_type'] !== 'Entity') {
            $blockData = $blockTable->getBlockByPostion($lang, $params['position']);
            $view["blockContent"] = $blockData[0]["block_content"];
         
        } else if ($blockType) {
            $blockData = $blockTable->getBlockByPostionEntity( $params['position']);
        
            $table = $blockData[0]['block_kind'];
            $order = $blockData[0]['order_by'];
            $limit = $blockData[0]['block_limit'];
            $where = "";
            switch ($table) {
                case "user":
                    $table = "users";
                    $fields = ' username AS title, aboutme as description, avatar as image';
                    $order_by = 'user_registration_date';
                    $where = 'LEFT join users_data on users.id = users_data.user_id WHERE users.user_group_id = 4 and users_data.lang='.'\''.$lang.'\'';
                    break;
                case "blogp":
                    $table = "blog_posts";
                    $fields = 'post_title as title, post_exerpt as description, post_image as image, post_lang, post_slug, category_title';
                    $order_by = 'post_date';
                    $where = 'LEFT join blog_categories on blog_categories.id = blog_posts.post_category_id WHERE post_status = 1 and post_lang ='.'\''.$lang.'\'';
                    break;
                case "newestrealproducts":
                    $table = "shop_products";
                    $fields = 'id, name, seo_name, description, image, price, rating';
                    $order_by = 'created_time';
                    $where = 'WHERE status = 1 and lesson_id IS NULL and is_deleted = 0 and product_lang ='.'\''.$lang.'\'';
                    $view["title"] = __("Newest products");
                    break;
                case "newestvirtualproducts":
                    $table = "shop_products";
                    $fields = 'id, name, seo_name, description, image, price, rating';
                    $order_by = 'created_time';
                    $where = 'WHERE status = 1 and lesson_id IS NOT NULL and is_deleted = 0 and product_lang ='.'\''.$lang.'\'';
                    $view["title"] = __("Newest products");
                    break;
                case "bestsellrealproducts":
                    $table = "shop_products";
                    $fields = 'id, name, seo_name, description, image, price, rating';
                    $order_by = 'sell_count';
                    $where = 'WHERE status = 1 and lesson_id IS NULL and is_deleted = 0 and product_lang ='.'\''.$lang.'\'';
                    $view["title"] = __("Best sell");
                    break;
                case "bestsellvirtualproducts":
                    $table = "shop_products";
                    $fields = 'id, name, seo_name, description, image, price, rating';
                    $order_by = 'sell_count';
                    $where = 'WHERE status = 1 and lesson_id IS NOT NULL and is_deleted = 0 and product_lang ='.'\''.$lang.'\'';
                    $view["title"] = __("Best sell");
                    break;
                case "similarproducts":
                    $table = "shop_products";
                    $fields = 'id, name, seo_name, description, image, price, rating';
                    $order_by = 'sell_count';
                    $where = 'WHERE status = 1 and is_deleted = 0 and id !=' . '\'' . $params["productId"] . '\' and category_id =' . '\'' . $params["productCategoryId"] . '\' and product_lang =' . '\''.$lang.'\'';
                    $view["title"] = __("Similar products");
                    break;
                case "portf":
                    $table = "portfolio ";
                    $fields = 'portfolio.id, title, description, image, instructor_id, CONCAT(firstname, " ", lastname) AS username';
                    $order_by = 'upload_date';
                    $where = 'left join portfolio_details on portfolio.id = portfolio_details.portfolio_id LEFT join users_data on users_data.user_id = portfolio.instructor_id ' . 'WHERE users_data.lang ='.'\''.$lang.'\' and portfolio_details.lang ='.'\''.$lang.'\'';
                    break;
                case "educationfs":
                    $table = "education_fieldofstudy";
                    $fields = 'id,name as title, intro, description, image, icon';
                    $order_by = '\'order\'';
                    $where = 'WHERE status = 1 and lang ='.'\''.$lang.'\'';
                    break;
                case "priorityproducts":
                    $table = "shop_products";
                    $fields = 'id, name, seo_name, description, image, price, rating';
                    $where = 'WHERE status = 1 and product_lang ='.'\''.$lang.'\' and is_deleted = 0';
                    $order_by = 'priority';
                    break;
                case "priorityinstructor":
                    $table = "users ";
                    $fields = 'users.id, CONCAT(firstname, " ", lastname) as title, avatar as image, aboutme as description';
                    $order_by = 'RAND()';
                    $where = 'left join users_data on users.id = users_data.user_id WHERE users.user_group_id = 4 and status = 1 and lang ='.'\''.$lang.'\'';
                    break;
                case "similaredu":
                    $table = "education_course ";
                    $fields = 'course_title as title, image, start_date as description, id';
                    $order_by = 'RAND()';
                    $where = 'WHERE status = 1 and course_type = \'Real\' and education_lesson_id ='.'\''.$params["id"].'\'';
                    break;
                case "dashboarduservideosorders":
                    $table = "shop_product_video_files";
                    $fields = 'shop_products.*';
                    $order_by = 'RAND() ';
                    $where = 'left join shop_products on shop_products.id = shop_product_video_files.product_id left join orders_details on orders_details.product_id = shop_product_video_files.product_id left join orders on orders_details.order_id = orders.id WHERE orders.payment_status = 1 and orders.user_id =' . $params["id"] . " group by shop_product_video_files.product_id";
                    $view["title"] = __("User virtual orders");
                    break;
                case "dashboardnewestvirtualproducts":
                    $table = "shop_products";
                    $fields = 'id, name, seo_name, description, image, price, rating';
                    $order_by = 'created_time';
                    $where = 'WHERE status = 1 and lesson_id IS NOT NULL and is_deleted = 0 and product_lang ='.'\''.$lang.'\'';
                    $view["title"] = __("Newest products");
                    break;
                case "language":
                    $table = "language_languages";
                    $fields = 'id, title, code';
                    $order_by = 'id';
                    $where = 'WHERE enable = 1 and code !='.'\''.$lang.'\'';
                    $view["title"] = __("Languages");
                    break;
                case "link":
                    $table = "link_category";
                    $fields = 'title, url';
                    $order_by = 'link.id';
                    $where = 'left join link on link_category.id = link.parent_id WHERE link_position = \'my_links\' and lang ='.'\''.$lang.'\'';
                    $view["title"] = __("Links");
                    break;
            }
            $db = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $sql = "SELECT $fields FROM $table $where order by $order_by  $order LIMIT $limit";
            $statement = $db->query($sql);
            $res =  $statement->execute();
            
            if($res instanceof ResultInterface && $res->isQueryResult()){
                $resultSet = new ResultSet;
                $resultSet->initialize($res);
                $temp = array();
                foreach ($resultSet as $k => $v) {
                    $temp[$k] = $v; 
                }
                $view["blockContent"] = $temp;
            }
        } 
        if (!$blockData) {
            $view["blockContent"] = "";
            $blockData[0]['layout'] = "default";
        }
        //var_dump($blockData[0]['layout']); die;
        $viewModel = new ViewModel($view);
        $viewModel->setTemplate("block/index/".$blockData[0]['layout'].".phtml");
        //$viewModel->setTemplate("block/index/index.phtml");
        return $viewModel;
	        	
	
	}
}
