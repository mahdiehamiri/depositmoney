<?php
namespace Block\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Block\Form\BlockForm;
use Zend\Session\Container;
use Block\Model\BlockTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Language\Model\LanguageLanguagesTable;
use Block\Form\AddLayoutForm;

class AdminController extends BaseAdminController
{

    public function listBlocksAction()
    {
        $this->setHeadTitle(__('List blocks'));
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $blockTable = new BlockTable($this->getServiceLocator());
        $groupId = $this->userData->user_group_id;
        $blocks = $blockTable->getAllBlock($groupId);
        $grid->setTitle(__('Blok List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($blocks, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('block_position');
        $col->setLabel(__('Position'));
        $grid->addColumn($col);
        
        $col = new Column\Select('block_lang');
        $col->setLabel(__('Language'));
        $options = array(
            'fa' => 'fa',
            'en' => 'en'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            'fa' => 'fa',
            'en' => 'en'
        );
        $grid->addColumn($col);
        
        $col = new Column\Select('block_content');
        $col->setLabel(__('Content'));
        $grid->addColumn($col);
        
        $blockType = new Column\Select('block_type');
        $blockType->setLabel(__('Type'));
        $options = array(
            'Simple' => 'Simple',
            'Entity' => 'Create block from entity'
        );
        $blockType->setFilterSelectOptions($options);
        $replaces = array(
            'Simple' => 'Simple',
            'Entity' => 'Create block from entity'
        );
        $blockType->setReplaceValues($replaces, $blockType);
        $grid->addColumn($blockType);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink('/' . $this->lang . '/admin-block/edit-block/' . $rowId);
        $viewAction->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink('/' . $this->lang . '/admin-block/delete-block/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $block[] = '<a href="/' . $this->lang . '/admin-block/add-block" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $block[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($block);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addBlockAction()
    {
        $this->setHeadTitle(__('Add new block'));
        $layouts = $this->generalhelper()->getFilesFromDir(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "View" . DIRECTORY_SEPARATOR . "block" . DIRECTORY_SEPARATOR . "index");
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $groupId = $this->userData->user_group_id;
        $blockForm = new BlockForm($allActiveLanguages, $layouts, $groupId);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $blockForm->setData($data);
            if ($blockForm->isValid()) {
                $validData = $blockForm->getData();
                $blockTable = new BlockTable($this->getServiceLocator());
                $isAdded = $blockTable->addBlock($validData);
                if ($isAdded) {
                    if (isset($_POST['submit'])) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toRoute('block-admin');
                    } else {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toRoute('block-admin', array(
                            'action' => 'add-block'
                        ));
                    }
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            }
            // var_dump($blockForm->getMessages()); die;
        }
        $view['blockForm'] = $blockForm;
        return new ViewModel($view);
    }

    public function editBlockAction()
    {
        $this->setHeadTitle(__('Edit block'));
        $layouts = $this->generalhelper()->getFilesFromDir(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "View" . DIRECTORY_SEPARATOR . "block" . DIRECTORY_SEPARATOR . "index");
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $groupId = $this->userData->user_group_id;
        $blockForm = new BlockForm($allActiveLanguages, $layouts,$groupId);
        $blockId = $this->params('id', - 1);
        $blockTable = new BlockTable($this->getServiceLocator());
        $blockData = $blockTable->getBlockById($blockId);
        $view['imageInfo'] = $blockData;
        $blockForm->populateValues($blockData[0]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            
            $blockForm->setData($data);
            if ($blockForm->isValid()) {
                $validData = $blockForm->getData();
                if ($blockId > 0 && is_numeric($blockId)) {
                    $isEditted = $blockTable->editBlock($validData, $blockId);
                    if ($isEditted) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toRoute('block-admin');
                    } else {
                        $this->flashMessenger()->addSuccessMessage(__("No changes happend!"));
                        return $this->redirect()->toRoute('block-admin');
                    }
                }
            } else {
                $this->layout()->errorMessage = $this->generalhelper()->getFormErrors($blockForm, true);
            }
        } else {
            // var_dump($galleryForm->getMessages());
        }
        
        $view['blockForm'] = $blockForm;
        $view['type'] = $blockData[0]['block_type'];
        return new ViewModel($view);
    }

    public function deleteBlockAction()
    {
        $config = $this->serviceLocator->get('Config');
        
        $container = new Container('token');
        $token = $this->params('token');
        $blockId = $this->params('id');
        $blockTable = new BlockTable($this->getServiceLocator());
        $blockById = $blockTable->getBlockById($blockId);
        if ($blockById && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = $blockTable->deleteBlock($blockId);
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            }
        }
        return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
    }

    public function ListsLayoutAction()
    {
        /*
         * $contanier = new Container('token');
         * $csrf = md5(time() . rand());
         * $contanier->csrf = $csrf;
         *
         * $layouts = $this->generalhelper()->getFilesFromDir(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "View" . DIRECTORY_SEPARATOR . "block" . DIRECTORY_SEPARATOR . "index");
         * //var_dump($layouts); die;
         * foreach ($layouts as $lay){
         * $layout[]['name'] = $lay;
         * }
         *
         * $view['layouts'] = $layout;
         * $view['csrf'] = $csrf;
         * return new ViewModel($view);
         */
        $this->setHeadTitle(__('Main menus list'));
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        
        $layout = array();
        $layouts = $this->generalhelper()->getFilesFromDir(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "View" . DIRECTORY_SEPARATOR . "block" . DIRECTORY_SEPARATOR . "index");
        
        foreach ($layouts as $key => $lay) {
            $layout[$key]['name'] = $lay;
            $layout[$key]['id'] = $key;
        }
        
        $grid->setTitle(__('Layout list'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($layout, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-block/edit-layout/name/' . $rowId);
        $viewAction1->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-block/delete-layout/name/' . $rowId . '/token/' . $csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-menu/add-main-menu class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addLayoutAction()
    {
        $Form = new AddLayoutForm();
        $dir = __DIR__ . "/.." . "/view/block/index/";
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $Form->setData($data);
            if ($Form->isValid()) {
                $validData = $Form->getData();
                
                $new_layout = fopen($dir . $validData['name'] . ".phtml", "w");
                $txt = $validData['layout_style'];
                fwrite($new_layout, $txt);
                fclose($new_layout);
                if ($new_layout) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toUrl('/admin-block/lists-layout');
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            }
        }
        
        $view['Form'] = $Form;
        return new ViewModel($view);
    }

    public function editLayoutAction()
    {
        $name = $this->params('name');
        $Form = new AddLayoutForm();
        $dir = __DIR__ . "/.." . "/view/block/index/";
        $request = $this->getRequest();
        
        $current_info = @file_get_contents($dir . '/' . $name . '.phtml');
        
        if (! $current_info) {
            $this->flashMessenger()->addErrorMessage(__("This layout not exist."));
            return $this->redirect()->toUrl('/admin-block/lists-layout');
        }
        $data['name'] = $name;
        $data['layout_style'] = $current_info;
        $Form->populateValues($data);
        
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $Form->setData($data);
            
            if ($Form->isValid()) {
                $validData = $Form->getData();
                $new_layout = file_put_contents($dir . $validData['name'] . ".phtml", $validData['layout_style']);
                
                if ($new_layout) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toUrl('/admin-block/lists-layout');
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            }
        }
        
        $view['Form'] = $Form;
        return new ViewModel($view);
    }

    public function deleteLayoutAction()
    {
        $name = $this->params('name');
        $dir = __DIR__ . "/../../.." . "/view/block/index/";
        $container = new Container('token');
        $token = $this->params('token');
        $file = glob($dir . $name . '.phtml');
        if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = @unlink($file[0]);
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toUrl('/admin-block/lists-layout');
            } else {
                $this->layout()->errorMessage = __("Operation failed!");
            }
        }
    }
}
