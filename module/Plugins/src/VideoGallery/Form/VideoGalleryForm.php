<?php
namespace VideoGallery\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class VideoGalleryForm extends Form
{

    public function __construct($allActiveLanguages = array(), $editMode = null, $parents)
    {
        parent::__construct('videoGalleryForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'novalidate' => true,
            'id' => 'videoGalleryForm'
        ));
        
        $persian_name = new Element\Text('persian_name');
        $persian_name->setAttributes(array(
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'placeholder' => __('Persian name')
        ));
        $persian_name->setLabel(__("Persian name"));
        
        $latin_name = new Element\Text('latin_name');
        $latin_name->setAttributes(array(
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'placeholder' => __('Unique name')
        ));
        $latin_name->setLabel(__("Unique name"));
        
        $parent = new Element\Select('parent_id');
        
        $arrayParent = array(
            '0' => 'Main category'
        );
        
        foreach ($parents as $item) {
            $arrayParent[$item['id']] = $item['name'];
        }
        
        $parent->setValueOptions($arrayParent);
        $parent->setLabel(__("Main VideoGallery"));
        $parent->setAttributes(array(
            'class' => 'form-control'
        
        ));
        
        $videoGalleryLang = new Element\Select('lang');
        $videoGalleryLang->setAttributes(array(
            'id' => 'page_lang',
            'class' => 'form-control'
        ));
        $languages = array();
        if ($allActiveLanguages) {
            foreach ($allActiveLanguages as $language) {
                $languages[$language['code']] = $language['title'];
            }
        }
        $videoGalleryLang->setValueOptions($languages);
        $videoGalleryLang->setLabel(__("Choose language"));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(_("Save"));
        $submit->setAttributes(array(
            'id' => 'submit',
            'class' => 'btn btn-primary'
        ));
        
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__("Save and New"));
        $submit2->setAttributes(array(
            'class' => 'btn btn-primary',
            'id' => 'submit'
        ));
        
        $this->add($persian_name)
            ->add($latin_name)
            ->add($parent)
            ->add($videoGalleryLang)
            ->add($csrf)
            ->add($submit2)
			->add($submit);
        $this->inputFilter($editMode);
    }

    public function inputFilter($editMode)
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'persian_name',
            'required' => true
        )));
        if (! $editMode) {
            $inputFilter->add($factory->createInput(array(
                'name' => 'latin_name',
                'required' => true
            )));
        }
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
