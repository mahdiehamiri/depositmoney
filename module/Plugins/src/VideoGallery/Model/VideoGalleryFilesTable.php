<?php 

namespace VideoGallery\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class VideoGalleryFilesTable extends BaseModel 
{
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('video_gallery_files', $this->getDbAdapter());
    }
    
    public function getAllVideosById($id)
    {
        $select = new Select('video_gallery_files');
        $where = new Where();
        $select->where->equalTo('video_gallery_id', $id);
        return $this->tableGateway->selectWith($select)->toArray();
    }
	
    public function getAllVideosByIdForPage($id)
    {
        $select = new Select();
        $where = new Where();
        $select->from('video_gallery_files');
        $select->join('video_galleries', 'video_galleries.id = video_gallery_files.video_gallery_id');
        if ($id) {
            $select->where->equalTo('video_galleries.id', $id);
        }
                
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
    public function getAllVideos($name, $lang)
	{
		$select = new Select();
		$where = new Where();
		$select->from('video_gallery_files');
		$select->join('video_galleries', 'video_galleries.id = video_gallery_files.video_gallery_id');
		$select->where->equalTo('latin_name', $name);
		$select->where->equalTo('lang', $lang);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchLatestVideos($limit) // This function is used for showing latest videos from galleryfiles in the HomePage of the site!
	{
		$select = new Select();
		$select->from('video_gallery_files');
		$select->limit($limit);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getVideoById($videoId)
	{
		$select = new Select();
		$select->from('video_gallery_files');
		if ($videoId > 0 && is_numeric($videoId)) {
			$select->where->equalTo('id', $videoId);
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	
	public function add($data)
	{
		return $this->tableGateway->insert(array(
				'caption'   => $data['caption'],
				'video'     => $data['video'],
				'thumbnail' => $data['thumbnail'],
		));
	}
	
	public function updateVideo($newData, $videoId)
	{
		return $this->tableGateway->update($newData, array(
				'id' => $videoId
		));		
	}
	
	public function delete($videoId)
	{
		if ($videoId > 0 && is_numeric($videoId)) {
			return $this->tableGateway->delete(array(
					'id' => $videoId
			));
		}
	}
	
	public function deleteByVideoGalleryId($Id)
	{
	    if ($Id > 0 && is_numeric($Id)) {
	        return $this->tableGateway->delete(array(
	            'video_gallery_id' => $Id
	        ));
	    }
	}
}
