<?php
namespace VideoGallery\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

class VideoGalleriesTable extends BaseModel
{

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('video_galleries', $this->getDbAdapter());
    }

    public function getAllParentVideoGallery($videoGalleryId = false)
    {
        $select = new Select();
        $select->from('video_galleries');
        $select->columns(array(
            'id',
            'name' => 'persian_name'
        
        ));
        $select->where->equalTo('parent_id', 0);
        if ($videoGalleryId) {
            $select->where->notEqualTo('id', $videoGalleryId);
        }
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function fatchAllChild($id)
    {
        $select = new Select();
        $select->from('video_galleries');
        
        $select->where->equalTo('parent_id', $id);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    

    public function getAllVideoGallery()
    {
        $sql = "SELECT `id`, `persian_name`, `latin_name`, b.`parent_id`, a.Count FROM `video_galleries` b LEFT OUTER JOIN (SELECT `parent_id`, COUNT(*) AS Count FROM `video_galleries` group by `parent_id`)a on b.`id` = a.`parent_id`";
        return $this->tableGateway->getAdapter()->driver->getConnection()->execute($sql);      
    }

    public function fetchLatestVideoGallery($limit) // This function is used for showing latest images from videoGallery in the HomePage of the site!
    {
        $select = new Select();
        $select->from('video_galleries');
        $select->limit($limit);
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getVideoGalleryById($Id)
    {
        $select = new Select();
        $select->from('video_galleries');
        if ($Id > 0 && is_numeric($Id)) {
            $select->where->equalTo('id', $Id);
            return $this->tableGateway->selectWith($select)->toArray();
        }
    }
    
    public function getAllVideoGallerySub()
    {
        $select = new Select();
        $select->from('video_galleries');
        $select->where->notEqualTo('parent_id', 0);
        return $this->tableGateway->selectWith($select)->toArray();
        
    }

    public function addVideoGallery($data)
    {
        return $this->tableGateway->insert(array(
            'lang' => $data['lang'],
            'latin_name' => $data['latin_name'],
            'persian_name' => $data['persian_name'],
            'parent_id' => $data['parent_id']
        ));
    }

    public function editVideoGallery($newData, $Id)
    {
        return $this->tableGateway->update($newData, array(
            'id' => $Id
        ));
    }

    public function deleteVideoGallery($Id)
    {
        $where = new Where();
        $where->equalTo('id', $Id)->or->equalTo('parent_id', $Id);
        return $this->tableGateway->delete($where);
    }
}
