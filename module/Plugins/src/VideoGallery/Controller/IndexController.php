<?php
namespace VideoGallery\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use VideoGallery\Model\VideoGalleriesTable;
use VideoGallery\Model\VideoGalleryFilesTable;
use Application\Helper\BasePluginController;
 
class IndexController extends BasePluginController 
{ 
	protected $videoGalleryTable;
	

    public function indexAction($params)
	{
	   
	        $lang = $this->lang;
    	    if (is_array($params)) {
                $latin_name = $params['latin_name'];
                $limit = $params['limit'];
                $layout = $params['layout'];
            } else {
                $latin_name = $params;
                $limit = 1000;
                $layout = "index";
            }
	        $videoGalleryFilesTable = new VideoGalleryFilesTable($this->getServiceLocator());
	        $allvideo = $videoGalleryFilesTable->getAllVideos($latin_name, $lang);
	        $ViewModel = new ViewModel();
	        $ViewModel->setTemplate('video-gallery/index/' . $layout . '.phtml');
	        if(count($allvideo) > 0 ){
	            $ViewModel->setVariable('videos', $allvideo);
	            return $ViewModel;
	        }else{
	            //$this->layout()->errorMessage = __('There is no video in this video gallery!');
	        }
	    
	}
	
	public function pageAction($id)
	{
	    $lang = $this->lang;
	    $galleryFilesTable = new VideoGalleryFilesTable($this->getServiceLocator());
	    $allvideo = $galleryFilesTable->getAllVideosByIdForPage($id);
	    $ViewModel = new ViewModel();
	    $ViewModel->setTemplate('video-gallery/index/page.phtml');
	    if(count($allvideo) >0 ){
	        $ViewModel->setVariable('videos', $allvideo);
	        return $ViewModel;
	    }else{
	        //$this->layout()->errorMessage = __('There is no video in this video gallery!');
	    }
	
	}
	
	public function showVideoAction($id)
	{
	    $videoGalleriesTable = new VideoGalleriesTable($this->getServiceLocator());
	    $videoGalleryFilesTable = new VideoGalleryFilesTable($this->getServiceLocator());
	    $allvideo = $videoGalleryFilesTable->getAllVideos($id);
	    $videoGalleryVideos = $videoGalleriesTable->getAllVideoGallery();
	     $menu = array(
	        'items' => array(),
	        'parents' => array()
	    );
	    foreach ($videoGalleryVideos as  $items){
	         
	        $menu['items'][$items['id']] = $items;
	        $menu['parents'][$items['parent_id']][] = $items['id'];
	         
	    }
	    $videoGalleryVideos = $this->generalhelper()->buildVideoGalleryMenu(0, $menu);
	    
	    if(count($allvideo) >0 ){
    	    $view['video'] = $allvideo;
    	    return new ViewModel($view);	    
	    }else{
	        $this->layout()->errorMessage = __('There is no video in this video gallery!');
	    }
	    
	}
	
	
}
