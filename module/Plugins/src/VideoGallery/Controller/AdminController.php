<?php

namespace VideoGallery\Controller;
   
use Zend\View\Model\ViewModel;
use VideoGallery\Form\VideoGalleryForm;
use Zend\Session\Container;
use VideoGallery\Model\VideoGalleriesTable;
use Application\Helper\BaseAdminController;
use VideoGallery\Model\VideoGalleryFilesTable;
use Language\Model\LanguageLanguagesTable;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Zend\Db\Adapter\Adapter;
 
class AdminController extends BaseAdminController 
{ 
	public function listVideoGalleriesAction()
	{
	    $this->setHeadTitle(__('List video galleries'));
 	 
	    $contanier = new Container('csrf');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	    
	    
	    $view['csrf'] = $csrf;
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $videoGalleriesTable = new VideoGalleriesTable($this->getServiceLocator());
        $videoGalleryVideos = $videoGalleriesTable->getAllVideoGallery();
	    foreach ($videoGalleryVideos as $dataFile) {
	        $coworkers[] = $dataFile;
	    }
	    $grid->setTitle(__('List galleries'));
	    $grid->setDefaultItemsPerPage(15);
	    $grid->setDataSource($coworkers, $dbAdapter);
	    
	    $col = new Column\Select('id');
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('persian_name');
	    $col->setLabel(__('Persian Name'));
	    $grid->addColumn($col);
	    
	    $col = new Column\Select('latin_name');
	    $col->setLabel(__('Latin Name'));
	    $grid->addColumn($col);

	    $col->addStyle(new Style\Bold());
	    $grid->addColumn($col);
	    
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction3 = new Column\Action\Icon();
	    $viewAction3->setIconClass('glyphicon glyphicon-plus');
	    $viewAction3->setLink("/" . $this->lang . '/admin-video-gallery/add-video/' . $rowId);
	    /*  $viewAction3->setAttribute('id', 'plus'); */
	    $viewAction3->setTooltipTitle(__('Add video to video gallery'));
	    
	    $viewAction1 = new Column\Action\Icon();
	    $viewAction1->setIconClass('glyphicon glyphicon-edit');
	    $viewAction1->setLink("/" . $this->lang . '/admin-video-gallery/edit-video-gallery/' . $rowId);
	    $viewAction1->setTooltipTitle(__('Edit'));
	    
	    $viewAction2 = new Column\Action\Icon();
	    $viewAction2->setIconClass('glyphicon glyphicon-remove');
	    $viewAction2->setLink("/" . $this->lang . '/admin-video-gallery/delete-video-gallery/' . $rowId . '/token/' . $csrf);
	    $viewAction2->setTooltipTitle(__('Delete'));
	    
	    $rowIds = $btn->getRowIdPlaceholder ();
	    $viewAction4 = new Column\Action\Icon ();
	    $viewAction4->setIconClass ( 'glyphicon glyphicon-chevron-up hideData' );
	    $viewAction4->setLink ( "#sub" );
	    $viewAction4->setTooltipTitle ( __( 'Show sub gallery' ) );
	    
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction4);
	    $actions2->addAction($viewAction3);
	    $actions2->addAction($viewAction1);
	    $actions2->addAction($viewAction2);
	    
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	    
	    $sampleAdd[] = '<a href=' . "/" . $this->lang . '/admin-video-gallery/add-video-gallery class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $sampleAdd[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
	    $grid->setLink($sampleAdd);
	    
	    
	    $subVideoGallery = array ();
	    $allSubVideo = $videoGalleriesTable->getAllVideoGallerySub();
	    
	    if ($allSubVideo) {
	        foreach ( $allSubVideo as $key => $data ) {
	            $subArray = array (
	                'parent_id' => $data['parent_id'],
	                'persian_name' => $data['persian_name'],
	                'latin_name' => $data['latin_name']
	            );
	            $subVideoGallery[$key] = $subArray;
	            $subVideoGallery[$key]['addgallerylink'] = '<td colspan="3" style="padding-right: 42px;"><a   href=' . "/" . $this->lang . '/admin-video-gallery/add-video/' . $data['id'] . ' class=""><i class="glyphicon glyphicon-plus"></i> </a>';
	            $subVideoGallery[$key]['editlink'] = '<a   href=' . "/" . $this->lang . '/admin-video-gallery/edit-video-gallery/' . $data['id'] . ' class=""> <i class="glyphicon glyphicon-edit"></i></a> ';
	            $subVideoGallery[$key]['deletelink'] = '<a   href=' . "/" . $this->lang . '/admin-video-gallery/delete-video-gallery/' . $data['id']  . '/' . $csrf.' class=""> <i class="glyphicon glyphicon-remove"></i></a></td> ';
	        }
	    }
	    
	    $grid->setExtraData ($subVideoGallery );
	    
	    $grid->render();
	    
	    return $grid->getResponse();
	}
	
	public function addVideoGalleryAction()
	{	    
	    $this->setHeadTitle(__('Add new video gallery'));
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
	    $videoGalleriesTable = new VideoGalleriesTable($this->getServiceLocator());
	    $parents = $videoGalleriesTable->getAllParentVideoGallery();

	    $videoGalleryForm = new VideoGalleryForm($allActiveLanguages, false, $parents);
	    $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $videoGalleryForm->setData($data);
            if ($videoGalleryForm->isValid()) {
                $validData = $videoGalleryForm->getData();
                $item = array(
                    'lang'           => $validData['lang'],
                    'persian_name'   => $validData['persian_name'],
                    'latin_name'     => $validData['latin_name'],
                    'parent_id'      => $validData['parent_id']
                );
                try {
                    $isAdded = $videoGalleriesTable->addVideoGallery($item);
	                if ($isAdded) {
// 	                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
// 	                    return $this->redirect()->toRoute('video-gallery-admin');
	                    if (isset($_POST['submit'])) {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('video-gallery-admin');
	                    } else {
	                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                        return $this->redirect()->toRoute('video-gallery-admin', array(
	                            'action' => 'add-video-gallery'
	                        ));
	                    }
	                }
                	
                } catch (\Exception $e) {
	                $this->layout()->errorMessage = __('Duplicated name! please choose another one!');
                }
            } 
        }
        $view['videoGalleryForm'] = $videoGalleryForm;
        return new ViewModel($view);
	
	}

	public function editVideoGalleryAction()
	{
	    $this->setHeadTitle(__('Edit video gallery'));
	    $config = $this->serviceLocator->get('Config');
	    $uploadDir = $config['base_route'] . "/uploads/videoGallery/";
	    $validationExt ="mpeg";
	    
	    $Id = $this->params('id', -1);
	    
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
	    $videoGalleriesTable = new VideoGalleriesTable($this->getServiceLocator());
	    $parents = $videoGalleriesTable->getAllParentVideoGallery($Id);
	    
	    $videoGalleryById = $videoGalleriesTable->getVideoGalleryById($Id);
	    $videoGalleryForm = new VideoGalleryForm($allActiveLanguages, true, $parents);
	    $view['videoGalleryById'] = $videoGalleryById;
	    $videoGalleryForm->populateValues($videoGalleryById[0]);
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $data = $request->getPost()->toArray();
	        $files =  $request->getFiles()->toArray();
	        $data = array_merge($data, $files);
	        $videoGalleryForm->setData($data);
	        if ($videoGalleryForm->isValid()) {
	            $validData = $videoGalleryForm->getData();
	            $item = array();

	            $item['latin_name'] = $validData['latin_name'];
	            $item['persian_name'] = $validData['persian_name'];
	            $item['lang'] = $validData['lang'];
	            if ($Id) {
	            	try {
	            	    $isEdited = $videoGalleriesTable->editVideoGallery($item, $Id);
		                if ($isEdited !== false) {
		                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
		                } else {
		                	$this->flashMessenger()->addErrorMessage(__('Operation failed!'));
		                }
		                return $this->redirect()->toRoute('video-gallery-admin');
	            	} catch (\Exception $e) {
	            		$this->layout()->errorMessage = __('Duplicated name! please choose another one!');
	            	}
	            }
	        } 
	    } 
	    $view['videoGalleryForm'] = $videoGalleryForm;
	    return new ViewModel($view);
	}
	
	public function addVideoAction()
	{
	    $this->setHeadTitle(__('Add video'));
        $view['userId'] = $this->userData->id;
        $view['videoGalleryId'] = $this->params('id');
        return new ViewModel($view);
	}
	
	public function deleteVideoGalleryAction()
	{
		$container = new Container('csrf');
		$token = $this->params('token');
		$videoGalleriesTable = new VideoGalleriesTable($this->getServiceLocator());
		$videoGalleryFilesTable = new VideoGalleryFilesTable($this->getServiceLocator());
		$Id = $this->params('id', -1); 
		
		if ($Id && $container->offsetExists('csrf') && $container->offsetGet('csrf') === $token) { 
		    $childExist = $videoGalleriesTable->fatchAllChild($Id);
		    if($childExist){
		        $this->flashMessenger()->addErrorMessage(__("This gallery has child, first delete sub video gallery"));
		        return $this->redirect()->toRoute('video-gallery-admin');
		    }
		    $Isdelete = $videoGalleriesTable->deleteVideoGallery($Id);
			if($Isdelete){
			    $allVideoForDelete = $videoGalleryFilesTable->getAllVideosById($Id);
			    $deleteVideos = $videoGalleryFilesTable->deleteByVideoGalleryId($Id);
			    if($deleteVideos) {
			        foreach ($allVideoForDelete as $video) {
			            if(file_exists($_SERVER['DOCUMENT_ROOT']."/uploads/videoGallery/".$video['file_name']) )
			            @unlink($_SERVER['DOCUMENT_ROOT']."/uploads/videoGallery/".$video['file_name']);
			        }
			    }
			}
			$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
			return $this->redirect()->toRoute('video-gallery-admin');

	}
		
	}
}
