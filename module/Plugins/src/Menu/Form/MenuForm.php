<?php 

namespace Menu\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class MenuForm extends Form
{
	public function __construct($allMenus, $pagesList)
	{
		parent::__construct('menuForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
    		    'id'      => 'menuForm',
    		    'novalidate'=> true
		));

		$menuId = new Element\Hidden('menu_id');
		$menuId->setAttributes(array(
		    'id'    	  => 'menu_id'
		));
		
		$menuCaption = new Element\Text('caption');
		$menuCaption->setAttributes(array(
				'id'    	  => 'caption',
				'class' 	  => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder' => __('Menu title'),
		));
		$menuCaption->setLabel(__("Menu title"));

		$menuExternalUrl = new Element\Text('external_url');
		$menuExternalUrl->setAttributes(array(
		    'id'    	  => 'external_url',
		    'class' 	  => 'form-control',
		    'placeholder' => 'http://www.google.com',
		));
		$menuExternalUrl->setLabel(__("External url"));
		
		
		$menuInternalUrl = new Element\Select('internal_url');
		$menuInternalUrl->setAttributes(array(
		    'id' => 'internal_url',
		    'class' => 'form-control',
		));
		$pageArray= array("" => "Choose an item");
		if ($pagesList) {
		    foreach ($pagesList as $page) {
		        if (!strpos($page["page_title"], " Help"))
		              $pageArray[$page['id']] = $page['page_title'];
		    }
		}
		$menuInternalUrl->setValueOptions($pageArray);
		$menuInternalUrl->setLabel(__("Choose a page"));
			
		$menuParentId = new Element\Select('parent_id');
		$menuParentId->setAttributes(array(
		    'id' => 'parent_id',
		    'class' => 'form-control',
		));
		$menusArray = array("0" => "Main menu");
		if ($allMenus) {
		    foreach ($allMenus as $menus) {
		        $menusArray[$menus['id']] = $menus['caption'];
		    }
		}
		$menuParentId->setValueOptions($menusArray);
		$menuParentId->setLabel(__("Choose parent menu"));
		
		$menuClassNameUl = new Element\Text('class_name_ul');
		$menuClassNameUl->setAttributes(array(
		    'id'    	  => 'class_name_ul',
		    'class' 	  => 'form-control',
		    'placeholder' => __('Menu`s ul class'),
		));
		$menuClassNameUl->setLabel(__("Menu`s ul class"));
		
		$menuClassNameLi = new Element\Text('class_name_li');
		$menuClassNameLi->setAttributes(array(
		    'id'    	  => 'class_name_li',
		    'class' 	  => 'form-control',
		    'placeholder' => __('Menu`s li class'),
		));
		$menuClassNameLi->setLabel(__("Menu`s li class"));
		
		$menuOrder = new Element\Text('order');
		$menuOrder->setAttributes(array(
		    'id'    	  => 'order',
		    'class' 	  => 'form-control',
		    'placeholder' => __('Menu order'),
		));
		$menuOrder->setLabel(__("Menu order"));
		
		$menuWidth = new Element\Text('width');
		$menuWidth->setAttributes(array(
		    'id'    	  => 'width',
		    'class' 	  => 'form-control',
		    'placeholder' => "20%, 100px",
		));
		$menuWidth->setLabel(__("Menu width"));
		
		$menuIcon = new Element\Text('icon');
		$menuIcon->setAttributes(array(
		    'id'    	  => 'sign',
		    'class' 	  => 'form-control',
		    'placeholder' => "",
		));
		$menuIcon->setLabel(__("Icon class"));
		
		$menuImage = new Element\File('image');
		$menuImage->setAttributes(array(
		    'id'    => 'fileUpload1'
		));
		$menuImage->setLabel(__("Choose image"));
		
		$menuActive = new Element\Checkbox('active');
		$menuActive->setAttributes(array(
		    'id'    => 'active',
		    'class' => 'form-dontrol'
		));
		$menuActive->setLabel(__("Active/Deactive"));
		$menuActive->setValue(1);
		
		$csrf = new Element\Csrf('csrf_menus');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save"));
		$submit->setAttributes(array(
				'id'    => 'update-menu',
				'class' => 'btn btn-primary'
		));
		
		$reset = new Element\Button('resetform');
		$reset->setLabel("Reset");
        $reset->setAttributes(array(
		    'id'    	  => 'resetfileds',
		    'class' 	  => 'btn btn-danger',
		));

		$this->add($menuId)
		     ->add($menuCaption)
		     ->add($menuExternalUrl)
		     ->add($menuInternalUrl)
		     ->add($menuParentId)
		     ->add($menuClassNameUl)
		     ->add($menuClassNameLi)
		     ->add($menuOrder)
		     ->add($menuWidth)
		     ->add($menuIcon)
		     ->add($menuImage)
		     ->add($menuActive)
			 ->add($csrf)
			 ->add($submit)
		     ->add($reset);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'caption',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
		    'name'     => 'internal_url',
		    'required' => false
		)));
		
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}