<?php 

namespace Menu\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class MainMenuForm extends Form
{
	public function __construct($allActiveLanguages)
	{
		parent::__construct('mainMenuForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
		        'id'      => 'mainMenuForm',
		        'novalidate'=> true
		));

		$mainMenuName = new Element\Text('name');
		$mainMenuName->setAttributes(array(
				'id'    	  => 'name',
				'class' 	  => 'form-control validate[required]',
		        'required' => 'required',
		        'placeholder' => __('Main menu title'),
		));
		$mainMenuName->setLabel(__("Main menu title"));

		$mainMenuPosition = new Element\Text('position');
		$mainMenuPosition->setAttributes(array(
		    'id'    	  => 'position',
		    'class' 	  => 'form-control validate[required]',
		    'required' => 'required',
		    'placeholder' => __('Position'),
		));
		$mainMenuPosition->setLabel(__("Position"));
		
		$mainMenuLang = new Element\Select('lang');
		$mainMenuLang->setAttributes(array(
		    'id' => 'page_lang',
		    'class' => 'form-control',
		));
		$languages = array();
		if ($allActiveLanguages) {
		    foreach ($allActiveLanguages as $language) {
		        $languages[$language['code']] = $language['title'];
		    }
		}
		$mainMenuLang->setValueOptions($languages);
		$mainMenuLang->setLabel(__("Choose language"));
		
		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Save and Close"));
		$submit->setAttributes(array(
				'id'    => 'update-menu',
				'class' => 'btn btn-primary'
		));
		
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(__("Save and New"));
		$submit2->setAttributes(array(
				'id'    => 'update-menu',
				'class' => 'btn btn-primary'
		));

		$this->add($mainMenuName)
		     ->add($mainMenuPosition)
		     ->add($mainMenuLang)
			 ->add($csrf)
			 ->add($submit)
			 ->add($submit2);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
		    'name'     => 'position',
		    'required' => true,
		    'filters' => array(
		        array(
		            'name' => 'StripTags'
		        )
		    )
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}