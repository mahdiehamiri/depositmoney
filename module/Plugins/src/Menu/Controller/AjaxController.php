<?php

namespace Menu\Controller;


use Menu\Model\MenuMenusTable;
use Application\Helper\BaseAdminController;

class AjaxController extends BaseAdminController
{	
	public function deleteMenuAction()
	{
		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			$menuId = $data['id'];
			$menuMenusTable = new MenuMenusTable($this->getServiceLocator());
			$checkHasChildren = $menuMenusTable->getMenuChilds($menuId);
			if ($checkHasChildren[0] == '') {
			    $menuInfo = $menuMenusTable->getMenu($menuId);
			    if ($menuInfo) {
			        $isDeleted = $menuMenusTable->deleteMenu($menuId);
			        if ($isDeleted) {
			            if ($menuInfo[0]["parent_id"] != 0 ) {
			                $menusInfo = $menuMenusTable->getMenuByParentId($menuInfo[0]["parent_id"]);
			                if (!$menusInfo) {
			                    $menuData["class_name_ul"] = "";
			                    $menuData["class_name_li"] = "";
			                    $menuMenusTable->editUlLi($menuInfo[0]["parent_id"], $menuData);
			                }
			            } 
			            die("true");
			        } else {
			            die("false");
			        }
			    } else {
			        die("false");
			    }
			} else {
			    die("child");
			}
		}
		die("false");
	}
}