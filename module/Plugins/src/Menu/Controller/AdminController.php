<?php

namespace Menu\Controller;
   
use Zend\View\Model\ViewModel; 
use Menu\Model\MenuMenusTable;
use Application\Helper\BaseAdminController;
use Menu\Model\MenuMainMenusTable;
use Menu\Form\MainMenuForm;
use Zend\Session\Container;
use Language\Model\LanguageLanguagesTable;
use Menu\Form\MenuForm;
use Page\Model\PagePageTable;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Gallery\Model\GalleriesTable;

class AdminController extends BaseAdminController 
{ 		
    
    
    public function listMainMenusAction()
	{
	    $this->setHeadTitle(__('Main menus list'));
	  /*  $container = new Container('token');
	    $csrf = md5(time() . rand());
	    $container->csrf = $csrf;
	    $view['csrf'] = $csrf;
	    $menuMainMenusTable = new MenuMainMenusTable($this->getServiceLocator());
		$mainMenus = $menuMainMenusTable->fetchAllMainMenus();
		$view['mainMenus'] = $mainMenus;
		return new ViewModel($view); */
	    
	    
	    $contanier = new Container('token');
	    $csrf = md5(time() . rand());
	    $contanier->csrf = $csrf;
	    
	    $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
	     
	    $config = $this->getServiceLocator()->get('Config');
	    $dbAdapter = new Adapter($config['db']);
	    $menuMainMenusTable = new MenuMainMenusTable($this->getServiceLocator());
		$mainMenus = $menuMainMenusTable->fetchAllMainMenus();
	    $grid->setTitle(__('Main menu list'));
	    $grid->setDefaultItemsPerPage(15);
	    $grid->setDataSource($mainMenus,$dbAdapter);
	     
	    $col = new Column\Select('id',"menu_main_menus");
	    $col->setLabel(__('Row'));
	    $col->setIdentity();
	    $grid->addColumn($col);
	     
	    $col = new Column\Select('name');
	    $col->setLabel(__('Title'));
	    $grid->addColumn($col);
	     
	    $col = new Column\Select('position');
	    $col->setLabel(__('Position'));
	    $grid->addColumn($col);
	     
	    $col = new Column\Select('lang');
	    $col->setLabel(__('Lang'));
	    $grid->addColumn($col);
	    
	    $btn = new Column\Action\Button();
	    $btn->setLabel(__('Edit'));
	    $rowId = $btn->getRowIdPlaceholder();
	    
	    $viewAction = new Column\Action\Icon();
	    $viewAction->setIconClass('glyphicon glyphicon-plus');
	    $viewAction->setLink("/". $this->lang . '/admin-menu/show-menu/' . $rowId);
	    $viewAction->setTooltipTitle(__('Add items'));
	     
	    $viewAction1 = new Column\Action\Icon();
	    $viewAction1->setIconClass('glyphicon glyphicon-edit');
	    $viewAction1->setLink("/". $this->lang . '/admin-menu/edit-main-menu/' . $rowId);
	    $viewAction1->setTooltipTitle(__('Edit'));
	     
	    $viewAction2 = new Column\Action\Icon();
	    $viewAction2->setIconClass('glyphicon glyphicon-remove');
	    $viewAction2->setLink("/". $this->lang . '/admin-menu/delete-main-menu/'.$rowId.'/token/'.$csrf);
	    $viewAction2->setTooltipTitle(__('Delete'));
	    
	    $link[] = '<a href='."/". $this->lang . '/admin-menu/add-main-menu class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a tablename="menu_menus"  href="/application-ajax-check-parent"  class="btn btn-danger delete_all"><i class="fa fa-times" aria-hidden="true"></i>' . __("Delete All") . '</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
	    $grid->setLink($link);
	    
	    $actions2 = new Column\Action();
	    $actions2->setLabel(__('Operations'));
	    $actions2->addAction($viewAction);
	    $actions2->addAction($viewAction1);
	    $actions2->addAction($viewAction2);
	    $actions2->setWidth(15);
	    $grid->addColumn($actions2);
	    
	     
	    $grid->render();
	    
	    return $grid->getResponse();
	    
	    
	    
	}
    public function addMainMenuAction()
    {
        $this->setHeadTitle(__('Add new main menu'));
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
        $mainMenuForm = new MainMenuForm($allActiveLanguages);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $mainMenuForm->setData($data);
            if ($mainMenuForm->isValid()) {
                $mainMenusData = $mainMenuForm->getData();
                $menuMainMenusTable = new MenuMainMenusTable($this->getServiceLocator());
                $isAdded = $menuMainMenusTable->addMainMenu($mainMenusData);
                if ($isAdded) {
                   
                    if (isset($_POST['submit'])) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toRoute('menu-admin', array(
                            'controller' => 'admin',
                            'action'     => 'list-main-menus'
                        ));
                    } else {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toRoute('menu-admin', array(
                            'controller' => 'admin',
                            'action'     => 'add-main-menu'
                        ));
                    }
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            }
        }
        $view['mainMenuForm'] = $mainMenuForm;
        return new ViewModel($view);
    
    }
    
    public function editMainMenuAction()
    {
        $this->setHeadTitle(__('Edit main menu'));
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
        $mainMenuForm = new MainMenuForm($allActiveLanguages);
        $mainMenuId = $this->params('id', -1);
        $menuMainMenusTable = new MenuMainMenusTable($this->getServiceLocator());
        $mainMenusData = $menuMainMenusTable->getMainMenuById($mainMenuId);
        if (!$mainMenusData) {
            return $this->redirect()->toRoute('menu-admin', array(
                'controller' => 'admin',
                'action'     => 'list-main-menus'
            ));
        }
        $mainMenuForm->populateValues($mainMenusData[0]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $mainMenuForm->setData($data);
            if ($mainMenuForm->isValid()) {
                $mainMenuData = $mainMenuForm->getData();
                $isUpdated = $menuMainMenusTable->editMainMenu($mainMenuData, $mainMenuId);
                if ($isUpdated !== false) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toRoute('menu-admin', array(
                        'controller' => 'admin',
                        'action'     => 'list-main-menus'
                    ));
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            }
        }
        $view['mainMenuForm'] = $mainMenuForm;
        return new ViewModel($view);
    }
    
    
    public function deleteMainMenuAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $mainMenuId = $this->params('id');
        $menuMainMenusTable = new MenuMainMenusTable($this->getServiceLocator());
        $mainMenuData = $menuMainMenusTable->getMainMenuById($mainMenuId);
        if ($container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
            $isDeleted = $menuMainMenusTable->deleteMainMenu($mainMenuId);
            if ($isDeleted) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            } else {
                $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
            }
        }
        return $this->redirect()->toRoute('menu-admin');
        
    }
	
	public function showMenuAction()
	{
	    $config = $this->serviceLocator->get('Config');
	    $uploadDir = $config['base_route'] . "/uploads/menus/";
	    $validationExt ="jpg,jpeg,png,gif";
	    
	   $container = new Container('token');
	   $csrf = md5(time() . rand());
	   $container->csrf = $csrf;
	   $view['csrf'] = $csrf; 
	   $mainMenuId = $this->params('id');
	   $menuMainMenusTable = new MenuMainMenusTable($this->getServiceLocator());
	   $mainMenuData = $menuMainMenusTable->getMainMenuById($mainMenuId);
       if (!$mainMenuData) {
            return $this->redirect()->toRoute('menu-admin', array(
                'controller' => 'admin',
                'action'     => 'list'
            ));
        }
      
        $pagePageTable = new PagePageTable($this->getServiceLocator());
        $pagesList = $pagePageTable->getPages(array("page_lang" => $mainMenuData[0]["lang"]));
               
	    $menuForm = new MenuForm($mainMenuData, $pagesList);
	    
	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        
	        $menusTable = new MenuMenusTable($this->getServiceLocator());
	        $data = $request->getPost()->toArray();
	        $files =  $request->getFiles()->toArray();
	        if(!empty($files['image']['name'])){
	            $data = array_merge($data, $files);
	        }
	        
	        $menuForm->setData($data);
	        if ($menuForm->isValid()) {
	            $menuData = $menuForm->getData();
	            if ($menuData["menu_id"]) { // EditMode
	               $mernuDataOld = $menusTable->getMenu($menuData["menu_id"]);
    
	               $isUpdated = $menusTable->editMenu($menuData["menu_id"], $menuData);
	               if ($isUpdated !== false) {
	                   if(!empty($files)){
	                       $newFileName = md5(time() . $menuData['image']['name']);
	                       $isUploaded = $this->imageUploader()->UploadFile( $menuData['image'], $validationExt, $uploadDir, $newFileName);
	                       $menuData["image"] = $isUploaded[1];
	                   }
	                   if (isset($mernuDataOld["parent_id"]) && $mernuDataOld["parent_id"] == 0) {
	                       if ($menuData["parent_id"] !== 0) {
	                           $menuData["class_name_ul"] = "dropdown-menu";
	                           $menuData["class_name_li"] = "dropdown";
	                           $menusTable->editUlLi($menuData["parent_id"], $menuData);
	                       } 
	                   } else {
	                       if ($menuData["parent_id"] !== 0) {
	                           $menuData["class_name_ul"] = "dropdown-menu";
	                           $menuData["class_name_li"] = "dropdown";
	                           $menusTable->editUlLi($menuData["parent_id"], $menuData);
	                       } else {
	                           $menuData["class_name_ul"] = "";
	                           $menuData["class_name_li"] = "";
	                           $menusTable->editUlLi($menuData["parent_id"], $menuData);
	                       }
	                   }
	                   $mainMenuData = $menuMainMenusTable->getMainMenuById($mainMenuId);
	                 
	                   $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                
	                  
	               } else {
	                   $this->layout()->errorMessage = __("Operation failed!");
	               }
	            } else { 
	                // InsertMode
	                if(!empty($files)){
	                    $newFileName = md5(time() . $menuData['image']['name']);
	                    $isUploaded = $this->imageUploader()->UploadFile( $menuData['image'], $validationExt, $uploadDir, $newFileName);
	                    $menuData["image"] = $isUploaded[1];
	                }
	                
	                
	                $isAdded = $menusTable->addMenu($mainMenuId, $menuData);
	                if ($isAdded) {
	                    if ($menuData["parent_id"] !== 0) {
	                        $menuData["class_name_ul"] = "dropdown-menu";
	                        $menuData["class_name_li"] = "dropdown";
	                        $menusTable->editUlLi($menuData["parent_id"], $menuData);
	                    }
	                    $mainMenuData = $menuMainMenusTable->getMainMenuById($mainMenuId);
	                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	                    return $this->redirect()->toRoute('menu-admin', array(
	                        'controller' => 'admin',
	                        'action'     => 'show-menu',
	                        'id'         =>  $mainMenuId
	                    ));
	                } else {
	                    $this->layout()->errorMessage = __("Operation failed!");
	                }
	            }
	        }
	    }
	    if ($mainMenuData) {
	        $pagesList = $pagePageTable->getPages(array("page_lang" => $mainMenuData[0]["lang"]));
	        
	        $pagesListArray = array();
	        foreach ($pagesList as $page) {
	            $pagesListArray[$page["id"]] = "/" . $page["page_lang"] . "/" . str_replace(" ", "-", $page["page_title"]);
	        }
	        
	        $tree = $this->generalHelper()->createTree($mainMenuData, false, $pagesListArray, $this->lang);
	        $view['menu'] = $mainMenuData[0];
	        $this->setHeadTitle($mainMenuData[0]["name"]);
	        $view['menuTree'] = $tree;
	    }
	  
	    $view['menuForm'] = $menuForm;
	    return new ViewModel($view);
		
	}

}
