<?php

namespace Menu\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Menu\Model\MenuMainMenusTable;
use Page\Model\PagePageTable;
use Application\Helper\BasePluginController;
use Application\Helper\GeneralHelper;
 
class IndexController extends BasePluginController 
{ 	
	
	public function indexAction($params)
	{
        //var_dump($params); die;
	    $view = array();
        $mainMenuPosition = $params[0];
        $classMain = (isset($params[1])?$params[1]:"nav navbar-nav navbar ");
        $menuMainMenusTable = new MenuMainMenusTable($this->getServiceLocator());
        
        $mainMenuData = $menuMainMenusTable->getMainMenuView($this->lang, $mainMenuPosition);
        $count = 0;
        foreach ($mainMenuData as $menuId) {
            if ($menuId["parent_id"] == 0 && $menuId["active"] == 1) {
                $count++;
            }
        }
        if ($mainMenuData) {
            $pagePageTable = new PagePageTable($this->getServiceLocator());
            $pagesList = $pagePageTable->getPages(array("page_lang" => $mainMenuData[0]["lang"]));
            $pagesListArray = array();
            if ($pagesList) {
                foreach ($pagesList as $page) {
                    $pagesListArray[$page["id"]] =  ($page["page_lang"] != $GLOBALS["dlang"]? "/" . $page["page_lang"] :"") . "/" . $page["page_slug"];
                }
            }
            $generalHelper = new GeneralHelper(); 
            $tree = $generalHelper->createTree($mainMenuData, true, $pagesListArray, $count);
            $view['menuId'] = $mainMenuData[0]["id"];
            $view['menu'] = $tree;
        }
        
        $viewModel = new ViewModel($view);
        $viewModel->setTemplate("menu/index/index.phtml");
        return $viewModel;
	        
	}
	
	
}
