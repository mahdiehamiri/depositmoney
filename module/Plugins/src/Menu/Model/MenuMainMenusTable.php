<?php 
namespace Menu\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModelInterface;
use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class MenuMainMenusTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('menu_main_menus', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function fetchAllMainMenus()
	{
		$select = new Select('menu_main_menus');
		return $select;
		//return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getMainMenuById($mainMenuId)
	{
		$select = new Select('menu_main_menus');
		if ($mainMenuId) {
			$select->where->equalTo('menu_main_menus.id', $mainMenuId);
		}
		$select->join('menu_menus', 'menu_menus.main_menu_id = menu_main_menus.id',array(
				'*'
		),$select::JOIN_LEFT);
		$select->order("order");
		return $this->tableGateway->selectWith($select)->toArray();
		//return $select;
	}

	public function getMainMenuView($menuLanguage = null, $mainMenuPosition = null)
	{
	    $select = new Select('menu_main_menus');
	    if ($mainMenuPosition) {
	        $select->where->equalTo('menu_main_menus.position', $mainMenuPosition);
	    }
	    if ($menuLanguage) {
	        $select->where->equalTo('menu_main_menus.lang', $menuLanguage);
	    }
	    $select->join('menu_menus', 'menu_menus.main_menu_id = menu_main_menus.id',array(
	        '*'
	    ),$select::JOIN_LEFT);
	    $select->order("order");
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addMainMenu($mainMenuData)
	{
		$result = $this->tableGateway->insert(array(
				'name' => $mainMenuData['name'],
		        'position' => $mainMenuData['position'],
		        'lang' => $mainMenuData['lang']
		));
		if ($result) {
			return $this->tableGateway->lastInsertValue;
		} else {
			return false;
		}
	}
	
	public function editMainMenu($editMainMenuData, $mainMenuId)
	{
		if ($mainMenuId) {
			return $this->tableGateway->update(array(
					'name' => $editMainMenuData['name'],
			        'position' => $editMainMenuData['position'],
			        'lang' => $editMainMenuData['lang']
			), array('id' => $mainMenuId));
		}
		return false;
	}
	
	public function deleteMainMenu($mainMenuId)
	{
		return $this->tableGateway->delete(array(
				'id' => $mainMenuId
		));
	}
}
