<?php 
namespace Menu\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModelInterface;
use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class MenuMenusTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('menu_menus', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getMenu($menuId)
	{
		$select = new Select('menu_menus');
		$select->where(array(
				'id' => $menuId
		));
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getMenuByParentId($menuId)
	{
	    $select = new Select('menu_menus');
	    $select->where(array(
	        'parent_id' => $menuId
	    ));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function deleteMenu($menuId)
	{
		if ($menuId) {
			return $this->tableGateway->delete(array(
					'id' => $menuId
			));
		}
	}
	
	public function editUlLi($menuId, $menuData)
	{
	    $data = array(
	        "class_name_ul" => $menuData["class_name_ul"],
	        "class_name_li" => $menuData["class_name_li"],
	    );
	    return $this->tableGateway->update($data,  array('id' => $menuId));
	}
	
	public function editMenu($menuId, $menuData)
	{
	    $data = array(
	        "caption" => $menuData["caption"],
	        "external_url" => $menuData["external_url"],
	        "internal_url" => ($menuData["internal_url"]==""?null:$menuData["internal_url"]),
	        "order" => $menuData["order"],
	        "width" => $menuData["width"],
	        "icon" => $menuData["icon"],
	        "image" => $menuData["image"],
	        "active" => $menuData["active"],
	    //    "class_name_ul" => $menuData["class_name_ul"],
	    //     "class_name_li" => $menuData["class_name_li"],
	        "parent_id" => $menuData["parent_id"],
	    );
	    return $this->tableGateway->update($data,  array('id' => $menuId));
	}
	public function addMenu($mainMenuId, $menuData)
	{
		$data = array(
				"main_menu_id" => $mainMenuId,
				"caption" => $menuData["caption"],
    		    "external_url" => $menuData["external_url"],
    		    "internal_url" => ($menuData["internal_url"]==""?null:$menuData["internal_url"]),
				"order" => $menuData["order"],
		        "width" => $menuData["width"],
		        "icon" => $menuData["icon"],
		        "image" => $menuData["image"],
		        "active" => $menuData["active"],
    		    "parent_id" => $menuData["parent_id"],
		);
		$this->tableGateway->insert($data);
		return $this->tableGateway->getLastInsertValue();
	}
	
	public function getMenuChilds($menuId)
	{
	    $ALLDATA = array();
	    $Cond = '';
	    $Tree = $this->tableGateway->select(array('parent_id = ?' => $menuId));
	    $statement = $this->adapter->query("
	        SELECT id FROM menu_menus
	        WHERE parent_id = $menuId AND id <> $menuId   ");
	    $Tree = $statement->execute();
	    $myData = array();
	    if($Tree)
	    {
	        $IDS = false;
	        foreach ($Tree as $key => $value) {
	            if(isset($value['id']))
	            {
	                $myData[] =   $value['id'];
	            }
	        }
	        $ALLDATA[] = $IDS = implode(",", $myData);
	    }
	    $myData = null;
	    if($IDS)
	    {
	        do
	        {
	     	 	    $statement = $this->adapter->query("
             			SELECT id  FROM menu_menus
             	 	            WHERE parent_id IN (".$IDS.")");
	     	 	    $results = $statement->execute();
	     	 	    if($results)
	     	 	    {
	     	 	        $IDS = false;
	     	 	        $myData = null;
	     	 	        foreach ($results as $key => $value) {
	     	 	            if(isset($value['id']))
	     	 	            {
	     	 	                $myData[] =   $value['id'];
	     	 	                //   var_dump($IDS);
	     	 	            }
	     	 	        }
	     	 	        if ($myData) {
	     	 	            $ALLDATA[] = $IDS = implode(",", $myData);
	     	 	        }
	     	 	    } else
	     	 	        $IDS = false;
	        }while($IDS);
	         
	    }
	    return $ALLDATA;
	}
    
	public function updateMenuActive($menuId,$active,$title=null)
	{//var_dump($menuId,$active);die;
	    return $this->tableGateway->update(array('active' => $active,'caption' => $title),  array('id' => $menuId));
	}
	
	public function updateMenu($menuId,$title)
	{
	    return $this->tableGateway->update(array('caption' => $title),  array('id' => $menuId));
	}
}
