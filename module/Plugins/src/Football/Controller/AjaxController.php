<?php

namespace Football\Controller;

use Application\Helper\BaseCustomerController;
use ProductComment\Model\ProductCommentTable;
use ProductComment\Form\ProdcutCommentForm;
use Zend\Mvc\Controller\AbstractActionController;
use Contact\Model\ContactTable;
use Configuration\Model\ConfigurationTable;
use Contact\Form\ContactForm;
use Football\Model\TeamTable;
use Football\Model\LeagueTable;
use Football\Model\LeagueTeamGameTable;
use Football\Model\GameTable;

class AjaxController extends AbstractActionController {
	public function getweekAction() {
	 
		$request = $this->getRequest (); 
		$data = $request->getPost ()->toArray ();
		if ($data ['leagueId']) {
			$footballTeamTable = new LeagueTable( $this->getServiceLocator () );
			$footballTeamTableData = $footballTeamTable->getRecordsById($data ['leagueId']);
			if ($footballTeamTableData) {
				die($footballTeamTableData['week_count']);
			}
			die(false);
		}
			die(false);
	}
	public function getteamAction() {
		$golezade = 0;
		$golekhorde = 0;
		$request = $this->getRequest ();
		$gameTableData = array ();
		$data = $request->getPost ()->toArray ();
		$resultDataArray = array();
		
		if ($data ['leagueId']) {
			$leagueId = $data ['leagueId']; 
			$footballTable = new LeagueTeamGameTable ( $this->getServiceLocator () );
			$footballGameTable = new GameTable ( $this->getServiceLocator () );
			$footballTeamTable = new TeamTable ( $this->getServiceLocator () );
			$footballTeamTableData = $footballTeamTable->getAllRecords ();
			if ($footballTeamTableData) {
				foreach ( $footballTeamTableData as &$dataTeam ) {
					$golezade = 0;
					$poan = 0;
					$golekhorde = 0;
					$dataTeam ['gole_zade'] = $golezade;
					$dataTeam ['gole_khorde'] = $golekhorde;
					$dataTeam ['poan'] = $poan;
					$dataTeam ['tafazol_gole_khorde'] = 0;
					$gameTableData = $footballTable->getRecordsOfTeamGame ( $leagueId, $dataTeam ['id'] );
					if ($gameTableData) {
						foreach ( $gameTableData as $data ) {
							$footballGameTableData = $footballGameTable->getRecordsById ( $data ['id'] );
							if ($footballGameTableData) {
								if ($dataTeam ['id'] == $footballGameTableData ['team_a']) {
									$golezade += $footballGameTableData ['team_a_gole'];
									$golekhorde += $footballGameTableData ['team_b_gole'];
									// calculate poan
									if ($footballGameTableData ['team_b_gole'] < $footballGameTableData ['team_a_gole']) {
										$poan += $footballGameTableData ['team_a_gole'] * 3;
									} else if ($footballGameTableData ['team_b_gole'] == $footballGameTableData ['team_a_gole']) {
										$poan += 1;
									}
								} else {
									$golezade += $footballGameTableData ['team_b_gole'];
									$golekhorde += $footballGameTableData ['team_a_gole'];
									// calculate poan
									if ($footballGameTableData ['team_a_gole'] < $footballGameTableData ['team_b_gole']) {
										$poan += $footballGameTableData ['team_a_gole'] * 3;
									} else if ($footballGameTableData ['team_b_gole'] == $footballGameTableData ['team_a_gole']) {
										$poan += 1;
									}
								}
							}
							$dataTeam ['gole_zade'] = $golezade;
							$dataTeam ['gole_khorde'] = $golekhorde;
							$dataTeam ['poan'] = $poan;
							$dataTeam ['tafazol_gole_khorde'] = $golezade - $golekhorde;
						} 
					 
					}
				}
			}
			if ($footballTeamTableData) { 
				foreach ($footballTeamTableData as $data) {
					$resultDataArray[$data['poan']] = $data;
				}
	 
			    sort ($resultDataArray);
 
				echo json_encode ( $resultDataArray );
				die ();
			}
		} else {
			die ( false );
		}
	}
 
	public function getprogramAction() {
		$golezade = 0;
		$golekhorde = 0;
		$request = $this->getRequest ();
		$gameTableData = array ();
		$data = $request->getPost ()->toArray ();
	 
		if ($data ['leagueId'] && $data ['weekId']) {
			$league_id = $data ['leagueId'];
			$weekId = $data ['weekId'];
			$programs = array();
			$footballTable = new LeagueTeamGameTable ( $this->getServiceLocator () ); 
			$footballGameTable = new GameTable ( $this->getServiceLocator () );  
			$footballTableData = $footballTable->getRecordsOfweekGame($league_id, $weekId); 
			if ($footballTableData) {
				foreach ($footballTableData as $legteamProgram) {
					$result =  $footballGameTable->getProgramsOfGame($legteamProgram['game_id']);
					if ($result) {
						$programs[$result['id']] =$result;
					}
				}
			} 
				
			echo json_encode ( $programs );
			die ();
		} else {
			die ( false );
		}
	}
}