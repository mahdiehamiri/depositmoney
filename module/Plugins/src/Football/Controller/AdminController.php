<?php

namespace Football\Controller;

use Zend\View\Model\ViewModel;
use Sample\Form\SampleForm;
use Zend\Session\Container;
use Sample\Model\SampleTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Language\Model\LanguageLanguagesTable;
use Football\Form\LeaguesForm;
use Football\Model\LeaguesTable;
use Football\Model\TeamTable;
use Football\Model\GameTable;
use Football\Form\FootballForm;
use Zend\Server\Reflection\ReflectionClass;
use Football\Model\LeagueTable;
use Football\Model\LeagueTeamGameTable;

class AdminController extends BaseAdminController {
	public function listParameterAction() {
		$paramter = 'League';
		$thisparams = $this->params ( 'parameter' );
		$thisparams = ucfirst ( $thisparams );
		if ($thisparams) {
			$paramter = $thisparams;
		}
		$contanier = new Container ( 'token' );
		$csrf = md5 ( time () . rand () );
		$contanier->csrf = $csrf;
		$grid = $this->getServiceLocator ()->get ( 'ZfcDatagrid\Datagrid' );
		
		$config = $this->getServiceLocator ()->get ( 'Config' );
		$dbAdapter = new Adapter ( $config ['db'] );
		$listTableDataRes = array ();
		$table = $paramter . 'Table';
		$game = false;
		$team = false;
		$league = false;
		$tablename = '';
		if ($table == 'TeamTable') {
			$this->setHeadTitle ( __( 'Team list' ) );
			$listTableData = new TeamTable ( $this->getServiceLocator () );
			$listTableDataRes = $listTableData->getRecords ();
			$team = true;
			$tablename = 'team';
		} else if ($table == 'GameTable') {
			$this->setHeadTitle ( __( 'Game list' ) );
			$listTableData = new GameTable ( $this->getServiceLocator () );
			$listTableDataRes = $listTableData->getRecords ();
			$game = true;
			$tablename = 'game';
		} else {
			$this->setHeadTitle ( __( 'Leagues list' ) );
			$listTableData = new LeagueTable ( $this->getServiceLocator () );
			$listTableDataRes = $listTableData->getRecords ();
			
			$league = true;
			$tablename = 'leagues';
		}
		/*
		 * $table = $paramter.'Table';
		 * $cname = "Football\\Model\\".$table;
		 * $listTableData = new $cname($this->getServiceLocator());
		 */
		$grid->setTitle ( __( 'Football category list' ) );
		$grid->setDefaultItemsPerPage ( 15 );
		$grid->setDataSource ( $listTableDataRes, $dbAdapter );
		
		$col = new Column\Select ( 'id', $tablename );
		$col->setLabel ( __( 'Row' ) );
		$col->setIdentity ();
		$col->addStyle ( new Style\Bold () );
		$grid->addColumn ( $col );
		
		$col = new Column\Select ( 'name' );
		$col->setLabel ( __( 'Name' ) );
		$grid->addColumn ( $col );
		
		$btn = new Column\Action\Button ();
		$btn->setLabel ( __( 'Edit' ) );
		$rowId = $btn->getRowIdPlaceholder ();
		
		$viewAction1 = new Column\Action\Icon ();
		$viewAction1->setIconClass ( 'glyphicon glyphicon-edit' );
		$viewAction1->setLink ( "/" . $this->lang . '/admin-football/edit-parameter/parameter/' . $paramter . '/' . $rowId );
		$viewAction1->setTooltipTitle ( __( 'Edit' ) );
		
		$viewAction = new Column\Action\Icon ();
		$viewAction->setIconClass ( 'glyphicon glyphicon-remove' );
		$viewAction->setLink ( "/" . $this->lang . '/admin-samples/delete-parameter/parameter/' . $paramter . '/' . $rowId . '/token/' . $csrf );
		$viewAction->setTooltipTitle ( __( 'Delete' ) );
		
		$actions2 = new Column\Action ();
		$actions2->setLabel ( __( 'Operations' ) );
		$actions2->addAction ( $viewAction1 );
		$actions2->addAction ( $viewAction );
		$actions2->setWidth ( 15 );
		$grid->addColumn ( $actions2 );
		
		$samplesbtn [] = '<a href=' . "/" . $this->lang . '/admin-football/add-parameter/parameter/' . $paramter . ' class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
		$samplesbtn [] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
		$grid->setLink ( $samplesbtn );
		
		$grid->render ();
		return $grid->getResponse ();
	}
	public function editParameterAction() {
		$id = $this->params ( 'id' );
		$paramter = $this->params ( 'parameter' );
		$paramter = ucfirst ( $paramter );
		$this->setHeadTitle ( __( 'Edit ' . $paramter ) );
		if (! $paramter) {
			return $this->redirect ()->toUrl ( "/admin-football");
		}
		$table = $paramter . 'Table';
		$game = false;
		$team = false;
		$league = false;
		$tablename = '';
		
		// SELECT MODELS
		if ($table == 'TeamTable') {
			$listTableData = new TeamTable ( $this->getServiceLocator () );
			$team = true;
			$tablename = 'team';
		} else if ($table == 'GameTable') {
			$listTableData = new GameTable ( $this->getServiceLocator () );
			$game = true;
			$tablename = 'game';
		} else {
			$listTableData = new LeagueTable ( $this->getServiceLocator () );
			$league = true;
			$tablename = 'leagues';
		}
		if ($id) {
			$editableData = $listTableData->getRecordsById ( $id );
			if (! $editableData) {
				return $this->redirect ()->toUrl ( "/admin-football/parameter/" . $paramter );
			}
		} else {
			$this->flashMessenger ()->addErrorMessage ( __("Bad request") );
			$this->flashMessenger ()->addErrorMessage ( __("Operation done successfully.") );
			return $this->redirect ()->toUrl ( "/admin-football/parameter/" . $paramter );
		}
		// PUT FIELDS
		$footbalFieldArray = array ();
		if ($paramter == 'League') {
			$footbalFieldArray = $this->getFootballFiels ( 'League' );
		}
		if ($paramter == 'Game') {
			$footbalFieldArray = $this->getFootballFiels ( 'Game' );
		}
		if ($paramter == 'Team') {
			$footbalFieldArray = $this->getFootballFiels ( 'Team' );
		}
		$footbalForm = new FootballForm ( $footbalFieldArray, $paramter );
		$footbalForm->populateValues ( $editableData );
		$request = $this->getRequest ();
		if ($request->isPost ()) {
			$return = false;
			$data = $request->getPost ()->toArray ();
			$footbalForm->setData ( $data );
			if (isset ( $data ['submitandreturn'] )) {
				$return = true;
			}
			$teamIsNotValid = false;
			if ($footbalForm->isValid ()) {
				$validData = $footbalForm->getData ();
				unset ( $validData ['csrf'] );
				unset ( $validData ['submitandreturn'] );
				unset ( $validData ['submit'] );
				try {
					if ($game) {
						$teamTableData = new TeamTable ( $this->getServiceLocator () );
						$nameTeamA = $teamTableData->getRecordsById ( $validData ['team_a'] );
						$nameTeamB = $teamTableData->getRecordsById ( $validData ['team_b'] );
						$validData ['name'] = $nameTeamA ['name'] . '_' . $nameTeamB ['name'];
						if ($validData ['team_a'] == $validData ['team_b']) {
							$teamIsNotValid = true;
							throw new \Exception ( "The field is undefined.");
						}
					}
					
					$isUpdate = $listTableData->editData ( $validData, $id );
					if (($isUpdate !== false) && $return) {
						$this->layout ()->successMessage = __("Operation done successfully.");
					} else {
						if (($isUpdate !== false) && ! $return) {
							$this->flashMessenger ()->addSuccessMessage ( __("Operation done successfully.") );
							return $this->redirect ()->toUrl ( "/admin-football/parameter/" . $paramter );
						}
					}
				} catch ( \Exception $e ) {
					if ($teamIsNotValid) {
						$this->layout ()->errorMessage = __("team A and B should be different");
					} else {
						$this->layout ()->errorMessage = __("Duplicated name! please choose another one!");
					}
				}
			} else {
				$s = $footbalForm->getMessages ();
				$this->layout ()->errorMessage = $this->generalhelper ()->getFormErrors ( $footbalForm, true );
			}
		}
		
		$view ['title'] = $paramter;
		$view ['footbalForm'] = $footbalForm;
		return new ViewModel ( $view );
	}
	public function addParameterAction() {
		$paramter = $this->params ( 'parameter' );
		$paramter = ucfirst ( $paramter );
		$this->setHeadTitle ( __( 'Add ' . $paramter ) );
		if (! $paramter) {
			return $this->redirect ()->toUrl ( "/admin-football");
		}
		
		$table = $paramter . 'Table';
		$game = false;
		$team = false;
		$league = false;
		$tablename = '';
		
		// SELECT MODELS
		if ($table == 'TeamTable') {
			$listTableData = new TeamTable ( $this->getServiceLocator () );
			$team = true;
			$tablename = 'team';
		} else if ($table == 'GameTable') {
			$listTableData = new GameTable ( $this->getServiceLocator () );
			$game = true;
			$tablename = 'game';
		} else {
			$listTableData = new LeagueTable ( $this->getServiceLocator () );
			$league = true;
			$tablename = 'leagues';
		}
		$time = date ( "H:i:s");
		
		// PUT FIELDS
		$footbalFieldArray = array ();
		if ($paramter == 'League') {
			$footbalFieldArray = $this->getFootballFiels ( 'League' );
		}
		if ($paramter == 'Game') {
			$footbalFieldArray = $this->getFootballFiels ( 'Game' );
		}
		if ($paramter == 'Team') {
			$footbalFieldArray = $this->getFootballFiels ( 'Team' );
		}
		$footbalForm = new FootballForm ( $footbalFieldArray, $paramter );
		
		$request = $this->getRequest ();
		if ($request->isPost ()) {
			$return = false;
			$data = $request->getPost ()->toArray ();
			$footbalForm->setData ( $data );
			if (isset ( $data ['submitandreturn'] )) {
				$return = true;
			}
			$teamIsNotValid = false;
			if ($footbalForm->isValid ()) {
				
				$validData = $footbalForm->getData ();
				
				unset ( $validData ['csrf'] );
				unset ( $validData ['submitandreturn'] );
				unset ( $validData ['submit'] );
				try {
					if ($game) {
						$teamTableData = new TeamTable ( $this->getServiceLocator () );
						$nameTeamA = $teamTableData->getRecordsById ( $validData ['team_a'] );
						$nameTeamB = $teamTableData->getRecordsById ( $validData ['team_b'] );
						$validData ['name'] = $nameTeamA ['name'] . '_' . $nameTeamB ['name'];
						if ($validData ['team_a'] == $validData ['team_b']) {
							$teamIsNotValid = true;
							throw new \Exception ( "The field is undefined.");
						}
					}
					
					$isAdded = $listTableData->addData ( $validData );
					if ($game) {
						$lgtData = array( 
							'team_id' => $validData ['team_a'],
							'league_id' => $validData ['league_id'],
							'game_id' => $isAdded,
							'week_id' => $validData ['game_week'], 
						);
						$lgtData1 = array( 
							'team_id' => $validData ['team_b'],
							'league_id' => $validData ['league_id'],
							'game_id' => $isAdded,
							'week_id' => $validData ['game_week'], 
						);
						$leagueTeamGame = new LeagueTeamGameTable( $this->getServiceLocator () );
						  $leagueTeamGame->addData($lgtData);
						 $leagueTeamGame->addData($lgtData1);
					}
					if ($isAdded && $return) {
						$this->layout ()->successMessage = __("Operation done successfully.");
					} else {
						if ($isAdded && ! $return) {
							$this->flashMessenger ()->addSuccessMessage ( __("Operation done successfully.") );
							return $this->redirect ()->toUrl ( "/admin-football/parameter/" . $paramter );
						}
					}
				} catch ( \Exception $e ) {
					if ($teamIsNotValid) {
						$this->layout ()->errorMessage = __("team A and B should be different");
					} else {
						$this->layout ()->errorMessage = __("Duplicated name! please choose another one!");
					}
				}
			} else {
				$s = $footbalForm->getMessages ();
				$this->layout ()->errorMessage = $this->generalhelper ()->getFormErrors ( $footbalForm, true );
			}
		}
		
		$view ['title'] = $paramter;
		$view ['footbalForm'] = $footbalForm;
		return new ViewModel ( $view );
	}
	public function deleteParameterAction() {
		/*
		 * $container = new Container('token');
		 * $token = $this->params('token');
		 * $galleriesTable = new GalleriesTable($this->getServiceLocator());
		 * $galleryFilesTable = new \Gallery\Model\GalleryFilesTable($this->getServiceLocator());
		 *
		 * $Id = $this->params('id', -1);
		 * if ($Id && $container->offsetExists('csrf') && $container->offsetGet('csrf') === $token) {
		 * $Isdelete = $galleriesTable->deleteGallery($Id);
		 * if($Isdelete){
		 * $allImageForDelete = $galleryFilesTable->getAllImagesById($Id);
		 * $deleteImages = $galleryFilesTable->deleteByGalleryId($Id);
		 * if($deleteImages){
		 * foreach ($allImageForDelete as $image){
		 * if(file_exists($_SERVER['DOCUMENT_ROOT']."/uploads/gallery/".$image['file_name']) )
		 * @unlink($_SERVER['DOCUMENT_ROOT']."/uploads/gallery/".$image['file_name']);
		 * }
		 * }
		 * }
		 * $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
		 * return $this->redirect()->toRoute('gallery-admin');
		 * }
		 */
	}
	public function getFootballFiels($table) {
		switch ($table) {
			case 'League' :
				return array (
						'name' => array (
								'type' => 'Text',
								'requierd' => true,
								'class' => 'form-control validate[required]',
								'label' => __("Name Of League"),
								'placeholder' => __("Name Of League") 
						),
						'week_count' => array (
								'type' => 'Text',
								'requierd' => true,
								'class' => 'form-control validate[required]',
								'label' => __("Week Count"),
								'placeholder' => __("Week Count Of League") 
						),
						'from' => array (
								'type' => 'Text',
								'class' => 'form-control validate[required]',
								'requierd' => 'required',
								'label' => __("From Year"),
								'placeholder' => __("Enter From Year") 
						),
						'to' => array (
								'type' => 'Text',
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("To Year"),
								'placeholder' => __("Enter To Year") 
						),
						'order' => array (
								'type' => 'Text',
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Order"),
								'placeholder' => __("list of league importance") 
						),
						'start_date' => array (
								'type' => 'Text',
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Start Date"),
								'placeholder' => __("list of league importance") 
						),
						'status' => array (
								'type' => 'Checkbox',
								'label' => __("Status") 
						) 
				);
			
			case 'Team' :
				return array (
						'name' => array (
								'type' => 'Text',
								'requierd' => true,
								'class' => 'form-control validate[required]',
								'label' => __("Name Of Team"),
								'placeholder' => __("Name Of Team") 
						),
						
						'status' => array (
								'type' => 'Checkbox',
								'label' => __("Status") 
						) 
				);
			case 'Game' : 
				$teamTable = new TeamTable ( $this->getServiceLocator () );
				$teamTableData = $teamTable->getAllRecords ();
				
				$leagueTable = new LeagueTable ( $this->getServiceLocator () );
				$leagueTableData = $leagueTable->getAllRecords ();
				
				return array (
						'league_id' => array (
								'type' => 'Select',
								'optionDataArray' => $leagueTableData,
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Name Of League"),
								'placeholder' => __("Please Name Of Team A") 
						),
						'team_a' => array (
								'type' => 'Select',
								'optionDataArray' => $teamTableData,
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Name Of Team A"),
								'placeholder' => __("Please Name Of Team A") 
						),
						'team_b' => array (
								'type' => 'Select',
								'optionDataArray' => $teamTableData,
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Name Of Team B"),
								'placeholder' => __("Please Name Of Team B") 
						),
						'team_a_gole' => array (
								'type' => 'Text',
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Gole Count Team A"),
								'placeholder' => __("Please Add Gole Count Team A") 
						),
						
						'team_b_gole' => array (
								'type' => 'Text',
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Gole Count Team B"),
								'placeholder' => __("Please Add Gole Count Team B") 
						),
						
						'game_date' => array (
								'type' => 'Text',
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Game Date") 
						),
						
						'game_time' => array (
								'type' => 'Text',
								'requierd' => 'required',
								'class' => 'form-control validate[required]',
								'label' => __("Game Time") 
						),
						
						'game_week' => array (
								'type' => 'Select',
								'requierd' => 'required',
								'optionDataArray' => '',
								'class' => 'form-control validate[required]',
								'label' => __("Game Week") 
						 ),
						
						'status' => array (
								'type' => 'Checkbox',
								'label' => __("Status") 
						) 
				);
		}
	}
	public function addImage($validData, $uploadDir) {
		$newFileName = md5 ( time () . $validData ['name'] );
		$validationExt = "jpg,jpeg,png,gif";
		$isUploaded = $this->imageUploader ()->UploadFile ( $validData, $validationExt, $uploadDir, $newFileName );
		return $isUploaded;
	}
}
