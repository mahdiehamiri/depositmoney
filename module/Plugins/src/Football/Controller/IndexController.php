<?php

namespace Football\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Link\Form\LinkForm;
use Link\Model\LinkTable;
use Link\Model\LinkCategoryTable;
use Application\Helper\BasePluginController;
use Football\Model\LeagueTable;
use Football\Model\LeagueTeamGameTable;
use Football\Model\GameTable;
use Football\Model\TeamTable;
use Application\Helper\GeneralHelper;
use Application\Helper\Jdates;

class IndexController extends BasePluginController {
	protected $linkTable;
	public function onDispatch(MvcEvent $e) {
		parent::onDispatch ( $e );
	}
	public function indexAction($params) {
		$viewModel = new ViewModel ();
		$lang = $this->lang;
		$position = 0;
		if ($params) {
			if (isset ( $params ['position'] )) {
				$position = $params ['position'];
			}
		}
		$footballTeamTable = new TeamTable ( $this->getServiceLocator () );
		$footballTable = new LeagueTeamGameTable ( $this->getServiceLocator () );
		$leagueTable = new LeagueTable ( $this->getServiceLocator () );
		$leagueTableData = $leagueTable->getAllRecords ();
		$footballGameTable = new GameTable ( $this->getServiceLocator () );
		$resultTeam = array ();
		if ($leagueTableData) {
			$dataTeam = $leagueTableData [0];
			$golezade = 0;
			$poan = 0;
			$golekhorde = 0;
			$gameTableData = $footballTable->getRecordsOfTeamGame ( $dataTeam ['id'] );
			if ($gameTableData) {
				foreach ( $gameTableData as $data ) {
					$teamRecordesData = $footballTeamTable->getRecordsById ( $data ['team_id'] );
					$footballGameTableData = $footballGameTable->getRecordsById ( $data ['id'] );
					if ($footballGameTableData) {
						if ($dataTeam ['id'] == $footballGameTableData ['team_a']) {
							$golezade += $footballGameTableData ['team_a_gole'];
							$golekhorde += $footballGameTableData ['team_b_gole'];
							// calculate poan
							if ($footballGameTableData ['team_b_gole'] < $footballGameTableData ['team_a_gole']) {
								$poan += $footballGameTableData ['team_a_gole'] * 3;
							} else if ($footballGameTableData ['team_b_gole'] == $footballGameTableData ['team_a_gole']) {
								$poan += 1;
							}
						} else {
							$golezade   += $footballGameTableData ['team_b_gole'];
							$golekhorde += $footballGameTableData ['team_a_gole'];
							// calculate poan
							if ($footballGameTableData ['team_a_gole'] < $footballGameTableData ['team_b_gole']) {
								$poan += $footballGameTableData ['team_a_gole'] * 3;
							} else if ($footballGameTableData ['team_b_gole'] == $footballGameTableData ['team_a_gole']) {
								$poan += 1;
							}
						}
					}
					$resultTeam [$teamRecordesData ['name']] ['gole_zade'] = $golezade;
					$resultTeam [$teamRecordesData ['name']] ['gole_khorde'] = $golekhorde;
					$resultTeam [$teamRecordesData ['name']] ['poan'] = $poan;
					$resultTeam [$teamRecordesData ['name']] ['tafazol_gole_khorde'] = $golezade - $golekhorde;
				}
			} 
			$viewModel->setVariable ( 'currentleagues', $leagueTableData [0] );
			$viewModel->setVariable ( 'resultTeam', $resultTeam );
			$viewModel->setVariable ( 'leagues', $leagueTableData );
		}
		$viewModel->setTemplate ( 'football/index/' . $position . '.phtml' );
		return $viewModel;
	}
	public function programAction($params) {
		$viewModel = new ViewModel (); 
		// MODELES
		$footballTeamTable = new TeamTable ( $this->getServiceLocator () );
		$footballTable = new LeagueTeamGameTable ( $this->getServiceLocator () );
		$leagueTable = new LeagueTable ( $this->getServiceLocator () );
		$footballGameTable = new GameTable ( $this->getServiceLocator () );
		$programs = array();
		$leagueTableData = $leagueTable->getAllRecords ();
		$resultTeam = array ();
		if ($leagueTableData) {
	 		// START ******************************** get league in week
				$currentLeague = $leagueTableData[0];
				$startData = $currentLeague['start_date'];
				
				$thisweek = $this->getGameDataWeekByWeek($startData);
		  
				$footballTableData = $footballTable->getRecordsOfweekGame($currentLeague['id'], $thisweek);
	 		//EDND **********************************
 
				
	 		// START ******************************** get league in week
				if ($footballTableData) {
					foreach ($footballTableData as $legteamProgram) {
						$result =  $footballGameTable->getProgramsOfGame($legteamProgram['game_id']);
						if ($result) {
							$programs[$result['id']] =$result;
						}
					}
				}
	 		//EDND **********************************
  
			$viewModel->setVariable ( 'currentleagues', $leagueTableData [0] );
			$viewModel->setVariable ( 'programs', $programs );
			$viewModel->setVariable ( 'weekId', $thisweek );
			$viewModel->setVariable ( 'leagues', $leagueTableData );
		}
		$viewModel->setTemplate ( 'football/index/program.phtml' );
		return $viewModel;
	}
	function getGameDataWeekByWeek($startData) {
	 
		$startData = explode('/', $startData);
		$g_y = $startData[0];
		$g_m = $startData[1];
		$g_d = $startData[2]; 
		$todayString = date('Y/m/d');
		
		$startOfLeageArray = Jdates::jalali_to_gregorian($g_y, $g_m, $g_d);
		$startOfLeageString = $startOfLeageArray[0]."/".$startOfLeageArray[1]."/".$startOfLeageArray[2];
		
		// get Difference between today and start day
		$datetime1 = new \DateTime($startOfLeageString);
		$datetime2 = new \DateTime($todayString);
		$interval = $datetime1->diff($datetime2);
		$weekCount =   ceil($interval->format('%a')/7);
       return $weekCount;
	}
}
