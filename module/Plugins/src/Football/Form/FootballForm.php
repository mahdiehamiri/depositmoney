<?php

namespace Football\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Validator\Identical;

class FootballForm extends Form {
	public function __construct($footbalFieldArray, $parameter) {
		parent::__construct ( 'Football Form' );
		$this->setAttributes ( array (
				'action' => '',
				'method' => 'post',
				'id' => 'footballForm',
				"enctype" => "multipart/form-data",
				'novalidate' => true 
		) );
		if ($footbalFieldArray) {
			foreach ( $footbalFieldArray as $key => $dataForm ) {
				if ($dataForm ['type'] == 'Text') {
					$element = new Element\Text ($key);
					$element->setAttributes ( array ( 
							'id' => $key, 
							'class' => $dataForm ['class'], 
							'placeholder' => (isset($dataForm ['placeholder']) ? $dataForm ['placeholder']:'') 
					) );
					if (isset ( $dataForm ['required'] )) {
						$element->setAttributes ( array (
								'required' => $dataForm ['required'] 
						) );
					}
					$element->setLabel ( $dataForm ['label'] );
					$this->add ( $element );
				}
				if ($dataForm ['type'] == 'Select') {
					$element = new Element\Select ($key);
					$optionsData = $dataForm['optionDataArray'];
					$options = array();
					if ($optionsData) {
						foreach ($optionsData as $data) {
							$options[$data['id']] = $data['name'];
						}
					}
					$element->setValueOptions($options);
					$element->setAttributes ( array ( 
							'id' => $key,
							'class' => $dataForm ['class'],  
					) );
					if (isset ( $dataForm ['required'] )) {
						$element->setAttributes ( array (
								'required' => $dataForm ['required'] 
						) );
					}
					$element->setLabel ( $dataForm ['label'] );
					$this->add ( $element );
				}
			}
		}
		
		$status = new Element\Checkbox ( 'status' );
		$status->setAttributes ( array (
				'id' => 'status',
				'placeholder' => __( 'Status' ) 
		) );
		$status->setLabel ( __( 'Status' ) );
		$this->add ( $status );
		
		//$csrf = new Element\Csrf ( 'csrf' );
		//$this->add ( $csrf );
		
		$submit = new Element\Submit ( 'submit' );
		$submit->setValue ( __( 'Save and Close' ) );
		$submit->setAttributes ( array (
				'id' => 'submitLink',
				'class' => 'btn btn-primary' 
		) );
		$this->add ( $submit );
		
		$submit = new Element\Submit ( 'submitandreturn' );
		$submit->setValue ( __( 'Save and New' ) );
		$submit->setAttributes ( array (
				'id' => 'submitLink',
				'class' => 'btn btn-info' 
		) );
		$this->add ( $submit );
		
		$this->inputFilter ($parameter);
	}
	public function inputFilter($parameter) {
		$inputFilter = new InputFilter ();
		$factory = new InputFactory ();
		if ($parameter != 'Game') {
			$inputFilter->add ( $factory->createInput ( array (
					'name' => 'name',
					'required' => true,
					'filters' => array (
							array (
									'name' => 'StripTags' 
							) 
					) 
			) ) );
		 }
 	 	if ($parameter == 'Game') {
 	 		$inputFilter->add ( $factory->createInput ( array (
 	 				'name' => 'team_a',
 	 				'required' => true, 
 	 		) ) );
 	 		$inputFilter->add ( $factory->createInput ( array (
 	 				'name' => 'team_b',
 	 				'required' => true, 
 	 		) ) );
 	 		$inputFilter->add ( $factory->createInput ( array (
 	 				'name' => 'league_id',
 	 				'required' => true, 
 	 		) ) ); 
 
		}  
		$this->setInputFilter ( $inputFilter );
		return $inputFilter;
	}
	/*
	 * public function isValid()
	 * {
	 * $this->getInputFilter()->remove('intro_img');
	 * $this->getInputFilter()->remove('mobile_view_img');
	 * $this->getInputFilter()->remove('tablet_view_img');
	 * $this->getInputFilter()->remove('website_view_img');
	 * return parent::isValid();
	 * }
	 */
}
