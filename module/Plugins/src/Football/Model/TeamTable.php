<?php
namespace Football\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class TeamTable extends BaseModel
{

    protected $tableGateway;

    protected $adapter;

    protected $serviceManager;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('team', $this->getDbAdapter());
    }

	public function getRecords()
	{
		 $select = new Select('team');
	    return $select;
	}
	
	public function getRecordsById($id)
	{
		$select = new Select('team');
		$select->where->equalTo('id', $id);
		return $this->tableGateway->selectWith($select)->current();
	    
	} 
	public function getAllRecords()
	{
		$select = new Select('team');
		$select->where->equalTo('status', 1);
		return $this->tableGateway->selectWith($select)->toArray();
	}
 
	public function addData($data)
	{
		 $this->tableGateway->insert( $data);
		 return $this->tableGateway->lastInsertValue;
	}
	
	public function editData($data, $samplesId)
	{
		return $this->tableGateway->update($data, array('id' => $samplesId));		
	}
	
	public function deleteDataById($samplesId)
	{
		return $this->tableGateway->delete(array(
				'id' => $samplesId
		));
	}  
	 
}
