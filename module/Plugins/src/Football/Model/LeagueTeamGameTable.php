<?php 
namespace Football\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class LeagueTeamGameTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('league_team_game', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	public function addData($data)
	{
		return $this->tableGateway->insert($data);
	}
	public function deleteFootballCategory($samplesId)
	{
		return $this->tableGateway->delete(array(
				'id' => $samplesId
		));
	}
	public function getGameFullRecode($leagueId, $weekId)
	{
		$select = new Select('league_team_game');
		$select->join('leagues', 'leagues.id = league_team_game.league_id', array(),'left'); 
		$select->join('game', 'game.id = league_team_game.game_id', array('team_a','team_b','team_a_gole','team_b_gole','game_week'),'left');
		
		$select->where->equalTo('league_team_game.league_id', $leagueId);
		$select->where->equalTo('league_team_game.week_id', $weekId);
		$select->where->equalTo('leagues.status', 1);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getRecordsOfTeamGame($league_id, $teamId = false) {
		$select = new Select ( 'league_team_game' );
		$select->where->equalTo ( 'league_id', $league_id );
		if ($teamId) {
			$select->where->equalTo ( 'team_id', $teamId );
		}
		return $this->tableGateway->selectWith ( $select )->toArray ();
	}
	public function getRecordsOfweekGame($league_id, $weekId = false) {
		$select = new Select ( 'league_team_game' );
		$select->where->equalTo ( 'league_id', $league_id );
		if ($weekId) {
			$select->where->equalTo ( 'week_id', $weekId );
		}
		return $this->tableGateway->selectWith ( $select )->toArray ();
	}
	
	public function getRecordsOfTeamWeek($league_id, $teamId) {
		$select = new Select ( 'league_team_game' );
		$select->where->equalTo ( 'league_id', $league_id );
		$select->where->equalTo ( 'week_id', $teamId );
		return $this->tableGateway->selectWith ( $select )->toArray ();
	}
	
}
