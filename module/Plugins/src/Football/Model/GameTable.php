<?php

namespace Football\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;

class GameTable extends BaseModel {
	protected $tableGateway;
	protected $adapter;
	protected $serviceManager;
	public function __construct($sm) {
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway ( 'game', $this->getDbAdapter () );
	}
	public function getRecords() {
		$select = new Select ( 'game' );
		return $select;
	}
	public function getRecordsCategorySample($id = false) {
		return $this->tableGateway->select ()->toArray ();
	}
    public function getRecordsById($id) {
		$select = new Select ( 'game' );
		$select->where->equalTo ( 'id', $id );
		return $this->tableGateway->selectWith ( $select )->current ();
	}
    public function getProgramsOfGame($id) {
		$select = new Select ( 'game' );
		$select->where->equalTo ( 'game.id', $id );
		$select->where->equalTo ( 'game.status', 1 );
		$select->join(array('teama' => 'team'), 'teama.id = game.team_a', array('team_a_name' => 'name'), Select::JOIN_LEFT);
		$select->join(array('teamb' => 'team'), 'teamb.id = game.team_b', array('team_b_name' => 'name'), Select::JOIN_LEFT);
		return $this->tableGateway->selectWith ( $select )->current ();
	}
    public function getRecordsOfTeamWeek($league_id, $teamId, $weekId) {
		$select = new Select ( 'game' ); 
		$select->where->equalTo ( 'team_a', $id );
		$select->where->equalTo ( 'id', $id );
		return $this->tableGateway->selectWith ( $select )->current ();
	}
  
	public function addData($data) {
		$this->tableGateway->insert ( $data );
		return $this->tableGateway->lastInsertValue;
	}
	public function editData($data, $samplesId) {
		return $this->tableGateway->update ( $data, array (
				'id' => $samplesId 
		) );
	}
	public function deleteDataById($samplesId) {
		return $this->tableGateway->delete ( array (
				'id' => $samplesId 
		) );
	}
}
