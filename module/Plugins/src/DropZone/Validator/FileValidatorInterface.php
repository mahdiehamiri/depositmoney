<?php 
namespace DropZone\Validator;

interface FileValidatorInterface
{
	public function uploadCurrentFile();
}
?>