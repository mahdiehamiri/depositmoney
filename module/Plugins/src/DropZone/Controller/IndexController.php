<?php
namespace DropZone\Controller;
 
use Zend\View\Model\ViewModel; 
use Zend\Session\Container;
use Zend\Db\Sql\Where; 
use DropZone\Model\GalleryFilesTable; 
use Application\Helper\BasePluginController;

/* @var $attributesScoresTable \Management\Model\AttributesScoresTable */
/* @var $customerProductScoresTable \Management\Model\CustomerProductScoresTable */
class IndexController extends BasePluginController 
{ 
    
    protected $factoryModel;
    protected $galleryfilesTable;
  
 
	public function indexAction($options)
	{    
		$serviceManager = $this->getServiceLocator();
		$configName = $options["configName"];
		$view['extraColumnsValue'] = $options["extraColumnsValue"];
		$configs = $serviceManager->get('Config');
		if (!isset($configs['drop-zone-configs']) && !isset($configs['drop-zone-configs'][$configName])) {
			echo _("Config not found!");
			return ;
		} else {
			$configOptions = $configs['drop-zone-configs'][$configName];
			$view['configName'] = md5(sha1($configName));
			$controller = $this;
    		$token = $this->generateDropZoneToken('tokenDropZone');
			$view['token'] = $token;
			$view['options'] = $configOptions;
			// initialize
			$where = new Where();
			//$where->equalTo("format", $configOptions["format"]);
			$where = array ();
			  if ($configOptions["extraColumns"]) {
				foreach ($configOptions["extraColumns"] as $k => $v) { 
					if ($options["extraColumnsValue"] &&  isset($options["extraColumnsValue"][$v])) {
					    if ($v != "user_id")
						 $where[] = ($v.'='.$options["extraColumnsValue"][$v]);
					}
				}
			}  
			$galleryFilesTable = new $configOptions['dbTable']($this->getServiceLocator());
		
    		//$uploadTable = $controller->getModel('DropZone', $configOptions['dbTable']);
			$filesInfo = $galleryFilesTable->getData($where);
			if ($filesInfo) {
				$view['filesInfo'] = $filesInfo;
			} 
		}
		$viewModel = new ViewModel($view);
		$viewModel->setTemplate("drop-zone/index/index.phtml");
		return $viewModel; 
		
	}
	public function generateDropZoneToken($sessionName = 'tokenDropZone')
	{
		$container = new Container('tokenholders');
		$token = md5(sha1(time().rand()).rand());
		$container->offsetSet($sessionName, $token);
		return $token;
	}
	
	
	 
}