<?php

namespace DropZone\Controller;

use Application\Helper\BaseAjaxController;
use Zend\Filter\File\Rename;
use Zend\File\Transfer\Adapter\Http;

use Zend\Session\Container;
use Zend\Db\Sql\Where;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use DropZone\Model\GalleryFilesTable;
use DropZone\Model\GalleryFilesModel;

class AjaxController  extends AbstractActionController  {
    public $userData;

    
    public function onDispatch(MvcEvent $e)
    {
        $this->layout("layout/empty.phtml");
        $authenticationService = $this->getServiceLocator()->get('AuthenticationService');
	    $this->userData = $authenticationService->isLoggedIn();
        if (!$this->userData) {
            return $this->redirect()->toRoute('home');
        }
        parent::onDispatch($e);
    }

	public function specialAction() {
		$request = $this->getRequest ();
		if ($request->isPost () ) {
			$postData = $request->getPost ();
			//var_dump($postData); die;
			$configHash = $postData['configName']; 
			$serviceManager = $this->getServiceLocator();
			$configs = $serviceManager->get('Config');
			$configOptions = array();
			if (!isset($configs['drop-zone-configs'])) {
				die("0");
			}
			foreach ($configs['drop-zone-configs'] as $configName => $config) {
				if ($configHash == md5(sha1($configName))) {
					$configOptions = $config;
					break;
				}
			}
			if (!$configOptions)
				die("0");
			if ($configOptions["canSpecialUploadedFile"] == "false")
				die("0");
			$fileId = $postData["fileId"];
			$extraColumnsValue =(array) json_decode(urldecode($postData["extraColumnsValue"]));
			
			$where = new Where();
			$where->equalTo("id", $fileId);
			$where->equalTo("format", $configOptions["format"]);
			//$where->equalTo("user_id", $this->userData->id);
			if ($configOptions["extraColumns"]) {
				foreach ($configOptions["extraColumns"] as $k => $v) {
					if ($extraColumnsValue && $k !="path"  && $v != "user_id")
						$where->equalTo($v, $extraColumnsValue[$v]);
				}
			}
			//die('yes');
		//	$uploadTable = $this->getModel('DropZone', $configOptions['dbTable']);
			$galleryFilesTable = new $configOptions['dbTable']($this->getServiceLocator());
			$fileInfo = $galleryFilesTable->getData($where);
			if ($fileInfo)
				$isSpecial = $galleryFilesTable->specialData($where, $postData["caption"], $postData["order"]);
			die("$isSpecial");
		} else {
			die("0");
		}
	}
	
	public function deleteAction() {
		$request = $this->getRequest (); 
		
		if ($request->isPost () ) {
			$postData = $request->getPost ();
			if(!$this->checkDropZoneToken($postData["token"])) {
				die("0");	
			}
			$configHash = $postData['configName'];
			$serviceManager = $this->getServiceLocator();
			$configs = $serviceManager->get('Config');
			$configOptions = array();
			if (!isset($configs['drop-zone-configs'])) {
				die("0");
			}
			foreach ($configs['drop-zone-configs'] as $configName => $config) {
				if ($configHash == md5(sha1($configName))) {
					$configOptions = $config;
					break;
				}
			}
			if (!$configOptions)
				die("0");
			if ($configOptions["canDeleteUploadedFile"] == "false")
				die("0");
			$fileId = $postData["fileId"]; 
			$extraColumnsValue =(array) json_decode(urldecode($postData["extraColumnsValue"]));
			$where = new Where();
			$where->equalTo("id", $fileId);
			$where->equalTo("format", $configOptions["format"]);
			//$where->equalTo("user_id", $this->userData->id);
			if ($configOptions["extraColumns"]) {
				foreach ($configOptions["extraColumns"] as $k => $v) {
					if ($extraColumnsValue && $v != "user_id")
						$where->equalTo($v, $extraColumnsValue[$v]);
				}
			}
			
			$galleryFilesTable = new $configOptions['dbTable']($this->getServiceLocator());
			$fileInfo = $galleryFilesTable->getData( $where); 
			if ($fileInfo && is_file($configOptions["uploadPath"] . (isset($postData["path"])?$postData["path"]:"") .  $fileInfo[0]["file_name"]))
				unlink($configOptions["uploadPath"] . (isset($postData["path"])?$postData["path"]:"") .  $fileInfo[0]["file_name"]);
	
			$isDeleted = $galleryFilesTable->deleteData($where);
 
			die("$isDeleted");
		} else {
			die("0");
		}
	}
	public function indexAction() { 
		$request = $this->getRequest ();
		
		$configHash = $this->params('configName'); 
		$extraColumnsValue = (array)json_decode(urldecode($this->params('extraColumnsValue')));
		$serviceManager = $this->getServiceLocator();
		$configs = $serviceManager->get('Config');
		$configOptions = array();
		if (!isset($configs['drop-zone-configs'])) {
			die("error");
		}
		foreach ($configs['drop-zone-configs'] as $configName => $config) {
			if ($configHash == md5(sha1($configName))) {
				$configOptions = $config;
				break;
			}
		}
		if (!$configOptions) 
			die("error");
		//var_dump($configOptions["extraColumns"]); die;
		if ($configOptions["extraColumns"]) {
			foreach ($configOptions["extraColumns"] as $k => $v) {
				if ($this->params('extraColumnsValue') && $k !== "path") {
				 //   if ($v != ' user_id') {
					  $insertData[$v] = $extraColumnsValue[$v];
				 //   }
				}
			}
		}
		
		if ($request->isPost ()) {
		    $galleryFilesTable = new $configOptions['dbTable']($this->getServiceLocator());
			$postData = $request->getPost ();
			$data = $_FILES["file"]; 
			$ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
			$newFileName = md5(sha1(time()));
			$uploadPath  = $configOptions["uploadPath"];
			if (isset($extraColumnsValue["path"])) {
			    $uploadPath = $uploadPath . $extraColumnsValue["path"] . "/";
			}
			$extension	= $configOptions["extensions"];
			$minSize	= $configOptions["minSize"];
			$maxSize	= $configOptions["maxSize"];
			$frontMaxFilesize	= $configOptions["front_maxFilesize"];
 			$className = "DropZone\\Validator\\File\\" . $configOptions['format'];
 			
 			$image = new $className(
								array(
										"extension" 	=> $extension ,
										"minSize"		=> $minSize,
										"maxSize"		=> $maxSize,
										"newFileName"	=> $newFileName,
										"uploadPath"	=> $uploadPath,
									)
 							);
 			
 			$ListData = $galleryFilesTable->getData($insertData);
 
 			if($ListData) {
 		 
 			     $count = count($ListData); 
 			    if ($count >= $frontMaxFilesize) {
                  $response['code']  = 400;
					$response['msg'] = _("More Than max File recieved!");
					echo json_encode($response);
					die();
 			    }
 			}
			$extension = pathinfo($data['name'], PATHINFO_EXTENSION); 
			if ($image->isValid ($data)) {
				//$uploadTable = $this->getModel('DropZone', $configOptions['dbTable']);
		 
				$insertData['user_id'] = $this->userData->id;
				$insertData['format'] = $configOptions['format'];
				$insertData['file_name'] = strtolower($newFileName . ".$extension");
				$insertData["original_file_name"] = $data["name"];
				$insertData["extention"] = strtolower($extension);
				$finfo = new \finfo(FILEINFO_MIME_TYPE);
				$mime = $finfo->file($data['tmp_name']);
				$insertData["mime_type"] = $mime;
				$fileId = $galleryFilesTable->addData ($insertData);
				$uploadStatus = $image->uploadCurrentFile();
				if ($uploadStatus["file"]["received"]) {
					$response['code']  = 200;
					$response['msg'] = $fileId;
					echo json_encode($response);
					die();
				}  else {
					$response['code']  = 400;
					$response['msg'] = _("File is not recieved!");
					echo json_encode($response);
					die();
				}
			} else {
					$response['code']  = 400;
					$response['msg'] = _("File is not valid!");
					echo json_encode($response);
					die();
			}
		}
			
		
	}
	public function checkDropZoneToken($token, $sessionName = 'tokenDropZone')
	{
		$container = new Container('tokenholders');
		if ($container->offsetExists($sessionName)) {
			$sessionToken = $container->offsetGet($sessionName); 
			if ($sessionToken == $token) {
				return true;
			}
		} 
		die($sessionToken);
		return false;
	}
	 
}