<?php

namespace DropZone\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;

class ShopProductVideoFilesTable extends BaseModel 
{
public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('shop_product_video_files', $this->getDbAdapter());
    }
 
	public function addData($data)
	{ 
		$this->tableGateway->insert($data);
		return $this->tableGateway->lastInsertValue;
		
	}
	
	public function deleteData($where)
	{  
		return $this->tableGateway->delete($where);
	}
	
    public function specialData($where, $caption, $order = null)
	{
	    return $this->tableGateway->update(array(
	        'caption'	=> $caption,
	        'order'	=> $order,
	    ), $where);
	
	}
	public function getData( $where=false)
	{  
	    
		if (!$where)
		$where = new Where();
		$select = new Select();
		$select->from("shop_product_video_files");
		$select->where($where); 
		$select->order('order DESC');
		$result= $this->tableGateway->selectWith($select)->toArray(); 
		//var_dump($result); die;
		return $result;
	}
	
	public function getVideoName($pvId, $videoPath)
	{
	    $where = new Where();
	    $select = new Select();
	    $select->from("shop_product_video_files"); 
	    $where->equalTo('product_id', $pvId); 
	    $select->order('order DESC'); 
	    $select->where($where);
	    $videos = $this->tableGateway->selectWith($select)->toArray();
	    if ($videos) {
	        $lastVideo = false;
	        foreach ($videos as $video) {  
	            if (!is_file($videoPath . $video['file_name'])) { 
// 	                $this->tableGateway->deleteData(array(
// 	                    'id' => $video['id']
// 	                ));
                    continue;
	            } else {
	                if ($video['caption']) {
	                    return $video;
	                }
	            }
	            $lastVideo = $video;
	        }
	        return $lastVideo;
	    }
	    return false;
	}
	
	
  
}