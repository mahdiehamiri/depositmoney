<?php

namespace DropZone\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;

class GalleryFilesTable extends BaseModel 
{
public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('gallery_files', $this->getDbAdapter());
    }
 
	public function addData($data)
	{ 
		$this->tableGateway->insert($data);
		return $this->tableGateway->lastInsertValue;
		
	}
	
	public function deleteData($where)
	{  
		return $this->tableGateway->delete($where);
	}
	
	public function specialData($where, $caption, $order = null)
	{
	    return $this->tableGateway->update(array(
	        'caption'	=> $caption,
	        'order'	=> $order,
	    ), $where);
	
	}
	public function getData( $where=false)
	{  
	    
		if (!$where)
		$where = new Where();
		$select = new Select();
		$select->from("gallery_files");
		$select->where($where); 	
		$select->order('order DESC');
		$result= $this->tableGateway->selectWith($select)->toArray(); 
		//var_dump($result); die;
		return $result;
	}
	
	public function getImageName($pvId, $imagePath)
	{
	    $where = new Where();
	    $select = new Select();
	    $select->from("gallery_files"); 
	    $where->equalTo('gallery_id', $pvId); 
	    $select->order('order DESC'); 
	    $select->where($where);
	    $images = $this->tableGateway->selectWith($select)->toArray();
	    if ($images) {
	        $lastImage = false;
	        foreach ($images as $image) {  
	            if (!is_file($imagePath . $image['file_name'])) { 
// 	                $this->tableGateway->deleteData(array(
// 	                    'id' => $image['id']
// 	                ));
                    continue;
	            } else {
	                if ($image['caption']) {
	                    return $image;
	                }
	            }
	            $lastImage = $image;
	        }
	        return $lastImage;
	    }
	    return false;
	}
	
	
  
}