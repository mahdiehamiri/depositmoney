<?php

namespace Contact\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Contact\Form\ContactForm;
use Zend\Session\Container;
use Contact\Model\ContactTable;
use Application\Helper\BaseAdminController;

class AdminController extends BaseAdminController
{
    public function listContactsAction()
	{
	    $this->setHeadTitle(__('Contact us list'));
		$container = new Container('token');
		$csrf = md5(time() . rand());
		$container->csrf = $csrf;
		$view['csrf'] = $csrf;
	    $contactTable = new ContactTable($this->getServiceLocator());
		$contacts = $contactTable->getAllContact();
		$view['contacts'] = $contacts;
		return new ViewModel($view);
	}
	public function deleteContactAction()
	{
	    $container = new Container('token');
	    $token = $this->params('token');
	    $contactId = $this->params('id', -1);
	    $contactTable = new ContactTable($this->getServiceLocator());
	    $contactById = $contactTable->getContactById($contactId);
	    if ($contactId && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
	        $isDeleted = $contactTable->deleteContact($contactId);
	        if ($isDeleted) {
	            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
	        } else {
	            $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
	        }
	    }
	    return $this->redirect()->toRoute('contact-admin');
	}
	
}
