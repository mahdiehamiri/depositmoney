<?php

namespace Contact\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Contact\Model\ContactTable;
use Contact\Form\ContactForm;
use Contact\Model\UsersTable;
use Configuration\Model\ConfigurationTable;
use Application\Helper\BasePluginController;

class IndexController extends BasePluginController
{
    public function indexAction()
    {
        $view = array();
        $lang = $this->lang;
    
        $view['lang'] =  $lang;
        $contactForm = new ContactForm();
        $view['contactForm'] = $contactForm;
        $viewModel = new ViewModel($view);
        $viewModel->setTemplate("contact/index/index.phtml");
        return $viewModel;
    }
    
   

}
