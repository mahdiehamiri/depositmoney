<?php

namespace Contact\Controller;


use Application\Helper\BaseCustomerController;
use ProductComment\Model\ProductCommentTable;
use ProductComment\Form\ProdcutCommentForm;
use Zend\Mvc\Controller\AbstractActionController;
use Contact\Model\ContactTable;
use Configuration\Model\ConfigurationTable;
use Contact\Form\ContactForm;

class AjaxController extends AbstractActionController
{	
    public function refreshAction()
    {
        $form = new ContactForm();
        $captcha = $form->get('myCaptcha')->getCaptcha();
        $data = array();
        $data['id']  = $captcha->generate();
        $data['src'] = $captcha->getImgUrl() .
        $captcha->getId() .
        $captcha->getSuffix();
        die(json_encode($data));
    }
    
	public function indexAction()
	{
		$request = $this->getRequest();
		
		$contactForm = new ContactForm();
		if ($request->isPost()) {
		    $data = $request->getPost()->toArray();
		    $contactForm->setData($data);
		    if ($contactForm->isValid()) {
		        $validData = $contactForm->getData();
		        $contactTable = new ContactTable($this->getServiceLocator());
		        $isInsert = $contactTable->addContact($validData);
		        $validData = $contactForm->getData();
		        $bodyText = 'نام: ' . $validData['name'];
		        $bodyText .= "<br />" .' ایمیل: ' . $validData['email'];
		        $bodyText .= "<br />" .' تلفن: ' . $validData['tel'];
		        $bodyText .= "<br />" . ' متن: ' . $validData['text'];
		        $mails = 'info@isakhtemoon.com';
		
		
		        $configurationTable = new ConfigurationTable($this->getServiceLocator());
		        $allConfigs = $configurationTable->getRecords();
		        $configs = array();
		        foreach ($allConfigs as $config) {
		            $configs[$config["name"]] = $config["value"];
		        }
		        //$message = __("You received a new contact");
		        //$this->SMSService()->send($message, $configs['admin-mobile']);
		        $emailSent = $this->mailService()->sendMail($bodyText, 'ایمیل از طرف فرم تماس با ما', $mails) ;
		        if ( $isInsert && $emailSent){
		            die("true");
		        } else {
		            die("false");
		        }
		    } 
		}
		die("false");
	}
	
	
}