<?php

namespace Contact\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha\Image;
    
class ContactForm extends Form
{
	public function __construct()
	{
		parent::__construct('contactForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$name = new Element\Text('name');
		$name->setAttributes(array(
		    'id'    => 'name',
		    'class' => 'form-control input-field form-item validate[required]',
		    'placeholder' => __('Full name'),
		    'Lang' => 'fa-IR',
		    'required' => 'required',
		));
	    $name->setLabel(__("Full name"));
		
		
		
		$email = new Element\Text('email');
		$email->setAttributes(array(
		    'id'    => 'email',
		    'class' => 'form-control input-field form-item validate[required, custom[email]]',
		    'placeholder' => 'example@example.com',
		    //'dir' => 'ltr',
		    'required' => 'required',
		));
		$email->setLabel(__("Email"));
		
		$tel = new Element\Text('tel');
		$tel->setAttributes(array(
		    'id'    => 'tel',
		    'class' => 'form-control input-field form-item validate[required,custom[onlyNumberSp]]',
		    'placeholder' => '02122222222',
		    'required' => 'required',
		));
		$tel->setLabel(__("Phone"));
		
		$text = new Element\Textarea('text');
		$text->setAttributes(array(
				'id'    => 'text',
				'class' => 'form-control input-field form-item field-subject validate[required]',
		        'placeholder' => __('write your message here'),
		        'Lang' => 'fa-IR',
		    'required' => 'required',
		));
		$text->setLabel(__("Text"));
		
 
		$captchaImage  = new Image(array(
		    'font' =>   $_SERVER['DOCUMENT_ROOT'] . "/fonts/arial.ttf",
		    'wordLen' => 4,
		    'timeout' => 300,
		    'width'   =>100,
		    'height'  => 50,
		    '_fsize' => 14,
		    'lineNoiseLevel' => 1 ,
		    'dotNoiseLevel' => 1));
		$captchaImage->setImgDir( $_SERVER['DOCUMENT_ROOT'] . "/captcha");
		$captchaImage->setImgUrl( "/captcha");
		
		$captcha = new Element\Captcha('myCaptcha');
		
		$captcha->setLabel(__("I'm not robot"));

		$captcha->setOptions( array(
		    'captcha' => $captchaImage,
		));
		$captcha->setAttributes(array(
		    'id'   => "captcha-id",
		    'class' => 'form-control input-field form-item',
		    'placeholder' => __("Enter image words here")
		));
		
		$submit = new Element\Button('contact_submit');
		
		$submit->setLabel(__("Send"));
		
		$submit->setAttributes(array(
				'id'    => 'contact_submit',
				'class' => 'fancy-button button-line button-white large zoom subform'
		));
		
		$this->add($name)
    		 ->add($email)
    		 ->add($tel)
		     ->add($text)
		     ->add($captcha)
			 ->add($submit);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
			array(
				'name'     => 'name',
				'required' => true,
				'filters'  => array(
						array(
								'name' => 'StripTags'
						)
				),
			)
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'tel',
		        'required' => false,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'email',
		        'required' => false,
		        'validators' => array(
		            array( 'name'    => 'EmailAddress'
		            )
		        ),
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$inputFilter->add($factory->createInput(
		    array(
		        'name'     => 'text',
		        'required' => false,
		        'filters'  => array(
		            array(
		                'name' => 'StripTags'
		            )
		        ),
		    )
		));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
