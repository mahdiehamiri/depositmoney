<?php 
namespace Contact\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class ContactTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('contact_us', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getAllContact()
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getContactById($contactId)
	{
	    $select = new Select('contact_us');
	    $select->where->equalTo('id', $contactId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addContact($postData)
	{
		$this->tableGateway->insert(array(
				'name' 	=> $postData['name'],
		        'tel' 	=> $postData['tel'],
    		    'email' => $postData['email'],
    		    'text' => $postData['text'],
		));
		return $this->tableGateway->getLastInsertValue();
	}
	
	public function deleteContact($contactId, $userId = null)
	{
		if ($contactId > 0 && is_numeric($contactId)) {
			if ($userId !== null) {
				return $this->tableGateway->delete(array(
						'id' 	  			  => $contactId,
						'post_author_user_id' => $userId
				));
			} else {
				return $this->tableGateway->delete(array(
						'id' => $contactId
				));
			}
		}
	}
}
