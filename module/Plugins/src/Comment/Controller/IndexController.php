<?php

namespace Comment\Controller;
   
use Zend\View\Model\ViewModel; 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Comment\Form\CommentForm;
use Comment\Model\CommentTable;
use Application\Helper\BasePluginController;
use Application\Helper\MailService;
use Comment\Model\CommentLikeDislikeTable;
use Application\Helper\GeneralHelper;
 
class IndexController extends BasePluginController
{ 
	protected $defaultGuestId = 1457; //There is no email set for this dummy user!
	 
	
	public function renderAction($params)
	{ 
			$commentTable = new CommentTable($this->getServiceLocator());
			$config = $this->serviceLocator->get('Config');
			
			$userIp = getenv('HTTP_CLIENT_IP')?:
			getenv('HTTP_X_FORWARDED_FOR')?:
			getenv('HTTP_X_FORWARDED')?:
			getenv('HTTP_FORWARDED_FOR')?:
			getenv('HTTP_FORWARDED')?:
			getenv('REMOTE_ADDR');
			$userId = false;
			if ($this->userData) {
				$userId = $this->userData->id;
				$view['userData'] = $this->userData;
			} else {
				$islogin = GeneralHelper::getUserCommentSession ( 'comment_session' );
				$guestIsExist = $commentTable->fetchCommentsById( (int)$islogin );
				if($islogin) {
					$userData['name'] = $guestIsExist ['guest_name'];
					$userData['email'] = $guestIsExist ['guest_email'];
					$userData['id'] = $guestIsExist['id'];
					$view['userData'] = $userData;
				}
			}
	 
			$commentForm = new CommentForm();
			$view['commentForm'] = $commentForm;  
	 		$request =   $this->getServiceLocator()->get('Request');
			if ($request->isPost()) {
				$data = $request->getPost(); 
				$commentForm->setData($data);
				if ($commentForm->isValid()) {
					$validData = $commentForm->getData();
					$validData['user_ip'] = $userIp;
					$commentId = $commentTable->addComment($validData, $this->defaultGuestId, $config['default_comment_status'], $validData['name']);
						if ($commentId) {
							$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
							//$this->getServiceLocator()->get('layout')->successMessage = __("Your comment sent.");
						} else {
							//$this->getServiceLocator()->get('layout')->errorMessage = "Problem in saving data.";
						}

				}
			}  
			$comments = $commentTable->fetchComments((int)$params['entity_id'], $params['entity_type']);
		 
			if (isset($params['likes']) && $params['likes']) {
				$commentLikeDislikeTable = new CommentLikeDislikeTable($this->getServiceLocator());
				 foreach ($comments as &$comment) {
					$comment['likes'] = 0;
					$comment['dislike'] = 0;
				 	$commentLikeDislikeData = $commentLikeDislikeTable->getLikeAndDislikeCount($comment['id']);
				 	if ($commentLikeDislikeData) {
				 		$comment['likes'] = $commentLikeDislikeData[0]['likess'];
				 		$comment['dislike'] = $commentLikeDislikeData[0]['dislikes'];
				 	}
				 } 			
				$view['setLikeButton'] = true;
			}
 
			$view['comments'] = $comments;
			$view['params'] = $params;
			
			$view['userId'] = $userId;
			$view['limit'] = 4;
			$viewModel = new ViewModel($view);
			return $viewModel->setTemplate('comment/index/render.phtml');
		 
	}

	public function sendNotificationMailAction()
	{
		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			if ($data['status'] == 200) {
				$title = "Relpy to your comment";
				$mailService = new MailService(); 
				$mailService->sendMail($data['link'], $title, $data['userEmail'], 'comment-notification-layout.phtml');
			}
		}
	}
}
