<?php

namespace Comment\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Application\Form\LoginForm;
use Application\Form\RegistrationForm;
use Application\Form\ForgotPasswordForm;
use Application\Form\ResetPasswordForm;
use Application\Form\UserAccountForm;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Session\Container;
use Application\Form\QuotationForm;
use Application\Model\UsersTable;
use Application\Model\CommentsTable;
use Tutorial\Model\ProjectsTable;
use Blog\Model\BlogPostTable;
use Jobs\Model\JobsCompaniesTable;
use Jobs\Model\JobsJobVacanciesTable;
use Application\Model\QuotationsTable;
use Application\Model\TeamMembersTable;
use Application\Form\TeamMembersForm;
use Application\Model\OrderTable;
use Application\Helper\BaseAdminController;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\Filter;
use Comment\Form\CommentForm;
use Comment\Model\CommentTable;
use Language\Model\LanguageLanguagesTable;
use Comment\Form\EditCommentForm;

class AdminController extends BaseAdminController
{
	protected $noOfCommentsPerPage = 40;
	

    
//     public function listCommentsAction()
//     {
//         $this->setHeadTitle(__('Users comments list'));
//     	$canSeeDeleteButton = $this->permissions()->getPermissions('application-admin-can-see-comment-delete-button');
//     	$view['canSeeDeleteButton'] = $canSeeDeleteButton;
//     	$container = new Container('token');
//     	$csrf = md5(time() . rand());
//     	$container->csrf = $csrf;
//     	$commentsTable = new CommentsTable($this->getServiceLocator());
//     	$comments = $commentsTable->fetchComments();
//     	$page = $this->params('page');
//     	$paginator = new Paginator(new ArrayAdapter($comments));
//     	$paginator->setCurrentPageNumber($this->params('page', 1));
//     	$paginator->setItemCountPerPage($this->noOfCommentsPerPage);
//     	$view['csrf'] = $csrf;
//     	$view['comments'] = $paginator;
//     	return new ViewModel($view);
//     }
    
    
    public function listCommentsAction()
    {

        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
         
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $commentsTable = new CommentsTable($this->getServiceLocator());
        $allDiscounts = $commentsTable->fetchComments();
        $grid->setTitle(__('Comment list'));
        $grid->setDefaultItemsPerPage(50);
        $grid->setDataSource($allDiscounts,$dbAdapter);
         
         
        $col = new Column\Select('id',"comments");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
         
        $col = new Column\Select('username', "users");
        $col->setLabel(__('First Name'));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
               
        $col = new Column\Select('comment');
        $col->setLabel(__('Comment'));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('entity_type');
        $col->setLabel(__('Entity type'));
        $col->setWidth(10);
        $options = array('Blog' => 'blog', 'Product' => 'product');
        $col->setFilterSelectOptions($options);
        $replaces = array('Blog' => 'blog', 'Product' => 'product');
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
         
        $col = new Column\Select('status', "comments");
        $col->setLabel(__('Status'));
        $col->setWidth(10);
        $options = array('0' => 'inactive', '1' => 'active');
        $col->setFilterSelectOptions($options);
        $replaces = array('0' => 'inactive', '1' => 'active');
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
         
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink("/". $this->lang .'/admin-comment/edit-comment/' . $rowId);
        $viewAction->setTooltipTitle(__('Edit'));
         
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-remove');
        $viewAction1->setLink("/". $this->lang .'/admin-comment/delete-comment/'.$rowId.'/token/'.$csrf);
        $viewAction1->setTooltipTitle(__('Delete'));
        
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
         
       // $link[] = '<a href='."/". $this->lang .'/admin-comment/add-comment class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($link);
        
        $grid->render();
         
        return $grid->getResponse();
        
        
    }
    public function editCommentAction()
    {
    	$this->setHeadTitle(__('Edit comment'));
    	$languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
    	$allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
    	$commentForm = new EditCommentForm();
    	$commentId = $this->params('id', -1);
    	$commentTable = new CommentTable($this->getServiceLocator());
    	$commentData = $commentTable->fetchCommentsById($commentId); 
    	$commentForm->populateValues($commentData); 
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$data = $request->getPost()->toArray(); 
    	 
    		$commentForm->setData($data);
    		if ($commentForm->isValid()) {
    			$validData = $commentForm->getData();
    			if ($commentId > 0 && is_numeric($commentId)) {
    				$isEditted=  $commentTable->updateCommentStatus($validData, $commentId);
    				if ($isEditted) {
    					$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
    					return $this->redirect()->toRoute('comment-admin');
    				} else {
    					$this->flashMessenger()->addSuccessMessage(__("No changes happend!"));
    					return $this->redirect()->toRoute('comment-admin');
    				}
    			}
    		} else {
    			$this->layout()->errorMessage = $this->generalhelper()->getFormErrors($commentForm, true);
    		}
    	}  
    	$view['commentForm'] = $commentForm;
 
    	return new ViewModel($view);
    }
    
    public function deleteCommentAction()
    {
    	$userId = $this->userData->id;
    	$container = new Container('token'); 
    	$commentId = $this->params('id', -1);
    	$token = $this->params('token');
    	$commentsTable = new CommentsTable($this->getServiceLocator());
    	
    	if ($commentId && $container->offsetExists('csrf') && $container->offsetGet('csrf') == $token) {
    	    $isDeleted = $commentsTable->deleteComment($commentId, $userId);
    		if ($isDeleted) {
    		    if ($isDeleted) {
    		        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
    		    } else {
    		        $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
    		    }
		    }
		    return $this->redirect()->toRoute('comment-admin');
    	} 
    }
    
   
}   
