<?php

namespace Comment\Controller;

use Application\Helper\BaseCustomerController;
use ProductComment\Model\ProductCommentTable;
use ProductComment\Form\ProdcutCommentForm;
use Zend\Mvc\Controller\AbstractActionController;
use Contact\Model\ContactTable;
use Configuration\Model\ConfigurationTable;
use Contact\Form\ContactForm;
use Comment\Model\CommentTable;
use Comment\Model\CommentLikeDislikeTable;
use Application\Helper\GeneralHelper;

class AjaxController extends AbstractActionController {
	public function ckeckUserExistanceAction() {
		$userIp = getenv ( 'HTTP_CLIENT_IP' ) ?  : getenv ( 'HTTP_X_FORWARDED_FOR' ) ?  : getenv ( 'HTTP_X_FORWARDED' ) ?  : getenv ( 'HTTP_FORWARDED_FOR' ) ?  : getenv ( 'HTTP_FORWARDED' ) ?  : getenv ( 'REMOTE_ADDR' );
		$userId = false;
		$isExist = false;
		$guestname = false;
		$guestemail = false;
		$commentTable = new CommentTable ( $this->getServiceLocator ());
		if (isset ( $this->userData )) { 
			$userId = $this->userData->id; 
			$guestname  = $this->userData->name;
			$guestemail = $this->userData->email;
		} else {
			// CHECK IF USER EXIST
			$islogin = GeneralHelper::getUserCommentSession ( 'comment_session' );
 
			if ($islogin) {
				$guestIsExist = $commentTable->fetchCommentsById( (int)$islogin );
				if ($guestIsExist) {
					if (!$userId) {
						$view['userData'] = $guestIsExist['id'];
					}
					$guestname  = $guestIsExist ['guest_name'];
					$guestemail = $guestIsExist ['guest_email'];
				} else {
					die ('userNotExist');
				}
			} else {
				die ('userNotExist');
			}
			// END CHECK IF USER EXIST
		}
		$this->layout ( "layout/empty" );
		$request = $this->getRequest ();
		$postData = $request->getPost ();
		if (isset($postData['justlogincheck'])) {
			$arraydata = array('name' => $guestemail, 'email' => $guestname);
			echo json_encode($arraydata);
		    die();
		}
		$view = array (); 
		if ($request->isPost ()) {
			$config = $this->serviceLocator->get ( 'Config' );
			$commentTable = new CommentTable ( $this->getServiceLocator () );
			$postData = $request->getPost ();
			 
			$postData ['user_ip'] = $userIp;
			if (trim ( $postData ['text'] ) == '') {
				$view ['status'] = false;
			} else {
				$postData['guest_name'] = $guestname;
				$postData['user_ip'] = $userIp;
				$postData['guest_email'] = $guestemail;
				$guestIsExist = $commentTable->fetchCommentsByUserEmailIpName( $guestemail, $userIp);
			 
				if ($guestIsExist) {
					$postData['guest_name'] = $guestIsExist['guest_name'];
					$postData['guest_email'] = $guestIsExist['guest_email']; 
				} 
				
				$commentId = $commentTable->addComment ( $postData, $userId, $config ['default_comment_status'] );
				if ($commentId) {
					$comment = $commentTable->fetchComments ( false, false, $commentId, false );
					if ($comment) {
						if ($config ['default_comment_status'] == '0') {
							$view ['status'] = 'hide-comment';
						} else {
							$comment [0] ['comment'] = htmlspecialchars ( $comment [0] ['comment'] );
							 
							if ($comment [0] ['guest_name'] == null) {
								$comment [0] ['guest_name'] = 'Anonymous';
							}
							$comment [0]['dislikes'] = 0;
							$comment [0]['likes'] = 0;
							$likeDisolikeTable  =new CommentLikeDislikeTable ( $this->getServiceLocator () );
							$likeDisolikeTableData = $likeDisolikeTable->getLikeAndDislikeCount($comment [0] ['id']);
							if ($likeDisolikeTableData) {
								$comment [0]['dislikes'] = $likeDisolikeTableData[0]['dislikes'];
								$comment [0]['likes'] = $likeDisolikeTableData[0]['likess'];
							}
							$view ['comment'] = $comment [0];
							$view ['status'] = 'show-comment';
						}
					} else {
						$view ['status'] = false;
					}
				} else {
					$view ['status'] = false;
				}
			}
		}
		echo (json_encode ( $view )); 
		die ();
	}
	public function addCommentAction() {
		$userIp = getenv ( 'HTTP_CLIENT_IP' ) ?  : getenv ( 'HTTP_X_FORWARDED_FOR' ) ?  : getenv ( 'HTTP_X_FORWARDED' ) ?  : getenv ( 'HTTP_FORWARDED_FOR' ) ?  : getenv ( 'HTTP_FORWARDED' ) ?  : getenv ( 'REMOTE_ADDR' );
		$userId = false;
		if (isset ( $this->userData )) {
			$userId = $this->userData->id;
		}
		$this->layout ( "layout/empty" );
		$request = $this->getRequest ();
		$view = array ();
		$view ['status'] = 'error';
		
		if ($request->isPost ()) {
			$config = $this->serviceLocator->get ( 'Config' );
			$commentTable = new CommentTable ( $this->getServiceLocator () );
			$postData = $request->getPost ();
		 
			$postData ['user_ip'] = $userIp;
			$postData['guest_name'] = $postData['name'];
			$postData['guest_email'] = $postData['email'];
			if (trim ( $postData ['text'] ) == '') {
				$view ['status'] = false;
			} else { 
				$commentId = $commentTable->addComment ( $postData, $userId, $config ['default_comment_status'] );
			  	$view['userData'] = $commentId; 
				if ($commentId) {
				 	GeneralHelper::setUserCommentSession($commentId);
					$comment = $commentTable->fetchComments ( false, false, $commentId, false );
					if ($comment) {
						if ($config ['default_comment_status'] == '0') {
								$view['status'] = 'hide-comment';
						} else {
							$comment [0] ['comment'] = htmlspecialchars ( $comment [0] ['comment'] );
							if ($comment [0] ['guest_name'] == null) {
								$comment [0] ['guest_name'] = 'Anonymous';
							}
							$comment [0]['dislikes'] = 0;
							$comment [0]['likes'] = 0;
							$likeDisolikeTable  =new CommentLikeDislikeTable ( $this->getServiceLocator () );
							$likeDisolikeTableData = $likeDisolikeTable->getLikeAndDislikeCount($comment [0] ['id']);
							if ($likeDisolikeTableData) {
								$comment [0]['dislikes'] = $likeDisolikeTableData[0]['dislikes'];
								$comment [0]['likes'] = $likeDisolikeTableData[0]['likess'];
							}
							$view ['comment'] = $comment [0];
							$view ['status'] = 'show-comment';
						}
					} else {
						$view ['status'] = false;
					}
				} else {
					$view ['status'] = false;
				}
			}
		} 
		echo (json_encode ( $view )); die ();
	}
	public function addCommentDislikeLikeAction() {
		$userIp = getenv ( 'HTTP_CLIENT_IP' ) ?  : getenv ( 'HTTP_X_FORWARDED_FOR' ) ?  : getenv ( 'HTTP_X_FORWARDED' ) ?  : getenv ( 'HTTP_FORWARDED_FOR' ) ?  : getenv ( 'HTTP_FORWARDED' ) ?  : getenv ( 'REMOTE_ADDR' );
		$userId = false;
		if (isset ( $this->userData )) {
			$userId = $this->userData->id;
		}
		
		$request = $this->getRequest ();
		$view = array ();
		$view ['status'] = false;
		
		if ($request->isPost ()) {
			$postData = $request->getPost ();
			$config = $this->serviceLocator->get ( 'Config' );
			if (isset ( $postData ['id'] ) && ($postData ['id'] > 0)) {
				$commentLikeDislikeTable = new CommentLikeDislikeTable ( $this->getServiceLocator () );
				$commentLikeDislikeTableData = $commentLikeDislikeTable->fetchCommentsByCommentId ( $postData ['id'], $userIp );
				$commentLikeDislikeCount = $commentLikeDislikeTable->getLikeAndDislikeCount ( $postData ['id'] );
		 
		 		if (!$commentLikeDislikeTableData) {   
						$dataArray = array (
								'user_id' => $userId,
								'comment_id' => $postData ['id'],
								'likes' => ($postData ['flag'] == 'plus' ? 1 : 0),
								'dislike' => ($postData ['flag'] == 'minus' ? 1 : 0),
								'ip' => $userIp 
						);
						$commentLikeDislikeTable->addCommentLikeDislike ( $dataArray );
						$commentLikeDislikeCount = $commentLikeDislikeTable->getLikeAndDislikeCount ( $postData ['id'] );
		 
						$view ['status'] = 'added';
						$view ['like'] = $commentLikeDislikeCount[0]['likess'];
						$view ['dislike'] = $commentLikeDislikeCount[0]['dislikes'];
						echo (json_encode ( $view ));
						die ();
				} else {
						 
						$view ['status'] = 'you are voted befor';
				 }
			
			}
		}
		echo (json_encode ( $view['status'] ));
		die ();
	}
}