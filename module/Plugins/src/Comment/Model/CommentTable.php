<?php 

namespace Comment\Model;
 
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Application\Services\BaseModel;

class CommentTable extends BaseModel
{
	protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('comments', $this->getDbAdapter());
	}
	
	public function fetchComments($entityId = false, $entityType = 'post', $commentId = false, $status = '1', $isUsers = false)
	{
		$select = new Select('comments');
		if ($entityId) { 
			$select->where->equalTo('entity_id', $entityId);
		}
		if ($entityType) {
			$select->where->equalTo('entity_type', $entityType);
		}
		if ($commentId) {
			$select->where->equalTo('comments.id', $commentId);
		}
		if ($isUsers) {
			$select->join('users', 'users.id = comments.user_id', array(
					'name' => new Expression("CONCAT(firstname, ' ', lastname)"),
					'email'
			));
		}  
		if ($status) {
			$select->where->equalTo('comments.status', $status);
		}
		$select->order('comments.id DESC');
		return $this->tableGateway->selectWith($select)->toArray();
	}
	public function fetchCommentsByUserEmail($email)
	{
		$select = new Select('comments');
		if ($email) { 
			$select->where->equalTo('email', $email);
		} 
		return $this->tableGateway->selectWith($select)->current();
	}
	
	public function fetchCommentsByUserEmailIpName($email,$ip)
	{
		$select = new Select('comments');
		if ($email) { 
			$select->where->equalTo('guest_email', $email);
		} 
		if ($ip) { 
			$select->where->equalTo('user_ip', $ip);
		} 
		return $this->tableGateway->selectWith($select)->current();
	}
	
	public function fetchCommentsById($entityId)
	{
		$select = new Select('comments');
		if ($entityId) { 
			$select->where->equalTo('id', $entityId);
		} 
	 
		return $this->tableGateway->selectWith($select)->current();
	}
	
	public function addComment($commentData, $userId, $commentStatus = '0', $name = null)
	{
		if ($name) {
			$newComment = array(
					'parent_id'   => (isset($commentData['parentId']) ? (int)$commentData['parentId'] : '0'),
					'entity_id'   => $commentData['id'],
					'entity_type' => $commentData['type'],
					'user_id'     => $userId,
					'comment'     => $commentData['text'],
					'status'      => $commentStatus,
					'user_ip'     => $commentData['user_ip'],
					'guest_name'  => $name,
					'guest_email' => $commentData['guest_email'] 
			);
		} else {
			$email = null;
			$guest_name = null;
			if (isset($commentData['guest_name'])) {
				$guest_name = $commentData['guest_name'];
				
			}
			if (isset($commentData['guest_email'])) {
				$email = $commentData['guest_email'];
			}
 
			$newComment = array(
					'parent_id'   => (isset($commentData['parentId']) ? (int)$commentData['parentId'] : '0'),
					'entity_id'   => $commentData['id'],
					'entity_type' => $commentData['type'],
					'user_id'     => $userId,
					'comment'     => $commentData['text'],
					'status'      => $commentStatus,
					'user_ip'     => $commentData['user_ip'],
					'guest_name'  => $guest_name,
					'guest_email' => $email
			);
		}
		$this->tableGateway->insert($newComment);
		return $this->tableGateway->lastInsertValue;
	}
	public function updateCommentStatus($newData, $id)
	{
		return $this->tableGateway->update(array( 
				'status'=> $newData['status'],
	
		), array('id' => $id));
	}
}
