<?php 

namespace Comment\Model;
 
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Application\Services\BaseModel;

class CommentLikeDislikeTable extends BaseModel
{
	protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('comment_like_dislike', $this->getDbAdapter());
	}
	
	public function fetchCommentsByCommentId($commentId = false, $ip = false)
	{
		$select = new Select('comment_like_dislike');
		if ($commentId) { 
			$select->where->equalTo('comment_id', $commentId);
		}  
		if ($ip) { 
			$select->where->equalTo('ip', $ip);
		}  
		return $this->tableGateway->selectWith($select)->current();
	}
	
	public function addCommentLikeDislike($data)
	{ 
		$this->tableGateway->insert($data);
		return $this->tableGateway->lastInsertValue;
	}
	public function updateCommentLikeDislike($newData)
	{ 
		return $this->tableGateway->update(array(
				'likes'         => $newData['like'],
				'dislike'=> $newData['dislike'],
		
		), array('id' => $newData['id']));
	}
	public function getLikeAndDislikeCount($commentId = false)
	{ 
		$select = new Select('comment_like_dislike');
		$select->columns(array(
			'likess' => New Expression('SUM(likes)'),
			'dislikes' => New Expression('SUM(dislike)'),
		));
 
	    $select->where->equalTo('comment_id', $commentId); 
		$select->group('comment_id') ;  
		return $this->tableGateway->selectWith($select)->toArray();
	}
}
