<?php 

namespace Comment\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha;
use Zend\Form\Fieldset;
use Zend\Captcha\Image;

class EditCommentForm extends Form
{
	public function __construct()
	{
		parent::__construct('commentForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$name = new Element\Text('name');
		$name->setAttributes(array(
				'id'    => 'name',
				'class' => 'form-control',
		        'placeholder'  => __("Name")
		));
		$name->setLabel(__("Name"));
			
		$comment = new Element\Textarea('comment');
		$comment->setAttributes(array(
				'id'    => 'comment',
				'class' => 'form-control',
		    'placeholder'  => __("Comment body")
		));
		$comment->setLabel(__("Comment body"));
		
		$email = new Element\Email('guest_email');
		$email->setAttributes(array(
				'id'    => '',
				'class' => 'form-control',
		        'placeholder'  => "example@example.com"
		));
		$email->setLabel(__("Email"));
 
		$status = new Element\Select('status');
		$status->setLabel(__("Status"));
		$status->setAttributes(array(
				'id'          => 'status',
				'class'       => 'form-control',
		));
		$stags = array(
				'0' => __("inactive"),
				'1' => __("active")
		);
		$status->setValueOptions($stags);
		//$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Submit"));
		$submit->setAttributes(array(
				'id'    => 'submit-comment',
				'class' => 'btn btn-primary'
		));

		$this->add($name)
			 ->add($comment)	
			 ->add($email)	  
			 ->add($status)
			 ->add($submit); 
	}
	
	 
}
