<?php 

namespace Comment\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Captcha;
use Zend\Form\Fieldset;
use Zend\Captcha\Image;

class CommentForm extends Form
{
	public function __construct()
	{
		parent::__construct('commentForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));

		$name = new Element\Text('name');
		$name->setAttributes(array(
				'id'    => 'name',
				'class' => 'form-control validate[required]',
		        'placeholder'  => __("Name")
		));
		$name->setLabel(__("Name"));
			
		$comment = new Element\Textarea('text');
		$comment->setAttributes(array(
				'id'    => 'comment',
				'class' => 'form-control',
		    'placeholder'  => __("Comment body")
		));
		$comment->setLabel(__("Comment body"));
		
		$email = new Element\Email('guest_email');
		$email->setAttributes(array(
				'id'    => 'guest_email',
				'class' => 'form-control validate[required, custom[email]]',
		        'placeholder'  => "example@example.com"
		));
		$email->setLabel(__("Email"));
		
		$captchaImage = new Image(array(
				'font' => $_SERVER['DOCUMENT_ROOT'] . "/fonts/arial.ttf",
				'wordLen' => 2,
				'timeout' => 300,
				'width' => 100,
				'height'=> 50,
				'_fsize' => 16,
				'lineNoiseLevel' => 1,
				'dotNoiseLevel' => 40
				
		));
		$captchaImage->setImgDir($_SERVER['DOCUMENT_ROOT'] .  "/captcha");
		$captchaImage->setImgUrl("/captcha");
		
		$captcha = new Element\Captcha('captcha');
		$captcha->setOptions(array(
				'captcha' => $captchaImage
		));
		$captcha->setAttributes(array(
				'id'    	  => 'captcha',
				'class' 	  => 'form-control',
		));
	    $captcha->setLabel(__("I'm not robot"));

		$entityType = new Element\Hidden('type');
		$entityId = new Element\Hidden('id');
		
		//$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__("Submit"));
		$submit->setAttributes(array(
				'id'    => 'submit-comment',
				'class' => 'btn btn-success'
		));

		$this->add($name)
			 ->add($comment)	
			 ->add($email)	
			 ->add($entityType)	
			 ->add($entityId)	
			 ->add($captcha)	
			 //->add($csrf)
			 ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'guest_email',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
				'validators' => array(
						array(
								'name'    => 'EmailAddress',
						)
				),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'text',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				)
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
