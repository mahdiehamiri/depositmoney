<?php

namespace Faq\Controller;

use Zend\View\Model\ViewModel; 
use Faq\Model\FaqTable; 
use Application\Helper\BasePluginController;
use Zend\Mvc\MvcEvent;

class IndexController extends BasePluginController
{
    public function onDispatch(MvcEvent $e)
    {
        parent::onDispatch($e);
    }
    
    public function indexAction()
    { 
        $lang = $this->lang;
        $faqTable = new FaqTable($this->getServiceLocator());
    	$paginator = $faqTable->fetchAllForView(true,$lang);
    	/* @var $paginator \Zend\Paginator\Paginator */

    	$view['faqs'] = $paginator;
       
    	$viewModel = new ViewModel($view);
    	$viewModel->setTemplate("faq/index/index.phtml");
    	return $viewModel;
    }
    
}
