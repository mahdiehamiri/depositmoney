<?php

namespace Faq\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Language\Model\LanguageLanguagesTable;
use Faq\Form\FaqForm;
use Faq\Model\FaqTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;

class AdminController extends BaseAdminController
{
	public function listAction()
	{	    
        
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
         
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $faqTable = new FaqTable($this->getServiceLocator());
        $questions = $faqTable->fetchAll(true);
        $grid->setTitle(__('Faq List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($questions,$dbAdapter);
        
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $grid->addColumn($col);
        
        $col = new Column\Select('question');
        $col->setLabel(__('Question'));
        $grid->addColumn($col);
         
        $col = new Column\Select('answer');
        $col->setLabel(__('Answer'));
        $grid->addColumn($col);
        
        $col = new Column\Select('order');
        $col->setLabel(__('Sort'));
        $grid->addColumn($col);
         
        $col = new Column\Select('lang');
        $col->setLabel(__('Lang'));
        $options = array('fa' => 'fa', 'en' => 'en');
        $col->setFilterSelectOptions($options);
        $replaces = array('fa' => 'fa', 'en' => 'en');
        $grid->addColumn($col);
         
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $options = array('0' => 'Inactive', '1' => 'Active');
        $col->setFilterSelectOptions($options);
        $replaces = array('0' => 'Inactive', '1' => 'Active');
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
         
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-edit');
        $viewAction->setLink('/' . $this->lang . '/admin-faq/edit-post/' . $rowId);
        $viewAction->setTooltipTitle(__('Edit'));
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setLink('/' . $this->lang . '/admin-faq/delete-post/'.$rowId.'/token/'.$csrf);
        $viewAction2->setTooltipTitle(__('Delete'));
         
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        $block[] = '<a href="/' . $this->lang . '/admin-faq/add-post" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
	    $block[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($block);
         
        $grid->render();
        
        return $grid->getResponse();
	}
	
	private function getLangs()
	{
// 		var_dump($this->lang);die;
		$languagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		$allActiveLanguages = $languagesTable->getAllLanguages(array("enable" => "1"));
		return array('langs'=>$allActiveLanguages, 'current'=>$this->lang);
	}
	
	private function uploadImage($files, $index = NULL)
	{
		$config = $this->serviceLocator->get('Config');
		$uploadDir = $config['base_route'] . "/images/faq/";
		$validationExt ="jpg,jpeg,png,gif";
		$postImage = array();
		foreach ($files as $k=>$file) {
			$newFileName = md5(time() . $file['name']);
			$isUploaded = $this->imageUploader()->UploadFile($file, $validationExt, $uploadDir, $newFileName);
			$postImage[] = @$isUploaded[1];
		}
		if ($index !== NULL)
			return $postImage[$index];
		return $postImage;
	}
	

	public function addPostAction()
	{
	    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
	    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
	    $postForm = new FaqForm($allActiveLanguages);
		$request = $this->getRequest();
		if ($request->isPost()) {
			$postData = $request->getPost();
			$data = $postData->toArray();
			//var_dump($data); die;
			if ($postForm->setData($data)->isValid()) {
				$postData = $postForm->getData();
				$faqTable = new FaqTable($this->getServiceLocator());
				//try {		
	
				    $newId = $faqTable->add($postData);
					if ($newId) {
						if (isset($_POST['submit'])) {
						    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
						    return $this->redirect()->toRoute('admin-faq');
						} else {
						    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
						    return $this->redirect()->toRoute('admin-faq', array(
						        'action' => 'add-post'
						    ));
						}
					} else {
						 $this->layout()->errorMessage = __("Operation failed!");
					}
				
			} else {
			    //var_dump($postForm->getMessages()); die;
				$this->layout()->errorMessage = __("Please Fill necessary fileds!");
			}
		}
		$view['postForm'] = $postForm;
		return new ViewModel($view);
	}
	
	public function editPostAction()
	{
		$userId = $this->userData->id;
		$Id = $this->params('id', -1);
		if (!$Id) {
			return $this->redirect()->toRoute('admin-faq');	
		}
		
		$personalTable = new FaqTable($this->getServiceLocator());
		$projectsPost = $personalTable->getRecords($Id);
		//var_dump($projectsPost); die;
		if (!$projectsPost) {
			return $this->redirect()->toRoute('admin-faq');
		} else {
		    $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
		    $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array("enable" => "1"));
			$postForm = new FaqForm($allActiveLanguages);
			//var_dump($projectsPost[0]); die;
			$postForm->populateValues($projectsPost[0]);
			$request = $this->getRequest();
			if ($request->isPost()) {
				$data = $request->getPost();
				$tags = array();
				$tags = $data['tag_title'];
				$_data = $data->toArray();
				$postForm->setData($_data);
				if ($postForm->isValid()) {
					$editData = $postForm->getData();
					//var_dump($editData); die;
					
					//try {
						$isEditted  = $personalTable->update($editData, $Id);
						if ($isEditted !== false) {
						    
							$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
							return $this->redirect()->toRoute('admin-faq');
						}else{
						    $this->layout()->errorMessage = __("Operation failed!");
						    return $this->redirect()->toRoute('admin-faq');
						}
					/* } catch (\Exception $e) {
					    //var_dump($e); die;
						$view['error'] = '<div class="alert alert-danger" role="alert">قبلا پروژه ای با این آدرس URL ثبت شده است. لطفا آدرس URL دیگری انتخاب نمایید.</div>';
					} */
				} else {
				    //var_dump($postForm->getMessages()); die;
					$this->layout()->errorMessage = __("Please Fill necessary fileds!");
				}
			}
			$view['post'] = $projectsPost;
			$view['postForm'] = $postForm;
			return new ViewModel($view);
		}
	}
	

	public function deletePostAction()
	{

		$container = new Container('token');
		$Id = $this->params('id', -1);
		$token = $this->params('token');
		$userId = $this->userData->id;
	    $faqTable = new FaqTable($this->getServiceLocator());
		$perssonById = $faqTable->getRecords($Id);

		
		$canDeleteAllPosts = $this->permissions()->getPermissions('faq-admin-delete-faq');
		if ($Id && $container->offsetExists('csrf')) {
			if ($container->offsetGet('csrf') == $token) {
				if ($canDeleteAllPosts) {
					$userId = null;
				}
				if ($faqTable->deleteFaq($Id)) {
					$this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
					return $this->redirect()->toRoute('admin-faq');
				} else {
					$this->layout()->errorMessage = __("Operation failed!");
					return $this->redirect()->toRoute('admin-faq');
				}
			}
		}
		return $this->redirect()->toRoute('admin-faq');
	}
	
	
}
