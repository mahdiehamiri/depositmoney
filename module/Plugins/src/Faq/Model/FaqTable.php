<?php 

namespace Faq\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;


class FaqTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('faq', $this->getDbAdapter());
    }
    
	public function getRecords($id = false)
	{
	    $select = new Select('faq');
	    $select->where(array(
	       'id' => $id
	    ));
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchAll($getPaginator = NULL)
	{
		$select = new Select('faq');
        $select->order('lang','order');
		if ($getPaginator) {
		   
		    return $select;
		} else {
		    return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	
	public function fetchAllForView($getPaginator = NULL,$lang)
	{
	    $select = new Select('faq');
	    $select->where(array(
	        'status' => 1,
	        'lang'   => $lang
	    ));
        $select->order('order');
	    return $this->tableGateway->selectWith($select)->toArray();

	}

	
	public function add($newData)
	{
		return $this->tableGateway->insert(array(
				'question'       => $newData['question'],
				'answer'   => $newData['answer'],
				'status'      => $newData['status'],
		        'lang'      => $newData['lang'],
		        'order'      => $newData['order']
		));
	}
	
	public function updateFaq($newData, $faqId)
	{

			return $this->tableGateway->update(array(
					'question'       => $newData['question'],
				'answer'   => $newData['answer'],
				'status'      => $newData['status'],
			    'lang'      => $newData['lang'],
			    'order'      => $newData['order']
			), array('id' => $faqId));
		
	}
	
	public function update($newData, $faqId)
	{
		return $this->tableGateway->update(array(
				'question'       => $newData['question'],
				'answer'   => $newData['answer'],
				'status'      => $newData['status'],
		        'lang'      => $newData['lang'],
		        'order'      => $newData['order']
		), array(
				'id' => $faqId
		));
	}
	
	public function edit($newData, $faqId)
	{
	    return $this->tableGateway->update(array(
	        
	        'order'     => $newData['order']
	    ), array(
	        'id' => $faqId
	    ));
	}
	
	public function deleteFaq($faqId)
	{
		return $this->tableGateway->delete(array(
				'id' => $faqId
		));
	}
}
