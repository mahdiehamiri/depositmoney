<?php

namespace Faq\Form;

use Zend\Form\Form;
use Zend\Form\Element; 
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class UploadForm extends Form
{
	public function __construct()
	{
		parent::__construct('postForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		 
		
		$image = new Element\File('image');  
		  
		$this->add($image);
		$this->inputFilters();
	}	
	
	public function inputFilters()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(
		    array(
		        'name' => 'image',
		        'required' => true,
		        'validators' => array(
		            array(
		                'name' => '\Application\Helper\ImageValidator',
		                'options' => array(
		                        'minSize' => '64',
		                        'maxSize' => '1024', 
		                        'uploadPath' => './public/image/projects/'
		                )
		            )
		        )
		    )
		)); 
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
	
}