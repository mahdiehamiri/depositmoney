<?php
namespace Faq\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class FaqForm extends Form
{

    public function __construct($allActiveLanguages)
    {
        parent::__construct('postForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'id' => 'FaqForm',
            'novalidate' => true
        ));
        
        $faqLang = new Element\Select('lang');
        $faqLang->setAttributes(array(
            'id' => 'page_lang',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $languages = array();
        if ($allActiveLanguages) {
            foreach ($allActiveLanguages as $language) {
                $languages[$language['code']] = $language['title'];
            }
        }
        $faqLang->setValueOptions($languages);
        $faqLang->setLabel(__("Choose language"));
        
        $question = new Element\Textarea('question');
        $question->setAttributes(array(
            'id' => 'question',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'placeholder' => __("Add question")
        ));
        $question->setLabel(__('Add question'));
        
        $answer = new Element\Textarea('answer');
        $answer->setAttributes(array(
            'id' => 'answer',
            'class' => 'form-control validate[required]',
            'required' => 'required',
            'placeholder' => __("Add answer")
        ));
        $answer->setLabel(__('Add answer'));
        
        $postStatus = new Element\Checkbox('status');
        $postStatus->setAttributes(array(
            'id' => 'status'
            // 'class' => 'form-control'
        ));
        $postStatus->setLabel(__('Active/Deactive'));
        
        $order = new Element\Text('order');
        $order->setAttributes(array(
            'id' => 'order',
            'class' => 'form-control form-control-mini',
            'placeholder' => __("Sort")
            // 'required' => 'required'
        ));
        $order->setLabel(__('Sort'));
        
        $csrf = new Element\Csrf('faqcsrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__('Save'));
        $submit->setAttributes(array(
            'id' => 'submit',
            'class' => 'btn btn-primary'
        ));
        
        $submit2 = new Element\Submit('submit2');
        $submit2->setValue(__("Save and New"));
        $submit2->setAttributes(array(
            'class' => 'btn btn-primary',
            'id' => 'submit'
        ));
        
        $this->add($question)
            ->add($faqLang)
            ->add($answer)
            ->add($postStatus)
            ->add($order)
            ->add($csrf)
            ->add($submit2)
            ->add($submit);
        $this->inputFilters();
    }

    public function inputFilters()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'question',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        $inputFilter->add($factory->createInput(array(
            'name' => 'answer',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }
}
