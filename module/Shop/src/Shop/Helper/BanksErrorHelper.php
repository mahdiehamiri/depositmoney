<?php 

namespace Shop\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Application\Model\GeneralTable;
use Application\Model\UserGroupGeneralTable;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Language\Model\LanguageLanguagesTable;
use Shop\Model\ShopProductsTable;
use Shop\Model\ShopCategoriesTable;
use Application\Model\ApplicationFileManagerTable;
use Shop\Model\ShopProductAttributeInfo;
use Shop\Model\ShopAttributeGroupIndexTable;
use Shop\Model\ShopProductAttributesTable;
use Shop\Model\DiscountTable;
use Zend\Db\Sql\Where;
use Shop\Model\TaxTable;

class BanksErrorHelper extends AbstractPlugin
{

    public function getMellatBankError($id)
    {
        $error ['0'] = 'تراكنش با موفقيت انجام شد';
        $error ['11'] = 'شماره كارت نامعتبر است ';
        $error ['12'] = 'موجودي كافي نيست ';
        $error ['13'] = 'رمز نادرست است ';
        $error ['14'] = 'تعداد دفعات وارد كردن رمز بيش از حد مجاز است ';
        $error ['15'] = 'كارت نامعتبر است ';
        $error ['16'] = 'دفعات برداشت وجه بيش از حد مجاز است';
        $error ['17'] = 'كاربر از انجام تراكنش منصرف شده است ';
        $error ['18'] = 'تاريخ انقضاي كارت گذشته است ';
        $error ['19'] = 'مبلغ برداشت وجه بيش از حد مجاز است ';
        $error ['111'] = 'صادر كننده كارت نامعتبر است ';
        $error ['112'] = 'خطاي سوييچ صادر كننده كارت ';
        $error ['113'] = 'پاسخي از صادر كننده كارت دريافت نشد ';
        $error ['114'] = 'دارنده كارت مجاز به انجام اين تراكنش نيست ';
        $error ['21'] = 'پذيرنده نامعتبر است ';
        $error ['23'] = 'خطاي امنيتي رخ داده است ';
        $error ['24'] = 'اطلاعات كاربري پذيرنده نامعتبر است ';
        $error ['25'] = 'مبلغ نامعتبر است ';
        $error ['31'] = 'پاسخ نامعتبر است ';
        $error ['32'] = 'فرمت اطلاعات وارد شده صحيح نمي باشد ';
        $error ['33'] = 'حساب نامعتبر است ';
        $error ['34'] = 'خطاي سيستمي ';
        $error ['35'] = 'تاريخ نامعتبر است ';
        $error ['41'] = 'شماره درخواست تكراري است ';
        $error ['42'] = 'تراکنش Sale یافت نشد';
        $error ['43'] = ' قبلا درخواست Verify داده شده است';
        $error ['44'] = ' درخواست Verfiy يافت نشد';
        $error ['45'] = 'تراكنش   Settle شده است';
        $error ['46'] = 'تراكنش   Settle نشده است';
        $error ['47'] = 'تراكنش   Settle يافت نشد';
        $error ['48'] = 'تراكنش  Reverse شده است ';
        $error ['49'] = 'تراكنش   Refund يافت نشد';
        $error ['412'] = 'شناسه قبض نادرست است  ';
        $error ['413'] = 'شناسه پرداخت نادرست است ';
        $error ['414'] = 'سازمان صادر كننده قبض نامعتبر است ';
        $error ['415'] = 'زمان جلسه كاري به پايان رسيده است ';
        $error ['416'] = 'خطا در ثبت اطلاعات ';
        $error ['417'] = 'شناسه پرداخت كننده نامعتبر است ';
        $error ['418'] = 'اشكال در تعريف اطلاعات مشتري ';
        $error ['419'] = 'تعداد دفعات ورود اطلاعات از حد مجاز گذشته است ';
        $error ['421'] = ' IP نامعتبر است';
        $error ['51'] = 'تراكنش تكراري است ';
        $error ['54'] = 'تراكنش مرجع موجود نيست ';
        $error ['55'] = 'تراكنش نامعتبر است ';
        $error ['61'] = 'خطا در واريز ';
        if(isset($error[$id]))
            return $error[$id];
        else
            return 'تراكنش نامعتبر است ';
    }
    
    
}