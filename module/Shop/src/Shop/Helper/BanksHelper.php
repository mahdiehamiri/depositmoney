<?php
namespace Shop\Helper;

//use Zend\Mvc\Controller\Plugin\AbstractPlugin;
;
use Shop\Model\PaymentsTable;
use Shop\Form\Banks\MellatForm;
use Shop\Form\Banks\SamanForm;
use Shop\Model\BanksTable;

class BanksHelper //extends AbstractPlugin
{
	public $userData ;
	protected $Configuration     ;
	protected $sm         ;
	protected $amount            ;
	protected $orderId          ;
	protected $id                ;
	protected $name              ;
	protected $form_action_url   ;
	protected $callback_url      ;
	protected $username          ;
	protected $password          ;
	protected $wsdl_url          ;
	protected $local_wsdl_url    ;
	protected $service_ip        ;
	protected $terminal_id       ;
	protected $extra_options     ;
	public function __construct($orderId , $amount , $bankId , $sm = null)
	{
	    if(!$this->orderId)
	    {   
	        $this->sm = $sm ;
	        $this->orderId = $orderId ;
	        $this->amount   = $amount ;
             $banksTable   = new BanksTable($sm);
             $BankInfo        = $banksTable->fetchAll(array('id = ?' => $bankId));
             if($BankInfo)
             {
                $BankInfo = $BankInfo[0];
              	 $this->id                 = $BankInfo['id'];
             	 $this->name               = $BankInfo['name'];
             	 $this->form_action_url    = $BankInfo['form_action_url'];
             	 $this->callback_url       = "http://".$BankInfo['callback_url']; 
             	 //$this->callback_url       = "http://lupayamak.netshahr.com".$BankInfo['callback_url']; 
             	 $this->username           = $BankInfo['username'];
             	 $this->password           = $BankInfo['password'];
             	 $this->wsdl_url           = $BankInfo['wsdl_url'];
             	 $this->local_wsdl_url     = $BankInfo['local_wsdl_url'];
             	 $this->service_ip         = $BankInfo['service_ip'];
             	 $this->terminal_id        = $BankInfo['terminal_id'];
             	 $this->bank_type          = $BankInfo['bank_type'];
             	 $this->extra_options      = $BankInfo['extra_options'];
             }
	    }
	}

	public function getForm($Configuration)
	{
	    switch ($this->bank_type)
	    {
	    	case 'mellat':  /// banke mellat
	    	    {
	    	        return $this->mellat();
	    	    }
	    	case 'saman':  /// banke mellat
	    	    {
	    	        $this->MID = $Configuration['saman-MID'];
	    	        return $this->Saman();
	    	    }
    	    case 'saderat':  /// banke mellat
    	        {
    	            $this->MID = $Configuration['saderat-MID'];
    	            return $this->Saderat();
    	        }
	    }
	    return array('result'=>false,'message'=>"id not found : id = ".$this->id);
	}

	public function mellat()
	{
	    // TODO DEVELOPER TEST SHOULD BE REMOVE
	    //============================ DEVELOPER TEST ==================
//         	    $mellatForm = new mellatForm("refid-example");
//         	    $mellatForm->setAttribute('action', $this->form_action_url);
//         	    return array('result'=>true,'form'=>$mellatForm ,'view' => 'mellat' );
	    //============================ END DEVELOPER TEST ==============

	    
	    
	    
	    $success = true;
	    $status = 0;
	    $RefId = 5555;
	    if ($success) {
	        $PaymentsTable = new PaymentsTable($this->sm);
	        $isUpdated = $PaymentsTable->setReferenceId($this->orderId, $RefId );
	        
	        if (! is_null ( $RefId ) && $status == 0 && $isUpdated !== false) {
	            $success = TRUE;
	        } else {
	            $success = FALSE;
	            $message = $status;
	        }
	    }
	    if ($success) {
	        
	        $mellatForm = new MellatForm($RefId);
	        $mellatForm->setAttribute('action', $this->form_action_url);
	        return array('result'=>true,'form'=> $mellatForm ,'view' => 'mellat');
	    }
	    die;
	    
		$success = true;
		$options = array();
		if (is_array($this->extra_options)) {
	        $options =  (array)$this->extra_options;
	    } elseif ($this->extra_options) {
	        $options =  (array)json_decode($this->extra_options);
	        if (json_last_error() != JSON_ERROR_NONE) {
	            $options = array();
	        }
	    } 
	    try {
	         $client = new \SoapClient($this->wsdl_url, $options);
	    }catch (\SoapFault $sf)
	    {
	        try{
	            $client = new \SoapClient($this->local_wsdl_url, $options);
	        }catch (\SoapFault $sf)
	        {
	            $success = FALSE;
	            $message = 'خطا در برقراری ارتباط با بانک ملت ';
	        }
	    }

	    if ($success) {
	        $params = array (
	                'terminalId' => $this->terminal_id ,
	                'userName' => $this->username ,
	                'userPassword' => $this->password ,
	                'orderId' =>  $this->orderId ,
	                'amount' =>   $this->amount ,
	                'localDate' => date ( "Ymd" ),
	                'localTime' => date ( "His" ),
	                'additionalData' => '',
	                'callBackUrl' =>  $this->callback_url ,
	                'payerId' => 0
	            );

	        try {
	            $result = $client->bpPayRequest($params);
	        } catch ( \SoapFault $sf ) {
	            $success = FALSE;
	            $message = 'خطا در برقراری ارتباط با بانک ملت';
	        }
	        if($success)
	        {
	            $result = ( array ) $result->return;
	            $result = $result [0];
	            $result = explode ( ',', $result );
	            $status = $result [0];
	            $RefId = @$result [1];
	            if (! is_null ( $RefId ) && $status == 0) {
	                $success = TRUE;
	            } else {
	                $success = FALSE;
	                $message = $status;
	            }
	        }
	    }
	    if ($success) {
	        $PaymentsTable = new PaymentsTable($this->sm);
	        $isUpdated = $PaymentsTable->SetReferenceId( $this->orderId , $RefId );
	        if (! is_null ( $RefId ) && $status == 0 && $isUpdated) {
	            $success = TRUE;
	        } else {
	            $success = FALSE;
	            $message = $status;
	        }
	    }

	    if ($success) {
             $mellatForm = new MellatForm($RefId);
             $mellatForm->setAttribute('action', $this->form_action_url);
	        return array('result'=>true,'form'=>$mellatForm ,'view' => 'mellat');
	    }else{
	        return array('result'=>false ,'message'=>$message);
	    }
	}

	
	
	
	public function Saman()
	{
	    // TODO DEVELOPER TEST SHOULD BE REMOVE
	    //============================ DEVELOPER TEST ==================
	    //         	    $mellatForm = new MellatForm("refid-example");
	    //         	    $MellatForm->setAttribute('action', $this->form_action_url);
	    //         	    return array('result'=>true,'form'=>$MellatForm ,'view' => 'mellat' );
	    //============================ END DEVELOPER TEST ==============
	    $success = true;
// 	    $options = array();
// 	    if (is_array($this->extra_options)) {
// 	        $options =  (array)$this->extra_options;
// 	    } elseif ($this->extra_options) {
// 	        $options =  (array)json_decode($this->extra_options);
// 	        if (json_last_error() != JSON_ERROR_NONE) {
// 	            $options = array();
// 	        }
// 	    }
// 	    try {
// 	        $client = new \SoapClient($this->wsdl_url, $options);
// 	    }catch (\SoapFault $sf)
// 	    {
// 	        try{
// 	            $client = new \SoapClient($this->local_wsdl_url, $options);
// 	        }catch (\SoapFault $sf)
// 	        {
// 	            $success = FALSE;
// 	            $message = 'خطا در برقراری ارتباط با بانک سامان ';
// 	        }
// 	    }
	
// 	    if ($success) {
// 	        $params = array (
// 	            'terminalId' => $this->terminal_id ,
// 	            'userName' => $this->username ,
// 	            'userPassword' => $this->password ,
// 	            'orderId' =>  $this->orderId ,
// 	            'amount' =>   $this->amount ,
// 	            'localDate' => date ( "Ymd" ),
// 	            'localTime' => date ( "His" ),
// 	            'additionalData' => '',
// 	            'callBackUrl' =>  $this->callback_url ,
// 	            'payerId' => 0
// 	        );
// 	        // 	        var_dump($params);
	
// 	        try {
// 	            $result = $client->bpPayRequest($params);
// 	        } catch ( \SoapFault $sf ) {
// 	            $success = FALSE;
// 	            $message = 'خطا در برقراری ارتباط با بانک سامان';
// 	        }
// 	        if($success)
// 	        {
// 	            $result = ( array ) $result->return;
// 	            $result = $result [0];
// 	            $result = explode ( ',', $result );
// 	            $status = $result [0];
// 	            $RefId = @$result [1];
// 	            if (! is_null ( $RefId ) && $status == 0) {
// 	                $success = TRUE;
// 	            } else {
// 	                $success = FALSE;
// 	                $message = $status;
// 	            }
// 	        }
// 	    }
// 	    if ($success) {
// 	        $PaymentsTable = new PaymentsTable($this->sm);
// 	        $isUpdated = $PaymentsTable->SetReferenceId( $this->orderId , $RefId );
// 	        if (! is_null ( $RefId ) && $status == 0 && $isUpdated) {
// 	            $success = TRUE;
// 	        } else {
// 	            $success = FALSE;
// 	            $message = $status;
// 	        }
// 	    }
	
	    if ($success) {
	        $SamanForm = new SamanForm($this->amount,$this->orderId,$this->MID,$this->callback_url);
	        $SamanForm->setAttribute('action', $this->form_action_url);
	        return array('result'=>true,'form'=>$SamanForm ,'view' => 'saman');
	    }else{
	        return array('result'=>false ,'message'=>"saman");
	    }
	}
	
	
	
	public function Saderat()
	{
	    // TODO DEVELOPER TEST SHOULD BE REMOVE
	    //============================ DEVELOPER TEST ==================
	    //         	    $MellatForm = new MellatForm("refid-example");
	    //         	    $MellatForm->setAttribute('action', $this->form_action_url);
	    //         	    return array('result'=>true,'form'=>$MellatForm ,'view' => 'mellat' );
	    //============================ END DEVELOPER TEST ==============
	    $success = true;
	 
	    if ($success) {
	        $SamanForm = new SamanForm($this->amount,$this->orderId,$this->MID,$this->callback_url);
	        $SamanForm->setAttribute('action', $this->form_action_url);
	        return array('result'=>true,'form'=>$SamanForm ,'view' => 'saderat');
	    }else{
	        return array('result'=>false ,'message'=>"saderat");
	    }
	}

	
}