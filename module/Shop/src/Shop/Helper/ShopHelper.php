<?php 

namespace Shop\Helper;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Application\Model\GeneralTable;
use Application\Model\UserGroupGeneralTable;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Language\Model\LanguageLanguagesTable;
use Shop\Model\ShopProductsTable;
use Shop\Model\ShopCategoriesTable;
use Application\Model\ApplicationFileManagerTable;
use Shop\Model\ShopProductAttributeInfo;
use Shop\Model\ShopAttributeGroupIndexTable;
use Shop\Model\ShopProductAttributesTable;
use Shop\Model\DiscountTable;
use Zend\Db\Sql\Where;
use Shop\Model\TaxTable;
use Shop\Model\UsersTable;
use Shop\Model\DiscountBulkTable;
use Configuration\Model\ConfigurationTable;
use Shop\Model\ShopProductVideoFilesTable;
use Zend\Session\Container;

class ShopHelper extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    public function getServiceLocator() {
	    return $this->serviceLocator->getServiceLocator();
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
	    $this->serviceLocator = $serviceLocator;
	}
	
	public function getTaxes($id = null, $returnTotal = true) {
	   $taxTable = new TaxTable($this->getServiceLocator());
	     
	   $allTaxes["all"] = 0;
	   $allTaxes["category"] = 0;
	   $allTaxes["product"] = 0;
	    
	   //All
	   $where = new Where();
	   $where->equalTo("kind", "All");
	   $allTaxes["all"] = $taxTable->getAllCurrentTaxes($where);
	   

	    
	   //Assign category
	   if ($id) {
	       $shopProductTable = new ShopProductsTable($this->getServiceLocator());
	       $productsInfo = $shopProductTable->fetchById($id);
	       $productCategoryId = $productsInfo[0]["category_id"];
	   
	      
	       $where = new Where();
	       $where->equalTo("kind", "Category");
	       $where->equalTo("category_product_id", $productCategoryId);
	       $allTaxes["category"] = $taxTable->getAllCurrentTaxes($where);

	    
	    
	   //Assign product
	  
	       $shopProductTable = new ShopProductsTable($this->getServiceLocator());
	        
	       
	       $where = new Where();
	       $where->equalTo("kind", "Product");
	       $where->equalTo("category_product_id", $id);
	       $allTaxes["product"] = $taxTable->getAllCurrentTaxes($where);
	   
	     
	   }
	   $allTaxes["total"]  = $allTaxes["all"] + $allTaxes["category"] + $allTaxes["product"] ;
	   if ($returnTotal) {
	       return $allTaxes["total"];
	   }
	   return $allTaxes;
	}
	
	public function getDiscounts($userId = null, $id = null, $returnTotal = true) {



	    $configurationTable = new ConfigurationTable($this->getServiceLocator());
	    $allConfigs = $configurationTable->getRecords();
	    $configs = array();
	    foreach ($allConfigs as $config) {
	        $configs[$config["name"]] = $config["value"];
	    }
	     
	    $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
	     
	    $discountTable = new DiscountTable($this->getServiceLocator());
	     
	    $allDiscounts["all_total"] = 0;
	    $allDiscounts["category_total"] = 0;
	    $allDiscounts["category_parent_total"] = 0;
	    $allDiscounts["product_total"] = 0;
	    $allDiscounts["purchase_total"] = 0;
	     
	    $allDiscounts["bulk_total"] = 0;
	    $usersTable = new UsersTable($this->getServiceLocator());
	    
	    if ($userId) {
	        $childsCount = $usersTable->getUserChildsCount($userId);
	        if ($childsCount) {
	            $count = $childsCount[0]["count"];
	            $discountBulkTable = new DiscountBulkTable($this->getServiceLocator());
	            $discount  = $discountBulkTable->getDiscountBulkByCount($count);
	             
	            if ($discount) {
	                $allDiscounts["bulk_total"] = $discount[0]["bulk_users_discount"];
	            }
	        }
	    }
	     
	    //All
	    $where = new Where();
	    $where->equalTo("type", "Global");
	    $where->equalTo("kind", "All");
	    $allDiscounts["all"] = $discountTable->getAllCurrentDiscounts($where);
	    
	     
	    if ($userId) {
	        //All
	        $where = new Where();
	        $where->equalTo("type", "Private");
	        $where->equalTo("kind", "All");
	        $where->equalTo("user_id", $userId);
	        $allDiscounts["all_user"] = $discountTable->getAllCurrentDiscounts($where);
	        $allDiscounts["all_total"] = $allDiscounts["all"] + $allDiscounts["all_user"];
	    } else {
	        $allDiscounts["all_total"] = $allDiscounts["all"];
	    }
	     
	     
	    //Purchase
	     
	    $container = new Container('addtocard');
	    if ($container->offsetExists("totalSum")) {
	        $totalSum = $container->offsetGet("totalSum");
	        //var_dump($totalSum);
	        $where = new Where();
	        $where->equalTo("type", "Global");
	        $where->equalTo("kind", "Purchase");
	        $where->lessThanOrEqualTo("purchase_from", $totalSum);
	        $where->greaterThanOrEqualTo("purchase_to", $totalSum);
	        $where->equalTo("kind", "Purchase");
	        $allDiscounts["purchase"] = $discountTable->getAllCurrentDiscounts($where);
	    
	        if ($userId) {
	            //Purchase
	            $where = new Where();
	            $where->equalTo("type", "Private");
	            $where->equalTo("kind", "Purchase");
	            $where->lessThanOrEqualTo("purchase_from", $totalSum);
	            $where->greaterThanOrEqualTo("purchase_to", $totalSum);
	            $where->equalTo("user_id", $userId);
	             
	            $allDiscounts["purchase_user"] = $discountTable->getAllCurrentDiscounts($where);
	            $allDiscounts["purchase_total"] = $allDiscounts["purchase"] + $allDiscounts["purchase_user"];
	        } else {
	            $allDiscounts["purchase_total"] = $allDiscounts["purchase"];
	        }
	    }
	     
	    //Assign category
	    if ($id) {
	        $shopProductTable = new ShopProductsTable($this->getServiceLocator());
	        $productsInfo = $shopProductTable->fetchById($id);
	        $productCategoryId = $productsInfo[0]["category_id"];
	    
	        //All
	        $where = new Where();
	        $where->equalTo("type", "Global");
	        $where->equalTo("kind", "Category");
	        $where->equalTo("p_g_id", $productCategoryId);
	        $allDiscounts["category"] = $discountTable->getAllCurrentDiscounts($where);
	         
	        if ($userId) {
	            //All
	            $where = new Where();
	            $where->equalTo("type", "Private");
	            $where->equalTo("kind", "Category");
	            $where->equalTo("p_g_id", $productCategoryId);
	            $where->equalTo("user_id", $userId);
	            $allDiscounts["category_user"] = $discountTable->getAllCurrentDiscounts($where);
	            $allDiscounts["category_total"] = $allDiscounts["category"] + $allDiscounts["category_user"];
	        } else {
	            $allDiscounts["category_total"] = $allDiscounts["category"];
	        }
	         
	        
	        //Parents category
	        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
	        $productCategoryId = $productsInfo[0]["category_id"];
	        $categoryInfo = $shopCategoriesTable->fetchCategory($productCategoryId);
	        $productParentCategoryId = $categoryInfo[0]["parent_id"];
	        //All
	        $where = new Where();
	        $where->equalTo("type", "Global");
	        $where->equalTo("kind", "Category");
	        $where->equalTo("p_g_id", $productParentCategoryId);
	        $allDiscounts["category_parent"] = $discountTable->getAllCurrentDiscounts($where);
	        
	        if ($userId) {
	            //All
	            $where = new Where();
	            $where->equalTo("type", "Private");
	            $where->equalTo("kind", "Category");
	            $where->equalTo("p_g_id", $productParentCategoryId);
	            $where->equalTo("user_id", $userId);
	            $allDiscounts["category_parent_user"] = $discountTable->getAllCurrentDiscounts($where);
	            $allDiscounts["category_parent_total"] = $allDiscounts["category_parent"] + $allDiscounts["category_parent_user"];
	        } else {
	            $allDiscounts["category_parent_total"] = $allDiscounts["category_parent"];
	        }
	        
	        
	        
	         
	         
	        //Assign product
	         
	        $shopProductTable = new ShopProductsTable($this->getServiceLocator());
	         
	        //All
	        $where = new Where();
	        $where->equalTo("type", "Global");
	        $where->equalTo("kind", "Product");
	        $where->equalTo("p_g_id", $id);
	        $allDiscounts["product"] = $discountTable->getAllCurrentDiscounts($where);
	    
	        if ($userId) {
	            //All
	            $where = new Where();
	            $where->equalTo("type", "Private");
	            $where->equalTo("kind", "Product");
	            $where->equalTo("p_g_id", $id);
	            $where->equalTo("user_id", $userId);
	            $allDiscounts["product_user"] = $discountTable->getAllCurrentDiscounts($where);
	            $allDiscounts["product_total"] = $allDiscounts["product"] + $allDiscounts["product_user"];
	        } else {
	            $allDiscounts["product_total"] = $allDiscounts["product"];
	        }
	    }
	     
	     
	    $allDiscounts["total"]  = $allDiscounts["all_total"] + $allDiscounts["purchase_total"] + $allDiscounts["category_total"] + $allDiscounts["category_parent_total"] + $allDiscounts["product_total"] + $allDiscounts["bulk_total"] ;
	    
	    if ($returnTotal) {
	        if ($allDiscounts["total"] >= $configs["max-discount"]) {
	            return $configs["max-discount"];
	        } else {
	            return $allDiscounts["total"];
	        }
	    }
	    return $allDiscounts;
	    
	}
	
    public function getProductsInfo($ids = null, $slugs = null, $returnDeleted = false, $lang = "fa") 
    {
        $shopProductTable = new ShopProductsTable($this->getServiceLocator());
        $productsInfo = $shopProductTable->getProductsByIdsSlugs($ids, $slugs, $returnDeleted, $lang);
        if (!$productsInfo) {
            return false;
        }
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $applicationFileManagerTable = new ApplicationFileManagerTable($this->getServiceLocator());
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $shopProductAttributeInfoTable = new ShopProductAttributeInfo($this->getServiceLocator());
        $applicationFileManagerTable = new ApplicationFileManagerTable($this->getServiceLocator());
        $shopAttributeGroupIndex = new ShopAttributeGroupIndexTable($this->getServiceLocator());
        $shopProductAttributes = new ShopProductAttributesTable($this->getServiceLocator());
        foreach ($productsInfo as $k => $productInfo) {
            
            $cat_id = $productInfo["category_id"];
          
            $productInfoByCategoryId = $shopCategoriesTable->fetchAll(null, $cat_id);
          
            //attributes start
            $productId = $productInfo["id"];
            $currentFiles = $applicationFileManagerTable->fetch($productId);
            
          
            $currentInfo = $shopProductAttributeInfoTable->fetch($productId);
            
            $attributeGroupsIds = $productInfoByCategoryId[0]['attribute_groups'];
            $attributeGroupsIds = explode(',', $attributeGroupsIds);
            
            $allAttributeIds = array();
            if ($attributeGroupsIds) {
                
                foreach ($attributeGroupsIds as $piece) {
                    $allAttributeIds[] = $shopAttributeGroupIndex->fetch($piece);
            
                }
            }
            
            $allAttributes = array();
            if ($allAttributeIds) {
               
                foreach ($allAttributeIds as $key => $attributeId) {
                    foreach ($attributeId as $innerKey => $id) {
                        $allAttributes[] = $shopProductAttributes->fetchAll($attributeId[$innerKey]['attribute_id']);                         
                    }
                }
            }
            $productInfo["all_attributes"] = array();
            $productInfo["all_files"]["public_file"] = array();
            $productInfo["all_files"]["image_file"] = array();
            foreach ($allAttributes as $attribute) {
                foreach ($currentInfo as $productAttr) {
                    if ($attribute[0]["type"] !== "public_file" && $attribute[0]["type"] !== "image_file") {
                        if (isset($productAttr["product_attribute_name"]) && $productAttr["product_attribute_name"] == $attribute[0]["developer_name"]) {
                            $productInfo["all_attributes"][$attribute[0]["developer_name"]]["name"] = $attribute[0]["title"];
                            $productInfo["all_attributes"][$attribute[0]["developer_name"]]["value"] = $productAttr["value"];

                        }
                    } else {
                        if (isset($currentFiles[$attribute[0]["developer_name"]][0])) {
                            $productInfo["all_files"][$currentFiles[$attribute[0]["developer_name"]][0]["field_type"]][$attribute[0]["developer_name"]]["name"] = $attribute[0]["title"];
                            $productInfo["all_files"][$currentFiles[$attribute[0]["developer_name"]][0]["field_type"]][$attribute[0]["developer_name"]]["path"] = $currentFiles[$attribute[0]["developer_name"]][0]['folder'] .  $currentFiles[$attribute[0]["developer_name"]][0]['entity_type'] . "/" . $currentFiles[$attribute[0]["developer_name"]][0]['name'];
                            $productInfo["all_files"][$currentFiles[$attribute[0]["developer_name"]][0]["field_type"]][$attribute[0]["developer_name"]]["value"] = $currentFiles[$attribute[0]["developer_name"]][0];
                        }
                    }
                }
            }
            $productsInfo[$k] = $productInfo;
        }
        
        return $productsInfo;
        
        
        
        
    }
    
    
}