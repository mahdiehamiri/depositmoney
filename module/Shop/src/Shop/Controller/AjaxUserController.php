<?php

namespace Shop\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Application\Model\ApplicationFileManagerTable;
use Application\Helper\BaseController;
use Shop\Model\EducationLessonsTable;
use Shop\Model\ShopProductsTable;
use Application\Helper\BaseAdminController;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Shop\Model\ProvincesTable;
use Shop\Model\CitiesTable;

class AjaxUserController extends BaseController
{	
    public function getCitiesAction()
    {
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
    
        $citiesTable = new CitiesTable($this->getServiceLocator());
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $citiesByProviceId = $citiesTable->getRecords($data['id']);
            die(json_encode($citiesByProviceId));
        }
    }
    public function getProvinceAction()
    {
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
    
        $ProvinceTable = new ProvincesTable($this->getServiceLocator());
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $citiesByProviceId = $ProvinceTable->getRecordsByCountry($data['id']);
            die(json_encode($citiesByProviceId));
        }
    }
    public function deleteProductCardAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $productId = $data['productId'];
    
            $container = new Container('addtocard');
            unset($container->{$productId});
    
            $totalSum = 0;
            $totalQuantity = 0;
            foreach ($container as $key => $value)
            {
                if (isset($value["sum"])) {
                    $totalSum += $value["sum"];
                    $totalQuantity += $value["quantity"];
                }
            }
            $container->totalSum = $totalSum;
            $data["totalQuantity"] = $totalQuantity;
            $data["totalSum"] = $totalSum;
           
            die(json_encode($data));
        }
        die("false");
    }

}
