<?php

namespace Shop\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Application\Model\ApplicationFileManagerTable;
use Application\Helper\BaseController;
use Shop\Model\EducationLessonsTable;
use Shop\Model\ShopProductsTable;
use Application\Helper\BaseAdminController;
use Shop\Model\ShopCategoriesTable;

class AjaxController extends BaseAdminController
{	
   public function getProductByIdAction(){
       $request = $this->getRequest();
       if ($request->isPost()) {
           $data = $request->getPost()->toArray();
           $shopProductTable = new ShopProductsTable($this->getServiceLocator());
           $productList = $shopProductTable->getRecordsByParentId($data["id"]);
           die(json_encode($productList));
       }
       else
           die('error');
       
   }
   
    public function getLessonsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $educationLessonsTable = new EducationLessonsTable($this->getServiceLocator());
		    $lessonsList = $educationLessonsTable->getRecordsByParentId($data["study_id"]);
            die(json_encode($lessonsList));
        }
        else
            die('error');
    }
    
    public function deleteFileAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
            $data = $request->getPost()->toArray();
            if ($data['productId']) {
                $productData = $shopProductsTable->fetchById($data['productId']);
                if ($productData) {
                    $isDeleted = $shopProductsTable->deleteProductFile($data['productId']);
                    if ($isDeleted) {
                        $fileToDelete = $_SERVER['DOCUMENT_ROOT'] . "/uploads/shop/product/" . $productData[0]['file'];
                        @unlink($fileToDelete);
                        die("1");
                    } else {
                        die("1");
                    }
                } else {
                    die("0");
                }
            } else {
                die("0");
            }
        } else {
            die("0");
        }
           
    }
    
    public function deleteCategoryIconAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
           
            $data = $request->getPost()->toArray();
            if ($data['catId']) {
                $cagegoryById = $shopCategoriesTable->fetchAll(null, $data['catId']);
                if ($cagegoryById) {
                    $isDeleted = $shopCategoriesTable->deleteCategoryIcon($data['catId']);
                    if ($isDeleted) {
                        $fileToDelete = $_SERVER['DOCUMENT_ROOT'] . "/uploads/shop/category/" . $cagegoryById[0]['icon'];
                        @unlink($fileToDelete);
                        die("1");
                    } else {
                        die("1");
                    }
                } else {
                    die("0");
                }
            } else {
                die("0");
            }
        } else {
            die("0");
        }
         
    }
    
	public function deleteImageAction()
	{
		$request = $this->getRequest();
		if ($request->isPost()) {
			$applicationFileManagerTable = new ApplicationFileManagerTable($this->getServiceLocator());
			$data = $request->getPost()->toArray();
			$imageName = $applicationFileManagerTable->fetchForDelete($data['imageId']);
			if ($imageName)
			{
				foreach ($imageName as $image) {
					$isDeleted = $applicationFileManagerTable->delete($data['imageId']);
					if ($isDeleted) {
						$fileToDelete = $_SERVER['DOCUMENT_ROOT'] . $image['folder'] .  $image['entity_type'] . "/" . $image['name'];
						@unlink($fileToDelete);
						
						echo json_encode(array(
								'status' => 200,
								'message' => 'Successfully deleted'
						));
					} else {
						echo json_encode(array(
								'status' => 400,
								'message' => 'Unable to delete'
						));
					}
				}
			} else {
				echo json_encode(array(
						'status' => 404,
						'message' => 'File not found'
				));
			}
			die;
		}
	}

}
