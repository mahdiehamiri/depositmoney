<?php
namespace Shop\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Smspanel\MasterController\MCConnector;
use Smspanel\Model\BankingTable;
use Smspanel\Model\UsersLinesTable;
use Zend\EventManager\EventManager;
use Smspanel\Helper\Log;
use Application\Helper\BaseController;
use Shop\Model\PaymentsTable;
use Shop\Model\BanksTable;
use Shop\Model\OrdersTable;
use Shop\Model\OrdersDetailsTable;
use Shop\Model\ShopProductsTable;
use Shop\Model\UsersTable;

class CallbackController extends BaseController
{

    public $UserData;

    public function mellatAction()
    {        
        $this->layout('layout/front-end');
        $configuration = $this->configs;
        $request = $this->getRequest();
        $formData = $request->getPost();
        $success = true;
        $paymentsTable = new PaymentsTable($this->getServiceLocator());
        if ($success) {
            $resCode = $formData['ResCode'];
            $RefId = $formData['RefId'];
            $saleOrderId = $order_id = $formData['SaleOrderId']; // / unique_id
            $saleReferenceId = $formData['SaleReferenceId'];
            if (! $RefId || ! $saleOrderId || ! $saleReferenceId) {
                $success = FALSE;
                $this->flashMessenger()->addMessage("اطلاعات نا معتبر است.");
                $this->redirect()->toUrl('shop-home');
            }
            if ($resCode == '0') {
                $success = TRUE;
            } else {
                $success = FALSE;
                $message = $this->banksErrorHelper()->getMellatBankError($resCode);
                
                if (! $message) {
                    $message = 'خطا در تراکنش';
                }
                
                $paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
                if ($saleReferenceId)
                    $message .= "شماره ارجاع بانک جهت پیگیری : $saleReferenceId";
            }
        }
        error_log("calback:" . $message);
        if ($success) {
            $payment = $paymentsTable->fetchAll(array('order_id = ?' => (int) $saleOrderId));
            if (is_null($payment)) {
                $success = FALSE;
                $message = 'چنین پرداختی وجود ندارد';
                $paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
            } else {
                $payment = $payment[0];
                if ($payment['status'] == 'unpaid') {
                    $success = TRUE;
                    $banksTable = new BanksTable($this->getServiceLocator());
                    $bankInfo = $banksTable->fetchAll(array(
                        'id = ?' => $payment['bank_id']
                    ));
                    if ($bankInfo) {
                        $bankInfo = $bankInfo[0];
                        $success = TRUE;
                    } else {
                        $success = FALSE;
                        $message = "اطلاعات بانک یافت نشد .";
                        $paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
                    }
                } else {
                    $success = FALSE;
                    $message = 'قبلا به این درخواست رسیدگی شده است';
                    //$paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
                }
            }
        }
        error_log("calback2:" . $message);
        if ($success) {
            $options = array();
            if (is_array($bankInfo['extra_options'])) {
                $options = (array) $bankInfo['extra_options'];
            } elseif ($bankInfo['extra_options']) {
                $options = (array) json_decode($bankInfo['extra_options']);
                if (json_last_error() != JSON_ERROR_NONE) {
                    $options = array();
                }
            }
            try {
                $client = new \SoapClient($bankInfo['wsdl_url'], $options);
            } catch (\SoapFault $sf) {
                try {
                    $client = new \SoapClient($bankInfo['local_wsdl_url'], $options);
                } catch (\SoapFault $sf) {
                    $success = FALSE;
                    $message = 'خطا در برقراری ارتباط با بانک ملت ';
                    $paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
                }
            }
        }
        if ($success) {
            $params = array(
                'terminalId' => $bankInfo['terminal_id'],
                'userName' => $bankInfo['username'],
                'userPassword' => $bankInfo['password'],
                'orderId' => $payment['order_id'],
                'saleOrderId' => $saleOrderId,
                'saleReferenceId' => $saleReferenceId
            );
            try {
                $result = $client->bpVerifyRequest($params);
                $result = (array) $result->return;
                $result = $result[0];
                if ($result == 0) {
                    $success = TRUE;
                } else {
                    $result = $client->bpInquiryRequest($params);
                    $result = (array) $result->return;
                    $result = $result[0];
                    if ($result == 0) {
                        $success = TRUE;
                    } else {
                        $success = FALSE;
                        $message = $this->banksErrorHelper()->getMellatBankError($result);
                        
                        if (! $message) {
                            $message = $result;
                        }
                        $paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
                        $client->bpReversalRequest($params);
                        if ($saleReferenceId)
                            $message .= " شماره ارجاع بانک جهت پیگیری  :  $saleReferenceId";
                    }
                }
            } catch (\SoapFault $sf) {
                $success = FALSE;
                $message = 'خطا در برقراری ارتباط با بانک ملت';
                $paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
            }
        }
        error_log("calback3:" . $message);
        if ($success) {
            $result = $client->bpSettleRequest($params);
            $result = (array) $result->return;
            $result = $result[0];
            
            if ($result == 0) {
                $success = TRUE;
            } else {
                $success = FALSE;
                $message = $this->banksErrorHelper()->getMellatBankError($result);
                
                if (! $message) {
                    $message = $result;
                }
                $paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
                $client->bpReversalRequest($params);
                if ($saleReferenceId)
                    $message .= " شماره ارجاع بانک جهت پیگیری  :  $saleReferenceId";
            }
        }
        
        if ($success) {
            $successMessage = "";
            $success = TRUE;
            $successMessage .= "<br> شماره ارجاع بانک جهت پیگیری : " . $saleReferenceId;
            
            
            // confirm payment and order status with transaction
            $con = $paymentsTable->adapter->getDriver()->getConnection();
            $con->beginTransaction();
            $ordersTable = new OrdersTable($this->getServiceLocator());
            
            $PayRes = $paymentsTable->confirmPayment($saleOrderId, $resCode, $saleReferenceId, (isset($message) ? $message : "success"));
            $isAdded = $ordersTable->changeOrderPaymentStatus($saleOrderId, "1");
            
            $ordersDetailsTable = new OrdersDetailsTable($this->getServiceLocator());
            $getOrderDetails = $ordersDetailsTable->getOrderDetailsByOrderId($saleOrderId);
            $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
            foreach ($getOrderDetails as $orderDetail) {
                $shopProductsTable->changeSellCount($orderDetail["product_id"], $orderDetail["quantity"]);
            }
            
            if ($PayRes && $isAdded) {
                
                $usersTable = new UsersTable($this->getServiceLocator());
                $userInfo = $usersTable->getRecords(array("id" => $payment["user_id"])); 
                if ($userInfo && $userInfo[0]["mobile"]) {
                    $message = __("Your order paid successfully, Bank Ref Id: ") . $saleReferenceId . __("CadAfzar order id: ") . $saleOrderId;
                    $this->SMSService()->send($message, $userInfo[0]["mobile"]);
                }
                $con->commit();
                //$this->flashMessenger()->addSuccessMessage(__("Your order paid successfully, Bank Ref Id: ") . $saleReferenceId . __("CadAfzar order id: ") . $saleOrderId);
                return $this->redirect()->toRoute('shop-home', array("action" => "finish-order", "slug" => $saleOrderId));
                //send sms
               
                //end send sms
            } else {
                $con->rollback();
                $success = FALSE;
                $message = "در ثبت اطلاعات مشکلی وجود دارد . مبلغ واریز شده به زودی به حساب شما باز خواهد گشت .";
                $paymentsTable->setErrorPayment($saleOrderId, $resCode, $saleReferenceId, $message);
                $client->bpReversalRequest($params);
                $message .= "<br> شماره ارجاع بانک جهت پیگیری : " . $saleReferenceId;
                return $this->redirect()->toRoute('shop-home', array("action" => "finish-order"));
            }
        }
        if ($success)
            $this->layout()->successMessage = ($successMessage ? $successMessage : ".<br> شماره ارجاع بانک جهت پیگیری : " . $saleReferenceId);
        else
            $this->layout()->errorMessage = ($message ? $message : "در ثبت اطلاعات خطایی رخ داده است .<br> شماره ارجاع بانک جهت پیگیری : " . $saleReferenceId);
        return new ViewModel();
    }
}