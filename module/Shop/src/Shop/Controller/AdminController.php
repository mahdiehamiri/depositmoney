<?php
namespace Shop\Controller;

use Zend\View\Model\ViewModel;
use Shop\Form\ShopCategoriesForm;
use Shop\Model\ShopCategoriesTable;
use Zend\Session\Container;
use Shop\Model\ShopAttributeGroupsTable;
use Shop\Form\ShopAttributeGroupsForm;
use Shop\Model\ShopAttributeGroupsCategoryIndexTable;
use Shop\Model\ShopProductAttributesTable;
use Shop\Form\ShopAttributesForm;
use Shop\Form\ShopProductAttributeOptionsForm;
use Shop\Model\ShopProductAttributeOptionsTable;
use Shop\Model\ShopAttributeGroupIndexTable;
use Shop\Model\ShopProductsTable;
use Shop\Form\ShopProductsForm;
use Shop\Model\ShopProductAttributeInfo;
use Application\Model\ApplicationFileManagerTable;
use Application\Helper\BaseAdminController;
use Zend\Db\Adapter\Adapter;
use ZfcDatagrid\Column;
use ZfcDatagrid\Column\Style;
use Shop\Model\ShopAttributeIndexTable;
use ZfcDatagrid\Filter;
use ZfcDatagrid\Column\Type;
use Shop\Model\UsersTable;
use Shop\Model\EducationFieldofstudyTable;
use Shop\Model\EducationLessonsTable;
use Shop\Model\PaymentsTable;
use Shop\Model\OrdersTable;
use Shop\Model\OrdersDetailsTable;
use Shop\Form\ShopOrderStatusesForm;
use Language\Model\LanguageLanguagesTable;
use Shop\Model\GalleriesTable;
use Shop\Model\ShopProductVideoFilesTable;
use Shop\Form\ShopReportForm;
use Zend\Db\Sql\Where;
use Application\Helper\Jdates;
use Symfony\Component\Validator\Constraints\Issn;
use Shop\Form\ProductsForm;
use Shop\Model\ProductsTable;
use Shop\Model\CategoriesTable;
use Shop\Form\CategoriesForm;
use Shop\Model\ProductsOrdersTable;
use Printservices\Model\OrderTable;
use Printservices\Model\OrderIndexTable;
use Shop\Model\ProductsOrderTable;
use Shop\Form\DesignerSampleForm;
use Shop\Model\DesignerSampleTable;
class AdminController extends BaseAdminController
{

    public function addProductAction()
    {
        $catId = $this->params('id');
        $view["catId"] = $catId;
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
    
        // attributes start
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $shopProductAttributeInfoTable = new ShopProductAttributeInfo($this->getServiceLocator());
        $applicationFileManagerTable = new ApplicationFileManagerTable($this->getServiceLocator());
    
        $productInfoByCategoryId = $shopCategoriesTable->fetchAll(null, $catId);
        // *****
        $instructorList = array();
        $studiesList = array();
        $lessonsList = array();
        $productType = "Real";
        $view['type'] = $productType;
        $productLang = $this->lang;
        if (isset($_GET['lang'])) {
            $productLang = $_GET['lang'];
        }
    
        if (isset($productInfoByCategoryId[0]["type"]) && $productInfoByCategoryId[0]["type"] == "Virtual") {
            $usersTable = new UsersTable($this->getServiceLocator());
            $instructorList = $usersTable->getAllInstructors();
    
            // start
            $educationFieldStudyTable = new EducationFieldofstudyTable($this->getServiceLocator());
            $studiesList = $educationFieldStudyTable->getRecords(array(
                "lang" => $productLang
            ));
    
            $parentIds = array();
            foreach ($studiesList as $studyList) {
                $parentIds[] = $studyList["id"];
            }
            $educationLessonsTable = new EducationLessonsTable($this->getServiceLocator());
            $lessonsList = $educationLessonsTable->getRecordsByParent($parentIds);
    
            // end
            $productType = "Virtual";
            $view['type'] = $productType;
        }
        $productsTable = new ShopProductsTable($this->getServiceLocator());
        $allProducts = $productsTable->fetchAll(false, true);
        $relatedProducts = array();
        foreach ($allProducts as $product) {
            $relatedProducts[$product['id']] = $product['name'];
        }
    
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
    
        $galleriesTable = new GalleriesTable($this->getServiceLocator());
        $galleries = $galleriesTable->getAllGallery();
    
        $shopProductsForm = new ShopProductsForm($allActiveLanguages, $catId, $productType, $instructorList, $studiesList, $lessonsList, $relatedProducts, $galleries);
        $shopProductsForm->populateValues(array(
            "product_lang" => $productLang
        ));
        $view['shopProductsForm'] = $shopProductsForm;
        // *****
        $allAttributeIds = array();
        if (isset($productInfoByCategoryId[0]['attribute_groups'])) {
            $attributeGroupsIds = $productInfoByCategoryId[0]['attribute_groups'];
            $attributeGroupsIds = explode(',', $attributeGroupsIds);
            if ($attributeGroupsIds) {
                $shopAttributeGroupIndex = new ShopAttributeGroupIndexTable($this->getServiceLocator());
                foreach ($attributeGroupsIds as $piece) {
                    $allAttributeIds[] = $shopAttributeGroupIndex->fetch($piece);
                }
            }
        }
        $allAttributes = array();
        if ($allAttributeIds) {
            $shopProductAttributes = new ShopProductAttributesTable($this->getServiceLocator());
            foreach ($allAttributeIds as $key => $attributeId) {
                foreach ($attributeId as $innerKey => $id) {
                    $allAttributes[] = $shopProductAttributes->fetchAll($attributeId[$innerKey]['attribute_id']);
                }
            }
        }
    
        $allFormFields = array();
  
        if ($allAttributes) {
            $shopProductAttributeOptions = new ShopProductAttributeOptionsTable($this->getServiceLocator());
            foreach ($allAttributes as $key => $singleAttribute) {
                $temp = $shopProductAttributeOptions->fetchAttributeValues($singleAttribute[0]['id']);
    
                $temp[0]['field_name'] = strtolower($singleAttribute[0]['developer_name']);
                $temp[0]['kind'] = strtolower($singleAttribute[0]['kind']);
                $temp[0]['name'] = strtolower($singleAttribute[0]['title']);
                $temp[0]['mandatory'] = strtolower($singleAttribute[0]['is_forced']);
                if ($temp)
                    $allFormFields[] = $temp;
            }
        } // die;
    
        $form = $this->formGenerator()->generateForm($allFormFields, 'completeProductInfo', '', false, '', 'POST', '', false, false, false);
    
        $view['form'] = $form;
        // attribute end
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            if (isset($data["related_products"])) {
                $data["related_products"] = implode(",", $data["related_products"]);
            }
            $files = $request->getFiles()->toArray();
            $data = array_merge($data, $files);
   
            $shopProductsForm->setData($data);
            if ($shopProductsForm->isValid()) {
                $validData = $shopProductsForm->getData();
                $validData['gallery_id'] = (isset($data['gallery_id']) ? $data['gallery_id'] : 0);
                $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
                $checkExist = $shopProductsTable->isExist($validData);
                if ($checkExist) {
                    $this->layout()->errorMessage = __("Duplicated Product Name !");
                } else {
                    $con = $shopProductsTable->adapter->getDriver()->getConnection();
                    $con->beginTransaction();
                    try {
                        $validData['image'] = false;
                        if (isset($files["image"])) {
                            $config = $this->serviceLocator->get('Config');
                            $uploadDir = $config['base_route'] . "/uploads/shop/product";
                            $validationExt = "jpg,jpeg,png,gif,pdf";
                            $newFileName = md5(time() . $files["image"]['name']);
                            $isUploaded = $this->imageUploader()->UploadFile($files["image"], $validationExt, $uploadDir, $newFileName);
                            if ($isUploaded[0]) {
                                $validData['image'] = $isUploaded[1];
                            }
                        }
                        $validData['file'] = null;
                        if (isset($files["file"])) {
                            $config = $this->serviceLocator->get('Config');
                            $uploadDir = $config['base_route'] . "/uploads/shop/product";
                            $validationExt = "";
                            $newFileName = md5(time() . $files["file"]['name']);
                            $isUploaded = $this->imageUploader()->UploadFile($files["file"], $validationExt, $uploadDir, $newFileName);
                            if ($isUploaded[0]) {
                                $validData['file'] = $isUploaded[1];
                            }
                        }
                        if (isset($validData['instructor_id'])) {
                            $validData["downloadable"] = ($validData["price"] == 0 ? 1 : 0);
                        } else {
                            $validData["downloadable"] = 0;
                        }
 ;
                        $productId = $shopProductsTable->addProduct($validData);
                        if ($productId) {
                            $validData = $request->getPost();
                            unset($validData['csrf']);
                            unset($validData['submit']);
                            $errors = $this->formGenerator()->isValid($allAttributes, false, $validData, $files);
    
                            if (count($errors) > 0) {
                                $this->layout()->errorMessage = "";
                                foreach ($errors as $error) {
                                    $this->layout()->errorMessage .= 'Please fill in the ' . $error[0] . ' field' . "<br>";
                                }
                            } else {
                                foreach ($allFormFields as $field) {
                                    foreach ($validData as $name => $validDatum) {
                                        if (isset($field[0]["field_name"]) && isset($field[0]["index"]) && $field[0]["index"] != "public_file_options" && $field[0]["index"] != "image_file_options" && $name == $field[0]["field_name"]) {
                                            $shopProductAttributeInfoTable->add($productId, $name, $validDatum);
                                        }
                                    }
                                }
                                $fileUploadInfo = $this->formGenerator()->uploadFiles(false, $files);
    
                                if (count($fileUploadInfo) > 0) {
                                    foreach ($fileUploadInfo as $name => $info) {
                                        $type = $this->formGenerator()->getFieldType($info[0], $allAttributes);
                                        $fileId = $applicationFileManagerTable->add($this->userData->id, $productId, $info[0], $info[1], '', $type, $info[2]);
                                        $shopProductAttributeInfoTable->add($productId, $info[0], $fileId);
                                    }
                                }
                                $con->commit();
                                if (isset($data['submitreturn'])) {
                                    $this->layout()->successMessage = __("Operation done successfully.");
                                } else {
                                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/products/' . $catId);
                                }
                            }
                            $con->rollback();
                        }
                    } catch (\Exception $e) {
                        $con->rollback();
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                }
            } else {
                $s = $shopProductsForm->getMessages();
                // var_dump($s);die;
    
                $this->layout()->errorMessage = __("Please fill the requiered filed!");
            }
        }
        return new ViewModel($view);
    }
    
    public function editProductAction()
    {
        $productId = $this->params('id', - 1);
        $shopProductTable = new ShopProductsTable($this->getServiceLocator());
        $catId = $shopProductTable->getCategoryId($productId);
        if (! $catId) {
            $this->flashMessenger()->addErrorMessage(__("Operation failed."));
            return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop');
        } else {
            $catId = $catId[0]["category_id"];
        }
        $view["catId"] = $catId;
        $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
        $productById = $shopProductsTable->fetchAll($productId);
    
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $productInfoByCategoryId = $shopCategoriesTable->fetchAll(null, $catId);
        // *****
        $instructorList = array();
        $studiesList = array();
        $lessonsList = array();
        $productType = "Real";
        $view['type'] = $productType;
        $view['catId'] = $catId;
        $productLang = $this->lang;
        if (isset($_GET['lang'])) {
            $productLang = $_GET['lang'];
        }
        if (isset($productInfoByCategoryId[0]["type"]) && $productInfoByCategoryId[0]["type"] == "Virtual") {
            $usersTable = new UsersTable($this->getServiceLocator());
            $instructorList = $usersTable->getAllInstructors();
    
            $educationFieldStudyTable = new EducationFieldofstudyTable($this->getServiceLocator());
            $studiesList = $educationFieldStudyTable->getRecords(array(
                "lang" => $productLang
            ));
    
            $parentIds = array();
            foreach ($studiesList as $studyList) {
                $parentIds[] = $studyList["id"];
            }
            $educationLessonsTable = new EducationLessonsTable($this->getServiceLocator());
            $lessonsList = $educationLessonsTable->getRecords($parentIds);
    
            $productType = "Virtual";
            $view['type'] = $productType;
        }
    
        $productsTable = new ShopProductsTable($this->getServiceLocator());
        $allProducts = $productsTable->fetchAll(false, true);
        // var_dump($allProducts); die;
        if ($allProducts) {
            foreach ($allProducts as $product) {
                $relatedProducts[$product['id']] = $product['name'];
            }
        } else {
            $relatedProducts = array();
        }
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
    
        $galleriesTable = new GalleriesTable($this->getServiceLocator());
        $galleries = $galleriesTable->getAllGallery();
    
        $shopProductsForm = new ShopProductsForm($allActiveLanguages, $catId, $productType, $instructorList, $studiesList, $lessonsList, $relatedProducts, $galleries, true);
        $productById[0]['related_products'] = explode(',', $productById[0]['related_products']);
        $productById[0]["product_lang"] = $productLang;
        $shopProductsForm->populateValues($productById[0]);
        $view['product'] = $productById[0];
        $view['shopProductsForm'] = $shopProductsForm;
    
        // attributes start
        $applicationFileManagerTable = new ApplicationFileManagerTable($this->getServiceLocator());
        $currentFiles = $applicationFileManagerTable->fetch($productId);
    
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $shopProductAttributeInfoTable = new ShopProductAttributeInfo($this->getServiceLocator());
        $applicationFileManagerTable = new ApplicationFileManagerTable($this->getServiceLocator());
        $currentInfo = $shopProductAttributeInfoTable->fetch($productId);
        $currentInfo['file_pack'] = $currentFiles;
        $productInfoByCategoryId = $shopCategoriesTable->fetchAll(null, $catId);
        $attributeGroupsIds = $productInfoByCategoryId[0]['attribute_groups'];
        $attributeGroupsIds = explode(',', $attributeGroupsIds);
    
        $allAttributeIds = array();
        if ($attributeGroupsIds) {
            $shopAttributeGroupIndex = new ShopAttributeGroupIndexTable($this->getServiceLocator());
            foreach ($attributeGroupsIds as $piece) {
                $allAttributeIds[] = $shopAttributeGroupIndex->fetch($piece);
            }
        }
        $allAttributes = array();
        if ($allAttributeIds) {
            $shopProductAttributes = new ShopProductAttributesTable($this->getServiceLocator());
            foreach ($allAttributeIds as $key => $attributeId) {
                foreach ($attributeId as $innerKey => $id) {
                    $allAttributes[] = $shopProductAttributes->fetchAll($attributeId[$innerKey]['attribute_id']);
                }
            }
        }
        $allFormFields = array();
        if ($allAttributes) {
            $shopProductAttributeOptions = new ShopProductAttributeOptionsTable($this->getServiceLocator());
            foreach ($allAttributes as $key => $singleAttribute) {
                $temp = $shopProductAttributeOptions->fetchAttributeValues($singleAttribute[0]['id']);
                $temp[0]['field_name'] = strtolower($singleAttribute[0]['developer_name']);
                $temp[0]['name'] = strtolower($singleAttribute[0]['title']);
                $temp[0]['mandatory'] = strtolower($singleAttribute[0]['is_forced']);
    
                $allFormFields[] = $temp;
            }
        } // die;
         
        $form = $this->formGenerator()->generateForm(@$allFormFields, 'completeProductInfo', '', $currentInfo, '', 'POST', '', false, false, false);
    
        $view['form'] = $form;
    
        // attribute end
    
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            if (isset($data["related_products"])) {
                $data["related_products"] = implode(",", $data["related_products"]);
            }
            $files = $request->getFiles()->toArray();
            $data = array_merge($data, $files);
            $shopProductsForm->setData($data);
            if ($shopProductsForm->isValid()) {
                $validData = $shopProductsForm->getData();
                $checkExist = $shopProductsTable->isExist($validData, $productId);
            
                $validData['gallery_id'] = $data['gallery_id'];
                if ($checkExist) {
                    $this->layout()->errorMessage = __("Duplicated Seo name!");
                } else {
                    $con = $shopProductsTable->adapter->getDriver()->getConnection();
                    $con->beginTransaction();
                    try {
                        $validData["image"] = false;
                        if (isset($files["image"]) && $files["image"]['name'] != '') {
                            $config = $this->serviceLocator->get('Config');
                            $uploadDir = $config['base_route'] . "/uploads/shop/product";
                            $validationExt = "jpg,jpeg,png,gif,pdf";
                            $newFileName = md5(time() . $files["image"]['name']);
    
                            if (is_uploaded_file($files["image"]['tmp_name'])) {
    
                                $isUploaded = $this->imageUploader()->UploadFile($files["image"], $validationExt, $uploadDir, $newFileName);
                                if (is_file($uploadDir . $productById[0]["image"]))
                                    @unlink($uploadDir . $productById[0]["image"]);
                                $validData["image"] = "deleted";
                                if ($isUploaded[0]) {
                                    $validData["image"] = $isUploaded[1];
                                }
                            }
                        }
    
                        $validData["file"] = false;
                        if (isset($files["file"]) && $files["file"]['name'] != '') {
                            $config = $this->serviceLocator->get('Config');
                            $uploadDir = $config['base_route'] . "/uploads/shop/product";
                            $validationExt = "";
                            $newFileName = md5(time() . $files["file"]['name']);
    
                            if (is_uploaded_file($files["file"]['tmp_name'])) {
                                $isUploaded = $this->imageUploader()->UploadFile($files["file"], $validationExt, $uploadDir, $newFileName);
                                if (is_file($uploadDir . $productById[0]["file"]))
                                    @unlink($uploadDir . $productById[0]["file"]);
                                $validData["file"] = "deleted";
                                if ($isUploaded[0]) {
                                    $validData["file"] = $isUploaded[1];
                                }
                            }
                        }
                        if (isset($validData['instructor_id'])) {
                            $validData["downloadable"] = ($validData["price"] == 0 ? 1 : 0);
                        } else {
                            $validData["downloadable"] = 0;
                        }
                      
                        $isUpdated = $shopProductsTable->updateProduct($validData, $productId);
    
                        if ($isUpdated !== false) {
                            $validData = $request->getPost()->toArray();
                            unset($validData['csrf']);
                            unset($validData['submit']);
                            $errors = $this->formGenerator()->isValid($allAttributes, $currentInfo, $validData);
    
                            if (count($errors) > 0) {
                                $this->layout()->errorMessage = "";
                                foreach ($errors as $error) {
                                    $this->layout()->errorMessage .= 'Please fill in the ' . $error[0] . ' field' . "<br>";
                                }
                            } else {
                                $shopProductAttributeInfoTable->delete($productId);
    
                                if (isset($allFormFields)) {
                                    foreach ($allFormFields as $field) {
                                        foreach ($validData as $name => $validDatum) {
                                            if (isset($field[0]["field_name"]) && isset($field[0]["index"]) && $field[0]["index"] != "public_file_options" && $field[0]["index"] != "image_file_options" && $name == $field[0]["field_name"]) {
                                                $shopProductAttributeInfoTable->add($productId, $name, $validDatum);
                                            }
                                        }
                                    }
                                }
                                // foreach ($validData as $name => $validDatum) {
                                // $shopProductAttributeInfoTable->add($productId, $name, $validDatum);
                                // }
    
                                $fileUploadInfo = $this->formGenerator()->uploadFiles($currentInfo, $files);
                                if (count($fileUploadInfo) > 0) {
                                    // Record file info in DB
                                    foreach ($fileUploadInfo as $name => $info) {
                                        $isExist = $applicationFileManagerTable->checkExist($productId, $info[0]);
                                        if ($isExist) {
                                            $isDeleted = $applicationFileManagerTable->delete($isExist[0]['id']);
                                            if ($isDeleted) {
                                                $fileToDelete = $_SERVER['DOCUMENT_ROOT'] . $isExist[0]['folder'] . $isExist[0]['entity_type'] . "/" . $isExist[0]['name'];
                                                @unlink($fileToDelete);
                                            }
                                        }
                                        $type = $this->formGenerator()->getFieldType($info[0], $allAttributes);
                                        $fileId = $applicationFileManagerTable->add($this->userData->id, $productId, $info[0], $info[1], '', $type, $info[2]);
                                        $shopProductAttributeInfoTable->add($productId, $info[0], $fileId);
                                    }
                                }
                                $con->commit();
                                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                                return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/products/' . $catId);
                            }
                            $con->rollback();
                        } else {
                            $this->flashMessenger()->addInfoMessage(__('Operation failed!'));
                        }
                    } catch (\Exception $e) {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                }
            } else {
                $this->layout()->errorMessage = __("Please fill the requiered field!");
            }
        }
        return new ViewModel($view);
    }
    
    public function instructorSellsReportAction()
    {}

    public function sellsReportAction()
    {}

    public function showProductVideosAction()
    {}

    public function downloadProductFilesAction()
    {}

    public function listUserFilesVideosAction()
    {}

    public function editOrderAction()
    {}
    // User payments and orders start
    public function orderUserDetailsAction()
    {}

    public function listUserOrdersAction()
    {}

    public function listUserPaymentsAction()
    {}
    // Admin payments and orders start
    public function orderDetailsAction()
    {}

    public function orderCustomerDetailsAction()
    {}

    public function listOrdersAction()
    {}

    public function listPaymentsAction()
    {}
    
    // categories
    public function categoriesAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $categories = $shopCategoriesTable->fetchAllList();
        $grid->setTitle(__('Product categories list'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($categories, $dbAdapter);
        
        $parent_id = new Column\Select('parent_id');
        $parent_id->setLabel(__('Title'));
        $parent_id->setHidden(true);
        $grid->addColumn($parent_id);
        
        $col = new Column\Select('id', "shop_categories");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $style = new Style\BackgroundColor(Style\BackgroundColor::$CHILD);
        $style->addByValue($parent_id, 0, Filter::EQUAL);
        $col->addStyle($style);
        $style = new Style\Align(Style\Align::$CENTER);
        $style->addByValue($parent_id, 0, Filter::NOT_EQUAL);
        $col->addStyle($style);
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Title'));
        $col->setWidth(20);
        $style = new Style\BackgroundColor(Style\BackgroundColor::$CHILD);
        $style->addByValue($parent_id, 0, Filter::EQUAL);
        $col->addStyle($style);
        $style = new Style\Align(Style\Align::$CENTER);
        $style->addByValue($parent_id, 0, Filter::NOT_EQUAL);
        $col->addStyle($style);
        $grid->addColumn($col);
        
        $catLang = new Column\Select('cat_lang');
        $catLang->setLabel(__('Language'));
        $grid->addColumn($catLang);
        
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $options = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        $style = new Style\BackgroundColor(Style\BackgroundColor::$CHILD);
        $style->addByValue($parent_id, 0, Filter::EQUAL);
        $col->addStyle($style);
        
        $style = new Style\Align(Style\Align::$CENTER);
        $style->addByValue($parent_id, 0, Filter::NOT_EQUAL);
        $col->addStyle($style);
        
        $col = new Column\Select('type');
        $col->setLabel(__('Products Type'));
        $options = array(
            'Real' => 'Real',
            'Virtual' => 'Virtual'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            'Real' => 'Real',
            'Virtual' => 'Virtual'
        );
        $col->setReplaceValues($replaces, $col);
        $style = new Style\BackgroundColor(Style\BackgroundColor::$CHILD);
        $style->addByValue($parent_id, 0, Filter::EQUAL);
        $col->addStyle($style);
        $style = new Style\Align(Style\Align::$LEFT);
        $style->addByValue($parent_id, 0, Filter::NOT_EQUAL);
        $col->addStyle($style);
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__(''));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-plus');
        $viewAction->setTooltipTitle('Add Product to category');
        $viewAction->setLink("/" . $this->lang . '/admin-shop/add-product/' . $rowId);
        $viewAction->addShowOnValue($parent_id, 0, Filter::NOT_EQUAL);
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setTooltipTitle(__('Edit'));
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/edit-category/' . $rowId);
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setTooltipTitle(__('Delete'));
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/delete-category/' . $rowId . '/token/' . $csrf);
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-list');
        $viewAction3->setTooltipTitle(__('Show products in category'));
        $viewAction3->setLink("/" . $this->lang . '/admin-shop/products/' . $rowId);
        $viewAction3->addShowOnValue($parent_id, 0, Filter::NOT_EQUAL);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->addAction($viewAction3);
        $actions2->addAction($viewAction);
        $actions2->setWidth(15);
        $style = new Style\BackgroundColor(Style\BackgroundColor::$CHILD);
        $style->addByValue($parent_id, 0, Filter::EQUAL);
        $actions2->addStyle($style);
        $style = new Style\Align(Style\Align::$CENTER);
        $style->addByValue($parent_id, 0, Filter::NOT_EQUAL);
        $col->addStyle($style);
        $grid->addColumn($actions2);
        
        $link[] = '<a href="/' . $this->lang . '/admin-shop/add-category" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        // $link[] = '<a href="/'. $this->lang .'/admin-shop/attributes" class="btn btn-primary">'.__("Attributes management").'</a>';
        $grid->setLink($link);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function categoriesChildsAction()
    {
        $parentId = $this->params('id', - 1);
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $categories = $shopCategoriesTable->fetchAllChildsList($parentId);
        $grid->setTitle(__('Shop Categories List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($categories, $dbAdapter);
        
        $col = new Column\Select('id', "shop_categories");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Title'));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $col->setWidth(10);
        $options = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $col = new Column\Select('type');
        $col->setLabel(__('Products Type'));
        $col->setWidth(10);
        $options = array(
            'Real' => 'Real',
            'Virtual' => 'Virtual'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            'Real' => 'Real',
            'Virtual' => 'Virtual'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__(''));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-plus');
        $viewAction->setTooltipTitle('Add Product to category');
        $viewAction->setLink("/" . $this->lang . '/admin-shop/add-product/' . $rowId);
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setTooltipTitle('Edit');
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/edit-category/' . $rowId);
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setTooltipTitle('Remove');
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/delete-category/' . $rowId . '/token/' . $csrf);
        $viewAction2->addShowOnValue($col, "0", Filter::NOT_EQUAL);
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-list');
        $viewAction3->setTooltipTitle('Show products in category');
        $viewAction3->setLink("/" . $this->lang . '/admin-shop/products/' . $rowId);
        $viewAction3->addShowOnValue($col, "0", Filter::NOT_EQUAL);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->addAction($viewAction3);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href="/' . $this->lang . '/admin-shop/add-category" class="btn btn-primary">' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/admin-shop/attribute-groups" class="btn btn-primary">' . __("Attributes group management") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addCategoryAction()
    {
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/shop/category/";
        $validationExt = "jpg,jpeg,png,gif";
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $allParentCategories = $shopCategoriesTable->fetchAll(true);
        
        $shopAttributeGroupsTable = new ShopAttributeGroupsTable($this->getServiceLocator());
        $attributeGroups = $shopAttributeGroupsTable->fetchAttributeGroups(null, true);
        
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        
        $shopCategoriesForm = new ShopCategoriesForm($allActiveLanguages, $allParentCategories, $attributeGroups);
        $view['shopCategoriesForm'] = $shopCategoriesForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            $data = array_merge($data, $files);
            $shopCategoriesForm->setData($data);
            if ($shopCategoriesForm->isValid()) {
                $validData = $shopCategoriesForm->getData();
                $fileName = array();
                $checkExist = $shopCategoriesTable->isExist($validData);
                if ($checkExist) {
                    $this->layout()->errorMessage = __("Duplicated name!");
                } else {
                    // try {
                    foreach ($files as $k => $file) {
                        $newFileName = md5(time() . $file['name']);
                        $isUploaded = $this->imageUploader()->UploadFile($file, $validationExt, $uploadDir, $newFileName);
                        if ($isUploaded[0]) {
                            $fileName[$k] = $isUploaded[1];
                        }
                    }
                    if ($fileName) {
                        foreach ($fileName as $k => $v) {
                            $validData[$k] = $v;
                        }
                    }
                    $validData['type'] = ($validData['parent_id'] == 0 ? null : $validData['type']);
                    $isAdded = $shopCategoriesTable->addCategory($validData);
                    
                    if ($isAdded) {
                        if (isset($data['attribute_groups']) && $data['attribute_groups']) {
                            $validData['attribute_groups'] = $data['attribute_groups'];
                            if ($validData['attribute_groups'] && $validData['parent_id'] != 0) {
                                $shopAttributeGroupsCategoryIndexTable = new ShopAttributeGroupsCategoryIndexTable($this->getServiceLocator());
                                foreach ($validData['attribute_groups'] as $value) {
                                    $shopAttributeGroupsCategoryIndexTable->add($value, $isAdded);
                                }
                            }
                        }
                        if (isset($data['submitreturn'])) {
                            $this->layout()->successMessage = __("Operation done successfully.");
                        } else {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('admin-shop', array(
                                'action' => 'categories'
                            ));
                        }
                    }
                    $this->layout()->errorMessage = __("Operation failed!");
                    // }
                }
            } else {
                $this->layout()->errorMessage = __("Please fill the requiered filed!");
            }
        }
        return new ViewModel($view);
    }

    public function editCategoryAction()
    {
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/shop/category/";
        $validationExt = "jpg,jpeg,png,gif";
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $allParentCategories = $shopCategoriesTable->fetchAll(true);
        $shopAttributeGroupsTable = new ShopAttributeGroupsTable($this->getServiceLocator());
        $attributeGroups = $shopAttributeGroupsTable->fetchAttributeGroups(null, true);
        $languageLanguageLanguagesTable = new LanguageLanguagesTable($this->getServiceLocator());
        $allActiveLanguages = $languageLanguageLanguagesTable->getAllLanguages(array(
            "enable" => "1"
        ));
        $shopCategoriesForm = new ShopCategoriesForm($allActiveLanguages, $allParentCategories, $attributeGroups);
        $categoryId = $this->params('id', - 1);
        $cagegoryById = $shopCategoriesTable->fetchAll(null, $categoryId);
        $cagegoryById[0]['attribute_groups'] = explode(',', $cagegoryById[0]['attribute_groups']);
        $shopCategoriesForm->populateValues($cagegoryById[0]);
        $view['cagegory'] = $cagegoryById[0];
        $view['shopCategoriesForm'] = $shopCategoriesForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $files = $request->getFiles()->toArray();
            $data = array_merge($data, $files);
            $shopCategoriesForm->setData($data);
            if ($shopCategoriesForm->isValid()) {
                $validData = $shopCategoriesForm->getData();
                $checkExist = $shopCategoriesTable->isExist($validData, $categoryId);
                if ($checkExist) {
                    $this->layout()->errorMessage = __("Duplicated name!");
                } else {
                    try {
                        
                        $item = array();
                        $fileName = "";
                        foreach ($files as $k => $file) {
                            $newFileName = md5(time() . $file['name']);
                            if (is_uploaded_file($file['tmp_name'])) {
                                
                                $isUploaded = $this->imageUploader()->UploadFile($file, $validationExt, $uploadDir, $newFileName);
                                
                                if (is_file($uploadDir . $cagegoryById[0][$k]))
                                    unlink($uploadDir . $cagegoryById[0][$k]);
                                
                                if ($isUploaded[0]) {
                                    $fileName[$k] = $isUploaded[1];
                                }
                            }
                        }
                        
                        if ($fileName) {
                            foreach ($fileName as $k => $v) {
                                $validData[$k] = $v;
                            }
                        } else {
                            unset($validData["icon"]);
                        }
                        
                        $validData['type'] = ($validData['parent_id'] == 0 ? null : $validData['type']);
                        
                        $isUpdated = $shopCategoriesTable->updateCategory($validData, $categoryId);
                        
                        $shopAttributeGroupsCategoryIndexTable = new ShopAttributeGroupsCategoryIndexTable($this->getServiceLocator());
                        $shopAttributeGroupsCategoryIndexTable->delete($categoryId);
                        if (isset($data['attribute_groups']) && $validData['parent_id'] != 0) {
                            $validData['attribute_groups'] = $data['attribute_groups'];
                            foreach ($validData['attribute_groups'] as $value) {
                                $shopAttributeGroupsCategoryIndexTable->add($value, $categoryId);
                            }
                        }
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toRoute('admin-shop', array(
                            'action' => 'categories'
                        ));
                    } catch (\Exception $e) {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                }
            } else {
                // var_dump($shopCategoriesForm->getMessages()); die;
                $this->layout()->errorMessage = __("Please fill the requiered filed!");
            }
        }
        return new ViewModel($view);
    }

    public function deleteCategoryAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $categoryId = $this->params('id', - 1);
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $hasChild = $shopCategoriesTable->getChildren($categoryId);
        
        $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
        $isAssignedToProduct = $shopProductsTable->hasCategory($categoryId);
        
        if ($categoryId && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                
                if (count($isAssignedToProduct) > 0) {
                    $this->flashMessenger()->addErrorMessage(__("Some products assigned to this category, Please delete them first"));
                    return $this->redirect()->toRoute('admin-shop', array(
                        'action' => 'categories'
                    ));
                }
                
                if ($hasChild) {
                    $this->flashMessenger()->addErrorMessage(__("This category has children, Please delete them first."));
                    return $this->redirect()->toRoute('admin-shop', array(
                        'action' => 'categories'
                    ));
                } else {
                    $shopAttributeGroupsCategoryIndexTable = new ShopAttributeGroupsCategoryIndexTable($this->getServiceLocator());
                    $shopAttributeGroupsCategoryIndexTable->delete($categoryId);
                    $shopCategoriesTable->deleteCategory($categoryId);
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                }
                return $this->redirect()->toRoute('admin-shop', array(
                    'action' => 'categories'
                ));
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
                return $this->redirect()->toRoute('admin-shop', array(
                    'action' => 'categories'
                ));
            }
        }
        return $this->redirect()->toRoute('admin-shop', array(
            'action' => 'categories'
        ));
    }
    
    // attribute groups
    public function attributeGroupsAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $shopAttributeGroupsTable = new ShopAttributeGroupsTable($this->getServiceLocator());
        $attributeGroups = $shopAttributeGroupsTable->fetchAttributeGroupsList(null, null);
        
        $grid->setTitle(__('Attribute groups List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($attributeGroups, $dbAdapter);
        
        $col = new Column\Select('id', "shop_attribute_groups");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(__('Title'));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $col->setWidth(10);
        $options = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__(''));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-plus');
        $viewAction->setTooltipTitle(__('Add attributes to group'));
        $viewAction->setLink("/" . $this->lang . '/admin-shop/add-attribute/' . $rowId);
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setTooltipTitle(__('Edit'));
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/edit-attribute-group/' . $rowId);
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setTooltipTitle(__('Delete'));
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/delete-attribute-group/' . $rowId . '/token/' . $csrf);
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-list');
        $viewAction3->setTooltipTitle(__('Show attributes'));
        $viewAction3->setLink("/" . $this->lang . '/admin-shop/attributes/' . $rowId);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->addAction($viewAction3);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href="/' . $this->lang . '/admin-shop/add-attribute-group" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        // $link[] = '<a href="/'. $this->lang .'/admin-shop" class="btn btn-primary">'.__("Product category list").'</a>';
        $grid->setLink($link);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addAttributeGroupAction()
    {
        $shopAttributeGroupsForm = new ShopAttributeGroupsForm();
        $view['shopAttributeGroupsForm'] = $shopAttributeGroupsForm;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $shopAttributeGroupsForm->setData($data);
            if ($shopAttributeGroupsForm->isValid()) {
                $validData = $shopAttributeGroupsForm->getData();
                try {
                    $shopAttributeGroupsTable = new ShopAttributeGroupsTable($this->getServiceLocator());
                    $title = trim($validData['title']);
                    $checkExists = $shopAttributeGroupsTable->isExist($title, null);
                    if ($checkExists) {
                        $this->layout()->errorMessage = __("Duplicated group name!");
                    } else {
                        $shopAttributeGroupsTable->add($validData);
                        if (isset($_POST['submit'])) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('admin-shop', array(
                                'action' => 'attribute-groups'
                            ));
                        } else {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                            return $this->redirect()->toRoute('admin-shop', array(
                                'action' => 'add-attribute-group'
                            ));
                        }
                    }
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            } else {
                $this->layout()->errorMessage = __("Please fill the requiered filed!");
            }
        }
        return new ViewModel($view);
    }

    public function editAttributeGroupAction()
    {
        $shopAttributeGroupsForm = new ShopAttributeGroupsForm();
        $shopAttributeGroupsTable = new ShopAttributeGroupsTable($this->getServiceLocator());
        $view['shopAttributeGroupsForm'] = $shopAttributeGroupsForm;
        $attributeGroupId = $this->params('id', - 1);
        $attributeGroupById = $shopAttributeGroupsTable->fetchAttributeGroups($attributeGroupId, null);
        $shopAttributeGroupsForm->populateValues($attributeGroupById['0']);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $shopAttributeGroupsForm->setData($data);
            if ($shopAttributeGroupsForm->isValid()) {
                $validData = $shopAttributeGroupsForm->getData();
                $title = $validData['title'];
                try {
                    $checkExists = $shopAttributeGroupsTable->isExist($title, $attributeGroupId);
                    if ($checkExists) {
                        $this->layout()->errorMessage = __("Duplicated group name!");
                    } else {
                        $isUpdated = $shopAttributeGroupsTable->update($validData, $attributeGroupId);
                        if ($isUpdated === 1) {
                            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        } else {
                            $this->flashMessenger()->addInfoMessage(__('No changes happened'));
                        }
                        return $this->redirect()->toRoute('admin-shop', array(
                            'action' => 'attribute-groups'
                        ));
                    }
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
            } else {
                $this->layout()->errorMessage = __("Please fill the requiered filed!");
            }
        }
        return new ViewModel($view);
    }

    public function deleteAttributeGroupAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $attributeGroupId = $this->params('id', - 1);
        $shopAttributeGroupsTable = new ShopAttributeGroupsTable($this->getServiceLocator());
        $isUsed = $shopAttributeGroupsTable->checkCategoryIsUsed($attributeGroupId);
        
        // daneshi add to check has children
        $shopProductAttributesTable = new ShopProductAttributesTable($this->getServiceLocator());
        $shopProductAttributeOptionsTable = new ShopProductAttributeOptionsTable($this->getServiceLocator());
        
        $shopAttributeGroupIndexTable = new ShopAttributeGroupIndexTable($this->getServiceLocator());
        
        // end
        
        if ($isUsed) {
            $this->flashMessenger()->addErrorMessage(__('Unable to delete. Some product categories used this attribute group!'));
        } else {
            if ($attributeGroupId && $container->offsetExists('csrf')) {
                if ($container->offsetGet('csrf') == $token) {
                    $attributesGroup = $shopAttributeGroupIndexTable->getAttributes($attributeGroupId);
                    if ($attributesGroup) {
                        $this->flashMessenger()->addErrorMessage(__('Please delete group attributes first!'));
                    } else {
                        $shopAttributeGroupsTable->delete($attributeGroupId);
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    }
                } else {
                    $this->layout()->errorMessage = __("Operation failed!");
                }
                return $this->redirect()->toRoute('admin-shop', array(
                    'action' => 'attribute-groups'
                ));
            }
        }
        
        return $this->redirect()->toRoute('admin-shop', array(
            'action' => 'attribute-groups'
        ));
    }
    
    // Attributes
    public function attributesAction()
    {
        $id = $this->params('id');
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $shopProductAttributesTable = new ShopAttributeIndexTable($this->getServiceLocator());
        $productAttributes = $shopProductAttributesTable->fetchByCatId($id);
        $grid->setTitle(__('Attributes List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($productAttributes, $dbAdapter);
        
        $col = new Column\Select('attribute_id', "shop_attribute_group_index");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->setWidth(5);
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(__('Title'));
        $col->setWidth(20);
        $col->setSortDefault(1, 'ASC');
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $col->setWidth(10);
        $options = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__(''));
        $rowId = $btn->getRowIdPlaceholder();
        
        /*
         * $viewAction = new Column\Action\Icon();
         * $viewAction->setIconClass('glyphicon glyphicon-pencil');
         * $viewAction->setTooltipTitle('Assign Value To Attribute');
         * $viewAction->setLink("/". $this->lang .'/admin-shop/assign-values-to-attribute/' . $rowId);
         */
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setTooltipTitle(__('Edit'));
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/edit-attribute/' . $rowId);
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setTooltipTitle(__('Delete'));
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/delete-attribute/' . $rowId . '/group/' . $id . '/token/' . $csrf);
        
        /*
         * $viewAction3 = new Column\Action\Icon();
         * $viewAction3->setIconClass('glyphicon glyphicon-list');
         * $viewAction3->setTooltipTitle(__('Show attributes'));
         * $viewAction3->setLink("/". $this->lang .'/admin-shop/attributes/'.$rowId);
         */
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        // $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        // $actions2->addAction($viewAction3);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href="/' . $this->lang . '/admin-shop/add-attribute/' . $id . '" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        $grid->setLink($link);
        
        $grid->render();
        
        return $grid->getResponse();
    }

    public function addAttributeAction()
    {
        $catId = $this->params('id');
        $shopAttributeGroupsTable = new ShopAttributeGroupsTable($this->getServiceLocator());
        $attributeGroups = $shopAttributeGroupsTable->fetchAttributeGroups(null, true);
        $shopAttributesForm = new ShopAttributesForm($attributeGroups, $catId);
        $shopProductAttributesTable = new ShopProductAttributesTable($this->getServiceLocator());
        $shopProductAttributeOptionsTable = new ShopProductAttributeOptionsTable($this->getServiceLocator());
        $view['shopAttributesForm'] = $shopAttributesForm;
        $view['cat_id'] = $catId;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $shopAttributesForm->setData($data);
            if ($shopAttributesForm->isValid()) {
                $validData = $shopAttributesForm->getData();
                $checkExist = $shopProductAttributesTable->checkExist($validData);
                $duplicate = in_array($validData["developer_name"], array(
                    "name",
                    "seo_name",
                    "image",
                    "galelry_id",
                    "file",
                    "price",
                    "instructor_id",
                    "study_id",
                    "lesson_id",
                    "description",
                    "meta_description",
                    "meta_keywords",
                    "priority",
                    "rating",
                    "downloadable",
                    "status",
                    "related_products",
                    "all_attributes",
                    "all_files"
                ));
                if ($checkExist || $duplicate) {
                    $this->layout()->errorMessage = __("Duplicated attribute name!");
                } else {
                    $attributeId = $shopProductAttributesTable->add($validData);
                    $attributeType = $validData["type"];
                    if ($attributeType == 'radio-button' || $attributeType == 'select') {
                        foreach ($data['value'] as $k => $value) {
                            $temp['index'] = $attributeType . '_options';
                            $temp['attribute_id'] = $attributeId;
                            $temp['value'] = $value;
                            $splitValues[] = $temp;
                        }
                    } else {
                        $temp['index'] = $attributeType . '_options';
                        $temp['attribute_id'] = $attributeId;
                        $temp['value'] = "";
                        $splitValues[] = $temp;
                    }
                    
                    foreach ($splitValues as $item) {
                        $shopProductAttributeOptionsTable->add($item);
                    }
                    
                    if ($validData['attribute_groups']) {
                        $shopAttributeGroupIndexTable = new ShopAttributeGroupIndexTable($this->getServiceLocator());
                        foreach ($validData['attribute_groups'] as $attributeGroupId) {
                            $shopAttributeGroupIndexTable->add($attributeGroupId, $attributeId);
                        }
                    }
                    if (isset($data['submitreturn'])) {
                        $this->layout()->successMessage = __("Operation done successfully.");
                    } else {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/attributes/' . $catId);
                    }
                }
            } else {
                $this->layout()->errorMessage = __("Please fill the requiered filed!");
            }
        }
        return new ViewModel($view);
    }

    public function editAttributeAction()
    {
        $attributeId = $this->params('id', - 1);
        $shopAttributeGroupsTable = new ShopAttributeGroupsTable($this->getServiceLocator());
        $attributeGroups = $shopAttributeGroupsTable->fetchAttributeGroups(null, true);
        $shopAttributesForm = new ShopAttributesForm($attributeGroups);
        $shopProductAttributesTable = new ShopProductAttributesTable($this->getServiceLocator());
        $attributeById = $shopProductAttributesTable->fetchAll($attributeId);
        
        $shopProductAttributeOptionsTable = new ShopProductAttributeOptionsTable($this->getServiceLocator());
        $productAttributeOptions = $shopProductAttributeOptionsTable->fetchAttributeValues($attributeId);
        // var_dump($attributeById[0]); die;
        $view['cat_id'] = $attributeById[0]['attribute_groups'];
        $view['productAttributeOptions'] = $productAttributeOptions;
        
        $view['shopAttributesForm'] = $shopAttributesForm;
        $attributeById[0]['attribute_groups'] = explode(',', $attributeById[0]['attribute_groups']);
        
        $shopAttributesForm->populateValues($attributeById[0]);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $shopAttributesForm->setData($data);
            if ($shopAttributesForm->isValid()) {
                $validData = $shopAttributesForm->getData();
                
                $duplicate = in_array($validData["developer_name"], array(
                    "name",
                    "seo_name",
                    "image",
                    "galelry_id",
                    "file",
                    "price",
                    "instructor_id",
                    "study_id",
                    "lesson_id",
                    "description",
                    "meta_description",
                    "meta_keywords",
                    "priority",
                    "rating",
                    "downloadable",
                    "status",
                    "related_products",
                    "all_attributes",
                    "all_files"
                ));
                if ($duplicate) {
                    $this->layout()->errorMessage = __("Duplicated attribute name!");
                }
                try {
                    $isUpdated = $shopProductAttributesTable->update($validData, $attributeId);
                } catch (\Exception $e) {
                    $this->layout()->errorMessage = __("Duplicated attribute name!");
                }
                
                // ******************
                
                $shopProductAttributeOptionsTable->delete($attributeId);
                $attributeType = $validData['type'];
                
                if ($attributeType == 'radio-button' || $attributeType == 'select') {
                    foreach ($data['value'] as $k => $value) {
                        $temp['index'] = $attributeType . '_options';
                        $temp['attribute_id'] = $attributeId;
                        $temp['value'] = $data['value'][$k];
                        if ($temp['value'] != '')
                            $splitValues[] = $temp;
                    }
                } else {
                    $temp['index'] = $attributeType . '_options';
                    $temp['attribute_id'] = $attributeId;
                    $temp['value'] = "";
                    $splitValues[] = $temp;
                }
                foreach ($splitValues as $item) {
                    $shopProductAttributeOptionsTable->add($item);
                }
                // ******************
                
                if ($validData['attribute_groups']) {
                    $shopAttributeGroupIndexTable = new ShopAttributeGroupIndexTable($this->getServiceLocator());
                    foreach ($attributeById[0]['attribute_groups'] as $attributeGroupIdToDelete) {
                        $shopAttributeGroupIndexTable->delete($attributeGroupIdToDelete, $attributeId);
                    }
                    foreach ($validData['attribute_groups'] as $attributeGroupId) {
                        $shopAttributeGroupIndexTable->add($attributeGroupId, $attributeId);
                    }
                }
                if ($isUpdated !== false) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                } else {
                    $this->flashMessenger()->addInfoMessage(__('Operation failed!'));
                }
                if ($data['url']) {
                    return $this->redirect()->toUrl($data['url']);
                } else {
                    return $this->redirect()->toRoute('admin-shop', array(
                        'action' => 'attributes',
                        "lang" => $this->lang,
                        "id" => $attributeById[0]["attribute_groups"][0]
                    ));
                }
            } else {
                $this->layout()->errorMessage = __("Please fill the requiered filed!");
            }
        }
        // $view['attribute_id'] = $attributeId;
        return new ViewModel($view);
    }

    public function deleteAttributeAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $attributeId = $this->params('id', - 1);
        $groupId = $this->params('group', - 1);
        // TODO
        // $isUsed = $shopProductAttributesTable->checkCategoryIsUsed($attributeGroupId);
        $isUsed = false;
        if ($isUsed) {
            // $this->layout()->errorMessage = __("'Unable to delete. delete dependency first!'");
            $this->flashMessenger()->addErrorMessage(__('Unable to delete. delete dependency first!'));
        } else {
            if ($attributeId && $container->offsetExists('csrf')) {
                if ($container->offsetGet('csrf') == $token) {
                    $shopProductAttributesTable = new ShopProductAttributesTable($this->getServiceLocator());
                    $shopAttributeGroupIndexTable = new ShopAttributeGroupIndexTable($this->getServiceLocator());
                    $shopProductAttributeOptionsTable = new ShopProductAttributeOptionsTable($this->getServiceLocator());
                    
                    $attributesGroup = $shopAttributeGroupIndexTable->checkExist($groupId, $attributeId);
                    if ($attributesGroup) {
                        $isDeleted = $shopAttributeGroupIndexTable->delete($groupId, $attributeId);
                    } else {
                        $isDeleted = $shopAttributeGroupIndexTable->delete(null, $attributeId);
                        $isDeleted = $shopProductAttributesTable->delete($attributeId);
                        $isDeleted = $shopProductAttributeOptionsTable->delete($attributeId);
                    }
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
                }
                return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
            }
        }
        // return $this->redirect()->toRoute('admin-shop', array('action' => 'attributes'));
        return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
    }
    
    // Assigning values to attributes
    public function assignValuesToAttributeAction()
    {
        $attributeId = $this->params('id', - 1);
        $shopProductAttributeOptionsForm = new ShopProductAttributeOptionsForm();
        $view['shopProductAttributeOptionsForm'] = $shopProductAttributeOptionsForm;
        $shopProductAttributeOptionsTable = new ShopProductAttributeOptionsTable($this->getServiceLocator());
        $registeredProductAttributeOptions = $shopProductAttributeOptionsTable->fetchAttributeValues($attributeId);
        $tempOptions = array();
        if ($registeredProductAttributeOptions) {
            foreach ($registeredProductAttributeOptions as $pieces) {
                unset($pieces['attribute_id']);
                unset($pieces['index']);
                $tempOptions[] = implode('|', $pieces);
            }
        }
        $oldValues = array();
        $oldValues['values'] = implode("\n", $tempOptions);
        
        $shopProductAttributeOptionsForm->populateValues($oldValues);
        $shopProductAttributesTable = new ShopProductAttributesTable($this->getServiceLocator());
        $attributeById = $shopProductAttributesTable->fetchAll($attributeId);
        $attributeType = $attributeById[0]['type'];
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $shopProductAttributeOptionsForm->setData($data);
            if ($shopProductAttributeOptionsForm->isValid()) {
                $validData = $shopProductAttributeOptionsForm->getData();
                $shopProductAttributeOptionsTable = new ShopProductAttributeOptionsTable($this->getServiceLocator());
                $shopProductAttributeOptionsTable->delete($attributeId);
                $values = explode(PHP_EOL, $validData['values']);
                $splitValues = array();
                foreach ($values as $value) {
                    $temp = explode('|', $value);
                    $temp['index'] = $attributeType . '_options';
                    $temp['attribute_id'] = $attributeId;
                    $splitValues[] = $temp;
                }
                foreach ($splitValues as $item) {
                    if ($item[0] === '') {
                        continue;
                    }
                    $shopProductAttributeOptionsTable->add($item);
                }
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('admin-shop', array(
                    'action' => 'attributes'
                ));
            } else {
                $this->layout()->errorMessage = __("Please fill the requiered filed!");
            }
        }
        return new ViewModel($view);
    }
    
    // Products
    public function productsAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $id = $this->params('id');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
        $products = $shopProductsTable->fetchByCategoryId($id);
        $grid->setTitle(__('Category product List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($products, $dbAdapter);
        
        $col = new Column\Select('id', "shop_products");
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $grid->addColumn($col);
        
        $downloadable = new Column\Select('downloadable');
        $downloadable->setLabel(__('Downloadable'));
        $downloadable->setHidden(true);
        $grid->addColumn($downloadable);
        
        $col = new Column\Select('name');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('priority');
        $col->setLabel(__('Priority'));
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('product_lang');
        $col->setLabel(__('Language'));
        $grid->addColumn($col);
        
        $instructorId = new Column\Select('instructor_id');
        $instructorId->setLabel(__('Title'));
        $instructorId->setHidden(true);
        $grid->addColumn($instructorId);
        
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $col->setWidth(10);
        $options = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__(''));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction = new Column\Action\Icon();
        $viewAction->setIconClass('glyphicon glyphicon-plus');
        $viewAction->setTooltipTitle(__('Add videos to product'));
        $viewAction->setLink("/" . $this->lang . '/admin-shop/add-video-to-product/' . $rowId);
        $viewAction->addShowOnValue($instructorId, null, Filter::NOT_EQUAL);
        // daneshi daneshi, chera downloadable =1 hast nemishe add video kard?
        // $viewAction->addShowOnValue($downloadable, 1, Filter::NOT_EQUAL);
        $viewAction->setShowOnValueOperator("AND");
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setTooltipTitle(__('Edit'));
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/edit-product/' . $rowId);
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setTooltipTitle(__('Remove'));
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/delete-product/' . $rowId . '/token/' . $csrf);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href="/' . $this->lang . '/admin-shop/add-product/' . $id . '" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        // $link[] = '<a href="/'. $this->lang .'/admin-shop" class="btn btn-primary">'.__("Products Management").'</a>';
        
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }

    public function addVideoToProductAction()
    {
        $productId = $this->params('id');
        $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
        $productData = $shopProductsTable->fetchById($productId);
        // var_dump($productData); die;
        // daneshi daneshi, chera downloadable =1 hast nemishe add video kard?
        // if (!$productData || $productData[0]["downloadable"] == 1) {
        if (! $productData) {
            return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop');
        }
        $this->setHeadTitle(__('Add video'));
        $view['userId'] = $this->userData->id;
        $view['category_id'] = $productData[0]['category_id'];
        $view['productId'] = $productId;
        return new ViewModel($view);
    }

  
    public function completeProductInfoAction()
    {
        /*
         * $categoryId = $this->params('id', -1);
         * $productId = $_GET['productId'];
         */
        // $productId = $this->params('id');
        // $shopProductTable = new ShopProductsTable($this->getServiceLocator());
        // $categoryId = $shopProductTable->getCategoryId($productId);
        // $categoryId = $categoryId[0]['category_id'];
        
        // $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        // $shopProductAttributeInfoTable = new ShopProductAttributeInfo($this->getServiceLocator());
        // $applicationFileManagerTable = new ApplicationFileManagerTable($this->getServiceLocator());
        // $currentInfo = $shopProductAttributeInfoTable->fetch($productId);
        // $productInfoByCategoryId = $shopCategoriesTable->fetchAll(null, $categoryId);
        // $attributeGroupsIds = $productInfoByCategoryId[0]['attribute_groups'];
        // $attributeGroupsIds = explode(',', $attributeGroupsIds);
        // $allAttributeIds = array();
        // if ($attributeGroupsIds) {
        // $shopAttributeGroupIndex = new ShopAttributeGroupIndexTable($this->getServiceLocator());
        // foreach ($attributeGroupsIds as $piece) {
        // $allAttributeIds[] = $shopAttributeGroupIndex->fetch($piece);
        
        // }
        // }
        
        // $allAttributes = array();
        // if ($allAttributeIds) {
        // $shopProductAttributes = new ShopProductAttributesTable($this->getServiceLocator());
        // foreach ($allAttributeIds as $key => $attributeId) {
        // foreach ($attributeId as $innerKey => $id) {
        // $allAttributes[] = $shopProductAttributes->fetchAll($attributeId[$innerKey]['attribute_id']);
        
        // }
        // }
        // }
        
        // $allFormFields = array();
        // if ($allAttributes) {
        // $shopProductAttributeOptions = new ShopProductAttributeOptionsTable($this->getServiceLocator());
        // foreach ($allAttributes as $key => $singleAttribute) {
        // $temp = $shopProductAttributeOptions->fetchAttributeValues($singleAttribute[0]['id']);
        // $temp[0]['field_name'] = strtolower($singleAttribute[0]['developer_name']);
        // $temp[0]['name'] = strtolower($singleAttribute[0]['title']);
        // $temp[0]['mandatory'] = strtolower($singleAttribute[0]['is_forced']);
        // $allFormFields[] = $temp;
        // }
        // }//die;
        
        // $currentFiles = $applicationFileManagerTable->fetch($productId);
        // $currentInfo['file_pack'] = $currentFiles;
        // $request = $this->getRequest();
        // if ($request->isPost()) {
        // $container = new Container('token');
        // $data = $request->getPost();
        // $files = $request->getFiles();
        // if ($container->offsetGet('csrf') === $data['csrf']) {
        // unset($data['csrf']);
        // unset($data['submit']);
        // $validData = $data->toArray();
        // if (isset($_GET['edit'])) {
        // $errors = $this->formGenerator()->isValid($allAttributes, $currentInfo, $validData, $files);
        // if (count($errors) > 0) {
        // foreach ($errors as $error) {
        // $view['errors'][] = 'Please fill in the ' . $error[0] . ' field';
        // }
        // } else {
        // $fileUploadInfo = $this->formGenerator()->uploadFiles($currentInfo, $files);
        // if (count($fileUploadInfo) > 0) {
        // $shopProductAttributeInfoTable->delete($productId);
        // foreach ($validData as $name => $validDatum) {
        
        // $errors[] = $this->formGenerator()->isValid($name, $currentInfo, $validData);
        // $shopProductAttributeInfoTable->add($productId, $name, $validDatum);
        // }
        // //Record file info in DB
        // foreach ($fileUploadInfo as $name => $info) {
        // $type = $this->formGenerator()->getFieldType($info[0], $allAttributes);
        // $fileId = $applicationFileManagerTable->add($this->userData->id, $productId, $info[0], $info[1], '', $type, $info[2]);
        // $shopProductAttributeInfoTable->add($productId, $info[0], $fileId);
        // }
        // $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
        // }
        // return $this->redirect()->toUrl('/admin-shop/products/'.$categoryId);
        // }
        // } else {
        
        // $errors = $this->formGenerator()->isValid($allAttributes, $currentInfo, $validData, $files);
        // if (count($errors) > 0) {
        // foreach ($errors as $error) {
        // $view['errors'][] = 'Please fill in the ' . $error[0] . ' field';
        // }
        // } else {
        // $fileUploadInfo = $this->formGenerator()->uploadFiles($currentInfo, $files);
        // if (count($fileUploadInfo) > 0) {
        // $shopProductAttributeInfoTable->delete($productId);
        // foreach ($validData as $name => $validDatum) {
        
        // $errors[] = $this->formGenerator()->isValid($name, $currentInfo, $validData);
        // $shopProductAttributeInfoTable->add($productId, $name, $validDatum);
        // }
        // //Record file info in DB
        // foreach ($fileUploadInfo as $name => $info) {
        // $type = $this->formGenerator()->getFieldType($info[0], $allAttributes);
        // $fileId = $applicationFileManagerTable->add($this->userData->id, $productId, $info[0], $info[1], '', $type, $info[2]);
        // $shopProductAttributeInfoTable->add($productId, $info[0], $fileId);
        // }
        // $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
        // }
        // return $this->redirect()->toUrl('/admin-shop/products/'.$categoryId);
        // }
        
        // }
        // } else {
        // $this->flashMessenger()->addErrorMessage('Token Invalid');
        // }
        // }
        
        // $container = new Container('token');
        // $hash = md5(time() . rand());
        // $container->csrf = $hash;
        // $csrf = $container->csrf;
        
        // $form = $this->formGenerator()->generateForm($allFormFields, 'completeProductInfo', $csrf, $currentInfo);
        // $view['form'] = $form;
        
        // return new ViewModel($view);
    }

    public function deleteProductAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $productId = $this->params('id', - 1);
        if ($productId && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
                $productInfo = $shopProductsTable->getCategoryId($productId);
                if ($productInfo) {
                    $isDelete = $shopProductsTable->deleteProduct($productId);
                    if ($isDelete) {
                        $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                        return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/products/' . $productInfo[0]['category_id']);
                    } else {
                        $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
                        return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/products/' . $productInfo[0]['category_id']);
                    }
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        return $this->redirect()->toRoute('admin-shop', array(
            'action' => 'products'
        ));
    }

    public function deleteOrderAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $orderId = $this->params('id', - 1);
        if ($orderId && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $ordersTable = new OrdersTable($this->getServiceLocator());
                $orderInfo = $ordersTable->getRecords(array(
                    "id" => $orderId
                ));
                if ($orderInfo) {
                    $ordersTable->deleteOrder($orderId);
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        return $this->redirect()->toRoute('admin-shop', array(
            'action' => 'list-orders'
        ));
    }

    public function deletePaymentAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $paymentId = $this->params('id', - 1);
        if ($paymentId && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $paymentsTable = new PaymentsTable($this->getServiceLocator());
                $paymentInfo = $paymentsTable->fetchAll(array(
                    "id" => $paymentId
                ));
                if ($paymentInfo) {
                    $paymentsTable->deletePayment($paymentId);
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        return $this->redirect()->toRoute('admin-shop', array(
            'action' => 'list-payments'
        ));
    }
    
    public function addAction(){
        $validationExt ="jpg,jpeg,png,gif,mp4";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/products/";
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $categories = $categoriesTable->fetchAll();
        $productsForm = new ProductsForm($categories);
        $view['productsForm'] = $productsForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $file =  $request->getFiles()->toArray();
//             $productsForm->setData($data);
//             if ($productsForm->isValid()) {
//                 $data = $productsForm->getData();
                $productsTable = new ProductsTable($this->getServiceLocator());
                $data['image'] = '';
                if ($file['image']['tmp_name'] != '') {
                    $newFileName = md5(time() . $file['image']['name']);
                    $isUploaded = $this->imageUploader()->UploadFile($file['image'],$validationExt, $uploadDir, $newFileName);
                    if($isUploaded[0] == true){
                        $data['image'] = $isUploaded[1];
                    }
                }
                $data['status'] = 'show';
                $addProduct = $productsTable->add($this->userData->id,$data);
                
                if($addProduct){
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/list-products');
                }
//             }
            
        }
        return new ViewModel($view);
    }
    
    public function listProductsAction(){
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $id = $this->params('id');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $productsTable = new ProductsTable($this->getServiceLocator());
        $products = $productsTable->fetchAll();
        $grid->setTitle(__('Digibenis Products List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($products, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $grid->addColumn($col);
        
        $col = new Column\Select('product_name');
        $col->setLabel(__('Product Name'));
        $grid->addColumn($col);
        
        $col = new Column\Select('price');
        $col->setLabel(__('Price'));
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('discount_price');
        $col->setLabel(__('Discount Price'));
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('unit');
        $col->setLabel(__('Unit'));
        $grid->addColumn($col);
        
        $col = new Column\Select('count');
        $col->setLabel(__('Count'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__(''));
        $rowId = $btn->getRowIdPlaceholder();
        
//         $viewAction = new Column\Action\Icon();
//         $viewAction->setIconClass('glyphicon glyphicon-plus');
//         $viewAction->setTooltipTitle(__('Add videos to product'));
//         $viewAction->setLink("/" . $this->lang . '/admin-shop/add-video-to-product/' . $rowId);
//         $viewAction->addShowOnValue($instructorId, null, Filter::NOT_EQUAL);
        // daneshi daneshi, chera downloadable =1 hast nemishe add video kard?
        // $viewAction->addShowOnValue($downloadable, 1, Filter::NOT_EQUAL);
//         $viewAction->setShowOnValueOperator("AND");
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setTooltipTitle(__('Edit'));
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/edit/' . $rowId);
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setTooltipTitle(__('Remove'));
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/delete/' . $rowId . '/token/' . $csrf);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
//         $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href="/' . $this->lang . '/admin-shop/add' . $id . '" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        // $link[] = '<a href="/'. $this->lang .'/admin-shop" class="btn btn-primary">'.__("Products Management").'</a>';
        
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    
    public function UserProductsListAction(){
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        $group = ['3','7','8'];
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        
        $id = $this->params('id');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $productsTable = new ProductsTable($this->getServiceLocator());
        $products = $productsTable->fetchByGroup($group);
        $grid->setTitle(__('Users Products List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($products, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $grid->addColumn($col);
        
        $col = new Column\Select('username');
        $col->setLabel(__('Username'));
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(__('User Group'));
        $grid->addColumn($col);
        
        $col = new Column\Select('product_name');
        $col->setLabel(__('Product Name'));
        $grid->addColumn($col);
        
        $col = new Column\Select('price');
        $col->setLabel(__('Price'));
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('unit');
        $col->setLabel(__('Unit'));
        $grid->addColumn($col);
        
        $col = new Column\Select('count');
        $col->setLabel(__('Count'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__(''));
        $rowId = $btn->getRowIdPlaceholder();
        
        //         $viewAction = new Column\Action\Icon();
        //         $viewAction->setIconClass('glyphicon glyphicon-plus');
        //         $viewAction->setTooltipTitle(__('Add videos to product'));
        //         $viewAction->setLink("/" . $this->lang . '/admin-shop/add-video-to-product/' . $rowId);
        //         $viewAction->addShowOnValue($instructorId, null, Filter::NOT_EQUAL);
        // daneshi daneshi, chera downloadable =1 hast nemishe add video kard?
        // $viewAction->addShowOnValue($downloadable, 1, Filter::NOT_EQUAL);
        //         $viewAction->setShowOnValueOperator("AND");
        
        $viewAction3 = new Column\Action\Icon();
        $viewAction3->setIconClass('glyphicon glyphicon-check');
        $viewAction3->setTooltipTitle(__('confirm'));
        $viewAction3->setLink("/" . $this->lang . '/admin-shop/change-status/' . $rowId);
        
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setTooltipTitle(__('Edit'));
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/edit/' . $rowId . '/group/3');
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setTooltipTitle(__('Remove'));
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/delete/' . $rowId  . '/group/3' . '/token/' . $csrf);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        //         $actions2->addAction($viewAction);
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->addAction($viewAction3);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
//         $link[] = '<a href="/' . $this->lang . '/admin-shop/add' . $id . '" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
//         $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        // $link[] = '<a href="/'. $this->lang .'/admin-shop" class="btn btn-primary">'.__("Products Management").'</a>';
        
//         $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }

    public function editAction(){ 
        $validationExt ="jpg,jpeg,png,gif,mp4";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/products/";
        $id = $this->params('id');
        $productsTable = new ProductsTable($this->getServiceLocator());
        $product = $productsTable->fetchById($id);
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $categories = $categoriesTable->fetchAll();
        //var_dump($categories);die;
        $productsForm = new ProductsForm($categories);
        $productsForm->populateValues($product);
        $view['productsForm'] = $productsForm;
        $view['product'] = $product;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $file =  $request->getFiles()->toArray();
            $productsTable = new ProductsTable($this->getServiceLocator());
            $data['image'] = ' ';
            if($file['image']['tmp_name'] != '') {
                $newFileName = md5(time() . $file['image']['name']);
                $isUploaded = $this->imageUploader()->UploadFile($file['image'],$validationExt, $uploadDir, $newFileName);
//                 var_dump($isUploaded);die;
                if($isUploaded[0] == true){
                    $data['image'] = $isUploaded[1];
                }
            }else{
                
                $data['image'] = $product['image'];
            }
            $updateProduct = $productsTable->update($id,$data);
            if($updateProduct){
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                if($this->params('group') == '3'){
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/user-products-list');
                }else{
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/list-products');
                }
                
            }  
        }
        return new ViewModel($view);
    }
    
    public function deleteAction(){
        $container = new Container('token');
        $token = $this->params('token');
        $productId = $this->params('id', - 1);
        if ($productId && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $shopProductsTable = new ProductsTable($this->getServiceLocator());
                $isDelete = $shopProductsTable->delete($productId);
                if ($isDelete) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/list-products');
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/list-products' );
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        if($this->params('group') == '3'){
            return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/user-products-list');
        }else{
            return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/list-products');
        }
    }
    
    public function categoryListAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $categories = $categoriesTable->fetchAll();
        $grid->setTitle(__('Category product List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($categories, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $grid->addColumn($col);
        
        $col = new Column\Select('title');
        $col->setLabel(__('Title'));
        $grid->addColumn($col);
        
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $col->setWidth(10);
        $options = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setFilterSelectOptions($options);
        $replaces = array(
            '1' => 'Active',
            '0' => 'InActive'
        );
        $col->setReplaceValues($replaces, $col);
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__(''));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('glyphicon glyphicon-edit');
        $viewAction1->setTooltipTitle(__('Edit'));
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/edit-product-category/' . $rowId);
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-remove');
        $viewAction2->setTooltipTitle(__('Remove'));
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/delete-product-category/' . $rowId . '/token/' . $csrf);
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction1);
        $actions2->addAction($viewAction2);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        $link[] = '<a href=' . "/" . $this->lang . '/admin-shop/add-product-category class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>'.__("Add").'</a>';
        $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>'.__("Help").'</a>';
        $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    
    public function addProductCategoryAction()
    {
        $productCategoriesForm = new CategoriesForm();
        $view['productCategoriesForm'] = $productCategoriesForm;
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $categoryAdd = $categoriesTable->addCategory($data);
            if($categoryAdd){
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toUrl('/' . $this->lang . '/admin-shop/category-list');
            }  
        }
        return new ViewModel($view);
    } 
    
    public function editProductCategoryAction()
    {
        $id = $this->params('id');
        $productCategoriesForm = new CategoriesForm();
        $categoriesTable = new CategoriesTable($this->getServiceLocator());
        $category = $categoriesTable->getById($id);
        $productCategoriesForm->populateValues($category);
        $view['productCategoriesForm'] = $productCategoriesForm;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $categoryEdit = $categoriesTable->editCategory($data,$id);
            $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
            return $this->redirect()->toRoute('admin-shop', array(
                'action' => 'category-list'
            ));
            
        }
        return new ViewModel($view);
    }
    
    public function deleteProductCategoryAction()
    {
        $container = new Container('token');
        $token = $this->params('token');
        $id = $this->params('id', - 1);
        if ($id && $container->offsetExists('csrf')) {
            if ($container->offsetGet('csrf') == $token) {
                $categoriesTable = new CategoriesTable($this->getServiceLocator());
                $isDelete = $categoriesTable->delete($id);
                if ($isDelete) {
                    $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/category-list');
                } else {
                    $this->flashMessenger()->addErrorMessage(__("Operation Encounter Problem."));
                    return $this->redirect()->toUrl("/" . $this->lang . '/admin-shop/category-list' );
                }
            } else {
                $this->flashMessenger()->addErrorMessage(__('Operation failed!'));
            }
        }
        return $this->redirect()->toRoute('admin-shop', array(
            'action' => 'category-list'
        ));
    }
    
    public function changeStatusAction(){
        $id = $this->params('id');
//         var_dump($id);die;
        $productsTable = new ProductsTable($this->getServiceLocator());
        $getProduct = $productsTable->fetchById($id);
        if($getProduct['status'] == 'pending'){
            $status = 'show';
        }elseif($getOrder['status'] == 'show'){
            $status = 'pending';
        }
//         elseif($getOrder['status'] == 'sent'){
//             $status = 'checking';
//         }
        
        $update = $productsTable->updateStatus($status, $id);
        if($update){
            $this->flashMessenger()->addSuccessMessage("وضعیت محصول به " . __($status) . " تغییر کرد.");
            return $this->redirect()->toRoute('admin-shop', array(
                'action' => 'user-products-list',
            ));
        }
    }
    
    public function ordersAction()
    {
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $productsOrderTable = new OrderTable($this->getServiceLocator());
        $orders = $productsOrderTable->getAll('product','all');//var_dump($orders);die;
        foreach ($orders as $key => $value){
            if($value['created']) {
                $val = explode(' ', $value['created']);
                $val =  explode('-', $val[0]);
                $g_y = $val[0];
                $g_m = $val[1];
                $g_d = $val[2];
                $val['created'] = Jdates::gregorian_to_jalali($g_y, $g_m, $g_d);
            }
            $orders[$key]['created'] = implode('/', $val['created']);
            if($value['status'] == 'sent'){
                $orders[$key]['status'] = 'ارسال شده';
            }elseif($value['status'] == 'checking'){
                $orders[$key]['status'] = 'در حال بررسی';
            }else{
                $orders[$key]['status'] = 'ثبت شده';
            }
        }
        $grid->setTitle(__('Order product List'));
        $grid->setDefaultItemsPerPage(10);
        $grid->setDataSource($orders, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Order Id'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('username');
        $col->setLabel(__('Ordered by'));
        $grid->addColumn($col);
        
        $col = new Column\Select('total_price');
        $col->setLabel(__('Total Price'));
        $grid->addColumn($col);
        
        $col = new Column\Select('created');
        $col->setLabel(__('Order Created'));
        $grid->addColumn($col);
        
        $col = new Column\Select('status');
        $col->setLabel(__('Status'));
        $grid->addColumn($col);
        
        $col = new Column\Select('address');
        $col->setLabel(__('Address'));
        $grid->addColumn($col);
        
        $col = new Column\Select('postal_code');
        $col->setLabel(__('Postal Code'));
        $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        $viewAction2 = new Column\Action\Icon();
        $viewAction2->setIconClass('glyphicon glyphicon-check');
        $viewAction2->setLink("/" . $this->lang . '/admin-shop/change-order-status/' . $rowId);
        $viewAction2->setTitle(__('change status'));
        
        $viewAction1 = new Column\Action\Icon();
        $viewAction1->setIconClass('fa fa-file-o');
        $viewAction1->setLink("/" . $this->lang . '/admin-shop/view-order/' . $rowId);
        $viewAction1->setTitle(__('view detail'));
        
        $actions2 = new Column\Action();
        $actions2->setLabel(__('Operations'));
        $actions2->addAction($viewAction2);
        $actions2->addAction($viewAction1);
        $actions2->setWidth(15);
        $grid->addColumn($actions2);
        
        //         $link[] = '<a href=' . "/" . $this->lang . '/admin-ourservices/add-attributes class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        //         $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        //         $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    
    public function changeOrderStatusAction(){
        $id = $this->params('id');//var_dump($id);die;
        
        $ordersTable = new OrderTable($this->getServiceLocator());
        $getOrder = $ordersTable->getById($id);
        if($getOrder['status'] == 'requested'){
            $status = 'checking';
        }elseif($getOrder['status'] == 'checking'){
            $status = 'sent';
        }elseif($getOrder['status'] == 'sent'){
            $status = 'checking';
        }
        
        $update = $ordersTable->updateStatus($status, $id);
        if($update){
            $this->flashMessenger()->addSuccessMessage("وضعیت سفارش به " . __($status) . " تغییر کرد.");
            return $this->redirect()->toRoute('admin-shop', array(
                'action' => 'orders',
            ));
        }
    }
    
    public function viewOrderAction(){
        $contanier = new Container('token');
        $csrf = md5(time() . rand());
        $contanier->csrf = $csrf;
        $id = $this->params('id');
//         $ordersTable = new OrderIndexTable($this->getServiceLocator());
        $grid = $this->getServiceLocator()->get('ZfcDatagrid\Datagrid');
        $config = $this->getServiceLocator()->get('Config');
        $dbAdapter = new Adapter($config['db']);
        $productOrderDetailTable = new ProductsOrderTable($this->getServiceLocator());
        $getDetail = $productOrderDetailTable->getByOrder($id);
//         $orders = $ordersTable->getByOrder($id);//var_dump($orders);die;
//         foreach ($orders as $key => $val){
//             $orders[$key]['value'] = implode(',', json_decode($val['value']));
//         }
        
        $grid->setTitle(__('Orders Detail List'));
        $grid->setDefaultItemsPerPage(15);
        $grid->setDataSource($getDetail, $dbAdapter);
        
        $col = new Column\Select('id');
        $col->setLabel(__('Row'));
        $col->setIdentity();
        $col->addStyle(new Style\Bold());
        $grid->addColumn($col);
        
        $col = new Column\Select('product_name');
        $col->setLabel(__('Product Name'));
        $grid->addColumn($col);
        
        $col = new Column\Select('price');
        $col->setLabel(__('Price'));
        $grid->addColumn($col);  
        
        $col = new Column\Select('qty');
        $col->setLabel(__('Qty'));
        $grid->addColumn($col); 
        
        
        $col = new Column\Select('total_price');
        $col->setLabel(__('Total Price'));
        $grid->addColumn($col);
        
        //         $col = new Column\Select('delivery_price');
        //         $col->setLabel(__('Delivery Price'));
        //         $grid->addColumn($col);
        
        //         $col = new Column\Select('created');
        //         $col->setLabel(__('Order Created'));
        //         $grid->addColumn($col);
        
        //         $col = new Column\Select('status');
        //         $col->setLabel(__('Status'));
        //         $grid->addColumn($col);
        
        $btn = new Column\Action\Button();
        $btn->setLabel(__('Edit'));
        $rowId = $btn->getRowIdPlaceholder();
        
        //         $viewAction2 = new Column\Action\Icon();
        //         $viewAction2->setIconClass('glyphicon glyphicon-check');
        //         $viewAction2->setLink("/" . $this->lang . '/admin-ourservices/change-order-status/id/' . $rowId);
        //         $viewAction2->setTitle(__('change status'));
        
        //         $viewAction1 = new Column\Action\Icon();
        //         $viewAction1->setIconClass('fa fa-file-o');
        //         $viewAction1->setLink("/" . $this->lang . '/admin-ourservices/view-order/id/' . $rowId);
        //         $viewAction1->setTitle(__('view detail'));
        
        //         $actions2 = new Column\Action();
        //         $actions2->setLabel(__('Operations'));
        //         $actions2->addAction($viewAction2);
        //         $actions2->addAction($viewAction1);
        //         $actions2->setWidth(15);
        //         $grid->addColumn($actions2);
        
        //         $link[] = '<a href=' . "/" . $this->lang . '/admin-ourservices/add-attributes class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i>' . __("Add") . '</a>';
        //         $link[] = '<a href="/' . $this->lang . '/help" class="btn btn-warning"><i class="fa fa-life-ring" aria-hidden="true"></i>' . __("Help") . '</a>';
        //         $grid->setLink($link);
        
        $grid->render();
        return $grid->getResponse();
    }
    
	    
    public function editSamplesAction(){
        $validationExt = "jpg,jpeg,png,gif,pdf";
        $config = $this->serviceLocator->get('Config');
        $uploadDir = $config['base_route'] . "/uploads/designer/samples/";
        $id = $this->params('id');
        $designerSampleForm = new DesignerSampleForm();
        $designerSampleTable = new DesignerSampleTable($this->getServiceLocator());
        $getSample = $designerSampleTable->fetchById($id);
        $designerSampleForm->populateValues($getSample);
        $view['designerSampleForm'] = $designerSampleForm;
        $view['getSample'] = $getSample;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $file = $request->getFiles()->toArray();
//             var_dump($data);var_dump($file);die;
            $designerSampleTable = new DesignerSampleTable($this->getServiceLocator());
            $data['file'] = '';
            if ($file['file']['tmp_name'] != '') {
                $newFileName = md5(time() . $file['file']['name']);
                $isUploaded = $this->imageUploader()->UploadFile($file['file'], $validationExt, $uploadDir, $newFileName);
                if ($isUploaded[0] == true) {
                    $data['file'] = $isUploaded[1];
                }
            }
            $add = $designerSampleTable->update($id, $data);
            
            if ($add) {
                $this->flashMessenger()->addSuccessMessage(__("Operation done successfully."));
                return $this->redirect()->toRoute('user-admin', array(
                    'action' => 'samples',
                    'id' => $getSample['user_id']
                ));
            }
        }
        
        
        return new ViewModel($view);

    }

}   
