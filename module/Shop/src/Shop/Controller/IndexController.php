<?php

namespace Shop\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Shop\Model\ShopCategoriesTable;
use Shop\Model\ShopProductsTable;
use Application\Model\ApplicationFileManagerTable;
use Shop\Model\ShopProductAttributeInfo;
use Shop\Model\ShopAttributeGroupIndexTable;
use Shop\Model\ShopProductAttributesTable;
use Zend\Session\Container;
use Application\Helper\BaseController;
use Application\Helper\BaseCustomerController;
use Application\Form\LoginForm;
use Application\Form\RegistrationForm;
use Shop\Model\UsersTable;
use Shop\Model\TransmitalwayTable;
use Shop\Model\PaymentwayTable;
use Shop\Model\CountryTable; 
use Shop\Model\BanksTable;
use Shop\Form\BuyOrderForm;
use Shop\Form\PayOrderForm;
use Shop\Model\PaymentsTable;
use Shop\Helper\BanksHelper;
use Shop\Model\OrdersTable;
use Shop\Model\OrdersDetailsTable;
use Shop\Model\UsersDataTable;
use Shop\Model\EducationLessonsTable;
use Shop\Model\EducationFieldofstudyTable;
use Application\Form\LoginSigninForm;
use Zend\EventManager\EventManager;
use Application\Helper\Jdates;
use Shop\Model\UsersLoginLogTable;
use Shop\Model\ProvincesTable;
use Shop\Model\CitiesTable;
use Shop\Model\ShopAttributeGroupsCategoryIndexTable;
use Zend\Db\Sql\Where;
use Gallery\Model\GalleryFilesTable;

class IndexController extends BaseCustomerController
{
    
    public function categoriesAction()
    {
        $this->setHeadTitle(__('Product Categories'));
         $lang = $this->lang;
        $catId = false;
        if ($this->params('id')) {
            $catId = $this->params('id');
        } 
        $view = array();
        $dataArray = array();
        $categoryShop = new ShopCategoriesTable($this->getServiceLocator()); 
        $productShop = new ShopProductsTable($this->getServiceLocator()); 
        $where1 = new Where();
        $where1->equalTo('shop_products.is_deleted', '0');
        $productShopData = $productShop->fetchAllProductOnly($where1);  
  
        $where = new Where();
        $where->equalTo('parent_id', 1); 
        $where->equalTo('cat_lang', $lang);
        
        $categoryShopData = $categoryShop->fetchAllByParentId($where);
 
        foreach ($categoryShopData as &$category) {
            if (!$catId) {
             $catId = $category['id'];
            }
            foreach ($productShopData as $product) {
                if ($product['category_id'] == $category['id']) {
                    $category['product'][] = $product;
                }
            }
        }  
        $view['productShopData'] = $categoryShopData;
        $view['categoryId'] = $catId;
    	return new ViewModel($view);
    }
    
    public function productDetailAction()
    { 
        $productId = $this->params('id', null);  
        $productSlug = $this->params('slug', -1);
        $where = new Where();
        $where->equalTo('shop_products.id', $productId);
        $where->equalTo('shop_products.is_deleted', '0');
        $productShop = new ShopProductsTable($this->getServiceLocator());
        $galleryDataObject = new GalleryFilesTable($this->getServiceLocator());
        $productShopData = $productShop->fetchAllProduct($where);
        $newDataArray= array();
        foreach ($productShopData as  $product) { 
        	$galleryImage = array();
        	if ($product['gallery_id'] > 0) {
        		$galleryImage = $galleryDataObject->getImageBygalleryId($product['gallery_id']);
        	}
            $attributeArray = array(
                'product_attribute_name' => $product['product_attribute_name'],
                'value' => $product['value'],
                'title' => $product['title'],
            ); 
            $newDataArray[$product['id']]['product'] = $product; 
            $newDataArray[$product['id']]['atribute'][] = $attributeArray;  
            $newDataArray[$product['id']]['galleryImage'] = $galleryImage;  
        }   
        if (isset($newDataArray[$productId])) {
            $newDataArray = $newDataArray[$productId];
        }  
        $view = array();
        if (isset($newDataArray['product']['name']))  $this->setHeadTitle($newDataArray['product']['name']);
        
        $view['productShopData'] = $newDataArray;
        $view['productId'] = $productId;
    	return new ViewModel($view);
    }
    public function realAction()
    {
        $request = $this->getRequest();
        $view['query'] = $request->getUri()->getQuery();
        $searchMode = $this->params()->fromQuery("search_submit", null);
        
        $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
     
        $view['breadcrumbs'] = $this->generalHelper()->shopbreadcrumbs($separator = ' ', $home = __('Home'), $this->lang, false, null);
        
        // category haro daram,agar category pedar bood tamame bachehash bayad ezafe beshe
        // age yeki az bachehash tik khorde bood dige faghat oon bache biad
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
     
        
        
        $categoryId = $this->params('id', null);
        
        $shopCategoriesTable = new ShopCategoriesTable($this->getServiceLocator());
        $allRealCategories = $shopCategoriesTable->fetchAllRealCategories(array("status" => 1, "id" => $categoryId));

        if (!$allRealCategories) {
            return $this->redirect()->toRoute('home');
        }
        $this->setHeadTitle($allRealCategories[0]["name"]);
        $view["categoryName"] = $allRealCategories[0]["name"];
        $productsPaginator = $shopProductsTable->searchReal($categoryId, $this->lang);
//var_dump($productsPaginator);die;
        
        $pageNo = (int)$this->params('page');
        $view['pageNo'] = $pageNo;
        
        $productsPaginator->setCurrentPageNumber($pageNo);
        $count = (int)$this->params('count', $this->configs['item-per-page']);
        $view["count"] = $count;
        $productsPaginator->setItemCountPerPage($count);
        
        $view['productsPaginator'] = $productsPaginator;
        return new ViewModel($view);
    }
    
    public function virtualAction()
    {
        $this->setHeadTitle(__('Virtual product shop'));
        $request = $this->getRequest();
        $view['query'] = $request->getUri()->getQuery();
        $searchMode = $this->params()->fromQuery("search_submit", null);
    
        $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
        $view['breadcrumbs'] = $this->generalHelper()->shopbreadcrumbs($separator = ' ', $home = __('Home'), $this->lang, true, null);
        
        // category haro daram,agar category pedar bood tamame bachehash bayad ezafe beshe
        // age yeki az bachehash tik khorde bood dige faghat oon bache biad
        $educationFieldofstudyTable = new EducationFieldofstudyTable($this->getServiceLocator());
        $educationLessonsTable = new EducationLessonsTable($this->getServiceLocator());
        $mainLessons = array();
        $instructorIds = $this->params()->fromQuery("instructor_ids", array());
        $studyIds = array();
        $lessonIds = array();
        $productId = array();
        if ($searchMode) {
            $studyIds = $this->params()->fromQuery("study_ids", array());
            $lessonIds = $this->params()->fromQuery("lesson_ids", array());
            $productId = $this->params()->fromQuery("slp_ids", array());
            $mainLessons = $lessonIds;
            if ($studyIds) {
                foreach ($studyIds as $k => $studyId) {
                    $categories = array();
                    $hasChild = false;
                    $childsData = $educationLessonsTable->getRecordsByParentId($studyId);
                    if ($childsData) {
                        foreach ($childsData as $child) {
                            if (in_array($child["id"], $lessonIds)) {
                                $lessons[$child["id"]] =  $child["id"];
                                $hasChild = true;
                            }
                            $lessonsChilds[$child["id"]] = $child["id"];
                        }
                        if ($hasChild) {
                            $mainLessons = array_merge($mainLessons, $lessons);
                        } else {
                            $mainLessons = array_merge($mainLessons, $lessonsChilds);
                        }
                    }
                }
            }
        }
        $mainLessons = array_unique($mainLessons);
        $view['studyIds'] = $studyIds;
        $view['lessonIds'] = $mainLessons;
        $view['instructorIds'] = $instructorIds;
        
        
        $view["searchParams"] = $request->getServer('QUERY_STRING');

        $educationFieldStudyTable = new EducationFieldofstudyTable($this->getServiceLocator());
        $studiesList = $educationFieldStudyTable->getRecords(array("status" => 1, "lang" => $this->lang));
        $view['allVirtualCategories'] = $studiesList;
    
        $educationLessonsTable = new EducationLessonsTable($this->getServiceLocator());
        $lessonsList = $educationLessonsTable->getRecords(array("status" => 1));
    
        $lessonsArray = array();
        foreach ($lessonsList as $lessons) {
            $lessonsArray[$lessons["parent_id"]][] = $lessons;
        }
        $view['allLessons'] = $lessonsArray;
    
    
        $usersTable = new UsersTable($this->getServiceLocator());
        $instructorList = $usersTable->getAllInstructors($this->lang);
        $view['instructorList'] = $instructorList;
    
         
    
         
        $pageSort = $this->params('sort', "newest");
        $view["pageSort"] = $pageSort;
    
        $ascDesc = $this->params('type', "asc");
        $view["ascDesc"] = $ascDesc;
    
        $pageCount = (int)$this->params('count', $this->configs['item-per-page']);
        $view["pageCount"] = $pageCount;
    
    
         
        $productsPaginator = $shopProductsTable->searchVirtual($productId, $mainLessons, $instructorIds, $pageSort, $ascDesc, $this->lang);
    
        $pageNo = (int)$this->params('page');
        $view['pageNo'] = $pageNo;
    
        $productsPaginator->setCurrentPageNumber($pageNo);
        $count = (int)$this->params('count', $this->configs['item-per-page']);
        $view["count"] = $count;
    
    
        $productsPaginator->setItemCountPerPage($count);
    
        $view['productsPaginator'] = $productsPaginator;
        return new ViewModel($view);
    }
    
    public function productAction()
    {
        $productSlug = $this->params('slug', -1);
        $productInfo = $this->shopHelper()->getProductsInfo(null, array($productSlug), false, $this->lang);
        $view['breadcrumbs'] = $this->generalHelper()->shopbreadcrumbs($separator = ' ', $home = __('Home'), $this->lang, $productInfo[0]['instructor_id'], $productSlug);
//var_dump($productInfo); die;
        if ($productInfo) {
            $this->setHeadTitle($productInfo[0]["name"]);
            $view['productInfo'] = $productInfo[0];
            $relatedProductsInfo = array();
            if ($productInfo[0]["related_products"]) {
                $relatedProducts = explode(",", $productInfo[0]["related_products"]);
                $relatedProductsInfo = $this->shopHelper()->getProductsInfo($relatedProducts);
            }
           
            $events = new EventManager();
            $events->trigger('logMe', null, array("entity_type" => "product", "entity_id" => $productInfo[0]["id"], "user_id" => (isset($this->UserData)?$this->UserData:null)));
             
            
            $productInfo[0]["related_products"] = $relatedProductsInfo;
            $view['productInfo'] = $productInfo[0];
            return new ViewModel($view);
        } else {
            return $this->redirect()->toRoute('home');
        }
    }
    
    public function showCardAction()
    {
        $container = new Container('addtocard');
        $view['breadcrumbs'] = $this->generalHelper()->shopbreadcrumbs($separator = ' ', $home = __('Home'), $this->lang, null, 'show-card');
        
        $view['userData'] = $this->userData;
        $view['card'] = $container;
        return new ViewModel($view);
    }
    
    public function checkoutAction()
    {
        $this->setHeadTitle(__('Login Or Register form'));
        
        if (isset($this->userData)) {
            return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
        }
        

        $registrationForm = new RegistrationForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            if (isset($postData["show_password"])) {
                $registrationForm->setData($postData);
                if ($registrationForm->isValid()) {
                    $config = $this->getServiceLocator()->get('Config');
                    $usersTable = new UsersTable($this->getServiceLocator());
                    $userId = $usersTable->registerNewUser((array)$registrationForm->getData(), $config['default_user_group']);
                    if ($userId == 'user_exists') {
                        $this->layout()->errorMessage =  __("This email is taken. Please choose another one.");
                    } elseif ($userId !== false) {
                
                        $usersDataTable = new UsersDataTable($this->getServiceLocator());
                        $userData['firstname'] = "";
                        $userData['lastname'] = "";
                        $userData['research'] = "";
                        $userData['experience'] = "";
                        $userData['research'] = "";
                        $userData['aboutme'] = "";
                        $userData['lang'] = $this->lang;
                        $usersDataTable->addUserData($userData, $userId);
                        
                         $message = 'Congratulation! You registerd successfully in Cadafzar. your username and password are: <br />'. $postData['email'].'<br />'.$postData['password'];
                         $this->SMSService()->send($message, $postData['mobile']);
                
                         $bodyText = 'Congratulation! You registerd successfully in Cadafzar. your username and password are: <br />'. $postData['email'].'<br />'.$postData['password'];
                         $this->mailService()->sendMail($bodyText, 'Welcome message', $postData['email']);
                
                        $this->flashMessenger()->addSuccessMessage(__("Your registration is completed, you can sign in throught the following form."));
                        $this->authenticationService->auth($postData['email'], $postData['password']);
                        
                        $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                        $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                        
                        return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
                    } else {
                        $this->layout()->errorMessage = __("Operation failed!");
                    }
                }
            }
            
        }
        $view['registrationForm'] = $registrationForm;
       

        $loginSigninForm = new LoginSigninForm("/shop/checkout");
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $loginSigninForm->setData($postData);
            if ($loginSigninForm->isValid()) {
                $postData = $loginSigninForm->getData();
                $authStatus = $this->authenticationService->auth($postData['username'], $postData['password'], $postData["remember_me"]);
               
                if ($authStatus === true) {
                    $usersTable = new UsersTable($this->getServiceLocator());
                    //$usersTable->updateUserLastLogin($this->identity()->id);
                    
                    $loginLogTable = new UsersLoginLogTable($this->getServiceLocator());
                    $ttt = $loginLogTable->insertUserLastLogin($this->identity()->id);
                    
                    $userInfo = $usersTable->getRecords(array("id" => $this->identity()->id));
                    if ($userInfo && $userInfo[0]["mobile"]) {
                        $userIp = getenv('HTTP_CLIENT_IP')?:
                        getenv('HTTP_X_FORWARDED_FOR')?:
                        getenv('HTTP_X_FORWARDED')?:
                        getenv('HTTP_FORWARDED_FOR')?:
                        getenv('HTTP_FORWARDED')?:
                        getenv('REMOTE_ADDR');
                        $message = __("You logged in: ") .Jdates::jdate(time()) . ", " . __("Your IP: ") . $userIp;
                        $this->SMSService()->send($message, $userInfo[0]["mobile"]);
                    }
                    
                    return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
                } else {
                    $this->layout()->errorMessage =  __("Your username or password is incorrect.");
                }
            }
        }
        $view['loginForm'] = $loginSigninForm;
        return new ViewModel($view);
    }
    
    
    public function transmitalWaysAction()
    {
        $container = new Container('addtocard');
        
        if (!$this->userData) {
            return $this->redirect()->toRoute('shop-home', array("action" => "checkout"));
        } else if (!$container->offsetExists("totalSum")) {
            return $this->redirect()->toRoute('shop-home', array("action" => "show-card"));
        } else {
            // user login karde pass discount va tax marboote emal shavad
            $totalSum = 0;
            $totalQuantity = 0;
            $shopProductsTable = new ShopProductsTable($this->getServiceLocator());
            foreach ($container as $productId => $productInfo) {
                if (isset($productInfo["sum"])) {
                    $discount =  $this->shopHelper()->getDiscounts($this->userData->id, $productId);
                    $tax = $this->shopHelper()->getTaxes($productId);

                    $data["id"] = $productInfo["id"];
                    $data["name"] = $productInfo["name"];
                    $data["price"] = $productInfo["price"];
                    $data["discountRate"] = $discount;
                    $data["taxRate"] = $tax;
                    $data["discount"] = $productInfo["price"] * $discount / 100;
                    $data["tax"] = ($productInfo["price"] - $data["discount"]) * $tax / 100;
                    $data["quantity"] = $productInfo["quantity"];
                    $data["sum"] = ($data["price"] - $data["discount"] + $data["tax"]) * $productInfo["quantity"];
                    $container->offsetSet($productId, $data);

                    $totalSum += $data["sum"];
                    $totalQuantity += $data["quantity"];
                    
                }
            }
            $container->totalSum = $totalSum;
        $view['breadcrumbs'] = $this->generalHelper()->shopbreadcrumbs($separator = ' ', $home = __('Home'), $this->lang, null, true);
        }
        $trasmitalWays = new TransmitalwayTable($this->getServiceLocator());
        $view["transmitalWays"]  = $trasmitalWays->getAllTransmitalways();
        
        $usersTable = new UsersTable($this->getServiceLocator());
      
        $userInfo = $usersTable->getUserById($this->userData->id, $this->lang);
        if (!$userInfo) {
            $userInfo[0] = array();
        }
        if (!$userInfo[0]["country"] ) {
            $userInfo[0]["country"] = 1;
            $userInfo[0]["province"] = 7;
            $userInfo[0]["city"] = 117;
        }
       
        if ($container->offsetExists("transmitalWay")) {
            $userInfo[0]["transmitalWay"] = $container->offsetGet("transmitalWay");
        }
        if ($container->offsetExists("firstName")) {
            $userInfo[0]["firstname"] = $container->offsetGet("firstName");
        }
        if ($container->offsetExists("lastName")) {
            $userInfo[0]["lastname"] = $container->offsetGet("lastName");
        }
        if ($container->offsetExists("country")) {
            $userInfo[0]["country"] = $container->offsetGet("country");
        }
        if ($container->offsetExists("province")) {
            $userInfo[0]["province"] = $container->offsetGet("province");
        }
        if ($container->offsetExists("city")) {
            $userInfo[0]["city"] = $container->offsetGet("city");
        }
        if ($container->offsetExists("postalcode")) {
            $userInfo[0]["postalcode"] = $container->offsetGet("postalcode");
        }
        if ($container->offsetExists("mobile")) {
            $userInfo[0]["mobile"] = $container->offsetGet("mobile");
        }
        if ($container->offsetExists("phone")) {
            $userInfo[0]["phone"] = $container->offsetGet("phone");
        }
        if ($container->offsetExists("address")) {
            $userInfo[0]["address"] = $container->offsetGet("address");
        }
        
        $view["userInfo"] = $userInfo[0];
        
        $countryTable = new CountryTable($this->getServiceLocator());
        $view["countryList"]  = $countryTable->getRecords();
        
        return new ViewModel($view);
        
    }
    
    public function paymentWaysAction()
    {
        $request = $this->getRequest();
        $view['breadcrumbs'] = $this->generalHelper()->shopbreadcrumbs($separator = ' ', $home = __('Home'), $this->lang, null, true);
        
        $container = new Container('addtocard');
        if (!$container->offsetExists("totalSum")) {
            return $this->redirect()->toRoute('shop-home', array("action" => "show-card"));
        }
        if ($request->isPost()) {
            
            $postedData = $request->getPost();
            
            if (isset($postedData["transmitalWay"])) {
                
                $trasmitalWays = new TransmitalwayTable($this->getServiceLocator());
                $transmitalWayInfo  = $trasmitalWays->getTransmitalway($postedData["transmitalWay"]);
                
                if ($transmitalWayInfo) {
                    $view["transmitalWayPrice"] = $transmitalWayInfo[0]["amount"];
                    $container->offsetSet("transmitalWay", $transmitalWayInfo[0]);
                    $container->offsetSet("firstName", $postedData["firstname"]);
                    $container->offsetSet("lastName", $postedData["lastname"]);
                    $container->offsetSet("country", $postedData["country"]);
                    $container->offsetSet("province", $postedData["province"]);
                    $container->offsetSet("city", $postedData["city"]);
                    $container->offsetSet("postalcode", $postedData["postalcode"]);
                    $container->offsetSet("mobile", $postedData["mobile"]);
                    $container->offsetSet("phone", $postedData["phone"]);
                    $container->offsetSet("address", $postedData["address"]);
                } else {
                    return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
                }
                
                $view['card'] = $container;
                $paymentwayTable = new PaymentwayTable($this->getServiceLocator());
                $view["paymentWays"]  = $paymentwayTable->getAllPaymentways();
                
                $banksTable = new BanksTable($this->getServiceLocator());
                $banks = $banksTable->getBanks();
                
                if (!$banks) {
                    return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
                }
                $view["banks"] = $banks;
                
                return new ViewModel($view);
            } else  if (isset($postedData["paymentWay"])) {
                $container->offsetSet("paymentWay", $postedData["paymentWay"]);
                if ($postedData["paymentWay"] == "online") {
                    $banksTable = new BanksTable($this->getServiceLocator());
                    $bankInfo  = $banksTable->getBanks($postedData["paymentWayBank"]);
                    
                    if ($bankInfo) {
                        $container->offsetSet("paymentWayBank", $postedData["paymentWayBank"]);
                        return $this->redirect()->toRoute('shop-home', array("action" => "show-factor"));
                    } else {
                        return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
                    }
                } else {
                    $container->offsetSet("paymentWayBank", 0);
                    return $this->redirect()->toRoute('shop-home', array("action" => "show-factor"));
                }
            }
            return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
            
        }
        else if ($container->offsetGet("transmitalWay")) {
            $view['card'] = $container;
            $paymentwayTable = new PaymentwayTable($this->getServiceLocator());
            $view["paymentWays"]  = $paymentwayTable->getAllPaymentways();
            
            $banksTable = new BanksTable($this->getServiceLocator());
            $banks = $banksTable->getBanks();
            
            if (!$banks) {
                return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
            }
            $view["banks"] = $banks;
            return new ViewModel($view);
        } else {
            return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
        }
    }
    
    public function showFactorAction()
    {
        $container = new Container('addtocard');
        $view['breadcrumbs'] = $this->generalHelper()->shopbreadcrumbs($separator = ' ', $home = __('Home'), $this->lang, null, true);
        
        if (!$container->offsetExists("totalSum")) {
            return $this->redirect()->toRoute('shop-home', array("action" => "show-card"));
        }
        if ($container->offsetExists("paymentWay")) {
            if ($container->offsetExists("paymentWayBank")) {
                $request = $this->getRequest();
                if ($request->isPost()) {
                    $postedData = $request->getPost();
                    if (isset($postedData["offline"])) {
                        $orderTable = new OrdersTable($this->getServiceLocator());
                        $con = $orderTable->adapter->getDriver()->getConnection();
                        $con->beginTransaction();
                        $rollback = false;
                        if ($container->country) {
                            $countryTable = new CountryTable($this->getServiceLocator());
                            $country = $countryTable->getRecords(array("id" => $container->country));
                            $container->country = $country[0]["country_name"];
                        }
                        if ($container->province) {
                            $provincesTable = new ProvincesTable($this->getServiceLocator());
                            $province = $provincesTable->getRecords(array("id" => $container->province));
                            $container->province = $province[0]["name"];
                        }
                        if ($container->city) {
                            $citiesTable = new CitiesTable($this->getServiceLocator());
                            $city = $citiesTable->fetchCity(array("id" => $container->city));
                            $container->city = $city[0]["name"];
                        }
                        $orderId = $orderTable->addOrder($this->userData->id, $container);
                        
                        $container->offsetSet("orderId", $orderId);
                        if ($orderId) {
                            $ordersDetails = new OrdersDetailsTable($this->getServiceLocator());
                            foreach ($container as $productId => $productInfo) {
                                if (isset($productInfo["sum"])) {
                                  
                                    $isAdded = $ordersDetails->addOrderDetails($orderId, $productId, $productInfo);
                                   
                                    if (!$isAdded) {
                                        $rollback = true;
                                    }
                                }
                            }
                            if (!$rollback) {
                                $usersTable = new UsersTable($this->getServiceLocator());
                                $userInfo = $usersTable->getRecords(array("id" => $this->userData->id));
                                if ($userInfo && $userInfo[0]["mobile"]) {
                                    $message = __("Your order finished successfully, CadAfzar order id:") . $orderId;
                                    $this->SMSService()->send($message, $userInfo[0]["mobile"]);
                                }
                                //$this->flashMessenger()->addSuccessMessageShop(__("Your order submitted successfully! Order Id: ") .  $orderId);
                                $con->commit();
                                return $this->redirect()->toRoute('shop-home', array("action" => "finish-order"));
                            }
                        }
                        $con->rollback();
                        $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
                        return $this->redirect()->toRoute('shop-home', array("action" => "payment-ways"));
                    } else if (isset($postedData["online"])) {
                        
                        $orderTable = new OrdersTable($this->getServiceLocator());
                        $con = $orderTable->adapter->getDriver()->getConnection();
                        $con->beginTransaction();
                        $rollback = false;
                        if ($container->country) {
                            $countryTable = new CountryTable($this->getServiceLocator());
                            $country = $countryTable->getRecords(array("id" => $container->country));
                            $container->country = $country[0]["country_name"];
                        }
                        if ($container->province) {
                            $provincesTable = new ProvincesTable($this->getServiceLocator());
                            $province = $provincesTable->getRecords(array("id" => $container->province));
                            $container->province = $province[0]["name"];
                        }
                        if ($container->city) {
                            $citiesTable = new CitiesTable($this->getServiceLocator());
                            $city = $citiesTable->fetchCity(array("id" => $container->city));
                            $container->city = $city[0]["name"];
                        }
                        $orderId = $orderTable->addOrder($this->userData->id, $container);
                        if ($orderId) {
                            $ordersDetails = new OrdersDetailsTable($this->getServiceLocator());
                            foreach ($container as $productId => $productInfo) {
                                if (isset($productInfo["sum"])) {
                                    $isAdded = $ordersDetails->addOrderDetails($orderId, $productId, $productInfo);
                                    if (!$isAdded) {
                                        $rollback = true;
                                    }
                                }
                            }
                            $paymentsTable = new PaymentsTable($this->getServiceLocator());
                            
                            $getPayment = $paymentsTable->fetchAll(array("order_id" => $orderId));
                            if (!$getPayment) {
                                $isAdded = $paymentsTable->addPayment($this->userData->id, $container->totalSum, $container->paymentWayBank, $orderId);
                            } else {
                                $isAdded = true;
                            }
                            if($isAdded && !$rollback) {
                                
                                $banks = new BanksHelper($orderId, $container->totalSum , $container->paymentWayBank , $this->getServiceLocator());
                                $banksForm = $banks->getForm($this->configs);
                                 
                                if(isset($banksForm['result']))
                                {
                                    if($banksForm['result'])
                                    {
                                        $con->commit();
                                        $bankForm = $banksForm['form'];  /// retrieve bank form
                                        $view['bankForm'] = $bankForm;
                                        $view['viewForm'] = $banksForm['view'] ;
                                        $this->layout()->successMessage = __("Redirecting to bank...");
                                    } else {
                                        if ($bankForm['message'] == "saman" || $bankForm['message'] == "saderat") {
                                            $this->layout()->errorMessage = __("Error in form...");
                                        } else {
                                            if($bankForm['message'] < 10000)
                                                $bankForm['message'] = $this->banksErrorHelper()->getMellatBankError($bankForm['message']);
                                            $this->layout()->errorMessage = $bankForm['message'];   /// message on error => from banks.php
                            
                                        }
                                    }
                                    return new ViewModel($view);
                                } else {
                                    $con->rollback();
                                    $this->flashMessenger()->addErrorMessage(__("Error in connecting to bank"));
                                    return $this->redirect()->toRoute('shop-home', array("action" => "payment-ways"));
                                }
                            } 
                        } 
                        $con->rollback();
                        $this->flashMessenger()->addErrorMessage(__("Operation failed!"));
                        return $this->redirect()->toRoute('shop-home', array("action" => "payment-ways"));
                    } else {
                        return $this->redirect()->toRoute('shop-home', array("action" => "payment-ways"));
                    }
                    
                } else {
                    $view['card'] = $container;
                    return new ViewModel($view);
                }
            }
            
        } else {
            return $this->redirect()->toRoute('shop-home', array("action" => "transmital-ways"));
        }
        
        
    }
    
    public function finishOrderAction()
    {
        $container = new Container('addtocard');
        $view['breadcrumbs'] = $this->generalHelper()->shopbreadcrumbs($separator = ' ', $home = __('Home'), $this->lang, null, true);
        
        if (!$container->offsetExists("totalSum")) {
            return $this->redirect()->toRoute('shop-home', array("action" => "show-card"));
        }
        if ($container->paymentWayBank == 0) {
            $view["successMessage"] = __("Your order submitted successfully! Order Id: ") .  $container->orderId;
            $view['card'] =  $container;
            return new ViewModel($view);
        } else {
            $paymentsTable = new paymentsTable($this->getServiceLocator());
            $orderInfo = $paymentsTable->fetchAll(array("order_id" => $container->orderId, "status" => "paid"));
            if ($orderInfo) {
                $view["successMessage"] = __("Your order paid successfully, Bank Ref Id: ") . $orderInfo[0]["sale_reference_id"] . __("CadAfzar order id: ") . $orderInfo[0]["order_id"];
                $view['card'] =  $container;
                return new ViewModel($view);
            }
        }
        $view["errorMessage"] = __("Operation failed!");
        $view['card'] =  $container;
        return new ViewModel($view);
    }
    public function showVideoAction()
    {
        return new ViewModel();
    }
}
