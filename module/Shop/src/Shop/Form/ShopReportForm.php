<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ShopReportForm extends Form
{
	public function __construct($instructorList = null)
	{
		parent::__construct('shopReportForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
		));
		 
	    $instructors = new Element\Select('instructor_id');
	    $instructors->setLabel(__("Instructors"));
	    $instructors->setAttributes(array(
	        'id'          => 'instructor_id',
	        'class'       => 'form-control',
	    ));
	    $instructorListArray = array(
	        "" =>  __("All"),
	    );
	    foreach ($instructorList as $ins){
	        $instructorListArray[$ins['id']] = $ins['username'];
	    }
	    $instructors->setValueOptions($instructorListArray);

	    
	    $paymentStatus = new Element\Select('payment_status');
	    $paymentStatus->setLabel(__("Payment status"));
	    $paymentStatus->setAttributes(array(
	        'id'          => 'payment_status',
	        'class'       => 'form-control',
	    ));
	    $paymentListArray = array(
	        "" =>  __("All"),
	        "0" => __("Unpaid"),
	        "1" => __("Paid"),
	    );
	    $paymentStatus->setValueOptions($paymentListArray);
	    
	    $orderStatus = new Element\Select('order_status');
	    $orderStatus->setLabel(__("Order status"));
	    $orderStatus->setAttributes(array(
	        'id'          => 'payment_status',
	        'class'       => 'form-control',
	    ));
	    $orderListArray = array(
	        ''  => __("All"),
	        '0' => __('Waiting'),
		    '1' => __('Sent'),
		    '2' => __('Cancel'),
	    );
	    $orderStatus->setValueOptions($orderListArray);
	    
		$submit = new Element\Submit('submit');
		$submit->setValue(__('Search'));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		
		
		$fromDate = new Element\Text('from_date');
		$fromDate->setLabel(__("From date"))
		->setAttributes(array(
		    'class'    => 'from_date form-control',
		    'id'       => 'from_date',
		    'pattern'  => '^1[0-9]{3}\/([01]{1}[0-9]{1})\/([0123]{1}[0-9]{1}))$'   /// validator date
		));
		 
		$toDate = new Element\Text('to_date');
		$toDate->setLabel(__("To date"))
		->setAttributes(array(
		    'class'    => 'to_date form-control',
		    'id'       => 'to_date',
		    'pattern'  => '^1[0-9]{3}\/([01]{1}[0-9]{1})\/([0123]{1}[0-9]{1}))$'   /// validator date
		));
		
		$this->add($instructors)
		     ->add($paymentStatus)
		     ->add($orderStatus)
		     ->add($fromDate)
		     ->add($toDate)
		     ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
	    $inputFilter = new InputFilter();
	    $factory     = new InputFactory();
	    $inputFilter->add($factory->createInput(array(
	        'name'     => 'instructor_id',
	        'required' => false,
	        'filters' => array(
	            array(
	                'name' => 'StripTags'
	            )
	        ),
	    )));
	    $inputFilter->add($factory->createInput(array(
	        'name'     => 'payment_status',
	        'required' => false,
	        'filters' => array(
	            array(
	                'name' => 'StripTags'
	            )
	        ),
	    )));
	    $inputFilter->add($factory->createInput(array(
	        'name'     => 'order_status',
	        'required' => false,
	        'filters' => array(
	            array(
	                'name' => 'StripTags'
	            )
	        ),
	    )));
	   
	
	    $this->setInputFilter($inputFilter);
	    return $inputFilter;
	}
}
	