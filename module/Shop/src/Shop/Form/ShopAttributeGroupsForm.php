<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ShopAttributeGroupsForm extends Form
{
	public function __construct()
	{
		parent::__construct('shopAttributeGroupsForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
    		    'id'      => 'shopAttributeGroupsForm',
    		    'novalidate'=> true
		));
		
		$title = new Element\Text('title');
		$title->setAttributes(array(
				'id'    => 'shop-attribute-group-title',
				'class' => 'form-control validate[required]',
		        'required' => 'required'
		));
		$title->setLabel(__('Attribute group name'));
		
		$status = new Element\Checkbox('status');
		$status->setAttributes(array(
				'id'    => 'shop-category-status',
				'class' => ''
		));
		$status->setLabel(__('Status'));

		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__('Save and Close'));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		
		$submit2 = new Element\Submit('submit2');
		$submit2->setValue(__("Save and New"));
		$submit2->setAttributes(array(
		    'class' => 'btn btn-primary',
		    'id'    =>  'submit'
		));

		$this->add($title)
		     ->add($status)
		     ->add($csrf)
		     ->add($submit2)
		     ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'title',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'status',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
