<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ShopOrderStatusesForm extends Form
{
	public function __construct()
	{
		parent::__construct('OrderStatusesForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		$paymentStatus = new Element\Select('payment_status');
		$paymentStatus->setAttributes(array(
		    'id'       => 'payment_status',
		    'class'    => 'form-control'
		));
		$paymentStatus->setLabel(__('Payment status'));
		$paymentStatus->setValueOptions(array(
		    '0' => __('Unpaid'),
		    '1' => __('Paid'),
		));
		
		$orderStatus = new Element\Select('order_status');
		$orderStatus->setAttributes(array(
		    'id'       => 'order_status',
		    'class'    => 'form-control'
		));
		$orderStatus->setLabel(__('Order status'));
		$orderStatus->setValueOptions(array(
		    '0' => __('Waiting'),
		    '1' => __('Sent'),
		    '2' => __('Cancel'),
		));

		$refCode = new Element\Text('ref_code');
		$refCode->setAttributes(array(
		    'id'    => 'ref_code',
		    'class' => 'form-control',
		));
		$refCode->setLabel(__('Postal reference code'));
		
		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue('ثبت');
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));

		$this->add($paymentStatus)
		     ->add($orderStatus)
		     ->add($refCode)
		     ->add($csrf)
		     ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'payment_status',
				'required' => false,
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'order_status',
				'required' => false,
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
