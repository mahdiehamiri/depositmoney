<?php
namespace Shop\Form\Banks;

use Zend\Form\Form;
use Zend\Form\Element;

class SamanForm extends Form
{
    public function __construct($amount,$orderId,$MIDVlaue,$callbackUrl)
    {
        // we want to ignore the name passed
        parent::__construct('smspanel');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'stdform');
        $this->setName('saman_payment');


//         $fieldSet = new Fieldset('numbers');
//         $fieldSet->add($senderno);


        $Amount = new Element\Hidden('Amount');
        $Amount->setValue($amount);
        
        $ResNum = new Element\Hidden('ResNum');
        $ResNum->setValue($orderId);
        
        $MID = new Element\Hidden('MID');
        $MID->setValue($MIDVlaue);
        
        $RedirectURL = new Element\Hidden('RedirectURL');
        $RedirectURL->setValue($callbackUrl);

        $send = new Element\Submit('send');
        $send->setValue('تکمیل پرداخت')
        		->setAttributes(array(
	               'id' => 'submitbutton',
	            	'class'=>'btn btn-primary',
	            ));
        $this->add($Amount);
        $this->add($ResNum);
        $this->add($MID);
        $this->add($RedirectURL);
        $this->add($send);

    }

}