<?php
namespace Shop\Form\Banks;

use Zend\Form\Form;
use Zend\Form\Element;

class MellatForm extends Form
{
    public function __construct($ReferenceId)
    {
        // we want to ignore the name passed
        parent::__construct('smspanel');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'stdform');
        $this->setName('mellat_payment');


//         $fieldSet = new Fieldset('numbers');
//         $fieldSet->add($senderno);


        $RefId = new Element\Hidden('RefId');
        $RefId->setValue($ReferenceId);

        $send = new Element\Submit('send');
        $send->setValue('تکمیل پرداخت')
        		->setAttributes(array(
	               'id' => 'submitbutton',
	            	'class'=>'btn btn-primary',
	            ));
        $this->add($RefId);
        $this->add($send);

    }

}