<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class DesignerSampleForm extends Form
{

    public function __construct()
    {
        parent::__construct('designerSampleForm');
         $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            'id' => 'designerSampleForm',
            'novalidate' => true
        ));
        
        $title = new Element\Text('title');
        $title->setAttributes(array(
            'id' => 'sample-title',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $title->setLabel(__('Sample Title'));
        
        $price = new Element\Text('price');
        $price->setAttributes(array(
            'id' => 'amount_total',
            'class' => 'form-control form-control-mini validate[required] priceFormat',
            'required' => 'required'
        ));
        $price->setLabel(__('Price'));
        
        $adminPercent = new Element\Text('admin_percent');
        $adminPercent->setAttributes(array(
            'id' => 'admin-percent',
            'class' => 'form-control validate[required]',
//             'required' => 'required'
        ));
        $adminPercent->setLabel(__('Admin Percent'));
        
        $confirm = new Element\Checkbox('confirm');
        $confirm->setAttributes(array(
            'id' => 'confirm',
            'class' => 'fform-control',
            //             'required' => 'required'
        ));
        $confirm->setLabel(__('Confirm'));
        
        $file = new Element\File('file');
        $file->setAttributes(array(
            'id' => 'file',
            'class' => 'form-control '
        ));
        $file->setLabel(__('File'));
        
        $description = new Element\Textarea('description');
        $description->setAttributes(array(
            'id' => 'shop-product-description',
            'class' => 'form-control shop-product-description'
        ));
        $description->setLabel(__('Description'));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__('Save'));
        $submit->setAttributes(array(
            'id' => 'submit',
            'class' => 'btn btn-success '
        ));
        $this->add($title)
            ->add($price)
            ->add($adminPercent)
            ->add($file)
            ->add($confirm)
            ->add($csrf)
            ->add($submit);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        
        
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }

    public function isValid()
    {
        $this->getInputFilter()->remove('gallery_id');
        return parent::isValid();
    }

}

