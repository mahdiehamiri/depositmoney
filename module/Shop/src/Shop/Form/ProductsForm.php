<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ProductsForm extends Form
{

    public function __construct($categoriesList)
    {
        parent::__construct('productsForm');
        $this->setAttributes(array(
            'action' => '',
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            'id' => 'productsForm',
            'novalidate' => true
        ));
        
        $productName = new Element\Text('product_name');
        $productName->setAttributes(array(
            'id' => 'product-name',
            'class' => 'form-control validate[required]',
            'required' => 'required'
        ));
        $productName->setLabel(__('Product Name'));
        
        $categoryId = new Element\Select('category_id');
        $categoryId->setAttributes(array(
		    'id' => 'category-id',
		    'class' => 'form-control category-id',

		));
		$categories = array(
		    "" => _("Please choose an item")
		);
		if ($categoriesList) {
		    foreach ($categoriesList as $category) {
		        $categories[$category['id']] = $category['title'];
		    }
		}
		$categoryId->setValueOptions($categories);
		$categoryId->setLabel(_("Category"));
        
        $price = new Element\Text('price');
        $price->setAttributes(array(
            'id' => 'amount_total',
            'class' => 'form-control form-control-mini validate[required] priceFormat',
            'required' => 'required'
        ));
        $price->setLabel(__('Price'));
        
        $discountPrice = new Element\Text('discount_price');
        $discountPrice->setAttributes(array(
            'id' => 'discount_price',
            'class' => 'form-control form-control-mini priceFormat',
//             'required' => 'required'
        ));
        $discountPrice->setLabel(__('قیمت تخفیف خورده'));
        
        $unit = new Element\Text('unit');
        $unit->setAttributes(array(
            'id' => 'product-unit',
            'class' => 'form-control ',
//             'required' => 'required'
        ));
        $unit->setLabel(__('Unit'));
        
        $minOrder = new Element\Text('minimum_order');
        $minOrder->setAttributes(array(
            'id' => 'minimum_order',
            'class' => 'form-control ',
//             'required' => 'required'
        ));
        $minOrder->setLabel(__('Minimum Order'));
        
        $discount = new Element\Text('discount');
        $discount->setAttributes(array(
            'id' => 'discount',
            'class' => 'form-control '
        ));
        $discount->setLabel(__('Discount'));
        
        $salePercent = new Element\Text('sale_percent');
        $salePercent->setAttributes(array(
            'id' => 'sale_percent',
            'class' => 'form-control '
        ));
        $salePercent->setLabel(__('Sale Percent'));
        
        $count = new Element\Text('count');
        $count->setAttributes(array(
            'id' => 'count',
            'class' => 'form-control '
        ));
        $count->setLabel(__('Count'));
        
        $image = new Element\File('image');
        $image->setAttributes(array(
            'id' => 'image',
            'class' => 'form-control '
        ));
        $image->setLabel(__('Image'));
        
        $description = new Element\Textarea('description');
        $description->setAttributes(array(
            'id' => 'shop-product-description',
            'class' => 'form-control shop-product-description'
        ));
        $description->setLabel(__('Description'));
        
        $csrf = new Element\Csrf('csrf');
        
        $submit = new Element\Submit('submit');
        $submit->setValue(__('Save'));
        $submit->setAttributes(array(
            'id' => 'submit',
            'class' => 'btn btn-primary'
        ));
        $this->add($productName)
            ->add($categoryId)
            ->add($price)
            ->add($unit)
            ->add($minOrder)
            ->add($discount)
            ->add($count)
            ->add($image)
            ->add($salePercent)
            ->add($description)
            ->add($discountPrice)
            ->add($csrf)
            ->add($submit);
        $this->inputFilter();
    }

    public function inputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                )
            )
        )));
        
        
        
        $this->setInputFilter($inputFilter);
        return $inputFilter;
    }

    public function isValid()
    {
        $this->getInputFilter()->remove('gallery_id');
        return parent::isValid();
    }
}

