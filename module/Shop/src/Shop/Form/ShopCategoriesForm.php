<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ShopCategoriesForm extends Form
{
	public function __construct($allActiveLanguages, $parentCategories = null, $attributeGroupsItems = null)
	{
		parent::__construct('shopCategoriesForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
    		    'id'      => 'shopCategoriesForm',
    		    'novalidate'=> true
		));
		
		$catLang = new Element\Select('cat_lang');
		$catLang->setAttributes(array(
		    'id' => 'cat_lang',
		    'class' => 'form-control validate[required]',
		    'required' => 'required'
		));
		$languages = array();
		if ($allActiveLanguages) {
		    foreach ($allActiveLanguages as $language) {
		        $languages[$language['code']] = $language['title'];
		    }
		}
		$catLang->setValueOptions($languages);
		$catLang->setLabel(__("Choose language"));
		
		$categoryType = new Element\Select('type');
		$categoryType->setAttributes(array(
		    'id'       => 'shop-category-type',
		    'class'    => 'form-control',
		    'required' => 'required'
		));
		$categoryType->setLabel(__('Attribute Type'));

		$categoryType->setValueOptions(array(
		    ''     => '',
		    'Real' => 'Real',
		    'Virtual' => 'Virtual'
		));

		
		$name = new Element\Text('name');
		$name->setAttributes(array(
				'id'    => 'shop-category-name',
				'class' => 'form-control validate[required]',
		        'required' => 'required'
		));
		$name->setLabel(__('Category name'));
		
		$parentId = new Element\Select('parent_id');
		$parentId->setAttributes(array(
				'id'    => 'shop-category-parent-id',
				'class' => 'form-control validate[required]',
		        'required' => 'required'
		));
		$parentId->setLabel(__('Parent category'));
		$categories = array(
				'0' => __('Main category')
		);
		if ($parentCategories) {
			foreach ($parentCategories as $category) {
				$categories[$category['id']] = $category['name'];
			}
		}
		$parentId->setValueOptions($categories);
		
		$attributeGroups = new Element\Select('attribute_groups');
		$attributeGroups->setAttributes(array(
				'id'       => 'shop-category-attribute-group-id',
				'class'    => 'form-control validate[required]',
				'multiple' => 'multiple',
		        'required' => 'required'
		));
		$attributeGroups->setLabel(__('Attribute groups'));
		$attributeGroupsOptions = array();
		if ($attributeGroupsItems) {
			foreach ($attributeGroupsItems as $item) {
				$attributeGroupsOptions[$item['id']] = $item['title'];
			}
			$attributeGroups->setValueOptions($attributeGroupsOptions);
		}
	
		$description = new Element\Textarea('description');
		$description->setAttributes(array(
		    'id'    => 'shop-category-description',
		    //'class' => 'form-control'
		));
		$description->setLabel(__('Description'));
		
		$status = new Element\Checkbox('status');
		$status->setAttributes(array(
				'id'    => 'shop-category-status',
				//'class' => 'form-control'
		));
		$status->setLabel(__('Status'));
		
		$icon = new Element\File('icon');
		$icon->setAttributes(array(
		    'id' => 'fileUpload1',
		));
		$icon->setLabel(__("Icon"));
		
		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__('Save and Close'));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		$submit2 = new Element\Submit('submitreturn');
		$submit2->setValue(__('Save and New'));
		$submit2->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));

		$this->add($catLang)
		     ->add($categoryType)
		     ->add($name)
		     ->add($submit2)
		     ->add($parentId)
		     ->add($attributeGroups)
		     ->add($description)
		     ->add($status)
		     ->add($icon)
		     ->add($csrf)
		     ->add($submit)
		     ->add($submit2);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				/* 'filters' => array(
						array(
								//'name' => 'StripTags'
						)
				), */
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'status',
				'required' => true,
				/* 'filters' => array(
						array(
								//'name' => 'StripTags'
						)
				), */
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'parent_id',
				'required' => true,
				/* 'filters' => array(
						array(
								//'name' => 'StripTags'
						)
				), */
		)));
		$inputFilter->add($factory->createInput(array(
		    'name'     => 'type',
		    'required' => false,
		    'allow_empty' => true,
		    /* 'filters' => array(
		     array(
		         //'name' => 'StripTags'
		     )
		    ), */
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'attribute_groups',
				'required' => false,
		        'allow_empty' => true,
				/* 'filters' => array(
						array(
								//'name' => 'StripTags'
						)
				), */
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
	
	public function isValid()
	{
		$this->getInputFilter()->remove('attribute_groups');
		return parent::isValid();
	}
}
