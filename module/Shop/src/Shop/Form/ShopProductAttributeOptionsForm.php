<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ShopProductAttributeOptionsForm extends Form
{
	public function __construct()
	{
		parent::__construct('shopProductAttributeOptionsForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post'
		));
		
		$values = new Element\Textarea('values');
		$values->setAttributes(array(
				'id'    => 'shop-attribute-values',
				'class' => 'form-control'
		));
		$values->setLabel('مقادیر');

		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue('ثبت');
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));

		$this->add($values)
		     ->add($csrf)
		     ->add($submit);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'values',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
