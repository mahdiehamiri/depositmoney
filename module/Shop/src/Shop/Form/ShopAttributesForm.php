<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ShopAttributesForm extends Form
{
	public function __construct($attributeGroupsItems = null, $cat_id = null)
	{
		parent::__construct('shopAttributesForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
    		    'id'      => 'shopAttributesForm',
    		    'novalidate'=> true
		));
		
		$title = new Element\Text('title');
		$title->setAttributes(array(
				'id'    => 'shop-attribute-title',
				'class' => 'form-control  validate[required]',
		        'required' => 'required'
		));
		$title->setLabel(__('Attribute name'));
		
		$kind = new Element\Select('kind');
		$kind->setAttributes(array(
				'id'    => 'shop-attribute-type',
				'class' => 'form-control  validate[required]',
				'required' => 'required'
		));
		$typs = array(
				0     => __("Int"),
				1     => __("Varchar"), 
		
		);
		$kind->setValueOptions($typs);
		$kind->setLabel(__('Attribute Kind intiget or character'));
		
		$titleEn = new Element\Text('title_en');
		$titleEn->setAttributes(array(
				'id'    => 'shop-attribute-title-en',
				'class' => 'form-control validate[required]',
		        'required' => 'required'
		));
		$titleEn->setLabel(__('English name'));
		
		$developerName = new Element\Text('developer_name');
		$developerName->setAttributes(array(
				'id'    => 'shop-attribute-developer-name',
				'class' => 'form-control validate[required]',
		        'required' => 'required'
		));
		$developerName->setLabel(__('Machine name (English)'));		
		
		$type = new Element\Select('type');
		$types = array(
				'text'         => __("text"),
				'textarea'     => __("textarea"),
				'radio-button' => __("radio button"),
				'select'       => __("select box"),
				'image_file'   => __("image_file"),
				'public_file'  => __("public_file")
				
		);
		$type->setValueOptions($types);
		$type->setAttributes(array(
				'id'    => 'shop-attribute-type',
				'class' => 'form-control validate[required]',
		        'required' => 'required'
		));
		$type->setLabel(_('Attribute type'));
		
		$attributeGroups = new Element\Select('attribute_groups');
		$attributeGroups->setAttributes(array(
				'id'       => 'shop-attributes-group-id',
				'class'    => 'form-control validate[required]',
				'multiple' => 'multiple',
		        'required' => 'required'
		));
		$attributeGroups->setLabel(__('attributes '));
		$attributeGroupsOptions = array();
		if ($attributeGroupsItems) {
			foreach ($attributeGroupsItems as $item) {
				$attributeGroupsOptions[$item['id']] = $item['title'];
			}
			$attributeGroups->setValueOptions($attributeGroupsOptions);
		}
		$attributeGroups->setValue($cat_id);
		
		$isForced = new Element\Checkbox('is_forced');
		$isForced->setAttributes(array(
				'id' => 'shop-attributes-is-forced',
				//'class' => 'form-control'
		));
		$isForced->setLabel(__('Required'));
		
		$status = new Element\Checkbox('status');
		$status->setAttributes(array(
				'id'    => 'shop-category-status',
				//'class' => 'form-control'
		        
		));
		$status->setLabel(__('Status')); 
		
		$order = new Element\Text('order');
		$order->setAttributes(array(
		    'id'    => 'order',
		    'class' => 'form-control',
		    //'required' => 'required'
		));
		$order->setLabel(__('Sort'));
		
		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__('Save and Close'));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		$submit2 = new Element\Submit('submitreturn');
		$submit2->setValue(__('Save and New'));
		$submit2->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		$submit2 = new Element\Submit('submitreturn');
		$submit2->setValue(__('Save and New'));
		$submit2->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		$this->add($developerName)
			 ->add($title)
		     ->add($titleEn)
		     ->add($kind)
		     ->add($submit2)
		     ->add($type)
		     ->add($attributeGroups)
		     ->add($isForced)
		     ->add($status)
		     ->add($order)
		     ->add($csrf)
		     ->add($submit2)
		     ->add($submit);
		//$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'developer_name',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));		
		$inputFilter->add($factory->createInput(array(
				'name'     => 'title',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'kind',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'title_en',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'type',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'is_forced',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'status',
				'required' => false,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'attribute_groups',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
}
