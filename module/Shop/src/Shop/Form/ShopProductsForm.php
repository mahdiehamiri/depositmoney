<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ShopProductsForm extends Form
{
	public function __construct($allActiveLanguages, $cat_id = null, $type = "Real", $instructorList = null, $studiesList = null, $lessonsList = null, $relatedProductsList = null, $galleries = null, $editMode = false)
	{
		parent::__construct('shopProductsForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
		        'enctype' => 'multipart/form-data',
    		    'id'      => 'shopProductsForm',
    		    'novalidate'=> true
		));
		
		$productLang = new Element\Select('product_lang');
		$productLang->setAttributes(array(
		    'id' => 'product_lang',
		    'class' => 'form-control validate[required]',
		    'required' => 'required'
		));
		$languages = array();
		if ($allActiveLanguages) {
		    foreach ($allActiveLanguages as $language) {
		        $languages[$language['code']] = $language['title'];
		    }
		}
		$productLang->setValueOptions($languages);
		$productLang->setLabel(__("Choose language")); 
		
		$name = new Element\Text('name');
		$name->setAttributes(array(
				'id'    => 'shop-product-name',
				'class' => 'form-control validate[required]',
		        'required' => 'required'
		));
		$name->setLabel(__('name'));		
		
		$seoName = new Element\Text('seo_name');
		$seoName->setAttributes(array(
				'id'    => 'shop-product-seo-name',
				'class' => 'form-control ',
		        'required' => 'required'
		));
		$seoName->setLabel(__('Seo name'));
		
		$categoryId = new Element\Hidden('category_id');
		$categoryId->setValue($cat_id);

		if($type == "Virtual") {
		    
		    $instructors = new Element\Select('instructor_id');
		    $instructors->setLabel(__("Instructors"));
		    $instructors->setAttributes(array(
		        'id'          => 'instructor_id',
		        'class'       => 'form-control validate[required] ',
		        'required' => 'required'
		    ));
		    $instructorListArray = array();
		    foreach ($instructorList as $ins){
		        $instructorListArray[$ins['id']] = $ins['username'];
		    }
		    $instructors->setValueOptions($instructorListArray);
		    
		    $instructorDiscount = new Element\Text('instructor_discount');
		    $instructorDiscount->setAttributes(array(
		        'id'    => 'shop-product-instructor-discount',
		        'class' => 'form-control'
		    ));
		    $instructorDiscount->setLabel(__('Instructor discount'));

		    $studies = new Element\Select('study_id');
		    $studies->setLabel(__("Studies"));
		    $studies->setAttributes(array(
		        'id'          => 'study_id',
		        'class'       => 'form-control validate[required] ',
		        'required' => 'required'
		    ));

		    foreach ($studiesList as $study){
		        $studiesListArray[$study['id']] = $study['name'];
		    }
		    $studies->setValueOptions($studiesListArray);
		     
		    $lessons = new Element\Select('lesson_id');
		    $lessons->setLabel(__("Lessons"));
		    $lessons->setAttributes(array(
		        'id'          => 'lesson_id',
		        'class'       => 'form-control validate[required] ',
		        'required' => 'required'
		    ));

		    foreach ($lessonsList as $lesson){
		        $lessonsListArray[$lesson['id']] = $lesson['name'];
		    }
		    $lessons->setValueOptions($lessonsListArray);
		    
		  
		    
		    $relatedProducts = new Element\Select('related_products');
		    $relatedProducts->setAttributes(array(
		        'id'    => 'related-products',
		        'class' => 'form-control',
		        'multiple' => true,
		    ));
		    $relatedProducts->setLabel(__("Related products"));
		    $relatedProducts->setValueOptions($relatedProductsList);
		    
		    $this->add($instructors);
		    $this->add($instructorDiscount);
		    $this->add($studies);
		    $this->add($lessons);
		    
		    $this->add($relatedProducts);
		    
		}
		
		 $file = new Element\File('file');
		$file->setLabel(__("Technical schema"));
		$this->add($file);
		 
		$price = new Element\Text('price');
		$price->setAttributes(array(
		    'id'    => 'amount_total',
		    'class' => 'form-control form-control-mini validate[required] priceFormat',
		    'required' => 'required'
		));
		$price->setLabel(__('Price (Toman)'));
		
		$image = new Element\File('image');
		if (!$editMode) {
		    $image->setAttributes(array(
		        'class' => 'validate[required]',
		        'required' => 'required',
		        'id' => 'fileUpload1'
		    ));
		}else{
		    $image->setAttributes(array(
		        'id' => 'fileUpload1'
		    ));
		}
		$image->setLabel(__("Image"));
		
		$galleryId = new Element\Select('gallery_id');
		$galleryId->setAttributes(array(
		    'class' => 'form-control',
		    'id'    => 'gallery_id'
		));
		$galleriesArray[""] = __("Choose gallery");
		if ($galleries) {
		    foreach ($galleries as $gallery) {
		        $galleriesArray[$gallery['id']] = $gallery['latin_name'];
		    }
		}
		$galleryId->setValueOptions($galleriesArray);
		$galleryId->setLabel(__("Gallery"));
		
		$priority = new Element\Text('priority');
		$priority->setAttributes(array(
				'id'    => 'shop-product-priority',
				'class' => 'form-control '
		));
		$priority->setLabel(__('Priority'));
		
		 $rating = new Element\Text('rating');
		$rating->setAttributes(array(
		    'id'    => 'shop-product-rating',
		    'class' => 'form-control'
		));
		$rating->setLabel(__('Guaranty months')); 
		
		$metaDescription = new Element\Textarea('meta_description');
		$metaDescription->setAttributes(array(
				'id' => 'shop-product-meta-description',
				'class' => 'form-control'
		));
		$metaDescription->setLabel(__('Meta descriptions'));
		
		$metaKeywords = new Element\Textarea('meta_keywords');
		$metaKeywords->setAttributes(array(
				'id' => 'shop-product-meta-keywords',
				'class' => 'form-control'
		));
		$metaKeywords->setLabel(__('Meta keywords'));
		
		$status = new Element\Checkbox('status');
		$status->setAttributes(array(
				'id'    => 'shop-product-status',
				'class' => ''
		));
		$status->setLabel(__('Status'));
		
		$descriptionAdditional = new Element\Textarea('description_additional');
		$descriptionAdditional->setAttributes(array(
		    'id' => 'shop-product-description',
		    'class' => 'form-control shop-product-description'
		));
		$descriptionAdditional->setLabel(__('Decsriptional additional'));
		
		$description = new Element\Textarea('description');
		$description->setAttributes(array(
				'id' => 'shop-product-description',
				'class' => 'form-control shop-product-description'
		));
		$description->setLabel(__('Description'));

		$csrf = new Element\Csrf('csrf');
	//	$csrf->setOptions('timeout', 180000000);
		
		$submit = new Element\Submit('submit');
		$submit->setValue(__('Save'));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		$submit2 = new Element\Submit('submitreturn');
		$submit2->setValue(__('Save and New'));
		$submit2->setAttributes(array(
				'id'    => 'submitreturn',
				'class' => 'btn btn-primary'
		));
		
		
		$this->add($productLang)
		     ->add($submit2)
		     ->add($name)
		     ->add($categoryId)
		     ->add($priority) 
		     ->add($image)
 		     ->add($galleryId)
		     ->add($seoName)
		     ->add($metaDescription)
		     ->add($metaKeywords)
		     ->add($status)
		     ->add($description) 
		     ->add($csrf)
		     ->add($submit);
		$this->inputFilter($editMode, $type);
	}
	
	public function inputFilter($editMode, $type)
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
 
		$inputFilter->add($factory->createInput(array(
				'name'     => 'seo_name',
				'required' => true,
				'filters' => array(
						array(
								'name' => 'StripTags'
						)
				),
		)));
 
		if (!$editMode) {
		    $inputFilter->add($factory->createInput(array(
		        'name'     => 'image',
		        'required' => true,
		    )));		     
		}
		
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
	public function isValid() {
	    $this->getInputFilter ()->remove ( 'gallery_id' );
	    return parent::isValid ();
	}
}

