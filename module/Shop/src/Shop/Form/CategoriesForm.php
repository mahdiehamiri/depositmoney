<?php

namespace Shop\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class CategoriesForm extends Form
{
	public function __construct()
	{
		parent::__construct('categoriesForm');
		$this->setAttributes(array(
				'action' => '',
				'method' => 'post',
    		    'id'      => 'categoriesForm',
		         'enctype' =>  "multipart/form-data",
    		    'novalidate'=> true
		));

		$title = new Element\Text('title');
		$title->setAttributes(array(
				'id'    => 'title',
				'class' => 'form-control validate[required]',
		        'required' => 'required'
		));
		$title->setLabel(__('Category title'));
		
		$status = new Element\Checkbox('status');
		$status->setAttributes(array(
				'id'    => 'shop-category-status',
		));
		$status->setLabel(__('Status'));
		
		$csrf = new Element\Csrf('csrf');

		$submit = new Element\Submit('submit');
		$submit->setValue(__('Save and Close'));
		$submit->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));
		$submit2 = new Element\Submit('submitreturn');
		$submit2->setValue(__('Save and New'));
		$submit2->setAttributes(array(
				'id'    => 'submit',
				'class' => 'btn btn-primary'
		));

		$this->add($title)
		     ->add($status)
		     ->add($csrf)
		     ->add($submit)
		     ->add($submit2);
		$this->inputFilter();
	}
	
	public function inputFilter()
	{
		$inputFilter = new InputFilter();
		$factory     = new InputFactory();
		$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				/* 'filters' => array(
						array(
								//'name' => 'StripTags'
						)
				), */
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'status',
				'required' => true,
				/* 'filters' => array(
						array(
								//'name' => 'StripTags'
						)
				), */
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'parent_id',
				'required' => true,
				/* 'filters' => array(
						array(
								//'name' => 'StripTags'
						)
				), */
		)));
		$inputFilter->add($factory->createInput(array(
		    'name'     => 'type',
		    'required' => false,
		    'allow_empty' => true,
		    /* 'filters' => array(
		     array(
		         //'name' => 'StripTags'
		     )
		    ), */
		)));
		$inputFilter->add($factory->createInput(array(
				'name'     => 'attribute_groups',
				'required' => false,
		        'allow_empty' => true,
				/* 'filters' => array(
						array(
								//'name' => 'StripTags'
						)
				), */
		)));
		$this->setInputFilter($inputFilter);
		return $inputFilter;
	}
	
	public function isValid()
	{
		$this->getInputFilter()->remove('attribute_groups');
		$this->getInputFilter()->remove('pdf[]');
		$this->getInputFilter()->remove('video[]');
		$this->getInputFilter()->remove('picture[]');
		return parent::isValid();
	}
}
