<?php
namespace Shop\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Application\Services\BaseModel;

class ProductsTable extends BaseModel 
{
	protected $tableGateway;
	protected $serviceManager;
	public $adapter;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->adapter = $this->getDbAdapter();
		$this->tableGateway = new TableGateway('products', $this->adapter);
	}
	
	
	public function add($userId,$data)
	{
	    if($data['sale_percent']){
	        $salePercent = $data['sale_percent'];
	    }else {
	        $salePercent = ' ';
	    }
	  
	    $this->tableGateway->insert(array(
	        'user_id'             => $userId,
	        'category_id'             => $data['category_id'],
	        'product_name'           => $data['product_name'],
	        'price'         => $data['price'],
	        'discount_price'         => $data['discount_price'],
	        'unit'        => $data['unit'],
	        'minimum_order'        => $data['minimum_order'],
	        'discount'        => $data['discount'],
	        'count'        => $data['count'],
	        'image'        => $data['image'],
	        'description'        => $data['description'],
	        'status'        => $data['status'],
	        'sale_percent'        => $salePercent,
	        'created'        => date('Y/m/d H:i:s'),
	        
	    ));
	    return $this->tableGateway->getLastInsertValue();
    }
	

	
	public function fetchAll()
	{
	    
	    $select = new Select('products');
	    $select->join("users", "users.id = products.user_id", array(
	    "user_group_id" ,
	    ), $select::JOIN_LEFT);
 	   $select->where->in('user_group_id', array('1','2'));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchByGroup($group)
	{
	    
	    $select = new Select('products');
	    $select->join("users", "users.id = products.user_id", array(
	        "user_group_id",
	        "username" 
	    ), $select::JOIN_LEFT);
	    $select->join("user_groups", "user_groups.id = users.user_group_id", array(
	        "title",
	    ), $select::JOIN_LEFT);
	    $select->where->in('user_group_id', $group);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchById($productId)
	{
	    $select = new Select('products');
        $select->where->equalTo('id', $productId);
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function fetchByUser($user)
	{
	    $select = new Select('products');
	    $select->where->equalTo('user_id', $user);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function update($id,$data)
	{
	     $this->tableGateway->update(array(
	        'product_name'           => $data['product_name'],
	        'category_id'             => $data['category_id'],
	        'price'         => $data['price'],
	        'discount_price'         => $data['discount_price'],
	        'unit'        => $data['unit'],
	        'minimum_order'        => $data['minimum_order'],
	        'discount'        => $data['discount'],
	        'count'        => $data['count'],
	        'image'        => $data['image'],
	        'description'        => $data['description'],
	    ), array(
	        'id' => $id
	    ));
	    return true;
	}
	
	public function delete($id)
	{
	    return $this->tableGateway->delete(array(
	        'id' => $id
	    ));
	}
    
	public function getByCategory($categoryId){
	    $select = new Select('products');
	    $select->join("users", "users.id = products.user_id", array(
	        "user_group_id" ,
	    ), $select::JOIN_LEFT);
	    $select->where->in('products.category_id', $categoryId);
	    $select->where->equalTo('products.status', 'show');
	    $select->where->notEqualTo('user_group_id', '7');
	    $select->order('id DESC');
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function searchByName($name){
	    $select = new Select('products');
// 	    $select->where->in('category_id', $categoryId);
	    $select->where->like('product_name', '%' . $name . '%');
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function searchByFilter($filter,$categoryId){
	    $select = new Select('products');
	    $select->where->in('category_id', $categoryId);
	    if($filter == 1){
	        $select->order('id DESC');
	    }elseif($filter == 0){
	        $select->order('id ASC');
	    }elseif($filter == 2){
// 	        $select->order('price ASC');
	        $select->order('discount_price ASC');
	    }elseif($filter == 3){
// 	        $select->order('price DESC');
	        $select->order('discount_price DESC');
	    }
	    $select->where->equalTo('status', 'show');
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function updateStatus($status, $id)
	{
	    return $this->tableGateway->update(array('status' => $status),  array('id' => $id));
	}
	
//     public function getOrders($where = null)
//     {
//         $select = new Select('orders');
//         $select->order("orders.id desc");
//         $select->join("users", "users.id = orders.user_id", array("email"), $select::JOIN_LEFT);
//         $select->join("banks", "banks.id = orders.bank_id",array("bank_name" => "name"), $select::JOIN_LEFT);
//         $select->join("payments", "payments.order_id = orders.id",array("payment_status" => "status"), $select::JOIN_LEFT);
//         $select->join("transmitalway", "transmitalway.name = orders.transmital_way",array("transmital_name" => "display_name"), $select::JOIN_LEFT);
        
//         if ($where) {
//             $select->where($where);
//         }
//         return $select;
//     }

    
    
//     public function changeOrderPaymentStatus($orderId , $status)
//     {
//         $data = array(
//             'payment_status' => $status
//         );
//         return $this->tableGateway->update($data , array('id = ?' => $orderId));
//     }
    
//     public function changeOrderStatus($orderId , $status, $refCode = false)
//     {
//         $data = array(
//                 'order_status' => $status 
//         ); 
//         if ($refCode) {
//             $data["ref_code"] = $refCode;
//         }
//         return $this->tableGateway->update($data , array('id = ?' => $orderId));
//     }

//     public function addOrder( $userId, $container)
//     {
//         $userIp = getenv('HTTP_CLIENT_IP')?:
//         getenv('HTTP_X_FORWARDED_FOR')?:
//         getenv('HTTP_X_FORWARDED')?:
//         getenv('HTTP_FORWARDED_FOR')?:
//         getenv('HTTP_FORWARDED')?:
//         getenv('REMOTE_ADDR');
//         $transmitalWay = $container->transmitalWay;
//         $data = array(
//                 'user_id'               => $userId ,
//                 'ip'                    => $userIp,
//                 'amount'                => $container->totalSum ,
//                 'bank_id'               => $container->paymentWayBank,
//                 'transmital_way'        => $transmitalWay["name"],
//                 'transmital_way_amount' => $transmitalWay["amount"],
//                 'total_amount'          => $container->totalSum + $transmitalWay["amount"],
//                 'added_date'            => new Expression("CURDATE()"),
//                 'added_time'            => new Expression("CURTIME()"),
//                 'customer_firstname'    => $container->firstName,
//                 'customer_lastname'     => $container->lastName,
//                 'customer_country'      => $container->country,
//                 'customer_province'     => $container->province,
//                 'customer_city'         => $container->city,
//                 'customer_phone'        => $container->phone,
//                 'customer_mobile'       => $container->mobile,
//                 'customer_address'      => $container->address,
//                 'customer_postalcode'   => $container->postalcode,
            
//         );
//         $this->tableGateway->insert($data);
//         return  $this->tableGateway->lastInsertValue;       
//     }

//     public function deleteOrder($orderId)
//     {
//         return $this->tableGateway->delete(array(
//             'id' => $orderId
//         ));
//     }
}