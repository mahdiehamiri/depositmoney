<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ShopCategoriesTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_categories', $this->getDbAdapter());
	}

	public function fetchAllByParentId($where)
	{
	    $select = new Select('shop_categories');
	    if ($where) {
	        $select->where($where);
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchCategory($id = null)
	{
	    $select = new Select('shop_categories');
	   
	    return $this->tableGateway->select(array("id" => $id))->toArray();
	}
	
	public function fetchAllRealCategories($where = false)
	{
	    $select = new Select('shop_categories');
	    if ($where) {
	        $select->where($where);
	    }
	    $select->where->equalTo("type", "Real");
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchAllList($flag = false)
	{
		$select = new Select('shop_categories');
		///$select->where->equalTo('parent_id', 0);
		$select->order(new Expression('case parent_id when 0 then id*1000 else parent_id*1000+id end,id'));
        if($flag){
            $select->columns(array('id', 'parent_id', 'name'));
            return $this->tableGateway->selectWith($select)->toArray();
        }else{
            return $select;
        }
	}
	
	public function fetchAllChildsList($parentId)
	{
	    $select = new Select('shop_categories');
	    $select->where->equalTo('parent_id', $parentId);
	    return $select;
	}
	
    public function fetchAll($justParents = null, $cagegoryId = null)
	{
		$select = new Select('shop_categories');
		if ($justParents) {
			$select->where->equalTo('parent_id', 0);
		} elseif($cagegoryId) {
			$select->where->equalTo('id', $cagegoryId);
			$select->join('shop_attribute_groups_category_index', 'shop_attribute_groups_category_index.shop_category_id = shop_categories.id',
					array("attribute_groups" => new Expression("Group_Concat(shop_attribute_groups_category_index.attribute_group_id)")),"LEFT"); 
		}
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function deleteCategoryIcon($categoryId)
	{
	    if ($categoryId) {
	        $newData = array(
	            'icon'            => null,
	        );
	        return $this->tableGateway->update($newData, array('id' => $categoryId));
	    }
	}
	
	public function getParentByLang($lang = "fa")
	{
	   
	    return $this->tableGateway->select(array(
	        'parent_id' => 0,
	        'status' => 1,
	        'cat_lang' => $lang,
	        
	    ))->toArray();
	}
	
	public function getChildren($categoryId, $paginator = false, $lang = "fa")
	{
	    
	    if ($paginator) {
	        $select = new Select('shop_categories');
	        $select->where->equalTo('parent_id', $categoryId);
	        $select->where->equalTo('cat_lang', $lang);
	        $select->where->equalTo('status', 1);
	        $paginatorAdapter = new DbSelect($select,
	            $this->tableGateway->getAdapter());
	        $paginator = new Paginator($paginatorAdapter);
	        return $paginator;
	    }
	    
		return $this->tableGateway->select(array(
				'parent_id' => $categoryId,
		))->toArray();
	}

	public function isExist($validData, $categoryId = null)
	{
		$select = new Select('shop_categories');
		if ($categoryId) {
			$select->where(array(
					'name' =>$validData['name'],
					'id != ?' => $categoryId
			));			
		} else {
			$select->where(array(
					'name' => $validData['name']
			));
		}
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function addCategory($validData)
	{ 
		$this->tableGateway->insert(array(
				'name'             => $validData['name'],
				'status'           => $validData['status'],
		        'cat_lang'         => $validData['cat_lang'],
				'parent_id'        => $validData['parent_id'],
		        'type'        => $validData['type'],
		        'description'        => $validData['description'],
		        'icon'        => $validData['icon'],
		    
		));
		return $this->tableGateway->getLastInsertValue();
	}
	
	public function updateCategory($validData, $categoryId)
	{
		if ($categoryId > 0 && is_numeric($categoryId)) {
			$newData = array(
				'name'             => $validData['name'],
				'status'           => $validData['status'],
			    'cat_lang'         => $validData['cat_lang'],
				'parent_id'        => $validData['parent_id'],
			    'type'        => $validData['type'],
			    'description'        => $validData['description'],
			);
			if (isset($validData['icon'])) {
			    $newData["icon"] = $validData['icon'];
			}
			return $this->tableGateway->update($newData, array('id' => $categoryId));
		}
	}
	
	public function deleteCategory($categoryId)
	{
		$select = new Select('shop_categories');
		$select->join('shop_attribute_groups_category_index', 'shop_attribute_groups_category_index.shop_category_id=shop_categories.id', 
				array('*'), 'left');
		$isUsed = $select->where->equalTo('shop_attribute_groups_category_index.shop_category_id', $categoryId);

		if ($categoryId > 0 && is_numeric($categoryId)) {
			return $this->tableGateway->delete(array(
					'id' => $categoryId
			));
		}
	}
}
