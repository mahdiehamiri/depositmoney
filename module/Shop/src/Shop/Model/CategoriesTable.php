<?php
namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class CategoriesTable extends BaseModel
{

    protected $tableGateway;

    protected $serviceManager;

    protected $tableCategoryFile;

    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('categories', $this->getDbAdapter());
    }

    public function addCategory($validData)
    {
        $this->tableGateway->insert(array(
            'title' => $validData['title'],
            'status' => $validData['status']
        ));
        return $this->tableGateway->getLastInsertValue();
    }

    public function fetchAll()
    {
        $select = new Select('categories');
        $select->where->notEqualTo('status', '2');
        return $this->tableGateway->selectWith($select)->toArray();
    }

    public function getById($id)
    {
        $select = new Select('categories');
        $select->where->equalTo('id', $id);
        return $this->tableGateway->selectWith($select)->current();
        
    }
    
    public function editCategory($data,$id){
        return $this->tableGateway->update(array(
            'title' => $data['title'],
            'status' => $data['status'],
        ), array(
            'id' => $id
        ));
    }
    
    public function delete($id)
    {
        return $this->tableGateway->update(array(
            'status' => '2',
        ), array(
            'id' => $id
        ));
    }
}
