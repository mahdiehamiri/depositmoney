<?php
namespace Shop\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Application\Services\BaseModel;

class OrdersTable extends BaseModel 
{
	protected $tableGateway;
	protected $serviceManager;
	public $adapter;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->adapter = $this->getDbAdapter();
		$this->tableGateway = new TableGateway('orders', $this->adapter);
	}

	
	public function getRecords($where = null)
	{
	    return $this->tableGateway->select($where)->toArray();
	}

    public function getOrders($where = null)
    {
        $select = new Select('orders');
        $select->order("orders.id desc");
        $select->join("users", "users.id = orders.user_id", array("email"), $select::JOIN_LEFT);
        $select->join("banks", "banks.id = orders.bank_id",array("bank_name" => "name"), $select::JOIN_LEFT);
        $select->join("payments", "payments.order_id = orders.id",array("payment_status" => "status"), $select::JOIN_LEFT);
        $select->join("transmitalway", "transmitalway.name = orders.transmital_way",array("transmital_name" => "display_name"), $select::JOIN_LEFT);
        
        if ($where) {
            $select->where($where);
        }
        return $select;
    }

    
    
    public function changeOrderPaymentStatus($orderId , $status)
    {
        $data = array(
            'payment_status' => $status
        );
        return $this->tableGateway->update($data , array('id = ?' => $orderId));
    }
    
    public function changeOrderStatus($orderId , $status, $refCode = false)
    {
        $data = array(
                'order_status' => $status 
        ); 
        if ($refCode) {
            $data["ref_code"] = $refCode;
        }
        return $this->tableGateway->update($data , array('id = ?' => $orderId));
    }

    public function addOrder( $userId, $container)
    {
        $userIp = getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
        getenv('HTTP_FORWARDED_FOR')?:
        getenv('HTTP_FORWARDED')?:
        getenv('REMOTE_ADDR');
        $transmitalWay = $container->transmitalWay;
        $data = array(
                'user_id'               => $userId ,
                'ip'                    => $userIp,
                'amount'                => $container->totalSum ,
                'bank_id'               => $container->paymentWayBank,
                'transmital_way'        => $transmitalWay["name"],
                'transmital_way_amount' => $transmitalWay["amount"],
                'total_amount'          => $container->totalSum + $transmitalWay["amount"],
                'added_date'            => new Expression("CURDATE()"),
                'added_time'            => new Expression("CURTIME()"),
                'customer_firstname'    => $container->firstName,
                'customer_lastname'     => $container->lastName,
                'customer_country'      => $container->country,
                'customer_province'     => $container->province,
                'customer_city'         => $container->city,
                'customer_phone'        => $container->phone,
                'customer_mobile'       => $container->mobile,
                'customer_address'      => $container->address,
                'customer_postalcode'   => $container->postalcode,
            
        );
        $this->tableGateway->insert($data);
        return  $this->tableGateway->lastInsertValue;       
    }

    public function deleteOrder($orderId)
    {
        return $this->tableGateway->delete(array(
            'id' => $orderId
        ));
    }
}