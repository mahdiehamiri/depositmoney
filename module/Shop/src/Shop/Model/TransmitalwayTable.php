<?php 
namespace Shop\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class TransmitalwayTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('transmitalway', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getTransmitalway($transmitalWayName)
	{
	    $where =  new Where();
	    $where->equalTo("name", $transmitalWayName);
	    $where->equalTo("enable", "1");
	    return $this->tableGateway->select($where)->toArray();
	}
	
	public function getAllTransmitalways($where = null, $enabled = true)
	{
	    $select = new Select('transmitalway');
	    if (!$where) {
	        $where =  new Where();
	    }
	    if ($enabled) {
	        $where->equalTo("enable", "1");
	    }
	    $select->where($where);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	

	
}
