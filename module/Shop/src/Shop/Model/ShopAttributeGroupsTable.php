<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class ShopAttributeGroupsTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_attribute_groups', $this->getDbAdapter());
	}
	
	public function fetchAtributeList($id = null)
	{
	    $select = new Select('shop_attribute_groups');
	    if ($id) {
	        $select->where->equalTo('id', $id);
	        
	        $select->join('shop_attribute_group_index', 'shop_attribute_group_index.attribute_group_id = shop_attribute_groups.id',
	            array("attributeList"=>new Expression("Group_Concat(shop_attribute_group_index.attribute_id)")),"LEFT");
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchAttributeGroupsList($attributeGroupId = null, $isActive = null)
	{
		$select = new Select('shop_attribute_groups');
		if ($attributeGroupId) {
			$select->where(array(
					'id = ?' => $attributeGroupId
			));
		} elseif($isActive) {
			$select->where(array(
					'status = ?' => 1
			));
		}
		//return $this->tableGateway->selectWith($select)->toArray();
		return $select;
	}
	
	public function fetchAttributeGroups($attributeGroupId = null, $isActive = null)
	{
	    $select = new Select('shop_attribute_groups');
	    if ($attributeGroupId) {
	        $select->where(array(
	            'id = ?' => $attributeGroupId
	        ));
	    } elseif($isActive) {
	        $select->where(array(
	            'status = ?' => 1
	        ));
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function isExist($title, $attributeGroupId = null)
	{
		$select = new Select('shop_attribute_groups');
		if ($attributeGroupId) {
			$select->where(array(
					'title = ?' => $title,
					'id != ?' => $attributeGroupId
			));			
		} else {
			$select->where(array(
					'title = ?' => $title,
			));
		}
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function add($validData)
	{
		return $this->tableGateway->insert(array(
				'title'  => $validData['title'],
				'status' => $validData['status']
 		));
	}
	
	public function update($validData, $attributeGroupId)
	{
		return $this->tableGateway->update(array(
				'title'  => $validData['title'],
				'status' => $validData['status']
		), array(
				'id' => $attributeGroupId
		));
	}
	
	public function checkCategoryIsUsed($categoryId)
	{
		$select = new Select('shop_attribute_groups');
		$select->join('shop_attribute_groups_category_index', 'shop_attribute_groups_category_index.attribute_group_id = shop_attribute_groups.id',
				array('*'), 'left');
		$select->where->equalTo('shop_attribute_groups_category_index.attribute_group_id', $categoryId);
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	
	public function delete($id)
	{
		if ($id > 0 && is_numeric($id)) {
			return $this->tableGateway->delete(array(
					'id' => $id
			));
		}
	}
}
