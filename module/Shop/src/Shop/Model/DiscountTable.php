<?php 
namespace Shop\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class DiscountTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'discount';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('discount', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getDiscountById($id)
	{
	    $select = new Select($this->table);
	    $select->where(array(
	        'id' => $id
	    ));
	    //return $select;
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	

	
	public function getAllCurrentDiscounts($where = null)
	{
	    $select = new Select($this->table);
	    if (!$where) {
	        $where = new Where();
	    }
	    $where->nest()->lessThanOrEqualTo("from_date", new Expression('CURDATE()'))->and->greaterThanOrEqualTo("to_date", new Expression('CURDATE()'))->unnest();
        $select->where($where);	   
        $select->order("discount desc");   
        $all = $this->tableGateway->selectWith($select)->toArray();
        if ($all) {
            return $all[0]["discount"];
        } else {
            return 0;
        }
	}
	

}
