<?php
namespace Shop\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Application\Services\BaseModel;

class DesignerSampleTable extends BaseModel 
{
	protected $tableGateway;
	protected $serviceManager;
	public $adapter;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->adapter = $this->getDbAdapter();
		$this->tableGateway = new TableGateway('designer_samples', $this->adapter);
	}
	
	
	public function add($userId,$data)
	{

	    $this->tableGateway->insert(array(
            'user_id'   => $userId,
	        'title'    => $data['title'],
	        'file'    => $data['file'],
	        'price'    => $data['price'],
	    ));
	    return $this->tableGateway->getLastInsertValue();
    }
	

	
	public function fetchAll()
	{
	    
	    $select = new Select('products');
	    $select->join("users", "users.id = products.user_id", array(
	    "user_group_id" ,
	    ), $select::JOIN_LEFT);
 	   $select->where->in('user_group_id', array('1','2'));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	
	
	public function fetchById($productId)
	{
	    $select = new Select('designer_samples');
        $select->where->equalTo('id', $productId);
	    return $this->tableGateway->selectWith($select)->current();
	}
	
	public function fetchByUser($user)
	{
	    $select = new Select('designer_samples');
	    $select->where->equalTo('user_id', $user);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function update($id,$data)
	{
	     $this->tableGateway->update(array(
	         'title'    => $data['title'],
	         'file'    => $data['file'],
	         'price'    => $data['price'],
	         'admin_percent'    => $data['admin_percent'],
	         'confirm'    => $data['confirm'],
	    ), array(
	        'id' => $id
	    ));
	    return true;
	}


	public function updateByUser($id,$data)
	{
	     $this->tableGateway->update(array(
	         'title'    => $data['title'],
	         'file'    => $data['file'],
	         'price'    => $data['price'],
	        //  'admin_percent'    => $data['admin_percent'],
	         'confirm'    => 0,
	    ), array(
	        'id' => $id
	    ));
	    return true;
	}


	
	public function delete($id)
	{
	    return $this->tableGateway->delete(array(
	        'id' => $id
	    ));
	}
    
	public function getByCategory($categoryId){
	    $select = new Select('products');
	    $select->join("users", "users.id = products.user_id", array(
	        "user_group_id" ,
	    ), $select::JOIN_LEFT);
	    $select->where->in('products.category_id', $categoryId);
	    $select->where->equalTo('products.status', 'show');
	    $select->where->notEqualTo('user_group_id', '7');
	    $select->order('id DESC');
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function searchByName($name){
	    $select = new Select('products');
// 	    $select->where->in('category_id', $categoryId);
	    $select->where->like('product_name', '%' . $name . '%');
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	
	
	public function updateStatus($status, $id)
	{
	    return $this->tableGateway->update(array('status' => $status),  array('id' => $id));
	}
}