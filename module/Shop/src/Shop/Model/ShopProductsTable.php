<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;

class ShopProductsTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	public $adapter;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->adapter = $this->getDbAdapter();
		$this->tableGateway = new TableGateway('shop_products', $this->adapter);
	}

	public function fetchAllProductOnly($where = array())
	{
		$select = new Select('shop_products'); 
		
	 	if ($where) { 
		    $select->where($where);
		}   
		
 		return $this->tableGateway->selectWith($select)->toArray();
	}
	public function fetchAllProduct($where = array())
	{
		$select = new Select('shop_products'); 
		$select->join('shop_product_attribute_info', 'shop_product_attribute_info.product_id=shop_products.id', array('*'), 'LEFT');
		$select->join('shop_product_attributes', 'shop_product_attributes.developer_name=shop_product_attribute_info.product_attribute_name', array('title','attribId'=>'id'), 'LEFT');
		$select->join('shop_categories', 'shop_categories.id=shop_products.category_id', array("category_name" => "name"), $select::JOIN_LEFT);
		if ($where) { 
		    $select->where($where);
		}   
 		return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getMaxPrice ($type = false) 
	{
	    $select = new Select('shop_products');
	    if ($type == "Real") {
	        $select->where->isNull("instructor_id");
	        $select->join("shop_categories", "shop_categories.id = shop_products.category_id", array("category_status" => "status"), $select::JOIN_LEFT);
            $select->where->equalTo("shop_categories.status", 1);
	    } else {
	        $select->where->isNotNull("instructor_id");
	        //$select->join("education_fieldofstudy", "education_fieldofstudy.id = shop_products.study_id", array("study_status" => "status"), $select::JOIN_LEFT);
	       // $select->join("education_lessons", "education_lessons.id = shop_products.lesson_id", array("lesson_status" => "status"), $select::JOIN_LEFT);
	       // $select->where->equalTo("education_fieldofstudy.status", 1);
	        //$select->where->equalTo("education_lessons.status", 1);
	    }
	    $select->where->equalTo("shop_products.status", 1);
	    $select->columns(array("max" => new Expression("MAX(CONVERT(price, SIGNED))")));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function changeSellCount($productsId, $value)
	{
	    $data = array(
	        'sell_count' => new Expression('sell_count + ' .$value)
	    );
	    return $this->tableGateway->update($data , array('id = ?' => $productsId));
	}
	
	public function changeVisitCount($productsId, $value)
	{
	    $data = array(
	        'visit_count' => new Expression('visit_count + ' .$value)
	    );
	    return $this->tableGateway->update($data , array('id = ?' => $productsId));
	}
	
    public function searchReal($catId = null, $lang)
	{
        $select = new Select('shop_products');
        $select->join("shop_categories", "shop_categories.id = shop_products.category_id", array("category_status" => "status", "category_name" => "name"), $select::JOIN_LEFT);
        $select->where->equalTo("shop_categories.status", 1);
        $select->where->isNull("shop_products.instructor_id");
        $select->where->equalTo("shop_products.product_lang", $lang);
        if ($catId) {
           $select->where->equalTo("category_id", $catId);
        }
    
        $select->where->equalTo("is_deleted", 0);
        $select->where->equalTo("shop_products.status", 1);
        $select->order("priority");
      
        $paginatorAdapter = new DbSelect($select,
        $this->tableGateway->getAdapter());
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
	}
	
	public function searchVirtual($productId = null, $mainLessons = null, $instructorIds = null, $pageSort = false, $ascDesc = "asc", $lang)
	{
	    $select = new Select('shop_products');
	    //$select->join("education_fieldofstudy", "education_fieldofstudy.id = shop_products.study_id", array("study_status" => "status"), $select::JOIN_LEFT);
	    //$select->join("education_lessons", "education_lessons.id = shop_products.lesson_id", array("lesson_status" => "status"), $select::JOIN_LEFT);
	    //$select->where->equalTo("education_fieldofstudy.status", 1);
	    //$select->where->equalTo("education_lessons.status", 1);
	    $select->where->isNotNull("shop_products.instructor_id");
	    $select->where->equalTo("shop_products.product_lang", $lang);
	    if($productId){
	        $select->where->in('related_products',$productId);
	    }
	    if ($mainLessons) {
	        $select->where->in("lesson_id", $mainLessons);
	    }
	    if ($instructorIds) {
	        $select->where->in("instructor_id", $instructorIds);
	    }
	    $select->where->equalTo("is_deleted", 0);
	    $select->where->equalTo("shop_products.status", 1);
	    if ($pageSort) {
	        if ($pageSort == "newest") {
	            $select->order("created_time " . ($ascDesc =="asc"?"ASC":"DESC") . ", priority");
	        } else if ($pageSort == "best_sell") {
	            $select->order("sell_count " . ($ascDesc =="asc"?"ASC":"DESC") . ", priority");
	        }
	    } else {
	        $select->order("priority");
	    }
	    $paginatorAdapter = new DbSelect($select,
	        $this->tableGateway->getAdapter());
	    $paginator = new Paginator($paginatorAdapter);
	    return $paginator;
	}
	
	
	public function fetchAll($productId = null, $related = false, $returnDeleted = false)
	{
		$select = new Select('shop_products');
		$select->join('shop_product_attribute_info', 'shop_product_attribute_info.product_id=shop_products.id', array('*'), 'LEFT');
		if ($productId > 0 && is_numeric($productId)) {
			$select->where->equalTo('id', $productId);
		}
		if (!$returnDeleted) {
		    $select->where->equalTo("is_deleted", 0);
		}
		if ($related) {
		    $select->where->isNull('related_products');
		    $select->where->isNotNull('instructor_id');
		}
		$select->group('shop_products.id');
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchById($productId)
	{
	    $select = new Select('shop_products');
	    if ($productId > 0 && is_numeric($productId)) {
	        $select->where->equalTo('id', $productId);
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getRecordsByParentId($catId)
	{
	    $select = new Select('shop_products');
	    if ($catId > 0 && is_numeric($catId)) {
	        $select->where->equalTo('category_id', $catId);
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetchByCategoryId($catId)
	{
	    $select = new Select('shop_products');
	    $select->join('shop_product_attribute_info', 'shop_product_attribute_info.product_id=shop_products.id', array('*'), 'LEFT');
	    if ($catId > 0 && is_numeric($catId)) { 
	        $select->where->equalTo('category_id', $catId);
	    }
	    $select->order('priority');
	    $select->where->equalTo('is_deleted', 0);
	    $select->group('shop_products.id');
	    //return $this->tableGateway->selectWith($select)->toArray();
	    return $select;
	}

	public function getCategoryId($productId)
	{
	    $select = new Select('shop_products');
	    
	    $select->where->equalTo('id', $productId);
	    $select->columns(array('category_id'));
	    return $this->tableGateway->selectWith($select)->toArray();
	    //return $select;
	}
	
	public function isExist($validData, $productId = null)
	{
		$select = new Select('shop_products');
		if ($productId) {
			$select->where(array(
					'(name = ? OR seo_name = ?)' => array($validData['name'], $validData['seo_name']),
					'id != ?' => $productId,
			        'is_deleted != ?' => 1
			));			
		} else {
			$select->where(array(
					'(name = ? OR seo_name = ?)' => array($validData['name'], $validData['seo_name']),
			        'is_deleted != ?' => 1
			));
		}
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function hasCategory($categoryId)
	{
		if ($categoryId > 0 && is_numeric($categoryId)) {
			$select = new Select('shop_products');
			$select->where->equalTo('category_id', $categoryId);
			$select->where->equalTo('is_deleted', "0");
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	
	public function addProduct($validData)
	{  
	  
		$this->tableGateway->insert(array(
				'name'             => $validData['name'],
				'seo_name'         => str_replace(array(" "), "-", $validData['seo_name']),
				'description'      => $validData['description'],
		        'description_additional'      => '',
				'meta_description' => $validData['meta_description'],
				'meta_keywords'    => $validData['meta_keywords'],
				'priority'         => $validData['priority'],
		        'rating'         => '',
    		    'price'            => '',
		        'downloadable'     => $validData['downloadable'],
    		    'image'            => $validData['image'],
		        'gallery_id'       => ((isset($validData['gallery_id']) && $validData['gallery_id']) ?$validData['gallery_id']:0),
                'file'             => (isset($validData['file'])?$validData['file']:''),
				'status'           => $validData['status'],
				'category_id'      => $validData['category_id'],
		   //     'related_products' => $validData['related_products'],
    		    'instructor_id'    => (isset($validData['instructor_id'])?$validData['instructor_id']:null),
		        'instructor_discount'    => (isset($validData['instructor_discount'])?$validData['instructor_discount']:null),
    		    'study_id'         => (isset($validData['study_id'])?$validData['study_id']:null),
    		    'lesson_id'        => (isset($validData['lesson_id'])?$validData['lesson_id']:null),
		        'product_lang'     => $validData['product_lang'],
		));
		return $this->tableGateway->getLastInsertValue();
	}
	
	public function updateProduct($validData, $productId)
	{
		if ($productId > 0 && is_numeric($productId)) {
			$newData = array(
				'name'             => $validData['name'],
				'seo_name'         => str_replace(array(" "), "-", $validData['seo_name']),
				'description'      => $validData['description'], 
				'meta_description' => $validData['meta_description'],
				'meta_keywords'    => $validData['meta_keywords'],
				'priority'         => $validData['priority'], 
			    'downloadable'     => $validData['downloadable'], 
				'status'           => $validData['status'],
				'category_id'      => $validData['category_id'],
			     'gallery_id'       => ((isset($validData['gallery_id']) && $validData['gallery_id']) ?$validData['gallery_id']:0),
                'file'             => (isset($validData['file'])?$validData['file']:''),
// 			    'related_products' => $validData['related_products'],
			    'instructor_id'    => (isset($validData['instructor_id'])?$validData['instructor_id']:null),
			    'instructor_discount'    => (isset($validData['instructor_discount'])?$validData['instructor_discount']:null),
			    'study_id'         => (isset($validData['study_id'])?$validData['study_id']:null),
			    'lesson_id'        => (isset($validData['lesson_id'])?$validData['lesson_id']:null),
			    'product_lang'     => $validData['product_lang'],
			);
			if ($validData['image'] == "deleted") {
			    $newData["image"] = null;
			} else if ($validData['image'] !== false) {
			    $newData["image"] = $validData['image'];
			}
			if ($validData['file'] == "deleted") {
			    $newData["file"] = null;
			} else if ($validData['file'] !== false) {
			    $newData["file"] = $validData['file'];
			}
			return $this->tableGateway->update($newData, array('id' => $productId));
		}
	}
	
	public function deleteProductImage($productId)
	{
	    if ($productId) {
	        $newData = array(
	            'image'            => null,
	        );
	        return $this->tableGateway->update($newData, array('id' => $productId));
	    }
	}
	
	public function deleteProductFile($productId)
	{
	    if ($productId) {
	        $newData = array(
	            'file'            => null,
	        );
	        return $this->tableGateway->update($newData, array('id' => $productId));
	    }
	}
	
	public function deleteProduct($productId)
	{
		if ($productId > 0 && is_numeric($productId)) {
			return $this->tableGateway->update(
			    array(
			        'is_deleted' => 1
			    ),
			    array(
					'id' => $productId
			));
		}
	}
	
	public function getProductsFilesByIds($ids = null)
	{
	    $select = new Select('shop_products');
	    $select->where->equalTo("is_deleted", 0);
	    if ($ids) {
	        $select->where->in('shop_products.id', $ids);
	        $select->where->isNotNull("file");
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getProductsFilesByIdsCheck($productId = null, $userId)
	{
	    $select = new Select('shop_products');
    	$select->where->equalTo("is_deleted", 0);
    	$select->join("orders_details", "orders_details.product_id = shop_products.id", array("order_id"), $select::JOIN_LEFT);
    	 
    	$select->join("orders", "orders_details.order_id = orders.id", array("payment_status"), $select::JOIN_LEFT);
    	 
    	 
    	$where = new Where();
    	$where->equalTo("payment_status", 1);
    	 
    	$where->equalTo("shop_products.id", $productId);
    	if ($userId) {
    	    $where->equalTo("orders.user_id", $userId);
    	}
    	
    	return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function search($product = false, $lang = "fa", $getPaginator = true)
	{
	/*      $textArra = array(
	         $product
	     );  */
	    $select = new Select('shop_products');
	    if ($product) { 
// 	        $select->where->expression("MATCH(name, description, meta_description, meta_keywords,sub_title) AGAINST (?)", $textArra);
	        $select->where->nest()->like('name', "%$product%")->or->like('description', "%$product%")->or->like('meta_keywords', "%$product%")->or->like('meta_description', "%$product%")->unnest();
	        $select->where->equalTo("product_lang", $lang);
	        $select->where->equalTo("is_deleted", "0");
	        $select->where->equalTo("status", "1");
	    } 
	    if ($getPaginator) {
	        $dbSelector = new DbSelect($select, $this->tableGateway->getAdapter());
	        return new Paginator($dbSelector);
	    } else {
	        return $this->tableGateway->selectWith($select)->toArray();
	    }
	}

	
	
	
	public function getProductsByIdsSlugs($ids = null, $slugs = null, $returnDeleted = false, $lang = "fa")
	{
	    $select = new Select('shop_products');
	    $select->join('shop_product_attribute_info', 'shop_product_attribute_info.product_id=shop_products.id', array('*'), 'LEFT');
	   // $select->join('users_data', 'users_data.user_id=shop_products.instructor_id', array('username' => new Expression('CONCAT(firstname, " ",lastname)')), 'LEFT');
	    //$select->where->equalTo("users_data.lang", $lang);
	    //$select->join('education_fieldofstudy', 'education_fieldofstudy.id=shop_products.study_id', array('study_name' => 'name'), 'LEFT');
	    //$select->join('education_lessons', 'education_lessons.id=shop_products.lesson_id', array('lesson_name' => 'name'), 'LEFT');
         $select->join('gallery_files',
            'shop_products.gallery_id = gallery_files.gallery_id', array(
                'gallery_images' => new Expression('Group_Concat(DISTINCT gallery_files.file_name)'),
        
            ), Select::JOIN_LEFT); 
        /* $select->join('shop_product_video_files',
            'shop_products.id = shop_product_video_files.product_id', array(
                'video_files' => new Expression('Group_Concat(DISTINCT CONCAT(shop_product_video_files.caption,"###", shop_product_video_files.file_name) ORDER BY shop_product_video_files.order ASC)'),
        
            ), Select::JOIN_LEFT); */
        if (!$returnDeleted) {
            $select->where->equalTo("is_deleted", 0);
        }
         if ($ids) {
            $select->where->in('shop_products.id', $ids);
        }
        if ($slugs) {
            $select->where->in('shop_products.seo_name', $slugs);
        } 
        $select->where->equalTo("product_lang", $lang);
        $select->group("shop_products.id"); 
        return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getMaxPriceNew ($type = false)
	{
	    $select = new Select('shop_products');
	    if ($type == "Real") {
	        $select->where->isNull("instructor_id");
	        $select->join("shop_categories", "shop_categories.id = shop_products.category_id", array("category_status" => "status"), $select::JOIN_LEFT);
	        $select->where->equalTo("shop_categories.status", 1);
	    } else {
	        $select->where->isNotNull("instructor_id");
	        //$select->join("education_fieldofstudy", "education_fieldofstudy.id = shop_products.study_id", array("study_status" => "status"), $select::JOIN_LEFT);
	        // $select->join("education_lessons", "education_lessons.id = shop_products.lesson_id", array("lesson_status" => "status"), $select::JOIN_LEFT);
	        // $select->where->equalTo("education_fieldofstudy.status", 1);
	        //$select->where->equalTo("education_lessons.status", 1);
	    }
	    $select->where->equalTo("shop_products.status", 1);
	    $select->columns(array("max" => new Expression("MAX(CONVERT(price, SIGNED))")));
	    $data =  $this->tableGateway->selectWith($select)->toArray();
	    var_dump($data);die;
	}
	
}
