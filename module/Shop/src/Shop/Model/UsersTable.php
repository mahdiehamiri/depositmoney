<?php 
namespace Shop\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Profiler\Profiler;

class UsersTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('users', $this->getDbAdapter());
    }
	public function getRecords($where = null)
	{
		return $this->tableGateway->select($where)->toArray();
	}
	
	public function getUserChildsCount($userId = null)
	{
	    $select = new Select('users');
	    $select->columns(array("count" => new Expression("COUNT(id)")));
	    $select->where(array(
	        'user_creator' => $userId,
	        'status' => 1
	    ));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function getUserById($userId = null, $lang)
	{
	    $select = new Select('users');
	    $select->join("users_data", "users.id = users_data.user_id", array('*'), $select::JOIN_LEFT);
	    $select->where(array(
	        'users.id' => $userId,
	        'lang' => $lang
	    ));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function updateUserLastLogin($userId)
	{
	    return $this->tableGateway->select(array(
	        'last_user_login' => new Expression("NOW()"),
	        'id = ?' => $userId
	    ))->toArray();
	}
	
	public function getAllInstructors($lang = null)
	{
	    $select = new Select('users');
	    $select->columns(array('id', 'email','username'));
	    if ($lang) {
	        $select->join("users_data", "users.id = users_data.user_id", array('firstname', 'lastname'), $select::JOIN_LEFT);
	        $select->where->equalTo("lang", $lang);
	    }
	    $select->where(array(
	        '(user_group_id = ? or user_group_id = ?)' => array(4,5)
	    ));
	    return $this->tableGateway->selectWith($select)->toArray();
	}

	public function registerNewUser($newUserData, $defaultUserGroup)
	{
	    $isEmailRegistered = $this->tableGateway->select(array(
	        'email' => $newUserData['email']
	    ))->toArray();
	    if (!$isEmailRegistered) {
	        $salt = rand(1000000, 9000000);
	        $data = array(
	            'email'    		=> $newUserData['email'],
	            'password' 		=> md5(sha1($newUserData['password']) . $salt),
	            'salt'     		=> $salt,
	            'user_group_id' => $defaultUserGroup,
	            'status'		=> 1,
	            'username'      => $newUserData['email'],
	        );
	        return $this->tableGateway->insert($data);
	    } else {
	        return 'user_exists';
	    }
	}
}
