<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ShopAttributeGroupsCategoryIndexTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_attribute_groups_category_index', $this->getDbAdapter());
	}
	public function getAttributesByAttributeId($attrId, $getProduct)
	{
	    $select = new Select('shop_attribute_groups_category_index');
	    $select->where->equalTo("shop_category_id", $attrId);
	    if ($getProduct) {
	        
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function add($attributeGroupId, $categoryId)
	{
		return $this->tableGateway->insert(array(
				'attribute_group_id' => $attributeGroupId,
				'shop_category_id'   => $categoryId
		));
		
	}
	
	public function delete($id)
	{
		return $this->tableGateway->delete(array(
				'shop_category_id' => $id
		));
	}
	
}
