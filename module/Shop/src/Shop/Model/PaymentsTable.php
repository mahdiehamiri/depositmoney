<?php
namespace Shop\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Application\Services\BaseModel;

class PaymentsTable extends BaseModel 
{
	protected $tableGateway;
	protected $serviceManager;
	public $adapter;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->adapter = $this->getDbAdapter();
		$this->tableGateway = new TableGateway('payments', $this->adapter);
	}

    public function fetchAll( $where = null )
    {
    	$results = $this->tableGateway->select( $where );
    	return $results->toArray() ;
    }

    public function getPayments($where = null)
    {
        $select = new Select('payments');
        $select->order("payments.id desc");
        $select->join(array("u" => "users"), "u.id = payments.user_id", array("email"));
        $select->join(array("b" => "banks"), "b.id = payments.bank_id",array("bank_name" => "name"));
        if ($where) {
            $select->where($where);
        }
        return $select;
    }

    
    
    public function setErrorPayment(  $orderId , $resCode , $saleReferenceID  , $message )
    {
        $data = array(
                'res_code'          => $resCode ,
                'sale_reference_id' => $saleReferenceID,
                'message'           => $message,
                'status'            => 'error'
        );
        return $this->tableGateway->update($data , array('order_id = ?' => $orderId));
    }

    public function confirmPayment($orderId, $resCode, $saleReferenceID, $message)
    {
        $data = array(
                'res_code'          => $resCode ,
                'sale_reference_id' => $saleReferenceID,
                'message'           => $message,
                'status'            => 'paid'
        ); 
        return $this->tableGateway->update($data , array('order_id = ?' => $orderId));
    }

    public function addPayment( $userId , $amount , $bankId , $orderId)
    {
        $userIp = getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
        getenv('HTTP_FORWARDED_FOR')?:
        getenv('HTTP_FORWARDED')?:
        getenv('REMOTE_ADDR');
        $data = array(
                'user_id'      => $userId ,
                'amount'       => $amount ,
                'bank_id'      => $bankId,
                'order_id'     => $orderId,
                'ip_address'   => $userIp,
                'added_date'    => new Expression("CURDATE()"),
                'added_time'    => new Expression("CURTIME()")
        );
        return $this->tableGateway->insert($data);
       
    }

    public function setReferenceId( $orderId , $referenceId )
    {
        $data = array(
                'reference_id'      => $referenceId
        );
        return $this->tableGateway->update($data , array('order_id = ?' => $orderId));
    }
    
    public function deletePayment($paymentId)
    {
        return $this->tableGateway->delete(array(
            'id' => $paymentId
        ));
    }

}