<?php 

namespace Shop\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Zend\Db\Sql;
use Zend\Db\Sql\Where;

class GalleriesTable extends BaseModel 
{
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('galleries', $this->getDbAdapter());
    }
	
	public function getAllGallery()
	{
	    $sql = "SELECT `id`, `persian_name`, `latin_name`, b.`parent_id`, a.Count FROM `galleries` b LEFT OUTER JOIN (SELECT  `parent_id`, COUNT(*) AS Count FROM `galleries` group by `parent_id`)a on b.`id` = a.`parent_id`";
	    return $this->tableGateway->getAdapter()->driver->getConnection()->execute($sql);
	}
	
}
