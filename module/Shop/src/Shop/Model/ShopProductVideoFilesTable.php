<?php

namespace Shop\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Services\BaseModel;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class ShopProductVideoFilesTable extends BaseModel 
{
public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('shop_product_video_files', $this->getDbAdapter());
    }
 
	public function getVideos($productIds = false, $userId)
	{  
		$select = new Select("shop_product_video_files");		
		$where = new Where();
		$where->in("shop_product_video_files.product_id", $productIds);
		
		$select->where($where);
		$select->group("file_name");
		$select->order('shop_product_video_files.order DESC');
		return $this->tableGateway->selectWith($select)->toArray(); 
	}
}