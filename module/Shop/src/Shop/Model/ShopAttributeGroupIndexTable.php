<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class ShopAttributeGroupIndexTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_attribute_group_index', $this->getDbAdapter());
	}
	
	public function getAttributes($groupId)
	{
	    $select = new Select('shop_attribute_group_index');
	    $select->where->equalTo("attribute_group_id", $groupId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function getAttributesByAttributeId($attrId)
	{
	    $select = new Select('shop_attribute_group_index');
	    $select->where->equalTo("shop_category_id", $attrId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	public function checkExist($groupId, $attributeId)
	{
	    $select = new Select('shop_attribute_group_index');
	    $select->where->notEqualTo("attribute_group_id", $groupId);
	    $select->where->equalTo("attribute_id", $attributeId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function checkCount($attributeId)
	{
	    $select = new Select('shop_attribute_group_index');
	    $select->columns(array(
	        'count' => new Expression('COUNT(attribute_id)')
	        ));
	    $select->where->equalTo("attribute_id", $attributeId);
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function fetch($groupId)
	{
		$select = new Select('shop_attribute_group_index');
		
		$select->join('shop_product_attributes', 'shop_attribute_group_index.attribute_id = shop_product_attributes.id',
		    array("order"),"LEFT");
			
		$select->order("order DESC");
		$select->where->equalTo('attribute_group_id', $groupId)	;
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	
	public function add($attributeGroupId, $attributeId)
	{
		if ($attributeGroupId && $attributeId) {
			return $this->tableGateway->insert(array(
					'attribute_group_id'   => $attributeGroupId,
					'attribute_id' => $attributeId
			));
		}
	}
	
	public function delete($attributeGroupId = null, $attributeId)
	{
		if ($attributeGroupId && $attributeId) {
			return $this->tableGateway->delete(array(
					'attribute_group_id'   => $attributeGroupId,
					'attribute_id' => $attributeId
			));
		} else {
			return $this->tableGateway->delete(array(
					'attribute_id' => $attributeId
			));			
		}
	}
	
}
