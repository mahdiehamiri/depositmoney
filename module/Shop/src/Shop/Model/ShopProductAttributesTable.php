<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class ShopProductAttributesTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_product_attributes', $this->getDbAdapter());
	} 
	 
	public function fetchAll($attributeId = null)
	{
	    $select = new Select('shop_product_attributes');
	    if ($attributeId) {
	        $select->where->equalTo('id', $attributeId);
	        $select->join('shop_attribute_group_index', 'shop_attribute_group_index.attribute_id = shop_product_attributes.id',
	            array("attribute_groups"=>new Expression("Group_Concat(shop_attribute_group_index.attribute_group_id)")),"LEFT");
	    }
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function checkExist($validData)
	{
		$select = new Select('shop_product_attributes');
		$select->where(array(
				'(developer_name = ?)' => array($validData['developer_name'])
		)); 
		return $this->tableGateway->selectWith($select)->toArray();
	}
	
	public function add($validData)
	{
		$this->tableGateway->insert(array(
				'title'           => $validData['title'],
				'title_en'        => $validData['title_en'],
				'developer_name'  => str_replace(" ", "_", $validData['developer_name']),
				'type'            => $validData['type'],
				'is_forced'       => $validData['is_forced'],
				'status'          => $validData['status'],
				'kind'            => $validData['kind'],
		        'order'           => $validData['order'],
		));
		return $this->tableGateway->getLastInsertValue();
	}
	
	public function update($validData, $attributeId)
	{
		if ($attributeId > 0 && is_numeric($attributeId)) {
			return $this->tableGateway->update(array(
					'title'     => $validData['title'],
					'title_en'  => $validData['title_en'],
					'type'      => $validData['type'],
					'is_forced' => $validData['is_forced'],
					'status'    => $validData['status'],
					'kind'      => $validData['kind'],
			        'order'     => $validData['order'],
			), array(
					'id' => $attributeId
			));
		}
	}
	
	public function delete($attributeId)
	{
		if ($attributeId > 0 && is_numeric($attributeId)) {
			return $this->tableGateway->delete(array(
					'id' => $attributeId
			));
		}
	}
}
