<?php 
namespace Shop\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class TaxTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'tax';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('tax', $this->getDbAdapter());
    }
	public function getRecords($id = false)
	{
		return $this->tableGateway->select()->toArray();
	}
	
	public function getTaxById($id)
	{
	    $select = new Select($this->table);
	    $select->where(array(
	        'id' => $id
	    ));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
	

	
	public function getAllCurrentTaxes($where = null)
	{
	    $select = new Select($this->table);
	    if (!$where) {
	        $where = new Where();
	    }
	    $where->nest()->lessThanOrEqualTo("from_date", new Expression('CURDATE()'))->and->greaterThanOrEqualTo("to_date", new Expression('CURDATE()'))->unnest();
        $select->where($where);	   
        $select->order("tax desc");        
        $all = $this->tableGateway->selectWith($select)->toArray();
        if ($all) {
            return $all[0]["tax"];
        } else {
            return 0;
        }
	}
	

}
