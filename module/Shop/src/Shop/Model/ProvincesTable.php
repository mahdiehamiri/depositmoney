<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class ProvincesTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('provinces', $this->getDbAdapter());
    }

	public function getRecords($where = null)
	{
		return $this->tableGateway->select($where)->toArray();
	}
	
	
	public function getRecordsByCountry($id = false)
	{
	    $select = new Select('provinces');
	    if ($id) {
	        $select->where->equalTo('country_id', $id);
	        return $this->tableGateway->selectWith($select)->toArray();
	    }
	}
}
