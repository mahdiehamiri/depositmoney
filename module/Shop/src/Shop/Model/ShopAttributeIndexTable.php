<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ShopAttributeIndexTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_attribute_group_index', $this->getDbAdapter());
	}
	
	public function fetchByCatId($groupId)
	{
		$select = new Select('shop_attribute_group_index');
		$select->join('shop_product_attributes', 'shop_product_attributes.id = shop_attribute_group_index.attribute_id');
		$select->where->equalTo('attribute_group_id', $groupId)	;
		//return $this->tableGateway->selectWith($select)->toArray();
	    return $select;
	}
	
	
}
