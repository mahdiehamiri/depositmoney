<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;

class BanksTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('banks', $this->getDbAdapter());
    }

    public function getBanks($bankId = false)
    {
        $where = new Where();
        $where->equalTo("status", "1");
        if ($bankId) {
            $where->equalTo("id", $bankId);
        }
        return $this->tableGateway->select($where)->toArray();
      
    }
    
    public function fetchAll( $where = null )
    {
        $results = $this->tableGateway->select( $where );
        return $results->toArray() ;
    }
}
