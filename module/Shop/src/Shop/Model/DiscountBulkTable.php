<?php 
namespace Shop\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class DiscountBulkTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'discount_bulk';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        //$this->tableGateway = new TableGateway('discount_bulk', $this->getDbAdapter());
    }

	/* public function getDiscountBulkByCount($count = null)
	{
	    $select = new Select($this->table);
	    if ($count) {
	        $where = new Where();
	        $where->lessThanOrEqualTo("bulk_users_count", $count);
	        $select->where($where);
	        $select->order("bulk_users_count DESC");
	        return $this->tableGateway->selectWith($select)->toArray();
	    } else {
	        return false;
	    }
	} */

}
