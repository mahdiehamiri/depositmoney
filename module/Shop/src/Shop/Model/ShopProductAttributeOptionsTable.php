<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ShopProductAttributeOptionsTable extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_product_attribute_options', $this->getDbAdapter());
	}
	
	public function fetchAttributeValues($attributeId)
	{
		if ($attributeId > 0 && is_numeric($attributeId)) {
			return $this->tableGateway->select(array(
					'attribute_id' => $attributeId
			))->toArray();
		}
	}
	
	public function add($validData)
	{
		return $this->tableGateway->insert(array(
				'attribute_id' => $validData['attribute_id'],
				'index'        => $validData['index'],
				'value'        => $validData['value'],
		));
	}
	
	public function delete($attributeId)
	{
		if ($attributeId > 0 && is_numeric($attributeId)) {
			return $this->tableGateway->delete(array(
					'attribute_id' => $attributeId
			));
		}
	}
}
