<?php
namespace Shop\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Application\Services\BaseModel;
use Zend\Db\Sql\Where;

class OrdersDetailsTable extends BaseModel 
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
    	 $this->serviceManager = $sm ;
    	 $this->tableGateway = new TableGateway('orders_details', $this->getDbAdapter());
    }

    public function getOrderDetailsByOrderId( $orderId = null )
    {
    	return $this->tableGateway->select(array("order_id" => $orderId))->toArray();
    }

    public function getVideosCheck($productIds = false, $userId = false)
    {
        $select = new Select("orders_details");
        $select->join("orders", "orders_details.order_id = orders.id", array("payment_status"), $select::JOIN_LEFT);
    
    
        $where = new Where();
        $where->equalTo("payment_status", 1);
    
        $where->in("orders_details.product_id", $productIds);
        if ($userId) {
            $where->equalTo("orders.user_id", $userId);
        }
        $select->where($where);
        return $this->tableGateway->selectWith($select)->toArray();
    }
    
    public function getSellsDetails($where = null, $paginator = true)
    {
        
        $select2 = new Select('orders_details');
         
        $select2->join("shop_products", "orders_details.product_id = shop_products.id", array("name", "instructor_id", "instructor_discount"), $select2::JOIN_LEFT);
        $select2->join("users", "shop_products.instructor_id = users.id", array("username"), $select2::JOIN_LEFT);
        $select2->join("orders", "orders_details.order_id = orders.id", array("order_status", "payment_status"), $select2::JOIN_LEFT);
        $select2->where($where);

        $select2->columns(array(
            "SumTotal" => new Expression("SUM(sum * quantity)"),
            "SumDiscount" => new Expression("SUM(sum * quantity * instructor_discount/100)"),
           
        ));
        $select2->group("instructor_id");
        
        $sumTotal = $this->tableGateway->selectWith($select2)->toArray();
      
       
        $select = new Select('orders_details');
       
        $select->join("shop_products", "orders_details.product_id = shop_products.id", array("name", "instructor_id", "instructor_discount"), $select::JOIN_LEFT);
        $select->join("users", "shop_products.instructor_id = users.id", array("username"), $select::JOIN_LEFT);
        $select->join("orders", "orders_details.order_id = orders.id", array("order_status", "payment_status","added_date_time"), $select::JOIN_LEFT);
        $select->where($where);
        if ($paginator) {
            $paginatorAdapter = new DbSelect(
                $select,
                $this->tableGateway->getAdapter()
            );
            $paginator = new Paginator($paginatorAdapter);
            return array($paginator, $sumTotal);
        } else {
            $data = $this->tableGateway->selectWith($select)->toArray();
            return array($data, $sumTotal);
        }
        
    }
    
    public function getOrderDetails($orderId = null)
    {
        $select = new Select('orders_details');
        $select->join("shop_products", "orders_details.product_id = shop_products.id", array("name"), $select::JOIN_LEFT);
        if($orderId)
            $select->where->equalTo("order_id", $orderId);
        return $select;
    }

    public function getUserFiles($userId = false)
    {
        $select = new Select('orders_details');
        $select->join("orders", "orders_details.order_id = orders.id", array("payment_status"), $select::JOIN_LEFT);
        $select->join("shop_products", "orders_details.product_id = shop_products.id", array("id", "name","file"), $select::JOIN_LEFT);
      
        
        $where = new Where();
        $where->equalTo("payment_status", 1);
        $where->isNotNull("instructor_id");
        if ($userId) {
            $where->equalTo("orders.user_id", $userId);
        }
        $select->where($where);
        return $select;
    }

    public function changeOrderStatus($orderId , $status)
    {
        $data = array(
                'status' => $status 
        ); 
        return $this->tableGateway->update($data , array('id = ?' => $orderId));
    }

    public function addOrderDetails( $orderId, $productId, $productInfo)
    {
        $data = array(
                'order_id'       => $orderId,
                'product_id'     => $productId,
                'price'          => $productInfo["price"],
                'total_price'    => $productInfo["price"] * $productInfo["quantity"],
                'quantity'       => $productInfo["quantity"],
                'total_tax'      => $productInfo["tax"] * $productInfo["quantity"],
                'tax_rate'       => $productInfo["taxRate"],
                'total_discount' => $productInfo["discount"] * $productInfo["quantity"],
                'discount_rate'  => $productInfo["discountRate"],
                'sum'            => $productInfo["sum"] 
        );
        return $this->tableGateway->insert($data);
       
    }

}