<?php 

namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ShopProductAttributeInfo extends BaseModel
{
	protected $tableGateway;
	protected $serviceManager;
	
	public function __construct($sm)
	{
		$this->serviceManager = $sm;
		$this->tableGateway = new TableGateway('shop_product_attribute_info', $this->getDbAdapter());
	}
	
	public function fetch($productId)
	{
		if ($productId > 0 && is_numeric($productId)) {
			$select = new Select('shop_product_attribute_info');
			$select->join('shop_product_attributes', 'shop_product_attributes.developer_name=shop_product_attribute_info.product_attribute_name', 
					array('type', 'is_forced'), 'LEFT');
			$select->where->equalTo('product_id', $productId);
			return $this->tableGateway->selectWith($select)->toArray();
		}
	}
	
	public function add($productId, $name, $value)
	{
		if ($productId > 0 && is_numeric($productId)) {
			return $this->tableGateway->insert(array(
					'product_id'             => $productId,
					'product_attribute_name' => $name,
					'value'                  => trim($value)
			));
		}
	}
	
	public function delete($productId)
	{
		if ($productId > 0 && is_numeric($productId)) {
			return $this->tableGateway->delete(array(
					'product_id' => $productId
			));
		}
	}
}
