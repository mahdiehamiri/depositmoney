<?php 
namespace Shop\Model;

use Application\Services\BaseModel;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;

class EducationLessonsTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    protected $table = 'education_lessons';
    
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway($this->table, $this->getDbAdapter());
    }
	public function getRecords($where = false)
	{
	    $select = new Select($this->table);
		return $this->tableGateway->select($where)->toArray();
	}

	public function getRecordsByParent($parentIds = false)
	{
	    $select = new Select($this->table);
	    $where = new Where();
	    $where->in("parent_id", $parentIds);
	    return $this->tableGateway->select($where)->toArray();
	}
	
	public function getRecordsByParentId($id = false)
	{
	    $select = new Select($this->table);
	    $select->where(array(
	        'parent_id' => $id
	    ));
	    return $this->tableGateway->selectWith($select)->toArray();
	}
}
