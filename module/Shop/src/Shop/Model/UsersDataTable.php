<?php 
namespace Shop\Model;

use Application\Services\BaseModel;
use Application\Services\BaseModelInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Profiler\Profiler;

class UsersDataTable extends BaseModel
{
    protected $tableGateway;
    protected $adapter;
    protected $serviceManager;
    public function __construct($sm)
    {
        $this->serviceManager = $sm;
        $this->tableGateway = new TableGateway('users_data', $this->getDbAdapter());
    }
	
	public function addUserData($postData, $userId)
	{
        $this->tableGateway->insert(array(
            'firstname' 	    => $postData['firstname'],
            'lastname' 		    => $postData['lastname'],
            'experience'        => $postData['experience'],
            'research'          => $postData['research'],
            'aboutme' 		    => $postData['aboutme'],
            'user_id'           => $userId,
            'lang'              => $postData['lang'],
        ));
        return $this->tableGateway->getLastInsertValue();
	}

	

}
