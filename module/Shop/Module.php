<?php
namespace Shop;

use Shop\Helper\ShopHelper;
use Shop\Helper\BanksHelper;
use Shop\Helper\BanksErrorHelper;
use Shop\Helper\ShopViewHelper;
class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getControllerPluginConfig()
    {
    	return array(
    		'factories' => array(
        		   'shopHelper' => function() {
        		    return new ShopHelper();
        		    },
        		    'banksErrorHelper' => function() {
        		    return new BanksErrorHelper();
        		    },
    		)
    	);
    }
    
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'shopViewHelper' =>  function() {
                    return new ShopViewHelper();
                },
    
                 
            ),
           
    
        );
    }
}
