<?php
return array(
    'router' => array(
        'routes' => array( 
        	'shop' => array(
        			'type'    => 'Segment',
        			'options' => array(
        					'route'    => '[/:lang]/shop[/:action]',
        					'constraints' => array(
        							'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
//         							'title' => '[a-zA-Z-][a-zA-Z0-9_-]+',
//         							'slug'  => '.+'
        					),
        					'defaults' => array(
        					    '__NAMESPACE__' => 'Shop\Controller',
        					    'controller' => 'Index',
        					    'action' => 'real'
        					),
        			),
        	), 
        	'admin-shop' => array(
        		'type'    => 'Segment', 
       			'options' => array(
       					'route'    => '[/:lang]/admin-shop[/:action][/:id][/group/:group][/token/:token][/clear/:clear][/export/:export][/page/:page][/count/:count]',
       					'constraints' => array(
       							'action' => '[a-zA-Z-][a-zA-Z0-9_-]+',
       							'id' 	 => '[1-9][0-9]*',
       					        'group' 	 => '[1-9][0-9]*',
       							'token'  => '[a-f0-9]{32}',
   					            'clear' => 'true',
       					        'export' => 'true',
           					    'count' => '[0-9]+',
           					    'page' => '[0-9]+',
       					),
       					'defaults' => array(
       							 '__NAMESPACE__' => 'Shop\Controller',
        					    'controller' => 'Admin',
        					    'action' => 'categories'
       				    	),
       			   ),
        	 ),   
        	'admin-shop_ajax_delete_product_image' => array(
        		'type'    => 'Literal', 
       			'options' => array(
       					'route'    => '/admin-shop/delete-image',
       					'defaults' => array(
       					    '__NAMESPACE__' => 'Shop\Controller',
       					    'controller' => 'Ajax',
       					    'action' => 'delete-image'

       				    	),
       			   ),
        	 ),   
            'admin-shop_ajax_delete_product_file' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin-shop/delete-file',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Shop\Controller',
                        'controller' => 'Ajax',
                        'action' => 'delete-file'
            
                    ),
                ),
            ),
            'admin-shop_ajax_delete_category_icon' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin-shop/delete-category-icon',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Shop\Controller',
                        'controller' => 'Ajax',
                        'action' => 'delete-category-icon'
            
                    ),
                ),
            ),
            'shop-home' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '[/:lang]/shop[/:action][/:id][/page/:page][/count/:count][/sort/:sort][/type/:type][/:slug][?:query]',
                    'constraints' => array(
                        'lang' => '[a-zA-Z]{2}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                        'count' => '[0-9]+',
                        'order' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'type' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'slug' => '.*',
                        'query' => '.*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Shop\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index'
                    ),
                )
            ),
            
            'admin-shop-ajax-get-lessons' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin-shop/get-lessons',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Shop\Controller',
                        'controller' => 'Ajax',
                        'action' => 'get-lessons'
            
                    ),
                ),
            ),
            
            'admin-shop-ajax-get-product-by-id' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin-shop/get-product-by-id',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Shop\Controller',
                        'controller' => 'Ajax',
                        'action' => 'get-product-by-id'
            
                    ),
                ),
            ),
            
            'shop-user-ajax' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/shop-user-ajax[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '.*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Shop\Controller',
                        'controller' => 'AjaxUser',
                        'action' => 'delete-product-card'
            
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
    ),

    'view_manager' => array(
        'template_map' => array(
            include(__DIR__ . '/../template_map.php'),
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),	
    ),
);
