-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 24, 2021 at 08:46 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `depositmoney`
--

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

DROP TABLE IF EXISTS `deposit`;
CREATE TABLE IF NOT EXISTS `deposit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `discount` varchar(10) NOT NULL,
  `tax` varchar(20) NOT NULL,
  `to_pay_price` varchar(50) NOT NULL,
  `description` text DEFAULT NULL,
  `request_check` varchar(50) DEFAULT NULL,
  `created_check` date DEFAULT NULL,
  `pay_to` varchar(100) NOT NULL,
  `payment_details` varchar(100) DEFAULT NULL,
  `created` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deposit`
--

INSERT INTO `deposit` (`id`, `user_id`, `discount`, `tax`, `to_pay_price`, `description`, `request_check`, `created_check`, `pay_to`, `payment_details`, `created`) VALUES
(1, 1, '20', '20', '', 'fdefe', 'dsdsa', '2021-08-21', 'tyut6u767', 'dsedfe', '2021-08-21'),
(11, 1, '2,000', '9', '25,070', 'aaaaaaaaaaaa', 'شش ماهه', '2021-09-27', 'شبدیز', '', '2021-08-24');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_account`
--

DROP TABLE IF EXISTS `deposit_account`;
CREATE TABLE IF NOT EXISTS `deposit_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deposit_id` int(11) NOT NULL,
  `account_type` varchar(50) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `account_owner` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deposit_account`
--

INSERT INTO `deposit_account` (`id`, `deposit_id`, `account_type`, `bank_name`, `account_number`, `account_owner`) VALUES
(1, 10, 'شماره کارت', 'saman', '621986', 'donya'),
(2, 11, 'شماره کارت', 'saman', '621986', 'donya');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_details`
--

DROP TABLE IF EXISTS `deposit_details`;
CREATE TABLE IF NOT EXISTS `deposit_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deposit_id` int(11) NOT NULL,
  `product` varchar(100) NOT NULL,
  `count` varchar(10) NOT NULL,
  `price` varchar(20) NOT NULL,
  `sum` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deposit_details`
--

INSERT INTO `deposit_details` (`id`, `deposit_id`, `product`, `count`, `price`, `sum`) VALUES
(1, 9, 'کالا 1', '1', '25,000', '25,000'),
(2, 11, 'کالا 1', '1', '25,000', '25,000'),
(3, 11, 'کالا 1', '1', '25,000', '25,000');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
