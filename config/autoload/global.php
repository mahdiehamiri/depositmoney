<?php

return array(
//      'db' => array(
//         'driver'         => 'Pdo',
//         'dsn'            => 'mysql:dbname=benis;host=109.110.163.10',
//         // 'dsn'            => 'mysql:unix_socket=/var/lib/mysql/mysql.sock;dbname=azimmwpy_cadafzar;',
//         'username'       => 'benis', 
//         'password'       => 'Tmld5@43',
//         'driver_options' => array(
//             PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
//         ),
//     ),  
    'db' => array(
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=shabdiz;host=localhost',
        'username'       => 'root',
        'password'       => '',
        // 'dsn'            => 'mysql:dbname=depositmoney;host=localhost',
        // 'username'       => 'root',
        // 'password'       => '',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
                    => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
		
	// The default user group is set here,
	// Type 2 is an ordinary user with limited permissions	
	'default_user_group' => 3,
		
	// Status 1 lets users comment without verification
	// Status 0, Blocks comments and needs admin verification	
	'default_comment_status' => '1',
		
	//'base_route' => getcwd() . "/../public/",
	//Sets the base root of the application 	
	'base_route' => $_SERVER['DOCUMENT_ROOT'],
    'site_url' => $_SERVER["SERVER_NAME"],
	// The layouts of the Content Module are set here.
	// To add a new layout, please add a new key => value to 
	// the following array
	'layout_types' => array(
			'0' => 'لطفا یک گزینه را انتخاب نمایید',
			'1' => 'blog',
			'2' => 'home',
			'3' => 'contact'
	),
     
// 	"activeLog"  =>  array(
// 			"myOnlineUserLog" => true,
// 	),
    "activeLog"  =>  array(
        "myProductEventLog"                => true,
        "myUserEventLog"                => true,
        "myIncorrectLoginEventLog"                => true,
    ),
    /* 'education' => array(
        '1' => __('Diploma'),
        '2' => __('Associate Degree'), // فوق دیپلم
        '3' => __('Bachelor'),
        '4' => __('MA'),
        '5' => __('P.H.D'),
        '6' => __('Post-doctoral'),
    ),
    'skill_status' => array(
        '1' => __('very good'),
        '2' => __('good'), // فوق دیپلم
        '3' => __('not bad'),
        '4' => __('bad'),
        '5' => __('very bad'),
    ), */
);
